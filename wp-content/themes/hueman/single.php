<?php get_header();?>

<section class="content">
	
	<?php get_template_part('inc/page-title'); ?>
	<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
	<div class="pad group">
		
		<?php while ( have_posts() ): the_post(); ?>
			<article <?php post_class(); ?>>	
			<div itemscope itemtype="http://schema.org/Recipe">
  <meta itemprop="name" content="<?php the_title( ); ?>">
   <?php	
   if( has_post_thumbnail() ) {
	$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( null ) );
	if( ! empty( $thumbnail ) ) { ?>
    <meta itemprop="image" content="<?php echo $thumbnail[0]; ?>">  
   <?php }}?>
  <span itemprop="author" itemscope itemtype="https://schema.org/Person">  
    <meta itemprop="name" content="<?php the_author(); ?>">
  </span>
  <?php
  foreach ( get_post_custom_values( 'total_time' ) as $key => $value ) {
    $total= $value; 
  }
?>
 
  <meta itemprop="totalTime" content="PT<?php if($total==null){echo '30';}else{ echo $total; } ?>M">
  <span class="duration"><span class="value-title" title="PT<?php if($total==null){echo '30';}else{ echo $total; } ?>M"> </span>   </span>
  <meta itemprop="description" class="description" content="<?php echo strip_tags(get_the_excerpt()); ?>">
				<div class="post-inner group"> 
					
					<h1 class="post-title"><?php the_title(); ?></h1>
					<p class="post-byline"><?php _e('by','hueman'); ?> <?php the_author_posts_link(); ?> &middot; <?php the_time(get_option('date_format')); ?></p>
					
					<?php if( get_post_format() ) { get_template_part('inc/post-formats'); } ?>
					<?php if(function_exists('the_ratings')) { 
		                            the_ratings(); 
?> 
                                          <?php } ?> 
					<div class="clear"></div>
					
					<div class="entry">	
						<div class="entry-inner">
							<?php the_content(); ?>
							<?php wp_link_pages(array('before'=>'<div class="post-pages">'.__('Pages:','hueman'),'after'=>'</div>')); ?>
						</div>
						<div class="clear"></div>				
					</div><!--/.entry-->
					</div>   
				</div><!--/.post-inner-->	
			</article><!--/.post-->				
		<?php endwhile; ?>
		 
		<div class="clear"></div>
		
		<?php the_tags('<p class="post-tags"><span>'.__('Tags:','hueman').'</span> ','','</p>'); ?>
		
		<?php if ( ( ot_get_option( 'author-bio' ) != 'off' ) && get_the_author_meta( 'description' ) ): ?>
			<div class="author-bio">
				<div class="bio-avatar"><?php echo get_avatar(get_the_author_meta('user_email'),'128'); ?></div>
				<p class="bio-name"><?php the_author_meta('display_name'); ?></p>
				<p class="bio-desc"><?php the_author_meta('description'); ?></p>
				<div class="clear"></div>
			</div>
		<?php endif; ?>
		
		<?php if ( ot_get_option( 'post-nav' ) == 'content') { get_template_part('inc/post-nav'); } ?>
		
		<?php if ( ot_get_option( 'related-posts' ) != '1' ) { get_template_part('inc/related-posts'); } ?>
		
		<?php comments_template('/comments.php',true); ?>
		
	</div><!--/.pad-->
	
</section><!--/.content-->

<?php get_sidebar(); ?>

<?php get_footer(); ?>