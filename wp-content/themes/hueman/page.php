<?php get_header(); ?>

<section class="content">
	
	<?php get_template_part('inc/page-title'); ?>
	
	<div class="pad group">
		
		<?php while ( have_posts() ): the_post(); ?>
		
			<article <?php post_class('group'); ?>>
				<div itemscope itemtype="http://schema.org/NewsArticle">
  <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage" itemid="https://google.com/article"/>
  <meta itemprop="inLanguage" content="vi-VN"> 
  <meta itemprop="headline" content="<?php the_title( ); ?>">
  <span itemprop="author" itemscope itemtype="https://schema.org/Person">
    <meta itemprop="name" content="<?php the_author(); ?>">
  </span>
  <meta itemprop="description" content="<?php echo strip_tags(get_the_excerpt()); ?>">
 <?php	$image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( null ),'full' );
if ( $image_attributes ) : ?>
  <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
   
    <meta itemprop="url" content="<?php echo $image_attributes[0]; ?>">
    <meta itemprop="width" content="<?php echo $image_attributes[1]; ?>">
    <meta itemprop="height" content="<?php echo $image_attributes[2]; ?>">
  </div>
  <?php endif; ?>
  <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
    <div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
  
      <meta itemprop="url" content="https://dietmoihoalam.com/wp-content/uploads/2016/11/logo-amp.jpg">
      <meta itemprop="width" content="600">
      <meta itemprop="height" content="60">  
    </div>
    <meta itemprop="name" content="Diệt mối hòa lâm">
  </div>
  <meta itemprop="datePublished" content="<?php the_time('c'); ?>"/>
  <meta itemprop="dateModified" content="<?php the_modified_date('c'); ?>"/>
				<?php get_template_part('inc/page-image'); ?>
				
				<div class="entry themeform">
					<?php the_content(); ?>
					<div class="clear"></div>
				</div><!--/.entry-->
	<?php if( get_post_format() ) { get_template_part('inc/post-formats'); } ?>
					<?php if(function_exists('the_ratings')) { 
		the_ratings(); ?> <?php } ?> 
				</div>
			</article>
			
			<?php if ( ot_get_option('page-comments') == 'on' ) { comments_template('/comments.php',true); } ?>
			
		<?php endwhile; ?>
		
	</div><!--/.pad-->
	
</section><!--/.content-->

<?php get_sidebar(); ?>

<?php get_footer(); ?>