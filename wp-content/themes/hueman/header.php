<!DOCTYPE html>
<html class="no-js" lang="vi">
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="p:domain_verify" content="e5626f73d62549b88ba6ad7fa3741184"/>
	<link rel="shortcut icon" href="/wp-content/uploads/2016/11/Hoa-Lam-logo002-1.png" />
	<link rel="alternate" href="https://dietmoihoalam.com/" hreflang="vn" />
	<title><?php wp_title('');?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url');?>">

	<?php wp_head();
header('Cache-Control: max-age=31536000, public');

?>
</head>

<body <?php body_class();?>>
<div id="hotline" style="right:0;bottom:0;position:fixed;z-index:99999;">
<a href="tel:0862956121">
<img src="/wp-content/uploads/2014/05/hinh-nen-hoa-van-go-dep-full-hd-so-4-0.jpg" style="width:200px;">
</a>
</div>
<div id="wrapper">

	<header id="header">

		<?php if (has_nav_menu('topbar')): ?>
			<nav class="nav-container group" id="nav-topbar">
				<div class="nav-toggle"><i class="fa fa-bars"></i></div>
				<div class="nav-text"><!-- put your mobile menu text here --></div>
				<div class="nav-wrap container"><?php wp_nav_menu(array('theme_location' => 'topbar', 'menu_class' => 'nav container-inner group', 'container' => '', 'menu_id' => '', 'fallback_cb' => false));?></div>

				<div class="container">
					<div class="container-inner">
						<div class="toggle-search"><i class="fa fa-search"></i></div>
						<div class="search-expand">
							<div class="search-expand-inner">
								<?php get_search_form();?>
							</div>
						</div>
					</div><!--/.container-inner-->
				</div><!--/.container-->

			</nav><!--/#nav-topbar-->
		<?php endif;?>

		<div class="container group">
			<div class="container-inner">
				<div class="group pad">
					<?php echo alx_site_title(); ?>
					<br><br>
					<?php if (ot_get_option('site-description') != 'off'): ?><p class="site-description" style="color:#fff;"><?php bloginfo('description');?></p><?php endif;?>
				</div>

				<?php if (has_nav_menu('header')): ?>
					<nav class="nav-container group" id="nav-header">
						<div class="nav-toggle"><i class="fa fa-bars"></i></div>
						<div class="nav-text"><!-- put your mobile menu text here --></div>
						<div class="nav-wrap container"><?php wp_nav_menu(array('theme_location' => 'header', 'menu_class' => 'nav container-inner group', 'container' => '', 'menu_id' => '', 'fallback_cb' => false));?></div>
					</nav><!--/#nav-header-->
				<?php endif;?>
			</div><!--/.container-inner-->
		</div><!--/.container-->

	</header><!--/#header-->

	<div class="container" id="page">
		<div class="container-inner">
			<div class="main">
				<div class="main-inner group">
