				</div><!--/.main-inner-->
			</div><!--/.main-->
		</div><!--/.container-inner-->
	</div><!--/.container-->

	<footer id="footer"  >
		<?php // footer widgets
$total = 4;
if (ot_get_option('footer-widgets') != '') {

	$total = ot_get_option('footer-widgets');
	if ($total == 1) {
		$class = 'one-full';
	}

	if ($total == 2) {
		$class = 'one-half';
	}

	if ($total == 3) {
		$class = 'one-third';
	}

	if ($total == 4) {
		$class = 'one-fourth';
	}

}

if ((is_active_sidebar('footer-1') ||
	is_active_sidebar('footer-2') ||
	is_active_sidebar('footer-3') ||
	is_active_sidebar('footer-4')) && $total > 0) {?>
		<section class="container" id="footer-widgets">
			<div class="container-inner">

				<div class="pad group">
					<?php $i = 0;while ($i < $total) {$i++;?>
						<?php if (is_active_sidebar('footer-' . $i)) {?>

					<div class="footer-widget-<?php echo $i; ?> grid <?php echo $class; ?> <?php if ($i == $total) {echo 'last';}?>">
						<?php dynamic_sidebar('footer-' . $i);?>
					</div>

						<?php }?>
					<?php }?>
				</div><!--/.pad-->

			</div><!--/.container-inner-->
		</section><!--/.container-->
		<?php }?>

		<?php if (has_nav_menu('footer')): ?>
			<nav class="nav-container group" id="nav-footer">
				<div class="nav-toggle"><i class="fa fa-bars"></i></div>
				<div class="nav-text"><!-- put your mobile menu text here --></div>
				<div class="nav-wrap"><?php wp_nav_menu(array('theme_location' => 'footer', 'menu_class' => 'nav container group', 'container' => '', 'menu_id' => '', 'fallback_cb' => false));?></div>
			</nav><!--/#nav-footer-->
		<?php endif;?>

		<section class="container" id="footer-bottom" >
			<div class="container-inner">

				<a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>

				<div class="pad group" >

					<div class="grid one-half"  >

						<?php if (ot_get_option('footer-logo')): ?>
							<img id="footer-logo" src="<?php echo ot_get_option('footer-logo'); ?>" alt="<?php get_bloginfo('name');?>">
						<?php endif;?>

						<div id="copyright" style="text-align:center;color:#fff;">
							<?php if (ot_get_option('copyright')): ?>
								<p><?php echo ot_get_option('copyright'); ?></p>
							<?php else: ?>
								<p><?php bloginfo();?> &copy; <?php echo date('Y'); ?>. <?php _e('All Rights Reserved.', 'hueman');?></p>
							<?php endif;?>
						</div><!--/#copyright-->

						<?php if (ot_get_option('credit') != 'off'): ?>
						<div id="credit" style="text-align:center;color:#fff;" >
								<h3 style="color: #fff;" >Công Ty Diệt Mối - Côn Trùng Hoa Lâm</h3>
								<p>Trụ sở chính: 283 Dương Quảng Hàm, Phường 6, Quận Gò Vấp, Hồ Chí Minh</p>
								<p>ĐT: (08) 6295 6121 - DĐ: 0983 72 38 36</p>
								<p>GPKD số: 0311450756 do Sở Kế Hoạch & Đầu Tư TP HCM cấp.</p>

						</div><!--/#credit-->
						<?php endif;?>

					</div><!--/.pad-->

					<div class="grid one-half last">
						<?php alx_social_links();?>
					</div>

				</div>

			</div><!--/.container-inner-->
		</section><!--/.container-->

	</footer><!--/#footer-->

</div><!--/#wrapper-->
<?php wp_footer();?>
<style>
body.home .post-ratings {
display:none;
 }
</style>
    <script type="application/ld+json">
        {
          "@context":"http://schema.org",
          "@type":"HealthClub",
          "image": "https://dietmoihoalam.com/wp-content/uploads/2014/05/dietcontrung.jpg",
          "@id":"https://dietmoihoalam.com/",
          "name":"Công Ty Diệt Mối - Côn Trùng Hoa Lâm",
          "address":{
            "@type":"PostalAddress",
            "streetAddress":"283 Duong Quang Ham",
            "addressLocality":"Ho Chi Minh",
            "addressRegion":"HCM",
            "postalCode":"700000",
            "addressCountry":"VN"
          },
          "geo":{
            "@type":"GeoCoordinates",
            "latitude":10.837782,
            "longitude":106.682684
          },
          "telephone":"(08) 6295 6121"
        }
</script>
</body>
</html>