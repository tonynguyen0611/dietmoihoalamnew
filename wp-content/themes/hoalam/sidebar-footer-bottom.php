<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Sidebar
 * DESCRIPTIONS: Contains Primary Widget Area.
 *
 * AUTHOR:  SEO Design Framework
 */
?>
	
<section class="footer-bottom-widget-area widget_area clearfix">
	<?php if ( ! dynamic_sidebar( 'footer-bottom-widget-area' ) ) : ?>
		<div class="text-center">
			<?php GetUltimate()->getAdmin()->getPlaceholder('footer-bottom'); ?>
		</div>
    <?php endif; ?>
</section>

