<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		
		<?php 
		$count_tab = 0;
		foreach ( $tabs as $key => $tab ) : $count_tab++; ?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading_<?php echo esc_attr( $key ) ?>">
					<h4 class="panel-title">
						<a class="accordion-toggle<?php if ($count_tab == 1) {echo ' in';} else echo ' collapsed'; ?>" data-toggle="collapse" data-parent="#accordion-woo" href="#<?php echo esc_attr( $key ); ?>_tab">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo esc_attr( $key ) ?>" aria-expanded="true" aria-controls="collapse_<?php echo esc_attr( $key ); ?>">
							<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ) ?>
							<span class="icon-closed icon-angle-down <?php if ($count_tab == 1) {echo ' hide';}?>"></span> 
							<span class="icon-opened icon-angle-down<?php if ($count_tab != 1) {echo ' hide';}?>"></span>
						</a>
					</h4>
				</div>
				<div id="collapse_<?php echo esc_attr( $key ); ?>" class="panel-collapse collapse<?php if ($count_tab == 1) {echo ' in';} else echo ' collapsed'; ?>" role="tabpanel" aria-labelledby="heading_<?php echo esc_attr( $key ); ?>">
					<div class="panel-body"><?php call_user_func( $tab['callback'], $key, $tab ) ?></div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>
