<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Footer
 * DESCRIPTIONS: Template to display the footer of the theme.
 *
 * AUTHOR:  SEO Design Framework
 */ 
?>
<?php sdf_start_footer(); ?>
<!-- end Footer Blocks -->      
<?php
	wp_footer();
?>

</body>
</html>
