<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: functions.php
 * DESCRIPTIONS: Theme Functions
 * @package seodesign
 *
 * AUTHOR:  SEO Design Framework
 */ 
 
	define('SDF_THEME_NAME', 'SEO Design');
	define('SDF_TXT_DOMAIN', 'seodesign');
	define('SDF_THEME_SUBMITTED_FIELD', 'sdf_theme_submitted');

	define('SDF_META_KEY', '_sdf_meta_settings');
	define('SDF_THEME_ADS_OPTIONS', '_sdf_theme_ads_options');
	
	// if ssl check
	define('SDF_SSL', (is_ssl()) ? true : false);
	define('SDF_PROTOCOL_PREF', (SDF_SSL) ? 'https://' : 'http://');


if( ! defined('SDF_PARENT_URL' ) )
{	
	define('SDF_PARENT_URL', get_bloginfo('template_url')); // get Parent URL
	define('SDF_IMAGES', SDF_PARENT_URL . '/lib/img');
	define('SDF_CSS', SDF_PARENT_URL. '/lib/css');
	define('SDF_JS', SDF_PARENT_URL. '/lib/js');
	define('SDF_PRETTYPHOTO_PATH', SDF_PARENT_URL.'/lib/prettyPhoto');
	define('SDF_YTPLAYER_PATH', SDF_PARENT_URL.'/lib/ytplayer');
	define('SDF_BOOTSTRAP', SDF_PARENT_URL. '/lib/bootstrap');
	define('SDF_LESS', SDF_PARENT_URL. '/lib/bootstrap/less');

	define('SDF_WIDGETS_URL', SDF_PARENT_URL. '/lib/widgets');

	define('SDF_SILO_URL', SDF_PARENT_URL. '/lib/ultimate-silo');
}
if( ! defined('SDF_PARENT_PATH' ) )
{	
	define('SDF_PARENT_PATH',  get_template_directory()); // get Parent Theme Directory Path
	define('SDF_SILO', SDF_PARENT_PATH. '/lib/ultimate-silo');

	define('SDF_LANG', SDF_PARENT_PATH. '/languages');
	define('SDF_LIB', SDF_PARENT_PATH. '/lib');
	define('SDF_WOO', SDF_PARENT_PATH. '/woocommerce');
	define('SDF_ADMIN', SDF_PARENT_PATH .'/lib/admin');
	define('SDF_CORE', SDF_PARENT_PATH .'/lib/core');
	define('SDF_WIDGETS', SDF_PARENT_PATH .'/lib/widgets');
} 

require_once( SDF_LIB   . "/vars.php");
require_once( SDF_ADMIN . "/global.php");
require_once( SDF_ADMIN . "/page.php");

require_once( SDF_LIB  . "/hooks.php");
require_once( SDF_LIB  . "/sdf.options.php");
require_once( SDF_LIB  . "/sdf.scripts.php");
require_once( SDF_LIB  . "/h5bp-g5-htaccess/h5bp-g5-htaccess.php");

require_once( SDF_CORE . "/sdf.cpt.ads.php");
require_once( SDF_CORE . "/sdf.cpt.portfolio.php");
require_once( SDF_CORE . "/breadcrumbs.php");
require_once( SDF_CORE . "/content.php");
require_once( SDF_CORE . "/sdf.comments.php");
require_once( SDF_CORE . "/footer.php");
require_once( SDF_CORE . "/head.php");
require_once( SDF_CORE . "/header.php");
require_once( SDF_CORE . "/layout.php");
require_once( SDF_CORE . "/navigation.php");
require_once( SDF_CORE . "/soundcloud-shortcode.php");
require_once( SDF_CORE . "/shortcodes.raw.php");
require_once( SDF_CORE . "/shortcodes.php");
require_once( SDF_CORE . "/sdf.theme_updater.php");

if(is_admin()) {
require_once( SDF_CORE . "/tinymce.php");
require_once( SDF_CORE . "/sdf.functions.admin.php");
}

require_once( SDF_CORE . "/sdf.sidebars.php");
require_once( SDF_CORE . "/sdf.display_widgets.php");
require_once( SDF_CORE . "/sdf.sidebars_custom.php");
require_once( SDF_CORE . "/sdf.plugins.php");
require_once( SDF_CORE . "/sdf.functions.php");
require_once( SDF_CORE . "/config-woocommerce.php");
require_once( SDF_CORE . "/sdf.front.helper.php");
require_once( SDF_CORE . "/sdf.social_sharing_buttons.php");
	
require_once( SDF_LIB  . "/css/styles.php");
require_once( SDF_LIB  . "/less-php/Less.php");

require_once( SDF_SILO  . "/ultimate-silo.php");

require_once( SDF_WIDGETS . "/category_archives.php");
require_once( SDF_WIDGETS . "/category_subcategories.php");
require_once( SDF_WIDGETS . "/address.php");
require_once( SDF_WIDGETS . "/silo.php");
require_once( SDF_WIDGETS . "/audio.php");
require_once( SDF_WIDGETS . "/video_oembed.php");
require_once( SDF_WIDGETS . "/social_follow_buttons.php");
require_once( SDF_WIDGETS . "/social_share_buttons.php");
require_once( SDF_WIDGETS . "/google-maps/google-maps.php");
require_once( SDF_WIDGETS . "/latest-posts/latest-posts.php");
require_once( SDF_WIDGETS . "/portfolio/portfolio.php");
require_once( SDF_WIDGETS . "/photostream/photostream.php");
require_once( SDF_WIDGETS . "/contact-form/contact-form.php");
require_once( SDF_WIDGETS . "/image-widget/image-widget.php");
require_once( SDF_WIDGETS . "/collapse-menu/bootstrap-collapse-menu.php");

add_action('wp','setupWidth');
function setupWidth(){

	$page = sdfGo()->sdf_get_option('layout','toggle_page_layout');
	$pageWidth = "wide";
	if($page == "1") {
		if(isset(sdfGo()->_sdfPageArgs['layout']['page_width'])){
			$pageWidth = sdfGo()->_sdfPageArgs['layout']['page_width'];
		}
	} else {
		$pageWidth =  sdfGo()->_sdfGlobalArgs['layout']['page_width'];
	}

	if($pageWidth == "boxed") {
		defined('SDF_PAGE_WIDTH') || define('SDF_PAGE_WIDTH', 'boxed');
	}
	else {
		defined('SDF_PAGE_WIDTH') || define('SDF_PAGE_WIDTH', 'wide');
	}
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
