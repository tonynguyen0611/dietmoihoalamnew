<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Sidebar
 * DESCRIPTIONS: Contains Primary Widget Area.
 *
 * AUTHOR:  SEO Design Framework
 */ 
?>
	
<section class="left-widget-area widget_area clearfix">
	<?php 
	if(function_exists('dynamic_sidebar') && is_active_sidebar('left-widget-area')):
		$widget_sidebars_content = array();
		ob_start();
		dynamic_sidebar( 'left-widget-area' );
		array_push($widget_sidebars_content, ob_get_clean());
		
		if(strlen($widget_sidebars_content[0]) > 0 ) :
			echo $widget_sidebars_content[0];
		else :
			sdf_placeholder('left-widget-area');
		endif;
	else :
		sdf_placeholder('left-widget-area');
	endif; 
	?>
</section>