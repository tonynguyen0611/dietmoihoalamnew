<?php
/*
Theme Name: SEO Design Framework - Master Theme
Theme URI: http://www.seodesignframework.com
Description: The SEO Design Framework 
Author: SEO Design Framework
Author URI: http://www.seodesignframework.com
*/

die('Restricted access');

?>

* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note


== Changelog ==


Version 2.3.0.7 [Updated October 5th, 2016]
------------------------------------------------
# Added empty( $silo_cat_id ) check and replaced deprecated get_currentuserinfo
# Silo pagination fix on blog page
# sdf_add_shortcodes action and shortcodes processing fix
# Admin SDF settings nav css fix


Version 2.3.0.6 [Updated September 12th, 2016]
------------------------------------------------
# Structured Data fix
# Featured image title fix
# Page Builder fix for "Save Draft"
# Multisite checks for scripts/files
# Latest Posts Widget image size fix
# $img_width, $img_height added to latest posts list layout markup
# Remove shortcodes one by one in sdf_run_shortcode function to avoid third party shortcodes removal
# Blog post meta dateModified fix
# Fix for deleting custom widget areas
# Excluded single post ID from latest posts and portfolio widget to avoid infinite loop in get_excerpt_by_id function
# Removed sdf_latest_posts and sdf_portfolio from excerpt to avoid infinite loop
+ Secondary Navigation Position
+ Image Module/Shortcode Border Radius
+ Line Height option added to Text Block and Headline builder modules and shortcodes


Version 2.3.0.5 [Updated July 5th, 2016]
------------------------------------------------
# bootstrap columns clearing floats on product pages
# Header fix
# WooCommerce smallscreen fix
^ woo templates for 2.6.x update
^ Revolution Slider v5.2.6
+ Language: it_IT



Version 2.3.0.4 [Updated June 16th, 2016]
------------------------------------------------
# mute_background_video fix
# sdf_rewrite_rules_on_theme_switch
# HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
# CSS3 transform IE9 fix
# Image module fallback issue (lightbox_ad)
# sdf_get_attachment_id_by_url function updated
# Image module z-index fix
^ Font Awesome 4.6.3
^ TGM-Plugin-Activation updated to v2.6.1
^ Revolution Slider v5.2.5.4
^ WooCommerce Static css for v2.6
^ jQuery.mbYTPlayer version: 3.0.3



Version 2.3.0.3 [Updated May 11th, 2016]
------------------------------------------------
# Transparent header fix
# Added touchstart event for input#publish in builder scripts
# chrome issue on animations at 100% height
^ Retina.js v1.3.0
^ jQuery Waypoints - v2.0.5
+ Retina.js support for data-no-retina attribute in modules/shortcodes
+ Counter builder module/shortcode
+ Lightbox Ad option on image module
+ Hero full page height option
+ Page level top/bottom padding option
+ Added 'auto' option for ratio in page background video options and added video quality option



Version 2.3.0.2 [Updated April 26th, 2016]
------------------------------------------------
# WP Engine and bootstrap .modal collision fix
# Problem with update button and draft status
# jquery.mb.YTPlayer.js fix for iframe and grey overlay fix
# Image module w/o hover effect opacity fix
# Bootstrap.min.js moved from CDN to local and defer instead of async


Version 2.3.0.1 [Updated April 14th, 2016]
------------------------------------------------
# Image Module Background Color Fix
^ TGM-Plugin-Activation updated to v2.5.2 


Version 2.3 [Updated April 13th, 2016]
------------------------------------------------
+ WPML Support
+ Sticky Overlay Menu
+ Deferring and Asyncing Render-Blocking JavaScript
+ Image Module Hover Effects
+ Switch for SDF Builder turned on by default
+ Background Color Alpha for Hero and Column modules
+ Featured Image Top/Bottom Padding
+ WooCommerce 2.5.3 templates
+ Added [sdf_current_year] shortcode
+ Menu Text Decoration Options
+ Page level toggle for related posts
+ Builder separator modules line width and color options and text module max-width option
^ New core skin options
^ Display Widgets plugin updated to v2.0.5
^ Revolution Slider updated to v5.2.4.1
^ jquery.mb.YTPlayer updated to v3.0.0
# schema.org author name check if SEO Ultimate(+) plugin is active
# Breadcrumbs fix
# Soundcloud shortcode plugin and Jetpack collision fix
# FitText fix for headline module
# Small screens (sm,xs) sidebar width fix
# WP 4.5 jQuery 1.12.0 selectors with # broken fix


Version 2.2.5.6 [Updated November 13th, 2015]
------------------------------------------------
# Custom sidebars name/id fix
# Woocommerce Single Product Tabs Fix
# Woocommerce Double product title fix


Version 2.2.5.5 [Updated October 21st, 2015]
------------------------------------------------
+ Added share buttons to portfolio posts
+ Toggle title on homepage
^ Revolution Slider v5.0.9 Files
^ constructor method for WP_Widget since version 4.3.0
^ Woocommerce Templates updated to 2.4.0
# Contact Form Widget Checkboxes Fix
# Custom Sidebars Fix
# Structured Data Fix (image and datePublished in article header)
# Latest Posts List .clearfix and admin typo
# Safari and Chrome fix for max-width inside table-cell
# Query for Child of Parent Silo meta_key=_wpu_silo_dws
# Exclude silos in blog page
# "Preview Changes" fixed for portfolio custom post type (post_format taxonomy)
# Builder JS fixes
# Set permalink on featured images for non singular post lists
# Blog pagination fix
# parallax iPhones/iPads fix
# Centered menu dropdown alignment fix
# Page templates fixes
# Notice for HTML5 Boilerplate's and 5G Blacklist's .htaccess Addition


Version 2.2.5.4 [Updated June 30th, 2015]
------------------------------------------------
+ Dropdown menu and searchform positioning based on viewport size and screen resize
+ Global/Local Comments show/hide Toggle for Blog Posts, Portfolio Posts and Pages
# Local page options and title fix for 'product' post type
# Global/Local Toggle for Featured Image on Pages
# Builder styles fix for all post types
# Backgound color+image Typo Fix


Version 2.2.5.3 [Updated June 8th, 2015]
------------------------------------------------
+ Copy row to sdf clipboard functionality
^ jQuery MiniColors 2.1.11
# Image Link Fix
# Variable notice fix
# Page assigned assets check
# Builder Separator/Spacer min-height Fix
# Opening accordion based on URL just for Export/Import/Reset Theme Options
# itemprop removed from shortcodes markup due to SEO Ultimate (Plus)
# css version with woocommerce ssl paths
# woocommerce card icons ssl fix


Version 2.2.5.2 [Updated May 9th, 2015]
------------------------------------------------
+ Show/Hide "Design" Menu in Admin Toolbar
+ Silo Status Toggle
# Page Builder Modal Width Fix
# Removed bottom margin for header blocks on small screen
# Fix for Silo Breadcrumbs
# esc_url > esc_url_raw


Version 2.2.5.1 [Updated April 29th, 2015]
------------------------------------------------
+ Local admin Bootstrap js
# Fixing add_query_arg() and remove_query_arg() usage
# Builder modal popup losing focus fix
# .modal-open >>> .sdf-modal-open


Version 2.2.5 [Updated April 28th, 2015]
------------------------------------------------
+ SDF admin bar menu
+ H1-H6 Top/Bottom Margin options
+ Menu Dropdown Border Options
+ Entry Title Top/Bottom Margin Options
+ Post Icons (calendar, comment, author, category)
^ Bootstrap Modal for Builder Elements/Modules
^ Admin Scoped Bootstrap 3.3.4
^ Silo Draft post status
^ Fixed elements modal open state fix for right padding
^ Opening admin collapsible based on URL
^ Less.php version 1.7.0.3
^ WooCommerce 2.3.8 templates sync
# Scoped TBS grid classes and form elements Fix
# Portfolio Template Fix
# sdf_collapsibles Shortcode Fix
# WooCommerce css url Fix
# Extended FlexVerticalCenter.js to avoid small screen header wrap issues
# Builder Edit Element Fix
# Microdata Fixes
# Empty default font family and font family availability check
# Menu breakpoint css/less
# Page Title and heading classes Fix
# Admin tbs modal open state and right padding fix
# Removed utf8_encode on headline rotator
# Author archive title fix


Version 2.2.4.1 [Updated April 10th, 2015]
------------------------------------------------
# Page Level Layout Option Fix


Version 2.2.4 [Updated April 9th, 2015]
------------------------------------------------
+ SmoothScroll
^ Core Options
^ Image Sizes list, Image Size/Shape checks, Image Size/Shape builder notices
# Widget List Style Fix
# Portfolio Widget Fix
# Portfolio/Posts List Layout image shape Fix
# Menu Layout Fixes, Page Layout Fix
# Sticky Menu Fix
# Logo/Navbar Toggle Fix
# Mobile Submenu Fix


Version 2.2.3 [Updated April 6th, 2015]
------------------------------------------------
# Scroll To Top Icon Fix


Version 2.2.2 [Updated April 6th, 2015]
------------------------------------------------
^ Core Options
# Boxed Box Background Fix
# Grid Image Scale Fallback
# Disable tinymce validation
# Boxed Layout Box Content Left/Right Padding


Version 2.2.1 [Updated April 3rd, 2015]
------------------------------------------------
^ Core Options
# Widget List  CSS Fix
# WooCommerce CSS Fix
# Version Identifier for dynamic CSS


Version 2.2 [Updated April 2nd, 2015]
------------------------------------------------
+ Portfolio / Latest Posts List Layout Image Sizes
+ Portfolio / Latest Posts Offset option
+ Portfolio/Latest Posts Widgets Show Overlay Title option
+ Logo Heading Tag styling option, Headings font size passed to LESS variables
+ Posts/Portfolio Grid Image overlay control (color, buttons styling, title)
+ Grid Images CSS3 Transition/Transform/Opacity Controls
+ Grayscale Grid Images Option
+ Portfolio module/widget grid and list layout
^ Bootstrap v3.3.4
^ Animate CSS v3.2.5
^ jquery.nicescroll 3.5.6 
^ TouchSwipe jQuery Plugin version 1.6.8 (Removes fixed-to-top and sticky menu on pinchIn)
# Custom List margin/padding and default values set to non-zero
# SDF Less customization for TBS v3.3.4, cdn assets version change
# Fix input-group button sizing issue
# Image Module/Shortcode Title/Alt Text inputs
# Isotope Initialize Fix
# .container.inner replaced with .container-fluid
# Removed Isotope CSS
# .navbar-fixed-top/.navbar-fixed-bottom Fix
# Info Display Bottom Margin Fix
# Latest Posts/Portfolio Shortcodes Fallback
# Grid Filter pills alignment
# Entry Date Format Fix
# Menu Itel Link padding Fix
# #menu-item-search padding Fix
# Child Theme Fix - PrettyPhoto moved to folder
# PrettyPhoto movie+="?rel=1"; > movie+="?rel=0&vq=hd720";


Version 2.1.1 [Updated March 5th, 2015]
------------------------------------------------
+ Masonry Grid Layout w/ and w/o Spacing


Version 2.1 [Updated March 4th, 2015]
------------------------------------------------
+ Related Posts List (tax_query 'category' OR 'post_tag' taxonomies) on Single Post (toggle and count options)
+ Separated Featured Image global control for Single Posts and Category/Archive Pages
+ Global Page Title heading control ( h1, h2, none )
^ WooCommerce v2.3.5
^ jquery.mb.YTPlayer.js 2.8.1 beta
^ Isotope Update v2.1.1
^ Minified sdf.js 
^ YTPlayer Play for iframe
# Logo img-responsive alignment fix
# Raw Html Fix ( get_excerpt_by_id instead of wp_trim_all_excerpt in Latest Posts Widget )
# Latest Posts Excerpt Fix
# Generate css on upgrader_process_complete and theme activation
# Rotator separator check ( $rotator_separator != '' )
# CTA Box and Team Member Box button styles list fix
# Equalizer v1.2.5 Fixes
# Add id to 1/1 header widget area in a row w/o nav and add '-inner' suffix to header area column id
# Woo onsale badge css
# WooCommerce Page options and page ID fix
# Woo Price Filter Widget Styling
# Page Builder elementContent.match(/< img/g) null fix
# Portfolio and Latest Posts Grid Layout Fixes
# Removed date from related posts list
# get_excerpt_by_id $length fix
# :not(.latest-post-title) Fix for latest posts widget


Version 2.0.1 [Updated February 5th, 2015]
------------------------------------------------
# Typos
# function sdf_is_product_category() moved to sdf.functions.php


Version 2.0 [Updated February 4th, 2015]
------------------------------------------------
+ WooCommerce Integration
+ Maintenance Mode/Page Option
+ Portfolio Widget/Shortcode/Module title position option (above/below) image
+ Set Title Above or Below Featured Image on Blog and Archive pages
^ Woocommerce TBS Grid Classes, Change number of columns/products for shop page, Change number of related columns/products for single product
^ WooCommerce LESS
^ Breadcrumbs extended for woocommerce
^ Update Apache Server Configs to v2.11.0 
^ Expires headers and ETag removal
^ jquery.mb.YTPlayer.js v2.7.9 and 2 fixes (iframe child focus/blur) and removed empty row check
# Equalize widget heights fix for checking if area is active before adding to the list
# WooCommerce and Archives Titles Change
# Removed Double Single Post Title under featured image
# Latest Posts Widget List Style Fix
# Modules overflow fix

Version 1.9.7 [Updated January 20th, 2015]
------------------------------------------------
^ Isotope PACKAGED v2.1.0
^ imagesLoaded PACKAGED v3.1.8
# Builder images overflow fix
# Dragging Builder Modules Fix

Version 1.9.6 [Updated January 19th, 2015]
------------------------------------------------
+ Fullwidth Hero Option 
+ Modal Positioning Options
^ Modal content/js markup via transients
# Media component media-left/right Fixes
# rawhtml priority after sdf.functions.php excerpt changes ('the_content' filter)
# Modal Scrolling Issues


Version 1.9.5 [Updated January 12th, 2015]
------------------------------------------------
^ jquery.mb.YTPlayer.js v2.7.8
^ YTPlayer Play/Pause Control
^ Bootstrap 3.3.1 Update
# Background Video + Image Fix
# SDF Fixes for Bootstrap 3.3.1
# Bootstrap 3.3.1 Media Component Teaser Fallback and Firefox Fix
# Post Excerpt and Latest Posts Widget Excerpt Fixes


Version 1.9.4 [Updated January 6th, 2015]
------------------------------------------------
+ Fixed to Bottom Menu added
+ Default $content_width and css "size-"classes
+ Blog Pagination
+ Clone row / hero
+ Timed Page Lightbox (added animation and size)
+ Bottom Lightbox (animation, size, position)
+ Top/Bottom Margins applied for modules
+ Social Share Buttons Position Global/Local Option ( Above Content/Below Content )
^ Revolution Slider v4.6.5
^ multiple Timed Modals, Add/Remove
^ Ads Lightboxes Tab on Page Level
^ Lightbox Shortcode added size and animation parameter
^ Modal Animations
^ Comment Form labels for translation
# Post Meta Fix
# Sticky Menu on Boxed Layout Fix
# Default animation duration Fix, mandatory loading for waypoints.js
# Fix for delete row button after template load
# Lightbox/Portfolio Posts Double Description Fix
# Bottom Lightbox Direction Fix
# Headline Module markup id fix
# p tag for text / title inputs on modules (Headline, Teaser Box, Team Member Box, Portfolio, Latest Posts, Separator With Text)
# Social Share urlencode Fix
# CTA Box button margins/alignment fix
# Team Member Box and Teaser Box inner elements margins
# Post Excerpt Fix
# Top/Bottom Margin Fixes (Blank value, Sticky Menu Top/Bottom Margin Reset)
# Contact Form Widget/Module Update/Fix ( Enable Captcha option in Builder Module, Captcha Answer Input )
# List Line Height Fix
# Builder Column Height Fix
# Fix for Builder columns after adding elements
# "Theme Image Sizes" form fix
# Filter Gradient Color IE9 Fix
# [sdf_short...] doubles Fixed
# Tinymce issue fix
# Share Icons Description/Excerpt Fix
# Archive Excerpt Fix


Version 1.9.3 [Updated November 18th, 2014]
------------------------------------------------
^ Navigation area additional widgets vertical centering
^ Animate.css v3.2.0 + custom
^ Isotope PACKAGED v2.0.1
^ jquery.mb.YTPlayer.js update v2.7.5
# LESS @icon-font-path moved to CDN https
# Headline title fallback; Headline, text block and icon color pickers instances
# Check links for missing http(s) prefix or replace http:// with https:// on SSL
# Latest Posts Module help texts
# Separator With Text Fix
# Teaser Margins and Icon Fixed Width class .fa-fw
# "Plain Link" style option for Share/Follow icons
# Admin Page Title UI and Post Meta Fix (removed newlines and sanitize_text_field)
# xxs hero inline style removal
# SpacerHeights Extended
# Portfolio/Latest Posts Grid sm/xs Fix
# Modal Fixes
# Image link clearfix


Version 1.9.2 [Updated November 3rd, 2014]
------------------------------------------------
+ Soundcloud Shortcode
+ jQuery FitText 1.2 added
+ Added email shortcode with antispambot
^ Headline Fit Text (params Compressor, Min Font Size, Max Font Size)
^ Navigation Global/Local Toggle
# Image Size Defaults
# Added antispambot to address widget email
$ Contact Form widget Localization


Version 1.9.1 [Updated October 28th, 2014]
------------------------------------------------
+ Social Share Icons Widget
+ Collapse Menu Widget
+ Container Sizes LESS Variables
+ Theme Image Sizes Option
+ TBS LESS Variables for: Font Size Large/Small, Sans Serif, Serif and Base Font Family
+ Logo Alignment Option
+ Blog Author Box and Post Meta Toggles
+ Headline Module/Shortcode Letter Spacing Option
+ Added Button Styles to Follow Buttons Style Option
^ Shortcode link check for http(s)// and append if missing
^ Button/CTA Button Icon Position Option in Module/Shortcode
^ Blockquote Quote Icon Options
# wp native image alignment classes
# Page Level Header Fix 
# Hero Background Contain Fix 
$ Localization Files Updated


Version 1.9 [Updated October 15th, 2014]
------------------------------------------------
+ Video/oEmbed Builder Module/Shortcode/Widget
+ Separators/Separators w/ Text Styles/Colors
+ "Google Fonts and Typography" Tabs added
+ Icon and Text List Builder Modules and Shortcodes
+ HTML5 Boilerplate's and 5G Blacklist's .htaccess Addition in Global Options
+ Pre Code Builder Module
+ Blockquote Styling
+ Column Shortcode/Module Background Styling Options
^ Separated HTML5 Boilerplate's and 5G Blacklist's .htaccess addition Option Toggles
^ Separated Default and Custom List Styling
^ Image Widget
^ mb.YTPlayer 1.9.2
^ Font Awesome 4.2
^ Text List Module's option for List Type
^ Moved prettyPhoto.css to styles.php
^ Preset static css minified and custom .css is appended
^ Social Icons Brand Colors Info
^ Additional Header Block Classes and XSmall screen logo/navbar-toggle positions
^ Shortcode Generator Markup/jQuery Revised
^ Widget Custom Typography Toggle
^ Headline Rotator Delay
^ Original Image Size Option
^ Button/CTA Button Icon Option
^ Added Black, Grey and White Button Styles
^ 15 Follow Icons Added (Yelp...)
^ Static CSS Classes for Border Radius 1-15px, 20px, 25px, 30px
# Module Width Fallback Fix
# Pass Default LESS colors to "blank" buttons
# Case of Blank Child Theme Options Fix (use Core Styles)
# Follow Icons' Styles Save Fix
# Contact Form Layout Fix
# RAW Shortcode Fix
# Excluded Sticky Menu on Mobile
# Mobile Menu Fixes (Dropdown Colors, Page Paddings)
# List Module Fix (Custom List Style Option, Help Texts)
# Attachment Page Fix
# Wide/Boxed Layout Backgrounds Fix
# .htaccess Content removal Fix
# Button Borders Fix and Added @button-border-width
# Hero Responsive Fix
# Page Builder WPView fix
# Address Widget's Icon Size Fix
# background-size: contain Fix
# Admin Options "Saved..." Alert Box
# Teaser Markup and Icon Position Fixes
# Latest Posts/Portfolio Widget/Module Fixes
# Local Share Icons Toggle Fix
- VideoJS library


Version 1.8 [Updated September 22nd, 2014]
------------------------------------------------
+ Default Typography List Styling
+ Default Widget Area and Custom (Per Area) Widgets List Styling
+ Page Builder Modules Width Control
^ Text Decoration property option for Default Links, Page Title and Widgets' Lists 
^ Text Align property option for Widgets' Lists 
^ jQuery.mb.YTPlayer updated to v2.7.2
^ Bootstrap Updated to 3.2.0
^ VideoJS updated to v4.8.2
^ VJS Media Youtube 1.2.5+
^ Ajax call for page level css replaced with inline css
# Page Structured Data Fix, Entry title for frontpage and hidden title pages/posts
# Video Widget Fix (initial pre resize set to 0x0px)
# Sticky Menu Fix
$ Localization Files Updated
! Autoupdate Enabled


Version 1.7 [Updated September 12th, 2014]
------------------------------------------------
+ Tagline Styling
+ SDF Custom LESS
- Button Text Size removed due to LESS
^ Builder Modules Entrance Animations Revised
^ Headline Module Word Rotator and Entrance Animation
^ VideoJS update v4.7.3
^ Video Widget Update
^ Video Shortcode (and Builder Module Update)
^ Color Picker and Input Check Methods Changed
^ Headings Letter Spacing
^ Text Alignment option added to Text Block Module
# Row, hero, col markup fix (style="")


Version 1.6 [Updated September 6th, 2014]
------------------------------------------------
+ RGBa color pickers/presets
+ Less PHP
+ Sticky Menu Waypoints and Opacity Option
+ Waypoints Bidirectional animations/offsets
^ Revolution Slider v4.6.0 Integration
^ Buttons, Navigation and Nav Dropdown letter spacing
^ Buttins and Menu/Dropdown Transition Properties
^ jQuery.mb.YTPlayer updated to v2.7.1
^ Buttons/CTA Buttons Layout options ( Align Left, Align Right, Centered and Stacked, Full Width and Stacked )
# WP 4.0 Builder Switch Fix
# Preset Colors Fix (Case where Primary Color value is some other Preset Color; Issue showed on Primary Color change to some other preset and CustomLESS now receives hex value; Also jQuery localized data check for this case on Primary value)
# Post Structured Data Fix


Version 1.5 [Updated August 22nd, 2014]
------------------------------------------------
+ displayColorPickerField method for Styles page
+ AutoHosted Theme Updater Integration
+ Follow Buttons Default and Custom Styling
+ Latest Posts Widget
^ TBS Comments form
^ VideoJS update
^ YouTube Media Controller update
^ Video widget added option for youtube video quality (1080p, 720p, 480p, 360p, 240p, 144p)
^ Font Awesome 4.1.0
^ Margins option for: text block module, image module, button module, cta button module
# Initial YTPlayer volume Fix
# @font-size-base pulled from "Default Font Size" option


Version 1.4 [Updated August 10th, 2014]
------------------------------------------------
+ Blockquote Builder Module
+ Featured Image Animation Toggle Global/Local
^ SDF Builder Switch Button Style
^ YT BG Player Play/Volume Controls separation
^ Hero Height, Padding, Box-Shadow and Border Option
^ Height, Top Padding and Bottom Padding Options for Widget Areas
# Contact Form Widget Fix


Version 1.3 [Updated July 25th, 2014]
------------------------------------------------
+ Portfolio Builder Module
+ Latest Posts Module
+ Headline Builder Module and Shortcode
^ Bootstrap Update to 3.1.1
^ Hero background cover(fullsize)
^ Navigation background cover(fullsize)
^ Display Widgets 2.0.3
# Scroll To Top Position and Labels Fix
# Media Upload Button Fix
# Changed deprecated jQuery live function
# Use RAW Shortcode Generator Content


Version 1.2 [Updated July 12th, 2014]
------------------------------------------------
+ Contact Page Builder Module
+ Admin Toolbar Toggle Global Option
+ Custom Read More Text Global Option
+ Featured Image Toggle Global/Local
^ YTPlayer native player bar option
^ YTPlayer removed window check (no sidebar issue)
^ YTPlayer: Option for YouTube logo and the link to the original video URL
^ Scroll To Top Button Styling
^ Hero BG Video Mute option
# Structured data author anchor markup fix
# Sticky Nav submenu z-index fix
# ImageModule Margin Fix
# TGM Plugin Activation Fix and Slider Version Check Update
# Display Widgets Plugin filter fix
# Import/Reset/Switch CSS Creation Fix


Version 1.1 [Updated June 7th, 2014]
------------------------------------------------
+ Category Archives Widget
+ Category Subcategories Widget
+ Photostream Widget
^ Revolution Slider 4.5.01
^ Nice Scroll 3.5.4
^ Page Level  Options Patches/Fallback
^ mb.YTPlayer update to 2.6.7
# Hero Fullwidth Boxed and Margin Fix
# Import/Reset Options Fix 
# Ajax Call CSS Fix
# NiceScroll fix(html overflow) 
# Entry Type Icon Fix
# YT BG Posts Fix ( is_single check added )
# Adsense in Shortcode Generator Fix


Version 1.0 [Updated May 15th, 2014]
------------------------------------------------
+ Scroll To Top Global Option Toggle (Script Loading)
+ Nice Scroll Global Option
+ FitVids Script
+ YT BG Video Global Option
+ Show Full Posts on Blog Page Global Option
+ Background Video Page Level Options (mute, volume, opacity, controls)
+ Autosave Global Option Toggle (Script Loading)
+ Hero Background Video Option
+ Hero Parallax Background Option
+ Bootstrap Buttons Styling ( padding, borders )
+ H4-H6 Headings Styling
+ Social Media Follow Buttons Styling
+ Post Format Buttons Styling
# Centering Teaser Icon Fix
# Page Level Preset Option Empty Value Fix
# Timed Lightbox Fix
# Image Logo w/o h1
$ Localization Files Updated


Version 1.0-beta.5 [Updated April 1st, 2014]
------------------------------------------------
# SDF Admin pages got a huge (multiple time) increase in speed except "Styles" page/tab as it's a large form with hundreds of fields yet load time was improved.
# Boxed Layout Increased left/right padding from 15px to 30px
# Adjusted size for Login logo
+ Style Presets "Clone" feature added to expedite development time
+ Search Icon/Form (available in navigation options) can be appended to menu
+ Excerpt Length option and Auto Excerpt Creation from Shortcoded(Page Builder) Content
+ Stick Footer to the bottom in case body does not have full viewport height
+ Page Level Style Preset option available


Version 1.0-beta.4 [Updated March 19th, 2014]
------------------------------------------------
* No direct access
^ functions.php
^ languages/en_US.mo
^ languages/en_US.po
^ lib/admin/global.php
^ lib/admin/page.php
^ lib/css/sdf.css
^ lib/css/styles.php
^ lib/hooks.php
^ lib/ultimate.php
^ lib/vars.php
# Collapsibles Shortcodes Fix
# sdf_get_image_id_by_url fix, 
# Page Header/Footer Additional HTML Fix
# TBS Button Hover Text Color Fix
# Undefined custom post types -> page template
# Translations Updated


Version 1.0-beta.3 [Updated March 15th, 2014]
------------------------------------------------
^ searchform.php
^ lib/core/content.php
^ lib/core/footer.php
^ lib/core/head.php
^ lib/core/sdf.front.helper.php
^ lib/core/sdf.functions.php
^ lib/core/shortcodes.php
# footer widget areas options moved to footer collapsible
# Search Form Action url Fix
# Silo Styles Fix 
# hero, row shortcodes fix
# teaser shortcode icon/title text-center case
# article, .row bottom margins Fix


Version 1.0-beta.2 [Updated March 12th, 2014]
------------------------------------------------
^ lib/widgets/image.php
^ lib/widgets/social_icons.php
# Check $this->_wpuGlobalArgs
# Header/Footer script shortcodes, Navigation area widget padding
# Default color for headings, widget titles, page title
# Social Icons Widget - Icon Size option


Version 1.0-beta.1 [Updated March 10th, 2014]
------------------------------------------------
+ Initial Pre Release
