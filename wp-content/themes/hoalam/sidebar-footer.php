<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Sidebar
 * DESCRIPTIONS: Contains Primary Widget Area.
 *
 * AUTHOR:  SEO Design Framework
 */
?>
	
<section class="footer-widget-area widget_area clearfix">
	<?php if ( ! dynamic_sidebar( 'footer-widget-area' ) ) : ?>
		<div class="text-center">
			<?php GetUltimate()->getAdmin()->getPlaceholder('footer'); ?>
		</div>
    <?php endif; ?>
</section>

