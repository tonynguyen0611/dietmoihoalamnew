<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Main Template File
 *
 * DESCRIPTIONS: This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 *
 * AUTHOR:  SEO Design Framework
 */ 
get_header();

sdf_start_page();

get_footer();