<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: WordPress Post Loop.
 * DESCRIPTIONS: WordPress Loop for displaying posts and post content.
 *
 * AUTHOR:  SEO Design Framework
 */ 

 	global $wp_query;
	
$sdf_wide_template = ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) ? true : false;

/* Exclude silos */
	if ( is_home()) {
		
		$args = array(
				'meta_query' => array(
													array(
														 'key' => '_wpu_silo_dws',
														 'compare' => 'NOT EXISTS'
													)
												),
				'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
		);
		
		$query = new WP_Query($args);
		
	}
	else {
		$query = $wp_query;
	}

/* If there are no posts to display e.g. an empty archive page */
	if ( ! $query->have_posts() ) : ?>

	<?php if ($sdf_wide_template) : ?>
		<div class="container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="container-fluid">
	<?php endif; ?>
	
			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title" itemprop="headline"><?php _e( 'Not Found', SDF_TXT_DOMAIN ); ?></h1>
				<div class="entry-content" itemprop="text">
					<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', SDF_TXT_DOMAIN ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->
			
	<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>
	
<?php endif; ?>

<?php while ($query->have_posts()) : $query->the_post();?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> itemscope="itemscope" itemtype="https://schema.org/BlogPosting" itemprop="blogPost">
		<?php if ($sdf_wide_template) : ?>
			<div class="container">
			<div class="row">	
			<div class="col-md-12">
		<?php endif;
		
			$post_id = get_the_ID();
			$anchor = "<a href=\"";
			$anchor .= get_permalink($post_id);
			$text_title = get_the_title($post_id);
			$anchor .= "\" title=\"". sprintf( esc_attr__( 'Permalink to %s', SDF_TXT_DOMAIN ), the_title_attribute( 'echo=0' ) )."\" rel=\"bookmark\">".$text_title."</a>";

			if ( !is_single() && sdfGo()->sdf_get_option('blog', 'post_title_position') ) : ?>
				<header class="entry-title">
					<h2 class="entry-title" itemprop="headline"><?php echo $anchor; ?></h2>
				</header>
			<?php else : ?>
				<header class="entry-title">
					<h2 class="hidden entry-title" itemprop="headline"><?php echo $anchor; ?></h2>
				</header>
			<?php endif; ?>
			
			<?php 
			$post_has_featured = false;
			$pin_view = array();
			
			if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail($post_id)) && (( sdfGo()->sdf_get_option('blog', 'toggle_single_featured_image') && is_singular() ) || ( sdfGo()->sdf_get_global('blog', 'toggle_featured_image') && !is_singular() )) ) : ?>
				
				<div class="entry-thumbnail">
				<?php 
				$post_has_featured = true;
				// featured image animation on/off
				$data_animation = '';
				$data_duration = '';
				$animation_class = '';
				if(sdfGo()->sdf_get_option('animations', 'feat_img_animate')) {
				$data_animation = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != 'No' && sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != '') ? ' data-animation="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_type').'"' : '';
				$data_duration = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration') != '' && $data_animation != '') ? ' data-duration="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration').'"' : '';
				$animation_class = ($data_animation != '') ? ' viewport_animate animate_now' : '';
				}
				
				$featured_view ='';
				$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'sdf-image-md-12', false, '' );
				$pin_view_src = $pin_view[0];
				$featured_view = '<img itemprop="image" class="img-responsive'.$animation_class.'"'.$data_animation.$data_duration.' alt="'.get_the_title().'" src="'.$pin_view_src.'">';
				if ( !is_singular() ) {
					echo '<div class="featured-image"><a href="'.get_permalink($post_id).'" title="'. sprintf( esc_attr__( 'Permalink to %s', SDF_TXT_DOMAIN ), the_title_attribute( 'echo=0' ) ).'" rel="bookmark">'.$featured_view.'</a></div>';
				}
				else {
					echo '<div class="featured-image"><a href="'.$pin_view_src.'" title="'.get_the_title($post_id).'" data-rel="prettyPhoto" class="pretty-view">'.$featured_view.'</a></div>';
				}
				?>
				</div><!-- .entry-thumbnail -->
				
			<?php endif; ?>
			
				<?php 
				// check post type
				$post_format_icon = '';
				$post_format = get_post_format();
				$post_format_class = strtolower($post_format);
				switch ($post_format) {
					case 'video':
						$post_format_icon = 'video-camera';
						break;
					case 'audio':
						$post_format_icon = 'music';
						break;
					case 'gallery':
						$post_format_icon = 'picture-o';
						break;
					case 'image':
						$post_format_icon = 'picture-o';
						break;
					case 'quote':
						$post_format_icon = 'quote-left';
						break;
					case 'link':
						$post_format_icon = 'link';
						break;
					case NULL:
						$post_format_icon = 'pencil';
						$post_format_class = 'standard';
						break;						
				}

				$btn_toggle = sdfGo()->sdf_get_global('blog', 'toggle_post_format_button');
				$btn_text = sdfGo()->sdf_get_global('typography','theme_'.$post_format_class.'_post_format_button_custom_text');
				$icon_size = sdfGo()->sdf_get_global('typography','theme_post_format_button_icon_size');
				$toggle_icon = sdfGo()->sdf_get_global('typography','theme_post_format_button_icon_toggle');
				$btn_icon = ($toggle_icon) ? '<i class="fa fa-'.$post_format_icon.' '.$icon_size.'"></i> ' : '';
				// post meta
				$post_time = sdfGo()->sdf_get_global('blog', 'toggle_post_meta_time');
				?>
			
				<header class="entry-header">
				<?php if ( !is_single() && !sdfGo()->sdf_get_option('blog', 'post_title_position') ) :
					// title below featured image
					$anchor = "<a href=\"";
					$anchor .= get_permalink($post_id);
					$text_title = get_the_title($post_id);
					$anchor .= "\" title=\"". sprintf( esc_attr__( 'Permalink to %s', SDF_TXT_DOMAIN ), the_title_attribute( 'echo=0' ) )."\" rel=\"bookmark\">".$text_title."</a>";
					?>
					<h2 class="entry-title" itemprop="headline"><?php echo $anchor; ?></h2>
				<?php endif; ?>
			
				<div class="entry-utility">
			
				<?php if( $btn_toggle ) : ?>
				<div class="entry-type <?php echo $post_format_class; ?>-format"><?php echo ($btn_icon.$btn_text); ?></div>
				<?php endif; ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_time')) { 
					$entry_date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark" class="post-time">' .
            '<time itemprop="datePublished" datetime="%3$s">%4$s</time>' .
            '</a>',
						esc_url( get_permalink($post_id) ),
						esc_attr( get_the_title($post_id) ),
						esc_attr( get_the_date( 'c' ) ),
						esc_html( get_the_date() )
					);
				?>
				<span class="entry-time"><i class="fa fa-calendar"></i> <?php echo $entry_date; ?></span>
				<?php } ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_categories')) : ?>
				<?php if ( count( get_the_category() ) ) : ?>
				<span class="cat-links"><?php printf( __( '<span class="%1$s"><i class="fa fa-sitemap"></i> in</span> %2$s', SDF_TXT_DOMAIN ), 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
				</span>
				<?php endif; ?>
				<?php endif; ?>
				
				<?php
				$author_id = get_query_var( 'author' );
				if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_author')) :
				
				if(empty($author_id)) $author_id = get_the_author_meta('ID');
				
				$by = __("by",SDF_TXT_DOMAIN);
				// check if SEO Ultimate(+) plugin is active
				if ( !class_exists( 'SEO_Ultimate' ) && !class_exists( 'SEO_Ultimate_Plus' ) ) {
					$author_link = get_author_posts_url($author_id);
					$author_name = '<span class="vcard author"><span class="fn"><a href="'.$author_link.'" rel="author">'.get_the_author_meta("display_name", $author_id).'</a></span></span>';
					echo('<span class="author-name" itemprop="name"><i class="fa fa-user"></i> '.$by.' '.$author_name.'</span>'); 
				}
				else {
					$author_name = '<span class="author"><span>'.get_the_author_meta("display_name", $author_id).'</span></span>';
					echo('<span class="author-name"><i class="fa fa-user"></i> '.$by.' '.$author_name.'</span>');
				}
				endif; ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_comments')) : ?>
					<?php if( !is_singular()) : ?>
						<?php if ( get_comments_number() != "0" || comments_open() ) : ?>
							<span class="comments-link"><i class="fa fa-comment-o"></i> <?php comments_popup_link( __( 'Leave a comment', SDF_TXT_DOMAIN ), __( '1 Comment', SDF_TXT_DOMAIN ), __( '% Comments', SDF_TXT_DOMAIN ) ); ?></span>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif;

				edit_post_link(__('Edit this post', SDF_TXT_DOMAIN), '<p class="edit">','</p>'); ?>
			
				</div><!-- .entry-utility -->
				</header>
				
				<?php 
				//share links on single post
				if (is_singular() && sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'above'){
					sdf_social_sharing_buttons();
				}
				
			if ($sdf_wide_template) : ?>
				</div>
				</div>
				</div>
			<?php endif; ?>
			
				<?php if ( is_search() ) : // Only display excerpts for search. ?>
					
					<div class="entry-summary">
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php endif; ?>
					
						<div itemprop="description"><?php the_excerpt(); ?></div>
								
						<?php if ($sdf_wide_template) : ?>
							</div>
						<?php endif; ?>
				
					</div><!-- .entry-summary -->
					
				<?php else : ?>
				
					<?php if( is_archive() || !is_singular()) : ?>
						
						<div class="entry-summary">
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php endif; ?>
						
						<?php if (sdfGo()->sdf_get_global('blog', 'toggle_full_posts')) : ?>	
						<div itemprop="articleBody"><?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?></div>
						<?php else: ?>
						<div itemprop="description"><?php the_excerpt(); ?></div>
						<?php endif; ?>
								
						<?php if ($sdf_wide_template) : ?>
							</div>
						<?php endif; ?>
						
						</div><!-- .entry-summary -->
						
					<?php else: ?>
					
						<div class="entry-content" itemprop="text">
						
							<?php 
							$post_content = get_the_content();
							if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
								<div class="container">
							<?php endif; ?>

								<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?>
								
							<?php if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
								</div>
							<?php endif; ?>
						
						</div><!-- .entry-content -->
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
							<div class="container-fluid">
						<?php endif; ?>
								<div class="row">	
									<div class="col-md-12">
						
						<?php 
						// tags
						if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_tags')) :
						
							$tags_list = get_the_tag_list('<div class="entry-tags" itemprop="keywords">',', ','</div>');
							if ( $tags_list ):
							?>
							<div class="tag-links">
								<h5 class="media-heading"><?php echo __( 'Tags', SDF_TXT_DOMAIN ); ?></h5>
								<?php echo $tags_list; ?>
							</div>
							<?php endif;
							
						endif; ?>
						
						<?php if (sdfGo()->sdf_get_option('blog', 'toggle_related_posts')){
							
							//get the categories 
							$cat_array = array();							
							if($cats = get_the_category($post->ID)) {
								foreach($cats as $key1 => $cat) {
									$cat_array[$key1] = $cat->slug;
								}
							}
							
							//get the tags   
							$tag_array = array();
							if($tags = get_the_tags($post->ID)) {
								foreach($tags as $key2 => $tag) {
									$tag_array[$key2] = $tag->slug;
								}
							}
							
							$args = array(
									'showposts'     	=> sdfGo()->sdf_get_option('blog', 'related_posts_count'),
									'meta_query' => array(
																		array(
																			'key' => '_wpu_silo_dws',
																			'compare' => 'NOT EXISTS'
																		)
									),
									'post__not_in'    => array($post->ID),
									'orderby'         => 'post_date',
									'order'           => 'DESC',
									'post_type'       => $post->post_type,
									'post_status'     => 'publish',
									'tax_query' => array(
																	'relation' => 'OR',
																	array(
																		'taxonomy' => 'category',
																		'field' => 'slug',
																		'terms' => $cat_array,
																		'include_children' => false 
																	),
																	array(
																		'taxonomy' => 'post_tag',
																		'field' => 'slug',
																		'terms' => $tag_array
																	)
									)
							);

							$my_query = new WP_Query($args);
							if( $my_query->have_posts() ) {
							
							?>
							<div class="related-posts">
							<h5 class="related-post-title"><?php _e('Related Posts', SDF_TXT_DOMAIN); ?></h5>
							<ul class="list-custom">
							<?php 
							
								while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
									<?php
								endwhile;
								wp_reset_query();
								
							?>
							</ul>
							</div>
							<?php 	
							}
						} ?>
						
						<?php 
						//share links on single post
						if (sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'below'){
							sdf_social_sharing_buttons();
						}
						
						if (sdfGo()->sdf_get_option('blog', 'toggle_post_author_box')){
							$gravatar     = get_avatar( get_the_author_meta('email', $author_id), '75' );
							
							// check if SEO Ultimate(+) plugin is active
							$itemprop_name = ( !class_exists( 'SEO_Ultimate' ) && !class_exists( 'SEO_Ultimate_Plus' ) ) ? ' itemprop="name"' : '';
							
							$heading      = __("About",SDF_TXT_DOMAIN) ." ".'<span class="author-name"'.$itemprop_name.'>'.$author_name.'</span>';
							$description  = get_the_author_meta('description', $author_id);

							echo '<div class="author-box media">';
							if(empty($description))
							{
								$description  = __("This author hasn't yet written their biography.",SDF_TXT_DOMAIN);
								$description .= '</br>'.sprintf( __( 'Still we are proud %s contributed %s great entries.', SDF_TXT_DOMAIN ), '<span class="author-name"'.$itemprop_name.'>'.$author_name.'</span>', count_user_posts( $author_id ) );

								if(current_user_can('edit_users') || get_current_user_id() == $author_id)
								{
									$description .= "</br><a href='".admin_url( 'profile.php?user_id=' . $author_id )."'>".__( 'Edit the profile description here.', SDF_TXT_DOMAIN )."</a>";
								}
							}


							echo '<div class="media-left"><a class="media-object">'.$gravatar.'</a></div>';
							echo '<div class="author_description media-body">
								<h4 class="author-title media-heading">'.$heading.'</h4>
								<div class="author_description_text">'.wpautop($description).'</div></div>';
							echo '</div>';
						} ?>
			
									</div>
								</div>
						<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
							</div>
						<?php endif; ?>	
					
					<?php endif; ?>
				<?php endif; ?>
			<?php 
			
			if (count($pin_view) == 0){
				$pin_view = array('', '', '');
			}
			
			?>
			<span class="hidden">
			<span class="sdf-structured-data" itemprop="image" itemtype="https://schema.org/ImageObject" itemscope="itemscope">
			<span itemprop="url"><?php echo $pin_view[0]; ?></span>
			<span itemprop="height"><?php echo $pin_view[2]; ?></span>
			<span itemprop="width"><?php echo $pin_view[1]; ?></span>
			</span>
			<span class="sdf-structured-data" itemscope="itemscope" itemtype="https://schema.org/Organization" itemprop="publisher">
			<span itemprop="name"><?php echo $author_name; ?></span>
			<span itemtype="http://schema.org/ImageObject" itemscope="" itemprop="logo">
			<span itemprop="url"></span>
			</span>
			</span>
			<span class="sdf-structured-data" itemtype="https://schema.org/Person" itemscope="itemscope" itemprop="author">
			<span itemprop="name"><?php echo $author_name; ?></span>
			</span>
			<?php 
			$entry_date = sprintf(
            '<span class="sdf-structured-data" itemtype="https://schema.org/datePublished" itemprop="datePublished" datetime="%1$s">%2$s</span>' .
            '<span class="sdf-structured-data" itemtype="https://schema.org/dateModified" itemprop="dateModified" datetime="%3$s">%4$s</span>',
						esc_attr( get_the_date( 'c' ) ),
						esc_html( get_the_date() ),
						esc_html( get_the_modified_date( 'c' ) ),
						esc_html( get_the_modified_date() )
					);
			 ?>
			<span class="sdf-structured-data" itemtype="https://schema.org/mainEntityOfPage" itemprop="mainEntityOfPage">
			<span itemprop="name"><?php echo $text_title; ?></span>
			</span>
			</span>
			
		</article><!-- #post-## -->

		<?php if ( is_singular() && sdfGo()->sdf_get_option('blog', 'toggle_single_comments') ) : ?>
		<?php if ( comments_open() || get_comments_number() != "0" ) : ?>
			<?php if ($sdf_wide_template) : ?>
				<div class="container">
			<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
				<div class="container-fluid">
			<?php endif; ?>
			
				<div class="row">	
					<div class="col-md-11 col-md-offset-1">
							<?php comments_template(); ?>
					</div>
				</div>
				
			<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<?php endif; ?>

<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php if ($sdf_wide_template) : ?>
			<div class="container">
<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
	<div class="container-fluid">
<?php endif; ?>
<?php if (  $query->max_num_pages > 1 ) : ?>
				<?php sdf_pagination($query->max_num_pages); ?>
<?php endif; ?>
<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
	</div>
<?php endif; ?>
