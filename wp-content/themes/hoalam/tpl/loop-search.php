<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: WordPress Post Loop.
 * DESCRIPTIONS: WordPress Loop for displaying posts and post content.
 *
 * AUTHOR:  SEO Design Framework
 */ 

$sdf_wide_template = ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) ? true : true;
 
	if ($sdf_wide_template) : ?>
		<div class="entry-search container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="entry-search container-fluid">
	<?php endif; ?>
	<?php /* Display navigation to next/previous pages when applicable */ ?>
	<?php if ( $wp_query->max_num_pages > 1 ) : ?>
		<div id="nav-above" class="navigation">
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', SDF_TXT_DOMAIN ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?></div>
		</div><!-- #nav-above -->
	<?php endif; ?>
	<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>

<?php /* If there are no results to display. */ ?>
<?php if ( ! have_posts() ) : ?>

	<?php if ($sdf_wide_template) : ?>
		<div class="entry-search container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="entry-search container-fluid">
	<?php endif; ?>
	
	<div id="post-0" class="post search not-found">
		<div class="entry-content">
			<h5><?php _e('Nothing Found', SDF_TXT_DOMAIN); ?></h5>
			<p><?php _e( 'Apologies, but no results were found for the requested search. Maybe you want to perform a similar search?', SDF_TXT_DOMAIN ); ?></p>
			<div class="row"><div class="col-md-3">
			<?php get_search_form(); ?>
			</div></div>

			<div class="search-help">
				<h5><?php _e('How to search', SDF_TXT_DOMAIN); ?></h5> 
				<p><?php _e('For best search results, follow these suggestions:', SDF_TXT_DOMAIN); ?></p>
				<ul>
					<li><?php _e('No matter what you\'re looking for, try to start with a simple search.', SDF_TXT_DOMAIN); ?></li>
					<li><?php _e('Always double check your spelling.', SDF_TXT_DOMAIN); ?></li>
					<li><?php _e('Try similar keywords, for example: tablet instead of laptop.', SDF_TXT_DOMAIN); ?></li>
					<li><?php _e('Try using more than one keyword.', SDF_TXT_DOMAIN); ?></li>
				</ul>
			</div>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
	
	<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>
	
<?php endif; ?>

<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
		<?php if ($sdf_wide_template) : ?>
			<div class="container">
			<div class="row">	
			<div class="col-md-12">
		<?php endif;
		
			$post_id = get_the_ID();
			$post_has_featured = false;
			
			if ( function_exists('has_post_thumbnail') && has_post_thumbnail() && (sdfGo()->sdf_get_option('blog', 'toggle_single_featured_image') && is_singular()) ) : ?>
				
				<div class="entry-thumbnail">
				<?php 
				$post_has_featured = true;
				// featured image animation on/off
				$data_animation = '';
				$data_duration = '';
				$animation_class = '';
				if(sdfGo()->sdf_get_option('animations', 'feat_img_animate')) {
				$data_animation = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != 'No' && sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != '') ? ' data-animation="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_type').'"' : '';
				$data_duration = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration') != '' && $data_animation != '') ? ' data-duration="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration').'"' : '';
				$animation_class = ($data_animation != '') ? ' viewport_animate animate_now' : '';
				}
				
				$featured_view ='';
				$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'sdf-image-md-12', false, '' );
				$pin_view = $pin_view[0];
				$featured_view = '<img itemprop="image" class="img-responsive'.$animation_class.'"'.$data_animation.$data_duration.' alt="'.get_the_title().'" src="'.$pin_view.'">';
				echo '<div class="featured-image"><a href="'.$pin_view.'" title="'.get_the_title().'" data-rel="prettyPhoto" class="pretty-view">'.$featured_view.'</a></div>';
				?>
				</div><!-- .entry-thumbnail -->
				
			<?php endif; ?>
			
				<?php 
				// check post type
				$post_format_icon = '';
				$post_format = get_post_format();
				$post_format_class = strtolower($post_format);
				switch ($post_format) {
					case 'video':
						$post_format_icon = 'video-camera';
						break;
					case 'audio':
						$post_format_icon = 'music';
						break;
					case 'gallery':
						$post_format_icon = 'picture-o';
						break;
					case 'image':
						$post_format_icon = 'picture-o';
						break;
					case 'quote':
						$post_format_icon = 'quote-left';
						break;
					case 'link':
						$post_format_icon = 'link';
						break;
					case NULL:
						$post_format_icon = 'pencil';
						$post_format_class = 'standard';
						break;						
				}

				$btn_toggle = sdfGo()->sdf_get_global('blog', 'toggle_post_format_button');
				$btn_text = sdfGo()->sdf_get_global('typography','theme_'.$post_format_class.'_post_format_button_custom_text');
				$icon_size = sdfGo()->sdf_get_global('typography','theme_post_format_button_icon_size');
				$toggle_icon = sdfGo()->sdf_get_global('typography','theme_post_format_button_icon_toggle');
				$btn_icon = ($toggle_icon) ? '<i class="fa fa-'.$post_format_icon.' '.$icon_size.'"></i> ' : '';
				// post meta
				$post_time = sdfGo()->sdf_get_global('blog', 'toggle_post_meta_time');
				?>
			
				<header class="entry-header">
				<?php if ( !( is_single() && !$post_has_featured )) :
					// title below featured image
					$anchor = "<a href=\"";
					$anchor .= get_permalink($post_id);
					$text_title = get_the_title($post_id);
					$anchor .= "\" title=\"". sprintf( esc_attr__( 'Permalink to %s', SDF_TXT_DOMAIN ), the_title_attribute( 'echo=0' ) )."\" rel=\"bookmark\">".$text_title."</a>";
					?>
					<h2 class="entry-title" itemprop="headline"><?php echo $anchor; ?></h2>
				<?php endif; ?>
			
				<div class="entry-utility">
			
				<?php if( $btn_toggle ) : ?>
				<div class="entry-type <?php echo $post_format_class; ?>-format"><?php echo ($btn_icon.$btn_text); ?></div>
				<?php endif; ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_time')) : ?>
				<span class="entry-time">
				<time class="updated" itemprop="datePublished" datetime="<?php the_time('Y/m/d H:i'); ?>"><?php the_time('l, F j'); ?></time>
				</span>
				<?php endif; ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_categories')) : ?>
				<?php if ( count( get_the_category() ) ) : ?>
				<span class="cat-links">
					<?php printf( __( '<span class="%1$s">Posted in</span> %2$s', SDF_TXT_DOMAIN ), 'entry-utility-prep entry-utility-prep-cat-links', get_the_category_list( ', ' ) ); ?>
				</span>
				<?php endif; ?>
				<?php endif; ?>
				
				<?php
				$author_id = get_query_var( 'author' );
				if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_author')) :
				
					if(empty($author_id)) $author_id = get_the_author_meta('ID');
					
					$by = __("by",SDF_TXT_DOMAIN);
					// check if SEO Ultimate(+) plugin is active
					if ( !class_exists( 'SEO_Ultimate' ) && !class_exists( 'SEO_Ultimate_Plus' ) ) {
						$author_link = get_author_posts_url($author_id);
						$author_name = '<span class="vcard author"><span class="fn"><a href="'.$author_link.'" rel="author">'.get_the_author_meta("display_name", $author_id).'</a></span></span>';
						echo('<span class="author-name" itemprop="name">'.$by.' '.$author_name.'</span>'); 
					}
					else {
						$author_name = '<span class="author"><span>'.get_the_author_meta("display_name", $author_id).'</span></span>';
						echo('<span class="author-name">'.$by.' '.$author_name.'</span>');
					}
				endif; ?>
				
				<?php if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_comments')) : ?>
					<?php if( !is_singular()) : ?>
						<?php if ( get_comments_number() != "0" || comments_open() ) : ?>
							<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', SDF_TXT_DOMAIN ), __( '1 Comment', SDF_TXT_DOMAIN ), __( '% Comments', SDF_TXT_DOMAIN ) ); ?></span>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif;

				edit_post_link(__('Edit this post', SDF_TXT_DOMAIN), '<p class="edit">','</p>'); ?>
			
				</div><!-- .entry-utility -->
				</header>
			<?php if ($sdf_wide_template) : ?>
				</div>
				</div>
				</div>
			<?php endif; ?>
			
				<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
					<div class="entry-summary">
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php endif; ?>
					
						<div itemprop="description"><?php the_excerpt(); ?></div>
								
						<?php if ($sdf_wide_template) : ?>
							</div>
						<?php endif; ?>
				
					</div><!-- .entry-summary -->
				<?php else : ?>
				
					<?php if( !is_singular()) : ?>
						<div class="entry-summary">
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php endif; ?>
						
						<?php if (sdfGo()->sdf_get_global('blog', 'toggle_full_posts')) : ?>	
						<div itemprop="articleBody"><?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?></div>
						<?php else: ?>
						<div itemprop="description"><?php the_excerpt(); ?></div>
						<?php endif; ?>
								
						<?php if ($sdf_wide_template) : ?>
							</div>
						<?php endif; ?>
						
						</div><!-- .entry-summary -->
					<?php else: ?>
					
						<div class="entry-content" itemprop="text">
						
							<?php if ($sdf_wide_template && !(strpos(get_the_content(),'[/sdf_hero]') || strpos(get_the_content(),'[/sdf_row]'))) : ?>
								<div class="container">
							<?php endif; ?>

								<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?>
								
							<?php if ($sdf_wide_template && !(strpos(get_the_content(),'[/sdf_hero]') || strpos(get_the_content(),'[/sdf_row]'))) : ?>
								</div>
							<?php endif; ?>
						
						</div><!-- .entry-content -->
						
						<?php if ($sdf_wide_template) : ?>
							<div class="container">
						<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
							<div class="container-fluid">
						<?php endif; ?>
						
						<?php 
						// tags
						if (sdfGo()->sdf_get_option('blog', 'toggle_post_meta_tags')) : ?>
						<?php
						$tags_list = get_the_tag_list('<div class="entry-tags" itemprop="keywords">',', ','</div>');
						if ( $tags_list ):
						?>
						<div class="tag-links">
							<h5 class="media-heading"><?php echo __( 'Tags', SDF_TXT_DOMAIN ); ?></h5>
							<?php echo $tags_list; ?>
						</div>
						<?php endif;
						endif; ?>
						
						<?php 
						//share links on single post
						if (sdfGo()->sdf_get_option('social_media', 'toggle_social_share')){
							sdf_social_sharing_buttons();
						}
						
						if (sdfGo()->sdf_get_option('blog', 'toggle_post_author_box')){
							$gravatar     = get_avatar( get_the_author_meta('email', $author_id), '75' );
							
							// check if SEO Ultimate(+) plugin is active
							$itemprop_name = ( !class_exists( 'SEO_Ultimate' ) && !class_exists( 'SEO_Ultimate_Plus' ) ) ? ' itemprop="name"' : '';
							
							$heading      = __("About",SDF_TXT_DOMAIN) ." ".'<span class="author-name"'.$itemprop_name.'>'.$author_name.'</span>';
							$description  = get_the_author_meta('description', $author_id);

							echo '<div class="author-box media">';
							if(empty($description))
							{
								$description  = __("This author hasn't yet written their biography.",SDF_TXT_DOMAIN);
								$description .= '</br>'.sprintf( __( 'Still we are proud %s contributed %s great entries.', SDF_TXT_DOMAIN ), '<span class="author-name"'.$itemprop_name.'>'.$author_name.'</span>', count_user_posts( $author_id ) );

								if(current_user_can('edit_users') || get_current_user_id() == $author_id)
								{
									$description .= "</br><a href='".admin_url( 'profile.php?user_id=' . $author_id )."'>".__( 'Edit the profile description here.', SDF_TXT_DOMAIN )."</a>";
								}
							}


							echo '<div class="media-left"><a class="media-object">'.$gravatar.'</a></div>';
							echo '<div class="author_description media-body">
								<h4 class="author-title media-heading">'.$heading.'</h4>
								<div class="author_description_text">'.wpautop($description).'</div></div>';
							echo '</div>';
						} ?>
						<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
							</div>
						<?php endif; ?>	
					
					<?php endif; ?>
				<?php endif; ?>
			
				
		</article><!-- #post-## -->

<?php endwhile; ?>
<?php if ($sdf_wide_template) : ?>
			<div class="container">
<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
	<div class="container-fluid">
<?php endif; ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', SDF_TXT_DOMAIN ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?></div>
				</div><!-- #nav-below -->
<?php endif; ?>
<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
	</div>
<?php endif; ?>