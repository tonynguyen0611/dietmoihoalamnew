<?php while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) : ?>
		<div class="entry-attachment container">
		<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="entry-attachment container-fluid">
		<?php endif; ?>

			<?php if ( wp_attachment_is_image( $post->id ) ) : $pin_view = wp_get_attachment_image_src( $post->id, "sdf-image-md-12"); ?>
				
				<?php 	
				
				$featured_view ='';
				$featured_view = '<img class="attachment img-responsive" alt="'.get_the_title().'" src="'.$pin_view[0].'" width="'.$pin_view[1].'" height="'.$pin_view[2].'">';
				echo '<div class="row"><div class="col-md-12"><a href="'.$pin_view[0].'" title="'.get_the_title().'" data-rel="prettyPhoto" class="pretty-view">'.$featured_view.'</a></div></div>';
				?>
				
			<?php else : ?>
				<a href="<?php echo wp_get_attachment_url($post->ID) ?>" title="<?php echo wp_specialchars( get_the_title($post->ID), 1 ) ?>" rel="attachment"><?php echo basename($post->guid) ?></a>
			<?php endif; ?>

		<?php if ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) : ?>
		</div>
		<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		</div>
		<?php endif; ?>
		<!-- .entry-attachment -->
	</article>
	
<?php endwhile; ?>