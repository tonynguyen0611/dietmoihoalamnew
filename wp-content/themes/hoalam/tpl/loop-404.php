<?php if ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) : ?>
<div class="entry-404 container">
<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
<div class="entry-404 container-fluid">
<?php endif; ?>


<h5><?php _e('Nothing Found', SDF_TXT_DOMAIN); ?></h5>
<p>
<?php _e('Sorry, the post you are looking for is not available. Maybe you want to perform a search?', SDF_TXT_DOMAIN); ?>
</p>
<div class="row"><div class="col-md-3">
<?php get_search_form(); ?>
</div></div>

<div class="404-help">
	<h5><?php _e('How to search', SDF_TXT_DOMAIN); ?></h5> 
	<p><?php _e('For best search results, follow these suggestions:', SDF_TXT_DOMAIN); ?></p>
	<ul>
		<li><?php _e('No matter what you\'re looking for, try to start with a simple search.', SDF_TXT_DOMAIN); ?></li>
		<li><?php _e('Always double check your spelling.', SDF_TXT_DOMAIN); ?></li>
		<li><?php _e('Try similar keywords, for example: tablet instead of laptop.', SDF_TXT_DOMAIN); ?></li>
		<li><?php _e('Try using more than one keyword.', SDF_TXT_DOMAIN); ?></li>
	</ul>
</div>

<?php if ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) : ?>
</div><!-- .entry-404 -->
<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
</div><!-- .entry-404 -->
<?php endif; ?>