<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: WordPress Post Loop.
 * DESCRIPTIONS: WordPress Loop for displaying posts and post content.
 *
 * AUTHOR:  SEO Design Framework
 */ 
 
 
$sdf_wide_template = ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) ? true : false;
 
?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<?php 
	
	$page_id = get_option('woocommerce_shop_page_id');

	$post_content = get_the_content();
	
	if ($sdf_wide_template) : ?>
		<div class="container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="container-fluid">
	<?php endif;
	//share links on single post
	if (is_singular() && sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'above'){
		sdf_social_sharing_buttons();
	}
	
	if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>

	<div class="entry-content">
	<?php if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
		<div class="container">
	<?php endif; ?>

		<?php woocommerce_content(); ?>
		
		<div class="entry-time hidden">
			<time class="updated" itemprop="datePublished" datetime="<?php the_time('Y/m/d H:i'); ?>"><?php the_time('l, F j'); ?></time>
		</div>
		<?php
		// check if SEO Ultimate(+) plugin is active
		if ( !class_exists( 'SEO_Ultimate' ) && !class_exists( 'SEO_Ultimate_Plus' ) ) {
			//plugin is activated
			$author_id = get_query_var( 'author' );
			if(empty($author_id)) $author_id = get_the_author_meta('ID');
			
			$author_link = get_author_posts_url($author_id);
			$author_name = '<span class="vcard author"><span class="fn"><a href="'.$author_link.'" rel="author">'.get_the_author_meta("display_name", $author_id).'</a></span></span>';
			echo('<div class="author-name hidden" itemprop="name">'.$author_name.'</div>'); 
		} 
		?>
		
	<?php if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
		</div>
	<?php endif; ?>
	
	</div><!-- .entry-content -->	
	
	<?php if ($sdf_wide_template) : ?>
		<div class="container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="container-fluid">
	<?php endif; ?>

		<?php wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', SDF_TXT_DOMAIN ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
		) );
	
		//share links on single post
		if (sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'below'){
			sdf_social_sharing_buttons();
		}
	
		edit_post_link(__('Edit page', SDF_TXT_DOMAIN), '<div class="container-fluid"><p class="edit">','</p></div>'); ?>
		
	<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>	
		
	</article>