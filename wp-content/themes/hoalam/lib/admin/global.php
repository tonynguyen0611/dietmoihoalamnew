<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
 * THEME NAME: SEO Design Framework 
 *
 * TYPE: sdf_global.php
 * CLASS NAME: SDF_global
 * DESCRIPTIONS: Implementation of Theme Admin Options    
 *
 * AUTHOR:  SEO Design Framework
 *    
 */

if ( !class_exists('SDF_Global') ) {  
	class SDF_Global extends ULTIncludes
	{
		
		public $_currentSettings = array();
		public $_themeOptions = array();
		public $_initialOptions = array();
		public $_currentPresetId;
		
		public function __construct() {	
			// check for child theme or give core options name
			$sdf_theme_options = defined('SDF_CHILD_THEME_OPTIONS') ? SDF_CHILD_THEME_OPTIONS : '_sdf_theme_options_core';
			define('SDF_THEME_OPTIONS', $sdf_theme_options);
			$sdf_style_preset_css_path = defined('SDF_CHILD_PATH') ? SDF_CHILD_PATH : SDF_LIB;
			define('SDF_PRESET_CSS_PATH', $sdf_style_preset_css_path);
			$sdf_style_preset_css_url = defined('SDF_CHILD_URL') ? SDF_CHILD_URL.'/css/' : SDF_CSS.'/';
			define('SDF_PRESET_CSS_URL', $sdf_style_preset_css_url);

			$this->_currentSettings = maybe_unserialize(get_option(SDF_THEME_OPTIONS));
			$this->setupGlobalArgs();
			if (!is_array($this->_currentSettings) || empty($this->_currentSettings) || $this->_currentSettings== NULL || $this->_currentSettings == '')
			{
				// if this is the first time setting up option, grab default
				$this->_initialOptions =  $this->_sdfGlobalArgs;
				$default_preset_id = $this->_initialOptions['preset']['theme_preset'];
				$this->_initialOptions['navigation'] = array();
				$this->_initialOptions['navigation'][$default_preset_id] = $this->_sdfGlobalArgs['navigation'];
				$this->_initialOptions['typography'] = array();
				$this->_initialOptions['typography'][$default_preset_id] = $this->_sdfGlobalArgs['typography'];

				add_option(SDF_THEME_OPTIONS, $this->getInitialThemeOptions($this->_initialOptions));
				
				add_action( 'wp', array(&$this, 'switchThemeOptions')); 
			} 
			else {
				// set preset navigation and typography
				if(!empty($this->_currentSettings['navigation']) && min(array_keys($this->_currentSettings['navigation'])) >= 1) {
					if(isset($this->_currentSettings['preset']['theme_preset'])) {
						$this->_currentPresetId = $this->_currentSettings['preset']['theme_preset'];
					}
					else{
						$this->_currentPresetId = min(array_keys($this->_currentSettings['navigation']));
					}
					
					if($this->_currentPresetId) {
						$this->_currentSettings['navigation'] = $this->_currentSettings['navigation'][$this->_currentPresetId];
						$this->_currentSettings['typography'] = $this->_currentSettings['typography'][$this->_currentPresetId];
					}
				}
			}

			if(is_admin()) {        
				// reset css after theme switch
				add_action( 'after_switch_theme', array(&$this, 'switchThemeOptions')); 
				add_action( 'after_switch_theme', array(&$this, 'createStaticCSS')); 
				// theme activation
				add_action( 'load-themes.php', array(&$this, 'init_theme'));
				add_action( 'upgrader_process_complete', array(&$this, 'upgrade_theme'), 20); 
				
				add_action('admin_menu', array(&$this, 'createMenu'));
				add_action('in_admin_header', array(&$this, 'sdf_theme_toolbar'));
				add_action('all_admin_notices', array(&$this, 'sdf_theme_header'));
				add_action('all_admin_notices', array(&$this, 'sdf_silo_subheader'));
			}
			// Login Logo
			add_action( 'login_enqueue_scripts', array(&$this, 'sdf_login_logo' ));
			add_filter( 'login_headertitle', array(&$this, 'sdf_login_logo_url_title' ));
			add_filter( 'login_headerurl', array(&$this, 'sdf_login_logo_url' ));
			//dashboard Footer
			add_filter('admin_footer_text', array(&$this, 'remove_footer_admin' ));
			
			add_action('template_redirect', array(&$this, 'admin_redirect_download_files'));
			add_filter('init', array(&$this, 'add_query_var_vars'));
			
			// new options patches
			$this->newOptionsPatches();
		}
		
		function createMenu(){
			
			// Add Default Settings, Enabled two methods to reach same setting page. Top Link and Settings link.
			add_menu_page(__(SDF_THEME_NAME.' Theme Settings', SDF_TXT_DOMAIN),__(SDF_THEME_NAME, SDF_TXT_DOMAIN),
			'manage_options', 'sdf', array(&$this,'getAdminPage'),'dashicons-admin-tools',3);
			// Add Settings Child
			add_submenu_page('sdf',__('Theme Settings - Global Settings', SDF_TXT_DOMAIN),
			__('Global Settings', SDF_TXT_DOMAIN), 'manage_options', 'sdf-settings', array(&$this,'getAdminPage'));
			add_submenu_page('sdf', __('Theme Settings - Silo Settings', SDF_TXT_DOMAIN),
			__('Silo', SDF_TXT_DOMAIN), 'manage_options', 'sdf-silo-manual-builder', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - SEO Settings', SDF_TXT_DOMAIN),
			__('SEO', SDF_TXT_DOMAIN), 'manage_options', 'seo', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Layout Settings', SDF_TXT_DOMAIN),
			__('Layout', SDF_TXT_DOMAIN), 'manage_options', 'sdf-layout', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Styles Settings', SDF_TXT_DOMAIN),
			__('Styles', SDF_TXT_DOMAIN), 'manage_options', 'sdf-styles', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Header Settings', SDF_TXT_DOMAIN),
			__('Header', SDF_TXT_DOMAIN), 'manage_options', 'sdf-header', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Footer Settings', SDF_TXT_DOMAIN),
			__('Footer', SDF_TXT_DOMAIN), 'manage_options', 'sdf-footer', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Slider Settings', SDF_TXT_DOMAIN),
			__('Slider', SDF_TXT_DOMAIN), 'manage_options', 'revslider', array(&$this,'getAdminPage'));
			add_submenu_page('sdf',__('Theme Settings - Shortcode Settings', SDF_TXT_DOMAIN),
			__('Shortcodes', SDF_TXT_DOMAIN), 'manage_options', 'sdf-shortcode', array(&$this,'getAdminPage'));
						
		}
		
		function getAdminPage() {
			if(isset($_GET['page'])):
			/* Display Theme Setting Tab based on Page variable selected. */
			 switch($_GET['page'])
				{
					case 'sdf':
						$this->getSettingsAdmin();            
					break;	
					case 'sdf-settings':
					$this->getSettingsAdmin('sdf-settings');
					break;	
					case 'seo':
					$this->getSettingsAdmin('seo');
					break;	
					case 'sdf-silo':
					$this->getSettingsAdmin('sdf-silo');
					break;	
					case 'sdf-silo-manual-builder':
					$this->getSettingsAdmin('sdf-silo-manual-builder');
					break;	
					case 'sdf-styles':
					$this->getSettingsAdmin('sdf-styles');
					break;	
					case 'sdf-layout':
					$this->getSettingsAdmin('sdf-layout');
					break;	
					case 'sdf-slider':
					$this->getSettingsAdmin('sdf-slider');
					break;
					case 'sdf-header':
					$this->getSettingsAdmin('sdf-header');
					break;	
					case 'sdf-footer':
					$this->getSettingsAdmin('sdf-footer');
					break;				
					case 'sdf-shortcode':
					$this->getSettingsAdmin('sdf-shortcode');
					break;	  
					default:
					break;        
				}
			endif;
		}
		/**
		 *  Setting Up Admin Header
		 */
		
		function sdf_theme_toolbar(){
		
			global $sdf_message, $current_page, $sdf_admin_pages;
			
			$current_page = (isset($_GET['page'])) ? $_GET['page'] : '';
			$sdf_admin_pages = array('sdf','sdf-settings','seo','sdf-silo','sdf-silo-manual-builder','sdf-header','sdf-layout','sdf-shortcode','sdf-styles','revslider','sdf-footer');
			if ( in_array( $current_page, $sdf_admin_pages)){
		
				echo '<div class="sdf-admin">';
				echo '<a id="sdf-settings"></a><a id="seo"><a id="sdf-silo"></a><a id="sdf-header"></a><a id="sdf-layout"></a><a id="sdf-styles"></a><a id="revslider"></a><a id="sdf-footer"></a>';	
				echo '<h2 class="wpu-logo">'.__('&nbsp;', SDF_TXT_DOMAIN);	
				echo '<div class="sdf-admin-toolbar">	
							<ul class="list-inline">
							<li><a href="http://www.seodesignframework.com/sdf-support/" title="Support" target="_blank">Support</a></li>	
							</ul>
						</div>';	
				echo '</h2>';		
				echo '</div>';
			}
		}	
		
		function sdf_theme_header(){	
			global $sdf_message, $current_page, $sdf_admin_pages;
			if ( in_array( $current_page, $sdf_admin_pages)){

				if($sdf_message != '') {
					echo '<div class="sdf-admin">';
					echo $sdf_message;
					echo '</div>';
				}
				
				echo '<div class="sdf-admin sdf-header">';
				echo '<ul id="wpu-tab" class="nav nav-tabs">';
				$active = ($current_page == 'sdf' || $current_page == 'sdf-settings') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-settings">Settings</a>
				</li>';
				$active = ($current_page == 'sdf-silo' || $current_page == 'sdf-silo-manual-builder') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-silo-manual-builder">Silo</a>
				</li>';
				$active = ($current_page == 'seo') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=seo">SEO</a>
				</li>';
				$active = ($current_page == 'sdf-layout') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-layout">Layout</a>
				</li>';
				$active = ($current_page == 'sdf-styles') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-styles">Styles</a>
				</li>';
				$active = ($current_page == 'sdf-header') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-header">Header</a>
				</li>';
				$active = ($current_page == 'sdf-footer') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-footer">Footer</a>
				</li>';
				$active = ($current_page == 'revslider') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=revslider">Slider</a>
				</li>';
				$active = ($current_page == 'sdf-shortcode') ? 'active' : '';
				echo '<li class="'.$active.'">
					<a href="admin.php?page=sdf-shortcode">Shortcodes</a>
				</li>';
				echo '</ul>';
				echo '</div>';
			}
		}
		
		// add Silo Submenu Pills
		function sdf_silo_subheader(){	
			global $current_page;
			if ( in_array( $current_page, array('sdf-silo','sdf-silo-manual-builder'))){
				
				$silo_title = ($current_page == 'sdf-silo') ? __("Current Silo", SDF_TXT_DOMAIN) : __("Manual Silo Builder", SDF_TXT_DOMAIN);
				$silo_link = ($current_page == 'sdf-silo') ? '<li class="active"><a href="#">' : '<li><a href="admin.php?page=sdf-silo">';
				$silo_link .=  __("Current Silo", SDF_TXT_DOMAIN).'</a></li>';
				$builder_link = ($current_page == 'sdf-silo-manual-builder') ? '<li class="active"><a href="#">' : '<li><a href="admin.php?page=sdf-silo-manual-builder">';
				$builder_link .=  __("Manual Silo Builder", SDF_TXT_DOMAIN).'</a></li>';
				
				echo '<div class="sdf-admin">';
				echo '<div class="row titlegroup">
							<div class="col-md-12">
								<div class="wpu-silo-icon"></div><h3 class="pull-left">'.$silo_title.'</h3>                     
							</div>
							</div>';
				echo '<ul class="nav nav-pills">'. $builder_link . $silo_link. '</ul>';
				echo '</div>';
			}
		}
		
		/**
		 *  Check if variable is posted and bind to _themeOptions array item
		 *  @param string $posted_option_section style section id (typography or navigation)
		 *  @param string $posted_option_id style option id
		 *  @param string $input_unit input unit for "validation"
		 */
		function sdf_style_input_posted($posted_option_section, $posted_option_id, $input_unit = '')
		{
			if(isset($_POST['sdf_'.$posted_option_id])) {
				// "validate" that input unit is added
				if($input_unit != '' && $_POST['sdf_'.$posted_option_id] != '' && !strpos($_POST['sdf_'.$posted_option_id], $input_unit)) {
					$_POST['sdf_'.$posted_option_id] = $_POST['sdf_'.$posted_option_id] . $input_unit;
				}
				$this->_themeOptions[$posted_option_section][$this->_currentPresetId][$posted_option_id] = $_POST['sdf_'.$posted_option_id];
			}
		}
		
		/**
		 *  Check if preset color (name) is posted and bind to _themeOptions array item
		 *  @param string $posted_option_section style section id (typography or navigation)
		 *  @param array/string $posted_option_id picker array var or hex color string
		 *  @return string returns the hex color value
		 */
		function sdf_style_preset_color_posted($posted_option_section, $posted_option_id, $return = false)
		{
			if(isset($_POST['sdf_'.$posted_option_id])) {
				$posted_color = $_POST['sdf_'.$posted_option_id];
				
				$newcolor = '';
				$preset_colors = $this->getColorPresets();

				$newcolor = trim($posted_color);
				
				if($return){
					return $newcolor;
				}
				else{
					$this->_themeOptions[$posted_option_section][$this->_currentPresetId][$posted_option_id] = $newcolor;
				}
			}
		}
		
		/**
		 *  Convert a preset color name(if set) to its hex color value
		 *  @param string $color hex color code
		 *  @return string returns the hex color value
		 */
		function sdf_get_picked_color($color, $transparent = true, $type = 'rgba')
		{
			if($color != '' && !is_array($color)){

				$preset_colors = $this->getColorPresets();
				
				if (array_key_exists($color, $preset_colors)) {
					if (array_key_exists($preset_colors[$color], $preset_colors)) {
						$color = strtolower($preset_colors[$preset_colors[$color]]);
					}
					else{
						$color = strtolower($preset_colors[$color]);
					}
				}
				else {
					$color = strtolower($color);
				}
				
				$newcolor = "";
				$hex_preset_val = '';
				$opacity_val = '';
				$rgb_args = explode(':', $color);
				$hex_preset_val = $rgb_args[0];
				$opacity_val = (count($rgb_args) > 1) ? trim($rgb_args[1]) : '1';
				
				if($type == 'rgba') {
					
					$newcolor = ($hex_preset_val != '') ? 'rgba(' . sdf_hex2RGB($hex_preset_val, true) . ',' . $opacity_val . ')' : 'transparent';
				}
				else{
					$newcolor = $hex_preset_val;
				}
				
				if(!$transparent && ($newcolor == '' || $newcolor == 'transparent')) {
					$newcolor = false;
				}
				
				return $newcolor;
			}
			elseif($transparent && $color == '') {
				return 'transparent';
			}
			else {
				return false;
			}
		}
		
		function should_update_settings($field = '')
		{
			if ( !current_user_can('edit_themes') )
				wp_die(__('Insufficient user privileges.', SDF_TXT_DOMAIN));

			if(isset($_POST[SDF_THEME_SUBMITTED_FIELD]))
			{
				if ($_POST[SDF_THEME_SUBMITTED_FIELD] == 'Y') {
					check_admin_referer($field);
					unset($_POST[SDF_THEME_SUBMITTED_FIELD]);
					return true;
				}
			}

			return false;
		}  // check if user can edit theme AND form is able to be processed.

		function getSettingsAdmin($default=''){
		
			global $sdf_message, $current_page;
			
			$sdf_message= '';
			
			$this->_themeOptions = maybe_unserialize(get_option(SDF_THEME_OPTIONS));
			$this->_currentPresetId = isset($this->_themeOptions['preset']['theme_preset']) ? $this->_themeOptions['preset']['theme_preset'] : '1';
			
			if(isset($_POST['footer-submit']))
			{
				if ($this->should_update_settings('wpu-theme-update-footer-settings')) {
					
						if(isset($_POST['sdf_toggle_footer'])):
							$this->_themeOptions['footer']['toggle_footer'] = prepare_toggle($_POST['sdf_toggle_footer']);
						else:
							$this->_themeOptions['footer']['toggle_footer'] = 0;
						endif;
						$this->_themeOptions['footer']['toggle_custom_footer'] = 1;
          				$this->_themeOptions['footer']['copyright_footer'] = $_POST['sdf_copyright_footer'];
          				$this->_themeOptions['footer']['footer_bottom_alignment'] = $_POST['sdf_footer_bottom_alignment'];
						$this->_themeOptions['misc']['custom_js_footer'] = $_POST['sdf_page_js_footer']; 
						$this->_themeOptions['misc']['theme_analytics'] = $_POST['sdf_global_analytics'];
							$this->updateGlobal($this->_themeOptions);
								$sdf_message = __('Footer Settings saved.', SDF_TXT_DOMAIN);
				}
			}
			if(isset($_POST['setting-submit']))
			{
				if ($this->should_update_settings('wpu-theme-update-global-settings')) {
					
						if(isset($_POST['sdf_toggle_placeholder'])){
							$tmpPlaceholdered = $_POST['sdf_toggle_placeholder'];
							if(!$tmpPlaceholdered ){ $tmpPlaceholdered  = 0; }
						}else{
							 $tmpPlaceholdered  = 0; 
						}
							$this->_themeOptions['misc']['toggle_placeholders'] =$tmpPlaceholdered;
				
						$this->_themeOptions['misc']['theme_maintenance_page'] = $_POST['sdf_theme_maintenance_page'];
				
						if(isset($_POST['sdf_toggle_maintenance_mode']))
							$this->_themeOptions['misc']['toggle_maintenance_mode'] = $_POST['sdf_toggle_maintenance_mode'];
						else
							$this->_themeOptions['misc']['toggle_maintenance_mode'] = 0;
				
						if(isset($_POST['sdf_theme_toggle_nav']))
							$this->_themeOptions['header']['toggle_nav'] = $_POST['sdf_theme_toggle_nav'];
						else
							$this->_themeOptions['header']['toggle_nav'] = 0;
				
						if(isset($_POST['sdf_theme_toggle_page_title']))
							$this->_themeOptions['title']['toggle_page_title'] = $_POST['sdf_theme_toggle_page_title'];
						else
							$this->_themeOptions['title']['toggle_page_title'] = 0;
				
						if(isset($_POST['sdf_theme_title_header']))
							$this->_themeOptions['title']['title_header'] = $_POST['sdf_theme_title_header'];
						else
							$this->_themeOptions['title']['title_header'] = 'h1';
						
						if(isset($_POST['sdf_theme_toggle_bread']))
							$this->_themeOptions['breadcrumbs']['toggle_breadcrumbs'] = $_POST['sdf_theme_toggle_bread'];
						else
							$this->_themeOptions['breadcrumbs']['toggle_breadcrumbs'] = 0;
						$this->_themeOptions['breadcrumbs']['bread_delim'] = $_POST['sdf_theme_bread_delim'];
						
						if(isset($_POST['sdf_theme_bread_home_disable']))
							$this->_themeOptions['breadcrumbs']['toggle_bread_home'] = $_POST['sdf_theme_bread_home_disable'];
						else
							$this->_themeOptions['breadcrumbs']['toggle_bread_home'] = 0;
						$this->_themeOptions['breadcrumbs']['bread_home_text'] = $_POST['sdf_theme_bread_home_text'];
						
						$this->_themeOptions['blog']['read_more_text'] = stripslashes( wp_filter_post_kses( addslashes($_POST['sdf_theme_read_more_text'])));
						$this->_themeOptions['blog']['excerpt_length'] = $_POST['sdf_theme_excerpt_length'];
						$this->_themeOptions['blog']['toggle_full_posts'] = $_POST['sdf_theme_toggle_full_posts'];
						$this->_themeOptions['blog']['toggle_featured_image'] = $_POST['sdf_theme_toggle_featured_image'];
						$this->_themeOptions['blog']['toggle_single_featured_image'] = $_POST['sdf_theme_toggle_single_featured_image'];
						$this->_themeOptions['blog']['toggle_page_featured_image'] = $_POST['sdf_theme_toggle_page_featured_image'];
						$this->_themeOptions['blog']['toggle_single_comments'] = $_POST['sdf_theme_toggle_single_comments'];
						$this->_themeOptions['blog']['toggle_page_comments'] = $_POST['sdf_theme_toggle_page_comments'];
						$this->_themeOptions['blog']['toggle_portfolio_comments'] = $_POST['sdf_theme_toggle_portfolio_comments'];
						$this->_themeOptions['blog']['post_title_position'] = $_POST['sdf_theme_post_title_position'];
						$this->_themeOptions['blog']['related_posts_count'] = $_POST['sdf_theme_related_posts_count'];
						$this->_themeOptions['blog']['toggle_related_posts'] = $_POST['sdf_theme_toggle_related_posts'];
						$this->_themeOptions['blog']['toggle_post_format_button'] = $_POST['sdf_theme_toggle_post_format_button'];
						$this->_themeOptions['blog']['toggle_post_author_box'] = $_POST['sdf_theme_toggle_post_author_box'];
						$this->_themeOptions['blog']['toggle_post_meta_time'] = $_POST['sdf_theme_toggle_post_meta_time'];
						$this->_themeOptions['blog']['toggle_post_meta_categories'] = $_POST['sdf_theme_toggle_post_meta_categories'];
						$this->_themeOptions['blog']['toggle_post_meta_author'] = $_POST['sdf_theme_toggle_post_meta_author'];
						$this->_themeOptions['blog']['toggle_post_meta_tags'] = $_POST['sdf_theme_toggle_post_meta_tags'];
						$this->_themeOptions['blog']['toggle_post_meta_comments'] = $_POST['sdf_theme_toggle_post_meta_comments'];
						
						$this->_themeOptions['misc']['toggle_admin_bar'] = $_POST['sdf_theme_toggle_admin_bar'];
						$this->_themeOptions['misc']['toggle_design_toolbar_menu'] = $_POST['sdf_theme_toggle_design_toolbar_menu'];
						$this->_themeOptions['misc']['toggle_h5bp_htaccess'] = $_POST['sdf_theme_toggle_h5bp_htaccess'];
						$this->_themeOptions['misc']['toggle_5g_htaccess'] = $_POST['sdf_theme_toggle_5g_htaccess'];
						
						if ( is_woocommerce_active() ) {
							$this->_themeOptions['misc']['woo_columns'] = $_POST['sdf_theme_woo_columns'];
							$this->_themeOptions['misc']['woo_products_per_page'] = $_POST['sdf_theme_woo_products_per_page'];
							$this->_themeOptions['misc']['woo_related_columns'] = $_POST['sdf_theme_woo_related_columns'];
							$this->_themeOptions['misc']['woo_related_products_per_page'] = $_POST['sdf_theme_woo_related_products_per_page'];
						}
						
						$this->_themeOptions['misc']['toggle_autosave'] = $_POST['sdf_theme_toggle_autosave'];
						$this->_themeOptions['misc']['toggle_nicescroll'] = $_POST['sdf_theme_toggle_nicescroll'];
						
						$this->_themeOptions['misc']['toggle_scrollto'] = $_POST['sdf_theme_toggle_scrollto'];
						$this->_themeOptions['misc']['scrolltotop_easing_type'] = $_POST['sdf_theme_scrolltotop_easing_type'];
						$this->_themeOptions['misc']['scrolltotop_duration'] = $_POST['sdf_theme_scrolltotop_duration'];
						
						$this->_themeOptions['misc']['auto_update_username'] = $_POST['sdf_theme_auto_update_username'];
						$this->_themeOptions['misc']['auto_update_email'] = $_POST['sdf_theme_auto_update_email'];
						
						$this->_themeOptions['misc']['toggle_yt_bg_video'] = $_POST['sdf_theme_toggle_yt_bg_video'];
						
						$this->_themeOptions['misc']['image_size_12_w'] = $_POST['sdf_theme_image_size_12_w'];
						$this->_themeOptions['misc']['image_size_12_h'] = $_POST['sdf_theme_image_size_12_h'];
						$this->_themeOptions['misc']['image_size_6_w'] = $_POST['sdf_theme_image_size_6_w'];
						$this->_themeOptions['misc']['image_size_6_h'] = $_POST['sdf_theme_image_size_6_h'];
						$this->_themeOptions['misc']['image_size_4_w'] = $_POST['sdf_theme_image_size_4_w'];
						$this->_themeOptions['misc']['image_size_4_h'] = $_POST['sdf_theme_image_size_4_h'];
						$this->_themeOptions['misc']['image_size_3_w'] = $_POST['sdf_theme_image_size_3_w'];
						$this->_themeOptions['misc']['image_size_3_h'] = $_POST['sdf_theme_image_size_3_h'];
						$this->_themeOptions['misc']['image_size_2_w'] = $_POST['sdf_theme_image_size_2_w'];
						$this->_themeOptions['misc']['image_size_2_h'] = $_POST['sdf_theme_image_size_2_h'];
						
						if(isset($_POST['sdf_theme_toggle_lightbox'])) 
							$this->_themeOptions['lightbox']['toggle_lightbox'] = $_POST['sdf_theme_toggle_lightbox'];
						else
							$this->_themeOptions['lightbox']['toggle_lightbox'] = 0;
							
						$this->_themeOptions['lightbox']['lightbox_animation_type'] = $_POST['sdf_theme_lightbox_animation_type'];	
						$this->_themeOptions['lightbox']['lightbox_size'] = $_POST['sdf_theme_lightbox_size'];
						
						if(isset($_POST['sdf_theme_toggle_animations'])) 
							$this->_themeOptions['animations']['toggle_animations'] = $_POST['sdf_theme_toggle_animations'];
						else
							$this->_themeOptions['animations']['toggle_animations'] = 0;
						
						if(isset($_POST['sdf_theme_feat_img_animate'])) 
							$this->_themeOptions['animations']['feat_img_animate'] = $_POST['sdf_theme_feat_img_animate'];
						else
							$this->_themeOptions['animations']['feat_img_animate'] = 0;
								
						$this->_themeOptions['animations']['default_animation_duration'] = $_POST['sdf_theme_default_animation_duration'];	
						$this->_themeOptions['animations']['feat_img_animation_type'] = $_POST['sdf_theme_feat_img_animation_type'];	
						$this->_themeOptions['animations']['feat_img_animation_duration'] = $_POST['sdf_theme_feat_img_animation_duration'];	
							
						if(isset($_POST['sdf_theme_toggle_social_share'])) 
							$this->_themeOptions['social_media']['toggle_social_share'] = $_POST['sdf_theme_toggle_social_share'];
						else
							$this->_themeOptions['social_media']['toggle_social_share'] = 0;
							
						if(isset($_POST['sdf_theme_social_share_position'])) 
							$this->_themeOptions['social_media']['social_share_position'] = $_POST['sdf_theme_social_share_position'];
						else
							$this->_themeOptions['social_media']['social_share_position'] = 'below';
						
						$this->_themeOptions['social_media']['social_share_animation_type'] = $_POST['sdf_theme_social_share_animation_type'];	
						$this->_themeOptions['social_media']['social_share_animation_duration'] = $_POST['sdf_theme_social_share_animation_duration'];
						$this->_themeOptions['social_media']['social_share_title'] = $_POST['sdf_theme_social_share_title'];
						$this->_themeOptions['social_media']['social_share_twitter_handle'] = $_POST['sdf_theme_social_share_twitter_handle'];
							
						foreach ( $this->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
							$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) );
							$this->_themeOptions['social_media']['social_media_'.$id] = (isset($_POST['sdf_theme_social_media_'.$id])) ? $_POST['sdf_theme_social_media_'.$id] : '';
						}
						
						foreach ( $this->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
							$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) );
							$this->_themeOptions['social_media']['toggle_'.$id.'_share'] = (isset($_POST['sdf_theme_toggle_'.$id.'_share'])) ? $_POST['sdf_theme_toggle_'.$id.'_share'] : '';
						}
						
						foreach ( $this->_sdfGlobalArgs['favtouch_icons'] as $icon_slug => $icon_default) {
							$this->_themeOptions['favtouch_icons'][$icon_slug] = (isset($_POST['sdf_theme_'.$icon_slug]) && $_POST['sdf_theme_'.$icon_slug] != '') ? $_POST['sdf_theme_'.$icon_slug] : '';
						}
					$this->updateGlobal($this->_themeOptions);
					$sdf_message = __('Global Settings saved.', SDF_TXT_DOMAIN);
				} 
			} // end POST['setting-submit']
			
			//start import json
			if(isset($_POST['sdf_import_json_options'])){
				if ($this->should_update_settings('wpu-theme-update-global-settings')) {
					if(isset($_POST['sfd_theme_options_json_import']) && ($_POST['sfd_theme_options_json_import'] != '')){
						$this->import_json_options($_POST['sfd_theme_options_json_import']);
					}
				}
			}
			//end import json
			
			//start reset json
			if(isset($_POST['sdf_reset_json_options'])){
				if ($this->should_update_settings('wpu-theme-update-global-settings')) {
					delete_option( SDF_THEME_OPTIONS );
					$this->_initialOptions =  $this->_sdfGlobalArgs;
					$default_preset_id = $this->_initialOptions['preset']['theme_preset'];
					$this->_initialOptions['navigation'] = array();
					$this->_initialOptions['navigation'][$default_preset_id] = $this->_sdfGlobalArgs['navigation'];
					$this->_initialOptions['typography'] = array();
					$this->_initialOptions['typography'][$default_preset_id] = $this->_sdfGlobalArgs['typography'];
					
					add_option(SDF_THEME_OPTIONS, $this->getInitialThemeOptions($this->_initialOptions));
					
					$this->switchThemeOptions();
					$this->createStaticCSS();
				}
			}
			//end reset json
			
			if(isset($_POST['header-submit'])){
				if ($this->should_update_settings('wpu-theme-update-logo-settings')) {
					unset($header_data);
					$header_data = array();
					$header_data = array_map( 'trim', explode(',', $_POST['sdf_theme_header_order']) );
					
					$this->_themeOptions['header']['header_order'] = $header_data;
					// box keys/sizes in order!
					unset($box_data);
					$box_data = array_filter( array_combine( $_POST['block-builder-box-id'], $_POST['block-builder-box-size'] ));
					$this->_themeOptions['header']['box_data'] = $box_data;
					// additional block classes
					$this->_themeOptions['header']['box_data_classes']['listItem_1'] = $_POST['sdf_theme_header_box_data_classes_header_block_1'];
					$this->_themeOptions['header']['box_data_classes']['listItem_2'] = $_POST['sdf_theme_header_box_data_classes_header_block_2'];
					$this->_themeOptions['header']['box_data_classes']['listItem_3'] = $_POST['sdf_theme_header_box_data_classes_header_block_3'];
					$this->_themeOptions['header']['box_data_classes']['listItem_4'] = $_POST['sdf_theme_header_box_data_classes_header_block_4'];
					$this->_themeOptions['header']['box_data_classes']['listItem_5'] = $_POST['sdf_theme_header_box_data_classes_header_block_5'];
					$this->_themeOptions['header']['box_data_classes']['listItem_6'] = $_POST['sdf_theme_header_box_data_classes_header_block_6'];
					$this->_themeOptions['header']['box_data_classes']['listItem_7'] = $_POST['sdf_theme_header_box_data_classes_header_block_7'];
					$this->_themeOptions['header']['box_data_classes']['listItem_8'] = $_POST['sdf_theme_header_box_data_classes_header_block_8'];
					
					$this->_themeOptions['misc']['custom_js_header'] = $_POST['sdf_theme_head_js'];	 	
				
					$this->_themeOptions['logo']['toggle_tagline'] = $_POST['toggle_tagline'];
					$this->_themeOptions['logo']['logo_type'] = $_POST['logo-options'];
					if($this->_themeOptions['logo']['logo_type'] == 'off'){
						$this->_themeOptions['logo']['logo_path'] = '';
					}elseif($this->_themeOptions['logo']['logo_type'] == 'logo-file-path'){
						$fileArray = array();
						$fileArray['file'] =$_POST['sdf_theme_logo_text'];
						$fileArray['url'] = $_POST['sdf_theme_logo_text'];
						$this->_themeOptions['logo']['logo_path'] = $fileArray;
					}				
					$this->updateGlobal($this->_themeOptions);
					
					$sdf_message = __('Header Settings saved.', SDF_TXT_DOMAIN);
				}
			}// end POST['header-submit']
			if(isset($_POST['layout-submit'])){
				if ($this->should_update_settings('wpu-theme-update-layout-settings')) {
					$this->_themeOptions['layout']['page_width'] = $_POST['sdf_theme_page_width'];
					$this->_themeOptions['layout']['theme_layout'] = $_POST['sdf_theme_layout_page'];
					$this->_themeOptions['layout']['page_sidebars'] = $_POST['sdf_page_sidebars_width'];
					$this->_themeOptions['layout']['custom_left'] = $_POST['sdf_theme_custom_left'];
					$this->_themeOptions['layout']['custom_center'] = $_POST['sdf_theme_custom_center'];
					$this->_themeOptions['layout']['custom_right'] = $_POST['sdf_theme_custom_right'];
		
					$this->updateGlobal($this->_themeOptions);
					$sdf_message = __('Layout Settings saved.', SDF_TXT_DOMAIN);
				}
			}// end POST['layout-submit']
			if(isset($_POST['shortCodeSubmit'])){
				if ($this->should_update_settings('wpu-theme-update-shortcode')) {
						$scodedata = array();
								$sdf_flag1 = $sdf_flag2 = false;
									
								$sdf_codes = $_POST['sdf_theme_shortcode_code'];
								$sdf_codes_content = $_POST['sdf_theme_shortcode'];
								$shortCount = count($sdf_codes_content)-1;
									
						for($i=0; $i<$shortCount; $i++){
							if(!get_magic_quotes_gpc()) {
								$sdf_codes_content[$i] = stripslashes($sdf_codes_content[$i]);
							}
							//$sdf_codes_content[$i] = htmlentities($sdf_codes_content[$i]);
							if( is_null($sdf_codes_content[$i]) || $sdf_codes_content[$i] == "" ) { 
								unset($sdf_codes[$i]); 
								unset($sdf_codes_content[$i]); 
							}
						}
						$sdf_codes   = array_values($sdf_codes);
						$sdf_codes_content = array_values($sdf_codes_content);
					
						$tmp_count_code_array =array_count_values($sdf_codes);
						foreach ($tmp_count_code_array as $duplicates) {
								if( $duplicates > 1 ) $sdf_flag1 = true;
								
							}
						$shortCodeCount = count($sdf_codes);	
						
						for ($i = 0; $i < $shortCodeCount; $i++) {
							
							if($i != 0 && empty($sdf_codes[$i])){
								$sdf_flag2 = true;
							}else{
								$sdf_codes[$i] = $sdf_codes[$i];
							}
						}
						$sdf_codes   = array_values($sdf_codes);
						$sdf_codes_content = array_values($sdf_codes_content);
						
						if ($sdf_codes_content ) {
							$scodedata['code'] = $sdf_codes;
							$scodedata['content']   = $sdf_codes_content;
						}
							
							if($sdf_flag1){//gotta fill it in
								$sdf_message = __('Shortcode Already exists, please use another shortcode, or edit the existing one.', SDF_TXT_DOMAIN);
							}elseif($sdf_flag2){
								$sdf_message = __('Shortcode was empty, please enter a shortcode.', SDF_TXT_DOMAIN);
							}else{
								$this->_themeOptions['misc']['theme_short_codes'] = $scodedata;
							
								
						$this->updateGlobal($this->_themeOptions);
						$sdf_message = __('Shortcodes Settings saved.', SDF_TXT_DOMAIN);
							}		
								 
								
				}
			}// end POST['shortCodeSubmit-submit']
			
			//start set default preset
			if(isset($_POST['sdf_submit_preset_default'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$this->_themeOptions['preset']['theme_preset'] = $_POST['sdf_theme_style_preset'];
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end set default preset
			
			//start add preset
			if(isset($_POST['sdf_submit_preset_add'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					if(!empty($this->_themeOptions['preset']['theme_presets'])) {
						$max_preset_id = max(array_keys($this->_themeOptions['preset']['theme_presets']));
						$max_preset_id++;
					}
					else{
						$max_preset_id = '1';
					}
					$this->_themeOptions['preset']['theme_presets'][$max_preset_id] = $_POST['sdf_input_preset_add'];
					$this->_themeOptions['navigation'][$max_preset_id] = $this->_sdfGlobalArgs['navigation'];
					$this->_themeOptions['typography'][$max_preset_id] = $this->_sdfGlobalArgs['typography'];
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end add preset
			
			//start clone preset
			if(isset($_POST['sdf_submit_preset_clone'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$clone_preset_id = $_POST['sdf_input_preset_rename_val'];
					if(!empty($this->_themeOptions['preset']['theme_presets'])) {
						$max_preset_id = max(array_keys($this->_themeOptions['preset']['theme_presets']));
						$max_preset_id++;
					}
					else{
						$max_preset_id = '1';
					}
					$this->_themeOptions['preset']['theme_presets'][$max_preset_id] = $_POST['sdf_input_preset_clone'];
					$this->_themeOptions['navigation'][$max_preset_id] = $this->_themeOptions['navigation'][$clone_preset_id];
					$this->_themeOptions['typography'][$max_preset_id] = $this->_themeOptions['typography'][$clone_preset_id];
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end clone preset
			
			//start rename preset
			if(isset($_POST['sdf_submit_preset_rename'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$rename_preset_id = $_POST['sdf_input_preset_rename_val'];
					$this->_themeOptions['preset']['theme_presets'][$rename_preset_id] = $_POST['sdf_input_preset_rename'];
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end rename preset
			
			//start remove preset
			if(isset($_POST['sdf_submit_preset_remove'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$remove_preset_id = $_POST['sdf_input_preset_remove_val'];
					unset ($this->_themeOptions['preset']['theme_presets'][$remove_preset_id]);
					if ( $this->_themeOptions['preset']['theme_preset'] == $remove_preset_id ) {
						if(!empty($this->_themeOptions['preset']['theme_presets'])) {
							$this->_themeOptions['preset']['theme_preset'] = min(array_keys($this->_themeOptions['preset']['theme_presets']));
						}
						else{
							$this->_themeOptions['preset']['theme_preset'] = '1';
						}
					}
					
					unset ($this->_themeOptions['navigation'][$remove_preset_id]);
					unset ($this->_themeOptions['typography'][$remove_preset_id]);
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end rename preset
			
			/*
			* Preset Colors
			*/
			
			//start add color
			if(isset($_POST['sdf_submit_color_add'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					
					$preset_colors = $this->getColorPresets();
					
					$posted_color_name = $_POST['sdf_input_color_name_add'];
					$posted_color = $_POST['sdf_input_color_hex_add'];

					if (!array_key_exists($posted_color_name, $preset_colors)) {
						$this->_themeOptions['typography'][$this->_currentPresetId]['preset_colors'][$posted_color_name] = $posted_color;
						$this->updateGlobal($this->_themeOptions);
					}
					
				}
			}
			//end add color
			
			//start change color
			if(isset($_POST['sdf_submit_color_change'])){	
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$change_preset_key = $_POST['sdf_input_color_change_val'];
					$new_preset_key = $_POST['preset_color_names'][0];
					$new_preset_val = $_POST['preset_color_hexs'][0];
				
					// update pickers with switched preset color name
					foreach ($this->_themeOptions['typography'][$this->_currentPresetId] as $key => $value) {
						if ( !is_array($value) && ($value == $change_preset_key)) {
							$this->_themeOptions['typography'][$this->_currentPresetId][$key] = $new_preset_key;
						}
					}
					$this->_themeOptions['typography'][$this->_currentPresetId]['preset_colors'][$change_preset_key] = $new_preset_val;
					$pendingKeys = array();
					$this->switchKeys( $this->_themeOptions['typography'][$this->_currentPresetId]['preset_colors'], $change_preset_key, $new_preset_key, $pendingKeys );
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end change color
			
			//start remove color
			if(isset($_POST['sdf_submit_color_remove'])){
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					$remove_preset_id = $_POST['sdf_input_color_remove_val'];
					unset($this->_themeOptions['typography'][$this->_currentPresetId]['preset_colors'][$remove_preset_id]);
					//$preset_colors = array_combine($_POST['preset_color_names'], $_POST['preset_color_hexs']);
					//$this->_themeOptions['preset_colors'] = $preset_colors;
					$this->updateGlobal($this->_themeOptions);
				}
			}
			//end rename color

			if(isset($_POST['styles-submit'])){	
				if ($this->should_update_settings('wpu-theme-update-type-settings')) {
					
					// google fonts
					if(isset($_POST['sdf_google_fonts'])) {
						$google_fonts = array();
						$sdf_flag1 = $sdf_flag2 = false;
						
						$sdf_fonts = $_POST['sdf_google_fonts'];
						$fontsCount = count($sdf_fonts)-1;
										
						for($i=0; $i<$fontsCount; $i++){
							if( is_null( $sdf_fonts[$i] ) || $sdf_fonts[$i]=="" ) { 
								unset($sdf_fonts[$i]);  
							}
						}
						
						if(is_array($sdf_fonts)):
							$sdf_fonts = array_values( $sdf_fonts );
							$tmp_count_font_array =array_count_values($sdf_fonts);
						endif;
						
						foreach($tmp_count_font_array as $duplicates) {
							if($duplicates >1)$sdf_flag1 = true;
						}
						$fontsCount = count($sdf_fonts);	
						
						for ($i = 0; $i < $fontsCount; $i++) {
							
							if($i != 0 && empty($sdf_fonts[$i])){
								$sdf_flag2 = true;
							}else{
								$sdf_fonts[$i] = $sdf_fonts[$i];
							}
						}
						$sdf_fonts   = array_values($sdf_fonts);
						
						if ($sdf_fonts) {
							$google_fonts = $sdf_fonts;
						}
								
						if($sdf_flag1){//gotta fill it in
							$sdf_message = __('Font Family Already exists, please use another font name.', SDF_TXT_DOMAIN);
						}elseif($sdf_flag2){
							$sdf_message = __('Font Family was empty, please enter a valid Family.', SDF_TXT_DOMAIN);
						}else{
							$this->_themeOptions['typography'][$this->_currentPresetId]['google_web_font'] = $google_fonts;
							
						}
					}
					
					// theme fonts
					$this->_themeOptions['typography'][$this->_currentPresetId]['theme_font_families'] = $this->_sdfGlobalArgs['typography']['theme_font_families'];
					
					$this->sdf_style_preset_color_posted('typography', 'theme_font_wide_bg_color');
					$this->sdf_style_input_posted('typography', 'theme_font_wide_bg_image');
					$this->sdf_style_input_posted('typography', 'theme_font_wide_bg_repeat');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_boxed_bg_color');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_bg_image');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_bg_repeat');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_boxed_box_bg_color');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_bg_image');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_bg_repeat');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_margin');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_padding');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_boxed_box_shadow_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_boxed_box_border_color');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_border_width');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_border_width_type');
					$this->sdf_style_input_posted('typography', 'theme_font_boxed_box_border_style');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_bg_color');
					$this->sdf_style_input_posted('typography', 'theme_font_link_text_decoration');
					$this->sdf_style_input_posted('typography', 'theme_font_link_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_box_shadow_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_hover_bg_color');
					$this->sdf_style_input_posted('typography', 'theme_font_link_hover_text_decoration');
					$this->sdf_style_input_posted('typography', 'theme_font_link_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_link_hover_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_font_link_transition');
					// default text
					$this->sdf_style_input_posted('typography', 'theme_font_size', 'px');
					$this->sdf_style_input_posted('typography', 'theme_font_family');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_color');
					// default subtitle
					$this->sdf_style_input_posted('typography', 'theme_font_subtitle_size');
					$this->sdf_style_input_posted('typography', 'theme_font_subtitle_family');
					$this->sdf_style_input_posted('typography', 'theme_font_subtitle_size_type');
					$this->sdf_style_preset_color_posted('typography', 'theme_font_subtitle_color');
					$this->sdf_style_input_posted('typography', 'theme_font_subtitle_weight');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_silo_font_color');
					$this->sdf_style_input_posted('typography', 'theme_silo_font_size');
					$this->sdf_style_input_posted('typography', 'theme_silo_font_size_type');
					$this->sdf_style_input_posted('typography', 'theme_silo_line_height');
					
					// default lists' styling
					$this->sdf_style_input_posted('typography', 'theme_font_list_padding');
					$this->sdf_style_input_posted('typography', 'theme_font_list_margin');
					$this->sdf_style_input_posted('typography', 'theme_font_list_item_line_height');
					$this->sdf_style_input_posted('typography', 'theme_font_list_style');

					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_styling');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_padding');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_margin');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_line_height');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_font_size');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_font_size_type');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_family');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_align');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_font_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_link_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_link_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_link_text_decoration');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_link_hover_text_decoration');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_style');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_icon');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_icon_size');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_icon_size_type');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_icon_margin');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_align');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_padding');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_margin');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_border_width');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_border_style');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_border_radius');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_background_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_border_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_box_shadow');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_background_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_border_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_hover_box_shadow');
					$this->sdf_style_input_posted('typography', 'theme_font_custom_list_item_hover_box_shadow_color');
					
					// headings styling
					if(isset($this->_headingsList) && is_array($this->_headingsList)){
					foreach ($this->_headingsList as $id => $val) {
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_letter_spacing');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_size');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_size_type');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_weight');
						$this->sdf_style_preset_color_posted('typography', 'theme_font_'.$id.'_color');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_family');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_margin_top');
						$this->sdf_style_input_posted('typography', 'theme_font_'.$id.'_margin_bottom');
					}
					}
					$this->sdf_style_input_posted('typography', 'logo_align');
					$this->sdf_style_input_posted('typography', 'logo_heading');
					$this->sdf_style_input_posted('typography', 'logo_padding_top');
					$this->sdf_style_input_posted('typography', 'logo_padding_bottom');
					$this->sdf_style_input_posted('typography', 'logo_tagline_margin');
					$this->sdf_style_input_posted('typography', 'logo_tagline_heading');
					$this->sdf_style_preset_color_posted('typography', 'logo_tagline_font_color');
					$this->sdf_style_preset_color_posted('typography', 'logo_tagline_font_hover_color');
					$this->sdf_style_input_posted('typography', 'logo_tagline_font_size');
					$this->sdf_style_input_posted('typography', 'logo_tagline_font_weight');
					$this->sdf_style_input_posted('typography', 'logo_tagline_font_size_type');
					$this->sdf_style_input_posted('typography', 'logo_tagline_font_family');
					
					$this->sdf_style_input_posted('typography', 'page_padding_top');
					$this->sdf_style_input_posted('typography', 'page_padding_bottom');
					$this->sdf_style_input_posted('typography', 'featured_image_padding_top');
					$this->sdf_style_input_posted('typography', 'featured_image_padding_bottom');
					
					
					$this->sdf_style_input_posted('typography', 'breadcrumbs_alignment');
					$this->sdf_style_input_posted('typography', 'breadcrumbs_padding_top');
					$this->sdf_style_input_posted('typography', 'breadcrumbs_padding_bottom');
					
					// background styling, page header, breadcrumbs
					foreach (array( 'header', 'logo', 'footer', 'footer_bottom', 'page_header', 'breadcrumbs', 'blockquote' ) as $id) {
						$this->sdf_style_input_posted('typography', $id.'_custom_bg');
						$this->sdf_style_preset_color_posted('typography', $id.'_bg_color');
						$this->sdf_style_input_posted('typography', $id.'_bg_image');
						$this->sdf_style_input_posted('typography', $id.'_bgrepeat');
						$this->sdf_style_input_posted('typography', $id.'_par_bg_ratio');
						$this->sdf_style_input_posted('typography', $id.'_box_shadow');
						$this->sdf_style_preset_color_posted('typography', $id.'_box_shadow_color');
						$this->sdf_style_preset_color_posted('typography', $id.'_font_color');
						$this->sdf_style_preset_color_posted('typography', $id.'_font_hover_color');
						$this->sdf_style_preset_color_posted('typography', $id.'_link_color');
						$this->sdf_style_preset_color_posted('typography', $id.'_link_hover_color');
						$this->sdf_style_input_posted('typography', $id.'_font_size');
						$this->sdf_style_input_posted('typography', $id.'_font_weight');
						$this->sdf_style_input_posted('typography', $id.'_font_size_type');
						$this->sdf_style_input_posted('typography', $id.'_font_family');
					}
					
					// page title/subtitle
					$this->sdf_style_input_posted('typography', 'page_header_link_text_decoration');
					$this->sdf_style_input_posted('typography', 'page_header_link_hover_text_decoration');
					$this->sdf_style_input_posted('typography', 'page_header_padding_top');
					$this->sdf_style_input_posted('typography', 'page_header_padding_bottom');
					$this->sdf_style_input_posted('typography', 'page_header_font_text_transform');
					$this->sdf_style_input_posted('typography', 'page_header_alignment');
					$this->sdf_style_preset_color_posted('typography', 'page_header_font_subtitle_color');
					$this->sdf_style_input_posted('typography', 'page_header_font_subtitle_family');
					$this->sdf_style_input_posted('typography', 'page_header_font_subtitle_weight');
					$this->sdf_style_input_posted('typography', 'page_header_font_subtitle_size');
					$this->sdf_style_input_posted('typography', 'page_header_font_subtitle_size_type');
					$this->sdf_style_input_posted('typography', 'page_header_font_subtitle_text_transform');
					$this->sdf_style_input_posted('typography', 'entry_title_padding_top');
					$this->sdf_style_input_posted('typography', 'entry_title_padding_bottom');
					
					// blockquote
					$this->sdf_style_input_posted('typography', 'blockquote_link_text_decoration');
					$this->sdf_style_input_posted('typography', 'blockquote_link_hover_text_decoration');
					$this->sdf_style_input_posted('typography', 'blockquote_padding');
					$this->sdf_style_input_posted('typography', 'blockquote_margin');
					$this->sdf_style_input_posted('typography', 'blockquote_font_text_transform');
					$this->sdf_style_preset_color_posted('typography', 'blockquote_font_author_color');
					$this->sdf_style_input_posted('typography', 'blockquote_font_author_family');
					$this->sdf_style_input_posted('typography', 'blockquote_font_author_weight');
					$this->sdf_style_input_posted('typography', 'blockquote_font_author_size');
					$this->sdf_style_input_posted('typography', 'blockquote_font_author_size_type');
					$this->sdf_style_input_posted('typography', 'blockquote_font_author_text_transform');
					$this->sdf_style_input_posted('typography', 'blockquote_border_style');
					$this->sdf_style_input_posted('typography', 'blockquote_border_radius');
					$this->sdf_style_input_posted('typography', 'blockquote_border_width');
					$this->sdf_style_preset_color_posted('typography', 'blockquote_border_color');
					
					// post format buttons styling
					$id = 'post_format_button';
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_icon_toggle');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_icon_size');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_family');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_size');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_size_type');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_border_width');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_border_style');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_border_radius');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_width');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_height');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_margin');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_custom_style');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_bg_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_bg_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_border_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_border_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_text_color');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_text_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_hover_shadow');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_'.$id.'_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_hover_box_shadow_color');
					
					foreach ( $this->_sdfPostFormats as $format_slug) {
						$id = $format_slug.'_post_format';
						
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_custom_text');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_bg_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_bg_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_border_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_border_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_text_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_text_hover_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_text_hover_shadow');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_box_shadow_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_hover_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_hover_box_shadow_color');
					}

					// follow buttons styling
					// common
					$this->sdf_style_input_posted('typography', 'theme_follow_button_icon_toggle');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_family');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_size');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_size_type');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_border_width');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_border_style');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_border_radius');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_width');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_height');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_margin');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_custom_style');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_bg_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_bg_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_border_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_border_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_text_color');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_text_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_text_hover_shadow');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_follow_button_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_follow_button_hover_box_shadow_color');
					//custom
					foreach ( $this->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
						$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_follow';
						
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_custom_text');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_hover_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_hover_shadow');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_box_shadow_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_hover_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_hover_box_shadow_color');
					}
					
					// scroll to top button styling
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_icon');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_icon_size');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_custom_text');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_family');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_size');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_size_type');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_border_width');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_border_style');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_border_radius');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_width');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_height');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_bottom');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_right');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_bg_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_bg_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_border_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_border_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_text_color');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_text_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_text_hover_shadow');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_scroll_to_top_button_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_scroll_to_top_button_hover_box_shadow_color');
					
					// sharing buttons styling
					// common
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_icon_toggle');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_icon_size');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_family');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_size');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_size_type');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_border_width');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_border_style');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_border_radius');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_width');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_height');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_margin');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_custom_style');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_bg_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_bg_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_border_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_border_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_text_color');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_text_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_text_hover_shadow');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_box_shadow_color');
					$this->sdf_style_input_posted('typography', 'theme_sharing_button_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_sharing_button_hover_box_shadow_color');
					//custom
					foreach ( $this->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
						$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_sharing';
						
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_custom_text');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_hover_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_hover_shadow');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_box_shadow_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_hover_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_hover_box_shadow_color');
					}
					
					// grid images and image overlay styling
					// grid styling
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_overlay_color');
					$this->sdf_style_input_posted('typography', 'theme_grid_image_grayscale');
					$this->sdf_style_input_posted('typography', 'theme_grid_image_opacity');
					$this->sdf_style_input_posted('typography', 'theme_grid_image_opacity_hover');
					$this->sdf_style_input_posted('typography', 'theme_grid_transition');
					$this->sdf_style_input_posted('typography', 'theme_grid_transform');
					$this->sdf_style_input_posted('typography', 'theme_grid_transform_hover');
					$this->sdf_style_input_posted('typography', 'theme_grid_transform_style');
					$this->sdf_style_input_posted('typography', 'theme_grid_transform_origin');
					
					$id = 'item_overlay_button';
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_icon_size');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_family');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_size');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_size_type');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_width');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_height');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_line_height');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_margin');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_padding');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_border_width');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_border_style');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_border_radius');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_bg_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_border_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_text_color');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_shadow');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_box_shadow_color');
					
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_bg_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_border_hover_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_text_hover_color');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_text_hover_shadow');
					$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_hover_box_shadow');
					$this->sdf_style_preset_color_posted('typography', 'theme_grid_'.$id.'_hover_box_shadow_color');
					
					foreach ( array( 'prettyphoto_button' => 'PrettyPhoto Button', 'item_button' => 'Item Button' ) as $btn_slug => $btn_name) {
						$id = 'item_overlay_'.str_replace( "-", "_", urlencode(strtolower($btn_slug)) );
						
						$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_icon');
						$this->sdf_style_input_posted('typography', 'theme_grid_'.$id.'_custom_text');
					}
					
					// buttons styling
					// common
					$this->sdf_style_input_posted('typography', 'theme_button_text_family');
					$this->sdf_style_input_posted('typography', 'theme_button_text_weight');
					$this->sdf_style_input_posted('typography', 'theme_button_letter_spacing');
					$this->sdf_style_input_posted('typography', 'theme_button_text_transform');
					$this->sdf_style_input_posted('typography', 'theme_button_border_width');
					$this->sdf_style_input_posted('typography', 'theme_button_border_style');
					$this->sdf_style_input_posted('typography', 'theme_button_transition');
					//custom
					foreach ($this->_sdfButtons as $id => $name) {
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_bg_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_border_hover_color');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_text_hover_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_text_hover_shadow');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_box_shadow_color');
						$this->sdf_style_input_posted('typography', 'theme_'.$id.'_button_hover_box_shadow');
						$this->sdf_style_preset_color_posted('typography', 'theme_'.$id.'_button_hover_box_shadow_color');
					}
					$this->sdf_style_preset_color_posted('typography', 'theme_link_button_text_color');
					$this->sdf_style_preset_color_posted('typography', 'theme_link_button_text_hover_color');
					// theme colors
					$this->sdf_style_preset_color_posted('typography', 'theme_primary_color');
					
					foreach ($this->_sdfSidebars as $id => $name) {
						if ($id != 'slider-widget-area') {
							$id_var = str_replace("-", "_", urlencode(strtolower($id)));
							// widget areas styling
							// used for header and footer areas
							if ( !in_array($id, array('default-widget-area', 'left-widget-area', 'right-widget-area')) ) {
								$this->sdf_style_input_posted('typography', $id_var.'_height');
								$this->sdf_style_input_posted('typography', $id_var.'_padding_top');
								$this->sdf_style_input_posted('typography', $id_var.'_padding_bottom');
							}
							$this->sdf_style_input_posted('typography', $id_var.'_custom_bg');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_bg_color');
							$this->sdf_style_input_posted('typography', $id_var.'_bg_image');
							$this->sdf_style_input_posted('typography', $id_var.'_bgrepeat');
							$this->sdf_style_input_posted('typography', $id_var.'_par_bg_ratio');
							$this->sdf_style_input_posted('typography', $id_var.'_box_shadow');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_box_shadow_color');
							$this->sdf_style_input_posted('typography', $id_var.'_equal_heights');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_align');
							// widgets styling
							$this->sdf_style_input_posted('typography', $id_var.'_wg_custom_style');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_border_width');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_border_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_border_style');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_border_radius');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_background_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_bg_image');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_bgrepeat');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_box_shadow');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_box_shadow_color');
							
							$this->sdf_style_input_posted('typography', $id_var.'_wg_custom_typo');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_align');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_margin_top');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_margin_bottom');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_transform');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_weight');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_size');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_size_type');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_family');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_title_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_title_box_shadow');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_title_box_shadow_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_content_align');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_font_size');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_font_size_type');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_family');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_font_color');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_link_color');
							$this->sdf_style_preset_color_posted('typography', $id_var.'_wg_link_hover_color');
							
							$this->sdf_style_input_posted('typography', $id_var.'_wg_custom_list_style');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_align');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_padding');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_margin');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_font_size');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_font_size_type');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_family');
							
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_font_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_link_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_link_hover_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_link_text_decoration');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_link_hover_text_decoration');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_style');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_icon');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_icon_size');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_icon_size_type');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_icon_margin');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_align');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_padding');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_margin');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_line_height');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_border_width');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_border_style');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_border_radius');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_background_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_border_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_box_shadow');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_box_shadow_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_background_hover_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_border_hover_color');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_hover_box_shadow');
							$this->sdf_style_input_posted('typography', $id_var.'_wg_list_item_hover_box_shadow_color');

						}
					}
					
					foreach($this->_ultTbsVars as  $option_key => $option_fields) {
						if($option_fields['type'] != 'callout') {
							$option_key_id = str_replace( "-", "_", urlencode(strtolower($option_key)) );
							if($option_fields['type'] == 'color') {
								$this->sdf_style_preset_color_posted('typography', 'theme_tbs_'.$option_key_id);
							}
							else{
								$this->sdf_style_input_posted('typography', 'theme_tbs_'.$option_key_id);
							}	
						}
					}
					
					$this->sdf_style_input_posted('navigation', 'menu_alignment');
					$this->sdf_style_input_posted('navigation', 'menu_alignment_2');
					$this->sdf_style_input_posted('navigation', 'menu_layout');
					$this->sdf_style_input_posted('navigation', 'sticky_menu_opacity');
					$this->sdf_style_input_posted('navigation', 'menu_style');
					
					foreach($this->_ultNav['menu_settings'] as $key => $val){
						
						if(isset($_POST['sdf_'.$key])) {	
							if(in_array($key, $this->_menuColorPickers)) {
								$this->_themeOptions['navigation'][$this->_currentPresetId]['menu_settings'][$key] = $this->sdf_style_preset_color_posted('', $key, true);
							}
							else {
								$this->_themeOptions['navigation'][$this->_currentPresetId]['menu_settings'][$key] = $_POST['sdf_'.$key];
							}
						}
					}

					$this->updateGlobal($this->_themeOptions);
					$sdf_message = __('Style Settings saved.', SDF_TXT_DOMAIN);					
					// get fresh theme options
					$this->switchThemeOptions();
					
					if (!is_multisite()) {
						$filename = 'preset-'.$this->_currentPresetId.'.css';
						$this->putCssContents($filename);
					}
					
				}
			}// end POST['styles-submit']
			
			if(isset($_POST['slider-submit'])){
				
				if ($this->should_update_settings('wpu-theme-update-slider')) {
					
					$sdf_message = __('Slider Settings saved.', SDF_TXT_DOMAIN);
				}
				
			}// end POST['slider-submit']
			
			// get fresh theme options
			$this->switchThemeOptions();
			
			// scroll panel shown
			$panel_collapse_shown = (sdf_is_not($_POST['sdf_panel_collapse_shown'], '')) ? $_POST['sdf_panel_collapse_shown'] : null;
			if($panel_collapse_shown){
echo '<script type="text/javascript">
/* <![CDATA[ */ 
/*global jQuery:false, Modernizr */
(function($) { 
"use strict"; 
jQuery(document).ready(function($) {
var shownPanel = $("#'.$panel_collapse_shown.'");
if (shownPanel.length > 0) {
setTimeout(function(){
shownPanel.collapse("show");
var panelHeadingElem = shownPanel.prev(".panel-heading");
var wpadminbarHeight = 0;
wpadminbarHeight = ($("#wpadminbar").length > 0) ? parseInt($("#wpadminbar").height(), 10) : 0;
var panelHeadingHeight = 0;
panelHeadingHeight = (panelHeadingElem.length > 0) ? parseInt(panelHeadingElem.height(), 10) : 0;
var scrollToPosition = shownPanel.offset().top - panelHeadingHeight - wpadminbarHeight - 4;
$("html, body").animate({ scrollTop: scrollToPosition }, scrollToPosition*3);
},600);
}
});
})(jQuery);
/* ]]> */ 
</script>';	
			}
	
			?>

			<?php if(isset($this->_currentSettings)): 
			
			$sdf_theme_font_size = ($this->_currentSettings['typography']['theme_font_size']) ? $this->_currentSettings['typography']['theme_font_size'] : '14px';
			if(!strpos($sdf_theme_font_size, 'px')) {
				$sdf_theme_font_size .= 'px';
			}
			
			?> 
<div class="wrap sdf-admin">	
<div class="tab-content">	

<?php if($sdf_message){
				echo '<div id="message-sdf" class="alert alert-sdf"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>' . $sdf_message . '</strong></div>';
			} ?>
<?php if ($current_page == 'sdf' || $current_page == 'sdf-settings') : ?>
<div id="sdf-settings" class="tab-pane<?php if($current_page == 'sdf' || $current_page == 'sdf-settings'): echo ' active'; endif; ?>">
	<form name="sdf_theme_header_settings" id="sdf_theme_header_settings" class="form-horizontal" method="post" action="#sdf-settings" >
	<div id="sdf_theme_settings_page" class="wrap">
                    <input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
                    <div class="row titlegroup">
					<div class="col-sm-8 col-md-9">					
                    <div class="wpu-settings-icon"></div><h3 class="pull-left"><?php _e('Global Settings', SDF_TXT_DOMAIN); ?><small class="expand-hide"><a href="javascript:" class="wpu-open-close">+ Expand All</a></small></h3>  
                         
                   </div>
					<div class="col-sm-4 col-md-3">
                           
                                 <input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="setting-submit" id="setting-submit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" />
                           
                    </div>
                   </div>
                   <div class="wpu-group">
<div class="panel-group" id="wpu-sett-acc">
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsenav" href="#collapsenav">
        <?php _e('Navigation', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsenav" class="panel-collapse collapse">
      <div class="panel-body">
	  
	  <div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Navigation', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_nav'],"1", "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_nav'],"0", "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_toggle_nav" value="<?php echo $this->_currentSettings['header']['toggle_nav']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>            
                        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsett2" href="#collapsett2">
        <?php _e('Page Title and Breadcrumbs', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsett2" class="panel-collapse collapse">
      <div class="panel-body">
	  
	  <div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Page Title', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_page_title'],"1", "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_page_title'],"0", "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_toggle_page_title" value="<?php echo $this->_currentSettings['title']['toggle_page_title']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Tag', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf-toggle-page-title" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"h1", "btn-sdf-grey")?>" value="h1">H1</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"h2", "btn-sdf-grey")?>" value="h2">H2</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"disable", "btn-sdf-grey")?>" value="disable">None</button>
					</div>
					<input type="hidden" name="sdf_theme_title_header" value="<?php echo $this->_currentSettings['title']['title_header']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('(H1, H2, or No HTML Tags)', SDF_TXT_DOMAIN) ?></div>
			</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Breadcrumbs', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'] ),1, "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_toggle_bread" value="<?php echo $this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'] ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Use this global setting to turn breadcrumbs off or on site wide (default setting is on). Also, you can specify a custom breadcrumb separator, turn the home link on or off or change the home link text to something more SEO friendly.', SDF_TXT_DOMAIN) ?></div>
		</div>

    <div class="form-group">
		<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Home Link', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<div class="btn-group sdf_toggle">
		  <button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['breadcrumbs']['toggle_bread_home']),1, "btn-sdf-grey")?>" value="1">On</button>
		  <button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['breadcrumbs']['toggle_bread_home']),0, "btn-sdf-grey")?>" value="0">Off</button>
		</div>
		<input type="hidden" name="sdf_theme_bread_home_disable" value="<?php echo $this->_currentSettings['breadcrumbs']['toggle_bread_home']  ; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"><?php _e('Disable Home link at the beginning of breadcrumb.', SDF_TXT_DOMAIN) ?></div>
	</div>
	
	<div class="form-group">
		<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Breadcrumb Home Title', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type='text' name='sdf_theme_bread_home_text' class="form-control" id='sdf_theme_bread_home_text' value='<?php echo $this->_currentSettings['breadcrumbs']['bread_home_text']; ?>' size='60' />
		</div>
		<div class="col-sm-4 col-md-5 help-text"><?php _e('Text used for home link.', SDF_TXT_DOMAIN) ?></div>
	</div>
                            
	<div class="form-group">
		<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Breadcrumb Separator', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type='text' name='sdf_theme_bread_delim' class="form-control" id='sdf_theme_bread_delim' value='<?php echo $this->_currentSettings['breadcrumbs']['bread_delim']; ?>' size='40' />
		</div>
		<div class="col-sm-4 col-md-5 help-text"><?php _e('Character used to separate your breadcrumbs. Default is &raquo;.', SDF_TXT_DOMAIN) ?></div>
	</div>               
                        
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapseblog" href="#collapseblog">
       <?php _e('Blog (Category, Archive, Single Posts) and Page Content Control', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapseblog" class="panel-collapse collapse">
      <div class="panel-body">
			
				<div class="bs-callout bs-callout-grey">
				<h4><?php _e('Pages', SDF_TXT_DOMAIN); ?></h4>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_toggle_page_featured_image" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Featured Image on Pages', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_page_featured_image'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_page_featured_image'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_page_featured_image" value="<?php echo $this->_currentSettings['blog']['toggle_page_featured_image']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e(' Turning On will keep default featured image.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_toggle_page_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Comments on Pages', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_page_comments'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_page_comments'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_page_comments" value="<?php echo $this->_currentSettings['blog']['toggle_page_comments']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="bs-callout bs-callout-grey">
				<h4><?php _e('Blog', SDF_TXT_DOMAIN); ?></h4>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Excerpt Length', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_excerpt_length' class="form-control" id='sdf_theme_excerpt_length' value='<?php echo $this->_currentSettings['blog']['excerpt_length']; ?>' size='40' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Automatic excerpt length (word count)', SDF_TXT_DOMAIN) ?></div>
				</div>
			
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Read More Text', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_read_more_text' class="form-control" id='sdf_theme_read_more_text' value='<?php echo $this->_currentSettings['blog']['read_more_text']; ?>' size='40' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Custom Excerpt Read More Text(or Icon)', SDF_TXT_DOMAIN) ?></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_full_posts" class="col-sm-4 col-md-4 control-label"><?php _e('Show Full Posts on Category and Archive pages', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_full_posts'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_full_posts'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_full_posts" value="<?php echo $this->_currentSettings['blog']['toggle_full_posts']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turning Off will keep default excerpt usage.', SDF_TXT_DOMAIN) ?></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_single_featured_image" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Featured Image on Single Posts', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_single_featured_image'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_single_featured_image'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_single_featured_image" value="<?php echo $this->_currentSettings['blog']['toggle_single_featured_image']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e(' Turning On will keep default featured image.', SDF_TXT_DOMAIN) ?></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_featured_image" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Featured Image on Category and Archive pages', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_featured_image'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_featured_image'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_featured_image" value="<?php echo $this->_currentSettings['blog']['toggle_featured_image']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e(' Turning On will keep default featured image.', SDF_TXT_DOMAIN) ?></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_post_title_position" class="col-sm-4 col-md-4 control-label"><?php _e('Set Title Above or Below Featured Image on Category and Archive pages', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['post_title_position'] ),1, "btn-sdf-grey")?>" value="1">Above</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['post_title_position'] ),0, "btn-sdf-grey")?>" value="0">Below</button>
					</div>
					<input type="hidden" name="sdf_theme_post_title_position" value="<?php echo $this->_currentSettings['blog']['post_title_position']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_single_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Comments on Single Posts', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_single_comments'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_single_comments'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_single_comments" value="<?php echo $this->_currentSettings['blog']['toggle_single_comments']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="bs-callout bs-callout-grey">
				<p><?php _e('Related Posts', SDF_TXT_DOMAIN); ?></p>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_toggle_related_posts" class="col-sm-4 col-md-4 control-label"><?php _e('Show Related Posts on Single Posts', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_related_posts'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_related_posts'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_related_posts" value="<?php echo $this->_currentSettings['blog']['toggle_related_posts']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Related Posts Count', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_related_posts_count' class="form-control" id='sdf_theme_related_posts_count' value='<?php echo $this->_currentSettings['blog']['related_posts_count']; ?>' size='40' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="bs-callout bs-callout-grey">
				<p><?php _e('Turn On/Off Post Meta Data', SDF_TXT_DOMAIN); ?></p>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Format Buttons', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_format_button'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_format_button'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_format_button" value="<?php echo $this->_currentSettings['blog']['toggle_post_format_button']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Format Icons globally. Turning Off will hide these Icons from Blog, Archives and Single Posts.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Author Box', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_author_box'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_author_box'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_author_box" value="<?php echo $this->_currentSettings['blog']['toggle_post_author_box']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Author Box globally. Turning Off will hide Author Box from Single Posts.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Time', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_time'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_time'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_meta_time" value="<?php echo $this->_currentSettings['blog']['toggle_post_meta_time']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Time globally. Turning Off will hide Time Data from Blog, Archives and Single Posts Meta Data.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Categories', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_categories'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_categories'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_meta_categories" value="<?php echo $this->_currentSettings['blog']['toggle_post_meta_categories']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Categories globally. Turning Off will hide Categories Data from Blog, Archives and Single Posts Meta Data.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Author', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_author'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_author'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_meta_author" value="<?php echo $this->_currentSettings['blog']['toggle_post_meta_author']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Author globally. Turning Off will hide Author from Blog, Archives and Single Posts Meta Data.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Tags', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_tags'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_tags'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_meta_tags" value="<?php echo $this->_currentSettings['blog']['toggle_post_meta_tags']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Tags globally. Turning Off will hide Tags Data from Blog, Archives and Single Posts Meta Data.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Comments Count/Link', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_comments'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_post_meta_comments'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_post_meta_comments" value="<?php echo $this->_currentSettings['blog']['toggle_post_meta_comments']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on Post Comments Count/Link globally. Turning Off will hide Comments Count/Link Data from Blog, Archives and Single Posts Meta Data.', SDF_TXT_DOMAIN) ?></div>
				</div>
			
				<div class="bs-callout bs-callout-grey">
				<h4><?php _e('Portfolio', SDF_TXT_DOMAIN); ?></h4>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_toggle_portfolio_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Comments on Portfolio Posts', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_portfolio_comments'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['blog']['toggle_portfolio_comments'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_portfolio_comments" value="<?php echo $this->_currentSettings['blog']['toggle_portfolio_comments']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
       
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapseimg" href="#collapseimg">
       <?php _e('Theme Image Sizes', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapseimg" class="panel-collapse collapse">
      <div class="panel-body">
				<div class="bs-callout bs-callout-grey">
				<h4><?php _e('For Advanced Users', SDF_TXT_DOMAIN) ?></h4>
				<p><?php _e('Set Image Width/Height According to Container Size Set in "Bootstrap LESS variables" under "Styles" Tab.', SDF_TXT_DOMAIN) ?></p>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Full Width Size', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3 form-inline">
						<div class="input-group">
						<input type='text' name='sdf_theme_image_size_12_w' class="form-control" id='sdf_theme_image_size_12_w' value='<?php echo $this->_currentSettings['misc']['image_size_12_w']; ?>' placeholder="Width" />
						<span class="input-group-addon">x</span>
						<input type='text' name='sdf_theme_image_size_12_h' class="form-control" id='sdf_theme_image_size_12_h' value='<?php echo $this->_currentSettings['misc']['image_size_12_h']; ?>' placeholder="Height" />
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel. Default is:
<code>1200x640</code>', SDF_TXT_DOMAIN); ?></div>
				</div>
       
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('One Half Size', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3 form-inline">
						<div class="input-group">
						<input type='text' name='sdf_theme_image_size_6_w' class="form-control" id='sdf_theme_image_size_6_w' value='<?php echo $this->_currentSettings['misc']['image_size_6_w']; ?>' placeholder="Width" />
						<span class="input-group-addon">x</span>
						<input type='text' name='sdf_theme_image_size_6_h' class="form-control" id='sdf_theme_image_size_6_h' value='<?php echo $this->_currentSettings['misc']['image_size_6_h']; ?>' placeholder="Height" />
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel. Default is:
<code>570x320</code>', SDF_TXT_DOMAIN); ?></div>
				</div>
       
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('One Third Size', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3 form-inline">
						<div class="input-group">
						<input type='text' name='sdf_theme_image_size_4_w' class="form-control" id='sdf_theme_image_size_4_w' value='<?php echo $this->_currentSettings['misc']['image_size_4_w']; ?>' placeholder="Width" />
						<span class="input-group-addon">x</span>
						<input type='text' name='sdf_theme_image_size_4_h' class="form-control" id='sdf_theme_image_size_4_h' value='<?php echo $this->_currentSettings['misc']['image_size_4_h']; ?>' placeholder="Height" />
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel. Default is:
<code>380x215</code>', SDF_TXT_DOMAIN); ?></div>
				</div>
       
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('One Quarter Size', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3 form-inline">
						<div class="input-group">
						<input type='text' name='sdf_theme_image_size_3_w' class="form-control" id='sdf_theme_image_size_3_w' value='<?php echo $this->_currentSettings['misc']['image_size_3_w']; ?>' placeholder="Width" />
						<span class="input-group-addon">x</span>
						<input type='text' name='sdf_theme_image_size_3_h' class="form-control" id='sdf_theme_image_size_3_h' value='<?php echo $this->_currentSettings['misc']['image_size_3_h']; ?>' placeholder="Height" />
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel. Default is:
<code>285x160</code>', SDF_TXT_DOMAIN); ?></div>
				</div>
       
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('One Sixth Size', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3 form-inline">
						<div class="input-group">
						<input type='text' name='sdf_theme_image_size_2_w' class="form-control" id='sdf_theme_image_size_2_w' value='<?php echo $this->_currentSettings['misc']['image_size_2_w']; ?>' placeholder="Width" />
						<span class="input-group-addon">x</span>
						<input type='text' name='sdf_theme_image_size_2_h' class="form-control" id='sdf_theme_image_size_2_h' value='<?php echo $this->_currentSettings['misc']['image_size_2_h']; ?>' placeholder="Height" />
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel. Default is:
<code>190x110</code>', SDF_TXT_DOMAIN); ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsebgvideo" href="#collapsebgvideo">
       <?php _e('Background Video', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsebgvideo" class="panel-collapse collapse">
      <div class="panel-body">

				<div class="form-group">
					<label for="sdf_theme_toggle_yt_bg_video" class="col-sm-4 col-md-4 control-label"><?php _e('Enable/Disable Background Video', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_yt_bg_video'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_yt_bg_video'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_yt_bg_video" value="<?php echo $this->_currentSettings['misc']['toggle_yt_bg_video']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('If Enabled, Background Video Script will be loaded.', SDF_TXT_DOMAIN) ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsenicescroll" href="#collapsenicescroll">
       <?php _e('Page Scroll (NiceScroll, SmoothScroll)', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsenicescroll" class="panel-collapse collapse">
      <div class="panel-body">

				<div class="form-group">
					<label for="sdf_theme_toggle_nicescroll" class="col-sm-4 col-md-4 control-label"><?php _e('Default (Native) Scroll / NiceScroll / SmoothScroll', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_nicescroll'] ),0, "btn-sdf-grey")?>" value="0">Default</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_nicescroll'] ),1, "btn-sdf-grey")?>" value="1">NiceScroll</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_nicescroll'] ),2, "btn-sdf-grey")?>" value="2">SmoothScroll</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_nicescroll" value="<?php echo $this->_currentSettings['misc']['toggle_nicescroll']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turning to Default will keep default page scroll functionality.', SDF_TXT_DOMAIN) ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsescrolltotop" href="#collapsescrolltotop">
       <?php _e('Scroll to Top', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsescrolltotop" class="panel-collapse collapse">
      <div class="panel-body">

				<div class="form-group">
					<label for="sdf_theme_toggle_scrollto" class="col-sm-4 col-md-4 control-label"><?php _e('Enable/Disable Scroll to Top Icon', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_scrollto'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_scrollto'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_scrollto" value="<?php echo $this->_currentSettings['misc']['toggle_scrollto']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turning Off will hide Scroll to Top Icon.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_scrolltotop_easing_type" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll to Top Easing Type', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select id="sdf_theme_scrolltotop_easing_type" name="sdf_theme_scrolltotop_easing_type" class="form-control"><?php echo $this->createEasingPicker( $this->_currentSettings['misc']['scrolltotop_easing_type']); ?></select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Choose Easing Type.', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_scrolltotop_duration" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll to Top Duration', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_scrolltotop_duration" name="sdf_theme_scrolltotop_duration" class="form-control" type="text" value="<?php echo $this->_currentSettings['misc']['scrolltotop_duration']  ; ?>">
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Enter integer value in miliseconds(e.g. 800), otherwise default value(700) will be used.', SDF_TXT_DOMAIN); ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapseautosave" href="#collapseautosave">
       <?php _e('Autosave', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapseautosave" class="panel-collapse collapse">
      <div class="panel-body">

				<div class="form-group">
					<label for="sdf_theme_toggle_autosave" class="col-sm-4 col-md-4 control-label"><?php _e('Autosave', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_autosave'] ),1, "btn-sdf-grey")?>" value="1">Enable</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_autosave'] ),0, "btn-sdf-grey")?>" value="0">Disable</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_autosave" value="<?php echo $this->_currentSettings['misc']['toggle_autosave']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Choosing "Enable" will keep default autosave functionality.', SDF_TXT_DOMAIN) ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsett3" href="#collapsett3">
       <?php _e('Lightbox (Modal)', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsett3" class="panel-collapse collapse">
      <div class="panel-body">
                             
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Lightbox', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['lightbox']['toggle_lightbox'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['lightbox']['toggle_lightbox'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_lightbox" value="<?php echo $this->_currentSettings['lightbox']['toggle_lightbox']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on lightbox functionality globally. Turning Off will disable All lightboxes.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_lightbox_animation_type" class="col-sm-4 col-md-4 control-label"><?php _e('Global Lightbox Entrance Animation Type', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select id="sdf_theme_lightbox_animation_type" name="sdf_theme_lightbox_animation_type" class="form-control"><?php echo $this->createModalAnimationPicker( $this->_currentSettings['lightbox']['lightbox_animation_types'], $this->_currentSettings['lightbox']['lightbox_animation_type'], false, 'default'); ?></select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_lightbox_size" class="col-sm-4 col-md-4 control-label"><?php _e('Global Lightbox Size', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select id="sdf_theme_lightbox_size" name="sdf_theme_lightbox_size" class="form-control"><?php echo $this->createModalAnimationPicker( $this->_currentSettings['lightbox']['lightbox_sizes'], $this->_currentSettings['lightbox']['lightbox_size']); ?></select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
                        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapseanim" href="#collapseanim">
       <?php _e('Entrance Animations', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapseanim" class="panel-collapse collapse">
      <div class="panel-body">
                             
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Animations', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['animations']['toggle_animations'] ),1, "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['animations']['toggle_animations'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="sdf_theme_toggle_animations" value="<?php echo $this->_currentSettings['animations']['toggle_animations']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on animations functionality globally. Turning Off will disable loading of a script.', SDF_TXT_DOMAIN) ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_default_animation_duration" class="col-sm-4 col-md-4 control-label"><?php _e('Default Animation Duration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_default_animation_duration" name="sdf_theme_default_animation_duration" class="form-control"><?php echo $this->createAnimationDurationPicker( $this->_currentSettings['animations']['default_animation_duration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		
		<div class="bs-callout bs-callout-grey">Featured Image</div>
                             
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Animations', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['animations']['feat_img_animate'] ),1, "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['animations']['feat_img_animate'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="sdf_theme_feat_img_animate" value="<?php echo $this->_currentSettings['animations']['feat_img_animate']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Turn on animations on Featured Images. Turning Off will disable All Featured Image animations.', SDF_TXT_DOMAIN) ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_feat_img_animation_type" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Animation Type', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_feat_img_animation_type" name="sdf_theme_feat_img_animation_type" class="form-control"><?php echo $this->createAnimationPicker( $this->_currentSettings['animations']['feat_img_animation_type']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_feat_img_animation_duration" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Animation Duration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_feat_img_animation_duration" name="sdf_theme_feat_img_animation_duration" class="form-control"><?php echo $this->createAnimationDurationPicker( $this->_currentSettings['animations']['feat_img_animation_duration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
                        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsefti" href="#collapsefti">
       <?php _e('Favicon and Touch Icons', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsefti" class="panel-collapse collapse">
      <div class="panel-body">
                             
        <div class="form-group">
		<label for="sdf_theme_favicon" class="col-sm-3 col-md-3 control-label"><?php _e('Favicon', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_favicon" name="sdf_theme_favicon" type="text" class="wpu-image form-control" size="40" value="<?php echo $this->_currentSettings['favtouch_icons']['favicon']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Favicon image should be 16px x 16px</div>
		</div>

        <div class="form-group">
		<label for="sdf_theme_apple_touch_icon_57" class="col-sm-3 col-md-3 control-label"><?php _e('Apple Touch Icon 57', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_apple_touch_icon_57" name="sdf_theme_apple_touch_icon_57" type="text" class="wpu-image form-control" size="40" value="<?php echo $this->_currentSettings['favtouch_icons']['apple_touch_icon_57']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Apple Touch Icon image should be 57px x 57px</div>
		</div>

        <div class="form-group">
		<label for="sdf_theme_apple_touch_icon_72" class="col-sm-3 col-md-3 control-label"><?php _e('Apple Touch Icon 72', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_apple_touch_icon_72" name="sdf_theme_apple_touch_icon_72" type="text" class="wpu-image form-control" size="40" value="<?php echo $this->_currentSettings['favtouch_icons']['apple_touch_icon_72']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Apple Touch Icon image should be 72px x 72px</div>
		</div>

        <div class="form-group">
		<label for="sdf_theme_apple_touch_icon_114" class="col-sm-3 col-md-3 control-label"><?php _e('Apple Touch Icon 114', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_apple_touch_icon_114" name="sdf_theme_apple_touch_icon_114" type="text" class="wpu-image form-control" size="40" value="<?php echo $this->_currentSettings['favtouch_icons']['apple_touch_icon_114']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Apple Touch Icon image should be 114px x 114px</div>
		</div>

        <div class="form-group">
		<label for="sdf_theme_apple_touch_icon_144" class="col-sm-3 col-md-3 control-label"><?php _e('Apple Touch Icon 144', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_apple_touch_icon_144" name="sdf_theme_apple_touch_icon_144" type="text" class="wpu-image form-control" size="40" value="<?php echo $this->_currentSettings['favtouch_icons']['apple_touch_icon_144']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Apple Touch Icon image should be 144px x 144px</div>
		</div>		
		
      </div>
    </div>
  </div>
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapse_socmed" href="#collapse_socmed">
       <?php _e('Social Media Follow Buttons', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapse_socmed" class="panel-collapse collapse">
      <div class="panel-body"> 
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('These buttons serve to promote your business\' presence on various social networks and help you generate fans/followers for those particular accounts. By placing these buttons on your business\' website, you can help to create visibility for your social media accounts and easily extend your reach there.', SDF_TXT_DOMAIN); ?></p>
			</div>
			
		
			<?php foreach ( $this->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
				
				$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ); ?>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e($soc_name, SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_social_media_<?php echo $id; ?>' class="form-control" id='sdf_theme_social_media_<?php echo $id; ?>' value='<?php echo $this->_currentSettings['social_media']['social_media_'.$id]; ?>' size='120' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Your '.$soc_name.' link.', SDF_TXT_DOMAIN) ?></div>
				</div>
			<?php } ?>
		
    </div>
  </div> 
</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapse_soc_share" href="#collapse_soc_share">
			 <?php _e('Social Media Share Buttons', SDF_TXT_DOMAIN); ?>
			</a>
		</div>
		<div id="collapse_soc_share" class="panel-collapse collapse">
			<div class="panel-body"> 
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('These buttons enable your website visitors and content viewers to easily share your content with their social media connections and networks.', SDF_TXT_DOMAIN); ?></p>
			</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Social Share Buttons', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['social_media']['toggle_social_share'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['social_media']['toggle_social_share'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_social_share" value="<?php echo $this->_currentSettings['social_media']['toggle_social_share']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Use this global setting to turn Social Media Follow Buttons off or on site wide (default setting is on).', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Social Share Buttons Position', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['social_share_position'],'below', "btn-sdf-grey" )?>" value="below">Below Content</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['social_share_position'],'above', "btn-sdf-grey" )?>" value="above">Above Content</button>
					</div>
					<input type="hidden" name="sdf_theme_social_share_position" value="<?php echo $this->_currentSettings['social_media']['social_share_position']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Use this global setting to turn Social Media Follow Buttons off or on site wide (default setting is on).', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_social_share_animation_type" class="col-sm-4 col-md-4 control-label"><?php _e('Entrance Animation Type', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select id="sdf_theme_social_share_animation_type" name="sdf_theme_social_share_animation_type" class="form-control"><?php echo $this->createAnimationPicker( $this->_currentSettings['social_media']['social_share_animation_type']); ?></select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_social_share_animation_duration" class="col-sm-4 col-md-4 control-label"><?php _e('Entrance Animation Duration', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select id="sdf_theme_social_share_animation_duration" name="sdf_theme_social_share_animation_duration" class="form-control"><?php echo $this->createAnimationDurationPicker( $this->_currentSettings['social_media']['social_share_animation_duration']); ?></select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Title', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_social_share_title' class="form-control" id='sdf_theme_social_share_title' value='<?php echo $this->_currentSettings['social_media']['social_share_title']; ?>' size='120' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Twitter Handle', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_social_share_twitter_handle' class="form-control" id='sdf_theme_social_share_twitter_handle' value='<?php echo $this->_currentSettings['social_media']['social_share_twitter_handle']; ?>' size='120' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('needed for Twitter Share link creation.', SDF_TXT_DOMAIN) ?></div>
				</div>
				
				<div class="bs-callout bs-callout-grey">
				<p><?php _e('Enable/Disable Each Share Button', SDF_TXT_DOMAIN); ?></p>
				</div>
			
				<?php foreach ( $this->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
					
					$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ); ?>
					
					<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e($soc_name.' Share Button', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['social_media']['toggle_'.$id.'_share'] ),1, "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['social_media']['toggle_'.$id.'_share'] ),0, "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_<?php echo $id; ?>_share" value="<?php echo $this->_currentSettings['social_media']['toggle_'.$id.'_share']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Use this global setting to turn '.$soc_name.' Share Button off or on site wide.', SDF_TXT_DOMAIN) ?></div>
				</div>
				<?php } ?>			
				
			</div>
		</div> 
	</div>
	<?php if ( is_woocommerce_active() ) { ?>
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsewoo" href="#collapsewoo">
       <?php _e('WooCommerce', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsewoo" class="panel-collapse collapse">
      <div class="panel-body">
				
				<div class="bs-callout bs-callout-grey">
					<p><?php _e('Change number of columns/products for shop page', SDF_TXT_DOMAIN); ?></p>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_woo_columns" class="col-sm-4 col-md-4 control-label"><?php _e('Number of columns (products per row) on shop page', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select name="sdf_theme_woo_columns" class="form-control" id="sdf_theme_woo_columns">
						<option value="1"<?php selected($this->_currentSettings['misc']['woo_columns'], "1"); ?>>1</option>
						<option value="2"<?php selected($this->_currentSettings['misc']['woo_columns'], "2"); ?>>2</option>
						<option value="3"<?php selected($this->_currentSettings['misc']['woo_columns'], "3"); ?>>3</option>
						<option value="4"<?php selected($this->_currentSettings['misc']['woo_columns'], "4"); ?>>4</option>
						<option value="6"<?php selected($this->_currentSettings['misc']['woo_columns'], "6"); ?>>6</option>
					</select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_woo_products_per_page" class="col-sm-4 col-md-4 control-label"><?php _e('Number of products per page', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_woo_products_per_page' class="form-control" id='sdf_theme_woo_products_per_page' value='<?php echo $this->_currentSettings['misc']['woo_products_per_page']; ?>' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="bs-callout bs-callout-grey">
					<p><?php _e('Change number of related columns/products for single product page', SDF_TXT_DOMAIN); ?></p>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_woo_related_columns" class="col-sm-4 col-md-4 control-label"><?php _e('Number of columns (products per row)', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<select name="sdf_theme_woo_related_columns" class="form-control" id="sdf_theme_woo_related_columns">
						<option value="1"<?php selected($this->_currentSettings['misc']['woo_related_columns'], "1"); ?>>1</option>
						<option value="2"<?php selected($this->_currentSettings['misc']['woo_related_columns'], "2"); ?>>2</option>
						<option value="3"<?php selected($this->_currentSettings['misc']['woo_related_columns'], "3"); ?>>3</option>
						<option value="4"<?php selected($this->_currentSettings['misc']['woo_related_columns'], "4"); ?>>4</option>
						<option value="6"<?php selected($this->_currentSettings['misc']['woo_related_columns'], "6"); ?>>6</option>
					</select>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_woo_related_products_per_page" class="col-sm-4 col-md-4 control-label"><?php _e('Number of products', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_woo_related_products_per_page' class="form-control" id='sdf_theme_woo_related_products_per_page' value='<?php echo $this->_currentSettings['misc']['woo_related_products_per_page']; ?>' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
       
      </div>
    </div>
  </div>
	<?php } ?>
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapseadminbar" href="#collapseadminbar">
       <?php _e('Admin Toolbar', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapseadminbar" class="panel-collapse collapse">
      <div class="panel-body">

				<div class="form-group">
					<label for="sdf_theme_toggle_admin_bar" class="col-sm-4 col-md-4 control-label"><?php _e('Show/Hide Toolbar when viewing site', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_admin_bar'] ),1, "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_admin_bar'] ),0, "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_admin_bar" value="<?php echo $this->_currentSettings['misc']['toggle_admin_bar']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Setting it to "Show" will keep default Toolbar functionality.', SDF_TXT_DOMAIN) ?></div>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_design_toolbar_menu" class="col-sm-4 col-md-4 control-label"><?php _e('Show/Hide "Design" Menu in Admin Toolbar', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_design_toolbar_menu'] ),1, "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_design_toolbar_menu'] ),0, "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_design_toolbar_menu" value="<?php echo $this->_currentSettings['misc']['toggle_design_toolbar_menu']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
       
      </div>
    </div>
  </div>
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsehtaccess" href="#collapsehtaccess">
       <?php _e('HTML5 Boilerplate\'s and 5G Blacklist\'s .htaccess Addition', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsehtaccess" class="panel-collapse collapse">
      <div class="panel-body">
				
				<div class="bs-callout bs-callout-grey">
					<h4><?php _e('Notice:', SDF_TXT_DOMAIN); ?></h4>
					<p><?php _e('1. This feature is not working on multisite installations <br /> 2. After changing any of these 2 options "Save changes" <br /> 3. Go to', SDF_TXT_DOMAIN) ?> <a href="<?php echo admin_url( 'options-permalink.php' ); ?>" target="_blank">Permalinks</a> <?php _e('page and resave it too as it will write these changes to .htaccess file.', SDF_TXT_DOMAIN) ?></p>
				</div>

				<div class="form-group">
					<label for="sdf_theme_toggle_h5bp_htaccess" class="col-sm-4 col-md-4 control-label"><?php _e('HTML5 Boilerplate\'s .htaccess', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_h5bp_htaccess'] ),1, "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_h5bp_htaccess'] ),0, "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_h5bp_htaccess" value="<?php echo $this->_currentSettings['misc']['toggle_h5bp_htaccess']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Setting it to "Show" will apppend the content recommended by <a href="http://html5boilerplate.com/" target="_blank">HTML5 Boilerplate</a> to your .htaccess. Note that you need permalinks enabled to get this.', SDF_TXT_DOMAIN) ?></div>
				</div>
       
				<div class="form-group">
					<label for="sdf_theme_toggle_5g_htaccess" class="col-sm-4 col-md-4 control-label"><?php _e('5G Blacklist\'s .htaccess', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_5g_htaccess'] ),1, "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_5g_htaccess'] ),0, "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_toggle_5g_htaccess" value="<?php echo $this->_currentSettings['misc']['toggle_5g_htaccess']  ; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Setting it to "Show" will apppend the <a href="http://perishablepress.com/5g-blacklist-2013/" target="_blank">5G Blacklist\'s</a> content to your .htaccess which helps reduce the number of malicious URL requests that hit your website. Note that you need permalinks enabled to get this.', SDF_TXT_DOMAIN) ?></div>
				</div>
       
      </div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsett1" href="#collapsett1">
       <?php _e('Placeholders', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsett1" class="panel-collapse collapse">
      <div class="panel-body">             		
        
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Theme Placeholders', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
  <button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_placeholders']),1, "btn-sdf-grey")?>" value="1">On</button>
  <button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_placeholders']),0, "btn-sdf-grey")?>" value="0">Off</button>
</div>
<input type="hidden" name="sdf_toggle_placeholder" value="<?php echo $this->_currentSettings['misc']['toggle_placeholders']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The <strong>SEO Design Framework</strong> comes with visual placeholders for webmasters to view where the widget and / or banner ads will appear (depending on your layout choice). The theme default has the placeholders active. To deactivate or reactivate. simply turn placeholders on or off here in the main global settings and click the <strong>update settings button</strong> to save changes.</div>
		</div>

                        
      </div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsemaintenance" href="#collapsemaintenance">
       <?php _e('Maintenance Mode', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsemaintenance" class="panel-collapse collapse">
      <div class="panel-body">             		
        
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Theme Maintenance Mode', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_maintenance_mode']),1, "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['misc']['toggle_maintenance_mode']),0, "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="sdf_toggle_maintenance_mode" value="<?php echo $this->_currentSettings['misc']['toggle_maintenance_mode']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enable/Disable maintenance mode on your site</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_maintenance_page" class="col-sm-4 col-md-4 control-label"><?php _e('Theme Maintenance Page', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<?php 
			$sdf_pages = array();
			$pages = get_posts(array(
					'post_type' => 'page'
			));
			foreach($pages as $page) {
					$sdf_pages[$page->post_title] = $page->ID;
			}
			?>
			<select id="sdf_theme_maintenance_page" name="sdf_theme_maintenance_page" class="form-control"><?php echo $this->createSelectBox( $sdf_pages, $this->_currentSettings['misc']['theme_maintenance_page'] ); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Choose Maintenance Page.', SDF_TXT_DOMAIN); ?></div>
		</div>
                        
      </div>
    </div>
  </div>
	
    <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsett5" href="#collapsett5">
       <?php _e('Export/Import/Reset Theme Options', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsett5" class="panel-collapse collapse">
      <div class="panel-body"> 
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose to Import, Export or Reset Theme Options', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-3">
				<div id="theme_options_io_option" class="btn-group sdf_toggle">
				  <button type="button" class="btn btn-sm btn-sdf-grey active" value="1">Import</button>
				  <button type="button" class="btn btn-sm btn-default" value="0">Export</button>
				  <button type="button" class="btn btn-sm btn-default" value="2">Reset</button>
				</div>
				<input type="hidden" name="sdf_theme_options_io" value="1" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group json-import">
				<label for="sfd_theme_options_json_import" class="col-sm-4 col-md-4 control-label"><?php _e('Paste Options Code Here', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-5">
				<textarea name="sfd_theme_options_json_import" id="sfd_theme_options_json_import" class="form-control" cols="45" rows="7"></textarea>
				</div>
				<div class="col-sm-4 col-md-3 help-text"></div>
			</div>
			
			<div class="form-group json-import">
				<label for="sdf_import_json_options" class="col-sm-4 col-md-4 control-label"></label>
				<div class="col-sm-4 col-md-5">
				<input class="btn btn-sm btn-custom pull-right" type="submit" value="Import Theme Options" name="sdf_import_json_options">
				</div>
				<div class="col-sm-4 col-md-3 help-text"></div>
			</div>
			
			<div class="form-group json-export" style="display:none;">
				<label for="sfd_theme_options_json_export" class="col-sm-4 col-md-4 control-label"><?php _e('This is Options Export Code', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-5">
				<textarea name="" id="sfd_theme_options_json_export" class="form-control" cols="45" rows="7"><?php echo $this->get_exported_options(); ?></textarea>
				</div>
				<div class="col-sm-4 col-md-3 help-text">You can copy this code and save these theme settings or download it in export file below</div>
			</div>
			
			<div class="form-group json-export" style="display:none;">
				<label for="sfd_theme_options_json_export" class="col-sm-4 col-md-4 control-label"><?php _e('Download Options Export File', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-5">
				<?php $this->create_export_download_link(true); ?>
				</div>
				<div class="col-sm-4 col-md-3 help-text"></div>
			</div>
			
			<div class="form-group json-reset" style="display:none;">
				<label for="sdf_import_json_options" class="col-sm-4 col-md-4 control-label">Are you sure you want to reset all theme options?</label>
				<div class="col-sm-4 col-md-5">
				<input class="btn btn-sm btn-danger pull-left" type="submit" value="Yes, Reset Theme Options" name="sdf_reset_json_options">
				</div>
				<div class="col-sm-4 col-md-3 help-text"></div>
			</div>
		
      </div>
		</div> 
		</div>
    <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsett6" href="#collapsett6">
       <?php _e('Automatic Theme Updates', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsett6" class="panel-collapse collapse">
      <div class="panel-body">
			
				<div class="form-group">
					<label for="sdf_theme_auto_update_username" class="col-sm-4 col-md-4 control-label"><?php _e('SDF Username', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_auto_update_username' class="form-control" id='sdf_theme_auto_update_username' value='<?php echo $this->_currentSettings['misc']['auto_update_username']; ?>' size='120' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Username Associated with SEO Design Framework Members Area.', SDF_TXT_DOMAIN) ?></div>
				</div>
			
				<div class="form-group">
					<label for="sdf_theme_auto_update_email" class="col-sm-4 col-md-4 control-label"><?php _e('SDF Email', SDF_TXT_DOMAIN) ?></label>
					<div class="col-sm-4 col-md-3">
					<input type='text' name='sdf_theme_auto_update_email' class="form-control" id='sdf_theme_auto_update_email' value='<?php echo $this->_currentSettings['misc']['auto_update_email']; ?>' size='120' />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Email Associated with SEO Design Framework Members Area.', SDF_TXT_DOMAIN) ?></div>
				</div>
		
      </div>
		</div> 
		</div>
</div>
</div>   
                       
		<div class="row">
		<div class="col-md-4">
			<button class="btn btn-sdf-grey btn-sm" type="submit" name="setting-submit" id="setting-submit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
		</div>
		</div>

	<?php wp_nonce_field('wpu-theme-update-global-settings'); ?>
	<input type="hidden" name="sdf_panel_collapse_shown" value="" />
 </form>

</div>
</div><!-- end #sdf-settings -->
<?php endif; ?>
<?php if ($current_page == 'sdf-header') : ?>
<div id="sdf-header" class="<?php if($current_page == 'sdf-header'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">
          
<form name="sdf_theme_logo_settings" id="sdf_theme_logo_settings" class="form-horizontal" method="post" action="#sdf-header"  enctype="multipart/form-data">
<div id="sdf_theme_logo_page" class="wrap">
<input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
<div class="row titlegroup">
<div class="col-sm-8 col-md-9">  
	<div class="wpu-header-icon"></div><h3 class="pull-left"><?php _e('Header Settings', SDF_TXT_DOMAIN); ?><small class="expand-hide"><a href="javascript:" class="wpu-open-close">+ Expand All</a></small></h3>
</div>  
<div class="col-sm-4 col-md-3">
	<span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="header-submit" id="header-submit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" /></span>	 
</div> 
</div>
<div class="wpu-group">
<div class="panel-group" id="wpuaccordion">
<div class="panel panel-default">
<div class="panel-heading">
<a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collapseOne" href="#collapseOne">Header Structure</a>
</div>
<div id="collapseOne" class="panel-collapse collapse">
<div class="panel-body">

	<div class="form-group">
		<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Rearange Header Blocks<br /> (Logo, Navigation, Slider, Widget Blocks)<br /> or Drag Blocks to Inactive Zone Below', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-9 col-md-9">
			<input type="hidden" name="sdf_theme_header_order" id="sdf_theme_header_order" value="<?php if($this->_currentSettings['header']['header_order']) echo implode(',', $this->_currentSettings['header']['header_order']); ?>" />
			
			<div id="sdf-block-header-active" class="sdf-block-header clearfix ui-sortable connectedHeaderList">
				<?php 
				// Header Sort Order
				if(is_array($this->_currentSettings['header']['header_order']) && !empty($this->_currentSettings['header']['header_order']))
				{
					// active boxes
					if(is_array($this->_currentSettings['header']['box_data']) && !empty($this->_currentSettings['header']['box_data']))
					{
						// sizes
						$box_data = $this->_currentSettings['header']['box_data'];

						foreach($this->_currentSettings['header']['header_order'] as $box_id)
						{
							if (isset($box_data[$box_id])) {
								echo '<div id="'.$box_id.'" class="block-builder-box '.$this->_smWidths[$box_data[$box_id]].'"><div class="block-builder-box-panel">
								<input class="block-builder-box-id" type="hidden" value="'.$box_id.'" name="block-builder-box-id[]">
								<input class="block-builder-box-size" type="hidden" value="'.$box_data[$box_id].'" name="block-builder-box-size[]">
								<div class="block-builder-box-size">
									<a class="block-builder-box-btn block-builder-box-size-dec" href="javascript:void(0);">-</a>
									<a class="block-builder-box-btn block-builder-box-size-inc" href="javascript:void(0);">+</a>
									<span class="block-builder-box-width">'.$box_data[$box_id].'</span>
								</div>
								<span class="block-builder-box-label">'.$this->_boxNames[$box_id]['name'].'</span>
								</div></div>';
							}
						}
					
						
					}
				}
				?>
			</div>
	</div>
	
	<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Inactive Zone', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-9 col-md-9">		
			<input type="hidden" name="sdf_theme_header_order_inactive" id="sdf_theme_header_order_inactive" value="" />
			
			<div id="sdf-block-header-inactive" class="sdf-block-header clearfix ui-sortable connectedHeaderList">
			
				<?php 
				// inactive boxes
				if(is_array($this->_currentSettings['header']['header_order']) && !empty($this->_currentSettings['header']['header_order']))
				{
					if(is_array($this->_currentSettings['header']['box_data']) && !empty($this->_currentSettings['header']['box_data']))
					{
						foreach($box_data as $box_id => $box_size)
						{
							
							if (!in_array($box_id, $this->_currentSettings['header']['header_order']))
							{
							echo '<div id="'.$box_id.'" class="block-builder-box col-md-4"><div class="block-builder-box-panel">
							<input class="block-builder-box-id" type="hidden" value="'.$box_id.'" name="block-builder-box-id[]">
							<input class="block-builder-box-size" type="hidden" value="1/3" name="block-builder-box-size[]">
							<div class="block-builder-box-size">
								<a class="block-builder-box-btn block-builder-box-size-dec" href="javascript:void(0);">-</a>
								<a class="block-builder-box-btn block-builder-box-size-inc" href="javascript:void(0);">+</a>
								<span class="block-builder-box-width">1/3</span>
							</div>
							<span class="block-builder-box-label">'.$this->_boxNames[$box_id]['name'].'</span>
							</div></div>';
							}
						}
					}
				}
				?>
			
			</div>
			
		</div>
	</div>
	
	<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Additional Block Classes', SDF_TXT_DOMAIN); ?></h4>
		<p><?php _e('You can use custom Classes or Bootstrap Grid Classes:', SDF_TXT_DOMAIN) ?> <a href="http://getbootstrap.com/css/#grid" target="_blank">Bootstrap Grid system</a></p>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_5" class="col-sm-4 col-md-4 control-label"><?php _e('Logo', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_5" class="form-control" id="sdf_theme_header_box_data_classes_header_block_5" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_5']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_6" class="col-sm-4 col-md-4 control-label"><?php _e('Navigation', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_6" class="form-control" id="sdf_theme_header_box_data_classes_header_block_6" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_6']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_8" class="col-sm-4 col-md-4 control-label"><?php _e('Secondary Navigation', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_8" class="form-control" id="sdf_theme_header_box_data_classes_header_block_8" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_8']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_1" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 1', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_1" class="form-control" id="sdf_theme_header_box_data_classes_header_block_1" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_1']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_2" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 2', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_2" class="form-control" id="sdf_theme_header_box_data_classes_header_block_2" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_2']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_3" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 3', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_3" class="form-control" id="sdf_theme_header_box_data_classes_header_block_3" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_3']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_4" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 4', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_4" class="form-control" id="sdf_theme_header_box_data_classes_header_block_4" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_4']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
	<div class="form-group">
		<label for="sdf_theme_header_box_data_classes_header_block_7" class="col-sm-4 col-md-4 control-label"><?php _e('Slider Block', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-3">
		<input type="text" name="sdf_theme_header_box_data_classes_header_block_7" class="form-control" id="sdf_theme_header_box_data_classes_header_block_7" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_7']; ?>" />
		</div>
		<div class="col-sm-4 col-md-5 help-text"></div>
	</div>
	
</div>
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
<a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collapseFour" href="#collapseFour">
<?php _e('Logo Settings', SDF_TXT_DOMAIN); ?>
</a>
</div>
<div id="collapseFour" class="panel-collapse collapse">
<div class="panel-body">
		 		
	<div class="form-group">
		<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Custom Logo', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div id="logo-option" class="btn-group sdf_toggle">
			  <button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['logo']['logo_type'] ,'logo-file-path', "btn-sdf-grey")?>" value="logo-file-path">Image</button>
			  <button type="button" class="btn btn-sm<?php echo checkButton(  (string)$this->_currentSettings['logo']['logo_type'] ,'off', "btn-sdf-grey")?>" value="off">Default Text</button>
			</div>
			<input type="hidden" name="logo-options" value="<?php echo $this->_currentSettings['logo']['logo_type']; ?>" />
		</div>
		<div class="col-sm-5 col-md-5 help-text">Upload custom Logo or Use default Text logo.</div>
	</div> 		
	<div class="form-group">
		<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Tagline', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div id="logo-option" class="btn-group sdf_toggle">
			  <button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['logo']['toggle_tagline'] ,'on', "btn-sdf-grey")?>" value="on">On</button>
			  <button type="button" class="btn btn-sm<?php echo checkButton(  (string)$this->_currentSettings['logo']['toggle_tagline'] ,'off', "btn-sdf-grey")?>" value="off">Off</button>
			</div>
			<input type="hidden" name="toggle_tagline" value="<?php echo $this->_currentSettings['logo']['toggle_tagline']; ?>" />
		</div>
		<div class="col-sm-5 col-md-5 help-text">Turn Tagline On/Off</div>
	</div>
	<div class="form-group logo-path">
		<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Logo Image', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4">
			<div class="input-group">
				<input id="sdf_theme_logo_text" name="sdf_theme_logo_text" type="text" class="wpu-image form-control" size="40" value="<?php if($this->_currentSettings['logo']['logo_type'] == 'logo-file-path') echo $this->_currentSettings['logo']['logo_path']['url']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
		</div>
		<div class="col-sm-5 col-md-5 help-text">Enter an URL or upload an image for the logo.</div>
	</div>
	<div class="form-group">
		<label for="" class="col-sm-3 col-md-3 control-label"></label>	
		

			<div class="col-sm-4 col-md-4 logo-path">
			<div class="">
			<div class="well" style="background:#fff; padding:25px;">
			<img src="<?php echo $this->_currentSettings['logo']['logo_path']['url']; ?>" class="img-responsive" />
			</div>
			</div>
			<div class="">
			<div class="well" style="background:#000; padding:25px;">
			<img src="<?php echo $this->_currentSettings['logo']['logo_path']['url']; ?>" class="img-responsive" />
			</div>
			</div>
			</div>
			
			
			<div class="col-sm-4 col-md-4 logo-text">
			<h1 class="logo"><?php echo get_bloginfo( 'name' ); ?></h1>
			</div>
			
		<div class="col-sm-5 col-md-5 help-text"><?php _e('Current Logo', SDF_TXT_DOMAIN); ?></div>
	</div>

	</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading">
<a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collapseThree" href="#collapseThree">
<?php _e('Additional HTML/CSS', SDF_TXT_DOMAIN); ?>
</a>
</div>
<div id="collapseThree" class="panel-collapse collapse">
<div class="panel-body">
   
	<div class="form-group">
		<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('HTML to Place in Head Tag', SDF_TXT_DOMAIN) ?></label>
		<div class="col-sm-4 col-md-5">
		<textarea name='sdf_theme_head_js' class="form-control" id='sdf_theme_head_js' cols="45" rows="7"><?php echo stripslashes( $this->_currentSettings['misc']['custom_js_header'] ); ?></textarea>
		</div>
		<div class="col-sm-4 col-md-3 help-text"><?php _e('(Place Styles, Scripts, or any HTML that needs to go into the &lt;head&gt; tag)', SDF_TXT_DOMAIN); ?></div>
	</div>

</div>
</div>
</div>
</div>
</div>




		   
	<div class="row">
	<div class="col-md-4">
		<button class="btn btn-sdf-grey btn-sm" type="submit" if="header-submit" name="header-submit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
	</div>
	</div>

	<?php wp_nonce_field('wpu-theme-update-logo-settings'); ?>
	<input type="hidden" name="sdf_panel_collapse_shown" value="" />
</div>
</form>

</div><!-- end #sdf-header -->
<?php endif; ?>

<?php if ($current_page == 'sdf-layout') : ?>				
<div id="sdf-layout" class="<?php if($current_page == 'sdf-layout'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">
	
<form name="sdf_theme_layout_settings" id="sdf_theme_layout_settings" class="form-horizontal" method="post" action="#sdf-layout">
<div id="sdf_theme_layout_page" class="wrap">
              <input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
<div class="row titlegroup">
<div class="col-sm-8 col-md-9">
<div class="wpu-layout-icon"></div><h3 class="pull-left"><?php _e('Layout Settings', SDF_TXT_DOMAIN); ?></h3> 
</div>  
<div class="col-sm-4 col-md-3">
<span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="layout-submit" id="layout-submit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" /></span>  
</div>
</div>

                    <div class="wpu-group">
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 1: <span class="step-text">Page Layout</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Layout', SDF_TXT_DOMAIN); ?></label>
							<div class="col-sm-4 col-md-3">
							<select name="sdf_theme_page_width" class="form-control" id="sdf_theme_page_width">
								<option value="wide"<?php if($this->_currentSettings['layout']['page_width'] == "wide") echo ' selected="selected"'; ?>>Wide</option>
								<option value="boxed"<?php if($this->_currentSettings['layout']['page_width'] == "boxed") echo ' selected="selected"'; ?>>Boxed</option>
							</select>
							</div>
							<div class="col-sm-4 col-md-5 help-text">A fixed website layout has a wrapper that is a fixed width. The layout style will be applied globally, throughout the entire site unless you set custom per page layout in page/post options.</div>
						</div>
					  </div>
					</div>
					
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 2: <span class="step-text">Columns</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<div class="wpu-page-layouts">
							<ul class="sdf-layouts col-md-12">
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_3" value="col_3"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_3') echo " checked"; ?> />
									<label for="col_3"><span class="lay_title">Both</span><span class="lay_subtitle">Sidebars</span><span class="layout_col_3"></span></label>
								</li>    
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_right_2" value="col_right_2"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_right_2') echo " checked"; ?>  />
									<label for="col_right_2"><span class="lay_title">Right</span><span class="lay_subtitle">Sidebar</span><span class="layout_col_right_2"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_left_2" value="col_left_2"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_left_2') echo " checked"; ?>  />
								<label for="col_left_2"><span class="lay_title">Left</span><span class="lay_subtitle">Sidebar</span><span class="layout_col_left_2"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="ssc_left" value="ssc_left"<?php if($this->_currentSettings['layout']['theme_layout'] == 'ssc_left') echo " checked"; ?>  />
								<label for="ssc_left"><span class="lay_title">Both (L)</span><span class="lay_subtitle">Sidebars Left</span><span class="layout_ssc_left"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="css_right" value="css_right"<?php if($this->_currentSettings['layout']['theme_layout'] == 'css_right') echo " checked"; ?>  />
								<label for="css_right"><span class="lay_title">Both (R)</span><span class="lay_subtitle">Sidebars Right</span><span class="layout_css_right"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="no_col" value="no_col"<?php if($this->_currentSettings['layout']['theme_layout'] == 'no_col') echo " checked"; ?>  />
								<label for="no_col"><span class="lay_title">Single</span><span class="lay_subtitle">No Sidebars</span><span class="layout_no_col"></span></label>
								</li>
							   <li id="wpu-custom-layout-li"><input type="radio" class="sdf_page_layout" id="sdf_custom_layout" name="sdf_theme_layout_page" value="custom"<?php if($this->_currentSettings['layout']['theme_layout'] == 'custom') echo " checked"; ?>  />
								<label for="sdf_custom_layout"><span class="lay_title">Custom</span><span class="lay_subtitle">Columns</span><span class="layout_sdf_custom_layout"></span></label>
								</li>
						   </ul>
						   </div>
						</div>
					  </div>
					</div>
					
					<div id="wpu-step3" class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 3: <span class="step-text">Sidebar Widths</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sidebar Width', SDF_TXT_DOMAIN); ?></label>
							<div id="wpu-sidebar-width" class="col-sm-4 col-md-3">
							<select name="sdf_page_sidebars_width" class="form-control" id="sdf_page_sidebars_width">
								<option value="6"<?php if($this->_currentSettings['layout']['page_sidebars'] == "6") echo ' selected="selected"'; ?>>One Half</option>
								<option value="5"<?php if($this->_currentSettings['layout']['page_sidebars'] == "5") echo ' selected="selected"'; ?>>Five Twelfth</option>
								<option value="4"<?php if($this->_currentSettings['layout']['page_sidebars'] == "4") echo ' selected="selected"'; ?>>One Third</option>
								<option value="3"<?php if($this->_currentSettings['layout']['page_sidebars'] == "3") echo ' selected="selected"'; ?>>One Fourth</option>
								<option value="2"<?php if($this->_currentSettings['layout']['page_sidebars'] == "2") echo ' selected="selected"'; ?>>One Sixth</option>
								<option value="1"<?php if($this->_currentSettings['layout']['page_sidebars'] == "1") echo ' selected="selected"'; ?>>One Twelfth</option>
							</select>
							</div>
                            <div id="custom-layout" style="display:none" class="col-sm-8 col-md-8">
                                  <script type="text/javascript">
                   					jQuery.noConflict();
									jQuery(document).ready(function($){
											try{
													 
										$( "#slider-layout" ).slider({
												range: true,
												min: 0,
												max: 1170,
												values: [<?php if($this->_currentSettings['layout']['custom_left'] && !($this->_currentSettings['layout']['custom_left'] === 0)):echo (int)$this->_currentSettings['layout']['custom_left'];  elseif($this->_currentSettings['layout']['custom_left'] === 0): echo '0'; else: echo '200'; endif; ?>,<?php if($this->_currentSettings['layout']['custom_right'] && !($this->_currentSettings['layout']['custom_right'] === 0)): echo (1170 - (int)$this->_currentSettings['layout']['custom_right'] ); elseif($this->_currentSettings['layout']['custom_right'] === 0): echo '0'; else: echo '740'; endif; ?>],
												slide: function( event, ui ) {
													$( "#sdf_theme_custom_left" ).val( ui.values[ 0 ]+"px" );
													$( "#sdf_theme_custom_center" ).val(1170  -  (ui.values[ 0 ] +(1170 - ui.values[ 1 ]))+"px" );
													$( "#sdf_theme_custom_right" ).val( 1170 - ui.values[ 1 ]+"px"  );
												}
											});
											$( "#sdf_theme_custom_left" ).val( ui.values[ 0 ]+"px"  );
													$( "#sdf_theme_custom_center" ).val( 1170 -  (ui.values[ 0 ] + (1170 - ui.values[ 1 ]))+"px"  );
													$( "#sdf_theme_custom_right" ).val(  1170 - ui.values[ 1 ] +"px" );
										}catch(err){}
										})
											</script>
									<label for="sdf_theme_layoutsize"><?php _e('Custom Layout:', SDF_TXT_DOMAIN) ?></label> 
									<ul id="custom-px-box">
									<li><?php  _e('Left Column', SDF_TXT_DOMAIN); ?><br />
									<input type="text" id="sdf_theme_custom_left" name="sdf_theme_custom_left" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if($this->_currentSettings['layout']['custom_left']): echo $this->_currentSettings['layout']['custom_left']; else: echo '200px'; endif; ?>" /></li>
									<li><?php  _e('Center Column', SDF_TXT_DOMAIN); ?><br />
									<input type="text" id="sdf_theme_custom_center" name="sdf_theme_custom_center" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if($this->_currentSettings['layout']['custom_center']): echo  $this->_currentSettings['layout']['custom_center']; else: echo '540px'; endif; ?>" /></li>
									<li><?php  _e('Right Column', SDF_TXT_DOMAIN); ?><br />
									<input type="text" id="sdf_theme_custom_right" name="sdf_theme_custom_right" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if($this->_currentSettings['layout']['custom_right']): echo $this->_currentSettings['layout']['custom_right']; else: echo '200px'; endif; ?>" /></li></ul>
									<div id="slider-layout"></div>
            				 </div>
							 
						</div>
					  </div>
					</div>
				    
                              
                       
                   </div>
				   
	<div class="row">
	<div class="col-md-4">
		<button class="btn btn-sdf-grey btn-sm" type="submit" name="layout-submit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
	</div>
	</div>

	<?php wp_nonce_field('wpu-theme-update-layout-settings'); ?>
	<input type="hidden" name="sdf_panel_collapse_shown" value="" />
</div>
</form>
            

</div><!-- end #sdf-layout -->
<?php endif; ?>

<?php if ($current_page == 'sdf-shortcode') : ?>	
<div id="sdf-shortcode" class="<?php if($current_page == 'sdf-shortcode'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">

<form name="sdf_theme_shortcode_settings" id="sdf_theme_shortcode_settings" class="form-horizontal" method="post" action="#sdf-shortcode">
<div id="sdf_theme_shortcode_page" class="wrap">
	<input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
     
            <div class="row titlegroup">
				<div class="col-sm-8 col-md-9">	
					<div class="wpu-short-code-icon"></div><h3 class="pull-left"><?php _e('Shortcodes', SDF_TXT_DOMAIN); ?><small class="expand-hide"><a href="javascript:" class="wpu-open-close">+ Expand All</a></small></h3> <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="bottom" data-html="true" title='<p>The dynamic Shortcode manager allows you to add html for things such as contact forms, local map data, text or other html or subroutines and then execute the function locally in pages, posts or widgets dynamically.</p><p>Simply enter the text or html>>>enter the name of the Shortcode in this example, we will use the word test in the sdf_short id=" "] region. Next click the update changes button to save your work.</p><p>To use the Shortcode, place it on the page, post or widget, for example [sdf_short id="test"] and the feature or function will appear where the Shortcode is inserted. You can create as many short codes as you like by clicking the [+] add or [-] remove buttons respectively.</p>'></i> 
				</div>   
				<div class="col-sm-4 col-md-3">	
				   <span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="shortCodeSubmit" id="shortCodeSubmit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" /></span>
				</div>
			</div>
                
<div class="wpu-group">
 <div class="panel-group" id="accordion2">
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapse1" href="#collapse1">
        Generator
      </a>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body"> 
				<div class="wpu-sub-single">
					<div class="wpu-sub">
							<div class="single-gtitle">Dynamic Shortcode generator</div>
          </div>
					<table class="table table-bordered wpu-tbl">
            <thead>
							<tr>
									<th>#</th>
									<th class="subrow nobor"><?php  _e('Shortcode', SDF_TXT_DOMAIN); ?><br /><small><?php _e('Copy &amp; Paste this shortcode in any page/post.', SDF_TXT_DOMAIN); ?></small></th>
									<th><?php  _e('HTML', SDF_TXT_DOMAIN); ?><br /><small><?php _e('Enter HTML to display in your ShortCode.', SDF_TXT_DOMAIN); ?></small></th>
									<th><?php  _e('Enter Shortcode Here', SDF_TXT_DOMAIN); ?><br /><small><?php _e('Enter ID for special Shortcode (No Duplicates)', SDF_TXT_DOMAIN); ?></small></th>
									<th class="text-center"><a class="wpu-add btn btn-small btn-warning"><i class="fa fa-plus-circle"></i></a><br /><small><?php _e('Add / Remove Rows', SDF_TXT_DOMAIN); ?></small></th>
							</tr>
            </thead>
						<tbody>
							<tr class="wpu-row">
								<th scope="row" class="index"></th>
								<td>[sdf_short id=""]</td>
								<td>
									<div class="form-group">
										<div class="col-md-12">
											<textarea class="form-control" id="sdf_theme_shortcode[]" name="sdf_theme_shortcode[]" cols="45" rows="7"></textarea>
										</div>
									</div>	
								</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">[sdf_short id="</span>
										<input type="text" value="" name="sdf_theme_shortcode_code[]" class="form-control" />
										<span class="input-group-addon">"]</span>
									</div>
								</td>
								<td class="text-center"><a class="wpu-remove btn btn-danger btn-small"><i class="fa fa-minus-circle"></i></a></td>
							</tr>
							<?php 
							$shortCode = $this->_currentSettings['misc']['theme_short_codes'];
							$countcodes = count($shortCode['code']);

							for ($ad=0; $ad<$countcodes; $ad++) { ?>
							<tr <?php if($ad % 2 === 0) echo "class=\"active\""; ?>>
								<th scope="row" class="index"><?php echo $ad+1; ?></th>
								<td>[sdf_short id="<?php if(is_array($shortCode['code'])) echo $shortCode['code'][$ad]; ?>"]</td>
								<td>
									<div class="form-group">
										<div class="col-md-12">
											<textarea id="sdf_theme_shortcode[]" name="sdf_theme_shortcode[]" cols="45" rows="7" class="form-control"><?php if(is_array($shortCode['code'])) echo stripslashes($shortCode['content'][$ad]); ?></textarea>
										</div>
									</div>	
								</td>
								<td>
									<div class="input-group">
										<span class="input-group-addon">[sdf_short id="</span>
										<input type="text" value="<?php if(is_array($shortCode['code'])) echo $shortCode['code'][$ad]; ?>" name="sdf_theme_shortcode_code[]" class="form-control" />
										<span class="input-group-addon">"]</span>
									</div>
								</td>
								<td class="text-center"><a class="wpu-remove btn btn-danger btn-small"><i class="fa fa-minus-circle"></i></a></td>
								<td class="midlast nobor"></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
                            </div>
                      
      </div>
      </div>
    </div>
  </div>
                   		
               </div>
		<div class="row">
		<div class="col-md-4">
			<button class="btn btn-sdf-grey btn-sm" type="submit" name="shortCodeSubmit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
		</div>
		</div>
		<?php wp_nonce_field('wpu-theme-update-shortcode'); ?>
		<input type="hidden" name="sdf_panel_collapse_shown" value="" />
</div>
</form> 
  
</div><!-- end #sdf-shortcode -->
<?php endif; ?>

<?php if ($current_page == 'sdf-styles') : ?>	
<div id="sdf-styles" class="<?php if($current_page == 'sdf-styles'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">

<form name="sdf_theme_font_settings" id="sdf_theme_font_settings" class="form-horizontal" role="form" method="post" action="#sdf-styles">
<div id="sdf_theme_fonts_page" class="wrap">           
           
                    <input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
            <div class="row titlegroup">
				<div class="col-sm-8 col-md-9">
                    <div class="wpu-font-icon"></div><h3 class="pull-left"><?php _e('Style Settings', SDF_TXT_DOMAIN); ?><small class="expand-hide"><a href="javascript:" class="wpu-open-close">+ Expand All</a></small></h3>
                     <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title="<p>When you see this icon, you can hover over it for more information about the feature it's next to</p>"></i>
				</div>
				<div class="col-sm-4 col-md-3">				
					<span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="styles-submit" id="styles-submit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" /></span>
					
                </div>
			</div>
<div class="wpu-group">
<div class="panel-group" id="wpu-font-acc">

<div class="panel panel-default">
<div class="panel-heading">
	<a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapstyles" href="#collapstyles">Appearance / Skin Selector</a>
</div>
<div id="collapstyles" class="panel-collapse collapse">
<div class="panel-body">			
	
	<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Skin Presets', SDF_TXT_DOMAIN); ?></h4>
		<p><?php _e('You can save entire sitewide style settings as skin presets, then load that preset on any page or post.', SDF_TXT_DOMAIN); ?></p>
		<p><?php _e('For Theme Options Export/Import/Reset check', SDF_TXT_DOMAIN); ?> <a href="<?php echo admin_url( 'admin.php?page=sdf-settings#collapsett5' ) ?>">Export/Import/Reset Theme Options</a> <?php _e('area on Global Settings page.', SDF_TXT_DOMAIN); ?></p>
	</div>
		
	<div class="form-group">
		<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Select Skin', SDF_TXT_DOMAIN); ?></label>
		<div class="col-sm-4 col-md-4 form-inline">
			<div class="form-group">
			<select name="sdf_theme_style_preset" class="form-control">
			<?php foreach ( $this->_currentSettings['preset']['theme_presets'] as $preset_id => $preset_name ) : ?>
			<option value="<?php echo $preset_id; ?>"<?php if($this->_currentSettings['preset']['theme_preset'] == $preset_id) echo ' selected="selected"'; ?>><?php echo $preset_name; ?></option>
			<?php endforeach; ?>
			</select>
			</div>
			<button type="submit" class="btn btn-default btn-sm go-preset-select" name="sdf_submit_preset_default">Set as Active</button>
		</div>
		<div class="col-sm-4 col-md-4 form-inline text-right style-preset-control-btns hide">
				<div class="btn-group">
				<button class="btn btn-default btn-sm go-preset-rename">
				<i class="fa fa-pencil"></i> Rename
				</button>
				<button class="btn btn-default btn-sm go-preset-remove">
				<i class="fa fa-times"></i> Remove
				</button>
				<button class="btn btn-default btn-sm go-preset-add">
				<i class="fa fa-plus"></i> Add New
				</button>
				<button class="btn btn-default btn-sm go-preset-clone">
				<i class="fa fa-copy"></i> Clone
				</button>
				</div>
				
				<div class="form-group theme-preset-rename">
					<input name="sdf_input_preset_rename" type="text" class="form-control" value="" />
					<input name="sdf_input_preset_rename_val" type="hidden" value="" />
				</div> 
				<button type="submit" class="btn btn-default btn-sm theme-preset-rename" name="sdf_submit_preset_rename">Rename Skin</button>
				
				<div class="form-group theme-preset-remove">
					<input name="sdf_input_preset_remove_val" type="hidden" value="" />
				</div>
				<button type="submit" class="btn btn-danger btn-sm theme-preset-remove" name="sdf_submit_preset_remove">Remove Skin</button>
				
				<div class="form-group theme-preset-add">
					<input name="sdf_input_preset_add" type="text" class="form-control" value="" placeholder="New Skin Name" />
				</div>
				<button type="submit" class="btn btn-default btn-sm theme-preset-add" name="sdf_submit_preset_add">Add Skin</button>
				
				<div class="form-group theme-preset-clone">
					<input name="sdf_input_preset_clone" type="text" class="form-control" value="" placeholder="Clone Skin Name" />
				</div>
				<button type="submit" class="btn btn-default btn-sm theme-preset-clone" name="sdf_submit_preset_clone">Clone Skin</button>
				
				<button class="btn btn-default btn-sm go-preset-cancel">Cancel</button>
		</div>
	</div>
</div>
</div>
</div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapcolors" href="#collapcolors">
      <?php _e('Preset Colors', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapcolors" class="panel-collapse collapse">
      <div class="panel-body">
				 
				<div class="bs-callout bs-callout-grey">
				<h4><?php _e('Primary Theme Color and Preset Colors', SDF_TXT_DOMAIN); ?></h4>
				<p>Primary Theme Color is a mandatory as it's used in Bootstrap LESS as @brand-primary</p>
				<p>Create dynamic preset colors for your websites’ color palette and use inside color pickers, SDF Page Builder modules or styles settings.</p>
				<p>Simply change the preset’s color picker or hex value here and all variables linked to that preset will dynamically update throughout the framework.</p>
				</div>
				
				<?php 
				$primary_color = ($this->_currentSettings['typography']['theme_primary_color']) ? $this->_currentSettings['typography']['theme_primary_color'] : '';
				
				$preset_colors = $this->getColorPresets();
				
				if (array_key_exists($primary_color, $preset_colors)) {
					$primary_color = strtolower($preset_colors[$primary_color]);
				}
				else {
					$primary_color = strtolower($primary_color);
				}
				
				$hex_preset_val = '';
				$opacity_val = '';
				if($primary_color){
					$rgb_args = explode(':', $primary_color);
					$hex_preset_val = $rgb_args[0];
					$opacity_val = (count($rgb_args) > 1) ? trim($rgb_args[1]) : '1';
				}
				?>
	  
				<div class="form-group">
					<label for="sdf_theme_primary_color" class="col-sm-4 col-md-4 control-label"><?php _e('Primary Theme Color', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<div class="input-group">
					  <input id="sdf_theme_primary_color" name="" class="form-control color-picker" type="text" data-opacity="<?php echo $opacity_val; ?>" value="<?php echo $hex_preset_val ; ?>" />
						<input id="sdf_theme_primary_color_preset" name="sdf_theme_primary_color" class="color-preset" type="hidden" value="<?php echo $primary_color ; ?>" />
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle preset_colors_dd" data-toggle="dropdown"><?php _e('Preset Colors', SDF_TXT_DOMAIN); ?> <span class="caret"></span></button>
						<ul class="dropdown-menu pull-right preset_colors"></ul>
					  </div><!-- /btn-group -->
					</div><!-- /input-group -->
					</div>
					<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Use Picker, Enter hex Value or use Preset Color. Picker has opacity slider so you can adjust color alpha value, 1 for solid(default), .01-.99 for more or less opacity.'></i>Used also as @brand-primary in Bootstrap LESS</div>
				</div>
				
				<input name="sdf_input_color_remove_val" type="hidden" value="" />
				<input name="sdf_input_color_change_val" type="hidden" value="" />
				<?php 
				if(is_array($this->_currentSettings['typography']['preset_colors'])) :
				foreach ( $this->_currentSettings['typography']['preset_colors'] as $preset_name => $preset_id ) : 
				
				$preset_colors = $this->getColorPresets();
				
				if (array_key_exists($preset_id, $preset_colors)) {
					$preset_id = strtolower($preset_colors[$preset_id]);
				}
				else {
					$preset_id = strtolower($preset_id);
				}
				
				$hex_preset_val = '';
				$opacity_val = '';
				if($preset_id){
					$rgb_args = explode(':', $preset_id);
					$hex_preset_val = $rgb_args[0];
					$opacity_val = (count($rgb_args) > 1) ? trim($rgb_args[1]) : '1';
				}	
				?>
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"></label>
					<div class="col-sm-5 col-md-5 form-inline">
						<div class="form-group">
							<input name="preset_color_names[]" type="text" class="form-control" value="<?php echo $preset_name; ?>" disabled />
						</div>
						<div class="form-group">	
							<input name="" type="text" class="form-control color-picker color-preset-name" data-opacity="<?php echo $opacity_val; ?>" value="<?php echo $hex_preset_val; ?>" disabled />
							<input name="preset_color_hexs[]" class="color-preset" type="hidden" value="<?php echo $preset_id; ?>" />
						</div>
						<div class="btn-group">
						<button class="btn btn-default btn-sm go-color-change">
						<i class="fa fa-pencil"></i> <?php _e('Change', SDF_TXT_DOMAIN); ?>
						</button>
						<button class="btn btn-default btn-sm go-color-remove">
						<i class="fa fa-times"></i> <?php _e('Remove', SDF_TXT_DOMAIN); ?>
						</button>
						</div>
						
						<div class="btn-group theme-color-change">
							<button type="submit" class="btn btn-sdf-grey btn-sm" name="sdf_submit_color_change"><?php _e('Change Color', SDF_TXT_DOMAIN); ?></button>
							<button class="btn btn-default btn-sm go-color-cancel"><?php _e('Cancel', SDF_TXT_DOMAIN); ?></button>
						</div>
						
						<div class="btn-group theme-color-remove">
							<button type="submit" class="btn btn-danger btn-sm" name="sdf_submit_color_remove"><?php _e('Remove Color', SDF_TXT_DOMAIN); ?></button>
							<button class="btn btn-default btn-sm go-color-cancel"><?php _e('Cancel', SDF_TXT_DOMAIN); ?></button>
						</div>
					</div>	
					<div class="col-sm-3 col-md-3 form-inline">
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('New Preset Color', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-5 col-md-5 form-inline">
						<div class="form-group">
							<input name="sdf_input_color_name_add" type="text" class="form-control" value="" placeholder="New Color Name" />
						</div>
						<div class="form-group">	
							<input name="" type="text" class="form-control color-picker" value="" placeholder="New Hex Color" />
							<input id="sdf_input_color_hex_add_preset" name="sdf_input_color_hex_add" class="color-preset" type="hidden" value="" />
						</div>
						<button type="submit" class="btn btn-default btn-sm" name="sdf_submit_color_add"><?php _e('Add Preset Color', SDF_TXT_DOMAIN); ?></button>
					</div>
					<div class="col-sm-3 col-md-3 form-inline">	
					</div>
				</div>
				
      </div>
    </div>
  </div> 
<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collaptbs" href="#collaptbs">
      <?php _e('Customize Bootstrap Less variables', SDF_TXT_DOMAIN); ?> 
      </a>
    </div>
    <div id="collaptbs" class="panel-collapse collapse">
      <div class="panel-body">
			
			<div class="bs-callout bs-callout-warning">
			<h4>Customize Less variables to define colors, sizes and more inside your custom CSS stylesheets.</h4>
			</div>
			
			<?php foreach($this->_ultTbsVars as  $option_key => $option_fields) : ?>
			<?php
				$option_key_id = str_replace( "-", "_", urlencode(strtolower($option_key)) );
			?>
				<?php if( $option_fields['type'] != 'callout' ) : ?>
					<div class="form-group">
						<label for="" class="col-sm-4 col-md-4 control-label"><?php echo $option_fields['name']; ?></label>
						<div class="col-sm-4 col-md-3">
						<?php echo $this->displaySettingsField('theme_tbs_'.$option_key_id, $this->_currentSettings['typography']['theme_tbs_'.$option_key_id], $option_fields)?>
						</div>
						<div class="col-sm-4 col-md-5 help-text"><?php if( $option_fields['type'] == 'color' ) echo '<i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="right" data-toggle="tooltip" data-original-title="Use Picker, Enter hex Value or use Preset Color. Picker has opacity slider so you can adjust color alpha value, 1 for solid(default), .01-.99 for more or less opacity."></i>'; ?> <?php if(isset($option_fields['help'])) echo $option_fields['help']; ?> <?php if(isset($option_fields['placeholder'])) echo 'Default value is: <code>' . $option_fields['placeholder'] . '</code>'; ?></div>
					</div>
				<?php else: ?>
					<?php echo $this->displaySettingsField($option_key, '', $option_fields)?>
				<?php endif; ?>
			
			<?php endforeach; ?>
	
      </div>
    </div>
  </div> 
<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfont1" href="#collapfont1">
      <?php _e('Google Web Fonts, Default Typography and List Styling', SDF_TXT_DOMAIN); ?> 
      </a>
    </div>
    <div id="collapfont1" class="panel-collapse collapse">
      <div class="panel-body">
	
	<ul class="nav nav-pills">
		<li class="active"><a href="#google_web_fonts" data-toggle="pill">Google Web Fonts</a></li>
		<li><a href="#default_typography" data-toggle="pill">Default Typography</a></li>
		<li><a href="#list_styling" data-toggle="pill">List Styling</a></li>
	</ul>
	<div class="tab-content style-pills-content">
	<div class="tab-pane fade in active" id="google_web_fonts">
	
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Google Font Families', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-4">
				<div class="table-responsive">
				<table class="table table-bordered wpu-tbl">
					<thead>
					<tr>
						<th class="text-center">#</th>
						<th class="text-center"><?php  _e('Font Name', SDF_TXT_DOMAIN); ?><br /><small><?php _e('Enter Google Web Font Name Only.', SDF_TXT_DOMAIN); ?></small></th>
						<th class="text-center"><a class="wpu-add btn btn-small btn-warning"><i class="fa fa-plus-circle"></i></a><br /><small><?php _e('Add / Remove Rows', SDF_TXT_DOMAIN); ?></small></th>
					</tr>
				  </thead>
					<tbody>
						<tr class="wpu-row">
							<th scope="row" class="index"></th>
							<td>
								<div class="form-group">
									<div class="col-sm-12 col-md-12">
										<input type="text" class="form-control" name="sdf_google_fonts[]" value="" placeholder="Google Font Name" />
									</div>
								</div>	
							</td>
							<td class="text-center"><a class="wpu-remove btn btn-danger btn-small"><i class="fa fa-minus-circle"></i></a></td>
						</tr>
						<?php 
						for ($ad=0; $ad< count($this->_currentSettings['typography']['google_web_font']); $ad++) {
							?>
							<tr <?php if($ad % 2 === 0) echo "class=\"active\""; ?>>
							<th scope="row" class="index"><?php echo $ad+1; ?></th>
							<td>
							<div class="form-group">
								<div class="col-sm-12 col-md-12">
									<input type="text" class="form-control" name="sdf_google_fonts[]" value="<?php if($this->_currentSettings['typography']['google_web_font'][$ad]) echo $this->_currentSettings['typography']['google_web_font'][$ad]; ?>" placeholder="Google Font Name" />
								</div>
							</div>
							</td>
							<td class="text-center"><a class="wpu-remove btn btn-danger btn-small"><i class="fa fa-minus-circle"></i></a></td>
						</tr>
						<?php }
						?>
					</tbody>
				</table>
				</div>
			</div>
			<div class="col-sm-4 col-md-4 help-text">Please visit <a href="http://www.google.com/webfonts" target="_blank">Google Web Fonts</a> for a complete listing of available <strong>web fonts</strong>. From there, copy the name of the font you wish to use. Then enter the name of the webfont you wish to add.</div>
		</div>
		
	</div>
	<div class="tab-pane fade" id="default_typography">
			
	<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Default Font', SDF_TXT_DOMAIN); ?></h4>
	</div>

		<div class="form-group">
			<label for="sdf_theme_font_size" class="col-sm-4 col-md-4 control-label"><?php _e('Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<input type="text" class="form-control" name="sdf_theme_font_size" value="<?php if($this->_currentSettings['typography']['theme_font_size']) echo $this->_currentSettings['typography']['theme_font_size']; ?>" placeholder="14px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Used also as @font-size-base in Bootstrap LESS</div>
		</div>	

		<div class="form-group">
			<label for="sdf_theme_font_family" class="col-sm-4 col-md-4 control-label"><?php _e('Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select class="form-control" id="sdf_theme_font_family" name="sdf_theme_font_family"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
		
		<?php echo $this->displayColorPickerField('theme_font_color', __('Font Color', SDF_TXT_DOMAIN)); ?>
			
	<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Default Links', SDF_TXT_DOMAIN); ?></h4>
	</div>

		<?php echo $this->displayColorPickerField('theme_font_link_color', __('Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_font_link_bg_color', __('Link Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_font_link_text_decoration" name="sdf_theme_font_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['theme_font_link_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for links', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_link_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Link Box Shadow', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_font_link_box_shadow" name="sdf_theme_font_link_box_shadow" value="<?php if($this->_currentSettings['typography']['theme_font_link_box_shadow']) echo $this->_currentSettings['typography']['theme_font_link_box_shadow']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_font_link_box_shadow_color', __('Link Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_link_transition" class="col-sm-4 col-md-4 control-label"><?php _e('Link Transition Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_font_link_transition" name="sdf_theme_font_link_transition" value="<?php if($this->_currentSettings['typography']['theme_font_link_transition']) echo $this->_currentSettings['typography']['theme_font_link_transition']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='[transition-property] [transition-duration] [transition-timing-function] [transition-delay]<br /> (e.g. "width 1s linear 2s")'></i>The transition property is a shorthand property used to represent up to four transition-related longhand properties. You may comma separate value sets to do different transitions on different properties (e.g. "width 2s, height 2s").</div>
		</div>

		<?php echo $this->displayColorPickerField('theme_font_link_hover_color', __('Link Hover Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_font_link_hover_bg_color', __('Link Hover Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_font_link_hover_text_decoration" name="sdf_theme_font_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['theme_font_link_hover_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for links on hover/focus', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_link_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Link Hover Box Shadow', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_font_link_hover_box_shadow" name="sdf_theme_font_link_hover_box_shadow" value="<?php if($this->_currentSettings['typography']['theme_font_link_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_font_link_hover_box_shadow']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_font_link_hover_box_shadow_color', __('Link Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
	
	<?php 
	if(isset($this->_headingsList) && is_array($this->_headingsList)) :
	foreach ($this->_headingsList as $h_id => $h_name) :
	?>
	
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e($h_name.' Heading', SDF_TXT_DOMAIN); ?></h4>
		</div>
	
		<div class="form-group">
			<label for="sdf_theme_font_<?php echo $h_id; ?>_family" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_font_<?php echo $h_id; ?>_family" name="sdf_theme_font_<?php echo $h_id; ?>_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_font_'.$h_id.'_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="sdf_theme_font_<?php echo $h_id; ?>_size" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" class="form-control" id="sdf_theme_font_<?php echo $h_id; ?>_size" name="sdf_theme_font_<?php echo $h_id; ?>_size" value="<?php if($this->_currentSettings['typography']['theme_font_'.$h_id.'_size'] ) echo $this->_currentSettings['typography']['theme_font_'.$h_id.'_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_font_<?php echo $h_id; ?>_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_'.$h_id.'_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_font_'.$h_id.'_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_font_'.$h_id.'_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_<?php echo $h_id; ?>_letter_spacing" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Letter Spacing', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<input type="text" class="form-control" id="sdf_theme_font_<?php echo $h_id; ?>_letter_spacing" name="sdf_theme_font_<?php echo $h_id; ?>_letter_spacing" value="<?php if($this->_currentSettings['typography']['theme_font_'.$h_id.'_letter_spacing'] ) echo $this->_currentSettings['typography']['theme_font_'.$h_id.'_letter_spacing']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
		
		<?php echo $this->displayColorPickerField('theme_font_'.$h_id.'_color', __($h_name.' Font Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_<?php echo $h_id; ?>_weight" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_font_<?php echo $h_id; ?>_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_'.$h_id.'_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_'.$h_id.'_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_font_<?php echo $h_id; ?>_weight" value="<?php echo $this->_currentSettings['typography']['theme_font_'.$h_id.'_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Margin Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_font_<?php echo $h_id; ?>_margin_top" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography']['theme_font_'.$h_id.'_margin_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>	
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e($h_name.' Margin Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_font_<?php echo $h_id; ?>_margin_bottom" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography']['theme_font_'.$h_id.'_margin_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>	
	
	<?php endforeach; ?>	
	<?php endif; ?>	
	
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e('Default Subtitle', SDF_TXT_DOMAIN); ?></h4>
		</div>

		<div class="form-group">
			<label for="sdf_theme_font_subtitle_family" class="col-sm-4 col-md-4 control-label"><?php _e('Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select class="form-control" id="sdf_theme_font_subtitle_family" name="sdf_theme_font_subtitle_family"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_font_subtitle_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="sdf_theme_font_subtitle_size" class="col-sm-4 col-md-4 control-label"><?php _e('Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" class="form-control" name="sdf_theme_font_subtitle_size" value="<?php if($this->_currentSettings['typography']['theme_font_subtitle_size']) echo $this->_currentSettings['typography']['theme_font_subtitle_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_font_subtitle_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_subtitle_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_font_subtitle_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_font_subtitle_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
		
		<?php echo $this->displayColorPickerField('theme_font_subtitle_color', __('Font Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_subtitle_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_font_subtitle_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_subtitle_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_subtitle_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_font_subtitle_weight" value="<?php echo $this->_currentSettings['typography']['theme_font_subtitle_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
	</div>
	<div class="tab-pane fade" id="list_styling">
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set Default List Styles <code>&lt;ul&gt;</code>', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Paddings', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_list_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_list_padding']) echo $this->_currentSettings['typography']['theme_font_list_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_list_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_list_margin']) echo $this->_currentSettings['typography']['theme_font_list_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Line Height', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_list_item_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_list_item_line_height']) echo $this->_currentSettings['typography']['theme_font_list_item_line_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Line Height for List', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_font_list_style" class="col-sm-4 col-md-4 control-label"><?php _e('List Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_theme_font_list_style" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_list_style'],'default', "btn-sdf-grey")?>" value="default">Default</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_list_style'],'none', "btn-sdf-grey")?>" value="none">None</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_list_style'],'square', "btn-sdf-grey")?>" value="square">Square</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_list_style'],'circle', "btn-sdf-grey")?>" value="circle">Circle</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_list_style'],'disc', "btn-sdf-grey")?>" value="disc">Disc</button>
					</div>
					<input type="hidden" id="sdf_theme_font_list_style" name="sdf_theme_font_list_style" value="<?php echo $this->_currentSettings['typography']['theme_font_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set Custom List Styles <code>&lt;ul class="list-custom"&gt;</code>', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_styling" class="col-sm-4 col-md-4 control-label">Set Custom List Styles</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_styling'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_styling'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_theme_font_custom_list_styling" name="sdf_theme_font_custom_list_styling" value="<?php echo $this->_currentSettings['typography']['theme_font_custom_list_styling']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-theme-font-custom-list-styles">
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Paddings', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_padding']) echo $this->_currentSettings['typography']['theme_font_custom_list_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_margin']) echo $this->_currentSettings['typography']['theme_font_custom_list_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_theme_font_custom_list_align" value="<?php echo $this->_currentSettings['typography']['theme_font_custom_list_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_font_size" class="col-sm-4 col-md-4 control-label">Custom List Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_theme_font_custom_list_font_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_font_size'] ) echo $this->_currentSettings['typography']['theme_font_custom_list_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_theme_font_custom_list_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_custom_list_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_font_custom_list_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_font_custom_list_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_family" class="col-sm-4 col-md-4 control-label">Custom List Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_theme_font_custom_list_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_font_custom_list_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField('theme_font_custom_list_font_color', __('Custom List Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField('theme_font_custom_list_link_color', __('Custom List Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField('theme_font_custom_list_link_hover_color', __('Custom List Link Hover Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_theme_font_custom_list_link_text_decoration" name="sdf_theme_font_custom_list_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['theme_font_custom_list_link_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Custom List Links', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_theme_font_custom_list_link_hover_text_decoration" name="sdf_theme_font_custom_list_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['theme_font_custom_list_link_hover_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Custom List Links on hover', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_style" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_theme_font_custom_list_style" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'default', "btn-sdf-grey")?>" value="default">Default</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'none', "btn-sdf-grey")?>" value="none">None</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'square', "btn-sdf-grey")?>" value="square">Square</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'circle', "btn-sdf-grey")?>" value="circle">Circle</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'disc', "btn-sdf-grey")?>" value="disc">Disc</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_style'],'icon', "btn-sdf-grey")?>" value="icon">FA Icon</button>
					</div>
					<input type="hidden" id="sdf_theme_font_custom_list_style" name="sdf_theme_font_custom_list_style" value="<?php echo $this->_currentSettings['typography']['theme_font_custom_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
				
			<div id="sdf-theme-font-custom-list-icon">
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Custom List Icon', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
						<div class="btn-group sdf_toggle">
						<?php
						if(isset($this->_ultIconsUnicode) && is_array($this->_ultIconsUnicode)){
							$list_icon_picker = '';
							foreach($this->_ultIconsUnicode as $key => $val){
								$list_icon_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography']['theme_font_custom_list_icon'],$val, "btn-sdf-grey") . '" value="'.$val.'"><i class="fa fa-lg '. $key. '"></i></button>';
							}
							echo $list_icon_picker;
						}
						?>
						</div>
						<input type="hidden" name="sdf_theme_font_custom_list_icon" value="<?php echo $this->_currentSettings['typography']['theme_font_custom_list_icon']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>	
				
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Icon Margins', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input name="sdf_theme_font_custom_list_icon_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_icon_margin']) echo $this->_currentSettings['typography']['theme_font_custom_list_icon_margin']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for Custom List Items', SDF_TXT_DOMAIN); ?></div>
				</div>
			
				<div class="form-group">
					<label for="sdf_theme_font_custom_list_icon_size" class="col-sm-4 col-md-4 control-label">Custom List Icon Font Size</label>
					<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="col-sm-8 col-md-8">
							<input type="text" name="sdf_theme_font_custom_list_icon_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_icon_size'] ) echo $this->_currentSettings['typography']['theme_font_custom_list_icon_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
							</div>
							<div class="col-sm-4 col-md-4">
							<select name="sdf_theme_font_custom_list_icon_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_custom_list_icon_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_font_custom_list_icon_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_font_custom_list_icon_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>	
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Padding', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_item_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_padding']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Padding for Custom List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_item_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_margin']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for Custom List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Line Height', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_item_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_line_height']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_line_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Line Height for Custom List', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Border Width', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_item_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_width']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_border_width']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
					
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Border Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_theme_font_custom_list_item_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Border Radius', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_theme_font_custom_list_item_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_border_radius']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_border_radius']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set Custom List Colors', SDF_TXT_DOMAIN); ?></p>
			</div>
				
			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_background_color', __('Custom List Items Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_border_color', __('Custom List Items Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_item_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_theme_font_custom_list_item_box_shadow" name="sdf_theme_font_custom_list_item_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_box_shadow']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_box_shadow_color', __('Custom List Items Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_background_hover_color', __('Custom List Items Hover Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_border_hover_color', __('Custom List Items Hover Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_theme_font_custom_list_item_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Custom List Items Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_theme_font_custom_list_item_hover_box_shadow" name="sdf_theme_font_custom_list_item_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_custom_list_item_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_font_custom_list_item_hover_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField('theme_font_custom_list_item_hover_box_shadow_color', __('Custom List Items Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>			
		
			</div>
			
    </div>                   
    </div>  
		
      </div>
    </div>
  </div>	
<div class="panel panel-default">	
	<div class="panel-heading">
		<a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfont2" href="#collapfont2">
		<?php _e('Wide/Boxed Layout Backgrounds', SDF_TXT_DOMAIN); ?>
		</a>
	</div>
<div id="collapfont2" class="panel-collapse collapse">
  <div class="panel-body">	
    
	<h4><?php _e('Wide Layout Settings', SDF_TXT_DOMAIN); ?></h4>
		<p><?php _e('Set Wide Layout Body Background', SDF_TXT_DOMAIN); ?></p>

		<?php echo $this->displayColorPickerField('theme_font_wide_bg_color', __('Change Body Background Color', SDF_TXT_DOMAIN), __('Body Background Color used on Wide Page Layouts', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_wide_bg_image" class="col-md-4 control-label">Add Body Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_theme_font_wide_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography']['theme_font_wide_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for website background.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_wide_bg_repeat" class="col-sm-4 col-md-4 control-label">Body Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_font_wide_bg_repeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography']['theme_font_wide_bg_repeat'], true);  ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="hr"></div>
	
	<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Boxed Layout Settings', SDF_TXT_DOMAIN); ?></h4>
		<p><?php _e('Set Boxed Layout Body Background', SDF_TXT_DOMAIN); ?></p>
	</div>
		
		<?php echo $this->displayColorPickerField('theme_font_boxed_bg_color', __('Change Body Background Color', SDF_TXT_DOMAIN), __('Body Background Color used on Boxed Page Layouts', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_bg_image" class="col-md-4 control-label">Add Body Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_theme_font_boxed_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography']['theme_font_boxed_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for website background.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_bg_repeat" class="col-sm-4 col-md-4 control-label">Body Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_font_boxed_bg_repeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography']['theme_font_boxed_bg_repeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
		<p>Set Box Background, Margin, Shadow and Border</p>
	</div>
		
		<?php echo $this->displayColorPickerField('theme_font_boxed_box_bg_color', __('Change Box Background Color', SDF_TXT_DOMAIN), __('Box Background Color used on Boxed Page Layouts', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_box_bg_image" class="col-md-4 control-label">Add Box Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_theme_font_boxed_box_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography']['theme_font_boxed_box_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for website background.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_box_bg_repeat" class="col-sm-4 col-md-4 control-label"><?php _e('Box Background Repeat', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_font_boxed_box_bg_repeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography']['theme_font_boxed_box_bg_repeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_box_margin" class="col-sm-4 col-md-4 control-label"><?php _e('Box Top/Bottom Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_font_boxed_box_margin" name="sdf_theme_font_boxed_box_margin" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography']['theme_font_boxed_box_margin']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> <?php _e('Set value for box top and bottom margin (in pixels) for boxed layout', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_box_padding" class="col-sm-4 col-md-4 control-label"><?php _e('Box Content Left/Right Padding', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_font_boxed_box_padding" name="sdf_theme_font_boxed_box_padding" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography']['theme_font_boxed_box_padding']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> <?php _e('Set value for box padding (in pixels) for boxed layout', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_font_boxed_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_font_boxed_box_shadow" name="sdf_theme_font_boxed_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_boxed_box_shadow']) echo $this->_currentSettings['typography']['theme_font_boxed_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<?php echo $this->displayColorPickerField('theme_font_boxed_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
								
		<div class="form-group">
			<label for="sdf_theme_boxed_box_border_style" class="col-sm-4 col-md-4 control-label"><?php _e('Box Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_boxed_box_border_style" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_style'] == 'none') echo ' selected'; ?> value="none">none</option><option value="solid"<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_boxed_box_border_width" class="col-sm-4 col-md-4 control-label"><?php _e('Box Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-3 col-md-2">
			<input name="sdf_theme_boxed_box_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_width']) echo $this->_currentSettings['typography']['theme_font_boxed_box_border_width']; ?>" placeholder="1px Default" />
			</div>
			<div class="col-sm-1 col-md-1">
			<select name="sdf_theme_boxed_box_border_width_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_width_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_width_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_font_boxed_box_border_width_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		 
		<?php echo $this->displayColorPickerField('theme_font_boxed_box_border_color', __('Box Border Color', SDF_TXT_DOMAIN)); ?>
		
      </div>
    </div>
	</div>
  
	<?php 
	$id = 'page-header';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Page Padding, Page Title, Entry Title and Breadcrumbs', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
      <div class="panel-body">
	  
		<div class="bs-callout bs-callout-grey">
		<h4>Page Top/Bottom Padding</h4>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Padding Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_padding_top']) echo $this->_currentSettings['typography']['page_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Padding Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_padding_bottom']) echo $this->_currentSettings['typography']['page_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
	  
	  <div class="bs-callout bs-callout-grey">
		<h4><?php _e('Page Title and Subtitle', SDF_TXT_DOMAIN); ?></h4>
	  </div>	
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_alignment" value="<?php echo $this->_currentSettings['typography'][$id_var.'_alignment']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Padding Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_header_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_header_padding_top']) echo $this->_currentSettings['typography']['page_header_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Padding Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_header_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_header_padding_bottom']) echo $this->_currentSettings['typography']['page_header_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
     
		<div class="form-group">
			<label for="sdf_page_header_font_size" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_page_header_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography']['page_header_font_size']) echo $this->_currentSettings['typography']['page_header_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_page_header_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography']['page_header_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography']['page_header_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['page_header_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_page_header_font_family" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_page_header_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['page_header_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>   	
		
		<div class="form-group">
			<label for="sdf_page_header_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_page_header_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['page_header_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['page_header_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_page_header_font_weight" value="<?php echo $this->_currentSettings['typography']['page_header_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_page_header_font_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_page_header_font_text_transform" name="sdf_page_header_font_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['page_header_font_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['page_header_font_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['page_header_font_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['page_header_font_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<?php echo $this->displayColorPickerField('page_header_font_color', __('Page Title Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('page_header_font_hover_color', __('Page Title Text Hover Color', SDF_TXT_DOMAIN)); ?>		
		
		<div class="form-group">
			<label for="sdf_page_header_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_page_header_link_text_decoration" name="sdf_page_header_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['page_header_link_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Page Title Link', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_page_header_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_page_header_link_hover_text_decoration" name="sdf_page_header_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography']['page_header_link_hover_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Page Title Link on hover', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<?php echo $this->displayColorPickerField('page_header_link_color', __('Page Title Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('page_header_link_hover_color', __('Page Title Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_page_header_font_subtitle_size" class="col-sm-4 col-md-4 control-label">Page Subtitle Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_page_header_font_subtitle_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography']['page_header_font_subtitle_size']) echo $this->_currentSettings['typography']['page_header_font_subtitle_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_page_header_font_subtitle_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_page_header_font_subtitle_family" class="col-sm-4 col-md-4 control-label">Page Subtitle Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_page_header_font_subtitle_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['page_header_font_subtitle_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>   
		
		<div class="form-group">
			<label for="sdf_page_header_font_subtitle_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Page Subtitle Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_page_header_font_subtitle_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['page_header_font_subtitle_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['page_header_font_subtitle_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_page_header_font_subtitle_weight" value="<?php echo $this->_currentSettings['typography']['page_header_font_subtitle_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
		
		<div class="form-group">
			<label for="sdf_page_header_font_subtitle_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Page Subtitle Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_page_header_font_subtitle_text_transform" name="sdf_page_header_font_subtitle_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['page_header_font_subtitle_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<?php echo $this->displayColorPickerField('page_header_font_subtitle_color', __('Page Subtitle Text Color', SDF_TXT_DOMAIN)); ?>
	  
		<div class="bs-callout bs-callout-grey">
		<h4>Page Title Background Settings</h4>
		<p>Set Custom Background for Page Title Area</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Page Title Area Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">

		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Area Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Area Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Area Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
		
				<?php 
		$id = 'breadcrumbs';
		$id_var = str_replace("-", "_", urlencode(strtolower($id)));
		?>  
	  <div class="bs-callout bs-callout-grey">
		<h4><?php _e('Breadcrumbs', SDF_TXT_DOMAIN); ?></h4>
	  </div>	
		
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Breadcrumbs Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_alignment'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_alignment" value="<?php echo $this->_currentSettings['typography'][$id_var.'_alignment']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
		
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Breadcrumbs Padding Top</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_top']) echo $this->_currentSettings['typography'][$id_var.'_padding_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Breadcrumbs Padding Bottom</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_bottom']) echo $this->_currentSettings['typography'][$id_var.'_padding_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
			</div>
     
			<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_size" class="col-sm-4 col-md-4 control-label">Breadcrumbs Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_<?php echo $id_var; ?>_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography'][$id_var.'_font_size']) echo $this->_currentSettings['typography'][$id_var.'_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_<?php echo $id_var; ?>_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_family" class="col-sm-4 col-md-4 control-label">Breadcrumbs Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div> 
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Breadcrumbs Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_font_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>  	
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Breadcrumbs Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_text_transform" name="sdf_<?php echo $id_var; ?>_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_font_color', __('Breadcrumbs Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_font_hover_color', __('Breadcrumbs Text Hover Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_link_color', __('Breadcrumbs Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_link_hover_color', __('Breadcrumbs Link Hover Color', SDF_TXT_DOMAIN)); ?>
	  
		<div class="bs-callout bs-callout-grey">
		<h4>Breadcrumbs Background Settings</h4>
		<p>Set Custom Background for Breadcrumbs Area</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Breadcrumbs Area Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Area Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Area Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Area Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
		
		<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Entry Title Paddings', SDF_TXT_DOMAIN); ?></h4>
		<p><?php _e('Used for Post Titles on Blog, Category and Archive Pages', SDF_TXT_DOMAIN); ?></p>
	  </div>	
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Entry Title Padding Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_entry_title_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['entry_title_padding_top']) echo $this->_currentSettings['typography']['entry_title_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Entry Title Padding Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_entry_title_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['entry_title_padding_bottom']) echo $this->_currentSettings['typography']['entry_title_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>

	  </div>
    </div>
  </div>
	
	<?php 
	$id = 'featured-image';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
	<div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Featured Image Top/Bottom Padding', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
      <div class="panel-body">

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Padding Top', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_featured_image_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['featured_image_padding_top']) echo $this->_currentSettings['typography']['featured_image_padding_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Padding Bottom', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_featured_image_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['featured_image_padding_bottom']) echo $this->_currentSettings['typography']['featured_image_padding_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
			</div>

			</div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfont11" href="#collapfont11">
       Widget Areas and Widgets
      </a>
    </div>
    <div id="collapfont11" class="panel-collapse collapse">
      <div class="panel-body">
	  

		<div class="bs-callout bs-callout-grey">
			<h4>Widget Areas Styling Settings</h4>
			<p>Set Widgets Alignment, Equal Height Options and Custom Background for each area or just leave it transparent with current body background.<br />
			Furthermore, customize widgets per area...</p>
		</div>

		<?php foreach ($this->_sdfSidebars as $id => $name) :

		if ( in_array($id, array('default-widget-area', 'above-content-area', 'left-widget-area', 'right-widget-area', 'below-content-area')) ) :
		$id_var = str_replace( "-", "_", urlencode(strtolower($id)) );
		 ?>
		<div class="bs-callout bs-callout-grey">
			<h4><?php echo $name; ?></h4>
		</div>
		
		<ul class="nav nav-pills">
			<li class="active"><a href="#area_<?php echo $id_var; ?>" data-toggle="pill">Customize Widget Area</a></li>
			<li><a href="#widget_style_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Styling</a></li>
			<li><a href="#widget_typo_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Typography</a></li>
			<li><a href="#widget_list_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets List Styling</a></li>
		</ul>
		<div class="tab-content style-pills-content">
		<div class="tab-pane fade in active" id="area_<?php echo $id_var; ?>">
	
			<?php if ( in_array($id, array('above-content-area', 'below-content-area')) ) : ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_height" class="col-sm-4 col-md-4 control-label">Area Height</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_height" name="sdf_<?php echo $id_var; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_height']) echo $this->_currentSettings['typography'][$id_var.'_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Height</div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_padding_top" class="col-sm-4 col-md-4 control-label">Area Top Padding</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_padding_top" name="sdf_<?php echo $id_var; ?>_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_top']) echo $this->_currentSettings['typography'][$id_var.'_padding_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Top Padding</div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_padding_bottom" class="col-sm-4 col-md-4 control-label">Area Bottom Padding</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_padding_bottom" name="sdf_<?php echo $id_var; ?>_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_bottom']) echo $this->_currentSettings['typography'][$id_var.'_padding_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Bottom Padding</div>
			</div>
			
			<?php endif; ?>
			
			<div class="form-group">
				<label for="wgs_align_<?php echo $id_var; ?>" class="col-sm-4 col-md-4 control-label"><?php _e('Widgets Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="wgs_align_<?php echo $id_var; ?>" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
		
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Set Equal Widget Heights</label>
				<div class="col-sm-4 col-md-3">
					<div id="" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'off', "btn-sdf-grey")?>" value="off">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'per_area', "btn-sdf-grey")?>" value="per_area">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_equal_heights" name="sdf_<?php echo $id_var; ?>_equal_heights" value="<?php echo $this->_currentSettings['typography'][$id_var.'_equal_heights']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Area Background</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-bg">
			
			<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Area Background Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
				<div class="col-sm-4 col-md-3">
				<div class="input-group">
					<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
					<span class="input-group-btn">
						<span class="btn btn-custom btn-file wpu-media-upload">
							<i class="fa fa-upload"></i> Upload Image <input type="file">
						</span>
					</span>
				</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
			</div>
			
			<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
				<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
					<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
				</select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
				<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Area Box Shadow Values</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Area Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			</div>
		</div>
		<div class="tab-pane fade" id="widget_style_<?php echo $id_var; ?>">
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Style</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_style" name="sdf_<?php echo $id_var; ?>_wg_custom_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-styles">

			<?php echo $this->displayColorPickerField($id_var.'_wg_background_color', __('Widget Background Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_bg_image" class="col-md-4 control-label">Widget Background Image</label>
				<div class="col-sm-4 col-md-3">
				<div class="input-group">
					<input name="sdf_<?php echo $id_var; ?>_wg_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_bg_image']; ?>">
					<span class="input-group-btn">
						<span class="btn btn-custom btn-file wpu-media-upload">
							<i class="fa fa-upload"></i> Upload Image <input type="file">
						</span>
					</span>
				</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="col-sm-4 col-md-4 control-label">Widget Background Repeat</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="form-control">
					<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_wg_bgrepeat'], true); ?>
				</select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="col-sm-4 col-md-4 control-label">Widget Box Shadow Values</label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_box_shadow_color', __('Widget Box Shadow Color', SDF_TXT_DOMAIN)); ?>
					 
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Width</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_width']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
				
			<?php echo $this->displayColorPickerField($id_var.'_wg_border_color', __('Widget Border Color', SDF_TXT_DOMAIN)); ?>
					
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Style</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Radius</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_radius']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
					
			</div>
		</div>
		<div class="tab-pane fade" id="widget_typo_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_typo" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Typography</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_typo" name="sdf_<?php echo $id_var; ?>_wg_custom_typo" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_typo']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-typography">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Title Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Title Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Top</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_top" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Bottom</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_bottom" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>		
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_size" class="col-sm-4 col-md-4 control-label">Widget Title Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_title_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_title_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_title_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_title_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_title_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_title_color', __('Widget Title Font Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Font Weight', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_title_weight" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_weight']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Text-transform', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_title_transform" name="sdf_<?php echo $id_var; ?>_wg_title_transform" class="form-control">
				<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'no') echo ' selected'; ?>>None</option>
				<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
				<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
				<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
				</select>
				</div>
				<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
			</div>	

			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
			</div>
	
			<?php echo $this->displayColorPickerField($id_var.'_wg_title_box_shadow_color', __('Widget Title Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Content Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_content_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_content_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_font_size" class="col-sm-4 col-md-4 control-label">Widget Content Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Content Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_font_color', __('Widget Content Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_color', __('Widget Content Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_hover_color', __('Widget Content Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
			</div>
		</div>
		<div class="tab-pane fade" id="widget_list_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_list_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets List Style</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_list_style" name="sdf_<?php echo $id_var; ?>_wg_custom_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-styles">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget List Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Paddings', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="col-sm-4 col-md-4 control-label">List Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_list_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_family" class="col-sm-4 col-md-4 control-label">List Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_list_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_font_color', __('List Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_color', __('List Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_hover_color', __('List Link Hover Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_hover_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links on hover', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_style" class="col-sm-4 col-md-4 control-label"><?php _e('List Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_list_style" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'default', "btn-sdf-grey")?>" value="default">Default</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'none', "btn-sdf-grey")?>" value="none">None</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'square', "btn-sdf-grey")?>" value="square">Square</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'circle', "btn-sdf-grey")?>" value="circle">Circle</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'disc', "btn-sdf-grey")?>" value="disc">Disc</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'icon', "btn-sdf-grey")?>" value="icon">FA Icon</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_list_style" name="sdf_<?php echo $id_var; ?>_wg_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
				
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-icon">
				<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose List Icon', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<?php
					if(isset($this->_ultIconsUnicode) && is_array($this->_ultIconsUnicode)){
						$list_icon_picker = '';
						foreach($this->_ultIconsUnicode as $key => $val){
							$list_icon_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_icon'],$val, "btn-sdf-grey") . '" value="'.$val.'"><i class="fa fa-lg '. $key. '"></i></button>';
						}
						echo $list_icon_picker;
					}
					?>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_icon" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Icon Margins', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input name="sdf_<?php echo $id_var; ?>_wg_list_icon_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
				</div>
			
				<div class="form-group">
					<label for="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="col-sm-4 col-md-4 control-label">List Icon Font Size</label>
					<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="col-sm-8 col-md-8">
							<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
							</div>
							<div class="col-sm-4 col-md-4">
							<select name="sdf_<?php echo $id_var; ?>_wg_list_icon_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>	
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Padding', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Padding for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Line Height', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Line Height for List', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Width', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
					
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_item_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Radius', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set List Colors', SDF_TXT_DOMAIN); ?></p>
			</div>
				
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_color', __('List Items Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_color', __('List Items Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_box_shadow_color', __('List Items Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_hover_color', __('List Items Hover Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_hover_color', __('List Items Hover Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_hover_box_shadow_color', __('List Items Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>			
		
			</div>
		
		</div>
		</div>
			
		
		<?php endif; ?>
		<?php endforeach; ?>
		</div>
		</div>
	</div>
		
	<?php 
	$id = 'header';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Header', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
      <div class="panel-body">

		<div class="form-group">
			<label for="sdf_header_font_size" class="col-sm-4 col-md-4 control-label">Header Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_header_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography']['header_font_size']) echo $this->_currentSettings['typography']['header_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_header_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography']['header_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography']['header_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['header_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_header_font_family" class="col-sm-4 col-md-4 control-label">Header Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_header_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['header_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>   
		
		<div class="form-group">
			<label for="sdf_header_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Header Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_header_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['header_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['header_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_header_font_weight" value="<?php echo $this->_currentSettings['typography']['header_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>  

		<div class="hr"></div>
		
		<?php echo $this->displayColorPickerField('header_font_color', __('Header Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('header_font_hover_color', __('Header Text Hover Color', SDF_TXT_DOMAIN)); ?>
                               
		<div class="hr"></div>

		<?php echo $this->displayColorPickerField('header_link_color', __('Header Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('header_link_hover_color', __('Header Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
		<h4>Header Background Settings</h4>
		<p>Set Custom Background for Header Area</p>
		</div>

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Header Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Header Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Header Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Header Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Header Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Header Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
	
	<div class="bs-callout bs-callout-grey">
	<h4>Header Widget Areas Styling Settings</h4>
	<p>Set Widgets Alignment, Equal Height Options and Custom Background for each area or just leave it transparent with current body background.<br />
	Furthermore, customize widgets per area...</p>
	</div>

	<?php foreach ($this->_sdfSidebars as $id => $name) :

	if (in_array($id, array('header-area-1', 'header-area-2', 'header-area-3', 'header-area-4'))) :
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
	<div class="bs-callout bs-callout-grey">
		<h4><?php echo $name; ?></h4>
	</div>
	
	<ul class="nav nav-pills">
		<li class="active"><a href="#area_<?php echo $id_var; ?>" data-toggle="pill">Customize Area</a></li>
		<li><a href="#widget_style_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Styling</a></li>
		<li><a href="#widget_typo_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Typography</a></li>
		<li><a href="#widget_list_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets List Styling</a></li>
	</ul>
	<div class="tab-content style-pills-content">
	<div class="tab-pane fade in active" id="area_<?php echo $id_var; ?>">
	
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_height" class="col-sm-4 col-md-4 control-label">Area Height</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_height" name="sdf_<?php echo $id_var; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_height']) echo $this->_currentSettings['typography'][$id_var.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i>  Define Area Height</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_padding_top" class="col-sm-4 col-md-4 control-label">Area Top Padding</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_padding_top" name="sdf_<?php echo $id_var; ?>_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_top']) echo $this->_currentSettings['typography'][$id_var.'_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Top Padding</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_padding_bottom" class="col-sm-4 col-md-4 control-label">Area Bottom Padding</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_padding_bottom" name="sdf_<?php echo $id_var; ?>_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_bottom']) echo $this->_currentSettings['typography'][$id_var.'_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Bottom Padding</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Widgets Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_align']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
	
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_equal_heights" class="col-sm-4 col-md-4 control-label">Set Equal Widget Heights</label>
			<div class="col-sm-4 col-md-3">
				<div id="" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'off', "btn-sdf-grey")?>" value="off">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'per_area', "btn-sdf-grey")?>" value="per_area">Yes</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_equal_heights" name="sdf_<?php echo $id_var; ?>_equal_heights" value="<?php echo $this->_currentSettings['typography'][$id_var.'_equal_heights']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Area Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Area Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Area Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Area Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
	</div>
	<div class="tab-pane fade" id="widget_style_<?php echo $id_var; ?>">
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Style</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_style" name="sdf_<?php echo $id_var; ?>_wg_custom_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_style']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-wg-styles">
		
		<?php echo $this->displayColorPickerField($id_var.'_wg_background_color', __('Widget Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_bg_image" class="col-md-4 control-label">Widget Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_wg_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="col-sm-4 col-md-4 control-label">Widget Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_wg_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="col-sm-4 col-md-4 control-label">Widget Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_wg_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_wg_box_shadow_color', __('Widget Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				 
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Width</label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_<?php echo $id_var; ?>_wg_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
			
		<?php echo $this->displayColorPickerField($id_var.'_wg_border_color', __('Widget Border Color', SDF_TXT_DOMAIN)); ?>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Style</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_wg_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Radius</label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_<?php echo $id_var; ?>_wg_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_radius']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		</div>
	</div>
	<div class="tab-pane fade" id="widget_typo_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_typo" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Typography</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_typo" name="sdf_<?php echo $id_var; ?>_wg_custom_typo" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_typo']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-typography">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Title Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Title Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Top</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_top" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Bottom</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_bottom" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>		
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_size" class="col-sm-4 col-md-4 control-label">Widget Title Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_title_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_title_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_title_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_title_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_title_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_title_color', __('Widget Title Font Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Font Weight', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_title_weight" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_weight']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Text-transform', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_title_transform" name="sdf_<?php echo $id_var; ?>_wg_title_transform" class="form-control">
				<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'no') echo ' selected'; ?>>None</option>
				<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
				<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
				<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
				</select>
				</div>
				<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
			</div>	

			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
			</div>
	
			<?php echo $this->displayColorPickerField($id_var.'_wg_title_box_shadow_color', __('Widget Title Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Content Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_content_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_content_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_font_size" class="col-sm-4 col-md-4 control-label">Widget Content Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Content Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_font_color', __('Widget Content Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_color', __('Widget Content Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_hover_color', __('Widget Content Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
			</div>
		</div>
		<div class="tab-pane fade" id="widget_list_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_list_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets List Style</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_list_style" name="sdf_<?php echo $id_var; ?>_wg_custom_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-styles">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget List Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Paddings', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="col-sm-4 col-md-4 control-label">List Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_list_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_family" class="col-sm-4 col-md-4 control-label">List Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_list_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_font_color', __('List Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_color', __('List Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_hover_color', __('List Link Hover Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_hover_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links on hover', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_style" class="col-sm-4 col-md-4 control-label"><?php _e('List Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_list_style" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'default', "btn-sdf-grey")?>" value="default">Default</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'none', "btn-sdf-grey")?>" value="none">None</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'square', "btn-sdf-grey")?>" value="square">Square</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'circle', "btn-sdf-grey")?>" value="circle">Circle</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'disc', "btn-sdf-grey")?>" value="disc">Disc</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'icon', "btn-sdf-grey")?>" value="icon">FA Icon</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_list_style" name="sdf_<?php echo $id_var; ?>_wg_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
				
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-icon">
				<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose List Icon', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<?php
					if(isset($this->_ultIconsUnicode) && is_array($this->_ultIconsUnicode)){
						$list_icon_picker = '';
						foreach($this->_ultIconsUnicode as $key => $val){
							$list_icon_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_icon'],$val, "btn-sdf-grey") . '" value="'.$val.'"><i class="fa fa-lg '. $key. '"></i></button>';
						}
						echo $list_icon_picker;
					}
					?>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_icon" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Icon Margins', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input name="sdf_<?php echo $id_var; ?>_wg_list_icon_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
				</div>
			
				<div class="form-group">
					<label for="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="col-sm-4 col-md-4 control-label">List Icon Font Size</label>
					<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="col-sm-8 col-md-8">
							<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
							</div>
							<div class="col-sm-4 col-md-4">
							<select name="sdf_<?php echo $id_var; ?>_wg_list_icon_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>	
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Padding', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Padding for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Line Height', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Line Height for List', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Width', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
					
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_item_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Radius', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set List Colors', SDF_TXT_DOMAIN); ?></p>
			</div>
				
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_color', __('List Items Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_color', __('List Items Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_box_shadow_color', __('List Items Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_hover_color', __('List Items Hover Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_hover_color', __('List Items Hover Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_hover_box_shadow_color', __('List Items Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>			
		
			</div>
		
		</div>
	</div>
	
	<?php endif; ?>
	<?php endforeach; ?>	
                       
      </div>
    </div>
  </div>
  
  <?php 
	$id = 'logo';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse" data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Logo and Tagline', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
      <div class="panel-body">

		<div class="form-group">
			<label for="logo_align_<?php echo $id_var; ?>" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="logo_align_<?php echo $id_var; ?>" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_align']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
			
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Padding Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_logo_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_top']) echo $this->_currentSettings['typography'][$id_var.'_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Padding Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_logo_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_bottom']) echo $this->_currentSettings['typography'][$id_var.'_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="hr"></div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_heading" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Logo Heading Tag', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_heading" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_headingsList) && is_array($this->_headingsList)){
					$this->_headingsList['none'] = 'None';
					$tag_picker = '';
					foreach($this->_headingsList as $key => $val){
						$tag_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography'][$id_var.'_heading'],$key, "btn-sdf-grey") . '" value="'.$key.'">'. $val. '</button>';
					}
					echo $tag_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_heading" value="<?php echo $this->_currentSettings['typography'][$id_var.'_heading']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_logo_font_size" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_logo_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography'][$id_var.'_font_size']) echo $this->_currentSettings['typography'][$id_var.'_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_logo_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_logo_font_family" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_logo_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Logo Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_font_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>  
	  
		<?php echo $this->displayColorPickerField('logo_font_color', __('Logo Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('logo_font_hover_color', __('Logo Text Hover Color', SDF_TXT_DOMAIN)); ?>
                               
		<div class="hr"></div>

		<?php echo $this->displayColorPickerField('logo_link_color', __('Logo Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('logo_link_hover_color', __('Logo Link Hover Color', SDF_TXT_DOMAIN)); ?> 

		<div class="bs-callout bs-callout-grey">
		<h4>Logo Background Settings</h4>
		<p>Set Custom Background for Logo Area</p>
		</div>

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Logo Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Logo Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Logo Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Logo Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Logo Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Logo Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>	
		
		<div class="bs-callout bs-callout-grey">
			<h4>Tagline Styling</h4>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_tagline_margin" class="col-sm-4 col-md-4 control-label">Tagline Margin</label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_<?php echo $id_var; ?>_tagline_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_tagline_margin']) echo $this->_currentSettings['typography'][$id_var.'_tagline_margin']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_tagline_heading" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Tagline Heading Tag', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_tagline_heading" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_headingsList) && is_array($this->_headingsList)){
					$this->_headingsList['p'] = 'p';
					$tag_picker = '';
					foreach($this->_headingsList as $key => $val){
						$tag_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography'][$id_var.'_tagline_heading'],$key, "btn-sdf-grey") . '" value="'.$key.'">'. $val. '</button>';
					}
					echo $tag_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_tagline_heading" value="<?php echo $this->_currentSettings['typography'][$id_var.'_tagline_heading']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_tagline_font_size" class="col-sm-4 col-md-4 control-label"><?php _e('Tagline Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_<?php echo $id_var; ?>_tagline_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography'][$id_var.'_tagline_font_size']) echo $this->_currentSettings['typography'][$id_var.'_tagline_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_<?php echo $id_var; ?>_tagline_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography'][$id_var.'_tagline_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_tagline_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_tagline_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_tagline_font_family" class="col-sm-4 col-md-4 control-label"><?php _e('Tagline Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_tagline_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_tagline_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_tagline_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Tagline Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_tagline_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_tagline_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_tagline_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_tagline_font_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_tagline_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>  
	  
		<?php echo $this->displayColorPickerField($id_var.'_tagline_font_color', __('Tagline Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_tagline_font_hover_color', __('Tagline Text Hover Color', SDF_TXT_DOMAIN)); ?>
                       
      </div>
    </div>
  </div>
  	<?php 
		$id = 'theme-navbar';
		$id_var = str_replace("-", "_", urlencode(strtolower($id)));
		?>
   <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Navigation', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
    	<div class="panel-body">   
		
	<ul class="nav nav-pills">
		<li class="active"><a href="#menu_settings_tab" data-toggle="pill">Global Menu Settings</a></li>
		<li><a href="#menu_styling_tab" data-toggle="pill">Menu Styling</a></li>
		<li><a href="#menu_items_styling_tab" data-toggle="pill">Menu Items Styling</a></li>
		<li><a href="#menu_drop_styling_tab" data-toggle="pill">Menu Dropdown Styling</a></li>
	</ul>
	<div class="tab-content style-pills-content">
	<div class="tab-pane fade in active" id="menu_settings_tab">

		<div class="bs-callout bs-callout-grey">
		<p>Set Menu Alignment, Layout and Font Options</p>
		</div>
		
		<div class="form-group">
			<label for="menu_align_<?php echo $id_var; ?>" class="col-sm-4 col-md-4 control-label"><?php _e('Primary Menu Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="menu_align_<?php echo $id_var; ?>" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				</div>
				<input type="hidden" name="sdf_menu_alignment" value="<?php echo $this->_currentSettings['navigation']['menu_alignment']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="menu_align_2_<?php echo $id_var; ?>" class="col-sm-4 col-md-4 control-label"><?php _e('Secondary Menu Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="menu_align_2_<?php echo $id_var; ?>" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment_2'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment_2'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_alignment_2'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				</div>
				<input type="hidden" name="sdf_menu_alignment_2" value="<?php echo $this->_currentSettings['navigation']['menu_alignment_2']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_navbar_append_search" class="col-sm-4 col-md-4 control-label"><?php _e('Append Search to Navigation', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_navbar_append_search" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_append_search'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_append_search'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" name="sdf_theme_navbar_append_search" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_append_search']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_menu_layout" class="col-sm-4 col-md-4 control-label">Menu Layout</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_menu_layout" name="sdf_menu_layout" class="form-control">
			<option value="default"<?php if($this->_currentSettings['navigation']['menu_layout'] == 'default') echo ' selected'; ?>>Default Menu</option>
			<option value="sticky_menu"<?php if($this->_currentSettings['navigation']['menu_layout'] == 'sticky_menu') echo ' selected'; ?>>Sticky Menu</option>
			<option value="sticky_overlay"<?php if($this->_currentSettings['navigation']['menu_layout'] == 'sticky_overlay') echo ' selected'; ?>>Sticky Overlay Menu</option>
			<option value="fixed_top"<?php if($this->_currentSettings['navigation']['menu_layout'] == 'fixed_top') echo ' selected'; ?>>Fixed to Top Menu</option>
			<option value="fixed_bottom"<?php if($this->_currentSettings['navigation']['menu_layout'] == 'fixed_bottom') echo ' selected'; ?>>Fixed to Bottom Menu</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_sticky_menu_opacity" class="col-sm-4 col-md-4 control-label">Sticky Menu Opacity</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_sticky_menu_opacity" name="sdf_sticky_menu_opacity" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['sticky_menu_opacity']) echo $this->_currentSettings['navigation']['sticky_menu_opacity']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" style="display:none">
			<label for="sdf_menu_style" class="col-sm-4 col-md-4 control-label">Menu Style</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_menu_style" name="sdf_menu_style" class="form-control"><?php echo $this->createMenuPicker( $this->_currentSettings['navigation']['menu_style']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		<div class="form-group">
			<label for="sdf_theme_navbar_family" class="col-sm-4 col-md-4 control-label">Navigation Link Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_navbar_family" name="sdf_theme_navbar_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['navigation']['menu_settings'][$id_var.'_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Links Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_weight" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_size" class="col-sm-4 col-md-4 control-label">Navigation Link Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_<?php echo $id_var; ?>_size" name="sdf_<?php echo $id_var; ?>_size" class="form-control" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_size']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_navbar_size_type" class="form-control"><option<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_letter_spacing" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Items Letter Spacing', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<input type="text" class="form-control" id="sdf_<?php echo $id_var; ?>_letter_spacing" name="sdf_<?php echo $id_var; ?>_letter_spacing" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_letter_spacing'] ) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_letter_spacing']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_transform" name="sdf_<?php echo $id_var; ?>_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Items Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_link_text_decoration" name="sdf_<?php echo $id_var; ?>_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['navigation']['menu_settings'][$id_var.'_link_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for menu links', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Items Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['navigation']['menu_settings'][$id_var.'_link_hover_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for menu links hover', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_transition" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Transition Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_<?php echo $id_var; ?>_transition" name="sdf_<?php echo $id_var; ?>_transition" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_transition']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_transition']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='[transition-property] [transition-duration] [transition-timing-function] [transition-delay]<br /> (e.g. "width 1s linear 2s")'></i>The transition property is a shorthand property used to represent up to four transition-related longhand properties. You may comma separate value sets to do different transitions on different properties (e.g. "width 2s, height 2s").</div>
		</div>
		
	</div>
	<div class="tab-pane fade" id="menu_styling_tab">

		<div class="bs-callout bs-callout-grey">
		<h4>Menu Background, Shadow, Margins and Borders</h4>
		</div>

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Menu Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		<?php echo $this->displayColorPickerField('theme_navbar_bg_color', __('Menu Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_navbar_hover_bg_color', __('Menu Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_navbar_sticky_bg_color', __('Sticky Menu Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_navbar_top_bg_color', __('Menu Background Color Top', SDF_TXT_DOMAIN), __('Use these values to create a linear vertical gradient for menu. Gradient is a blend of top and bottom colors.', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_navbar_bottom_bg_color', __('Menu Background Color Bottom', SDF_TXT_DOMAIN), __('Use these values to create a linear vertical gradient for menu. Gradient is a blend of top and bottom colors.', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['navigation']['menu_settings'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p>Menu Shadow</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Menu Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_box_shadow']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Menu Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
			<p>Menu Margins</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_margin_top" class="col-sm-4 col-md-4 control-label">Menu Top Margin</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_margin_top" name="sdf_<?php echo $id_var; ?>_margin_top" class="form-control" type="text" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_margin_top']; ?>" />		
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
			
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_margin_bottom" class="col-sm-4 col-md-4 control-label">Menu Bottom Margin</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_margin_bottom" name="sdf_<?php echo $id_var; ?>_margin_bottom" class="form-control" type="text" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_margin_bottom']; ?>" />				
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p>Menu Borders</p>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_border_radius" class="col-sm-4 col-md-4 control-label">Menu Border Radius</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_border_radius" name="sdf_<?php echo $id_var; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_radius']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_radius']; ?>" />					
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
			
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_border_width" class="col-sm-4 col-md-4 control-label">Menu Border Width</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_border_width" name="sdf_<?php echo $id_var; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_width']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_border_width']; ?>" />			
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_border_color', __('Menu Border Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
		
	</div>
	<div class="tab-pane fade" id="menu_items_styling_tab">

		<div class="bs-callout bs-callout-grey">
		<h4>Menu Items Margins, Paddings, Background, Shadow Settings</h4>
		</div>
		
		<?php foreach($this->_ultSettings['menu_settings'] as  $option_key => $option_fields) : ?>
		
			<?php if( $option_fields['type'] != 'callout' ) : ?>
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php echo $option_fields['name']; ?></label>
					<div class="col-sm-4 col-md-3">
					<?php echo $this->displaySettingsField($option_key, $this->_currentSettings['navigation']['menu_settings'][$option_key], $option_fields)?>
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php if( $option_fields['type'] == 'color' ) echo '<i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="right" data-toggle="tooltip" data-original-title="Use Picker, Enter hex Value or use Preset Color. Picker has opacity slider so you can adjust color alpha value, 1 for solid(default), .01-.99 for more or less opacity."></i>'; ?><?php if( isset($option_fields['help']) ) echo $option_fields['help']; ?></div>
				</div>
			<?php else: ?>
				<?php echo $this->displaySettingsField($option_key, '', $option_fields)?>
			<?php endif; ?>
		
		<?php endforeach; ?>
		
	</div>
	<div class="tab-pane fade" id="menu_drop_styling_tab">
		
		<div class="bs-callout bs-callout-grey">
		<p>Set Dropdown Background and Link Colors</p>
		</div>	
		<?php echo $this->displayColorPickerField( 'theme_navbar_drop_color', __('Dropdown Menu Background Color', SDF_TXT_DOMAIN) ); ?>
		<?php echo $this->displayColorPickerField( 'theme_navbar_drop_hover_color', __('Dropdown Menu Background Hover Color', SDF_TXT_DOMAIN) ); ?>
		
		<div class="form-group">
			<label for="sdf_theme_navbar_drop_transition" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Transition Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_navbar_drop_transition" name="sdf_theme_navbar_drop_transition" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transition']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transition']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='[transition-property] [transition-duration] [transition-timing-function] [transition-delay]<br /> (e.g. "width 1s linear 2s")'></i>The transition property is a shorthand property used to represent up to four transition-related longhand properties. You may comma separate value sets to do different transitions on different properties (e.g. "width 2s, height 2s").</div>
		</div>

		<div class="form-group">
			<label for="sdf_theme_navbar_drop_link_height" class="col-sm-4 col-md-4 control-label">Dropdown Menu Link Height</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_navbar_drop_link_height" name="sdf_theme_navbar_drop_link_height" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_link_height']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_link_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying dropdown menu link height (e.g. "30px")</div>
		</div>

		<div class="form-group">
			<label for="sdf_theme_navbar_drop_link_padding" class="col-sm-4 col-md-4 control-label">Dropdown Menu Link Padding</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_navbar_drop_link_padding" name="sdf_theme_navbar_drop_link_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_link_padding']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_link_padding']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying dropdown menu link padding (e.g. "0 15px")</div>
		</div>
             
		<?php echo $this->displayColorPickerField( 'theme_navbar_drop_acolor', __('Dropdown Menu Link Color', SDF_TXT_DOMAIN) ); ?>
		<?php echo $this->displayColorPickerField( 'theme_navbar_drop_ahover', __('Dropdown Menu Link Hover Color', SDF_TXT_DOMAIN) ); ?>

		<div class="form-group">
			<label for="sdf_theme_navbar_drop_family" class="col-sm-4 col-md-4 control-label">Dropdown Menu Link Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_navbar_drop_family" name="sdf_theme_navbar_drop_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_navbar_drop_size" class="col-sm-4 col-md-4 control-label">Dropdown Menu Link Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_navbar_drop_size" name="sdf_theme_navbar_drop_size" class="form-control" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_size']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_navbar_drop_size_type" class="form-control"><option<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Links Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_drop_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_drop_weight" value="<?php echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_letter_spacing" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Links Letter Spacing', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<input type="text" class="form-control" id="sdf_<?php echo $id_var; ?>_drop_letter_spacing" name="sdf_<?php echo $id_var; ?>_drop_letter_spacing" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_letter_spacing'] ) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_letter_spacing']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_navbar_drop_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_navbar_drop_transform" name="sdf_theme_navbar_drop_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>	
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Items Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_drop_text_decoration" name="sdf_<?php echo $id_var; ?>_drop_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for menu links', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Dropdown Menu Items Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_drop_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_drop_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_hover_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for menu links hover', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
		<p>Set Menu Dropdown Box Shadow defined by 2-4 length values and color value</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_navbar_drop_box_shadow" class="col-sm-4 col-md-4 control-label">Dropdown Menu Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_navbar_drop_box_shadow" name="sdf_theme_navbar_drop_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_box_shadow']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField( 'theme_navbar_drop_box_shadow_color', __('Dropdown Menu Box Shadow Color', SDF_TXT_DOMAIN) ); ?>
		
		<div class="bs-callout bs-callout-grey">
			<p>Set Menu Dropdown Borders</p>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Menu Dropdown Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_navbar_drop_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['navigation']['menu_settings']['theme_navbar_drop_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['navigation']['menu_settings']['theme_navbar_drop_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['navigation']['menu_settings']['theme_navbar_drop_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['navigation']['menu_settings']['theme_navbar_drop_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_border_radius" class="col-sm-4 col-md-4 control-label">Menu Dropdown Border Radius</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_drop_border_radius" name="sdf_<?php echo $id_var; ?>_drop_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_border_radius']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_border_radius']; ?>" />					
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
			
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_drop_border_width" class="col-sm-4 col-md-4 control-label">Menu Dropdown Border Width</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_drop_border_width" name="sdf_<?php echo $id_var; ?>_drop_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_border_width']) echo $this->_currentSettings['navigation']['menu_settings'][$id_var.'_drop_border_width']; ?>" />			
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_drop_border_color', __('Menu Dropdown Border Color', SDF_TXT_DOMAIN)); ?>
                          
	</div>       
	</div>  
			
		</div>       
      </div>
    </div>

  <?php 
	$id = 'footer';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collap_<?php echo $id_var; ?>" href="#collap_<?php echo $id_var; ?>">
       <?php _e('Footer', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collap_<?php echo $id_var; ?>" class="panel-collapse collapse">
      <div class="panel-body">  
		<div class="bs-callout bs-callout-grey">
		<h4>Footer Font Settings</h4>
		<p>Set Custom Font Family, Font Sizes and Font Colors for Footer Area.</p>
		</div>

		<div class="form-group">
			<label for="sdf_footer_font_size" class="col-sm-4 col-md-4 control-label">Footer Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_footer_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography']['footer_font_size']) echo $this->_currentSettings['typography']['footer_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_footer_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography']['footer_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography']['footer_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['footer_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_footer_font_family" class="col-sm-4 col-md-4 control-label">Footer Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_footer_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['footer_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>  
		
		<div class="form-group">
			<label for="sdf_footer_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Footer Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_footer_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['footer_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['footer_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_footer_font_weight" value="<?php echo $this->_currentSettings['typography']['footer_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<?php echo $this->displayColorPickerField( 'footer_font_color', __('Footer Text Color', SDF_TXT_DOMAIN) ); ?>
		<?php echo $this->displayColorPickerField( 'footer_font_hover_color', __('Footer Text Hover Color', SDF_TXT_DOMAIN) ); ?>
		<?php echo $this->displayColorPickerField( 'footer_link_color', __('Footer Link Color', SDF_TXT_DOMAIN) ); ?>
		<?php echo $this->displayColorPickerField( 'footer_link_hover_color', __('Footer Link Hover Color', SDF_TXT_DOMAIN) ); ?>
                               
        <div class="hr"></div>
		
		<div class="bs-callout bs-callout-grey">
		<h4>Footer Background Settings</h4>
		<p>Set Custom Background for Footer Area</p>
		</div>

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Footer Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField( $id_var.'_bg_color', __('Footer Background Color', SDF_TXT_DOMAIN) ); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Footer Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Footer Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Footer Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField( $id_var.'_box_shadow_color', __('Footer Box Shadow Color', SDF_TXT_DOMAIN) ); ?>
		
		</div>
		
		<div class="hr"></div>

		<div class="bs-callout bs-callout-grey">
		<h4>Footer Bottom Font Settings</h4>
		<p>Set Custom Font Family, Font Sizes and Font Colors for Footer Bottom Area.</p>
		</div>

		<div class="form-group">
			<label for="sdf_footer_bottom_font_size" class="col-sm-4 col-md-4 control-label">Footer Bottom Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_footer_bottom_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography']['footer_bottom_font_size']) echo $this->_currentSettings['typography']['footer_bottom_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_footer_bottom_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography']['footer_bottom_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography']['footer_bottom_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['footer_bottom_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_footer_bottom_font_family" class="col-sm-4 col-md-4 control-label">Footer Bottom Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_footer_bottom_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['footer_bottom_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_footer_bottom_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Footer Bottom Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_footer_bottom_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['footer_bottom_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['footer_bottom_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_footer_bottom_font_weight" value="<?php echo $this->_currentSettings['typography']['footer_bottom_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<?php echo $this->displayColorPickerField('footer_bottom_font_color', __('Footer Bottom Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('footer_bottom_font_hover_color', __('Footer Bottom Text Hover Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('footer_bottom_link_color', __('Footer Bottom Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('footer_bottom_link_hover_color', __('Footer Bottom Link Hover Color', SDF_TXT_DOMAIN)); ?>
                               
        <div class="hr"></div>
		 <?php 
	$id = 'footer-bottom';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
		<div class="bs-callout bs-callout-grey">
		<h4>Footer Bottom Background Settings</h4>
		<p>Set Custom Background for Footer Bottom Area</p>
		</div>

		<div class="form-group" id="collap_<?php echo $id_var; ?>">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Footer Bottom Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Footer Bottom Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Footer Bottom Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Footer Bottom Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Footer Bottom Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Footer Bottom Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>  

		<div class="bs-callout bs-callout-grey">
	<h4>Footer Widget Areas Styling Settings</h4>
	<p>Set Widgets Alignment, Equal Height Options and Custom Background for each area or just leave it transparent with current body background.<br />
	Furthermore, customize widgets per area...</p>
	</div>

	<?php foreach ($this->_sdfSidebars as $id => $name) :

	if (in_array($id, array('footer-widget-area', 'footer-bottom-widget-area'))) :
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
	<div class="bs-callout bs-callout-grey">
		<h4><?php echo $name; ?></h4>
	</div>
	
	<ul class="nav nav-pills">
		<li class="active"><a href="#area_<?php echo $id_var; ?>" data-toggle="pill">Customize Area</a></li>
		<li><a href="#widget_style_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Styling</a></li>
		<li><a href="#widget_typo_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets Typography</a></li>
		<li><a href="#widget_list_<?php echo $id_var; ?>" data-toggle="pill">Customize Widgets List Styling</a></li>
	</ul>
	<div class="tab-content style-pills-content">
	<div class="tab-pane fade in active" id="area_<?php echo $id_var; ?>">
	
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_height" class="col-sm-4 col-md-4 control-label">Area Height</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_height" name="sdf_<?php echo $id_var; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_height']) echo $this->_currentSettings['typography'][$id_var.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Height</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_padding_top" class="col-sm-4 col-md-4 control-label">Area Top Padding</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_padding_top" name="sdf_<?php echo $id_var; ?>_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_top']) echo $this->_currentSettings['typography'][$id_var.'_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Top Padding</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_padding_bottom" class="col-sm-4 col-md-4 control-label">Area Bottom Padding</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_padding_bottom" name="sdf_<?php echo $id_var; ?>_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding_bottom']) echo $this->_currentSettings['typography'][$id_var.'_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i> Define Area Bottom Padding</div>
		</div>
	
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Widgets Alignment', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_align']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
	
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_equal_heights" class="col-sm-4 col-md-4 control-label">Set Equal Widget Heights</label>
			<div class="col-sm-4 col-md-3">
				<div id="" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'off', "btn-sdf-grey")?>" value="off">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_equal_heights'],'per_area', "btn-sdf-grey")?>" value="per_area">Yes</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_equal_heights" name="sdf_<?php echo $id_var; ?>_equal_heights" value="<?php echo $this->_currentSettings['typography'][$id_var.'_equal_heights']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Area Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">
		
		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Area Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Area Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Area Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Area Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Area Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
	</div>
	<div class="tab-pane fade" id="widget_style_<?php echo $id_var; ?>">
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Style</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_style" name="sdf_<?php echo $id_var; ?>_wg_custom_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_style']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-wg-styles">
		
		<?php echo $this->displayColorPickerField($id_var.'_wg_background_color', __('Widget Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_bg_image" class="col-md-4 control-label">Widget Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_wg_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="col-sm-4 col-md-4 control-label">Widget Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_wg_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_wg_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="col-sm-4 col-md-4 control-label">Widget Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_wg_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_wg_box_shadow_color', __('Widget Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				 
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Width</label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_<?php echo $id_var; ?>_wg_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
			
		<?php echo $this->displayColorPickerField($id_var.'_wg_border_color', __('Widget Border Color', SDF_TXT_DOMAIN)); ?>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Style</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_wg_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Radius</label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_<?php echo $id_var; ?>_wg_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_border_radius']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		</div>
	</div>
	<div class="tab-pane fade" id="widget_typo_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_typo" class="col-sm-4 col-md-4 control-label">Set Custom Widgets Typography</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_typo'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_typo" name="sdf_<?php echo $id_var; ?>_wg_custom_typo" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_typo']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-typography">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Title Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Title Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Top</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_top" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_top']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Margin Bottom</label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_title_margin_bottom" class="form-control" type="text" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_margin_bottom']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>		
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_size" class="col-sm-4 col-md-4 control-label">Widget Title Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_title_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_title_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_title_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Title Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_title_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_title_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_title_color', __('Widget Title Font Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Font Weight', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_title_weight" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_title_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_title_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_title_weight']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Text-transform', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_title_transform" name="sdf_<?php echo $id_var; ?>_wg_title_transform" class="form-control">
				<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'no') echo ' selected'; ?>>None</option>
				<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
				<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
				<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
				</select>
				</div>
				<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
			</div>	

			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Widget Title Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_title_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_title_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
			</div>
	
			<?php echo $this->displayColorPickerField($id_var.'_wg_title_box_shadow_color', __('Widget Title Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget Content Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_content_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_content_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_content_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_font_size" class="col-sm-4 col-md-4 control-label">Widget Content Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Content Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_font_color', __('Widget Content Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_color', __('Widget Content Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_link_hover_color', __('Widget Content Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
			</div>
		</div>
		<div class="tab-pane fade" id="widget_list_<?php echo $id_var; ?>">
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_custom_list_style" class="col-sm-4 col-md-4 control-label">Set Custom Widgets List Style</label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_custom_list_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_custom_list_style" name="sdf_<?php echo $id_var; ?>_wg_custom_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_custom_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-styles">
			
			<div class="bs-callout bs-callout-grey">
			<p><?php _e('Widget List Styling', SDF_TXT_DOMAIN); ?></p>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Paddings', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Content Alignment', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_align'],'justify', "btn-sdf-grey")?>" value="justify"><i class="fa fa-align-justify"></i></button>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_align" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_align']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
					
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="col-sm-4 col-md-4 control-label">List Font Size</label>
				<div class="col-sm-4 col-md-3">
					<div class="row">
						<div class="col-sm-8 col-md-8">
						<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_font_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_font_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
						</div>
						<div class="col-sm-4 col-md-4">
						<select name="sdf_<?php echo $id_var; ?>_wg_list_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_family" class="col-sm-4 col-md-4 control-label">List Font Family</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_wg_list_family']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_font_color', __('List Text Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_color', __('List Link Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_link_hover_color', __('List Link Hover Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('List Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_wg_list_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_wg_list_link_hover_text_decoration']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for List Links on hover', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_style" class="col-sm-4 col-md-4 control-label"><?php _e('List Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div id="sdf_<?php echo $id_var; ?>_wg_list_style" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'default', "btn-sdf-grey")?>" value="default">Default</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'none', "btn-sdf-grey")?>" value="none">None</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'square', "btn-sdf-grey")?>" value="square">Square</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'circle', "btn-sdf-grey")?>" value="circle">Circle</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'disc', "btn-sdf-grey")?>" value="disc">Disc</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_style'],'icon', "btn-sdf-grey")?>" value="icon">FA Icon</button>
					</div>
					<input type="hidden" id="sdf_<?php echo $id_var; ?>_wg_list_style" name="sdf_<?php echo $id_var; ?>_wg_list_style" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_style']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
				
			<div id="sdf-<?php echo $id; ?>-custom-wg-list-icon">
				<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose List Icon', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
					<div class="btn-group sdf_toggle">
					<?php
					if(isset($this->_ultIconsUnicode) && is_array($this->_ultIconsUnicode)){
						$list_icon_picker = '';
						foreach($this->_ultIconsUnicode as $key => $val){
							$list_icon_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography'][$id_var.'_wg_list_icon'],$val, "btn-sdf-grey") . '" value="'.$val.'"><i class="fa fa-lg '. $key. '"></i></button>';
						}
						echo $list_icon_picker;
					}
					?>
					</div>
					<input type="hidden" name="sdf_<?php echo $id_var; ?>_wg_list_icon" value="<?php echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>	  
				<div class="form-group">
					<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Icon Margins', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input name="sdf_<?php echo $id_var; ?>_wg_list_icon_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_margin']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
				</div>
			
				<div class="form-group">
					<label for="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="col-sm-4 col-md-4 control-label">List Icon Font Size</label>
					<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="col-sm-8 col-md-8">
							<input type="text" name="sdf_<?php echo $id_var; ?>_wg_list_icon_size" class="form-control" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size'] ) echo $this->_currentSettings['typography'][$id_var.'_wg_list_icon_size']; ?>" placeholder="<?php echo $sdf_theme_font_size; ?> Default"  />
							</div>
							<div class="col-sm-4 col-md-4">
							<select name="sdf_<?php echo $id_var; ?>_wg_list_icon_size_type" class="form-control"><option<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_icon_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>	
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Padding', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_padding']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Padding for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Margins', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_margin']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margins for List Items', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Line Height', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_line_height']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Line Height for List', SDF_TXT_DOMAIN); ?></div>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Width', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_width']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
					
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Style', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_wg_list_item_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Border Radius', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input name="sdf_<?php echo $id_var; ?>_wg_list_item_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_border_radius']; ?>" placeholder="0px Default" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
			
			<div class="bs-callout bs-callout-grey">
				<p><?php _e('Set List Colors', SDF_TXT_DOMAIN); ?></p>
			</div>
				
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_color', __('List Items Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_color', __('List Items Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_box_shadow_color', __('List Items Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_background_hover_color', __('List Items Hover Background Color', SDF_TXT_DOMAIN)); ?>
			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_border_hover_color', __('List Items Hover Border Color', SDF_TXT_DOMAIN)); ?>
			
			<div class="form-group">
				<label for="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('List Items Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" name="sdf_<?php echo $id_var; ?>_wg_list_item_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_wg_list_item_hover_box_shadow']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
			</div>

			<?php echo $this->displayColorPickerField($id_var.'_wg_list_item_hover_box_shadow_color', __('List Items Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>			
		
			</div>
		
		</div>
	</div>
		
	
	<?php endif; ?>
	<?php endforeach; ?>
                       
      </div>
    </div>
  </div> 	
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsegrid" href="#collapsegrid">
       <?php _e('Posts/Portfolio Grid Images and Image Overlay Styling', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapsegrid" class="panel-collapse collapse">
      <div class="panel-body">
                             
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set CSS3 Transition', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_transition" class="col-sm-4 col-md-4 control-label"><?php _e('Grid Transition Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_transition" name="sdf_theme_grid_transition" value="<?php if($this->_currentSettings['typography']['theme_grid_transition']) echo $this->_currentSettings['typography']['theme_grid_transition']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='[transition-property] [transition-duration] [transition-timing-function] [transition-delay]<br /> (e.g. "width 1s linear 2s")'></i>The transition property is a shorthand property used to represent up to four transition-related longhand properties. You may comma separate value sets to do different transitions on different properties (e.g. "width 2s, height 2s").</div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Image CSS3 Transform/Opacity', SDF_TXT_DOMAIN); ?></p>
	  </div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Image Transform Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_transform" name="sdf_theme_grid_transform" value="<?php if($this->_currentSettings['typography']['theme_grid_transform']) echo $this->_currentSettings['typography']['theme_grid_transform']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default value: none'></i>The transform property applies a 2D or 3D transformation to an element. This property allows you to rotate, scale, move, skew, etc., elements.</div>
		</div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_transform_hover" class="col-sm-4 col-md-4 control-label"><?php _e('Image Transform on Hover Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_transform_hover" name="sdf_theme_grid_transform_hover" value="<?php if($this->_currentSettings['typography']['theme_grid_transform_hover']) echo $this->_currentSettings['typography']['theme_grid_transform_hover']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default value: none'></i>The transform property applies a 2D or 3D transformation to an element. This property allows you to rotate, scale, move, skew, etc., elements.</div>
		</div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_transform_origin" class="col-sm-4 col-md-4 control-label"><?php _e('Image Transform Origin Property', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_transform_origin" name="sdf_theme_grid_transform_origin" value="<?php if($this->_currentSettings['typography']['theme_grid_transform_origin']) echo $this->_currentSettings['typography']['theme_grid_transform_origin']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default value: 	50% 50% 0'></i>The transform-origin property allows you to change the position of transformed elements. 2D transformations can change the x- and y-axis of an element. 3D transformations can also change the z-axis of an element.</div>
		</div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_transform_style" class="col-sm-4 col-md-4 control-label"><?php _e('Image Transform Style Property', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_transform_style" name="sdf_theme_grid_transform_style" value="<?php if($this->_currentSettings['typography']['theme_grid_transform_style']) echo $this->_currentSettings['typography']['theme_grid_transform_style']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default is flat - Specifies that child elements will NOT preserve its 3D position.<br /><br />preserve-3d - Specifies that child elements will preserve its 3D position'></i>The transform-style property specifies how nested elements are rendered in 3D space.</div>
		</div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_image_opacity" class="col-sm-4 col-md-4 control-label"><?php _e('Image Opacity', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_image_opacity" name="sdf_theme_grid_image_opacity" value="<?php if($this->_currentSettings['typography']['theme_grid_image_opacity']) echo $this->_currentSettings['typography']['theme_grid_image_opacity']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default value: 1'></i>The opacity property sets the opacity level for an element.<br/>The opacity-level describes the transparency-level, where 1 is not transparant at all, 0.5 is 50% see-through, and 0 is completely transparent.</div>
		</div>
                             
		<div class="form-group">
			<label for="sdf_theme_grid_image_opacity_hover" class="col-sm-4 col-md-4 control-label"><?php _e('Image Opacity on Hover', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_grid_image_opacity_hover" name="sdf_theme_grid_image_opacity_hover" value="<?php if($this->_currentSettings['typography']['theme_grid_image_opacity_hover']) echo $this->_currentSettings['typography']['theme_grid_image_opacity_hover']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='Default value: 1'></i>The opacity property sets the opacity level for an element.<br/>The opacity-level describes the transparency-level, where 1 is not transparant at all, 0.5 is 50% see-through, and 0 is completely transparent.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_image_grayscale" class="col-sm-4 col-md-4 control-label"><?php _e('Grayscale Grid Images', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_grid_image_grayscale" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_grid_image_grayscale'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_grid_image_grayscale'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" name="sdf_theme_grid_image_grayscale" value="<?php echo $this->_currentSettings['typography']['theme_grid_image_grayscale']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Image Overlay Color Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<?php echo $this->displayColorPickerField('theme_grid_overlay_color', __('Grid Image Overlay Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Image Overlay Buttons Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<?php $id = 'item_overlay_button'; ?>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_icon_size" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Button\'s Icon Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_grid_<?php echo $id; ?>_icon_size" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_ultIconSizes) && is_array($this->_ultIconSizes)){
					$size_picker = '';
					foreach($this->_ultIconSizes as $key => $val){
						$size_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography']['theme_grid_'.$id.'_icon_size'],$val, "btn-sdf-grey") . '" value="'.$val.'">'. $key. '</button>';
					}
					echo $size_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_theme_grid_<?php echo $id; ?>_icon_size" value="<?php echo $this->_currentSettings['typography']['theme_grid_'.$id.'_icon_size']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('To increase icon sizes relative to their container, use the Large (33% increase), 2X, 3X, 4X, or 5X size.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_family" class="col-sm-4 col-md-4 control-label">Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_grid_<?php echo $id; ?>_text_family" name="sdf_theme_grid_<?php echo $id; ?>_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_grid_'.$id.'_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_size" class="col-sm-4 col-md-4 control-label">Button Text Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_grid_<?php echo $id; ?>_text_size" name="sdf_theme_grid_<?php echo $id; ?>_text_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_size']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_text_size']; ?>"  placeholder="<?php echo $sdf_theme_grid_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_grid_<?php echo $id; ?>_text_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_grid_<?php echo $id; ?>_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_grid_'.$id.'_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_grid_'.$id.'_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_grid_<?php echo $id; ?>_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_grid_'.$id.'_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_grid_<?php echo $id; ?>_text_transform" name="sdf_theme_grid_<?php echo $id; ?>_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_width']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Width for Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_height']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Height for Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Line Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_line_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_line_height']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_line_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Height for Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_margin']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_margin']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margin for Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Padding', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_padding']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_padding']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Padding for Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_width']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_border_width']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_grid_<?php echo $id; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Button Border Radius', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_grid_<?php echo $id; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_border_radius']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_border_radius']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Button Colors', SDF_TXT_DOMAIN); ?></p>
	  </div>
			
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_grid_<?php echo $id; ?>_text_shadow" name="sdf_theme_grid_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_text_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_grid_<?php echo $id; ?>_box_shadow" name="sdf_theme_grid_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_grid_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_grid_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_text_hover_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_grid_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_grid_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_grid_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_hover_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_grid_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e('Set Buttons\' Icon/Text', SDF_TXT_DOMAIN); ?></h4>
	  </div>
			
		<?php foreach ( array( 'prettyphoto_button' => 'PrettyPhoto Button', 'item_button' => 'Item Button' ) as $btn_slug => $btn_name) {
		$id = 'item_overlay_'.str_replace( "-", "_", urlencode(strtolower($btn_slug)) );
		 ?>
	
			<div class="bs-callout bs-callout-grey">
				<p><?php echo $btn_name; ?></p>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_grid_<?php echo $id; ?>_icon" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Button Icon', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<select id="sdf_theme_grid_<?php echo $id; ?>_icon" name="sdf_theme_grid_<?php echo $id; ?>_icon" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultIcons, $this->_currentSettings['typography']['theme_grid_'.$id.'_icon']); ?></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
	
			<div class="form-group">
				<label for="sdf_theme_grid_<?php echo $id; ?>_custom_text" class="col-sm-4 col-md-4 control-label"><?php _e('Set Button Text', SDF_TXT_DOMAIN); ?></label>
				<div class="col-sm-4 col-md-3">
				<input id="sdf_theme_grid_<?php echo $id; ?>_custom_text" name="sdf_theme_grid_<?php echo $id; ?>_custom_text" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_grid_'.$id.'_custom_text']) echo $this->_currentSettings['typography']['theme_grid_'.$id.'_custom_text']; ?>" />
				</div>
				<div class="col-sm-4 col-md-5 help-text"><?php _e('This text wil be added to the button in combination with icon if icon is enabled in common options above.', SDF_TXT_DOMAIN); ?></div>
			</div>
			
		<?php } ?>		
                        
      </div>
    </div>
  </div>	
	
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapbuttons" href="#collapbuttons"><?php _e('Theme Buttons', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapbuttons" class="panel-collapse collapse">
      <div class="panel-body">  
	  <div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Common Button Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>

		<div class="form-group">
			<label for="sdf_theme_button_text_family" class="col-sm-4 col-md-4 control-label">Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_button_text_family" name="sdf_theme_button_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_button_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_button_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_button_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_button_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_button_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_button_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_button_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_button_letter_spacing" class="col-sm-4 col-md-4 control-label"><?php _e('Button Text Letter Spacing', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<input type="text" class="form-control" id="sdf_theme_button_letter_spacing" name="sdf_theme_button_letter_spacing" value="<?php if($this->_currentSettings['typography']['theme_button_letter_spacing'] ) echo $this->_currentSettings['typography']['theme_button_letter_spacing']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_button_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_button_text_transform" name="sdf_theme_button_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_button_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_button_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_button_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_button_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_button_border_width" class="col-sm-4 col-md-4 control-label"><?php _e('Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_button_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_button_border_width']) echo $this->_currentSettings['typography']['theme_button_border_width']; ?>" placeholder="1px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		<div class="form-group">
			<label for="sdf_theme_button_border_style" class="col-sm-4 col-md-4 control-label"><?php _e('Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_button_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_button_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_button_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_button_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_button_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_button_transition" class="col-sm-4 col-md-4 control-label"><?php _e('Button Transition Properties', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" class="form-control" id="sdf_theme_button_transition" name="sdf_theme_button_transition" value="<?php if($this->_currentSettings['typography']['theme_button_transition']) echo $this->_currentSettings['typography']['theme_button_transition']; ?>"/>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title='[transition-property] [transition-duration] [transition-timing-function] [transition-delay]<br /> (e.g. "width 1s linear 2s")'></i>The transition property is a shorthand property used to represent up to four transition-related longhand properties. You may comma separate value sets to do different transitions on different properties (e.g. "width 2s, height 2s").</div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
		<h4><?php _e('Set Buttons Background, Box Shadow and Text Colors', SDF_TXT_DOMAIN); ?></h4>
	  </div>
	<?php foreach ($this->_sdfButtons as $id => $name) : ?>
		<div class="bs-callout bs-callout-grey">
		<p><?php echo $name; ?> Button</p>
	  </div>		
	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_button_text_shadow" class="col-sm-4 col-md-4 control-label">Text Shadow Values</label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_button_text_shadow" name="sdf_theme_<?php echo $id; ?>_button_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_button_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_button_text_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text">Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")</div>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_button_box_shadow" class="col-sm-4 col-md-4 control-label">Box Shadow Values</label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_button_box_shadow" name="sdf_theme_<?php echo $id; ?>_button_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_button_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_button_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_button_text_hover_shadow" class="col-sm-4 col-md-4 control-label">Hover Text Shadow Values</label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_button_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_button_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_button_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_button_text_hover_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text">Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")</div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_button_hover_box_shadow" class="col-sm-4 col-md-4 control-label">Hover Box Shadow Values</label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_button_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_button_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_button_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_button_hover_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
				</div>
				
				<?php echo $this->displayColorPickerField('theme_'.$id.'_button_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				
	<?php endforeach; ?>					
				
      </div>
    </div>
  </div> 
		
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsocfollow" href="#collapsocfollow"><?php _e('Social Media Follow Buttons', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapsocfollow" class="panel-collapse collapse">
		<div class="panel-body"> 
			
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Common Follow Buttons Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<?php $id = 'follow_button'; ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_icon_toggle" class="col-sm-4 col-md-4 control-label"><?php _e('Show Social Icons in Follow Buttons', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_icon_toggle" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'on', "btn-sdf-grey")?>" value="on">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'off', "btn-sdf-grey")?>" value="off">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_icon_toggle" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_icon_toggle']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('If turned off you\'ll have only button text which can be customized in per media options below.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_family" class="col-sm-4 col-md-4 control-label">Follow Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_family" name="sdf_theme_<?php echo $id; ?>_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_'.$id.'_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_size" class="col-sm-4 col-md-4 control-label">Follow Button Text Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_<?php echo $id; ?>_text_size" name="sdf_theme_<?php echo $id; ?>_text_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_<?php echo $id; ?>_text_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_transform" name="sdf_theme_<?php echo $id; ?>_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Width for Follow Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_height']) echo $this->_currentSettings['typography']['theme_'.$id.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Height for Follow Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_margin']) echo $this->_currentSettings['typography']['theme_'.$id.'_margin']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margin for Follow Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_width']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_<?php echo $id; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Follow Button Border Radius', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_radius']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_radius']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Default Follow Button Colors (or enable custom per media styling options using toggle below)', SDF_TXT_DOMAIN); ?></p>
	  </div>
			
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Custom (per media) Follow Button Styles', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<div class="form-group" id="theme_<?php echo $id; ?>_custom_style_cont">
			<label for="sdf_theme_<?php echo $id; ?>_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Follow Buttons Styles</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" id="sdf_theme_<?php echo $id; ?>_custom_style" name="sdf_theme_<?php echo $id; ?>_custom_style" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_custom_style']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-theme-custom-follow-button-styles">
			
			<?php foreach ( $this->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
			$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_follow_button';
			 ?>
		
				<div class="bs-callout bs-callout-grey">
					<p><?php echo $soc_name; ?> Follow Button<?php echo (isset($this->_sdfSocialBrandColors[$soc_slug])) ? '<br /><small>Brand Color</small> <code>'.$this->_sdfSocialBrandColors[$soc_slug].'</code>' : ''; ?></p>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_custom_text" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Button Text', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_custom_text" name="sdf_theme_<?php echo $id; ?>_custom_text" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_custom_text']) echo $this->_currentSettings['typography']['theme_'.$id.'_custom_text']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('This text wil be added to the button in combination with icon if icon is enabled in common options above.', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				
			<?php } ?>
			
		</div>
			
		</div>
    </div>
  </div>
		
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsocshare" href="#collapsocshare"><?php _e('Social Media Sharing Buttons', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapsocshare" class="panel-collapse collapse">
		<div class="panel-body"> 
			
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Common Sharing Buttons Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<?php $id = 'sharing_button'; ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_icon_toggle" class="col-sm-4 col-md-4 control-label"><?php _e('Show Social Icons in Sharing Buttons', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_icon_toggle" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'on', "btn-sdf-grey")?>" value="on">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'off', "btn-sdf-grey")?>" value="off">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_icon_toggle" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_icon_toggle']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('If turned off you\'ll have only button text which can be customized in per media options below.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_icon_size" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Icon Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_icon_size" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_ultIconSizes) && is_array($this->_ultIconSizes)){
					$size_picker = '';
					foreach($this->_ultIconSizes as $key => $val){
						$size_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_size'],$val, "btn-sdf-grey") . '" value="'.$val.'">'. $key. '</button>';
					}
					echo $size_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_icon_size" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_icon_size']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('To increase icon sizes relative to their container, use the Large (33% increase), 2X, 3X, 4X, or 5X size.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_family" class="col-sm-4 col-md-4 control-label">Sharing Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_family" name="sdf_theme_<?php echo $id; ?>_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_'.$id.'_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_size" class="col-sm-4 col-md-4 control-label">Sharing Button Text Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_<?php echo $id; ?>_text_size" name="sdf_theme_<?php echo $id; ?>_text_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_<?php echo $id; ?>_text_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_transform" name="sdf_theme_<?php echo $id; ?>_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Width for Sharing Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_height']) echo $this->_currentSettings['typography']['theme_'.$id.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use unit suffix (em, pt, px) after number value"></i> <?php _e('Set Height for Sharing Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_margin']) echo $this->_currentSettings['typography']['theme_'.$id.'_margin']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margin for Sharing Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_width']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_<?php echo $id; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sharing Button Border Radius', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_radius']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_radius']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Default Sharing Button Colors (or enable custom per media styling options using toggle below)', SDF_TXT_DOMAIN); ?></p>
	  </div>
			
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Custom (per media) Sharing Button Styles', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<div class="form-group" id="theme_<?php echo $id; ?>_custom_style_cont">
			<label for="sdf_theme_<?php echo $id; ?>_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Sharing Buttons Styles</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" id="sdf_theme_<?php echo $id; ?>_custom_style" name="sdf_theme_<?php echo $id; ?>_custom_style" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_custom_style']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-theme-custom-sharing-button-styles">
			
			<?php foreach ( $this->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
			$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_sharing_button';
			 ?>
		
				<div class="bs-callout bs-callout-grey">
					<p><?php echo $soc_name; ?> Sharing Button<?php echo (isset($this->_sdfSocialBrandColors[$soc_slug])) ? '<br /><small>Brand Color</small> <code>'.$this->_sdfSocialBrandColors[$soc_slug].'</code>' : ''; ?></p>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_custom_text" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Button Text', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_custom_text" name="sdf_theme_<?php echo $id; ?>_custom_text" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_custom_text']) echo $this->_currentSettings['typography']['theme_'.$id.'_custom_text']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('This text wil be added to the button in combination with icon if icon is enabled in common options above.', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				
			<?php } ?>
			
		</div>
			
		</div>
    </div>
  </div>
		
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collappostformat" href="#collappostformat"><?php _e('Blog Post Format Buttons', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collappostformat" class="panel-collapse collapse">
		<div class="panel-body"> 
			
		<div class="bs-callout bs-callout-grey">
		<p><?php _e('Set Common Post Format Buttons Styling', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<?php $id = 'post_format_button'; ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_icon_toggle" class="col-sm-4 col-md-4 control-label"><?php _e('Show Social Icons in Post Format Buttons', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_icon_toggle" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'on', "btn-sdf-grey")?>" value="on">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_toggle'],'off', "btn-sdf-grey")?>" value="off">Off</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_icon_toggle" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_icon_toggle']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('If turned off you\'ll have only button text which can be customized in per format options below.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_icon_size" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Icon Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_icon_size" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_ultIconSizes) && is_array($this->_ultIconSizes)){
					$size_picker = '';
					foreach($this->_ultIconSizes as $key => $val){
						$size_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_icon_size'],$val, "btn-sdf-grey") . '" value="'.$val.'">'. $key. '</button>';
					}
					echo $size_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_icon_size" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_icon_size']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('To increase icon sizes relative to their container, use the Large (33% increase), 2X, 3X, 4X, or 5X size.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_family" class="col-sm-4 col-md-4 control-label">Post Format Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_family" name="sdf_theme_<?php echo $id; ?>_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_'.$id.'_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_size" class="col-sm-4 col-md-4 control-label">Post Format Button Text Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_<?php echo $id; ?>_text_size" name="sdf_theme_<?php echo $id; ?>_text_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_<?php echo $id; ?>_text_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_<?php echo $id; ?>_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_<?php echo $id; ?>_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_<?php echo $id; ?>_text_transform" name="sdf_theme_<?php echo $id; ?>_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Width for Post Format Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_height']) echo $this->_currentSettings['typography']['theme_'.$id.'_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Height for Post Format Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_margin']) echo $this->_currentSettings['typography']['theme_'.$id.'_margin']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Margin for Post Format Buttons', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_width']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_width']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_<?php echo $id; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Post Format Button Border Radius', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_<?php echo $id; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_border_radius']) echo $this->_currentSettings['typography']['theme_'.$id.'_border_radius']; ?>" placeholder="0 Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Default Post Format Button Colors (or enable custom per format styling options using toggle below)', SDF_TXT_DOMAIN); ?></p>
	  </div>
			
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Custom Post Format Button Styles', SDF_TXT_DOMAIN); ?></p>
	  </div>
		
		<div class="form-group" id="theme_<?php echo $id; ?>_custom_style_cont">
			<label for="sdf_theme_<?php echo $id; ?>_custom_style" class="col-sm-4 col-md-4 control-label">Set Custom Post Format Buttons Styles</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_'.$id.'_custom_style'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				</div>
				<input type="hidden" id="sdf_theme_<?php echo $id; ?>_custom_style" name="sdf_theme_<?php echo $id; ?>_custom_style" value="<?php echo $this->_currentSettings['typography']['theme_'.$id.'_custom_style']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-theme-custom-post-format-button-styles">
			
			<?php 
			foreach ( $this->_sdfPostFormats as $format_slug) {
			$id = str_replace( "-", "_", urlencode(strtolower($format_slug)) ).'_post_format_button';
			$format_name = ucfirst($format_slug);
			
			?>
		
				<div class="bs-callout bs-callout-grey">
					<p><?php echo $format_name; ?> Post Format Button</p>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_custom_text" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Button Text', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_custom_text" name="sdf_theme_<?php echo $id; ?>_custom_text" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_custom_text']) echo $this->_currentSettings['typography']['theme_'.$id.'_custom_text']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('This text wil be added to the button in combination with icon if icon is enabled in common options above.', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>	
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_shadow" name="sdf_theme_<?php echo $id; ?>_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_box_shadow" name="sdf_theme_<?php echo $id; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>

				<?php echo $this->displayColorPickerField('theme_'.$id.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
				<?php echo $this->displayColorPickerField('theme_'.$id.'_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>

				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_text_hover_shadow" name="sdf_theme_<?php echo $id; ?>_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_text_hover_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<div class="form-group">
					<label for="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
					<div class="col-sm-4 col-md-3">
					<input id="sdf_theme_<?php echo $id; ?>_hover_box_shadow" name="sdf_theme_<?php echo $id; ?>_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_'.$id.'_hover_box_shadow']; ?>" />
					</div>
					<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
				</div>
				
				<?php echo $this->displayColorPickerField('theme_'.$id.'_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
				
			<?php } ?>
			
		</div>
			
		</div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapsecrolltotop" href="#collapsecrolltotop"><?php _e('Scroll To Top Button', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapsecrolltotop" class="panel-collapse collapse">
		<div class="panel-body">
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_custom_text" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Button Text', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_scroll_to_top_button_custom_text" name="sdf_theme_scroll_to_top_button_custom_text" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_custom_text']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_custom_text']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('This text wil be added to the button in combination with icon if icon is set in option below.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_icon" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll to Top Icon', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_scroll_to_top_button_icon" name="sdf_theme_scroll_to_top_button_icon" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultIcons, $this->_currentSettings['typography']['theme_scroll_to_top_button_icon']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_icon_size" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Icon Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_scroll_to_top_button_icon_size" class="btn-group sdf_toggle">
				<?php
				if(isset($this->_ultIconSizes) && is_array($this->_ultIconSizes)){
					$size_picker = '';
					foreach($this->_ultIconSizes as $key => $val){
						$size_picker .= '<button type="button" class="btn btn-sm' . checkButton( (string)$this->_currentSettings['typography']['theme_scroll_to_top_button_icon_size'],$val, "btn-sdf-grey") . '" value="'.$val.'">'. $key. '</button>';
					}
					echo $size_picker;
				}
				?>
				</div>
				<input type="hidden" name="sdf_theme_scroll_to_top_button_icon_size" value="<?php echo $this->_currentSettings['typography']['theme_scroll_to_top_button_icon_size']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('To increase icon sizes relative to their container, use the Large (33% increase), 2X, 3X, 4X, or 5X size.', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_family" class="col-sm-4 col-md-4 control-label">Scroll To Top Button Text Font Family</label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_scroll_to_top_button_text_family" name="sdf_theme_scroll_to_top_button_text_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography']['theme_scroll_to_top_button_text_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>								

		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_size" class="col-sm-4 col-md-4 control-label">Scroll To Top Button Text Font Size</label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input type="text" id="sdf_theme_scroll_to_top_button_text_size" name="sdf_theme_scroll_to_top_button_text_size" class="form-control" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_size']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_text_size']; ?>"  placeholder="<?php echo $sdf_theme_font_size; ?> Default" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_theme_scroll_to_top_button_text_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Text Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_theme_scroll_to_top_button_text_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_scroll_to_top_button_text_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography']['theme_scroll_to_top_button_text_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_theme_scroll_to_top_button_text_weight" value="<?php echo $this->_currentSettings['typography']['theme_scroll_to_top_button_text_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_theme_scroll_to_top_button_text_transform" name="sdf_theme_scroll_to_top_button_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_width']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_width']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Width for Scroll To Top Button', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Height', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_height" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_height']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_height']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Height for Scroll To Top Button', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Right Position', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_right" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_right']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_right']; ?>" placeholder="0px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Right Position Value', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Bottom Position', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_bottom']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_bottom']; ?>" placeholder="0px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Bottom Position Value', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Border Width', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_width']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_border_width']; ?>" placeholder="0px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
				
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Border Style', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_theme_scroll_to_top_button_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Scroll To Top Button Border Radius', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_theme_scroll_to_top_button_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_border_radius']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_border_radius']; ?>" placeholder="0px Default" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="bs-callout bs-callout-grey">
			<p><?php _e('Set Scroll To Top Button Colors', SDF_TXT_DOMAIN); ?></p>
	  </div>
			
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_bg_color', __('Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_text_color', __('Text Color', SDF_TXT_DOMAIN)); ?>

		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_scroll_to_top_button_text_shadow" name="sdf_theme_scroll_to_top_button_text_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_shadow']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_text_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_scroll_to_top_button_box_shadow" name="sdf_theme_scroll_to_top_button_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_box_shadow']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_bg_hover_color', __('Hover Background Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_border_hover_color', __('Hover Border Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_text_hover_color', __('Hover Text Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_text_hover_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Text Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_scroll_to_top_button_text_hover_shadow" name="sdf_theme_scroll_to_top_button_text_hover_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_text_hover_shadow']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_text_hover_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Syntax is "text-shadow: h-shadow v-shadow blur color|none|initial|inherit;" (e.g. "2px 2px #ff0000")', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_scroll_to_top_button_hover_box_shadow" class="col-sm-4 col-md-4 control-label"><?php _e('Hover Box Shadow Values', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_theme_scroll_to_top_button_hover_box_shadow" name="sdf_theme_scroll_to_top_button_hover_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['theme_scroll_to_top_button_hover_box_shadow']) echo $this->_currentSettings['typography']['theme_scroll_to_top_button_hover_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")', SDF_TXT_DOMAIN); ?></div>
		</div>

		<?php echo $this->displayColorPickerField('theme_scroll_to_top_button_hover_box_shadow_color', __('Hover Box Shadow Color', SDF_TXT_DOMAIN)); ?>
			
		</div>
    </div>
  </div> 	
	 <?php 
	$id = 'blockquote';
	$id_var = str_replace("-", "_", urlencode(strtolower($id)));
	 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapbq" href="#collapbq"><?php _e('Blockquote', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapbq" class="panel-collapse collapse">
      <div class="panel-body">	
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_padding" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Padding', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_padding" name="sdf_<?php echo $id_var; ?>_padding" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_padding']) echo $this->_currentSettings['typography'][$id_var.'_padding']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>

		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_margin" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Margin', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_margin" name="sdf_<?php echo $id_var; ?>_margin" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_margin']) echo $this->_currentSettings['typography'][$id_var.'_margin']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
     
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_size" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_<?php echo $id_var; ?>_font_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography'][$id_var.'_font_size']) echo $this->_currentSettings['typography'][$id_var.'_font_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_<?php echo $id_var; ?>_font_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_family" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_font_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_font_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>   	
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_font_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_font_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_font_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_font_text_transform" name="sdf_<?php echo $id_var; ?>_font_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_font_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_font_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_font_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_font_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_font_color', __('Blockquote Text Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_font_hover_color', __('Blockquote Text Hover Color', SDF_TXT_DOMAIN)); ?>		
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_link_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Link Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_link_text_decoration" name="sdf_<?php echo $id_var; ?>_link_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_link_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Blockquote Link', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" class="col-sm-4 col-md-4 control-label"><?php _e('Blockquote Link Hover Text Decoration', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" name="sdf_<?php echo $id_var; ?>_link_hover_text_decoration" class="form-control"><?php echo $this->createSelectBox(sdfGo()->_wpuGlobal->_ultTextDecoration, $this->_currentSettings['typography'][$id_var.'_link_hover_text_decoration']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Define CSS "text-decoration" property for Blockquote Link on hover', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_link_color', __('Blockquote Link Color', SDF_TXT_DOMAIN)); ?>
		<?php echo $this->displayColorPickerField($id_var.'_link_hover_color', __('Blockquote Link Hover Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_author_size" class="col-sm-4 col-md-4 control-label"><?php _e('Author Font Size', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div class="row">
					<div class="col-sm-8 col-md-8">
					<input name="sdf_<?php echo $id_var; ?>_font_author_size" class="form-control" type="text" placeholder="<?php echo $sdf_theme_font_size; ?> Default" value="<?php if($this->_currentSettings['typography'][$id_var.'_font_author_size']) echo $this->_currentSettings['typography'][$id_var.'_font_author_size']; ?>" />
					</div>
					<div class="col-sm-4 col-md-4">
					<select name="sdf_<?php echo $id_var; ?>_font_author_size_type" class="form-control"><option value="px"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_size_type'] == 'px') echo ' selected'; ?>>px</option><option value="em"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
                                
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_author_family" class="col-sm-4 col-md-4 control-label"><?php _e('Author Font Family', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_font_author_family" class="form-control"><?php echo $this->createFontPicker( $this->_currentSettings['typography'][$id_var.'_font_author_family']); ?></select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>   
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_author_weight" class="col-sm-4 col-md-4 control-label"><?php _e('Author Font Weight', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf_<?php echo $id_var; ?>_font_author_weight" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_author_weight'],'normal', "btn-sdf-grey")?>" value="normal">Normal</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_font_author_weight'],'bold', "btn-sdf-grey")?>" value="bold">Bold</button>
				</div>
				<input type="hidden" name="sdf_<?php echo $id_var; ?>_font_author_weight" value="<?php echo $this->_currentSettings['typography'][$id_var.'_font_author_weight']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>	
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_font_author_text_transform" class="col-sm-4 col-md-4 control-label"><?php _e('Author Text-transform', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<select id="sdf_<?php echo $id_var; ?>_font_author_text_transform" name="sdf_<?php echo $id_var; ?>_font_author_text_transform" class="form-control">
			<option value="no"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_text_transform'] == 'no') echo ' selected'; ?>>None</option>
			<option value="capitalize"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_text_transform'] == 'capitalize') echo ' selected'; ?>>Capitalize</option>
			<option value="uppercase"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_text_transform'] == 'uppercase') echo ' selected'; ?>>Uppercase</option>
			<option value="lowercase"<?php if($this->_currentSettings['typography'][$id_var.'_font_author_text_transform'] == 'lowercase') echo ' selected'; ?>>Lowercase</option>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text">The text-transform property controls the capitalization of text.</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_font_author_color', __('Author Text Color', SDF_TXT_DOMAIN)); ?>
	  
		<div class="bs-callout bs-callout-grey">
		<h4>Blockquote Background Settings</h4>
		<p>Set Custom Background for Blockquote Box</p>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_custom_bg" class="col-sm-4 col-md-4 control-label">Set Custom Blockquote Box Background</label>
			<div class="col-sm-4 col-md-3">
				<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'no', "btn-sdf-grey")?>" value="no">No</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'yes', "btn-sdf-grey")?>" value="yes">Yes</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['typography'][$id_var.'_custom_bg'],'parallax', "btn-sdf-grey")?>" value="parallax">Parallax</button>
				</div>
				<input type="hidden" id="sdf_<?php echo $id_var; ?>_custom_bg" name="sdf_<?php echo $id_var; ?>_custom_bg" value="<?php echo $this->_currentSettings['typography'][$id_var.'_custom_bg']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div id="sdf-<?php echo $id; ?>-custom-bg">

		<?php echo $this->displayColorPickerField($id_var.'_bg_color', __('Box Background Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_bg_image" class="col-md-4 control-label">Box Background Image</label>
			<div class="col-sm-4 col-md-3">
			<div class="input-group">
				<input name="sdf_<?php echo $id_var; ?>_bg_image" type="text" class="wpu-image form-control" value="<?php echo $this->_currentSettings['typography'][$id_var.'_bg_image']; ?>">
				<span class="input-group-btn">
					<span class="btn btn-custom btn-file wpu-media-upload">
						<i class="fa fa-upload"></i> Upload Image <input type="file">
					</span>
				</span>
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text">Enter an URL or upload an image for background.</div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_bgrepeat_option">
			<label for="sdf_<?php echo $id_var; ?>_bgrepeat" class="col-sm-4 col-md-4 control-label">Box Background Repeat</label>
			<div class="col-sm-4 col-md-3">
			<select name="sdf_<?php echo $id_var; ?>_bgrepeat" class="form-control">
				<?php echo $this->createBgRepeatPicker($this->_currentSettings['typography'][$id_var.'_bgrepeat'], true); ?>
			</select>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group" id="sdf_<?php echo $id_var; ?>_par_bg_ratio_option">
			<label for="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="col-sm-4 col-md-4 control-label">Parallax Background Ratio</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_par_bg_ratio" name="sdf_<?php echo $id_var; ?>_par_bg_ratio" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_par_bg_ratio']) echo $this->_currentSettings['typography'][$id_var.'_par_bg_ratio']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">The ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.</div>
		</div>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_box_shadow" class="col-sm-4 col-md-4 control-label">Box Shadow Values</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_box_shadow" name="sdf_<?php echo $id_var; ?>_box_shadow" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_box_shadow']) echo $this->_currentSettings['typography'][$id_var.'_box_shadow']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text">Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")</div>
		</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_box_shadow_color', __('Box Shadow Color', SDF_TXT_DOMAIN)); ?>
		
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_border_radius" class="col-sm-4 col-md-4 control-label">Border Radius</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_border_radius" name="sdf_<?php echo $id_var; ?>_border_radius" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_border_radius']) echo $this->_currentSettings['typography'][$id_var.'_border_radius']; ?>" />					
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
			
		<div class="form-group">
			<label for="sdf_<?php echo $id_var; ?>_border_width" class="col-sm-4 col-md-4 control-label">Border Width</label>
			<div class="col-sm-4 col-md-3">
			<input id="sdf_<?php echo $id_var; ?>_border_width" name="sdf_<?php echo $id_var; ?>_border_width" class="form-control" type="text" value="<?php if($this->_currentSettings['typography'][$id_var.'_border_width']) echo $this->_currentSettings['typography'][$id_var.'_border_width']; ?>" />			
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Please enter just integer value and the unit is pixel', SDF_TXT_DOMAIN); ?></div>
		</div>
		
		<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label">Widget Border Style</label>
				<div class="col-sm-4 col-md-3">
				<select name="sdf_<?php echo $id_var; ?>_border_style" class="form-control"><option value="solid"<?php if($this->_currentSettings['typography'][$id_var.'_border_style'] == 'solid') echo ' selected'; ?>>solid</option><option value="dashed"<?php if($this->_currentSettings['typography'][$id_var.'_border_style'] == 'dashed') echo ' selected'; ?>>dashed</option><option value="dotted"<?php if($this->_currentSettings['typography'][$id_var.'_border_style'] == 'dotted') echo ' selected'; ?>>dotted</option><option value="double"<?php if($this->_currentSettings['typography'][$id_var.'_border_style'] == 'double') echo ' selected'; ?>>double</option></select>
				</div>
				<div class="col-sm-4 col-md-5 help-text"></div>
			</div>
		
		<?php echo $this->displayColorPickerField($id_var.'_border_color', __('Border Color', SDF_TXT_DOMAIN)); ?>
		
		</div>
   
      </div>
    </div>
  </div>
	
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfont9" href="#collapfont9"><?php _e('Silo Styles', SDF_TXT_DOMAIN); ?></a>
    </div>
    <div id="collapfont9" class="panel-collapse collapse">
      <div class="panel-body">

				<?php echo $this->displayInputField('typography', 'theme_silo_line_height', __('Silo Line-height'), '1.5 Default', ''); ?>
				
				<div class="form-group">
					<label for="sdf_theme_silo_font_size" class="col-sm-4 col-md-4 control-label">Silo Link Size</label>
					<div class="col-sm-4 col-md-3">
						<div class="row">
							<div class="col-sm-8 col-md-8">
							<input type="text" class="form-control" name="sdf_theme_silo_font_size" value="<?php if($this->_currentSettings['typography']['theme_silo_font_size']) echo $this->_currentSettings['typography']['theme_silo_font_size']; ?>" placeholder="14px Default" />
							</div>
							<div class="col-sm-4 col-md-4">
							<select name="sdf_theme_silo_font_size_type" class="form-control"><option<?php if($this->_currentSettings['typography']['theme_silo_font_size_type'] == 'px') echo ' selected'; ?> value="px">px</option><option value="em"<?php if($this->_currentSettings['typography']['theme_silo_font_size_type'] == 'em') echo ' selected'; ?>>em</option><option value="pt"<?php if($this->_currentSettings['typography']['theme_silo_font_size_type'] == 'pt') echo ' selected'; ?>>pt</option></select>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-5 help-text"></div>
				</div>
				
				<?php echo $this->displayColorPickerField('theme_silo_font_color', __('Silo Link Color', SDF_TXT_DOMAIN)); ?>
   
      </div>
    </div>
  </div>
  </div>
                      
</div>
	 
<div class="row">
<div class="col-md-4">
<button class="btn btn-sdf-grey btn-sm" type="submit" name="styles-submit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
</div>
</div>
	
			<?php wp_nonce_field('wpu-theme-update-type-settings'); ?>
			<input type="hidden" name="sdf_panel_collapse_shown" value="" />
</div>
</form>

</div><!-- end #sdf-styles -->
<?php endif; ?>

<?php if ($current_page == 'sdf-slider') : ?>	
<div id="sdf-slider" class="<?php if($current_page == 'sdf-slider'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">
 
<form name="sdf_theme_masthead_settings" class="form-horizontal" id="sdf_theme_masthead_settings" method="post" action="#sdf-slider">
<div id="sdf_theme_masthead_page" class="wra"> 
	<input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
<div class="row titlegroup">
<div class="col-sm-8 col-md-9">
                       <div class="wpu-slider-icon"></div><h3 class="pull-left"><?php _e('Slider Settings', SDF_TXT_DOMAIN); ?></h3>
                       <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title='<p>The global slider allows you to add images which will appear by default on all pages. You can override global settings locally (on the page level) and either disable the slider completely or even add custom images (for that page) which will be displayed in the slider area.<br /><br />In the global slider settings, you can select from single display(for a static image) or rotating display (which also has sub features under the select slider type such as (1) no controls (2) dot controls (3) number controls (4) text controls (where the permalink title appears) and (5) arrow controls to enhance the visual appearance.</p>'></i>  
                      
</div>
<div class="col-sm-4 col-md-3">
	<?php if($current_page != 'sdf-slider'): ?>
		<span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="slider-submit" id="slider-submit" value="<?php _e('Update Settings', SDF_TXT_DOMAIN) ?>" /></span>
	<?php endif; ?>
</div>
</div>
          
            

            
                    <?php wp_nonce_field('wpu-theme-update-slider'); ?>
					<input type="hidden" name="sdf_panel_collapse_shown" value="" />
</div>            
</form>

</div><!-- end #sdf-slider -->
<?php endif; ?>

<?php if ($current_page == 'sdf-footer') : ?>	
<div id="sdf-footer" class="<?php if($current_page == 'sdf-footer'): echo ' tab-pane active'; else: echo ' tab-pane'; endif; ?>">
<form name="sdf_theme_analytics" id="sdf_theme_analytics" class="form-horizontal" method="post" action="#sdf-footer" >
<div id="sdf_theme_analytics" class="wrap">
                    <input type="hidden" name="<?php echo SDF_THEME_SUBMITTED_FIELD; ?>" value="Y" />
                    <div class="row titlegroup">  
					<div class="col-sm-8 col-md-9">     
                    <div class="wpu-footer-icon"></div><h3 class="pull-left"><?php _e('Footer Settings', SDF_TXT_DOMAIN); ?><small class="expand-hide"><a href="javascript:" class="wpu-open-close">+ Expand All</a></small></h3>
                    </div>
					<div class="col-sm-4 col-md-3">
					<span class="submit"><input type="submit" class="btn btn-sdf-grey btn-sm pull-right" name="footer-submit" id="footer-submit" value="<?php _e('Save Changes', SDF_TXT_DOMAIN) ?>" /></span>
					</div>
                   </div>
                   <div class="wpu-group">
                   <div class="panel-group" id="wpu-foot-acc">
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfoot1" href="#collapfoot1">
      <?php _e('Footer Settings', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapfoot1" class="panel-collapse collapse">
      <div class="panel-body">
                       		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Turn On/Off Footer for entire site</label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
			<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['footer']['toggle_footer']),1, "btn-sdf-grey")?>" value="1">On</button>
			<button type="button" class="btn btn-sm<?php echo checkButton( intval($this->_currentSettings['footer']['toggle_footer']),0, "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="sdf_toggle_footer" value="<?php echo $this->_currentSettings['footer']['toggle_footer']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label">Copyright Bottom Footer Alignment</label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-toolbar">
			  <div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['footer']['footer_bottom_alignment'],'left', "btn-sdf-grey")?>" value="left"><i class="fa fa-align-left"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['footer']['footer_bottom_alignment'],'center', "btn-sdf-grey")?>" value="center"><i class="fa fa-align-center"></i></button>
				<button type="button" class="btn btn-sm<?php echo checkButton( (string)$this->_currentSettings['footer']['footer_bottom_alignment'],'right', "btn-sdf-grey")?>" value="right"><i class="fa fa-align-right"></i></button>
			  </div>
			  <input type="hidden" name="sdf_footer_bottom_alignment" value="<?php echo $this->_currentSettings['footer']['footer_bottom_alignment']; ?>" />
			</div>
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_copyright_footer" class="col-sm-4 col-md-4 control-label">Add Copyright Bottom Footer Content to every page. <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title='Any text, links or html code entered in the global footer below will appear on the local pages (unless footer is turned off on local page level setting).'></i></label>
			<div class="col-sm-8 col-md-8">
			<?php wp_editor( stripslashes($this->_currentSettings['footer']['copyright_footer']), "sdf_copyright_footer",  array('textarea_rows'=>5,'wpautop' => false) ); ?>
			</div>
		</div>
                            
                        
      </div>
     </div>
	</div>
 <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfoot2" href="#collapfoot2">
        <?php _e('Additional HTML', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapfoot2" class="panel-collapse collapse">
      <div class="panel-body">
                       	  
		<div class="form-group">
			<label for="sdf_page_js_footer" class="col-sm-4 col-md-4 control-label"><?php _e('HTML to Place before end Body Tag', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-5">
			<textarea name='sdf_page_js_footer' id='sdf_page_js_footer' cols='45' rows='7' class="form-control"><?php echo stripslashes( $this->_currentSettings['misc']['custom_js_footer'] ); ?></textarea>
			</div>
			<div class="col-sm-4 col-md-3 help-text"><?php _e('Script placed before end &lt;&frasl;body&gt; tag', SDF_TXT_DOMAIN) ?></div>
		</div>
                            
                        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <a class="accordion-toggle wpu-gtitle" data-toggle="collapse"  data-target="#collapfoot3" href="#collapfoot3">
        <?php _e('Analytics', SDF_TXT_DOMAIN); ?>
      </a>
    </div>
    <div id="collapfoot3" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="form-group">
			<label for="sdf_global_analytics" class="col-sm-4 col-md-4 control-label"><?php _e('Add Global Analytics Code to Theme', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-5">
			<textarea class="form-control" id="sdf_global_analytics" name="sdf_global_analytics" cols="45" rows="7"><?php echo stripslashes( $this->_currentSettings['misc']['theme_analytics'] ); ?></textarea>
			</div>
			<div class="col-sm-4 col-md-3 help-text"><?php _e('This code will add analytics code to the bottom of every page.', SDF_TXT_DOMAIN) ?></div>
		</div>         
    </div>
  </div>
</div>	
                  </div>
</div>				  
					<div class="row">
					<div class="col-md-4">
						<button class="btn btn-sdf-grey btn-sm" type="submit" name="footer-submit" id="footer-submit"><?php _e('Save Changes', SDF_TXT_DOMAIN) ?></button>
					</div>
					</div>
            
                    <?php wp_nonce_field('wpu-theme-update-footer-settings'); ?>
					<input type="hidden" name="sdf_panel_collapse_shown" value="" />
            
</form>

</div>
</div><!-- end #sdf-footer -->
<?php endif; ?>
</div>
</div>

		<?php  endif;
		}
		
		function updateGlobal($current){
				update_option(SDF_THEME_OPTIONS, maybe_serialize($current));
		}
		
		function getInitialThemeOptions($initialOptions){
			$sdf_theme_options = '';
			// get child theme defaults
			if(function_exists('sdf_child_theme_options')) {
				$sdf_theme_options = unserialize( sdf_decode( sdf_child_theme_options() ) );
				if($sdf_theme_options == '') {
					$sdf_theme_options = unserialize( sdf_decode( $this->sdf_core_theme_options() ));
				}
			}
			elseif(method_exists($this, 'sdf_core_theme_options')) {
				$sdf_theme_options = unserialize( sdf_decode( $this->sdf_core_theme_options() ));
			}
			else {
				$sdf_theme_options = maybe_serialize($initialOptions);
			}
			return $sdf_theme_options;
		}
		
		function switchThemeOptions() {
			// get fresh
			$this->_currentSettings = maybe_unserialize(get_option(SDF_THEME_OPTIONS));
			$this->setupGlobalArgs();
			if(!empty($this->_currentSettings['navigation']) && min(array_keys($this->_currentSettings['navigation'])) >= 1) {
				if(isset($this->_currentSettings['preset']['theme_preset'])) {
					$this->_currentPresetId = $this->_currentSettings['preset']['theme_preset'];
				}
				else{
					$this->_currentPresetId = min(array_keys($this->_currentSettings['navigation']));
				}
				
				if($this->_currentPresetId) {
					$this->_currentSettings['navigation'] = $this->_currentSettings['navigation'][$this->_currentPresetId];
					$this->_currentSettings['typography'] = $this->_currentSettings['typography'][$this->_currentPresetId];
				}
			}
			
			// new options patches
			$this->newOptionsPatches();

		}
		
		function newOptionsPatches(){
		
			$diff = $this->array_diff_key_recursive($this->_sdfGlobalArgs, $this->_currentSettings);
			if(!empty($diff)) {
				$this->_currentSettings = $this->mergeSettings($this->_sdfGlobalArgs, $this->_currentSettings, $diff);
			}
			
			// fallback for theme_font_size
			if($this->_currentSettings['typography']['theme_font_size'] && !strpos($this->_currentSettings['typography']['theme_font_size'], 'px')) {
				$this->_currentSettings['typography']['theme_font_size'] .= 'px';
			}
		}
		
		function getCustomLESS(){
		
			// get customized tbs less vars
			$custom_tbs_vars = array();
			$current_tbs_var = false;
			
			foreach ($this->_ultTbsVars as $tbs_key => $tbs_val){
				if($tbs_val['type'] != 'callout') {
					$tbs_key_id = str_replace( "-", "_", urlencode(strtolower($tbs_key)) );
					if($this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id]) {
						if($tbs_val['type'] == 'color') {
							// exclude transparent due to tbs default values
							$current_tbs_var = sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id], false, 'hex' );
							if($current_tbs_var){
								$custom_tbs_vars[$tbs_key] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id], false, 'hex' );
							}							 
						}
						else{
							$custom_tbs_vars[$tbs_key] = $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id];
						}
					}
				}
			}
			
			$options = array( 'compress'=>true );
			$parser = new Less_Parser( $options );
			$parser->parseFile( SDF_LIB.'/bootstrap/less/bootstrap.less', SDF_LESS );
			
			// assign bg color, primary color and base font size from SDF options
			$sdf_page_width = sdfGo()->sdf_get_global('layout','page_width');
			$sdf_body_bg = ($sdf_page_width == 'wide') ? sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_wide_bg_color'), false, 'hex' ) : sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_bg_color'), false, 'hex' );
			$body_bg = ($sdf_body_bg) ? $sdf_body_bg : false;
			if ($body_bg) $parser->ModifyVars( array( 'body-bg' => $body_bg ) );
			
			$brand_primary = (sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_primary_color'], false, 'hex' )) ? sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_primary_color'], false, 'hex' ) : false;
			if ($brand_primary) $parser->ModifyVars( array( 'brand-primary' => $brand_primary ) );
			
			$base_font_size = ($this->_currentSettings['typography']['theme_font_size']) ? $this->_currentSettings['typography']['theme_font_size'] : false;
			if ($base_font_size) $parser->ModifyVars( array( 'font-size-base' => $base_font_size ) );
			
			$h1_font_size = ($this->_currentSettings['typography']['theme_font_h1_size']) ? $this->_currentSettings['typography']['theme_font_h1_size'].$this->_currentSettings['typography']['theme_font_h1_size_type'] : false;
			if ($h1_font_size) $parser->ModifyVars( array( 'font-size-h1' => $h1_font_size ) );
			
			$h2_font_size = ($this->_currentSettings['typography']['theme_font_h2_size']) ? $this->_currentSettings['typography']['theme_font_h2_size'].$this->_currentSettings['typography']['theme_font_h2_size_type'] : false;
			if ($h2_font_size) $parser->ModifyVars( array( 'font-size-h2' => $h2_font_size ) );
			
			$h3_font_size = ($this->_currentSettings['typography']['theme_font_h3_size']) ? $this->_currentSettings['typography']['theme_font_h3_size'].$this->_currentSettings['typography']['theme_font_h3_size_type'] : false;
			if ($h3_font_size) $parser->ModifyVars( array( 'font-size-h3' => $h3_font_size ) );
			
			$h4_font_size = ($this->_currentSettings['typography']['theme_font_h4_size']) ? $this->_currentSettings['typography']['theme_font_h4_size'].$this->_currentSettings['typography']['theme_font_h4_size_type'] : false;
			if ($h4_font_size) $parser->ModifyVars( array( 'font-size-h4' => $h4_font_size ) );
			
			$h5_font_size = ($this->_currentSettings['typography']['theme_font_h5_size']) ? $this->_currentSettings['typography']['theme_font_h5_size'].$this->_currentSettings['typography']['theme_font_h5_size_type'] : false;
			if ($h5_font_size) $parser->ModifyVars( array( 'font-size-h5' => $h5_font_size ) );
			
			$h6_font_size = ($this->_currentSettings['typography']['theme_font_h6_size']) ? $this->_currentSettings['typography']['theme_font_h6_size'].$this->_currentSettings['typography']['theme_font_h6_size_type'] : false;
			if ($h6_font_size) $parser->ModifyVars( array( 'font-size-h6' => $h6_font_size ) );
			
			$button_border_width = ($this->_currentSettings['typography']['theme_button_border_width']) ? $this->_currentSettings['typography']['theme_button_border_width'] : '1px';
			if ($button_border_width) $parser->ModifyVars( array( 'button-border-width' => $button_border_width ) );
			
			$parser->ModifyVars( array( 'icon-font-path' => "'https://netdna.bootstrapcdn.com/bootstrap/3.3.4/fonts/'" ) );
			
			$parser->ModifyVars( $custom_tbs_vars );
			$tbs_css = $parser->getCss();
			
			return $tbs_css;
		
		}
		
		function getWooLESS(){
		
			// get customized tbs less vars
			$custom_tbs_vars = array();
			$current_tbs_var = false;
			
			foreach ($this->_ultTbsVars as $tbs_key => $tbs_val){
				if($tbs_val['type'] != 'callout') {
					$tbs_key_id = str_replace( "-", "_", urlencode(strtolower($tbs_key)) );
					if($this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id]) {
						if($tbs_val['type'] == 'color') {
							// exclude transparent due to tbs default values
							$current_tbs_var = sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id], false, 'hex' );
							if($current_tbs_var){
								$custom_tbs_vars[$tbs_key] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id], false, 'hex' );
							}							 
						}
						else{
							$custom_tbs_vars[$tbs_key] = $this->_currentSettings['typography']['theme_tbs_'.$tbs_key_id];
						}
					}
				}
			}
			
			$options = array( 'compress'=>true );
			$parser = new Less_Parser( $options );
			$parser->parseFile( SDF_WOO.'/assets/css/sdf-woocommerce.less', SDF_WOO_LESS );
			
			// assign bg color, primary color and base font size from SDF options
			$sdf_page_width = sdfGo()->sdf_get_global('layout','page_width');
			$sdf_body_bg = ($sdf_page_width == 'wide') ? sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_wide_bg_color'), false, 'hex' ) : sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_bg_color'), false, 'hex' );
			$body_bg = ($sdf_body_bg) ? $sdf_body_bg : false;
			if ($body_bg) $parser->ModifyVars( array( 'contentbg' => $body_bg ) );
			
			$brand_primary = (sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_primary_color'], false, 'hex' )) ? sdfGo()->_wpuGlobal->sdf_get_picked_color( $this->_currentSettings['typography']['theme_primary_color'], false, 'hex' ) : false;
			if ($brand_primary) {
				$parser->ModifyVars( array( 'brand-primary' => $brand_primary ) );
				$parser->ModifyVars( array( 'primary' => $brand_primary ) );
				$parser->ModifyVars( array( 'woocommerce' => $brand_primary ) );
			}
			
			$base_font_size = ($this->_currentSettings['typography']['theme_font_size']) ? $this->_currentSettings['typography']['theme_font_size'] : false;
			if ($base_font_size) $parser->ModifyVars( array( 'font-size-base' => $base_font_size ) );
			
			$button_border_width = ($this->_currentSettings['typography']['theme_button_border_width']) ? $this->_currentSettings['typography']['theme_button_border_width'] : '1px';
			if ($button_border_width) $parser->ModifyVars( array( 'button-border-width' => $button_border_width ) );
			
			$parser->ModifyVars( $custom_tbs_vars );
			$woo_css = $parser->getCss();
			
			return '';
		
		}
		
		function createStaticCSS(){
			
			// get fresh
			$presetOptions = array();
			$presetOptions = maybe_unserialize(get_option(SDF_THEME_OPTIONS));
			$this->setupGlobalArgs();
			
			$theme_presets = $presetOptions['preset']['theme_presets'];
			
			if(isset($theme_presets) && is_array($theme_presets)){
				foreach ( $theme_presets as $preset_id => $preset_name ) {
				
					$this->_currentSettings = $presetOptions;
					$this->_currentSettings['navigation'] = $this->_currentSettings['navigation'][$preset_id];
					$this->_currentSettings['typography'] = $this->_currentSettings['typography'][$preset_id];
					
					// new options patches
					$this->newOptionsPatches();
				
					if (!is_multisite()) {
						$filename = 'preset-'.$preset_id.'.css';
						$this->putCssContents($filename);
					}
				}
			}
		
		}
		
		function putCssContents($filename){

			// create static custom css files
			require_once(ABSPATH . 'wp-admin/includes/file.php');
			WP_Filesystem();
			global $wp_filesystem;
			
			$profile_css = '';
			ob_start();
			sdf_typography_styles($this->_currentSettings);
			$profile_css = ob_get_clean();
			
			$profile_css_ssl = '';
			ob_start();
			sdf_typography_styles($this->_currentSettings, true);
			$profile_css_ssl = ob_get_clean();
		
			// get less part
			$tbs_css = $this->getCustomLESS();
			
			$preset_css = $tbs_css . $profile_css;
			$preset_css_ssl = $tbs_css . $profile_css_ssl;
			$static_css = array(SDF_PRESET_CSS_PATH.'/css/custom.css');
			
			
			$min_preset_css = $this->minifyPresetStyles($preset_css, $static_css);
			$min_preset_css_ssl = $this->minifyPresetStyles($preset_css_ssl, $static_css);
			
			if ( ! $wp_filesystem->put_contents( SDF_PRESET_CSS_PATH.'/css/'.$filename, $min_preset_css, FS_CHMOD_FILE) ) {
				echo 'Error Saving CSS File "'.$filename.'"!';
			}
			if ( ! $wp_filesystem->put_contents( SDF_PRESET_CSS_PATH.'/css/ssl-'.$filename, $min_preset_css_ssl, FS_CHMOD_FILE) ) {
				echo 'Error Saving CSS File "'.$filename.'"!';
			}
		}

		function getColorPresets(){
		
      $preset_colors = $this->_currentSettings['typography']['preset_colors'];
			if ($this->_currentSettings['typography']['theme_primary_color'] != '') {
				$preset_colors['Primary'] = $this->_currentSettings['typography']['theme_primary_color'];
			}
			$preset_colors['Transparent'] = '';
			
			return $preset_colors;
		}
		
		function displayInputField($group_key, $option_key, $label, $placeholder, $help_text){
			
			$option_val = ($this->_currentSettings[$group_key][$option_key]) ? $this->_currentSettings[$group_key][$option_key] : '';
			
			$output = '<div class="form-group">';
			$output .= '<label for="sdf_'.$option_key.'" class="col-sm-4 col-md-4 control-label">'.$label.'</label>';
			$output .= '<div class="col-sm-4 col-md-3">';
			$output .= '<input type="text" class="form-control" name="sdf_'.$option_key.'" value="'.$option_val.'"  placeholder="'.$placeholder.'"/></p>';
			$output .= '</div>';
			$output .= '<div class="col-sm-4 col-md-5 help-text">'.$help_text.'</div>';
			$output .= '</div>';
			
			return $output;
		}
		
		function displayColorPickerField( $option_key, $label = '', $help_text = ''){
			$picker_val = (isset($this->_currentSettings['typography'][$option_key])) ? $this->_currentSettings['typography'][$option_key] : 
			((isset($this->_currentSettings['navigation']['menu_settings'][$option_key])) ? $this->_currentSettings['navigation']['menu_settings'][$option_key] : '');
			
			$preset_colors = $this->getColorPresets();
			
			$hex_preset_val = '';
			$opacity_val = '';
			$tr_picker_val = '';
			if (array_key_exists($picker_val, $preset_colors)) {
				if (array_key_exists($preset_colors[$picker_val], $preset_colors)) {
					$tr_picker_val = strtolower($preset_colors[$preset_colors[$picker_val]]);
				}
				else{
					$tr_picker_val = strtolower($preset_colors[$picker_val]);
				}
			}
			else {
				$tr_picker_val = strtolower($picker_val);
			}
			
			if($tr_picker_val){
				$rgb_args = explode(':', $tr_picker_val);
				$hex_preset_val = $rgb_args[0];
				$opacity_val = (count($rgb_args) > 1) ? trim($rgb_args[1]) : '1';
			}	
				
			$output = '<div class="form-group">';
			$output .= '<label for="" class="col-sm-4 col-md-4 control-label">'.$label.'</label>';
			$output .= '<div class="col-sm-4 col-md-3">';
			$output .= '<div class="input-group">';
			$output .= '<input id="sdf_'.$option_key.'" name="" class="form-control color-picker" type="text" data-opacity="'.$opacity_val.'" value="'.$this->sdf_get_picked_color($hex_preset_val, false, 'hex').'"  />';
			$output .= '<input id="sdf_'.$option_key.'_preset" name="sdf_'.$option_key.'" class="color-preset" type="hidden" value="'.$picker_val.'" />';
			$output .= '<div class="input-group-btn">';
			$output .= '<button type="button" class="btn btn-default dropdown-toggle preset_colors_dd" data-toggle="dropdown">'.__("Preset Colors", SDF_TXT_DOMAIN).' <span class="caret"></span></button>';
			$output .= '<ul class="dropdown-menu pull-right preset_colors"></ul>';
			$output .= '</div><!-- /btn-group -->';
			$output .= '</div><!-- /input-group -->';
			$output .= '</div>';
			$output .= '<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="right" data-html="true" title="'.__("Use Picker, Enter hex Value or use Preset Color. Picker has opacity slider so you can adjust color alpha value, 1 for solid(default), .01-.99 for more or less opacity.", SDF_TXT_DOMAIN).'"></i>'.$help_text.'</div>';
			$output .= '</div>';
			
			return $output;
		}

		function displaySettingsField( $option_key, $option_val, $option_fields ){
			$output = '';
			
			switch ($option_fields['type']){
				case 'text':
					$option_placeholder = '';
					if(isset($option_fields['placeholder'])) {
						$option_placeholder = ($option_fields['placeholder']) ? ' placeholder="'.$option_fields['placeholder'].'"' : '';
					}
					$output = '<input name="sdf_'.$option_key.'" class="form-control" type="text" value="'.$option_val.'"'.$option_placeholder.' />';
					break;
				case 'callout':
					$output = '<div class="bs-callout bs-callout-grey">'.$option_fields['callout_text'].'</div>';
					break;
				case 'color':
					$option_placeholder = '';
					if(isset($option_fields['placeholder'])) {
						$option_placeholder = ($option_fields['placeholder']) ? ' placeholder="'.$option_fields['placeholder'].'"' : '';
					}
					
					$preset_colors = $this->getColorPresets();
					
					$hex_preset_val = '';
					$opacity_val = '';
					$tr_option_val = '';
					if (array_key_exists($option_val, $preset_colors)) {
						$tr_option_val = strtolower($preset_colors[$option_val]);
					}
					else {
						$tr_option_val = strtolower($option_val);
					}
			
					
					if($tr_option_val){
						$rgb_args = explode(':', $tr_option_val);
						$hex_preset_val = $rgb_args[0];
						$opacity_val = (count($rgb_args) > 1) ? trim($rgb_args[1]) : '1';
					}	
					$output = '<div class="input-group">
					  <input id="sdf_'.$option_key.'" name="" class="form-control color-picker" type="text" data-opacity="'.$opacity_val.'" value="'.$this->sdf_get_picked_color($hex_preset_val, false, 'hex').'"'.$option_placeholder.' />
					  <input id="sdf_'.$option_key.'_preset" name="sdf_'.$option_key.'" class="color-preset" type="hidden" value="'.$option_val.'" />
					  <div class="input-group-btn">
						<button type="button" class="btn btn-default dropdown-toggle preset_colors_dd" data-toggle="dropdown">Preset Colors <span class="caret"></span></button>
						<ul class="dropdown-menu pull-right preset_colors"></ul>
					  </div><!-- /btn-group -->
					</div><!-- /input-group -->';
					break;
				case 'checkbox':
					if($option_val) $checked = ' checked="checked"'; else $checked = '';
					$output = '<input name="sdf_'.$option_key.'" type="checkbox"'.$checked.' />';
					break;
				default:
					break; 
			}
				
			return $output;	
		}
		function createSelectBox($options_arr, $default){
			$sel_options = '';
			
			if(isset($options_arr) && is_array($options_arr)){
				foreach($options_arr as $key => $val){
					$sel_options .= '<option value="' . $val . '"';
					if($default == $val) $sel_options .= ' selected="selected" ';
					$sel_options .= '>'. $key. '</option>';
				}
			}
			return $sel_options;
		}
		function createBgRepeatPicker($default, $bgFullScr = false){
			$rep_picker = '';
			$bgRepeatTypes = $this->_ultBgRepeatType;
			if( !$bgFullScr ) {
				unset ($bgRepeatTypes['cover']);
			}
			if(isset($bgRepeatTypes) && is_array($bgRepeatTypes)){
				foreach($bgRepeatTypes as $key => $val){
					$rep_picker .= '<option value="' . $key . '"';
					if($default == $key) $rep_picker .= ' selected="selected" ';
					$rep_picker .= '>'. $val. '</option>';
				}
			}
			return $rep_picker;
		}
		function createEasingPicker($default = 'linear'){
			$eas_picker = '';
			
			if(isset($this->_ultEasingEffects) && is_array($this->_ultEasingEffects)){
				foreach($this->_ultEasingEffects as $key){
					$eas_picker .= '<option value="' . $key . '"';
					if($default == $key) $eas_picker .= ' selected="selected" ';
					$eas_picker .= '>'. $key. '</option>';
				}
			}
			return $eas_picker;
		}
		function createAnimationPicker($default = 'fadeIn'){
			$anim_picker = '';
			
			if(isset($this->_currentSettings['animations']['animation_types']) && is_array($this->_currentSettings['animations']['animation_types'])){
				foreach($this->_currentSettings['animations']['animation_types'] as $key){
					$val = $key;
					$key = (!in_array($key, array('', 'No'))) ? $key : '';
					$anim_picker .= '<option value="' . $key . '"';
					if($default == $key) $anim_picker .= ' selected="selected" ';
					$anim_picker .= '>'. $val. '</option>';
				}
			}
			return $anim_picker;
		}
		function createModalAnimationPicker($animation_types, $anim_picked = '1', $global_val = false, $default_val = ''){
			$anim_picker = '';
			
			if(isset($animation_types) && is_array($animation_types)){
				
				if($global_val) {
					$anim_picker .= '<option value="glob"';
					if($anim_picked == 'glob') $anim_picker .= ' selected="selected" ';
					$anim_picker .= '>Global</option>';
				}
				
				$anim_picker .= '<option value="'.$default_val.'"';
				if($anim_picked == $default_val) $anim_picker .= ' selected="selected" ';
				$anim_picker .= '>Default</option>';
				
				foreach($animation_types as $key => $val){
					$anim_picker .= '<option value="' . $key . '"';
					if($anim_picked == $key) $anim_picker .= ' selected="selected" ';
					$anim_picker .= '>'. $val. '</option>';
				}
			}
			return $anim_picker;
		}
		function createAnimationDurationPicker($default = 'animated'){
			$anim_picker = '';
			
			if(isset($this->_currentSettings['animations']['animation_duration']) && is_array($this->_currentSettings['animations']['animation_duration'])){
				foreach($this->_currentSettings['animations']['animation_duration'] as $key => $val){
					$anim_picker .= '<option value="' . $val . '"';
					if($default == $val) $anim_picker .= ' selected="selected" ';
					$anim_picker .= '>'. $key. '</option>';
				}
			}
			return $anim_picker;
		}
		function createMenuPicker($default = 'default'){
			$menu_picker = '';
			
			if(isset($this->_currentSettings['navigation']['menu_styles']) && is_array($this->_currentSettings['navigation']['menu_styles'])){
				foreach($this->_currentSettings['navigation']['menu_styles'] as $key => $menustyle){
					$menu_picker .= '<option value="' . $key . '"';
					if($default == $key) $menu_picker .= ' selected="selected" ';
					$menu_picker .= '>'. $menustyle. '</option>';
				}
			}
			return $menu_picker;
		}
		function createFontPicker($default = ''){
			$font_picker = '<option value="">Default</option>';
			
			if(isset($this->_currentSettings['typography']['theme_font_families']) && is_array($this->_currentSettings['typography']['theme_font_families'])){
				foreach($this->_currentSettings['typography']['theme_font_families'] as $t_font){
					$font_family = explode(",", $t_font);
					$font_picker .= '<option style="font-family:' .$font_family[0]  .'" value="' . $t_font . '"';
					if($default == $t_font) $font_picker .= ' selected="selected" ';
					$font_picker .= '>'. $font_family[0]. '</option>';
				}
			}
			
			if(isset($this->_currentSettings['typography']['google_web_font']) && is_array($this->_currentSettings['typography']['google_web_font'])){
				foreach($this->_currentSettings['typography']['google_web_font'] as $g_font){
					if($g_font){
					$font_fix = $g_font.',sans-serif';
					$font_picker .= '<option style="font-family:' .$font_fix.'" value="' . $g_font . '"';
					if($default == $g_font) $font_picker .= ' selected="selected" ';
					$font_picker .= '>'. $g_font. '</option>';
					}
				}
			}
			
			return $font_picker;
		}
		
		// Change Login Logo
		function sdf_login_logo(){ 
			$himg = (sdf_get_logo_image() != '') ? sdf_get_logo_image() : get_bloginfo( 'template_directory' ).'/lib/img/logos/sdf_logo.png';
		
			$output = '<style type="text/css">
			body.login div#login h1 a {
			background-image: url('.$himg.');
			background-position: 50% 0;
			background-repeat: no-repeat;
			position: relative;
			-webkit-background-size: contain;
			-o-background-size: contain;
			background-size: contain;
			width: auto;
			}
			</style>';
			echo $output;
		}
		
		function sdf_login_logo_url() {
			return get_bloginfo( 'url' );
		}
		
		function sdf_login_logo_url_title() {
			return esc_html( get_bloginfo('name'), 1 );
		}
		
		// Admin footer modification
		function remove_footer_admin () {
			echo '<span id="footer-thankyou">Thank You for Creating with the <a href="http://www.seodesignframework.com" target="_blank" title="SEO Design Framework">SEO Design Framework</a></span>';
		}
		
		// Options Import/Export
		// get options string
		public function get_exported_options() {
			$get_options = maybe_unserialize( get_option( SDF_THEME_OPTIONS ));
			$exported_options = sdf_encode( serialize( $get_options ));
			//$exported_options = "<!*!* START export Code !*!*>\n".$exported_options."\n<!*!* END export Code !*!*>";
			return $exported_options;
		}
		
		// import all theme options
		public function import_json_options($import_code) {
			if ( !current_user_can('edit_themes') ){
				wp_die('<p>'.__('You do not have sufficient permissions for this action.').'</p>');
			}
			delete_option( SDF_THEME_OPTIONS );
			//$import_code = str_replace("<!*!* START export Code !*!*>\n","",$import_code);
			//$import_code = str_replace("\n<!*!* END export Code !*!*>","",$import_code);
			$imported_options = unserialize( sdf_decode( $import_code ) );
			add_option( SDF_THEME_OPTIONS, $imported_options );
			
			$this->switchThemeOptions();
			$this->createStaticCSS();
			
		}
		
		// theme activation on themes.php page
		public function init_theme() {
			if ( isset( $_GET['activated'] ) ) {
				$this->switchThemeOptions();
				$this->createStaticCSS();
			}
		}
		
		// theme upgrade on update.php page
		public function upgrade_theme() {
			if ( isset( $_GET['action'] ) && $_GET['action'] == 'upgrade-theme' && isset( $_GET['theme'] ) && $_GET['theme'] == 'seodesign' ) {
				$this->switchThemeOptions();
				$this->createStaticCSS();
			}
		}

		// add a new query var
		public function add_query_var_vars() {
			global $wp;
			$wp->add_query_var('theme_export_options');
		}

		//add a template redirect which looks for that query var and if found calls the download function
		public function admin_redirect_download_files(){
			global $wp;
			global $wp_query;
			//download theme export
			if (array_key_exists('theme_export_options', $wp->query_vars) && $wp->query_vars['theme_export_options'] == 'safe_download'){
				$this->download_options();
				die();
			}
		}

		// define the function that will take care of the actual download
		public function download_options($content = null, $file_name = null){
			if (! wp_verify_nonce($_REQUEST['nonce'], 'theme_export_options') ) 
				wp_die('Security check'); 

			// get the options to export and set it as content, ex:
			$content = $this->get_exported_options();
			$file_name = 'SEO_Design_Framework_Settings_'.date('Y-m-d').'.txt';
			header('HTTP/1.1 200 OK');

			if ( !current_user_can('edit_themes') ){
				wp_die('<p>'.__('You do not have sufficient permissions for this action.').'</p>');
			}
			
			if ($content === null || $file_name === null){
				wp_die('<p>'.__('Error Downloading file.').'</p>');     
			}
			
			$this->download_file($content, $file_name);
		}
		
		public function download_file($content, $file_name){
			
			//output for download
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Description: File Transfer');
			header('Content-Type: text/html; charset=UTF-8');
			header("Content-Disposition: attachment; filename=".$file_name.";");
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".strlen($content));	
			header("Expires: 0");
			header("Pragma: public");			
			echo $content;			
			exit();
		}

		// function to create the download export url/link
		function create_export_download_link($echo = false){
			$site_url = get_bloginfo('url');
			$args = array(
				'theme_export_options' => 'safe_download',
				'nonce' => wp_create_nonce('theme_export_options')
			);
			$export_url = esc_url( add_query_arg($args, $site_url));
			$export_link = '<a class="btn btn-sm btn-custom" href="'.$export_url.'" target="_blank"><i class="fa fa-download"></i> Download Export File</a>';
			if ($echo === true)
				echo $export_link;
			elseif ($echo == 'url')
				return $export_url;
			return $export_link;
		}
		
		function switchKeys( &$arr, $origKey, $newKey, &$pendingKeys ) {
			if( !isset( $arr[$newKey] ) ) {
				$arr[$newKey] = $arr[$origKey];
				unset( $arr[$origKey] );
				if( isset( $pendingKeys[$origKey] ) ) {
					// recursion to handle conflicting keys
					switchKeys( $arr, $pendingKeys[$origKey], $origKey, $pendingKeys );
					unset( $pendingKeys[$origKey] );
				}
			} elseif( $newKey != $origKey ) {
				$pendingKeys[$newKey] = $origKey;
			}
		}
		
		function minifyPresetStyles($cssCode, $cssFiles = array()){

			WP_Filesystem();
			global $wp_filesystem;
						
			$buffer = "";
			$buffer .= $cssCode;
			foreach ($cssFiles as $cssFile) {
				$buffer .= $wp_filesystem->get_contents($cssFile);
			}

			// Remove comments
			$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

			// Remove space after colons
			$buffer = str_replace(': ', ':', $buffer);

			// Remove whitespace
			$buffer = str_replace(array("\r\n", "\r", "\n", '  ', '    ', '    '), '', $buffer);
			
			return $buffer;

		}
	
	}
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
