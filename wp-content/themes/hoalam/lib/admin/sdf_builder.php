<?php 
wp_enqueue_script( 'jquery-ui-dialog' );
wp_enqueue_style( 'wp-jquery-ui-dialog' );

$image_hover_effect = array( 'None' => '', 'Lily' => 'lily', 'Sadie' => 'sadie', 'Roxy' => 'roxy', 'Bubba' => 'bubba', 'Romeo' => 'romeo', 'Honey' => 'honey', 'Oscar' => 'oscar', 'Marley' => 'marley', 'Ruby' => 'ruby', 'Milo' => 'milo', 'Dexter' => 'dexter', 'Sarah' => 'sarah', 'Chico' => 'chico' , 'Julia' => 'julia' , 'Goliath' => 'goliath' , 'Selena' => 'selena' , 'Apollo' => 'apollo' , 'Steve' => 'steve' , 'Moses' => 'moses' , 'Jazz' => 'jazz' , 'Ming' => 'ming' , 'Lexi' => 'lexi' , 'Duke' => 'duke' );
$linkTargets = array( 'None' => '', 'Same window' => '_self', 'New window' => '_blank' );
$themeSidebars = array('Default Widget Block', 'Header Block 1', 'Header Block 2', 'Header Block 3', 'Header Block 4', 'Slider Block');					
$imageShapes = sdfGo()->_wpuGlobal->_ultImages['shapes'];
$imageSizes = sdfGo()->_wpuGlobal->_ultImages['sizes'];
$entranceAnimations = sdfGo()->_wpuGlobal->_ultEntranceAnimations['animation_types'];
$entranceAnimationDuration = sdfGo()->_wpuGlobal->_ultEntranceAnimations['animation_duration'];
$rotatorDelay = array ( '1 second' => '1000', '1.5 seconds' => '1500', '2 seconds' => '2000', '2.5 seconds' => '2500', '3 seconds' => '3000', '3.5 seconds' => '3500', '4 seconds' => '4000', '4.5 seconds' => '4500', '5 second' => '5000', '5.5 seconds' => '5500', '6 seconds' => '6000', '6.5 seconds' => '6500', '7 seconds' => '7000', '7.5 seconds' => '7500', '8 seconds' => '8000', '8.5 seconds' => '8500', '9 seconds' => '9000', '9.5 seconds' => '9500', '10 seconds' => '10000',
);
$spacerHeights = sdfGo()->_wpuGlobal->_ultSpacerHeights;
$iconSizes = sdfGo()->_wpuGlobal->_ultIconSizes;
$sdfButtons = array_flip(sdfGo()->_wpuGlobal->_sdfButtons);
$faIcons = sdfGo()->_wpuGlobal->_ultIcons;
$faIcons = array('No Icon' => 'no') + $faIcons;
$bgRepeatType = array_flip(sdfGo()->_wpuGlobal->_ultBgRepeatType);
$defaultAttrOptions = array('No' => 'no', 'Yes' => 'yes');
$defaultYesAttrOptions = array('Yes' => 'yes', 'No' => 'no');
$listItemImagePosition = array('Top' => 'top', 'Bottom' => 'bottom', 'Left' => 'left', 'Right' => 'right');
$gridTitlePosition = array('Below Image' => 'below', 'Above Image' => 'above');
$textAlignment = array('Left' => 'left', 'Right' => 'right', 'Center' => 'center', 'Justify' => 'justify');
$separatorStyles = array('Gradient Line' => 'style-1', 'Solid Line' => 'style-2', 'Solid Line w/ Grey Underline' => 'style-3', 'Dashed Line' => 'style-4', 'Dashed Line w/ Grey Background' => 'style-5', 'Dotted Line' => 'style-6', 'Dotted Line w/ Grey Background' => 'style-7', 'Double Line' => 'style-8', 'Drop Shadow Line' => 'style-9', 'Cloud Line' => 'style-10', 'Radial Shadow Line' => 'style-11');
$separatorTextStyles = array('Gradient Line' => 'style-1', 'Solid Line' => 'style-2', 'Solid Line w/ Grey Underline' => 'style-3', 'Dashed Line' => 'style-4', 'Dashed Line w/ Grey Background' => 'style-5', 'Dotted Line' => 'style-6', 'Dotted Line w/ Grey Background' => 'style-7', 'Double Line' => 'style-8', 'Drop Shadow Line' => 'style-9');
$customBackground = array( 'Background Color/Image' => 'image', 'Parallax' => 'parallax', 'Background Video' => 'video' );
$borderStyle = array( 'Solid' => 'solid', 'Dotted' => 'dotted', 'Dashed' => 'dashed' );
$oembedAspectRatio = array( '16:9' => '16by9', '4:3' => '4by3' );
$sdfSidebars = array();	
foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
	if(!in_array( ucwords($sidebar['name']), $themeSidebars )){
		$sdfSidebars[ucwords( $sidebar['name'] )] = $sidebar['id'];
	} 
}	
// portfolio vars
$portfolio_project_types = get_terms('portfolio_items', array( 'hide_empty' => 0 ));	
$project_types = array();						
if(is_array( $portfolio_project_types ) && !empty( $portfolio_project_types )) {
	foreach ( $portfolio_project_types as $term ) {
		$project_types[$term->name] = $term->slug;
	}
} else {
	$project_types['Please set Portfolio Project Types!'] = ''; 
}

// latest posts vars
$post_categories = get_terms('category', array( 'hide_empty' => 0 ));
$post_types = array();						
if(is_array( $post_categories ) && !empty( $post_categories )) {
	foreach ( $post_categories as $term ) {
		$post_types[$term->name] = $term->slug;
	}
} else {
	$post_types['Please set Post Categories!'] = ''; 
}
// ads list
$ads_list = array('No' => 'no');
$ads = get_posts(array('post_type' => 'ads'));
foreach($ads as $ad) {
	$ads_list[$ad->post_title] = $ad->ID;
}

// fonts list
$fontsList = array('Default' => '');

if(isset(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families']) && is_array(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families'])){
	foreach(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families'] as $font){
		$font_family = explode(",", $font);
		$fontsList[$font_family[0]] = $font;
	}
}

if(isset(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font']) && is_array(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font'])){
	foreach(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font'] as $font){
		if($font){
		$font_fix = $font;
		$fontsList[$font] = $font_fix;
		}
	}
}

$sdfElements = array(
	'Text Block' => array(
		'wpeditor1-content' => 'Text:::sdfaddtextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'input1' => 'Max Width',
		'select3' => array(
			'title' => 'Text Alignment',
			'options' => $textAlignment
		),
		'select4' => array(
			'title' => 'Font Family',
			'options' => $fontsList
		),
		'input2' => 'Font Size',
		'input3' => 'Line Height',
		'picker1' => 'Text Color',
		'select5' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select6' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input4' => 'Link',
		'input5' => 'Link Title',
		'select7' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select8' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'input6' => 'Class'
	),
	'Headline' => array(
		'input1-content' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'select4' => array(
			'title' => 'Font Family',
			'options' => $fontsList
		),
		'input2' => 'Font Size',
		'input3' => 'Line Height',
		'input4' => 'Letter Spacing',
		'picker1' => 'Text Color',
		'select5' => array(
			'title' => 'Text Alignment',
			'options' => $textAlignment
		),
		'select6' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select7' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select8' => array(
			'title' => 'Rotate Words',
			'options' => $defaultAttrOptions
		),
		'select9' => array(
			'title' => 'Rotator Delay',
			'options' => $rotatorDelay
		),
		'input5' => 'Rotator Separator',
		'select10' => array(
			'title' => 'Fit Text',
			'options' => $defaultAttrOptions
		),
		'input6' => 'Fit Compressor',
		'input7' => 'Fit Min Font Size',
		'input8' => 'Fit Max Font Size',
		'input9' => 'Class'
	),
	'Image' => array(
		'input1' => 'Title',
		'input2' => 'Alt Text',
		'imageupload1' => 'Image:::image',
		'select1' => array(
			'title' => 'Has Retina Version',
			'options' => $defaultAttrOptions
		),
		'select2' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select4' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select5' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select6' => array(
			'title' => 'Image Alignment',
			'options' => array(
				'Align Left' => 'left',
				'Align Right' => 'right',
				'Align Center'  => 'center'
			)
		),
		'select7' => array(
			'title' => 'Image Size',
			'options' => $imageSizes
		),
		'select8' => array(
			'title' => 'Image Shape',
			'options' => $imageShapes
		),
		'input3' => 'Image Border Radius',
		'select9' => array(
			'title' => 'Lightbox',
			'options' => $defaultAttrOptions
		),
		'select10' => array(
			'title' => 'Lightbox Ad',
			'options' => $ads_list
		),
		'input4' => 'Link',
		'input5' => 'Link Title',
		'select11' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select12' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'select13' => array(
			'title' => 'Image Hover Effect',
			'options' => $image_hover_effect
		),
		'input6' => 'Image Hover Title',
		'input7' => 'Image Hover Subtitle',
		'input8' => 'Class'
	),
	'Video/oEmbed' => array(
		'input1-content' => 'Title',
		'input2' => 'Video/oEmbed URL',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Video/oEmbed Aspect Ratio',
			'options' => $oembedAspectRatio
		),
	),
	'Widget' => array(
		'input1-content' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select4' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select5' => array(
			'title' => 'Choose Sidebar',
			'options' => $sdfSidebars
		),
		'input2' => 'Class'	
	),
	'Icon' => array(
		'select1' => array(
			'title' => 'Icon',
			'options' => $faIcons
		),
		'select2' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select4' => array(
			'title' => 'Icon Size',
			'options' => $iconSizes
		),
		'select5' => array(
			'title' => 'Icon Alignment',
			'options' => array(
				'Align Left' => 'left',
				'Align Right' => 'right',
				'Centered'  => 'center'
			)
		),
		'picker1' => 'Icon Color',
		'input1' => 'Icon Link',
		'select6' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select7' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'select8' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select9' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input2' => 'Class'
	),
	'Button' => array(
		'input1-content' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Button Icon',
			'options' => $faIcons
		),
		'select4' => array(
			'title' => 'Button Icon Position',
			'options' => array(
				'Align Left' => 'icon-left-side',
				'Align Right' => 'icon-right-side'
			)
		),
		'select5' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select6' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select7' => array(
			'title' => 'Button Layout',
			'options' => array(
				'Align Left' => 'left',
				'Align Right' => 'right',
				'Centered'  => 'center',
				'Full Width' => 'block'
			)
		),
		'select8' => array(
			'title' => 'Style',
			'options' => $sdfButtons
		),
		'select9' => array(
			'title' => 'Size',
			'options' => array(
				'Large'  => 'btn-lg',
				'Default' => 'default',
				'Small' => 'btn-sm',
				'Extra small' => 'btn-xs'
			)
		),
		'input2' => 'Link',
		'select10' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select11' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'input3' => 'Class'
	),
	'CTA Button' => array(
		'input1-content' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Button Icon',
			'options' => $faIcons
		),
		'select4' => array(
			'title' => 'Button Icon Position',
			'options' => array(
				'Align Left' => 'icon-left-side',
				'Align Right' => 'icon-right-side'
			)
		),
		'select5' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select6' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select7' => array(
			'title' => 'Button Layout',
			'options' => array(
				'Align Left' => 'left',
				'Align Right' => 'right',
				'Centered'  => 'center',
				'Full Width' => 'block'
			)
		),
		'input2' => 'Subtitle',
		'select8' => array(
			'title' => 'Style',
			'options' => $sdfButtons
		),
		'select9' => array(
			'title' => 'Size',
			'options' => array(
				'Large'  => 'btn-lg',
				'Default' => 'default',
				'Small' => 'btn-sm',
				'Extra small' => 'btn-xs'
			)
		),
		'input5' => 'Link',
		'select10' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select11' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'input6' => 'Class'
	),
	'Text List' => array(
		'wpeditor1-content' => 'List Content:::sdfaddtextlistblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'List Content Alignment',
			'options' => $textAlignment
		),
		'select4' => array(
			'title' => 'List Type',
			'options' => array(
				'Unordered'  => 'ul',
				'Ordered'  => 'ol'
			)
		),
		'select5' => array(
			'title' => 'List Style',
			'options' => array(
				'Default'  => 'default',
				'Custom'  => 'custom',
				'None' => 'none',
				'Circle' => 'circle',
				'Disc' => 'disc',
				'Square' => 'square',
				'FA Icon' => 'icon'
			)
		),
		'select6' => array(
			'title' => 'List Icon',
			'options' => $faIcons
		),
		'select7' => array(
			'title' => 'Icon Size',
			'options' => $iconSizes
		),
		'select8' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select9' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input1' => 'Class'
	),
	'CTA Box' => array(
		'wpeditor1-content' => 'Box Text:::sdfaddctatextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select4' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input1' => 'Button title',
		'input2' => 'Button subtitle',
		'select5' => array(
			'title' => 'Button Style',
			'options' => $sdfButtons
		),
		'input3' => 'Button Class',
		'select6' => array(
			'title' => 'Button size',
			'options' => array(
				'Large'  => 'btn-lg',
				'Default' => 'default',
				'Small' => 'btn-sm',
				'Extra small' => 'btn-xs'
			)
		),
		'input4' => 'Button width',
		'input5' => 'Button height',
		'input6' => 'Button link',
		'select7' => array(
			'title' => 'Button Target',
			'options' => $linkTargets
		),
		'select8' => array(
			'title' => 'Button Nofollow',
			'options' => $defaultAttrOptions
		),
		'select9' => array(
			'title' => 'Button Position',
			'options' => array(
				'top', 'bottom', 'left', 'right'
			)
		),
		'input7' => 'Button Top Margin',
		'input8' => 'Button Bottom Margin',
		'input9' => 'Class',
		'select10' => array(
			'title' => 'Button block',
			'options' => $defaultAttrOptions
		)
	),
	'Message Box' => array(
		'wpeditor1-content' => 'Message Text:::sdfaddmessagetextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select4' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select5' => array(
			'title' => 'Style',
			'options' => array(
				'Success' => 'alert-success',
				'Info' => 'alert-info',
				'Warning' => 'alert-warning',
				'Danger' => 'alert-danger'
			)
		),
		'select6' => array(
			'title' => 'Dismissable',
			'options' => $defaultAttrOptions
		),
		'input1' => 'Class'
	),
	'Teaser Box' => array(
		'wpeditor1-content' => 'Box Text:::sdfaddteasertextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Text Alignment',
			'options' => $textAlignment
		),
		'select4' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select5' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input1' => 'Title',
		'select6' => array(
			'title' => 'Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'select7' => array(
			'title' => 'Title Position',
			'options' => array(
				'Above Text'  => 'above_text',
				'Below Text' => 'below_text'
			)
		),
		'input2' => 'Title Link',
		'select8' => array(
			'title' => 'Target',
			'options' => $linkTargets
		),
		'select9' => array(
			'title' => 'Nofollow',
			'options' => $defaultAttrOptions
		),
		'input3' => 'Subtitle',
		'select10' => array(
			'title' => 'Teaser Icon',
			'options' => $faIcons
		),
		'select11' => array(
			'title' => 'Icon Size',
			'options' => $iconSizes
		),
		'select12' => array(
			'title' => 'Button Style',
			'options' => $sdfButtons
		),
		'select13' => array(
			'title' => 'Button Position',
			'options' => array(
				'Aside Left'  => 'aside_left',
				'Aside Right' => 'aside_right',
				'Above Title'  => 'above_title',
				'Below Title' => 'below_title',
				'Left of Title' => 'left_title',
				'Right of Title' => 'right_title',
				'Above Text'  => 'above_text',
				'Below Text' => 'below_text',
				'Left of Text' => 'left_text',
				'Right of Text' => 'right_text'
			)
		),
		'input4' => 'Button Border Width',
		'input5' => 'Button Border Radius',
		'input6' => 'Button Padding',
		'input7' => 'Button Width',
		'input8' => 'Button Height',
		'imageupload1' => 'Teaser Image:::teaser-image',
		'select14' => array(
			'title' => 'Has Retina Version',
			'options' => $defaultAttrOptions
		),
		'select15' => array(
			'title' => 'Image Size',
			'options' => $imageSizes
		),
		'select16' => array(
			'title' => 'Image Shape',
			'options' => $imageShapes
		),
		'select17' => array(
			'title' => 'Image Position',
			'options' => array(
				'Top' => 'top', 
				'Bottom' => 'bottom'
			)
		),
		'input9' => 'Class'
	),
	'Team Member Box' => array(
		'wpeditor1-content' => 'Box Text:::sdfaddteammembertextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Text Alignment',
			'options' => $textAlignment
		),
		'select4' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select5' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input1' => 'Member Name',
		'select6' => array(
			'title' => 'Name Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'select7' => array(
			'title' => 'Name Position',
			'options' => array(
				'Above Text'  => 'above_text',
				'Below Text' => 'below_text'
			)
		),
		'input2' => 'Name Link',
		'select8' => array(
			'title' => 'Name Link Target',
			'options' => $linkTargets
		),
		'select9' => array(
			'title' => 'Name Link Nofollow',
			'options' => $defaultAttrOptions
		),
		'input3' => 'Member Title',
		'select10' => array(
			'title' => 'Member Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'imageupload1' => 'Member Image:::member-image',
		'select11' => array(
			'title' => 'Has Retina Version',
			'options' => $defaultAttrOptions
		),
		'select12' => array(
			'title' => 'Image Size',
			'options' => $imageSizes
		),
		'select13' => array(
			'title' => 'Image Shape',
			'options' => $imageShapes
		),
		'select14' => array(
			'title' => 'Image Position',
			'options' => array(
				'Above Name'  => 'above_name',
				'Below Name' => 'below_name',
				'Left of Name' => 'left_name',
				'Right of Name' => 'right_name',
				'Above Text'  => 'above_text',
				'Below Text' => 'below_text',
				'Left of Text' => 'left_text',
				'Right of Text' => 'right_text'
			)
		),
		'input4' => 'Image Link',
		'select15' => array(
			'title' => 'Image Link Target',
			'options' => $linkTargets
		),
		'select16' => array(
			'title' => 'Image Link Nofollow',
			'options' => $defaultAttrOptions
		),
		'select17' => array(
			'title' => 'Icon Size',
			'options' => $iconSizes
		),
		'select18' => array(
			'title' => 'Button Style',
			'options' => $sdfButtons
		),
		'input5' => 'Button Border Width',
		'input6' => 'Button Border Radius',
		'input7' => 'Button Padding',
		'input8' => 'Button Margin',
		'input9' => 'Button Width',
		'input10' => 'Button Height',
		'input11' => 'Twitter',
		'input12' => 'Facebook',
		'input13' => 'Google Plus',
		'input14' => 'Skype',
		'input15' => 'LinkedIn',
		'input16' => 'Pinterest',
		'input17' => 'Class'
	),
	'Portfolio' => array(
		'input1' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'selectmulti1' => array(
			'title' => 'Project Type',
			'options' => $project_types
		),
		'select3' => array(
			'title' => 'Layout Type',
			'options' => array('Grid' => 'grid', 'List' => 'list')
		),
		'select4' => array(
			'title' => 'Grid Layout',
			'options' => array('Default Grid with Spacing' => 'grid_default', 'Grid without Spacing' => 'grid_no_space', 'Masonry with Spacing' => 'masonry_default', 'Masonry without Spacing' => 'masonry_no_space')
		),
		'input2' => 'Posts Count',
		'input3' => 'Posts Offset',
		'input4' => 'Rows',
		'select5' => array(
			'title' => 'Columns',
			'options' => array('1', '2', '3', '4', '6')
		),
		'select6' => array(
			'title' => 'Portfolio Filter',
			'options' => $defaultAttrOptions
		),
		'select7' => array(
			'title' => 'Info Display',
			'options' => array(
													'Show Title' => 'show-title', 
													'Show Title and Post Excerpt' => 'show-title-excerpt', 
													'Show Title and Meta Data' => 'show-info', 
													'Show Meta Data and Post Excerpt' => 'show-meta-excerpt', 
													'Show Post Excerpt' => 'show-excerpt',
													'Show Title, Meta Data and Post Excerpt' => 'show-all',
													'Hide All' => 'hide-all'
												)
		),
		'select8' => array(
			'title' => 'Item Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'input5' => 'Word Count',
		'select9' => array(
			'title' => 'Show Images',
			'options' => $defaultYesAttrOptions
		),
		'select10' => array(
			'title' => 'List Item Image Position',
			'options' => $listItemImagePosition
		),
		'select11' => array(
			'title' => 'Grid Title Position',
			'options' => $gridTitlePosition
		),
		'select12' => array(
			'title' => 'Show Overlay Title',
			'options' => $defaultYesAttrOptions
		),
		'select13' => array(
			'title' => 'Show PrettyPhoto Button',
			'options' => $defaultYesAttrOptions
		),
		'select14' => array(
			'title' => 'Show Item Link Button',
			'options' => $defaultYesAttrOptions
		),
		'select15' => array(
			'title' => 'Show Custom Button',
			'options' => $defaultAttrOptions
		),
		'select16' => array(
			'title' => 'Custom Button Icon',
			'options' => $faIcons
		),
		'input6' => 'Custom Button Text',
		'input7' => 'Custom Button Link',
		'select17' => array(
			'title' => 'Custom Button Link Target',
			'options' => $linkTargets
		),
		'select18' => array(
			'title' => 'Thumbnails Size',
			'options' => $imageSizes
		),
		'select19' => array(
			'title' => 'Thumbnails Shape',
			'options' => $imageShapes
		),
		'select20' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select21' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		)
	),
	'Latest Posts' => array(
		'input1' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'selectmulti1' => array(
			'title' => 'Post Categories',
			'options' => $post_types
		),
		'select3' => array(
			'title' => 'Layout Type',
			'options' => array('Grid' => 'grid', 'List' => 'list')
		),
		'select4' => array(
			'title' => 'Grid Layout',
			'options' => array('Default Grid with Spacing' => 'grid_default', 'Grid without Spacing' => 'grid_no_space', 'Masonry with Spacing' => 'masonry_default', 'Masonry without Spacing' => 'masonry_no_space')
		),
		'input2' => 'Posts Count',
		'input3' => 'Posts Offset',
		'input4' => 'Rows',
		'select5' => array(
			'title' => 'Columns',
			'options' => array('1', '2', '3', '4', '6')
		),
		'select6' => array(
			'title' => 'Posts Filter',
			'options' => $defaultAttrOptions
		),
		'select7' => array(
			'title' => 'Info Display',
			'options' => array(
													'Show Title' => 'show-title', 
													'Show Title and Post Excerpt' => 'show-title-excerpt', 
													'Show Title and Meta Data' => 'show-info', 
													'Show Meta Data and Post Excerpt' => 'show-meta-excerpt', 
													'Show Post Excerpt' => 'show-excerpt',
													'Show Title, Meta Data and Post Excerpt' => 'show-all',
													'Hide All' => 'hide-all'
												)
		),
		'select8' => array(
			'title' => 'Item Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'input5' => 'Word Count',
		'select9' => array(
			'title' => 'Show Images',
			'options' => $defaultYesAttrOptions
		),
		'select10' => array(
			'title' => 'List Item Image Position',
			'options' => $listItemImagePosition
		),
		'select11' => array(
			'title' => 'Grid Title Position',
			'options' => $gridTitlePosition
		),
		'select12' => array(
			'title' => 'Show Overlay Title',
			'options' => $defaultYesAttrOptions
		),
		'select13' => array(
			'title' => 'Show PrettyPhoto Button',
			'options' => $defaultYesAttrOptions
		),
		'select14' => array(
			'title' => 'Show Item Link Button',
			'options' => $defaultYesAttrOptions
		),
		'select15' => array(
			'title' => 'Show Custom Button',
			'options' => $defaultAttrOptions
		),
		'select16' => array(
			'title' => 'Custom Button Icon',
			'options' => $faIcons
		),
		'input6' => 'Custom Button Text',
		'input7' => 'Custom Button Link',
		'select17' => array(
			'title' => 'Custom Button Link Target',
			'options' => $linkTargets
		),
		'select18' => array(
			'title' => 'Thumbnails Size',
			'options' => $imageSizes
		),
		'select19' => array(
			'title' => 'Thumbnails Shape',
			'options' => $imageShapes
		),
		'select20' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select21' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		)
	),
	'Blockquote' => array(
		'wpeditor1-content' => 'Text:::sdfblockquotetext',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Blockquote Alignment',
			'options' => array(
				'Left' => 'left',
				'Right' => 'right'
			)
		),
		'select4' => array(
			'title' => 'Quote Icon',
			'options' => $defaultAttrOptions
		),
		'select5' => array(
			'title' => 'Quote Icon Size',
			'options' => $iconSizes
		),
		'input1' => 'Blockquote Author',
		'input2' => 'Cite Title',
		'input3' => 'Cite Link',
		'select6' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select7' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input4' => 'Class'
	),
	'Counter' => array(
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Counter Type',
			'options' => array('Zero' => 'zero', 'Random' => 'random')
		),
		'select4' => array(
			'title' => 'Text Alignment',
			'options' => $textAlignment
		),
		'input1' => 'Number',
		'input2' => 'Number Font Size',
		'select6' => array(
			'title' => 'Number Font Weight',
			'options' => array('100', '200', '300', '400', '500', '600', '700', '800', '900')
		),
		'picker1' => 'Number Color',
		'input3' => 'Text',
		'input4' => 'Text Font Size',
		'select8' => array(
			'title' => 'Text Font Weight',
			'options' => array('100', '200', '300', '400', '500', '600', '700', '800', '900')
		),
		'select9' => array(
			'title' => 'Text Transform',
			'options' => array('None' => 'none', 'capitalize' => 'Capitalize', 'Uppercase' => 'uppercase', 'Lowercase' => 'lowercase')
		),
		'picker2' => 'Text Color',
		'select10' => array(
			'title' => 'Separator',
			'options' => $defaultAttrOptions
		),	
		'select11' => array(
			'title' => 'Separator Style',
			'options' => $separatorStyles
		),
		'picker3' => 'Separator Line Color',
		'input5' => 'Separator Line Width',
		'select12' => array(
			'title' => 'Separator Top Margin',
			'options' => $spacerHeights
		),
		'select13' => array(
			'title' => 'Separator Bottom Margin',
			'options' => $spacerHeights
		),
		'select14' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select15' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'input6' => 'Class'
	),
	'Blank Spacer' => array(
		'select1' => array(
			'title' => 'Height',
			'options' => $spacerHeights
		),
		'input1' => 'Class'
	),
	'Separator' => array(
		'select1' => array(
			'title' => 'Separator Style',
			'options' => $separatorStyles
		),
		'select2' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'picker1' => 'Line Color',
		'input1' => 'Custom Line Width',
		'input2' => 'Class'
	),
	'Separator With Text' => array(
		'select1' => array(
			'title' => 'Separator Style',
			'options' => $separatorTextStyles
		),
		'picker1' => 'Line Color',
		'input1' => 'Custom Line Width',
		'select2' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'input2' => 'Title',
		'select4' => array(
			'title' => 'Title Heading',
			'options' => array(
				'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
			)
		),
		'input3' => 'Class'
	),
	'Well' => array(
		'wpeditor1-content' => 'Well Text:::sdfaddwelltextblock',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Entrance Animation',
			'options' => $entranceAnimations
		),
		'select4' => array(
			'title' => 'Entrance Animation Duration',
			'options' => $entranceAnimationDuration
		),
		'select5' => array(
			'title' => 'Size',
			'options' => array(
				'Default' => '',
				'Large' => 'well-lg',
				'Small' => 'well-sm'
			)
		)
	),
	'Badge' => array(
		'input1' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Align',
			'options' => array(
				'Left' => 'left', 'Right' => 'right'
			)
		)
	),
	'Label' => array(
		'input1' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Align',
			'options' => array(
				'Left' => 'left', 'Right' => 'right'
			)
		),
		'select4' => array(
			'title' => 'Style',
			'options' => array(
				'Default' => 'default',
				'Primary' => 'primary',
				'Success' => 'success',
				'Info' => 'info',
				'Warning' => 'warning',
				'Danger' => 'danger'
			)
		)
	),
	'Contact' => array(
		'input1' => 'Title',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
		'select3' => array(
			'title' => 'Choose Form Layout',
			'options' => array(
				'Inline Inputs' => 'inline',
				'Stacked Inputs' => 'stacked'
			)
		),
		'select4' => array(
			'title' => 'Enable Captcha',
			'options' => $defaultAttrOptions
		),
		'input2' => 'Recipient Email'
	),
	'Raw HTML' => array(
		'textarea1-content' => 'Raw HTML',
		'select1' => array(
			'title' => 'Top Margin',
			'options' => $spacerHeights
		),
		'select2' => array(
			'title' => 'Bottom Margin',
			'options' => $spacerHeights
		),
	),
	'Raw JS' => array(
		'textarea1-content' => 'Raw JS',
	),
	'Pre Code' => array(
		'textarea1-content' => 'Code',
	),
	'Hero' => array(
		'select1' => array(
			'title' => 'Fullwidth Hero',
			'options' => $defaultAttrOptions
		),
		'select2' => array(
			'title' => 'Full Height Hero',
			'options' => $defaultAttrOptions
		),
		'input1' => 'Hero Height',
		'input2' => 'Padding Top',
		'input3' => 'Padding Bottom',
		'input4' => 'Margins',
		'select3' => array(
			'title' => 'Background Type',
			'options' => $customBackground
		),
		'input5' => 'Parallax Background Ratio',
		'picker1' => 'Background Color',
		'input6' => 'Background Color Alpha',
		'imageupload1' => 'Background Image:::background-image',
		'select4' => array(
			'title' => 'Background Repeat',
			'options' => $bgRepeatType
		),
		'input7' => 'Youtube Background Video',
		'select5' => array(
			'title' => 'Mute Background Video',
			'options' => $defaultAttrOptions
		),
		'picker2' => 'Box Shadow Color',
		'input8' => 'Box Shadow',
		'picker3' => 'Border Color',
		'select6' => array(
			'title' => 'Border Style',
			'options' => $borderStyle
		),
		'input9' => 'Border Width',
		'input10' => 'Class'
	),
	'Row' => array(
		'picker1' => 'Box Shadow Color',
		'input1' => 'Box Shadow',
		'picker2' => 'Border Color',
		'select1' => array(
			'title' => 'Border Style',
			'options' => $borderStyle
		),
		'input2' => 'Border Width',
		'input3' => 'Class'
	),
	'Column' => array(
		'input1' => 'Column Height',
		'input2' => 'Padding Top',
		'input3' => 'Padding Bottom',
		'select1' => array(
			'title' => 'Background Type',
			'options' => $customBackground
		),
		'input4' => 'Parallax Background Ratio',
		'picker1' => 'Background Color',
		'input5' => 'Background Color Alpha',
		'imageupload1' => 'Background Image:::background-image',
		'select2' => array(
			'title' => 'Background Repeat',
			'options' => $bgRepeatType
		),
		'input6' => 'Youtube Background Video',
		'select3' => array(
			'title' => 'Mute Background Video',
			'options' => $defaultAttrOptions
		),
		'picker2' => 'Box Shadow Color',
		'input7' => 'Box Shadow',
		'input8' => 'Border Radius',
		'picker3' => 'Border Color',
		'select4' => array(
			'title' => 'Border Style',
			'options' => $borderStyle
		),
		'input9' => 'Border Width',
		'input10' => 'Class'
	),
);
function html_attribute($array) {
 
	if (!is_array($array)) {
		return strtolower(str_replace(array(' ', '/'), '-', $array));
	}
 
	$newArray = array();
 
	foreach ($array as $key => $value) {
		$newArray[$key] = html_attribute(' ', '-', $value);
	}
	 
	return $newArray;
}
function get_element_type($element) {
	preg_match('/([a-z]+)/i', $element, $matches);
	return $matches[1];
}
function render_input($title) {
	$class = html_attribute($title);
	
// help text
$help_text = '';
$field_help_text = array (
	'rotator-separator' => '*enter separator string and use the same in title input to allow it\'s separation into rotating chunks',
	'font-size' => '*use px suffix after number value',
	'line-height' => '*use px suffix after number value',
	'fit-compressor' => '*if your text is resizing poorly, you\'ll want to turn tweak up/down using Compressor. The default is 1',
	'fit-min-font-size' => '*if you want to preserve hierarchy, you can specify pixel value for minimal font size',
	'fit-max-font-size' => '*if you want to preserve hierarchy, you can specify pixel value for maximal font size',
	'hero-height' => '*use px suffix after number value',
	'border-radius' => '*use unit suffix after number value',
	'button-border-radius' => '*use unit suffix after number value (fill this to change the radius of selected button style)',
	'button-height' => '*use px suffix after number value (fill this to change the height of selected button style)',
	'button-width' => '*use px suffix after number value (fill this to change the width of selected button style)',
	'button-top-margin' => '*use px suffix after number value',
	'button-bottom-margin' => '*use px suffix after number value',
	'padding-top' => '*use px suffix after number value',
	'padding-bottom' => '*use px suffix after number value',
	'margins' => '*this is shorthand property so use some value like this "25px 50px 75px 100px"',
	'class' => '*add CSS class which will be appended to this module and then refer to it in your custom css',
	'box-shadow' => '*e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px"',
	'border-width' => '*use px suffix after number value',
	'link' => '*use absolute URL value',
	'block' => '*make this module stacked',
	'posts-count' => '*used in "List" Layout Type',
	'rows' => '*used in "Grid" Layout Type',
	'word-count' => '*set excerpt length or enter 0 for showing full posts',
	'youtube-background-video' => '*use the complete URL, short URL provided by YouTube or the video ID',
	'video-oembed-url' => '*use full oEmbed URL. Check about supported formats at <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">WordPress Codex Page</a>',
	'parallax-background-ratio' => '*ratio is relative to the natural scroll speed, so a ratio of 0.5 would cause the element to scroll at half-speed, a ratio of 1 would have no effect, and a ratio of 2 would cause the element to scroll at twice the speed.',
	'background-color-alpha' => '*alpha (opacity) parameter is a number between 0.0 (fully transparent) and 1.0 (fully opaque)',
	'custom-line-width' => '*enter numeric value for line width and use unit suffix (default is 1px width)',
	'max-width' => '*enter numeric value for text module width and use unit suffix',
);
if( isset( $field_help_text[$class]) ) $help_text = '<span>' . $field_help_text[$class] . '</span>';

	echo '<label>' . $title . $help_text . '</label><input id="input-' .$class . '" name="' .$class . '" class="sdf-input ' . $class . '" type="text">';
}
function render_imageupload($title) {
	$parts = explode(':::', $title);
	echo '<label>' . $parts[0] . '</label>
	<div class="input-group">
	<input name="' . $parts[1] . '" type="text" class="wpu-image form-control sdf-image" value="" id="' . $parts[1] . '">
	<span class="input-group-btn">
	<span class="btn btn-custom btn-file wpu-media-upload">
	<i class="fa fa-upload"></i> Upload Image <input type="file">
	</span>
	</span>
	</div>';
}
function render_select($title, $options) {
	$class = html_attribute($title);
	
// help text
$help_text = '';
$field_help_text = array (
	'fit-text' => '*used to achieve scalable headlines that fill the width of a parent element. Check 3 options below.',
	'rotate-words' => '*enable/disable words rotation',
	'grid-layout' => '*used in "Grid" Layout Type',
	'columns' => '*used in "Grid" Layout Type',
	'rotator-delay' => '*choose value for delay',
	'posts-filter' => '*used in "Grid" Layout Type',
	'show-overlay-title' => '*used in "Grid" Layout Type',
	'grid-title-position' => '*used in "Grid" Layout Type',
	'list-item-image-position' => '*used in "List" Layout Type',
	'word-count' => '*used in "List" Layout Type',
	'show-images' => '*used in "List" Layout Type',
	'image-shape' => '*Notice: All image shapes are using cropped image version so if you want original image ratio pick these image sizes:"WP Full Size", "WP Large Size", "WP Medium Size".',
	'image-size' => '*Notice: "WP Full Size", "WP Large Size", "WP Medium Size" and "WP Thumbnail Size" are image sizes defined in WP Admin "Media Settings".',
	'thumbnails-size' => '*Notice: "WP Full Size", "WP Large Size", "WP Medium Size" and "WP Thumbnail Size" are image sizes defined in WP Admin "Media Settings".',
	'list-style' => '*choose list style type',
	'list-icon' => '*select FA Icon below. View <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FA Icons</a> in new Window.',
	'custom-button-icon' => '*select FA Icon below. View <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FA Icons</a> in new Window.',
	'icon' => '*choose FontAwesome Icon. You can see available icons here: <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">FA Icons</a>',
	'target' => '*specifies where to open the linked document',
	'module-display' => '*specifies CSS display property making module block or inline element',
	'nofollow' => '*use nofollow option (when linking to other websites) to avoid potential search engine penalties.',
	'portfolio-filter' => '*used in "Grid" Layout Type',
	'posts-filter' => '*used in "Grid" Layout Type',
	'show-prettyphoto-button' => '*used in "Grid" Layout Type',
	'show-item-link-button' => '*used in "Grid" Layout Type',
	'show-custom-button' => '*used in "Grid" Layout Type',
	'show-overlay-title' => '*used in "Grid" Layout Type',
	'lightbox' => '*Opens Image in Lightbox',
	'lightbox-ad' => '*Opens Ad in Lightbox',
	'has-retina-version' => '*Set to "Yes" if you have uploaded high-resolution version (image@2x) of this image',
);

if( isset( $field_help_text[$class]) ) $help_text = '<span>' . $field_help_text[$class] . '</span>';

	echo '<label>' . $title . $help_text . '</label><select id="input-' .$class . '" name="' .$class . '" class="sdf-select wpb_vc_param_value dropdown wpb-input wpb-select ' . $class . '">';
		foreach($options as $o) { 
			if(key($options)) { 
				$plain = key($options); 
				$att = $o;
				next($options);
			} else {
				$plain = $o;
				$att = $o; 
			}
			echo '<option value="' . $att . '" title="' . $att .'">' . $plain . '</option>';
		}
	echo '</select>';
}
function render_selectmulti($title, $options) {
	$class = html_attribute($title);
	echo '<label>' . $title . '</label><select multiple id="input-' .$class . '" name="' .$class . '" class="sdf-selectmulti wpb_vc_param_value dropdown wpb-input wpb-selectmulti ' . $class . '">';
		foreach($options as $o) { 
			if(key($options)) { 
				$plain = key($options); 
				$att = $o;
				next($options);
			} else {
				$plain = $o;
				$att = $o; 
			}
			echo '<option value="' . $att . '" title="' . $att .'">' . $plain . '</option>';
		}
	echo '</select>';
}
function render_textarea($title) {
	$class = html_attribute($title);
	echo '<label>' . $title . '</label><textarea id="input-' . $class . '" name="' . $class . '" class="sdf-textarea ' . $class . '"></textarea>';
}
function render_wpeditor($title) {

	$parts = explode(':::', $title);
	$class = html_attribute($parts[0]);
	// help text
	$help_text = '';
	$field_help_text = array (
		'list-content' => '*enter each list item in new line'
	);
	if( isset( $field_help_text[$class]) ) $help_text = '<span>' . $field_help_text[$class] . '</span>';
	echo '<label>' . $parts[0] . $help_text . '</label>';
	wp_editor('', $parts[1], $settings = array( 'editor_class' => 'sdf-textarea sdf-wpeditor', 'textarea_rows' => 7 )); 
}
function render_picker($title) {
	$class = html_attribute($title);
	
// help text
$help_text = '';
$field_help_text = array (
	'color' => '*use Picker, Enter hex Value or use Preset Color',
	'border-color' => '*use Picker, Enter hex Value or use Preset Color',
	'background-color' => '*use Picker, Enter hex Value or use Preset Color',
	'box-shadow-color' => '*use Picker, Enter hex Value or use Preset Color',
	'line-color' => '*use Picker, Enter hex Value or use Preset Color (default is body font color value)',
);
if( isset( $field_help_text[$class]) ) $help_text = '<span>' . $field_help_text[$class] . '</span>';

	echo '<label>' . $title . $help_text . '</label><div class="input-group">
	<input id="input-' . $class . '" name="' . $class . '" class="form-control color-picker sdf-picker ' . $class . '" type="text" value="" data-opacity="1" />
	<div class="input-group-btn">
	<button type="button" class="btn btn-default dropdown-toggle preset_colors_dd" data-toggle="dropdown">Preset Colors <span class="caret"></span></button>
	<ul class="dropdown-menu pull-right preset_colors"></ul>
	</div><!-- /btn-group -->
	</div><!-- /input-group -->';
}
?>
<div id="sdf-wrap" class="sdf-admin">
	<div class="sdf-builder-switch sdf-builder-icon">
	<button class="button-primary" type="button">SDF Builder</button>
	</div>
	<div class="sdf-builder-default">
	Turn on SDF Builder by default
	<input id="sdf-builder-default-switch" type="checkbox">
	</div>

	<div class="clr"></div>
	<div class="postbox sdf-builder-box">		
		<div class="inside">
			<div class="sdf-buttons">
				<button class="button sdf-add-element-button" type="button"><span class="icon"></span>Add Module</button>
				<button class="button sdf-add-row-button" type="button"><span class="icon"></span>Add Row</button>
				<button class="button sdf-add-hero-button" type="button"><span class="icon"></span>Add Hero</button>
				<div class="sdf-templates-list-wrap">
					<button class="button sdf-template-load" type="button" disabled><span class="sdf-icon"></span>Load Template</button>
					<div class="sdf-templates-list-box dn">
						<ul class="sdf-templates-list"></ul>
					</div>
				</div>
				<button class="button sdf-template-save" type="button" disabled><span class="sdf-icon"></span>Save Template</button>
				<button class="button sdf-paste" type="button" disabled><span class="sdf-icon"></span>Paste Row</button>
			</div>
			<div id="sdf-rows"></div>
		</div>
	</div>
</div>
<div class="sdf-loader-shade"></div>

<div class="sdf-admin">
<div id="modal-for-builder-module" class="modal fade md-effect-default" tabindex="-1" role="dialog" aria-labelledby="modal-for-builder-module" aria-hidden="true">
<div class="modal-dialog modal-vertical-center modal-lg" data-animation="md-effect-default">
<div class="modal-content">
<div class="modal-body">


	<div class="sdf-add-element-box">
	
		<button type="button" class="close sdf-dialog-close" data-dismiss="modal" aria-hidden="true">×</button>
		
		<div class="box-title invisible">Edit <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title="<p>For Box Shadow inputs be aware that aside number you need to enter unit (e.g. 500px)</p>"></i></div>
		<div class="buttons-set hidden">
				<button class="button button-primary sdf-button update" type="button">Update</button>
				<div class="clr"></div>
		</div>
	
		<div class="item sdf-add-element-selection hidden">
		<?php
		$i = 0;
		foreach($sdfElements as $k => $v) {
			$att = html_attribute($k);
			echo '<button class="button sdf-button" id="' . $att . '-button" type="button"><span class="icon" style="background-position: -' . $i * 24 . 'px 0"></span><span class="text">' . $k . '</span></button>
			';
			$i++;
		}
		?>
		</div>
		<?php
		foreach($sdfElements as $type => $inputs) {
			$att = html_attribute($type);
			echo '<div class="item hidden" id="' . $att .'-box">';
			foreach($inputs as $element => $v) {
				switch(get_element_type($element)) {
					case 'input' :
						render_input($v);
						break;
					case 'imageupload' :
						render_imageupload($v);
					break;
					case 'select' :
						render_select($v['title'], $v['options']);
						break;
					case 'selectmulti' :
						render_selectmulti($v['title'], $v['options']);
						break;
					case 'textarea' :
						render_textarea($v);
						break;
					case 'wpeditor' :
						render_wpeditor($v);
						break;
					case 'picker' :
						render_picker($v);
						break;
				}
			}
			echo '</div>';
		}
		?>
	
		<div class="buttons-set hidden">
			<button class="button sdf-button back" type="button">Back to Selection</button>
			<button class="button button-primary sdf-button update" type="button">Update</button>
			<div class="clr"></div>
		</div>
	
	</div>

</div>
</div>
</div>
</div>
</div>

<div class="sdf-row" id="original-sdf-row" sdfclass="">
	<div class="icon sdf-row-sorter" title="Drag to reposition this row"></div>
	<div class="icon sdf-row-layout" title="Select column layout">
		<button class="sdf-layout-switch" type="button"></button>
		<div class="inner">
			<?php 
			$rows = array("1/1", "1/2 1/2", "1/3 2/3", "2/3 1/3", "1/3 1/3 1/3", "1/4 1/4 1/4 1/4", "1/4 3/4", "3/4 1/4", "1/4 1/2 1/4", "1/6 5/6", "5/6 1/6", "1/6 1/6 1/6 1/6 1/6 1/6", "1/6 4/6 1/6", "1/6 1/6 1/6 1/2");
			foreach($rows as $r) {
				echo '<button class="layout-button" id="layout-button-'.$r.'" type="button">'.$r.'</button>';
			}
			?>
			<button id="sdf-custom-layout-button" class="layout-button" type="button">Custom Layout</button>
		</div>
	</div>
	<div class="sdf-row-content clr"></div>
	<div class="clr"></div>
	<div class="sdf-actions sdf-row-actions"><div class="sdf-icon sdf-edit sdf-row-edit" title="Edit Row"></div><div class="sdf-icon sdf-clone sdf-row-clone" title="Clone Row"></div><div class="sdf-icon sdf-clipboard sdf-row-clipboard" title="Copy row to SDF clipboard"></div><div class="sdf-icon sdf-delete sdf-row-delete" title="Delete Row"></div></div>
</div>
<div class="sdf-shortcodes dn"></div>
<div class="sdf-custom-layout-box dn">
	<input type="text" class="sdf-enter" />
	<button type="button" class="button">OK</button>
	<p class="mask">Please enter your layout as fractions separated with spaces, e.g. <strong>1/4 1/8 5/8</strong></p>
	<p class="error">Format error. Please correct the format.</p>
</div>
<div class="sdf-template-name dn">
	<input type="text" class="sdf-enter" />
	<button type="button" class="button">OK</button>
</div>
<div class="sdf-image-load"></div>
<script>
/*! waitForImages jQuery Plugin 2014-11-14 */
!function(a){var b="waitForImages";a.waitForImages={hasImageProperties:["backgroundImage","listStyleImage","borderImage","borderCornerImage","cursor"],hasImageAttributes:["srcset"]},a.expr[":"].uncached=function(b){if(!a(b).is('img[src][src!=""]'))return!1;var c=new Image;return c.src=b.src,!c.complete},a.fn.waitForImages=function(){var c,d,e,f=0,g=0,h=a.Deferred();if(a.isPlainObject(arguments[0])?(e=arguments[0].waitForAll,d=arguments[0].each,c=arguments[0].finished):1===arguments.length&&"boolean"===a.type(arguments[0])?e=arguments[0]:(c=arguments[0],d=arguments[1],e=arguments[2]),c=c||a.noop,d=d||a.noop,e=!!e,!a.isFunction(c)||!a.isFunction(d))throw new TypeError("An invalid callback was supplied.");return this.each(function(){var i=a(this),j=[],k=a.waitForImages.hasImageProperties||[],l=a.waitForImages.hasImageAttributes||[],m=/url\(\s*(['"]?)(.*?)\1\s*\)/g;e?i.find("*").addBack().each(function(){var b=a(this);b.is("img:uncached")&&j.push({src:b.attr("src"),element:b[0]}),a.each(k,function(a,c){var d,e=b.css(c);if(!e)return!0;for(;d=m.exec(e);)j.push({src:d[2],element:b[0]})}),a.each(l,function(c,d){var e,f=b.attr(d);return f?(e=f.split(","),void a.each(e,function(c,d){d=a.trim(d).split(" ")[0],j.push({src:d,element:b[0]})})):!0})}):i.find("img:uncached").each(function(){j.push({src:this.src,element:this})}),f=j.length,g=0,0===f&&(c.call(i[0]),h.resolveWith(i[0])),a.each(j,function(e,j){var k=new Image,l="load."+b+" error."+b;a(k).one(l,function m(b){var e=[g,f,"load"==b.type];return g++,d.apply(j.element,e),h.notifyWith(j.element,e),a(this).off(l,m),g==f?(c.call(i[0]),h.resolveWith(i[0]),!1):void 0}),k.src=j.src})}),h.promise()}}(jQuery);

sdfBuilder = {
	activeRow : '',
	activeCol : '',
	activeElement : '',
	elementType : '',
	action : '',
	extraClassNode : '',
	colSpacing : 10,
	colRightGutter : 30,
	elementHeight : 80,
	contentAttributeList : [ 
		<?php 
			$content = ''; 
			foreach($sdfElements as $type => $inputs) {
				reset($inputs);
				if(strpos(key($inputs), 'content') !== false) {
					$att = html_attribute($type);
					$content .= "['$att','" . preg_replace('/[^:]+:::/', '', html_attribute(reset($inputs))) . "'],";
				}
			}
			echo substr($content, 0, -1) 
		?> 
	],
	widths : {
	'1/12' : 'col-md-1',
	'1/6' : 'col-md-2',
	'1/4' : 'col-md-3',
	'1/3' : 'col-md-4',
	'5/12' : 'col-md-5',
	'1/2' : 'col-md-6',
	'7/12' : 'col-md-7',
	'2/3' : 'col-md-8',
	'3/4' : 'col-md-9',
	'5/6' : 'col-md-10',
	'11/12' : 'col-md-11',
	'1/1' : 'col-md-12'
	},	
	moduleWidths : ['1/12', '1/6', '1/4', '1/3', '5/12', '1/2', '7/12', '2/3', '3/4', '5/6', '11/12', '1/1'],
	contentAttribute : function(t) {
		var a = '';
		jQuery.each(sdfBuilder.contentAttributeList, function(i,v) {
			if(v[0] == t) {
				a = v[1];
			}
		});
		return a;
	},
	addRow : function(addCol, attributes) {
		attributes = attributes || {};
		var newId = 'sdf-row-' + (jQuery('#sdf-wrap .sdf-row').length + 1);
		attributes.id = newId;
		jQuery('#original-sdf-row').clone(true).appendTo('#sdf-wrap #sdf-rows').attr(attributes);
		if(addCol==true) { 
			jQuery('#sdf-rows #' + newId + ' .layout-button:first-child').trigger('click');
		}
		if(addCol=='fromeditor') { 
			sdfBuilder.rowLayout(['1/1'],jQuery('#sdf-row-1'), true);
		}
		sdfBuilder.activeRow = newId;
		if(attributes.sdfhero == 'hero') {
			jQuery('#sdf-rows #' + newId).addClass('sdf-hero').append('<div class="hero-label">Hero</div>');
		}
		jQuery('.sdf-template-save').removeAttr('disabled');
		if(!attributes.fromEditor) {
			sdfBuilder.scrollToRow(newId);
		}
		return newId;
	},
	editRow : function(e) {
		sdfBuilder.elementType = 'row';
		sdfBuilder.activeElement = 'newrow';
		sdfBuilder.addElementDialog();
		jQuery('.sdf-add-element-selection #row-button').triggerHandler('click');
		jQuery('.sdf-add-element-box .box-title').html('Edit Row').removeClass('invisible');
		jQuery('.sdf-add-element-box .box-title').append(' <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title="<p>For Box Shadow inputs be aware that aside number you need to enter unit (e.g. 500px)</p>"></i>');
		jQuery('[data-toggle="tooltip"]').tooltip();
		jQuery('.sdf-add-element-box .back').hide();
		jQuery('#row-box #input-box-shadow').val(jQuery(e.target).parents('.sdf-row').attr('sdfboxshadow'));
		
		var row_sdfboxshadowcolor = (jQuery(e.target).parent().closest('.sdf-row').attr('sdfboxshadowcolor')) ? jQuery(e.target).parent().closest('.sdf-row').attr('sdfboxshadowcolor') : '';
		jQuery('#row-box #input-box-shadow-color').val(row_sdfboxshadowcolor).minicolors('value', row_sdfboxshadowcolor);
		
		var row_sdfbordercolor = (jQuery(e.target).parent().closest('.sdf-row').attr('sdfbordercolor')) ? jQuery(e.target).parent().closest('.sdf-row').attr('sdfbordercolor') : '';
		jQuery('#row-box #input-border-color').val(row_sdfbordercolor).minicolors('value', row_sdfbordercolor);
		
		jQuery('#row-box #input-border-style').val(jQuery(e.target).parents('.sdf-row').attr('sdfborderstyle'));
		jQuery('#row-box #input-border-width').val(jQuery(e.target).parents('.sdf-row').attr('sdfborderwidth'));
		jQuery('#row-box #input-class').val(jQuery(e.target).parents('.sdf-row').attr('sdfclass'));
	},
	scrollToRow : function(rowId) {
		if(jQuery('#sdf-rows #' + rowId).offset().top > jQuery(window).scrollTop() + jQuery(window).height() - 50) {
			jQuery('#sdf-rows #' + rowId).css('visibility','hidden');
			jQuery('html, body').animate({ scrollTop: jQuery('#sdf-rows #' + rowId).offset().top - jQuery(window).height() + 200 },400, function() {jQuery('#sdf-rows #' + rowId).css('visibility','visible');});
		}
	},
	editColumn : function(e) {
		sdfBuilder.elementType = 'column';
		sdfBuilder.activeElement = 'newcolumn';
		sdfBuilder.addElementDialog();
		jQuery('.sdf-add-element-selection #column-button').triggerHandler('click');
		jQuery('.sdf-add-element-box .box-title').html('Edit Column').removeClass('invisible');
		jQuery('.sdf-add-element-box .box-title').append(' <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title="<p>For Height, Padding and Box Shadow inputs be aware that aside number you need to enter unit (e.g. 500px)</p>"></i>');
		jQuery('[data-toggle="tooltip"]').tooltip();
		jQuery('.sdf-add-element-box .back').hide();
		jQuery('#column-box #input-column-height').val(jQuery(e.target).parents('.sdf-col').attr('sdfcolumnheight'));
		jQuery('#column-box #input-padding-top').val(jQuery(e.target).parents('.sdf-col').attr('sdfpaddtop'));
		jQuery('#column-box #input-padding-bottom').val(jQuery(e.target).parents('.sdf-col').attr('sdfpaddbottom'));
		jQuery('#column-box #input-background-type').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgtype'));
		var column_sdfbg = (jQuery(e.target).parent().closest('.sdf-col').attr('sdfbg')) ? jQuery(e.target).parent().closest('.sdf-col').attr('sdfbg') : '';
		jQuery('#column-box #input-background-color').val(column_sdfbg).minicolors('value', column_sdfbg);
		jQuery('#column-box #input-background-color-alpha').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgalpha'));
		jQuery('#column-box #background-image').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgimg'));
		jQuery('#column-box #input-background-repeat').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgrep'));
		jQuery('#column-box #input-parallax-background-ratio').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgrat'));
		jQuery('#column-box #input-youtube-background-video').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgvid'));
		jQuery('#column-box #input-mute-background-video').val(jQuery(e.target).parents('.sdf-col').attr('sdfbgvidmute'));
		jQuery('#column-box #input-box-shadow').val(jQuery(e.target).parents('.sdf-col').attr('sdfboxshadow'));
		var column_sdfboxshadowcolor = (jQuery(e.target).parent().closest('.sdf-col').attr('sdfboxshadowcolor')) ? jQuery(e.target).parent().closest('.sdf-col').attr('sdfboxshadowcolor') : '';
		jQuery('#column-box #input-box-shadow-color').val(column_sdfboxshadowcolor).minicolors('value', column_sdfboxshadowcolor);
		var column_sdfbordercolor = (jQuery(e.target).parent().closest('.sdf-col').attr('sdfbordercolor')) ? jQuery(e.target).parent().closest('.sdf-col').attr('sdfbordercolor') : '';
		jQuery('#column-box #input-border-color').val(column_sdfbordercolor).minicolors('value', column_sdfbordercolor);
		jQuery('#column-box #input-border-style').val(jQuery(e.target).parents('.sdf-col').attr('sdfborderstyle'));
		jQuery('#column-box #input-border-radius').val(jQuery(e.target).parents('.sdf-col').attr('sdfborderradius'));
		jQuery('#column-box #input-border-width').val(jQuery(e.target).parents('.sdf-col').attr('sdfborderwidth'));
		jQuery('#column-box #input-class').val(jQuery(e.target).parents('.sdf-col').attr('sdfclass'));		
	},
	editHero : function(e) {
		sdfBuilder.elementType = 'hero';
		sdfBuilder.activeElement = 'newhero';
		sdfBuilder.addElementDialog();
		jQuery('.sdf-add-element-selection #hero-button').triggerHandler('click');
		jQuery('.sdf-add-element-box .box-title').html('Edit Hero').removeClass('invisible');
		jQuery('.sdf-add-element-box .box-title').append(' <i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="auto right" data-html="true" title="<p>For Height, Padding, Margin and Box Shadow inputs be aware that aside number you need to enter unit (e.g. 500px)</p>"></i>');
		jQuery('[data-toggle="tooltip"]').tooltip();
		jQuery('.sdf-add-element-box .back').hide();
		jQuery('#hero-box #input-hero-height').val(jQuery(e.target).parents('.sdf-row').attr('sdfheroheight'));
		jQuery('#hero-box #input-padding-top').val(jQuery(e.target).parents('.sdf-row').attr('sdfpaddtop'));
		jQuery('#hero-box #input-padding-bottom').val(jQuery(e.target).parents('.sdf-row').attr('sdfpaddbottom'));
		jQuery('#hero-box #input-background-type').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgtype'));
		jQuery('#hero-box #input-fullwidth-hero').val(jQuery(e.target).parents('.sdf-row').attr('sdffullwidthhero'));
		jQuery('#hero-box #input-full-height-hero').val(jQuery(e.target).parents('.sdf-row').attr('sdffullheighthero'));
		
		var hero_sdfbg = (jQuery(e.target).parent().closest('.sdf-row').attr('sdfbg')) ? jQuery(e.target).parent().closest('.sdf-row').attr('sdfbg') : '';
		jQuery('#hero-box #input-background-color').val(hero_sdfbg).minicolors('value', hero_sdfbg);
		
		jQuery('#hero-box #input-background-color-alpha').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgalpha'));
		
		jQuery('#hero-box #background-image').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgimg'));
		jQuery('#hero-box #input-background-repeat').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgrep'));
		jQuery('#hero-box #input-parallax-background-ratio').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgrat'));
		jQuery('#hero-box #input-youtube-background-video').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgvid'));
		jQuery('#hero-box #input-mute-background-video').val(jQuery(e.target).parents('.sdf-row').attr('sdfbgvidmute'));
		jQuery('#hero-box #input-margins').val(jQuery(e.target).parents('.sdf-row').attr('sdfmargins'));
		jQuery('#hero-box #input-box-shadow').val(jQuery(e.target).parents('.sdf-row').attr('sdfboxshadow'));
		
		var hero_sdfboxshadowcolor = (jQuery(e.target).parent().closest('.sdf-row').attr('sdfboxshadowcolor')) ? jQuery(e.target).parent().closest('.sdf-row').attr('sdfboxshadowcolor') : '';
		jQuery('#hero-box #input-box-shadow-color').val(hero_sdfboxshadowcolor).minicolors('value', hero_sdfboxshadowcolor);
		
		var hero_sdfbordercolor = (jQuery(e.target).parent().closest('.sdf-row').attr('sdfbordercolor')) ? jQuery(e.target).parent().closest('.sdf-row').attr('sdfbordercolor') : '';
		jQuery('#hero-box #input-border-color').val(hero_sdfbordercolor).minicolors('value', hero_sdfbordercolor);
		
		jQuery('#hero-box #input-border-style').val(jQuery(e.target).parents('.sdf-row').attr('sdfborderstyle'));
		jQuery('#hero-box #input-border-width').val(jQuery(e.target).parents('.sdf-row').attr('sdfborderwidth'));
		jQuery('#hero-box #input-class').val(jQuery(e.target).parents('.sdf-row').attr('sdfclass'));
	},
	rowLayoutEvent :  function() {
		jQuery('#sdf-wrap').on('click', '.layout-button:not(.active)', function(e) {
			jQuery(this).addClass('active').siblings('.layout-button').removeClass('active');
			sdfBuilder.activeRow = jQuery(this).parents('.sdf-row').attr('id');
			if(jQuery(e.target).attr('id')=='sdf-custom-layout-button') {	
				jQuery('.sdf-custom-layout-box').dialog({
					dialogClass: 'sdf-dialog sdf-custom-layout-dialog',
					width: 240,
					height: 140,
					modal: true,
					title: 'Custom Layout',
					closeText: '',
					closeOnEscape: true,
					open: function(ev, ui) { 
						jQuery('.sdf-custom-layout-box').attr('title', jQuery(e.target).parents('.sdf-row').attr('id'));
						var currentLayout = jQuery(e.target).parents('.sdf-row').attr('sdf-cols');
						if(currentLayout) {
							jQuery('.sdf-custom-layout-box input').val(currentLayout);
						} else {
							jQuery('.sdf-custom-layout-box input').val('');
						}
					}
				});	
			} else {
				var row = jQuery(this).parents('.sdf-row');
				var fractions = jQuery(this).attr('id').replace('layout-button-', '').split(' ');
				sdfBuilder.rowLayout(fractions, row);	
			}
		});
	},
	rowLayout : function(fractions, row, fromEditor) {
		var content = row.find('.sdf-row-content');
		var oldFractions = row.attr('sdf-cols');
		oldFractionsLength = (oldFractions) ? oldFractions.split(' ').length : 0;
		row.attr('sdf-cols', fractions.join(' '));
		var colsTotal = 0;
		var rowId = row.attr('id');
		jQuery.each(fractions, function (i, v) {
			var fraction = v.split('/');
			var num = fraction[0];
			var denom = fraction[1];
			colsTotal += num / denom;
			var colId = 'sdf-col-' + i;
			var colWidth = sdfBuilder.colWidth(v,fractions.length);
			if(i >= oldFractionsLength) {
				content.append(sdfBuilder.column(v,colId,colWidth));
			} else {
				var nextCol = jQuery('#' + rowId + ' .sdf-col:nth-child(' + (i+1) + ')');
				nextCol.attr({'sdf-width' : v, 'id' : colId, 'class' : 'sdf-col ' + colWidth});
				nextCol.find('.sdf-col-label').html(num + " / " + denom);
			}
		});
		if(fractions.length < oldFractionsLength) {
			jQuery.each(oldFractions.split(' ').slice(fractions.length), function (i, v) {
				var cell = '#' + rowId + ' .sdf-col:nth-child(' + (fractions.length + i + 1) + ')';
				jQuery(cell + ' .sdf-element').each(function(ii, vv) { 
					jQuery(vv).appendTo('#' + rowId + ' .sdf-col:nth-child(' + fractions.length + ') .sdf-col-content'); 
				});
				jQuery(cell).remove();
			});
			var cols = jQuery('#' + rowId + ' .sdf-col');
			if((cols.length) > fractions.length) {
				cols.last().remove();
			}
		}
		if(colsTotal > 1) { alert('Warning: columns\' total width exceeds row width!'); }
		
		sdfBuilder.fixAttributes();
		// if(fromEditor != true) { 
			// sdfBuilder.updateEditor(true);
		// }
	},
	resetCustomLayout : function() {
		jQuery('.sdf-custom-layout-box input').val('').removeClass('error-format');
		jQuery('.sdf-custom-layout-box .error').hide();
	},
	enumerateRows : function() {
		jQuery('#sdf-rows .sdf-row').each(function(i,v) {
			jQuery(v).attr('id', 'sdf-row-' + (i + 1));
			jQuery('#' + v.id + ' .sdf-col').each(function(ii, vv) { 
				jQuery(vv).attr('id', 'sdf-col-' + (ii + 1));
			});
			jQuery('#sdf-rows .sdf-row').removeAttr('sdflast');
			jQuery('#sdf-rows .sdf-row').last().attr('sdflast', 'yes');
		});
	},
	fixAttributes : function() {
		sdfBuilder.enumerateRows();
		sdfBuilder.refreshSortables();
	},
	setActiveComponents : function(el) {
		sdfBuilder.activeRow = jQuery(el).parents('.sdf-row').attr('id');
		sdfBuilder.activeCol = jQuery(el).parents('.sdf-col').attr('id');
		sdfBuilder.activeElement = jQuery(el).parents('.sdf-element').attr('id') || 'new';
		jQuery('.sdf-col').removeClass('selected');
		jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol).addClass('selected');
	},
	column : function(fraction,colId,colWidth) {
		return '<div class="sdf-col ' + colWidth + '" sdf-width="' + fraction + '" id="' + colId + '" sdfclass=""><div class="sdf-col-content"></div><div class="icon sdf-add-element" title="Add Module"></div><div class="sdf-col-label">' + fraction.replace('/', ' / ') + '</div><div class="sdf-actions sdf-col-actions"><div class="sdf-icon sdf-edit sdf-col-edit" title="Edit Column"></div><div class="sdf-icon sdf-delete sdf-col-delete" title="Delete Column"></div></div></div>';
	},
	element : function(id, type, parent, content, moduleWidth) {
		return '<div class="sdf-element sdf-element-' + type + ' ' + sdfBuilder.widths[moduleWidth] + '" id="sdf-element-' + id + '" sdf-title="' + type + '" module_width="' + moduleWidth + '" ><div class="sdf-element-content"><div class="inner">' + content + '</div></div><div class="sdf-element-type">' + type.replace(/-/g, ' ') + '</div><div class="sdf-module-size"><a class="block-builder-box-btn block-builder-box-size-dec" href="javascript:void(0);">-</a><a class="block-builder-box-btn block-builder-box-size-inc" href="javascript:void(0);">+</a><span class="block-builder-box-width">' + moduleWidth + '</span></div><div class="sdf-actions sdf-element-actions"><div class="sdf-icon sdf-edit sdf-element-edit" title="Edit Module"></div><div class="sdf-icon sdf-clone" title="Clone Module"></div><div class="sdf-icon sdf-delete sdf-element-delete" title="Delete Module"></div></div></div>';
	},
	addElement : function (element, edit) {
		if(!edit) {
			sdfBuilder.setActiveComponents(element);
		}
		sdfBuilder.addElementDialog();
		jQuery('#modal-for-builder-module').on('shown.bs.modal', function() {
			if(sdfBuilder.elementType == '') {
				jQuery('.sdf-add-element-box .back').triggerHandler('click');
			} else {
				jQuery('.sdf-add-element-selection #' + sdfBuilder.elementType + '-button').triggerHandler('click');
			}
			
			sdfBuilder.addMiniColors();
			
			if(sdfBuilder.elementType == 'headline'){
				var headline_sdftextrcolor = (jQuery(element).closest('.sdf-element-headline').find('#sdf-element-text-color').html()) ? jQuery(element).closest('.sdf-element-headline').find('#sdf-element-text-color').html() : '';
				jQuery('#headline-box #input-text-color').val(headline_sdftextrcolor).minicolors('value', headline_sdftextrcolor);
			}
			else if(sdfBuilder.elementType == "text-block"){
				var text_block_sdftextrcolor = (jQuery(element).closest('.sdf-element-text-block').find('#sdf-element-text-color').html()) ? jQuery(element).closest('.sdf-element-text-block').find('#sdf-element-text-color').html() : '';
				jQuery('#text-block-box #input-text-color').val(text_block_sdftextrcolor).minicolors('value', text_block_sdftextrcolor);
			}
			else if(sdfBuilder.elementType == 'icon'){
				var icon_sdftextrcolor = (jQuery(element).closest('.sdf-element-icon').find('#sdf-element-icon-color').html()) ? jQuery(element).closest('.sdf-element-icon').find('#sdf-element-icon-color').html() : '';
				jQuery('#icon-box #input-icon-color').val(icon_sdftextrcolor).minicolors('value', icon_sdftextrcolor);
			}
			
			jQuery(document).off('focusin.modal');
		});
	},
	addElementDialog : function () {
		var modal_for_module = jQuery('#modal-for-builder-module');
		modal_for_module.modal('show');
		modal_for_module.on('show.bs.modal', function() {
			var modal_dialog = jQuery('#modal-for-builder-module .modal-dialog');
			modal_dialog.removeClass("animate_now");
			var animation_type = (modal_dialog.data("animation") != 'No') ? modal_dialog.data("animation") : 'default';
			modal_dialog.addClass(animation_type);
		});
	},
	addMiniColors : function () {
		// Enabling miniColors
		jQuery('.sdf-add-element-box').find('input.color-picker').minicolors({
			defaultValue: '',
			theme: 'bootstrap',
			position: 'bottom right',
			change: function(hex, opacity) {
					if( !hex ) return;
					if( opacity ) hex += ', ' + opacity;
					try {
						console.log(hex);
					} catch(e) {}
				},
		});
	},
	selectElementType : function (el) {
		var elId = el.attr('id');
		jQuery('.sdf-add-element-selection').addClass('hidden');
		sdfBuilder.elementType = elId.replace(/-button$/, '');
		jQuery('#'+elId.replace(/-button$/, '-box')).removeClass('hidden');
		jQuery('.sdf-add-element-box .buttons-set').removeClass('hidden');
		if(sdfBuilder.activeElement=='new') { 
			var action = 'Insert'; 
			jQuery('.sdf-add-element-box .back').show();
			var elementContent = '';
			jQuery('.sdf-select option:selected').removeAttr('selected');
			jQuery('.sdf-input, .sdf-textarea, .sdf-image').val('');
			if(jQuery('.sdf-add-element-box .wp-editor-wrap').hasClass('tmce-active')) { 
				tinymce.activeEditor.setContent('');
			}
		} else { 
			var action = 'Edit'; 
			jQuery('.sdf-add-element-box .back').hide();
			var element = '#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' .sdf-col-content' + ' #' + sdfBuilder.activeElement;
			switch(elId) {
				<?php
				foreach($sdfElements as $type => $inputs) {
					$att = html_attribute($type);
					echo "case '$att-button' : \n";
					foreach($inputs as $element => $title) {
						$v = html_attribute( (gettype($title)=="string") ? $title : $title['title'] );
						$vis = ".sdf-add-element-box .item:visible";
						if($att == 'hero' || $att == 'row' || $att == 'column') { echo "element = element.replace('# ', '').replace('#new" . $att . "', ''); \n"; }
						switch(get_element_type($element)) {
							case 'input' :
								echo "jQuery('$vis .sdf-input.$v').val(jQuery(element + ' #sdf-element-$v').html()); \n";
								// headline title fallback
								if ($att == 'headline' && $v == 'title') {
									echo "var fall_title_len = jQuery(element + ' #sdf-element-$v').not(\".content\").length;
												if( fall_title_len && jQuery(element + ' #sdf-element-$v').not(\".content\").html() != 'undefined'){
													jQuery('$vis .sdf-input.$v').val(jQuery(element + ' #sdf-element-$v').not(\".content\").html());
													jQuery(element + ' #sdf-element-$v.content').html(jQuery(element + ' #sdf-element-$v').not(\".content\").html());
												} \n";
								}
								break;
							case 'imageupload' :
								$v = explode(':::', $v); $v = $v[1];
								echo "jQuery('$vis #$v').val(jQuery(element + ' #sdf-element-$v').attr('src')); \n";
								break;
							case 'select' :
								echo "var selValue = jQuery(element + ' #sdf-element-$v').attr('sdf-val') || jQuery(element + ' #sdf-element-$v').html();
									jQuery('$vis .sdf-select.$v option:selected').removeAttr('selected');
									jQuery('$vis').val(selValue);
									jQuery('$vis .sdf-select.$v option[value=\"' + selValue + '\"]').attr('selected', true); \n";
								break;
							case 'selectmulti' :
								echo "var selValue = jQuery(element + ' #sdf-element-$v').attr('sdf-val') || jQuery(element + ' #sdf-element-$v').html();
											jQuery('$vis .sdf-selectmulti.$v option:selected').removeAttr('selected');
											jQuery.each(selValue.split(','), function(i,e){
												jQuery('$vis').val(e);
												jQuery('$vis .sdf-selectmulti.$v option[value=\"' + e + '\"]').attr('selected', true);
											});\n";
								break;
							case 'textarea' :
								echo "jQuery('$vis .sdf-textarea.$v').val(jQuery(element + ' #sdf-element-$v').html()); \n";
								break;
							case 'wpeditor' :
								$v = explode(':::', $v); $v = $v[1];
								echo "var elementContent = jQuery(element + ' .sdf-element-content .inner #sdf-element-$v').html();
								jQuery('#$v').val(elementContent).focus();
								if(jQuery('$vis #wp-$v-wrap').hasClass('tmce-active')) {	
									tinymce.get('$v').setContent(elementContent, {format : 'raw'});
								} \n";
								break;
							case 'picker' :
								echo "jQuery('$vis .sdf-picker.$v').val(jQuery(element + ' #sdf-element-$v').html()); \n";
								break;
						}
					}
					echo "break;\n";
				}
				?>
			}
		}
		jQuery('.sdf-add-element-box .box-title').html(action + ' ' + jQuery('.text', el).html()).removeClass('invisible');
	},
	updateElement : function (type) {
		var elementContent = '';
		if(type == 'hero' || type == 'row' || type == 'column') {
			switch(type) {
				case 'hero' :
					if(sdfBuilder.activeElement == 'new') {
						jQuery('.sdf-row[sdftemp="true"]').remove();
						sdfBuilder.addRow(true, {sdfhero : 'hero', sdfheroheight : jQuery('#hero-box #input-hero-height').val(), sdfpaddtop : jQuery('#hero-box #input-padding-top').val(), sdfpaddbottom : jQuery('#hero-box #input-padding-bottom').val(), sdffullwidthhero : jQuery('#hero-box #input-fullwidth-hero').val(), sdffullheighthero : jQuery('#hero-box #input-full-heighth-hero').val(), sdfbgtype : jQuery('#hero-box #input-background-type').val(), sdfbg : jQuery('#hero-box #input-background-color').val(), sdfbgalpha : jQuery('#hero-box #input-background-color-alpha').val(), sdfbgimg : jQuery('#hero-box #background-image').val(), sdfbgrep : jQuery('#hero-box #input-background-repeat').val(), sdfbgrat : jQuery('#hero-box #input-parallax-background-ratio').val(), sdfbgvid : jQuery('#hero-box #input-youtube-background-video').val(), sdfbgvidmute : jQuery('#hero-box #input-mute-background-video').val(), sdfmargins : jQuery('#hero-box #input-margins').val(), sdfboxshadow : jQuery('#hero-box #input-box-shadow').val(), sdfboxshadowcolor : jQuery('#hero-box #input-box-shadow-color').val(), sdfborderwidth : jQuery('#hero-box #input-border-width').val(), sdfborderstyle : jQuery('#hero-box #input-border-style').val(), sdfbordercolor : jQuery('#hero-box #input-border-color').val(), sdfclass : jQuery('#hero-box #input-class').val()});
						jQuery('#sdf-rows .sdf-row').last().addClass('sdf-hero');
					} else {
						jQuery('#' + sdfBuilder.activeRow).attr({ 'sdfheroheight' : jQuery('#hero-box #input-hero-height').val(), 'sdfpaddtop' : jQuery('#hero-box #input-padding-top').val(), 'sdfpaddbottom' : jQuery('#hero-box #input-padding-bottom').val(), 'sdffullwidthhero' : jQuery('#hero-box #input-fullwidth-hero').val(), 'sdffullheighthero' : jQuery('#hero-box #input-full-height-hero').val(), 'sdfbgtype' : jQuery('#hero-box #input-background-type').val(), 'sdfbg' : jQuery('#hero-box #input-background-color').val(), 'sdfbgalpha' : jQuery('#hero-box #input-background-color-alpha').val(), 'sdfbgimg' : jQuery('#hero-box #background-image').val(), 'sdfbgrep' : jQuery('#hero-box #input-background-repeat').val(), 'sdfbgrat' : jQuery('#hero-box #input-parallax-background-ratio').val(), 'sdfbgvid' : jQuery('#hero-box #input-youtube-background-video').val(), 'sdfbgvidmute' : jQuery('#hero-box #input-mute-background-video').val(), 'sdfmargins' : jQuery('#hero-box #input-margins').val(), 'sdfboxshadow' : jQuery('#hero-box #input-box-shadow').val(), 'sdfboxshadowcolor' : jQuery('#hero-box #input-box-shadow-color').val(), 'sdfborderwidth' : jQuery('#hero-box #input-border-width').val(), 'sdfborderstyle' : jQuery('#hero-box #input-border-style').val(), 'sdfbordercolor' : jQuery('#hero-box #input-border-color').val(), 'sdfclass' : jQuery('#hero-box #input-class').val() });
					}
				break;
				case 'row' :
					if(sdfBuilder.activeElement == 'new') {
						jQuery('.sdf-row[sdftemp="true"]').remove();
						sdfBuilder.addRow(true, { sdfboxshadow : jQuery('#row-box #input-box-shadow').val(), sdfboxshadowcolor : jQuery('#row-box #input-box-shadow-color').val(), sdfborderwidth : jQuery('#row-box #input-border-width').val(), sdfborderstyle : jQuery('#row-box #input-border-style').val(), sdfbordercolor : jQuery('#row-box #input-border-color').val(), sdfclass : jQuery('#row-box #input-class').val()});
					} else {
						jQuery('#' + sdfBuilder.activeRow).attr({ 'sdfboxshadow' : jQuery('#row-box #input-box-shadow').val(), 'sdfboxshadowcolor' : jQuery('#row-box #input-box-shadow-color').val(), 'sdfborderwidth' : jQuery('#row-box #input-border-width').val(), 'sdfborderstyle' : jQuery('#row-box #input-border-style').val(), 'sdfbordercolor' : jQuery('#row-box #input-border-color').val(), 'sdfclass' : jQuery('#row-box #input-class').val() });
					}
				break;
				case 'column' :
					jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol).attr({ 'sdfcolumnheight' : jQuery('#column-box #input-column-height').val(), 'sdfpaddtop' : jQuery('#column-box #input-padding-top').val(), 'sdfpaddbottom' : jQuery('#column-box #input-padding-bottom').val(), 'sdfbgtype' : jQuery('#column-box #input-background-type').val(), 'sdfbg' : jQuery('#column-box #input-background-color').val(), 'sdfbgalpha' : jQuery('#column-box #input-background-color-alpha').val(), 'sdfbgimg' : jQuery('#column-box #background-image').val(), 'sdfbgrep' : jQuery('#column-box #input-background-repeat').val(), 'sdfbgrat' : jQuery('#column-box #input-parallax-background-ratio').val(), 'sdfbgvid' : jQuery('#column-box #input-youtube-background-video').val(), 'sdfbgvidmute' : jQuery('#column-box #input-mute-background-video').val(), 'sdfboxshadow' : jQuery('#column-box #input-box-shadow').val(), 'sdfboxshadowcolor' : jQuery('#column-box #input-box-shadow-color').val(), 'sdfborderradius' : jQuery('#column-box #input-border-radius').val(), 'sdfborderwidth' : jQuery('#column-box #input-border-width').val(), 'sdfborderstyle' : jQuery('#column-box #input-border-style').val(), 'sdfbordercolor' : jQuery('#column-box #input-border-color').val(), 'sdfclass' : jQuery('#column-box #input-class').val() });
				break;
			}
			
		} else {
			switch(type) {
				<?php
				foreach($sdfElements as $type => $inputs) {
					$att = html_attribute($type);
					echo "case '$att' : \n";
					foreach($inputs as $element => $title) {
						$content = (strpos($element,'content') !== false) ? ' content' : '';
						$el = get_element_type($element);
						$v = html_attribute($title);
						$vis = ".sdf-add-element-box .item:visible";
						switch($el) {
							case 'input' :
								echo "elementContent += '<label>$title:</label><span class=\"sdf-attr$content\" id=\"sdf-element-$v\" class=\"sdf-attr\">' + jQuery('$vis .sdf-input.$v').val() + '</span><br/>'; \n";
								break;
							case 'imageupload' :
								$v = explode(':::', $v); $v = $v[1];
								echo "elementContent += '<label>$title:</label><img id=\"sdf-element-$v\" src=\"' + jQuery('$vis #$v').val() + '\" class=\"sdf-attr\" /><br/>'; \n";
							break;
							case 'select' :
								$v = html_attribute($title['title']);
								echo "elementContent += '<label>".$title['title'].":</label><span class=\"sdf-attr$content\" id=\"sdf-element-$v\">' + jQuery('$vis .sdf-select.$v').val() + '</span><br/>'; \n";
								break;
							case 'selectmulti' :
								$v = html_attribute($title['title']);
								echo "elementContent += '<label>".$title['title'].":</label><span class=\"sdf-attr$content\" id=\"sdf-element-$v\">' + jQuery('$vis .sdf-selectmulti.$v').val() + '</span><br/>'; \n";
								break;
							case 'textarea' :
								echo "elementContent += '<label>$title:</label><span class=\"sdf-attr$content\" id=\"sdf-element-$v\">' + jQuery('$vis .sdf-textarea.$v').val() + '</span><br/>'; \n";
								break;
							case 'wpeditor' :
								$v = explode(':::', $v); $v = $v[1];
								echo "elementContent += '<label>Text block:</label><span $content id=\"sdf-element-$v\" class=\"sdf-attr$content\">'; \n
								if(jQuery('$vis #wp-$v-wrap').hasClass('tmce-active')) {
									elementContent += jQuery.trim(tinymce.get('$v').getContent({format : 'raw'}));
								} else {
									elementContent += jQuery('#$v').val();
								} \n
								elementContent += '</span><br/>'; \n";
								break;
							case 'picker' :
								echo "elementContent += '<label>$title:</label><span class=\"sdf-attr$content\"  id=\"sdf-element-$v\" class=\"sdf-attr\">' + jQuery('$vis .sdf-picker.$v').val() + '</span><br/>'; \n";
								break;		
						}
					}
					echo "break;\n";
				}
				?>
			}
			elementContent = elementContent.replace(/(<\/?)iframe/g, '$1ifrplaceholder');
			if(sdfBuilder.activeElement == 'new') {
				var id = jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' .sdf-element').length;
				var parent = jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol);
				var content = elementContent;
				sdfBuilder.activeElement = 'sdf-element-' + jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' .sdf-element').length;
				jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' .sdf-col-content').append(sdfBuilder.element(id, type, parent, content, '1/1'));
			} else {
				jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' #' + sdfBuilder.activeElement + ' .sdf-element-content .inner').html(elementContent);
			}
			var images = jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' #' + sdfBuilder.activeElement + ' .sdf-element-content .inner img');
			var imagesCount = (elementContent.match(/<img/g) != null) ? elementContent.match(/<img/g).length : 0;
			if(imagesCount > 0) {
				jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' #' + sdfBuilder.activeElement + ' .sdf-element-content .inner').waitForImages(function() {
					var col = jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol);
					var el = jQuery('#' + sdfBuilder.activeRow + ' #' + sdfBuilder.activeCol + ' #' + sdfBuilder.activeElement);
					if (col.offset().top > el.offset().top) {
						col.height(col.height() + el.offset().top - col.offset().top);
					}
					
				});
			} else {
				
			}			
		}
		sdfBuilder.fixIframePlaceholders();
		jQuery('#modal-for-builder-module').modal('hide');
		jQuery('#modal-for-builder-module').on('hidden.bs.modal', function() {
			jQuery('.sdf-add-element-box .item').addClass('hidden');
			jQuery('.sdf-add-element-box .box-title').addClass('invisible');
		});
		sdfBuilder.fixAttributes();
		sdfBuilder.refreshSortables();
		jQuery('#sdf-rows .sdf-row').last().removeAttr('sdftemp');
		// sdfBuilder.updateEditor(true);
	},
	updateEditor : function (mode, publish) {
		sdfBuilder.enumerateRows();
		var singleText = (jQuery('#sdf-rows .sdf-row').not('.sdf-hero').length == 1 && jQuery('#sdf-rows .sdf-row .sdf-col').length == 1 && jQuery('#sdf-rows .sdf-row .sdf-col .sdf-element').length == 1 && jQuery('.sdf-element').attr('sdf-title') == 'text-block');
		if(mode == true) { // builder to editor
			if(singleText == true) {
				var content = jQuery('#sdf-element-sdfaddtextblock').html();
			} else {
				var text; 
				var content = '';
				jQuery('#sdf-rows > *').each(function(i, v) { 
					var rowId = jQuery(v).attr('id');
					if(jQuery(v).hasClass('sdf-row')) { 
						var tagName = (jQuery(v).hasClass('sdf-hero')) ? 'hero' : 'row';
						var sdfheroheight = (jQuery(v).attr('sdfheroheight')) ? ' hero_height="' + jQuery(v).attr('sdfheroheight') + '"' : '';
						var sdfpaddtop = (jQuery(v).attr('sdfpaddtop')) ? ' padding_top="' + jQuery(v).attr('sdfpaddtop') + '"' : '';
						var sdfpaddbottom = (jQuery(v).attr('sdfpaddbottom')) ? ' padding_bottom="' + jQuery(v).attr('sdfpaddbottom') + '"' : ''; 
						var sdffullwidthhero = (jQuery(v).attr('sdffullwidthhero')) ? ' fullwidth_hero="' + jQuery(v).attr('sdffullwidthhero') + '"' : '';
						var sdffullheighthero = (jQuery(v).attr('sdffullheighthero')) ? ' full_height_hero="' + jQuery(v).attr('sdffullheighthero') + '"' : '';
						var sdfbgtype = (jQuery(v).attr('sdfbgtype')) ? ' background_type="' + jQuery(v).attr('sdfbgtype') + '"' : '';
						var sdfbg = (jQuery(v).attr('sdfbg')) ? ' background_color="' + jQuery(v).attr('sdfbg') + '"' : '';
						var sdfbgalpha = (jQuery(v).attr('sdfbgalpha')) ? ' background_color_alpha="' + jQuery(v).attr('sdfbgalpha') + '"' : '';
						var sdfbgimg = (jQuery(v).attr('sdfbgimg')) ? ' background_image="' + jQuery(v).attr('sdfbgimg') + '"' : '';
						var sdfbgrep = (jQuery(v).attr('sdfbgrep')) ? ' background_repeat="' + jQuery(v).attr('sdfbgrep') + '"' : '';
						var sdfbgrat = (jQuery(v).attr('sdfbgrat')) ? ' parallax_background_ratio="' + jQuery(v).attr('sdfbgrat') + '"' : '';
						var sdfbgvid = (jQuery(v).attr('sdfbgvid')) ? ' youtube_background_video="' + jQuery(v).attr('sdfbgvid') + '"' : '';
						var sdfbgvidmute = (jQuery(v).attr('sdfbgvidmute')) ? ' mute_background_video="' + jQuery(v).attr('sdfbgvidmute') + '"' : '';
						var sdfmargins = (jQuery(v).attr('sdfmargins')) ? ' margins="' + jQuery(v).attr('sdfmargins') + '"' : '';
						var sdfboxshadowcolor = (jQuery(v).attr('sdfboxshadowcolor')) ? ' box_shadow_color="' + jQuery(v).attr('sdfboxshadowcolor') + '"' : '';
						var sdfboxshadow = (jQuery(v).attr('sdfboxshadow')) ? ' box_shadow="' + jQuery(v).attr('sdfboxshadow') + '"' : '';
						var sdfbordercolor = (jQuery(v).attr('sdfbordercolor')) ? ' border_color="' + jQuery(v).attr('sdfbordercolor') + '"' : '';
						var sdfborderstyle = (jQuery(v).attr('sdfborderstyle')) ? ' border_style="' + jQuery(v).attr('sdfborderstyle') + '"' : '';
						var sdfborderwidth = (jQuery(v).attr('sdfborderwidth')) ? ' border_width="' + jQuery(v).attr('sdfborderwidth') + '"' : '';
						var sdfclass = (jQuery(v).attr('sdfclass')) ? ' class="' + jQuery(v).attr('sdfclass') + '"' : '';
						var sdflast = (jQuery(v).attr('sdflast')) ? ' last="yes"' : '';
						content += '[sdf_' + tagName + ' id="' + rowId + '"' + sdfheroheight + sdfpaddtop + sdfpaddbottom + sdffullwidthhero + sdffullheighthero + sdfbgtype + sdfbg + sdfbgalpha + sdfbgimg + sdfbgrep + sdfbgrat + sdfbgvid+ sdfbgvidmute + sdfmargins + sdfboxshadow + sdfboxshadowcolor + sdfborderwidth + sdfborderstyle + sdfbordercolor + sdfclass + sdflast + ']';
						jQuery('#' + rowId + ' .sdf-col').each(function(ii, vv) {
							var colId = jQuery(vv).attr('id');
						var sdfcolumnheight = (jQuery(vv).attr('sdfcolumnheight')) ? ' column_height="' + jQuery(vv).attr('sdfcolumnheight') + '"' : '';
						var sdfpaddtop = (jQuery(vv).attr('sdfpaddtop')) ? ' padding_top="' + jQuery(vv).attr('sdfpaddtop') + '"' : '';
						var sdfpaddbottom = (jQuery(vv).attr('sdfpaddbottom')) ? ' padding_bottom="' + jQuery(vv).attr('sdfpaddbottom') + '"' : ''; 
						var sdffullwidthhero = (jQuery(vv).attr('sdffullwidthhero')) ? ' fullwidth_hero="' + jQuery(vv).attr('sdffullwidthhero') + '"' : '';
						var sdffullheighthero = (jQuery(vv).attr('sdffullheighthero')) ? ' full_height_hero="' + jQuery(vv).attr('sdffullheighthero') + '"' : '';
						var sdfbgtype = (jQuery(vv).attr('sdfbgtype')) ? ' background_type="' + jQuery(vv).attr('sdfbgtype') + '"' : '';
						var sdfbg = (jQuery(vv).attr('sdfbg')) ? ' background_color="' + jQuery(vv).attr('sdfbg') + '"' : '';
						var sdfbgalpha = (jQuery(vv).attr('sdfbgalpha')) ? ' background_color_alpha="' + jQuery(vv).attr('sdfbgalpha') + '"' : '';
						var sdfbgimg = (jQuery(vv).attr('sdfbgimg')) ? ' background_image="' + jQuery(vv).attr('sdfbgimg') + '"' : '';
						var sdfbgrep = (jQuery(vv).attr('sdfbgrep')) ? ' background_repeat="' + jQuery(vv).attr('sdfbgrep') + '"' : '';
						var sdfbgrat = (jQuery(vv).attr('sdfbgrat')) ? ' parallax_background_ratio="' + jQuery(vv).attr('sdfbgrat') + '"' : '';
						var sdfbgvid = (jQuery(vv).attr('sdfbgvid')) ? ' youtube_background_video="' + jQuery(vv).attr('sdfbgvid') + '"' : '';
						var sdfbgvidmute = (jQuery(vv).attr('sdfbgvidmute')) ? ' mute_background_video="' + jQuery(vv).attr('sdfbgvidmute') + '"' : '';
						var sdfboxshadowcolor = (jQuery(vv).attr('sdfboxshadowcolor')) ? ' box_shadow_color="' + jQuery(vv).attr('sdfboxshadowcolor') + '"' : '';
						var sdfboxshadow = (jQuery(vv).attr('sdfboxshadow')) ? ' box_shadow="' + jQuery(vv).attr('sdfboxshadow') + '"' : '';
						var sdfbordercolor = (jQuery(vv).attr('sdfbordercolor')) ? ' border_color="' + jQuery(vv).attr('sdfbordercolor') + '"' : '';
						var sdfborderstyle = (jQuery(vv).attr('sdfborderstyle')) ? ' border_style="' + jQuery(vv).attr('sdfborderstyle') + '"' : '';
						var sdfborderradius = (jQuery(vv).attr('sdfborderradius')) ? ' border_radius="' + jQuery(vv).attr('sdfborderradius') + '"' : '';
						var sdfborderwidth = (jQuery(vv).attr('sdfborderwidth')) ? ' border_width="' + jQuery(vv).attr('sdfborderwidth') + '"' : '';
						var sdfclass = (jQuery(vv).attr('sdfclass')) ? ' class="' + jQuery(vv).attr('sdfclass') + '"' : '';
							content += '[sdf_col id="' + colId + '" width="' + jQuery(vv).attr('sdf-width') + '"' + sdfheroheight + sdfpaddtop + sdfpaddbottom + sdfbgtype + sdfbg + sdfbgalpha + sdfbgimg + sdfbgrep + sdfbgrat + sdfbgvid+ sdfbgvidmute + sdfboxshadow + sdfboxshadowcolor + sdfborderradius + sdfborderwidth + sdfborderstyle + sdfbordercolor + sdfclass + ']';
							jQuery('#' + rowId + ' #' + colId + ' .sdf-element').each(function(iii, vvv) {
								var elementId = jQuery(vvv).attr('id');
								var tagName = 'sdf_' + jQuery(vvv).attr('sdf-title').replace(/-/g, '_');
								var moduleWidth = jQuery(vvv).attr('module_width');
								content += '[' + tagName + ' id="' + elementId + '" module_width="' + moduleWidth + '"';
									text = '';
									jQuery('#' + rowId + ' #' + colId + ' #' + elementId + ' .inner > .sdf-attr').each(function(iiii,vvvv) {
										if(jQuery(vvvv).hasClass('content')) {
											text = jQuery(vvvv).html().replace(/(<\/?)ifrplaceholder/g, '$1iframe');
										} else {
											if(jQuery(vvvv).attr('id') != 'sdf-element-module-width') {
												content += ' ' + jQuery(vvvv).attr('id').replace('sdf-element-', '').replace(/-/g, '_') + '="' + (jQuery(vvvv).attr('src') || jQuery(vvvv).attr('sdf-val') || jQuery(vvvv).html()) + '"'; 
											}
										}
									});
								content += ']' + text + '[/' + tagName + ']';
							});
							content += '[/sdf_col]';
						});
						content += '[/sdf_' + tagName + ']';
					}
				});
			}
			jQuery('#content.wp-editor-area').val(content);
			if(jQuery('#wp-content-wrap').hasClass('tmce-active')) {
				tinymce.get('content').setContent(content, {format : 'raw'});
			}
		} else { // editor to builder
			var content = (jQuery.trim((jQuery('#wp-content-wrap').hasClass('tmce-active')) ? tinymce.get('content').getContent({format : 'raw'}) : jQuery('#content.wp-editor-area').val())) || '';
			var shortcode = '(sdf_row|sdf_col<?php $elReg = ''; foreach($sdfElements as $k => $v) { $elReg .= "|sdf_" . html_attribute($k); } echo str_replace("-", "_", $elReg); ?>)';
			var regTags1 = new RegExp("\\[(/?" + shortcode + "([\\s][^\\]]*)?)\\]", "g");
			var regTags2 = new RegExp("\\<(/?" + shortcode + "([\\s][^\\>]*)?)\\>", "g");
			var regStrip1 = new RegExp("<p>(<\/?" + shortcode + "[^>]*>*)", "g");
			var regStrip2 = new RegExp("(<\/?" + shortcode + "[^>]*>*)<\/p>", "g");
			var regStrip3 = new RegExp("<p>([^<\/p>]*<\/" + shortcode + ">)", "g");
			var regStrip4 = new RegExp("(<" + shortcode + "[^>]*>[^<p>]*)<\/p>", "g");
			content = content.replace(regTags1, '<$1>');		
			content = content.replace(regStrip1, '$1');
			content = content.replace(regStrip2, '$1');
			content = content.replace(regStrip3, '$1');
			content = content.replace(regStrip4, '$1');
			content = content.replace(/(<\/?)iframe/g, '$1ifrplaceholder');
			content = content.replace(/<div[^>]+data-wpview-text=\"([^"]+)"[^>]*>[\s\S]+?wpview-selection-after[^>]+>(?:&nbsp;|\u00a0)*<\/p><\/div>/g, function (match, p1, p2, p3, offset, string) { return decodeURIComponent(p1); });
			jQuery('.sdf-shortcodes').html(content);
			if(jQuery('.sdf-shortcodes > p').length > 0) {
				jQuery('.sdf-shortcodes').html(jQuery('.sdf-shortcodes > p').html());
			}
			jQuery('#sdf-rows').html('');
			if(jQuery('.sdf-shortcodes > sdf_row, .sdf-shortcodes > sdf_hero').length == 0) {
				if(content.replace(/(<([^>]+)>)/ig,'').length > 0) { 
					sdfBuilder.addRow('fromeditor');
					var elementContent = '<label>Text block:</label><span class="sdf-attr content" id="sdf-element-sdfaddtextblock">' + content + '</span><br><label>Block:</label><span id="sdf-element-block" sdf-val="yes">Yes</span><br><label>Animation:</label><span id="sdf-element-animation" sdf-val="no">No</span><br><label>Class:</label><span id="sdf-element-class"></span><br>';
					jQuery('#sdf-row-1 #sdf-col-1 .sdf-col-content').append(sdfBuilder.element(0, 'text-block', jQuery('#sdf-row-1 #sdf-col-1'), elementContent, '1/1'));
				}
			} else {
				jQuery('.sdf-shortcodes > sdf_row, .sdf-shortcodes > sdf_hero').each(function(i,v){
					var attributes = {};
					if(jQuery(v).prop('tagName').toLowerCase() == 'sdf_hero') { attributes.sdfhero = 'hero'; }
					if(jQuery(v).attr('hero_height')) { attributes.sdfheroheight = jQuery(v).attr('hero_height'); }
					if(jQuery(v).attr('padding_top')) { attributes.sdfpaddtop = jQuery(v).attr('padding_top'); }
					if(jQuery(v).attr('padding_bottom')) { attributes.sdfpaddbottom = jQuery(v).attr('padding_bottom'); }
					if(jQuery(v).attr('fullwidth_hero')) { attributes.sdffullwidthhero = jQuery(v).attr('fullwidth_hero'); }
					if(jQuery(v).attr('full_height_hero')) { attributes.sdffullheighthero = jQuery(v).attr('full_height_hero'); }
					if(jQuery(v).attr('background_type')) { attributes.sdfbgtype = jQuery(v).attr('background_type'); }
					if(jQuery(v).attr('background_color')) { attributes.sdfbg = jQuery(v).attr('background_color'); }
					if(jQuery(v).attr('background_color_alpha')) { attributes.sdfbgalpha = jQuery(v).attr('background_color_alpha'); }
					if(jQuery(v).attr('background_image')) { attributes.sdfbgimg = jQuery(v).attr('background_image'); }
					if(jQuery(v).attr('background_repeat')) { attributes.sdfbgrep = jQuery(v).attr('background_repeat'); }
					if(jQuery(v).attr('parallax_background_ratio')) { attributes.sdfbgrat = jQuery(v).attr('parallax_background_ratio'); }
					if(jQuery(v).attr('youtube_background_video')) { attributes.sdfbgvid = jQuery(v).attr('youtube_background_video'); }
					if(jQuery(v).attr('mute_background_video')) { attributes.sdfbgvidmute = jQuery(v).attr('mute_background_video'); }
					if(jQuery(v).attr('margins')) { attributes.sdfmargins = jQuery(v).attr('margins'); }
					if(jQuery(v).attr('box_shadow_color')) { attributes.sdfboxshadowcolor = jQuery(v).attr('box_shadow_color'); }
					if(jQuery(v).attr('box_shadow')) { attributes.sdfboxshadow = jQuery(v).attr('box_shadow'); }
					if(jQuery(v).attr('border_color')) { attributes.sdfbordercolor = jQuery(v).attr('border_color'); }
					if(jQuery(v).attr('border_style')) { attributes.sdfborderstyle = jQuery(v).attr('border_style'); }
					if(jQuery(v).attr('border_width')) { attributes.sdfborderwidth = jQuery(v).attr('border_width'); }
					if(jQuery(v).attr('class')) { attributes.sdfclass = jQuery(v).attr('class'); }
					if(jQuery(v).attr('last')) { attributes.sdflast = jQuery(v).attr('last'); }
					attributes.fromEditor = true;
					var rowId = sdfBuilder.addRow(false, attributes);
					var columns = jQuery('.sdf-shortcodes #' + rowId + ' sdf_col');
					var fractions = [];
					var rowContent = '#sdf-rows #' + rowId + ' .sdf-row-content';
					columns.each(function(ii,vv){
						var attributes = {};
						if(jQuery(vv).attr('column_height')) { attributes.sdfcolumnheight = jQuery(vv).attr('column_height'); }
						if(jQuery(vv).attr('padding_top')) { attributes.sdfpaddtop = jQuery(vv).attr('padding_top'); }
						if(jQuery(vv).attr('padding_bottom')) { attributes.sdfpaddbottom = jQuery(vv).attr('padding_bottom'); }
						if(jQuery(vv).attr('fullwidth_hero')) { attributes.sdffullwidthhero = jQuery(vv).attr('fullwidth_hero'); }
						if(jQuery(vv).attr('full_height_hero')) { attributes.sdffullheighthero = jQuery(vv).attr('full_height_hero'); }
						if(jQuery(vv).attr('background_type')) { attributes.sdfbgtype = jQuery(vv).attr('background_type'); }
						if(jQuery(vv).attr('background_color')) { attributes.sdfbg = jQuery(vv).attr('background_color'); }
						if(jQuery(vv).attr('background_color_alpha')) { attributes.sdfbgalpha = jQuery(vv).attr('background_color_alpha'); }
						if(jQuery(vv).attr('background_image')) { attributes.sdfbgimg = jQuery(vv).attr('background_image'); }
						if(jQuery(vv).attr('background_repeat')) { attributes.sdfbgrep = jQuery(vv).attr('background_repeat'); }
						if(jQuery(vv).attr('parallax_background_ratio')) { attributes.sdfbgrat = jQuery(vv).attr('parallax_background_ratio'); }
						if(jQuery(vv).attr('youtube_background_video')) { attributes.sdfbgvid = jQuery(vv).attr('youtube_background_video'); }
						if(jQuery(vv).attr('mute_background_video')) { attributes.sdfbgvidmute = jQuery(vv).attr('mute_background_video'); }
						if(jQuery(vv).attr('margins')) { attributes.sdfmargins = jQuery(vv).attr('margins'); }
						if(jQuery(vv).attr('box_shadow_color')) { attributes.sdfboxshadowcolor = jQuery(vv).attr('box_shadow_color'); }
						if(jQuery(vv).attr('box_shadow')) { attributes.sdfboxshadow = jQuery(vv).attr('box_shadow'); }
						if(jQuery(vv).attr('border_color')) { attributes.sdfbordercolor = jQuery(vv).attr('border_color'); }
						if(jQuery(vv).attr('border_style')) { attributes.sdfborderstyle = jQuery(vv).attr('border_style'); }
						if(jQuery(vv).attr('border_radius')) { attributes.sdfborderradius = jQuery(vv).attr('border_radius'); }
						if(jQuery(vv).attr('border_width')) { attributes.sdfborderwidth = jQuery(vv).attr('border_width'); }
						if(jQuery(vv).attr('class')) { attributes.sdfclass = jQuery(vv).attr('class'); }
						var fraction = jQuery(vv).attr('width');
						var colId = jQuery(vv).attr('id');
						var colWidth = sdfBuilder.colWidth(fraction,columns.length);
						jQuery(rowContent).append(sdfBuilder.column(fraction,colId,colWidth));
						jQuery(rowContent + ' #' + colId).attr(attributes);
						fractions.push(fraction);
						jQuery('.sdf-shortcodes #' + rowId + ' #' + colId + ' > *').each(function(iii,vvv) {
							var attributes = sdfBuilder.getAttributes(jQuery(vvv));
							var id = iii;
							var type = jQuery(vvv).prop('tagName').toLowerCase().replace('sdf_', '').replace(/_/g, '-');
							var parent = jQuery('#sdf-rows #' + rowId + ' #' + colId);
							var moduleWidth = jQuery(vvv).attr('module_width') || '1/1';
							var contentAttr = sdfBuilder.contentAttribute(type);
							var label = (contentAttr.indexOf('sdfadd') == -1) ? contentAttr : 'Text Block';
							var content = (contentAttr != '') ? ('<label>' + label + ':</label><span class="content sdf-attr" id="sdf-element-' + contentAttr + '">' + jQuery.trim(jQuery(vvv).html().replace(regTags2, '[$1]')) + '</span><br/>') : '';
							jQuery.each(attributes, function(iiii,vvvv) {
								if(iiii != 'id' && iiii != 'type') {
									var span, sdfVal, select = '.sdf-add-element-box #' + type + '-box select#input-' + iiii;
									if(jQuery(select).length > 0) { 
										span = jQuery(select + ' option[value="' + vvvv + '"]').html();
										sdfVal = ' sdf-val="' + vvvv + '"';
									} else {
										span = vvvv;
										sdfVal = '';
									}
									var sdfAttr = (iiii.substr(iiii.length - 5) == 'image') ? '<img id="sdf-element-' + iiii.replace(/_/g, '-') + '" src="' + span + '" class="sdf-attr img-responsive">' : '<span id="sdf-element-' + iiii.replace(/_/g, '-') + '"' + sdfVal + ' class="sdf-attr">' + span + '</span>';
									content += '<label>' + iiii.replace(/_/g, ' ') + ':</label>' + sdfAttr + '<br/>';
								}
							});
							jQuery('#sdf-rows #' + rowId + ' #' + colId + ' .sdf-col-content').append(sdfBuilder.element(id, type, parent, content, moduleWidth));
						});
					});
					jQuery('#sdf-rows #' + rowId).attr('sdf-cols', fractions.join(' '));
				});
				sdfBuilder.fixAttributes();
				jQuery('#sdf-rows .sdf-row').each(function(i,v) {
					
				});
			}
			if(localStorage.getItem('sdfclipboard') === null) {
				jQuery('.sdf-paste').attr('disabled', 'disabled');
			} else {
				jQuery('.sdf-paste').removeAttr('disabled');
			}
		}
		if(publish == true) {
			jQuery('#post').submit();
			}
		sdfBuilder.fixIframePlaceholders();
	},
	colWidth : function (fraction,columnsCount) {
		if(fraction=='4/6') { fraction = '2/3'; }
		var width = sdfBuilder.widths[fraction];
		return width;
	},
	addTemplate : function (v) {
		jQuery('.sdf-templates-list').append('<li class="sdf-templates-list-item"><span class="sdf-label">' + v + '</span><span class="sdf-icon sdf-delete" title="Delete Template"></span></li>'); 
	},
	refreshSortables : function () {
		 jQuery('#sdf-wrap #sdf-rows').sortable({
			items : '.sdf-row',
			handle: ".sdf-row-sorter",
			axis: "y"
			 // update: function( ev, ui ) { 
				 // sdfBuilder.updateEditor(true);
			 // },
		 });
		jQuery('#sdf-wrap .sdf-row-content').sortable({
			items : '.sdf-col',
			axis: "x",
			placeholder : 'sdf-column-placeholder',
			start: function( ev, ui ) { 
				ui.item.parent().parent().css('z-index', 2);
			},
			update: function( ev, ui ) { 
				jQuery('.sdf-col').css('z-index', 1);
				sdfBuilder.setActiveComponents(ui.item);
				
				sdfBuilder.fixAttributes();
				// sdfBuilder.updateEditor(true);
			}
		});
		jQuery('#sdf-wrap .sdf-col').sortable({ 
			items : '.sdf-element',
			connectWith : '.sdf-col',
			placeholder : 'sdf-element-placeholder',
			appendTo: document.body,
			start: function( ev, ui ) { 
				sdfBuilder.sortableSender = ui.item.parents('.sdf-row')[0].id;
				ui.item.parent().parent().css('z-index', 2);
			},
			over: function( ev, ui ) {
				
				
			},
			out: function( ev, ui ) {
				
				
			},
			receive: function( ev, ui ) {
				if(jQuery(ev.target).hasClass('selected')) {
					var index = ui.item.index();
					if(jQuery(ui.item).is(':last-child')) {
						jQuery(ui.item).appendTo(jQuery(ev.target).find('.sdf-col-content'));
					}
					if(index == 0) {
						jQuery(ui.item).prependTo(jQuery(ev.target).find('.sdf-col-content'));
					} else {
						jQuery(ui.item).insertAfter(jQuery(ev.target).find('.sdf-col-content .sdf-element:nth-child(' + index + ')'));
					}
				}
			},
			update: function( ev, ui ) {
				sdfBuilder.showLoader();
				jQuery(ev.toElement).parents('.sdf-col-content').children('.sdf-element').each(function(i,v) {
					jQuery(v).attr('id', 'sdf-element-' + i);
				});
				jQuery(ev.target).children('.sdf-element').each(function(i,v) {
					jQuery(v).attr('id', 'sdf-element-' + i);
				});
				jQuery('.sdf-col').css('z-index', 1);
				sdfBuilder.setActiveComponents(ui.item);
				sdfBuilder.fixAttributes();
				// sdfBuilder.updateEditor(true);
				sdfBuilder.hideLoader();
			}
		}).disableSelection();
	},
	escapeRegExp : function (str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	},
	replaceAll : function (find, repl, str) {
		return str.replace(new RegExp(sdfBuilder.escapeRegExp(find), 'g'), repl);
	},
	getAttributes : function (e) {
		var attributes = {}; 
		if( e.length ) {
			jQuery.each( e[0].attributes, function( index, attr ) {
				attributes[ attr.name ] = attr.value;
			}); 
		}
		return attributes;
	},
	showLoader : function () {
		jQuery('.sdf-loader-shade').show().css({ left : -jQuery('#wpcontent').offset().left, width: jQuery(document).width(), height: jQuery(document).height(), 'background-position' : '50% ' + (jQuery(window).scrollTop() + Math.round(jQuery(window).height() / 2) - 50) + 'px' });;
	},
	hideLoader : function () {
		jQuery('.sdf-loader-shade').hide();
	},
	fixIframePlaceholders : function () {
		jQuery('#sdf-rows ifrplaceholder').each(function(i,v) {
			jQuery(v).height(jQuery(v).attr('height'));
		});
	}
};
jQuery( document ).ready( function() {
	//jQuery('#publish').after('<button type="button" id="update-and-publish" class="button button-primary button-large">Update</button>');
	jQuery( "body" ).on( "click touchstart", "input#publish, input#save-post", function(){
		if(jQuery('.sdf-builder-switch button').hasClass('active')) {
			sdfBuilder.updateEditor(true, true);
		} else {
			jQuery('#post').submit();
		}
	});
	if (jQuery('#content').length > 0) {
		var imgReg = /image="([^"]*)"/g;
		var content = jQuery('#content').val().match(imgReg);
		if (content) {
		jQuery.each(content, function(i,v) {
			jQuery('.sdf-image-load').append('<img src="' + v.replace(imgReg, '$1') + '">')
		})
		}
	}
	if(jQuery('.sdf-add-element-box .wp-editor-wrap').hasClass('tmce-active')) {
		tinyMCE.init({
			cleanup_on_startup: false,
			trim_span_elements: false,
			verify_html: false,
			cleanup: false,
			convert_urls: false
		});
	}
});

// initial templates list
var sdfTemplatesString = localStorage.getItem('sdfTemplates');
var sdfTemplates = (sdfTemplatesString) ? JSON.parse(sdfTemplatesString) : {};
var sdfTemplatesKeys = Object.keys(sdfTemplates);
if(sdfTemplatesKeys.length > 0) {
	jQuery.each(sdfTemplatesKeys, function(i,v) {
		sdfBuilder.addTemplate(v);
	});
	jQuery('.sdf-template-load').removeAttr('disabled');
}
// toggle builder
jQuery('.sdf-builder-switch button').click(function() {
	var mode = jQuery(this).hasClass('active'),
	editorType = (mode) ? 'SDF Builder' : 'Text Editor';
	jQuery(this).toggleClass('active').text(editorType);
	jQuery('.sdf-builder-switch').toggleClass('sdf-builder-icon');
	jQuery('.sdf-builder-box, #postdivrich').toggle();
	sdfBuilder.updateEditor(mode);
});
// toggle builder by default
jQuery('#sdf-builder-default-switch').click(function() {
	if(localStorage.getItem('sdfbydefault') === null) {
		localStorage.setItem('sdfbydefault', true);
	} else {
		localStorage.setItem('sdfbydefault', jQuery('#sdf-builder-default-switch').prop("checked"));
	}
});
jQuery(window).load(function() {
	if(localStorage.getItem('sdfbydefault') == 'true'){
		jQuery('.sdf-builder-switch button').triggerHandler('click');
		jQuery('#sdf-builder-default-switch').prop('checked', 'true');
	}	
});

// add row
jQuery('.sdf-add-row-button').click(function() {
	sdfBuilder.addRow(true);
});
// add hero
jQuery('.sdf-add-hero-button').click(function() {
	sdfBuilder.addRow(true, {sdfhero : 'hero'});
});
// paste row from sdf clipboard
jQuery('.sdf-paste').click(function() {
	jQuery('#sdf-rows').append(localStorage.getItem('sdfclipboard'));
	var newId = 'sdf-row-' + jQuery('#sdf-rows .sdf-row').length;
	jQuery('#sdf-rows .sdf-row').last().attr('id', newId);
	sdfBuilder.scrollToRow(newId);
	// sdfBuilder.updateEditor(true);
});
// add / edit extra class for row / column
jQuery('#sdf-wrap .sdf-row-edit, #sdf-wrap .sdf-col-edit').off();
jQuery('#sdf-wrap').on('click', '.sdf-row-edit, .sdf-col-edit', function(e) {
	var nodeType = (jQuery(this).parents('.sdf-col').length > 0) ? 'col' : 'row';
	sdfBuilder.extraClassNode = jQuery(this).parents('.sdf-' + nodeType);
	sdfBuilder.activeRow = jQuery(e.target).parents('.sdf-row').attr('id');
	sdfBuilder.addMiniColors();
	if(nodeType == 'col') { 
		sdfBuilder.activeCol = jQuery(e.target).parents('.sdf-col').attr('id');
		sdfBuilder.editColumn(e); 
	} 
	if(nodeType == 'row') { 
		if (jQuery(e.target).parents('.sdf-row').attr('sdfhero')=='hero') {
			sdfBuilder.editHero(e); 
		} else {
			sdfBuilder.editRow(e); 
		}
	}
});
// clone row
jQuery('#sdf-wrap').on('click', '.sdf-row-clone', function(e) {
	var clone = jQuery(e.target).parents('.sdf-row').clone(true, true).appendTo(jQuery(e.target).parents('#sdf-rows'));
	var count = jQuery('#sdf-rows .sdf-row').length;
	sdfBuilder.enumerateRows();
			
	sdfBuilder.fixAttributes();
	sdfBuilder.refreshSortables();
	// sdfBuilder.updateEditor(true);
	sdfBuilder.scrollToRow('sdf-row-' + count);
});
// delete row
jQuery('#sdf-wrap').on('click', '.sdf-row-delete', function(e) {
	if(confirm('Delete this row?')==true) {
		jQuery(this).parents('.sdf-row').fadeOut(200, function() { 
			jQuery(this).remove(); 
			if(jQuery('#sdf-rows .sdf-row').length == 0) {
				jQuery('.sdf-template-save').attr('disabled','disabled');
			}
			// sdfBuilder.updateEditor(true);
		});
	}
});

// insert columns
sdfBuilder.rowLayoutEvent();

jQuery('.sdf-enter').keyup(function(e) {
	if(e.which == 13) {
		jQuery(e.target).next('.button').triggerHandler('click');
	}
});
// custom layout
jQuery('.sdf-custom-layout-box .button').click(function(e) {
	var error = false;
	var input = jQuery(this).prev('input');
	var text = input.val().replace(/^\s+|\s+$|[^\d|/| ]/g, '');
	input.val(text);
	var fractions = text.split(' ');
	jQuery.each(fractions, function (i, v) {
		var fraction = v.split('/');
		if(fraction.length != 2) {
			error = true;
		}
	});
	if(error == true) { 
		jQuery('.sdf-custom-layout-box .error').fadeIn();
		input.addClass('error-format').focus();
	}
	else {
		var row = jQuery('#sdf-rows #' + jQuery('.sdf-custom-layout-box').attr('title'));
		jQuery('.sdf-custom-layout-box').dialog('close');
		sdfBuilder.rowLayout(fractions, row);
		sdfBuilder.resetCustomLayout();
	}
});
// delete column
jQuery('#sdf-wrap .sdf-col-delete').off();
jQuery('#sdf-wrap').on('click', '.sdf-col-delete', function(e) {
	if(confirm('Delete this column?')==true) {
		var title = jQuery(this).parents('.sdf-col').attr('sdf-width');
		var cols = jQuery(this).parents('.sdf-col').siblings('.sdf-col');
		var fractions = new Array();
		jQuery.each(cols, function (ii, vv) { 
			fractions.push(jQuery(vv).attr('sdf-width'));
		});
		jQuery(this).parents('.sdf-row').attr('sdf-cols', fractions.join(' '));
		jQuery(this).parents('.sdf-col').fadeOut(200, function() { 
			jQuery(this).remove(); 
			// sdfBuilder.updateEditor(true);
		});
	}
});
// add element (button within column)
jQuery('#sdf-wrap .sdf-add-element').off();
jQuery('#sdf-wrap').on('click', '.sdf-add-element', function(e) {
	sdfBuilder.elementType = '';
	sdfBuilder.addElement(e.target, false);
});
// add element (main nav button)
jQuery('.sdf-add-element-button').click(function() {
	sdfBuilder.activeElement = 'new';
	if(jQuery('#sdf-rows .sdf-row').length == 0) {
		sdfBuilder.addRow(true, {sdftemp : 'true'});
	}
	jQuery('.sdf-add-element').trigger('click');
});
// select element type
jQuery('.sdf-add-element-selection .button').click(function() { 
	sdfBuilder.selectElementType(jQuery(this));
});
// edit element
jQuery('#sdf-wrap .sdf-element-edit').off();
jQuery('#sdf-wrap').on('click', '.sdf-element-edit', function(e) {
	sdfBuilder.setActiveComponents(e.target);
	sdfBuilder.activeElement = jQuery(e.target).parents('.sdf-element').attr('id');
	sdfBuilder.elementType = jQuery(e.target).parents('.sdf-element').attr('sdf-title');
	sdfBuilder.addElement(e.target, true);
});
// back to selection
jQuery('.sdf-add-element-box .back').click(function() {
	jQuery('.sdf-add-element-box .buttons-set, .sdf-add-element-box .item').addClass('hidden');
	jQuery('.sdf-add-element-box .box-title').addClass('invisible');
	jQuery('.sdf-add-element-selection').removeClass('hidden');
	jQuery('.sdf-add-element-box .box-title').html('Select Module').removeClass('invisible');
});
// update element
jQuery('.sdf-add-element-box .button.update').click(function() {
	var type = jQuery('.sdf-add-element-box .item:visible').attr('id').replace('-box', '');
	sdfBuilder.updateElement(type);
});
jQuery('.sdf-add-element-box .sdf-dialog-close').click(function(e) {
	sdfBuilder.elementType = '';
	jQuery('.sdf-row[sdftemp="true"]').remove();
	jQuery('#modal-for-builder-module').modal('hide');
	jQuery('#modal-for-builder-module').on('hidden.bs.modal', function() {
		jQuery('.sdf-add-element-box .item').addClass('hidden');
		jQuery('.sdf-add-element-box .box-title').addClass('invisible');
	});
});
jQuery(document).keyup(function(e) {
	if(e.which == 27) {
		jQuery('.sdf-add-element-box .sdf-dialog-close').triggerHandler('click');
	}
});
jQuery('.sdf-add-element-box .sdf-image').keydown(function(e){
	if(e.which==13) {
		e.preventDefault(); 
		jQuery('.sdf-add-element-box .sdf-button.update').triggerHandler('click');
	} 
});
// delete element
jQuery('#sdf-wrap .sdf-element-delete').off();
jQuery('#sdf-wrap').on('click', '.sdf-element-delete', function(e) {
	if(confirm('Delete this module?')==true) {
		var par = jQuery(e.target).parents('.sdf-element');
		var sibl = par.siblings('.sdf-element');
		par.fadeOut(200, function() {
					
			par.remove(); 
			sdfBuilder.fixAttributes();
			sibl.each(function(i,v) {
				jQuery(v).attr('id', 'sdf-element-' + i);
			});
			// sdfBuilder.updateEditor(true);
		});
	}
});
// clone element
jQuery('#sdf-wrap .sdf-element .sdf-clone').off();
jQuery('#sdf-wrap').on('click', '.sdf-element .sdf-clone', function(e) {
	var clone = jQuery(e.target).parents('.sdf-element').clone(true, true).appendTo(jQuery(e.target).parents('.sdf-col-content'));
	var count = jQuery(e.target).parents('.sdf-element').siblings().length;
	clone.attr('id', 'sdf-element-' + count);
			
	sdfBuilder.fixAttributes();
	sdfBuilder.refreshSortables();
	// sdfBuilder.updateEditor(true);
});
// copy row to sdf clipboard
jQuery('#sdf-wrap').on('click', '.sdf-row .sdf-clipboard', function(e) {
	var clip = jQuery(e.target).parents('.sdf-row')[0].outerHTML;
	localStorage.setItem('sdfclipboard', clip);
	jQuery('.sdf-paste').removeAttr('disabled');
});
// save template
jQuery('.sdf-template-save').click(function() {
	jQuery('.sdf-template-name input').val('');
	jQuery('.sdf-template-name').dialog({
		dialogClass: 'sdf-dialog sdf-template-name-dialog',
		width: 270,
		height: 100,
		modal: true,
		title: 'Template Name:',
		closeText: '',
		closeOnEscape: true
	});
});
jQuery('.sdf-template-name .button').click(function() {
	var templateName = jQuery(this).prev('input').val();
	sdfTemplates[templateName] = jQuery('#sdf-rows').html();
	localStorage.setItem('sdfTemplates', JSON.stringify(sdfTemplates));
	sdfBuilder.addTemplate(templateName); 
	jQuery('.sdf-template-name').dialog('close');
	jQuery('.sdf-template-load').removeAttr('disabled');
});
// load template
jQuery('#sdf-wrap .sdf-templates-list-item .sdf-label').off();
jQuery('#sdf-wrap').on('click', '.sdf-templates-list-item .sdf-label', function(e) {
	var templateName = jQuery(e.target).html();
	jQuery('#sdf-rows').append(JSON.parse(localStorage.getItem('sdfTemplates'))[templateName]);
	sdfBuilder.enumerateRows();
	sdfBuilder.rowLayoutEvent();
	sdfBuilder.refreshSortables();
});
// delete template
jQuery('#sdf-wrap .sdf-templates-list-item .sdf-delete').off();
jQuery('#sdf-wrap').on('click', '.sdf-templates-list-item .sdf-delete', function(e) {
	var templateName = jQuery(e.target).prev('.sdf-label').html();
	if(confirm('Delete template ' + templateName + '?')==true) {
		var sdfTemplates = JSON.parse(localStorage.getItem('sdfTemplates'));
		delete sdfTemplates[templateName];
		if(Object.keys(sdfTemplates).length == 0) { 
			jQuery('.sdf-template-load').attr('disabled','disabled');
		}
		localStorage.setItem('sdfTemplates', JSON.stringify(sdfTemplates));
		jQuery(e.target).parents('.sdf-templates-list-item').remove();
	}
});
jQuery(window).scroll(function(e) {
	if(jQuery(window).scrollTop() > jQuery('.sdf-buttons').offset().top - 30 && jQuery('.sdf-buttons').hasClass('pinned') != true) {
		jQuery('.sdf-buttons').addClass('pinned');
	} 
	if(jQuery(window).scrollTop() < jQuery('#sdf-wrap').offset().top && jQuery('.sdf-buttons').hasClass('pinned') == true) {
		jQuery('.sdf-buttons').removeClass('pinned');
	} 
});	

// increase / decrease module width
jQuery('#sdf-wrap').on('click', '.block-builder-box-size-inc, .block-builder-box-size-dec', function(e) {
	var box = jQuery(this).parents('.sdf-module-size');
	var index = sdfBuilder.moduleWidths.indexOf(box.find('.block-builder-box-width').text());
	var newIndex = (jQuery(e.target).hasClass('block-builder-box-size-inc') ? index+1 : index-1);
	if(newIndex >= 0 && newIndex < sdfBuilder.moduleWidths.length) {
		for( var i = 0; i < sdfBuilder.moduleWidths.length-1; i++ ){
			box
				.removeClass( sdfBuilder.moduleWidths[index] )
				.addClass( sdfBuilder.moduleWidths[newIndex] )
			box.find('.block-builder-box-width').text( sdfBuilder.moduleWidths[newIndex]);
			box.parents('.sdf-element')
				.attr('module_width', sdfBuilder.moduleWidths[newIndex])
				.removeClass(sdfBuilder.widths[sdfBuilder.moduleWidths[index]])
				.addClass(sdfBuilder.widths[sdfBuilder.moduleWidths[newIndex]]);
		}
		
		// sdfBuilder.updateEditor(true);	
	}	
});

</script>