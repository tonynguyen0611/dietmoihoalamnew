<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}
/**
 * THEME NAME: SEO Design Framework 
 *
 * TYPE: sdf_page.php
 * CLASS NAME: SDF_Page
 * DESCRIPTIONS: Implementation of Theme Admin Page Level Options    
 *
 * AUTHOR:  SEO Design Framework
 *    
 */
 
if ( !class_exists('SDF_Page') ) {  
	class SDF_Page extends ULTIncludes
	{
		protected $_sdfPageArgs = array(
			'breadcrumbs' => array( 
					'toggle_breadcrumbs' => 'global', 
					'bread_current_text' => '' 
				),
			'portfolio' => array( 
					'portfolio_type' => 'single_image' 
				),
			'social_media' => array( 
					'toggle_social_share' => 'global',
					'social_share_position' => 'global'
				),
			'blog' => array( 
					'toggle_single_featured_image' => 'global',
					'toggle_page_featured_image' => 'global', 
					'toggle_single_comments' => 'global', 
					'toggle_page_comments' => 'global', 
					'toggle_portfolio_comments' => 'global', 
					'toggle_related_posts' => 'global' 
				),
			'animations' => array( 
					'feat_img_animate' => 'global' 
				),
			'footer' => array( 
					'toggle_footer' => 'global', 
					'toggle_custom_footer' => '0', 
					'custom_nav_name' => '', 
					'copyright_footer' => '', 
				),
			'layout' => array( 
					'theme_layout' => 'global', 
					'toggle_page_layout'  => 'global', 
					'page_width' => '', 
					'page_sidebars' => '3', 
					'custom_left' => '', 
					'custom_center' => '', 
					'custom_right' => '' 
				),
			'header' => array( 
					'toggle_custom_header' => '0',
					'toggle_nav' => 'global', 
					'toggle_custom_nav' => '0', 
					'custom_nav_name' => '',
					'box_data_classes' => array(
							'listItem_1' => '',
							'listItem_2' => '',
							'listItem_3' => '',
							'listItem_4' => '',
							'listItem_5' => '',
							'listItem_6' => '',
							'listItem_7' => '',
							'listItem_8' => ''
						)
				),
			'title' => array( 
					'toggle_page_title' => 'global', 
					'title_header' => 'global', 
					'toggle_custom_title' => '1', 
					'title_custom_link' => '', 
					'title_custom_text' => '', 
					'subtitle_header' => 'h2', 
					'subtitle_text' => '' 
				),
			'slider' => array( 
					'toggle_slider' => 'global' 
				),
			'lightbox' => array( 
					'timed_lightbox_page' => '', 
					'timed_lightbox_animation' => 'glob', 
					'timed_lightbox_size' => 'glob', 
					'timed_lightbox_position' => '',
					'timed_lightbox_seconds' => '', 
					'bottom_lightbox_page' => '', 
					'bottom_lightbox_animation' => 'glob', 
					'bottom_lightbox_size' => 'glob', 
					'bottom_lightbox_position' => '' 
				),
			'preset' => array( 
					'theme_preset' => ''
				),
			'misc' => array( 
					'custom_js_header' => '', 
					'custom_js_footer' => '', 
					'yt_bg_video_url' => '', 
					'yt_bg_video_mute' => '0', 
					'yt_bg_video_volume' => '80', 
					'yt_bg_video_player_controls' => '0', 
					'yt_bg_video_volume_button' => '0', 
					'yt_bg_video_play_button' => '0', 
					'yt_bg_video_opacity' => '0.9', 
					'yt_bg_video_quality' => 'default', 
					'yt_bg_video_ratio' => 'auto', 
					'yt_bg_video_autoplay' => '1',
					'yt_bg_video_logo' => '0'
				),
			'typography' => array( 
					'page_padding_top' => '',
					'page_padding_bottom' => ''
				)
		);
		
		var $_currentSettings = array();
		var $sdf_post_id;

		public function __construct() 
		{
			//add_action( 'after_setup_theme',array(&$this, 'setup_meta_data') );
			//add_action( 'wp_insert_post',array(&$this, 'setup_meta_data_new') );
			add_action( 'add_meta_boxes', array(&$this, 'metaBoxesInit') );
			add_action( 'load-post.php', array(&$this, 'setFire') );
			add_action( 'load-post-new.php', array(&$this, 'setFire') );		
		}
		function setFire()
	 	{
			add_action( 'pre_post_update', array(&$this,'saveMetaBoxes'), 10, 2 );
			add_action( 'edit_post',		array( $this, 'saveMetaBoxes' ) );
			add_action( 'publish_post',		array( $this, 'saveMetaBoxes' ) );
			add_action( 'add_attachment',	array( $this, 'saveMetaBoxes' ) );
			add_action( 'edit_attachment',	array( $this, 'saveMetaBoxes' ) );	
			add_action( 'save_post', array(&$this, 'saveMetaBoxes'), 10, 2 );
			add_action( 'edit_page_form',	array( $this, 'saveMetaBoxes' ) );			
	 	}
		function setup_meta_data_new($post_id, $post = null){
			if($post_id){
				$meta = get_post_meta($post_id, SDF_META_KEY,true);
				
				if (empty($meta) || !is_array($meta) || $meta== NULL || $meta == ''){
					add_post_meta($post_id, SDF_META_KEY, $this->_sdfPageArgs,true);
				}			
			}
		}
		function setup_meta_data(){
			$the_theme_status = get_option( '_sdf_setup_status' );
			if ( $the_theme_status !== '1' ) {
				$posts = get_posts(array('numberposts' => -1,'post_type' => 'any') );

				foreach($posts as $p) { 
					$meta = get_post_meta($p->ID, SDF_META_KEY,true);
				 
					if (empty($meta)){
						add_post_meta($p->ID, SDF_META_KEY, $this->_sdfPageArgs,true);
					}
				}

				update_option( '_sdf_setup_status', '1' );
				
				$default_size = get_option('image_default_size');
				if($default_size == 'medium' || $default_size == '') $default_size = update_option('image_default_size', 'full');
			}
		}
		public function metaBoxesInit(){
			global $post;
			
			if( is_object($post) ){
				$this->sdf_post_id = $post->ID;
				
				if( $this->sdf_post_id ){
					 $this->_currentSettings = get_post_meta( $this->sdf_post_id, SDF_META_KEY, true );
				
					if (empty( $this->_currentSettings))
					{
						add_post_meta( $this->sdf_post_id, SDF_META_KEY, $this->_sdfPageArgs,true);
						$this->_currentSettings = $this->_sdfPageArgs;
					} 
					else{
						// new options patches
						$diff = $this->array_diff_key_recursive($this->_sdfPageArgs, $this->_currentSettings);
						if(!empty($diff)) {
							$this->_currentSettings = $this->mergeSettings($this->_sdfPageArgs, $this->_currentSettings, $diff);
						}
					}
				}
				add_meta_box('sdf_meta_settings', 'SEO Design Framework Local Page Settings',  array(&$this,'sdf_meta_box'), 'post', 'normal', 'high');
				add_meta_box('sdf_meta_settings', 'SEO Design Framework Local Page Settings',  array(&$this,'sdf_meta_box'), 'page', 'normal', 'high'); 	
				add_meta_box('sdf_meta_settings', 'SEO Design Framework Local Page Settings',  array(&$this,'sdf_meta_box'), 'portfolio', 'normal', 'high');
				add_meta_box('sdf_meta_settings', 'SEO Design Framework Local Page Settings',  array(&$this,'sdf_meta_box'), 'product', 'normal', 'high');
			}
		}
		public function setPageArgs($p_args){
			$this->_sdfPageArgs = $p_args;
		}
		public function getPageArgs(){
			return $this->_sdfPageArgs;	
		}
		function sdf_meta_box(){
			global $post;
			
		// Page Layout ?>
<div class="sdf-meta-wrap sdf-admin">
            
<ul class="nav nav-tabs" id="wpu-tab">
<?php if ($post->post_type == 'portfolio_TODO') : ?>
 <li class="active"><a href="#wpu-portfolio-settings" data-toggle="tab">Portfolio Options</a></li>
 <li><a href="#wpu-page-settings" data-toggle="tab">Settings</a></li>
<?php else : ?>
 <li class="active"><a href="#wpu-page-settings" data-toggle="tab">Settings</a></li>
<?php endif; ?>
 <li><a href="#wpu-page-header" data-toggle="tab">Header</a></li>
 <li><a href="#wpu-page-layout" data-toggle="tab">Layout</a></li>
 <li><a href="#wpu-page-footer" data-toggle="tab">Footer</a></li>
 <li><a href="#wpu-page-title" data-toggle="tab">Display Title</a></li>
 <li><a href="#wpu-page-background-video" data-toggle="tab">Background Video</a></li>
 <li><a href="#wpu-page-ads-lightboxes" data-toggle="tab">Ad Manager</a></li>
</ul>
 
<div class="tab-content">
	<?php if ($post->post_type == 'portfolio_TODO') : ?>
	<div class="tab-pane fade in active" id="wpu-portfolio-settings">
		<div class="wpu-group">
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Portfolio Item Type', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<select name="sdf_theme_page_portfolio_type" id="sdf_theme_page_portfolio_type" class="form-control" >
					<option value="single_image"<?php if($this->_currentSettings['portfolio']['portfolio_type'] == "single_image") echo ' selected="selected"'; ?>>Single Image</option>
					<option value="image_gallery"<?php if($this->_currentSettings['portfolio']['portfolio_type'] == "image_gallery") echo ' selected="selected"'; ?>>Image Gallery</option>
					<option value="image_slider"<?php if($this->_currentSettings['portfolio']['portfolio_type'] == "image_slider") echo ' selected="selected"'; ?>>Image Slider</option>
					<option value="video"<?php if($this->_currentSettings['portfolio']['portfolio_type'] == "video") echo ' selected="selected"'; ?>>Video</option>
					<option value="audio"<?php if($this->_currentSettings['portfolio']['portfolio_type'] == "audio") echo ' selected="selected"'; ?>>Audio</option>
				</select>
			</div>
			<div class="col-sm-4 col-md-4 help-text">
			</div>
			
		</div>
		</div>
	</div>
	<div class="tab-pane fade in" id="wpu-page-settings">
	<?php else : ?>
	<div class="tab-pane fade in active" id="wpu-page-settings">
	<?php endif; ?>
	
  	 <div class="wpu-group">
      <div class="bs-callout bs-callout-grey">
				<span class="sdf-settings wpu-meta-title"><?php _e('Local Page Settings', SDF_TXT_DOMAIN) ?></span>
			</div>
		<div class="form-group">
			<label for="sdf_theme_style_preset" class="col-sm-4 col-md-4 control-label"><?php _e('Assign Custom Style Preset', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<select name="sdf_theme_style_preset" class="form-control">
					<?php 
					$theme_presets = sdfGo()->sdf_get_global('preset', 'theme_presets');
					$page_style_preset = (isset($this->_currentSettings['preset']['theme_preset'])) ? $this->_currentSettings['preset']['theme_preset'] : '' ?>
					<option value=""<?php if($page_style_preset == "") echo ' selected="selected"'; ?>> Choose Preset </option>
					<?php foreach ( $theme_presets as $preset_id => $preset_name ) : ?>
					<option value="<?php echo $preset_id; ?>"<?php if($page_style_preset == $preset_id) echo ' selected="selected"'; ?>><?php echo $preset_name; ?></option>
					<?php endforeach; ?>
				</select>
				</div>
				<div class="col-sm-4 col-md-4 help-text">Choose Custom Style Preset for this page</div>
		</div>

		<div class="form-group">
			<label for="sdf_page_padding_top" class="col-sm-4 col-md-4 control-label"><?php _e('Page Padding Top', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_padding_top" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_padding_top']) echo $this->_currentSettings['typography']['page_padding_top']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>

		<div class="form-group">
			<label for="sdf_page_padding_bottom" class="col-sm-4 col-md-4 control-label"><?php _e('Page Padding Bottom', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-3">
			<input name="sdf_page_padding_bottom" class="form-control" type="text" value="<?php if($this->_currentSettings['typography']['page_padding_bottom']) echo $this->_currentSettings['typography']['page_padding_bottom']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><i class="fa fa-info-circle pull-left" title="" data-html="true" data-placement="top" data-toggle="tooltip" data-original-title="Use px suffix after number value"></i></div>
		</div>
		
		<?php 
		$screen = get_current_screen(); 
		if($screen->post_type == 'post') {
		?>
		
		<div class="form-group">
			<label for="toggle_single_featured_image" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Featured Image', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_featured_image'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_featured_image'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_featured_image'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_single_featured_image" value="<?php echo $this->_currentSettings['blog']['toggle_single_featured_image']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="toggle_single_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Post Comments', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_comments'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_comments'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_single_comments'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_single_comments" value="<?php echo $this->_currentSettings['blog']['toggle_single_comments']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="toggle_related_posts" class="col-sm-4 col-md-4 control-label"><?php _e('Show Related Posts', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_related_posts'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_related_posts'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_related_posts'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_related_posts" value="<?php echo $this->_currentSettings['blog']['toggle_related_posts']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<?php 
		}
		elseif($screen->post_type == 'page') {
		?>
		
		<div class="form-group">
			<label for="toggle_page_featured_image" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Page Featured Image', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_featured_image'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_featured_image'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_featured_image'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_page_featured_image" value="<?php echo $this->_currentSettings['blog']['toggle_page_featured_image']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="toggle_page_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Page Comments', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_comments'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_comments'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_page_comments'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_page_comments" value="<?php echo $this->_currentSettings['blog']['toggle_page_comments']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<?php 
		}
		elseif($screen->post_type == 'portfolio') {
		?>
		
		<div class="form-group">
			<label for="toggle_portfolio_comments" class="col-sm-4 col-md-4 control-label"><?php _e('Turn On/Off Portfolio Comments', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<div class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_portfolio_comments'],'global', "btn-sdf-grey")?>" value="global">Global</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_portfolio_comments'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['blog']['toggle_portfolio_comments'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="toggle_portfolio_comments" value="<?php echo $this->_currentSettings['blog']['toggle_portfolio_comments']  ; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<?php } ?>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Featured Image Animation on Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton($this->_currentSettings['animations']['feat_img_animate'],'global', "btn-sdf-grey")?>" value="global">Global</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['animations']['feat_img_animate'],'1', "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['animations']['feat_img_animate'],'0', "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="feat_img_animate" value="<?php echo $this->_currentSettings['animations']['feat_img_animate']; ?>" />				
			</div>
			<div class="col-sm-4 col-md-4 help-text"></div>
		</div>
						
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Social Share Buttons on Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['toggle_social_share'],'global', "btn-sdf-grey")?>" value="global">Global</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['toggle_social_share'],'1', "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['toggle_social_share'],'0', "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="theme_toggleSocialIcons" value="<?php echo $this->_currentSettings['social_media']['toggle_social_share']; ?>" />				
			</div>
			<div class="col-sm-4 col-md-4 help-text"></div>
		</div>
						
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Social Share Buttons Position', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['social_share_position'],'global', "btn-sdf-grey")?>" value="global">Global</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['social_share_position'],'below', "btn-sdf-grey")?>" value="below">Below Content</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['social_media']['social_share_position'],'above', "btn-sdf-grey")?>" value="above">Above Content</button>
				</div>
				<input type="hidden" name="theme_social_share_position" value="<?php echo $this->_currentSettings['social_media']['social_share_position']; ?>" />				
			</div>
			<div class="col-sm-4 col-md-4 help-text"></div>
		</div>
						
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Breadcrumbs on Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<div class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'],'global', "btn-sdf-grey")?>" value="global">Global</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'],'1', "btn-sdf-grey")?>" value="1">On</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['breadcrumbs']['toggle_breadcrumbs'],'0', "btn-sdf-grey")?>" value="0">Off</button>
				</div>
				<input type="hidden" name="theme_toggleBreadcrumbs" value="<?php echo $this->_currentSettings['breadcrumbs']['toggle_breadcrumbs']; ?>" />				
			</div>
			<div class="col-sm-4 col-md-4 help-text"></div>
		</div>
					
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Breadcrumb for Current Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<input type="text" id="sdf_theme_bread_text" class="form-control input-sm regular-text" name="sdf_theme_bread_text" value="<?php if(isset($this->_currentSettings['breadcrumbs']['bread_current_text'])): echo $this->_currentSettings['breadcrumbs']['bread_current_text']; endif; ?>" />
			</div>
			<div class="col-sm-4 col-md-4 help-text"><em><?php _e('(Default will use Page Title)', SDF_TXT_DOMAIN); ?></em></div>
		</div>

		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('HTML to Place in Head Tag', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<textarea name="sdf_theme_custom_js_header" class="form-control" id="sdf_theme_custom_js_header" cols="45" rows="5"><?php echo stripslashes( $this->_currentSettings['misc']['custom_js_header'] ); ?></textarea>
			</div>
			<div class="col-sm-4 col-md-4 help-text"><em><?php _e('(Place Styles, Scripts, or any HTML that needs to go into the &lt;head&gt; tag)', SDF_TXT_DOMAIN); ?></em></div>
		</div>
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('HTML to Place before end Body Tag', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<textarea name="sdf_theme_custom_js_footer" class="form-control" id="sdf_theme_custom_js_footer" cols="45" rows="5"><?php echo stripslashes( $this->_currentSettings['misc']['custom_js_footer'] ); ?></textarea>
			</div>
			<div class="col-sm-4 col-md-4 help-text"><em><?php _e('Script placed before end &lt;&frasl;body&gt; tag', SDF_TXT_DOMAIN) ?></em></div>
		</div>
	
	</div>     		
  </div>
  <div class="tab-pane fade" id="wpu-page-header"> <div class="wpu-group">
		<div class="bs-callout bs-callout-grey">
			<span class="wpu-navigation wpu-meta-title"><?php _e('Page Header', SDF_TXT_DOMAIN) ?> <em><?php _e('(Set Custom Header Structure and/or Navigation for Page)', SDF_TXT_DOMAIN); ?></em></span>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Navigation on Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-8 col-md-8">
                <div class="btn-group sdf_toggle">

                <button type="button" class="btn btn-sm<?php echo checkButton((string)$this->_currentSettings['header']['toggle_nav'],'global', "btn-sdf-grey")?>" value="global">Global</button>

				<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_nav'],'1', "btn-sdf-grey")?>" value="1">On</button>

   				<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_nav'],'0', "btn-sdf-grey")?>" value="0">Off</button>

 				</div>
 				<input type="hidden" name="theme_toggle_nav" value="<?php echo $this->_currentSettings['header']['toggle_nav']; ?>" />
				</div>
		</div>
    <div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Custom Navigation on Page?', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-4">
				<div id="wpu-page-nav" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_custom_nav'],'1', "btn-sdf-grey")?>" value="1">On</button>
   				<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_custom_nav'],'0', "btn-sdf-grey")?>" value="0">Off</button>
 				</div>
 				<input type="hidden" name="theme_pageNav" value="<?php echo $this->_currentSettings['header']['toggle_custom_nav']; ?>" />
			</div>
			<div class="col-sm-4 col-md-4 help-text"><em><?php _e('(Enabling custom navigation will be updatable through <a href="nav-menus.php">Menus</a> in WordPress.)', SDF_TXT_DOMAIN); ?></em></div>
		</div>
    <div id="page-menu-list" class="form-group" <?php $check = checkButton( $this->_currentSettings['header']['toggle_custom_nav'], '1'); if($check) echo 'style="display:block;"'; else echo 'style="display:none;"';  ?>>
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Select a created menu', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-4 col-md-4">
                          <?php try{
                                $getMenus = wp_get_nav_menus();?>
                                <select name="theme_pageNavName" class="form-control">
                                <option value="wpu-select-default"<?php if($this->_currentSettings['header']["custom_nav_name"]=="wpu-select-default")echo " selected=\"yes\""; ?>>-- Select Menu --</option>
                                <?php 
                                foreach($getMenus as $menus ){
                                ?>
                                <option value="<?php echo $menus->term_id; ?>"<?php if($this->_currentSettings['header']["custom_nav_name"]==$menus->term_id)echo " selected=\"yes\""; ?>><?php echo $menus->name; ?></option>
                                 
                                <?php  } ?>
                                 </select>
                                  <?php  }catch(Exception $err){}?>
                               
      </div>
			<div class="col-sm-4 col-md-4 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Custom Header on Page?', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-8 col-md-8">
			<div id="sdf-page-header" class="btn-group sdf_toggle">
				<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_custom_header'],'1', "btn-sdf-grey")?>" value="1">On</button>
				<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['header']['toggle_custom_header'],'0', "btn-sdf-grey")?>" value="0">Off</button>
			</div>
			<input type="hidden" name="theme_pageHeader" value="<?php echo $this->_currentSettings['header']['toggle_custom_header']; ?>" />
			</div>
		</div>
		<div id="page-custom-header">
		<div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Rearange Header Blocks (Logo, Navigation, Slider, Widget Blocks) or Drag Blocks to Inactive Zone Below', SDF_TXT_DOMAIN); ?></label>
			<div class="col-sm-8 col-md-8">
				<?php 
				if(!isset($this->_currentSettings['header']['header_order'])) {
					$this->_currentSettings['header']['header_order'] = sdfGo()->sdf_get_global('header', 'header_order');
				}
				if(!isset($this->_currentSettings['header']['box_data'])) {
					$this->_currentSettings['header']['box_data'] = sdfGo()->sdf_get_global('header', 'box_data');
				}
				?>
				
				<input type="hidden" name="sdf_theme_header_order" id="sdf_theme_header_order" value="<?php if(isset($this->_currentSettings['header']['header_order'])) echo implode(',', $this->_currentSettings['header']['header_order']); ?>" />
				
				<div id="sdf-block-header-active" class="sdf-block-header clearfix ui-sortable connectedHeaderList">
					<?php 
					// Header Sort Order
					if(is_array($this->_currentSettings['header']['header_order']) && !empty($this->_currentSettings['header']['header_order']))
					{
						// active boxes
						if(is_array($this->_currentSettings['header']['box_data']) && !empty($this->_currentSettings['header']['box_data']))
						{
							// sizes
							$box_data = $this->_currentSettings['header']['box_data'];
						
							foreach($this->_currentSettings['header']['header_order'] as $box_id)
							{
								
								echo '<div id="'.$box_id.'" class="block-builder-box '.$this->_smWidths[$box_data[$box_id]].'"><div class="block-builder-box-panel">
								<input class="block-builder-box-id" type="hidden" value="'.$box_id.'" name="block-builder-box-id[]">
								<input class="block-builder-box-size" type="hidden" value="'.$box_data[$box_id].'" name="block-builder-box-size[]">
								<div class="block-builder-box-size">
									<a class="block-builder-box-btn block-builder-box-size-dec" href="javascript:void(0);">-</a>
									<a class="block-builder-box-btn block-builder-box-size-inc" href="javascript:void(0);">+</a>
									<span class="block-builder-box-width">'.$box_data[$box_id].'</span>
								</div>
								<span class="block-builder-box-label">'.$this->_boxNames[$box_id]['name'].'</span>
								</div></div>';
							}
						
							
						}
					}
					?>
				</div>
		</div>
		
		<label for="" class="col-sm-4 col-md-4 control-label">Inactive Zone</label>
			<div class="col-sm-8 col-md-8">		
				<input type="hidden" name="sdf_theme_header_order_inactive" id="sdf_theme_header_order_inactive" value="" />
				
				<div id="sdf-block-header-inactive" class="sdf-block-header clearfix ui-sortable connectedHeaderList">
				
					<?php 
					// inactive boxes
					if(is_array($this->_currentSettings['header']['header_order']) && !empty($this->_currentSettings['header']['header_order']))
					{
						if(is_array($this->_currentSettings['header']['box_data']) && !empty($this->_currentSettings['header']['box_data']))
						{
							foreach($box_data as $box_id => $box_size)
							{
								
								if (!in_array($box_id, $this->_currentSettings['header']['header_order']))
								{
								echo '<div id="'.$box_id.'" class="block-builder-box col-md-4"><div class="block-builder-box-panel">
								<input class="block-builder-box-id" type="hidden" value="'.$box_id.'" name="block-builder-box-id[]">
								<input class="block-builder-box-size" type="hidden" value="1/3" name="block-builder-box-size[]">
								<div class="block-builder-box-size">
									<a class="block-builder-box-btn block-builder-box-size-dec" href="javascript:void(0);">-</a>
									<a class="block-builder-box-btn block-builder-box-size-inc" href="javascript:void(0);">+</a>
									<span class="block-builder-box-width">1/3</span>
								</div>
								<span class="block-builder-box-label">'.$this->_boxNames[$box_id]['name'].'</span>
								</div></div>';
								}
							}
						}
					}
					?>
				
				</div>
				
			</div>
			</div>
	
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e('Additional Block Classes', SDF_TXT_DOMAIN); ?></h4>
			<p><?php _e('You can use custom Classes or Bootstrap Grid Classes:', SDF_TXT_DOMAIN) ?> <a href="http://getbootstrap.com/css/#grid" target="_blank">Bootstrap Grid system</a></p>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_5" class="col-sm-4 col-md-4 control-label"><?php _e('Logo', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_5" class="form-control" id="sdf_theme_header_box_data_classes_header_block_5" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_5']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_6" class="col-sm-4 col-md-4 control-label"><?php _e('Navigation', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_6" class="form-control" id="sdf_theme_header_box_data_classes_header_block_6" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_6']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_8" class="col-sm-4 col-md-4 control-label"><?php _e('Secondary Navigation', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_8" class="form-control" id="sdf_theme_header_box_data_classes_header_block_8" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_8']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_1" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 1', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_1" class="form-control" id="sdf_theme_header_box_data_classes_header_block_1" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_1']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_2" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 2', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_2" class="form-control" id="sdf_theme_header_box_data_classes_header_block_2" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_2']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_3" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 3', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_3" class="form-control" id="sdf_theme_header_box_data_classes_header_block_3" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_3']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_4" class="col-sm-4 col-md-4 control-label"><?php _e('Header Block 4', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_4" class="form-control" id="sdf_theme_header_box_data_classes_header_block_4" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_4']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		<div class="form-group">
			<label for="sdf_theme_header_box_data_classes_header_block_7" class="col-sm-4 col-md-4 control-label"><?php _e('Slider Block', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
			<input type="text" name="sdf_theme_header_box_data_classes_header_block_7" class="form-control" id="sdf_theme_header_box_data_classes_header_block_7" value="<?php echo $this->_currentSettings['header']['box_data_classes']['listItem_7']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"></div>
		</div>
		
		</div>
		
		
             
	</div>           
  </div>
  <div class="tab-pane fade" id="wpu-page-layout">
	<div class="wpu-group">
		<div class="bs-callout bs-callout-grey">
			<span class="sdf-layout wpu-meta-title"><?php _e('Page Layout', SDF_TXT_DOMAIN) ?> <em><?php _e('(Select Layout Settings)', SDF_TXT_DOMAIN); ?></em></span>
		</div>
              
	<div class="wpu-sub">
    <div class="form-group">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Layout', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-4 col-md-3">
				<div id="sdf-page-layout" class="btn-group sdf_toggle">
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['layout']['toggle_page_layout'],'global', "btn-sdf-grey")?>" value="global">Global</button>
					<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['layout']['toggle_page_layout'],'1', "btn-sdf-grey")?>" value="1">Custom</button>
				</div>
				<input type="hidden" name="sdf_toggle_page_layout" value="<?php echo $this->_currentSettings['layout']['toggle_page_layout']; ?>" />
			</div>
			<div class="col-sm-4 col-md-5 help-text"><?php _e('Set Custom Page/Post Layout or Use Global Settings', SDF_TXT_DOMAIN) ?></div>
		</div>                    	
                        	
				 

			 <div id="page-custom-layout">
			 <div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 1: <span class="step-text">Page Layout</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Layout', SDF_TXT_DOMAIN); ?></label>
							<div class="col-sm-4 col-md-3">
							<select name="sdf_theme_page_width" class="form-control" id="sdf_theme_page_width">
								<option value="wide"<?php if($this->_currentSettings['layout']['page_width'] == "wide") echo ' selected="selected"'; ?>>Wide</option>
								<option value="boxed"<?php if($this->_currentSettings['layout']['page_width'] == "boxed") echo ' selected="selected"'; ?>>Boxed</option>
							</select>
							</div>
							<div class="col-sm-4 col-md-5 help-text">A fixed website layout has a wrapper that is a fixed width. The layout style will be applied globally, throughout the entire site unless you set custom per page layout in page/post options.</div>
						</div>
					  </div>
					</div>
       
					<div class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 2: <span class="step-text">Columns</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<div class="wpu-page-layouts wpu-sub-single">
							<ul class="sdf-layouts col-md-12">
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_3" value="col_3"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_3') echo " checked"; ?> />
									<label for="col_3"><span class="lay_title">Both</span><span class="lay_subtitle">Sidebars</span><span class="layout_col_3"></span></label>
								</li>    
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_right_2" value="col_right_2"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_right_2') echo " checked"; ?>  />
									<label for="col_right_2"><span class="lay_title">Right</span><span class="lay_subtitle">Sidebar</span><span class="layout_col_right_2"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="col_left_2" value="col_left_2"<?php if($this->_currentSettings['layout']['theme_layout'] == 'col_left_2') echo " checked"; ?>  />
								<label for="col_left_2"><span class="lay_title">Left</span><span class="lay_subtitle">Sidebar</span><span class="layout_col_left_2"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="ssc_left" value="ssc_left"<?php if($this->_currentSettings['layout']['theme_layout'] == 'ssc_left') echo " checked"; ?>  />
								<label for="ssc_left"><span class="lay_title">Both (L)</span><span class="lay_subtitle">Sidebars Left</span><span class="layout_ssc_left"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="css_right" value="css_right"<?php if($this->_currentSettings['layout']['theme_layout'] == 'css_right') echo " checked"; ?>  />
								<label for="css_right"><span class="lay_title">Both (R)</span><span class="lay_subtitle">Sidebars Right</span><span class="layout_css_right"></span></label>
								</li>
								<li><input type="radio" class="sdf_page_layout" name="sdf_theme_layout_page" id="no_col" value="no_col"<?php if($this->_currentSettings['layout']['theme_layout'] == 'no_col') echo " checked"; ?>  />
								<label for="no_col"><span class="lay_title">Single</span><span class="lay_subtitle">No Sidebars</span><span class="layout_no_col"></span></label>
								</li>
							   <li id="wpu-custom-layout-li"><input type="radio" class="sdf_page_layout" id="sdf_custom_layout" name="sdf_theme_layout_page" value="custom"<?php if($this->_currentSettings['layout']['theme_layout'] == 'custom') echo " checked"; ?>  />
								<label for="sdf_custom_layout"><span class="lay_title">Custom</span><span class="lay_subtitle">Columns</span><span class="layout_sdf_custom_layout"></span></label>
								</li>
						   </ul>
						</div>
						</div>
						</div>
					</div>


					<div id="wpu-step3" class="panel panel-default">
					  <div class="panel-heading">
						<h3 class="panel-title step-title">Step 3: <span class="step-text">Sidebar Widths</span></h3>
					  </div>
					  <div class="panel-body">
						<div class="form-group">
							<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Sidebar Width', SDF_TXT_DOMAIN); ?></label>
							<div id="wpu-sidebar-width" class="col-sm-4 col-md-3">
							<select name="sdf_page_sidebars_width" id="sdf_page_sidebars_width" class="form-control">
								<option value="6"<?php if($this->_currentSettings['layout']['page_sidebars'] == "6") echo ' selected="selected"'; ?>>One Half</option>
								<option value="5"<?php if($this->_currentSettings['layout']['page_sidebars'] == "5") echo ' selected="selected"'; ?>>Five Twelfth</option>
								<option value="4"<?php if($this->_currentSettings['layout']['page_sidebars'] == "4") echo ' selected="selected"'; ?>>One Third</option>
								<option value="3"<?php if($this->_currentSettings['layout']['page_sidebars'] == "3") echo ' selected="selected"'; ?>>One Fourth</option>
								<option value="2"<?php if($this->_currentSettings['layout']['page_sidebars'] == "2") echo ' selected="selected"'; ?>>One Sixth</option>
								<option value="1"<?php if($this->_currentSettings['layout']['page_sidebars'] == "1") echo ' selected="selected"'; ?>>One Twelfth</option>
							</select>
				</div>       
							 <div id="custom-layout" class="wpu-sub-single" style="display:none">   
							 
								 <script type="text/javascript">
                jQuery.noConflict();
        jQuery(document).ready(function($){
                try{
                    
            $( "#slider-layout" ).slider({
                    range: true,
                    min: 0,
                    max: 1170,
                    values: [<?php if($this->_currentSettings['layout']['custom_left'] && !($this->_currentSettings['layout']['custom_left'] === 0)):echo (int)$this->_currentSettings['layout']['custom_left'];  elseif($this->_currentSettings['layout']['custom_left'] === 0): echo '0'; else: echo '200'; endif; ?>,<?php if($this->_currentSettings['layout']['custom_right'] && !($this->_currentSettings['layout']['custom_right'] === 0)): echo (1170 - (int)$this->_currentSettings['layout']['custom_right'] ); elseif($this->_currentSettings['layout']['custom_right'] === 0): echo '0'; else: echo '740'; endif; ?>],
                    slide: function( event, ui ) {
                        $( "#sdf_theme_custom_left" ).val( ui.values[ 0 ]+"px" );
                        $( "#sdf_theme_custom_center" ).val(1170  -  (ui.values[ 0 ] +(1170 - ui.values[ 1 ]))+"px" );
                        $( "#sdf_theme_custom_right" ).val( 1170 - ui.values[ 1 ]+"px"  );
                    }
                });
                $( "#sdf_theme_custom_left" ).val( ui.values[ 0 ]+"px"  );
                        $( "#sdf_theme_custom_center" ).val( 1170 -  (ui.values[ 0 ] + (1170 - ui.values[ 1 ]))+"px"  );
                        $( "#sdf_theme_custom_right" ).val(  1170 - ui.values[ 1 ] +"px" );
            }catch(err){}
            })
                </script>
                                            
                                                    <label for="sdf_theme_layoutsize"><?php _e('Custom Layout:', SDF_TXT_DOMAIN) ?></label> 
                                                    <ul id="custom-px-box">
                                                    <li><?php  _e('Left Column', SDF_TXT_DOMAIN); ?><br />
                                                    <input type="text" id="sdf_theme_custom_left" name="sdf_theme_custom_left" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if(isset($this->_currentSettings['layout']['custom_left'])): echo $this->_currentSettings['layout']['custom_left']; else: echo '200px'; endif; ?>" /></li>
                                                    
                                                    <li><?php  _e('Center Column', SDF_TXT_DOMAIN); ?><br />
                                                    <input type="text" id="sdf_theme_custom_center" name="sdf_theme_custom_center" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if(isset($this->_currentSettings['layout']['custom_center'])): echo $this->_currentSettings['layout']['custom_center']; else: echo '540px'; endif; ?>" /></li>
                                                    <li><?php  _e('Right Column', SDF_TXT_DOMAIN); ?><br />
                                                    <input type="text" id="sdf_theme_custom_right" name="sdf_theme_custom_right" style="border:0; color:#ed941e; font-weight:bold;" value="<?php if(isset($this->_currentSettings['layout']['custom_right'])): echo $this->_currentSettings['layout']['custom_right']; else: echo '200px'; endif; ?>" /></li></ul>
                                                    <div id="slider-layout"></div>
        						 </div>  
 						</div>
					  </div>
					</div> 
 
    </div> 
	</div> 
  
 </div>
 </div>
  <div class="tab-pane fade" id="wpu-page-footer">
		<div class="wpu-group">
      <div class="bs-callout bs-callout-grey">
				<span class="sdf-footer wpu-meta-title"><?php _e('Page Footer', SDF_TXT_DOMAIN) ?> <em><?php _e('(Disable -or- Create Custom Footer For Page/Post)', SDF_TXT_DOMAIN); ?></em></span>
			</div>	
                         
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Display Footer on Page?', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-8 col-md-8">
					<div class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['footer']['toggle_footer'],'global', "btn-sdf-grey")?>" value="global">Global</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['footer']['toggle_footer'],'1', "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['footer']['toggle_footer'],'0', "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="sdf_theme_toggleFooter" value="<?php echo $this->_currentSettings['footer']['toggle_footer']; ?>" />
				</div>
			</div>          
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Add Copyright Bottom Footer Content', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-8 col-md-8">
					<?php wp_editor( stripslashes($this->_currentSettings['footer']['copyright_footer']), "sdf_page_footer",  array('textarea_rows'=>5,'wpautop' => true) ); ?>
				</div>
			</div>
			<!-- end footer settings -->
		</div>
	</div>
 
  <div class="tab-pane fade" id="wpu-page-title">
		<div class="wpu-group">
      <div class="bs-callout bs-callout-grey">
				<span class="wpu-title-settings wpu-meta-title"><?php _e('On Page Title Display', SDF_TXT_DOMAIN) ?> <em><?php _e('(Select on Page Title Settings)', SDF_TXT_DOMAIN); ?></em></span>
			</div>
			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Toggle Title', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf-toggle-page-title" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_page_title'],'global', "btn-sdf-grey")?>" value="global">Global</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_page_title'],"1", "btn-sdf-grey")?>" value="1">On</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_page_title'],"0", "btn-sdf-grey")?>" value="0">Off</button>
					</div>
					<input type="hidden" name="theme_toggleTitle" value="<?php echo $this->_currentSettings['title']['toggle_page_title']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"></div>
			</div>			
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Title Tag', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf-toggle-page-title" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"global", "btn-sdf-grey")?>" value="global">Global</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"h1", "btn-sdf-grey")?>" value="h1">H1</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"h2", "btn-sdf-grey")?>" value="h2">H2</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['title_header'],"disable", "btn-sdf-grey")?>" value="disable">None</button>
					</div>
					<input type="hidden" name="theme_titleHeader" value="<?php echo $this->_currentSettings['title']['title_header']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('(H1, H2, or No HTML Tags)', SDF_TXT_DOMAIN) ?></div>
			</div>            
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Make Page Title a Link', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf-toggle-page-title" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_custom_title'],"1", "btn-sdf-grey")?>" value="1">Yes</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['toggle_custom_title'],"0", "btn-sdf-grey")?>" value="0">No</button>
					</div>
					<input type="hidden" name="theme_toggleTitleAnchor" value="<?php echo $this->_currentSettings['title']['toggle_custom_title']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"></div>
			</div>            
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Title Link', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" name="theme_toggleCustomLink" value="<?php echo $this->_currentSettings['title']['title_custom_link']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('(*Only works if above is set to "Yes".<br /> This will OVERRIDE page title link )', SDF_TXT_DOMAIN) ?></div>
			</div>                        
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Custom Title Text', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" name="theme_toggleCustomText" value="<?php echo $this->_currentSettings['title']["title_custom_text"]; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('(*This will replace current Title with text from textbox. )', SDF_TXT_DOMAIN) ?></div>
			</div>                       
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Page Subtitle Tag', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf-toggle-page-title" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['subtitle_header'],"h2", "btn-sdf-grey")?>" value="h2">H2</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['subtitle_header'],"h3", "btn-sdf-grey")?>" value="h3">H3</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['subtitle_header'],"h4", "btn-sdf-grey")?>" value="h4">H4</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['subtitle_header'],"h5", "btn-sdf-grey")?>" value="h5">H5</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['title']['subtitle_header'],"disable", "btn-sdf-grey")?>" value="disable">None</button>
					</div>
					<input type="hidden" name="theme_subtitleHeader" value="<?php echo $this->_currentSettings['title']['subtitle_header']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('(H2, H3, H4 or No HTML Tags)', SDF_TXT_DOMAIN) ?></div>
			</div>
			<div class="form-group">
				<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Subtitle Text', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" name="theme_subtitleText" value="<?php if(isset($this->_currentSettings['title']['subtitle_text'])): echo $this->_currentSettings['title']['subtitle_text']; endif; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"></div>
			</div>
		</div>                 
		<!-- end on page title settings -->
  </div>
 
  <div class="tab-pane fade" id="wpu-page-background-video">
		<div class="wpu-group">
      <div class="bs-callout bs-callout-grey">
				<span class="wpu-title-settings wpu-meta-title"><?php _e('On Page Background Video', SDF_TXT_DOMAIN) ?> <em><?php _e('(Select Background Video Settings)', SDF_TXT_DOMAIN); ?></em></span>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_url" class="col-sm-4 col-md-4 control-label"><?php _e('YouTube Video', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" id="sdf_theme_yt_bg_video_url" name="sdf_theme_yt_bg_video_url" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_url']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Parameter that can contain either the complete URL or the short URL provided by YouTube or the video ID e.g. "http://www.youtube.com/watch?Qz1023Q" or "Qz1023Q"', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_mute" class="col-sm-4 col-md-4 control-label"><?php _e('Mute', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_mute" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_mute'],"1", "btn-sdf-grey")?>" value="1">Yes</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_mute'],"0", "btn-sdf-grey")?>" value="0">No</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_mute" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_mute']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Mute the audio', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_volume_button" class="col-sm-4 col-md-4 control-label"><?php _e('Show or hide custom volume control button', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_volume_button" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_volume_button'],"1", "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_volume_button'],"0", "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_volume_button" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_volume_button']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Show or hide volume controls', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_play_button" class="col-sm-4 col-md-4 control-label"><?php _e('Show or hide custom play/stop control button', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_play_button" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_play_button'],"1", "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_play_button'],"0", "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_play_button" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_play_button']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Show or hide play controls', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_quality" class="col-sm-4 col-md-4 control-label"><?php _e('Quality', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_quality" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"default", "btn-sdf-grey")?>" value="default">Default</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"medium", "btn-sdf-grey")?>" value="medium">Medium</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"large", "btn-sdf-grey")?>" value="large">Large</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"hd720", "btn-sdf-grey")?>" value="hd720">hd720</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"hd1080", "btn-sdf-grey")?>" value="hd1080">hd1080</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_quality'],"highres", "btn-sdf-grey")?>" value="highres">High Res</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_quality" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_quality']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Set the quality of the movie', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_ratio" class="col-sm-4 col-md-4 control-label"><?php _e('Aspect Ratio', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_ratio" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_ratio'],"16/9", "btn-sdf-grey")?>" value="16/9">16/9</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_ratio'],"4/3", "btn-sdf-grey")?>" value="4/3">4/3</button>
						<button type="button" class="btn btn-xs<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_ratio'],"auto", "btn-sdf-grey")?>" value="auto">auto</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_ratio" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_ratio']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Set the aspect ratio of the movie', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_autoplay" class="col-sm-4 col-md-4 control-label"><?php _e('Autoplay', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_autoplay" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_autoplay'],"1", "btn-sdf-grey")?>" value="1">Yes</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_autoplay'],"0", "btn-sdf-grey")?>" value="0">No</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_autoplay" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_autoplay']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Play the video once ready', SDF_TXT_DOMAIN) ?></div>
			</div>		
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_volume" class="col-sm-4 col-md-4 control-label"><?php _e('Volume', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" id="sdf_theme_yt_bg_video_volume" name="sdf_theme_yt_bg_video_volume" value="<?php echo $this->_currentSettings['misc']["yt_bg_video_volume"]; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Set the volume level of the video (1 to 100 (number))', SDF_TXT_DOMAIN) ?></div>
			</div> 		
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_opacity" class="col-sm-4 col-md-4 control-label"><?php _e('Opacity', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<input type="text" class="form-control input-sm regular-text" id="sdf_theme_yt_bg_video_opacity" name="sdf_theme_yt_bg_video_opacity" value="<?php echo $this->_currentSettings['misc']["yt_bg_video_opacity"]; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Define the opacity of the video (0 to 1 (number) and default value is 0.9)', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_player_controls" class="col-sm-4 col-md-4 control-label"><?php _e('Show or hide native player control bar', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_player_controls" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_player_controls'],"1", "btn-sdf-grey")?>" value="1">Show</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_player_controls'],"0", "btn-sdf-grey")?>" value="0">Hide</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_player_controls" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_player_controls']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Show or hide player controls at the bottom', SDF_TXT_DOMAIN) ?></div>
			</div>
			
			<div class="form-group">
				<label for="sdf_theme_yt_bg_video_logo" class="col-sm-4 col-md-4 control-label"><?php _e('YouTube Logo', SDF_TXT_DOMAIN) ?></label>
				<div class="col-sm-4 col-md-4">
					<div id="sdf_theme_yt_bg_video_logo" class="btn-group sdf_toggle">
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_logo'],"1", "btn-sdf-grey")?>" value="1">Yes</button>
						<button type="button" class="btn btn-sm<?php echo checkButton( $this->_currentSettings['misc']['yt_bg_video_logo'],"0", "btn-sdf-grey")?>" value="0">No</button>
					</div>
					<input type="hidden" name="sdf_theme_yt_bg_video_logo" value="<?php echo $this->_currentSettings['misc']['yt_bg_video_logo']; ?>" />
				</div>
				<div class="col-sm-4 col-md-4 help-text"><?php _e('Show or hide the YouTube logo and the link to the original video URL in case player control bar (above) is enabled.', SDF_TXT_DOMAIN) ?></div>
			</div>
			
		</div>                 
		<!-- end on page background video settings -->
  </div> 

  <div class="tab-pane fade" id="wpu-page-ads-lightboxes">
		<div class="wpu-group">
		
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e('Timed Lightbox', SDF_TXT_DOMAIN) ?></h4>
			<p><?php _e('Automatically display a lightbox on this page', SDF_TXT_DOMAIN); ?></p>
		</div>
		<div class="form-group ads-head">
			<div class="col-sm-4 col-md-4">
			<div class="row">
			<label for="" class="col-sm-2 col-md-2 control-label">#</label>
			<label for="" class="col-sm-10 col-md-10 control-label"><?php _e('Choose Which Lightbox Ad to Show', SDF_TXT_DOMAIN) ?></label>
			</div>
			</div>
			<label for="" class="col-sm-2 col-md-2 control-label"><?php _e('Modal Size', SDF_TXT_DOMAIN) ?></label>
			<label for="" class="col-sm-2 col-md-2 control-label"><?php _e('Entrance Animation', SDF_TXT_DOMAIN) ?></label>
			<div class="col-sm-3 col-md-3">
			<div class="row">
			<label for="" class="col-sm-6 col-md-6 control-label"><?php _e('Show After', SDF_TXT_DOMAIN) ?></label>
			<label for="" class="col-sm-6 col-md-6 control-label"><?php _e('Modal Position', SDF_TXT_DOMAIN) ?></label>
			</div>
			</div>
			<label for="" class="col-sm-1 col-md-1 control-label"><i class="fa fa-plus-circle fa-lg"></i> <i class="fa fa-minus-circle fa-lg"></i></label>
		</div>
		<?php
		$timed_lightbox_page = array();
		$timed_lightbox_size = array();
		$timed_lightbox_animation = array();
		$timed_lightbox_seconds = array();
		$timed_lightbox_position = array();
		// fallback for single timed lightbox
		if (!is_array($this->_currentSettings['lightbox']['timed_lightbox_page'])) {
			$timed_lightbox_page[] = $this->_currentSettings['lightbox']['timed_lightbox_page'];
			$timed_lightbox_size[] = $this->_currentSettings['lightbox']['timed_lightbox_size'];
			$timed_lightbox_animation[] = $this->_currentSettings['lightbox']['timed_lightbox_animation'];
			$timed_lightbox_seconds[] = $this->_currentSettings['lightbox']['timed_lightbox_seconds'];
			$timed_lightbox_position[] = $this->_currentSettings['lightbox']['timed_lightbox_position'];
		}
		else{
			$timed_lightbox_page = $this->_currentSettings['lightbox']['timed_lightbox_page'];
			$timed_lightbox_size = $this->_currentSettings['lightbox']['timed_lightbox_size'];
			$timed_lightbox_animation = $this->_currentSettings['lightbox']['timed_lightbox_animation'];
			$timed_lightbox_seconds = $this->_currentSettings['lightbox']['timed_lightbox_seconds'];
			$timed_lightbox_position = $this->_currentSettings['lightbox']['timed_lightbox_position'];
		}
		
		?>
		<div class="modal-ads">	
		
		<div class="form-group ads-row-prototype hide">	
			<div class="col-sm-4 col-md-4">
			<div class="row">
			<div class="col-sm-2 col-md-2 text-right row-index">
			</div>
			<div class="col-sm-10 col-md-10">
				<select name="sdf_timed_lightbox_page[]" id="sdf_timed_lightbox_page" class="form-control" >
					<option value="">No</option>
					<?php
						$ads = get_posts(array('post_type' => 'ads'));
						$pages_hash = array();
						foreach($ads as $ad) {
							echo '<option value="'.$ad->ID.'">'.$ad->post_title.'</option>';
						} ?>
				</select>
			</div>
			</div>
			</div>
			<div class="col-sm-2 col-md-2">
				<select name="sdf_timed_lightbox_size[]" id="sdf_timed_lightbox_size" class="form-control" >
					<?php
						$modal_sizes = array( '' => 'Default', 'lg' => 'Large', 'sm' => 'Small' );
						foreach($modal_sizes as $key => $val) {
							echo '<option value="' . $key . '">' . $val . '</option>';
						} ?>
				</select>
			</div>
			<div class="col-sm-2 col-md-2">
				<select id="sdf_timed_lightbox_animation" name="sdf_timed_lightbox_animation[]" class="form-control"><?php echo sdfGo()->_wpuGlobal->createModalAnimationPicker( sdfGo()->sdf_get_global('lightbox', 'lightbox_animation_types'), '', true, 'default' ); ?></select>
			</div>
			<div class="col-sm-3 col-md-3">
			<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="input-group">
					<input type="text" id="sdf_timed_lightbox_seconds" name="sdf_timed_lightbox_seconds[]" class="form-control" placeholder="number of seconds" value="" />
					<span class="input-group-addon">secs</span>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<select name="sdf_timed_lightbox_position[]" id="sdf_timed_lightbox_position" class="form-control" >
					<?php
						$modal_positions = sdfGo()->sdf_get_global('lightbox', 'modal_lightbox_positions');
						foreach($modal_positions as $key => $val) {
							$selected = $timed_lightbox_position[$val] == $key;
							echo '<option value="' . $key . (($selected) ? '" selected="yes">' : '">') . $val . '</option>';
						} ?>
				</select>
			</div>
			</div>
			</div>
			<div class="col-sm-1 col-md-1">
				<a class="ads-remove btn btn-small btn-danger pull-right"><i class="fa fa-minus-circle fa-lg"></i></a>
			</div>
		</div>	
		
		<?php 
		$modals_count = count($timed_lightbox_page);
				
		for($i=0; $i<$modals_count; $i++):
		?>
		
		<div class="form-group ads-row">	
			<div class="col-sm-4 col-md-4">
			<div class="row">
			<div class="col-sm-2 col-md-2 text-right">
				<?php echo ($i+1); ?>.
			</div>
			<div class="col-sm-10 col-md-10">
				<select name="sdf_timed_lightbox_page[]" id="sdf_timed_lightbox_page" class="form-control" >
					<option value="">No</option>
					<?php
						$ads = get_posts(array('post_type' => 'ads'));
						$pages_hash = array();
						foreach($ads as $ad) {
							$selected = $timed_lightbox_page[$i] == $ad->ID;
							echo '<option value="'.$ad->ID.(($selected) ? '" selected="yes">' : '">').$ad->post_title.'</option>';
						} ?>
				</select>
			</div>
			</div>
			</div>
			<div class="col-sm-2 col-md-2">
				<select id="sdf_timed_lightbox_size" name="sdf_timed_lightbox_size[]" class="form-control"><?php echo sdfGo()->_wpuGlobal->createModalAnimationPicker( sdfGo()->sdf_get_global('lightbox', 'lightbox_sizes'), $timed_lightbox_size[$i], true ); ?></select>
			</div>
			<div class="col-sm-2 col-md-2">
				<select id="sdf_timed_lightbox_animation" name="sdf_timed_lightbox_animation[]" class="form-control"><?php echo sdfGo()->_wpuGlobal->createModalAnimationPicker( sdfGo()->sdf_get_global('lightbox', 'lightbox_animation_types'), $timed_lightbox_animation[$i], true, 'default' ); ?></select>
			</div>
			<div class="col-sm-3 col-md-3">
			<div class="row">
			<div class="col-sm-6 col-md-6">
				<div class="input-group">
					<input type="text" id="sdf_timed_lightbox_seconds" name="sdf_timed_lightbox_seconds[]" class="form-control" placeholder="number of seconds" value="<?php echo $timed_lightbox_seconds[$i]; ?>" />
					<span class="input-group-addon">secs</span>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<select name="sdf_timed_lightbox_position[]" id="sdf_timed_lightbox_position" class="form-control" >
					<?php
						$modal_positions = sdfGo()->sdf_get_global('lightbox', 'modal_lightbox_positions');
						foreach($modal_positions as $key => $val) {
							$selected = $timed_lightbox_position[$i] == $key;
							echo '<option value="' . $key . (($selected) ? '" selected="yes">' : '">') . $val . '</option>';
						} ?>
				</select>
			</div>
			</div>
			</div>
			<div class="col-sm-1 col-md-1">
				<a class="ads-remove btn btn-small btn-danger pull-right"><i class="fa fa-minus-circle fa-lg"></i></a>
			</div>
		</div>	
		
		<?php endfor; ?>
		
		<div class="form-group add-ads-holder">
			<label for="" class="col-sm-11 col-md-11 control-label"></label>
			<label for="" class="col-sm-1 col-md-1 control-label"><a class="ads-add btn btn-small btn-success"><i class="fa fa-plus-circle fa-lg"></i></a></label>
		</div> 
		
		</div> 
		
		<div class="bs-callout bs-callout-grey">
			<h4><?php _e('Bottom Triggered Lightbox', SDF_TXT_DOMAIN) ?></h4>
			<p><?php _e('Lightbox triggered when scrolled to the page bottom', SDF_TXT_DOMAIN); ?></p>
		</div>
		<div class="form-group ads-head">
			<label for="" class="col-sm-4 col-md-4 control-label"><?php _e('Choose Which Lightbox Ad to Show', SDF_TXT_DOMAIN) ?></label>
			<label for="" class="col-sm-2 col-md-2 control-label"><?php _e('Modal Size', SDF_TXT_DOMAIN) ?></label>
			<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Entrance Animation', SDF_TXT_DOMAIN) ?></label>
			<label for="" class="col-sm-3 col-md-3 control-label"><?php _e('Modal Position', SDF_TXT_DOMAIN) ?></label>
		</div>
		<?php
		
		$bottom_lightbox_page = $this->_currentSettings['lightbox']['bottom_lightbox_page'];
		$bottom_lightbox_size = $this->_currentSettings['lightbox']['bottom_lightbox_size'];
		$bottom_lightbox_animation = $this->_currentSettings['lightbox']['bottom_lightbox_animation'];
		$bottom_lightbox_position = $this->_currentSettings['lightbox']['bottom_lightbox_position'];
		
		?>
		<div class="form-group ads-row">
			<div class="col-sm-4 col-md-4">
				<select name="sdf_bottom_lightbox_page" id="sdf_bottom_lightbox_page" class="form-control" >
					<option value="">No</option>
					<?php
						$ads = get_posts(array('post_type' => 'ads'));
						$pages_hash = array();
						foreach($ads as $ad) {
							$selected = $bottom_lightbox_page == $ad->ID;
							echo '<option value="'.$ad->ID.(($selected) ? '" selected="yes">' : '">').$ad->post_title.'</option>';
						} ?>
				</select>
			</div>
			<div class="col-sm-2 col-md-2">
				<select id="sdf_bottom_lightbox_size" name="sdf_bottom_lightbox_size" class="form-control"><?php echo sdfGo()->_wpuGlobal->createModalAnimationPicker( sdfGo()->sdf_get_global('lightbox', 'lightbox_sizes'), $bottom_lightbox_size, true ); ?></select>
			</div>
			<div class="col-sm-3 col-md-3">
				<select id="sdf_bottom_lightbox_animation" name="sdf_bottom_lightbox_animation" class="form-control"><?php echo sdfGo()->_wpuGlobal->createModalAnimationPicker( sdfGo()->sdf_get_global('lightbox', 'lightbox_animation_types'), $bottom_lightbox_animation, true, 'default' ); ?></select>
			</div>
			<div class="col-sm-3 col-md-3">
				<select name="sdf_bottom_lightbox_position" id="sdf_bottom_lightbox_position" class="form-control" >
					<?php
						$modal_positions = sdfGo()->sdf_get_global('lightbox', 'modal_lightbox_positions');
						foreach($modal_positions as $key => $val) {
							$selected = $bottom_lightbox_position == $key;
							echo '<option value="' . $key . (($selected) ? '" selected="yes">' : '">') . $val . '</option>';
						} ?>
				</select>
			</div>
		</div>
	
		</div>
		<!-- end on page ads lightboxes settings -->
  </div>
 
</div> </div>         
          <?php wp_nonce_field( basename( __FILE__ ), 'sdf_post_nonce' );
         
	 }

  	public function saveMetaBoxes($post_id){ 
			global $post;
			
			// dont run if the post array is no set
			if(empty($_POST) || empty($_POST['post_ID'])) 
				return;
			
			// don't save if this is an auto save
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
					return;
	
			// don't save if the function is called for saving revision.
			if ( $post->post_type == 'revision' )
					return $post_id;
	
	    if ( !isset( $_POST['sdf_post_nonce'] ) || !wp_verify_nonce( $_POST['sdf_post_nonce'], basename( __FILE__ ) ) )
        return $post_id;


			if ($post->post_type == 'page') {
				if ( !current_user_can( 'edit_page', $post_id )) return $post_id;
			} elseif ( $post->post_type == 'post') {
				if ( !current_user_can( 'edit_post', $post_id )) return $post_id;
			} elseif ( $post->post_type == 'portfolio') {
				if ( !current_user_can( 'edit_post', $post_id )) return $post_id;
			} elseif ( $post->post_type == 'product') {
				if ( !current_user_can( 'edit_post', $post_id )) return $post_id;
			} else {
				return $post_id;
			}
		

			// Featured Image Toggle
			$toggle_single_featured_image = '';
			if ( isset($_POST["toggle_single_featured_image"]) )
			$toggle_single_featured_image = $_POST["toggle_single_featured_image"];
			if(!$toggle_single_featured_image) $toggle_single_featured_image = "0";
			$this->_currentSettings['blog']["toggle_single_featured_image"] = $toggle_single_featured_image;
			
			$toggle_page_featured_image = '';
			if ( isset($_POST["toggle_page_featured_image"]) )
			$toggle_page_featured_image = $_POST["toggle_page_featured_image"];
			if(!$toggle_page_featured_image) $toggle_page_featured_image = "0";
			$this->_currentSettings['blog']["toggle_page_featured_image"] = $toggle_page_featured_image;
			
			// Comments Toggle
			$toggle_single_comments = '';
			if ( isset($_POST["toggle_single_comments"]) )
			$toggle_single_comments = $_POST["toggle_single_comments"];
			if(!$toggle_single_comments) $toggle_single_comments = "0";
			$this->_currentSettings['blog']["toggle_single_comments"] = $toggle_single_comments;
			
			$toggle_page_comments = '';
			if ( isset($_POST["toggle_page_comments"]) )
			$toggle_page_comments = $_POST["toggle_page_comments"];
			if(!$toggle_page_comments) $toggle_page_comments = "0";
			$this->_currentSettings['blog']["toggle_page_comments"] = $toggle_page_comments;
			
			$toggle_portfolio_comments = '';
			if ( isset($_POST["toggle_portfolio_comments"]) )
			$toggle_portfolio_comments = $_POST["toggle_portfolio_comments"];
			if(!$toggle_portfolio_comments) $toggle_portfolio_comments = "0";
			$this->_currentSettings['blog']["toggle_portfolio_comments"] = $toggle_portfolio_comments;
			
			// Related posts
			$toggle_related_posts = '';
			if ( isset($_POST["toggle_related_posts"]) )
			$toggle_related_posts = $_POST["toggle_related_posts"];
			if(!$toggle_related_posts) $toggle_related_posts = "0";
			$this->_currentSettings['blog']["toggle_related_posts"] = $toggle_related_posts;
			
			// Featured Image Animation
			$feat_img_animate = '';
			if ( isset($_POST["feat_img_animate"]) )
			$feat_img_animate = $_POST["feat_img_animate"];
			if(!$feat_img_animate) $feat_img_animate = "0";
			$this->_currentSettings['animations']["feat_img_animate"] = $feat_img_animate;
			
			if ($post->post_type == 'portfolio_TODO') {
				// Portfolio
				$portfolio_type = $_POST["sdf_theme_page_portfolio_type"];
				if(!$portfolio_type) $portfolio_type = "single_image";
				$this->_currentSettings['portfolio']['portfolio_type'] = $portfolio_type;
			}
			// Page Layout Settings
			$pageLayout = ( isset($_POST["sdf_theme_layout_page"]) ) ? $_POST["sdf_theme_layout_page"] : 'no_col';
			$this->_currentSettings['layout']['theme_layout'] = $pageLayout;
			$this->_currentSettings['layout']['custom_left'] = $_POST['sdf_theme_custom_left'];
			$this->_currentSettings['layout']['custom_center'] = $_POST['sdf_theme_custom_center'];
			$this->_currentSettings['layout']['custom_right'] = $_POST['sdf_theme_custom_right'];
			// Page Title Settings
			$titleAnchor = $_POST["theme_toggleTitle"];
			if(!$titleAnchor) $titleAnchor = "0";
			$this->_currentSettings['title']["toggle_page_title"] = $titleAnchor;

			$titleHeader = $_POST["theme_titleHeader"];
			$this->_currentSettings['title']["title_header"] = $titleHeader;

			$customLink = $_POST["theme_toggleTitleAnchor"];
			$this->_currentSettings['title']["toggle_custom_title"] = $customLink;

			$customLink = $_POST["theme_toggleCustomLink"];
			$this->_currentSettings['title']["title_custom_link"] = $customLink;
			$customTest = $_POST["theme_toggleCustomText"];
			$this->_currentSettings['title']["title_custom_text"] = $customTest;
			
			// Page Subtitle Settings
			$subtitleHeader = $_POST["theme_subtitleHeader"];
			$this->_currentSettings['title']["subtitle_header"] = $subtitleHeader;
			$subtitleText = $_POST["theme_subtitleText"];
			$this->_currentSettings['title']["subtitle_text"] = $subtitleText;
			// Page Layout Settings
			$pageLayoutTog= $_POST['sdf_toggle_page_layout'];
			if(!$pageLayoutTog) $pageLayoutTog = "0";
			$this->_currentSettings['layout']['toggle_page_layout'] = $pageLayoutTog;
						
			$pageWidth = $_POST["sdf_theme_page_width"];
			$this->_currentSettings['layout']['page_width'] = $pageWidth;
			// Page Layout Settings
			$sidebarWidth = $_POST["sdf_page_sidebars_width"];
			$this->_currentSettings['layout']['page_sidebars'] = $sidebarWidth;
			
			// Disable Navigation on Page
			$disableNav = $_POST["theme_toggle_nav"];
			if(!$disableNav ) $disableNav = "0";
			$this->_currentSettings['header']["toggle_nav"] = $disableNav;
			// Custom Page Navigation
			$temp_nav = $_POST["theme_pageNav"];
			if(!$temp_nav) $temp_nav = "0";
			$this->_currentSettings['header']["toggle_custom_nav"] = $temp_nav;
			$temp_nav_name = $_POST["theme_pageNavName"];
			$this->_currentSettings['header']["custom_nav_name"] = $temp_nav_name;
			
			// Custom Page Header
			$temp_header = $_POST["theme_pageHeader"];
			if(!$temp_header) $temp_header = "0";
			$this->_currentSettings['header']["toggle_custom_header"] = $temp_header;
			// save if necessary
				unset($header_data);
				$header_data = array();
				$header_data = array_map( 'trim', explode(',', $_POST['sdf_theme_header_order']) );
				$this->_currentSettings['header']['header_order'] = ($temp_header != "0") ? $header_data : null;
				// box keys/sizes in order!
				unset($box_data);
				$box_data = array_filter( array_combine( $_POST['block-builder-box-id'], $_POST['block-builder-box-size'] ));
				$this->_currentSettings['header']['box_data'] = ($temp_header != "0") ? $box_data : null;
			// additional block classes
				$this->_currentSettings['header']['box_data_classes']['listItem_1'] = $_POST['sdf_theme_header_box_data_classes_header_block_1'];
				$this->_currentSettings['header']['box_data_classes']['listItem_2'] = $_POST['sdf_theme_header_box_data_classes_header_block_2'];
				$this->_currentSettings['header']['box_data_classes']['listItem_3'] = $_POST['sdf_theme_header_box_data_classes_header_block_3'];
				$this->_currentSettings['header']['box_data_classes']['listItem_4'] = $_POST['sdf_theme_header_box_data_classes_header_block_4'];
				$this->_currentSettings['header']['box_data_classes']['listItem_5'] = $_POST['sdf_theme_header_box_data_classes_header_block_5'];
				$this->_currentSettings['header']['box_data_classes']['listItem_6'] = $_POST['sdf_theme_header_box_data_classes_header_block_6'];
				$this->_currentSettings['header']['box_data_classes']['listItem_7'] = $_POST['sdf_theme_header_box_data_classes_header_block_7'];
				$this->_currentSettings['header']['box_data_classes']['listItem_8'] = $_POST['sdf_theme_header_box_data_classes_header_block_8'];
					
			// Page Level Social Icons
			$disableSocial = $_POST["theme_toggleSocialIcons"];
			if(!$disableSocial) $disableSocial = "0";
			$this->_currentSettings['social_media']["toggle_social_share"] = $disableSocial;
			$socialPosition = $_POST["theme_social_share_position"];
			if(!$socialPosition) $socialPosition = "0";
			$this->_currentSettings['social_media']["social_share_position"] = $socialPosition;
			
			// Page Level Breadcrumbs
			$disableBread = $_POST["theme_toggleBreadcrumbs"];
			if(!$disableBread) $disableBread = "0";
			$this->_currentSettings['breadcrumbs']["toggle_breadcrumbs"] = $disableBread ;
			
			$currentBread = $_POST['sdf_theme_bread_text'];
			$this->_currentSettings['breadcrumbs']['bread_current_text'] = $currentBread;
			// Custom JS
			$this->_currentSettings["misc"]["custom_js_header"] = str_replace( array("\n", "\r\n", "\r"), " ", $_POST['sdf_theme_custom_js_header'] );
			$this->_currentSettings["misc"]["custom_js_footer"] = str_replace( array("\n", "\r\n", "\r"), " ", $_POST['sdf_theme_custom_js_footer'] );
			$this->_currentSettings["misc"]["yt_bg_video_url"] = sanitize_text_field($_POST['sdf_theme_yt_bg_video_url']);
			$this->_currentSettings["misc"]["yt_bg_video_mute"] = isset($_POST['sdf_theme_yt_bg_video_mute']) ? $_POST['sdf_theme_yt_bg_video_mute'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_volume"] = isset($_POST['sdf_theme_yt_bg_video_volume']) ? $_POST['sdf_theme_yt_bg_video_volume'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_player_controls"] = isset($_POST['sdf_theme_yt_bg_video_player_controls']) ? $_POST['sdf_theme_yt_bg_video_player_controls'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_volume_button"] = isset($_POST['sdf_theme_yt_bg_video_volume_button']) ? $_POST['sdf_theme_yt_bg_video_volume_button'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_play_button"] = isset($_POST['sdf_theme_yt_bg_video_play_button']) ? $_POST['sdf_theme_yt_bg_video_play_button'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_opacity"] = isset($_POST['sdf_theme_yt_bg_video_opacity']) ? $_POST['sdf_theme_yt_bg_video_opacity'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_ratio"] = isset($_POST['sdf_theme_yt_bg_video_ratio']) ? $_POST['sdf_theme_yt_bg_video_ratio'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_quality"] = isset($_POST['sdf_theme_yt_bg_video_quality']) ? $_POST['sdf_theme_yt_bg_video_quality'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_autoplay"] = isset($_POST['sdf_theme_yt_bg_video_autoplay']) ? $_POST['sdf_theme_yt_bg_video_autoplay'] : "";
			$this->_currentSettings["misc"]["yt_bg_video_logo"] = isset($_POST['sdf_theme_yt_bg_video_logo']) ? $_POST['sdf_theme_yt_bg_video_logo'] : "";
			
			// Timed Lightbox 
			$sdf_timed_lightbox_page = isset($_POST['sdf_timed_lightbox_page']) ? $_POST['sdf_timed_lightbox_page'] : "";
			$sdf_timed_lightbox_seconds = isset($_POST['sdf_timed_lightbox_seconds']) ? $_POST['sdf_timed_lightbox_seconds'] : "";
			$sdf_timed_lightbox_size = isset($_POST['sdf_timed_lightbox_size']) ? $_POST['sdf_timed_lightbox_size'] : "";
			$sdf_timed_lightbox_animation = isset($_POST['sdf_timed_lightbox_animation']) ? $_POST['sdf_timed_lightbox_animation'] : "";
			$sdf_timed_lightbox_position = isset($_POST['sdf_timed_lightbox_position']) ? $_POST['sdf_timed_lightbox_position'] : "";
			$timedCount = count($sdf_timed_lightbox_page)-1;
										
			for($i=0; $i<$timedCount; $i++){
				if( is_null( $sdf_timed_lightbox_page[$i] ) || $sdf_timed_lightbox_page[$i] == "" ) {
					unset($sdf_timed_lightbox_page[$i]);  
					unset($sdf_timed_lightbox_seconds[$i]);  
					unset($sdf_timed_lightbox_size[$i]);  
					unset($sdf_timed_lightbox_animation[$i]);  
					unset($sdf_timed_lightbox_position[$i]);  
				}
			}
			
			$this->_currentSettings['lightbox']["timed_lightbox_page"] = array_values( $sdf_timed_lightbox_page );
			$this->_currentSettings['lightbox']["timed_lightbox_seconds"] = array_values( $sdf_timed_lightbox_seconds );
			$this->_currentSettings['lightbox']["timed_lightbox_size"] = array_values( $sdf_timed_lightbox_size );
			$this->_currentSettings['lightbox']["timed_lightbox_animation"] = array_values( $sdf_timed_lightbox_animation );
			$this->_currentSettings['lightbox']["timed_lightbox_position"] = array_values( $sdf_timed_lightbox_position );
			
			// Bottom Lightbox
			$this->_currentSettings['lightbox']["bottom_lightbox_page"] = isset($_POST['sdf_bottom_lightbox_page']) ? $_POST['sdf_bottom_lightbox_page'] : "";
			$this->_currentSettings['lightbox']["bottom_lightbox_animation"] = isset($_POST['sdf_bottom_lightbox_animation']) ? $_POST['sdf_bottom_lightbox_animation'] : "";
			$this->_currentSettings['lightbox']["bottom_lightbox_size"] = isset($_POST['sdf_bottom_lightbox_size']) ? $_POST['sdf_bottom_lightbox_size'] : "";
			$this->_currentSettings['lightbox']["bottom_lightbox_position"] = isset($_POST['sdf_bottom_lightbox_position']) ? $_POST['sdf_bottom_lightbox_position'] : "";
			
			// Page Padding Top/Bottom
			$this->_currentSettings['typography']["page_padding_top"] = isset($_POST['sdf_page_padding_top']) ? $_POST['sdf_page_padding_top'] : "";
			$this->_currentSettings['typography']["page_padding_bottom"] = isset($_POST['sdf_page_padding_bottom']) ? $_POST['sdf_page_padding_bottom'] : ""; 
			
			// Style Preset
			$themePreset = $_POST['sdf_theme_style_preset'];
			$this->_currentSettings['preset']['theme_preset'] = $themePreset;

			// Page Level Footer
			$toggleFooter = $_POST["sdf_theme_toggleFooter"];
			if(!$toggleFooter) $toggleFooter = "0";
			$this->_currentSettings['footer']['toggle_footer'] = $toggleFooter;		

			$toggleCustom = ( isset($_POST["theme_customFooter"]) ) ? $_POST["theme_customFooter"] : '';
			if(!$toggleCustom) $toggleCustom = "0";
			$this->_currentSettings['footer']["toggle_custom_footer"] = $toggleCustom;	
			$temp_foot_nav_name = ( isset($_POST["theme_pagefootNavName"]) ) ? $_POST["theme_pagefootNavName"] : '';
			$this->_currentSettings['footer']["custom_nav_name"] = $temp_foot_nav_name;
			//$customFooter = htmlspecialchars($_POST["sdf_page_footer"], ENT_QUOTES);
			if ( current_user_can('unfiltered_html') )
				$customFooter =  $_POST["sdf_page_footer"];
			else
				$customFooter = stripslashes( wp_filter_post_kses( addslashes($_POST["sdf_page_footer"]) ) ); // wp_filter_post_kses() expects slashed
				
			$this->_currentSettings['footer']['copyright_footer'] = str_replace( array("\n", "\r\n", "\r"), " ", $customFooter );

			// Add or Update the meta field
			if ( ! update_post_meta ($post_id, SDF_META_KEY,  $this->_currentSettings) ) { 
				add_post_meta($post_id, SDF_META_KEY,  $this->_currentSettings, true );	
			}; 
				
				return $post_id;
		}
		
		
		
	}
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
