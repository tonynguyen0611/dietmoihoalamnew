<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: styles.php
 * DESCRIPTIONS: Theme Styles Options.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: ULTSlider
 * Descripton: Implementation of Theme Slider Options    
 */

function sdf_tbs_styles() {
	
	if (!empty($_REQUEST['nonce']))
	{
		if ( !wp_verify_nonce( $_REQUEST['nonce'], "nonce_css_tbs")) {
			exit("No naughty business please");
		}

		header("Content-type: text/css; charset=utf-8");
		header("Cache-Control: must-revalidate");

		$exp_str = "Expires: " . gmdate("D, d M Y H:i:s", time() + 3600) . " GMT";
		header($exp_str);
	}

	$tbs_custom_css = sdfGo()->_wpuGlobal->getCustomLESS();
	echo $tbs_custom_css;

}


function sdf_get_page_styles($post_id){
	
	$custom_style = array();
	$custom_style['theme_layout_page'] = '';
	if(!is_attachment()) {
		$tmpRight = '';
		$tmpCenter = '';
		$tmpLeft = '';
		
		$pageLayout = '';
		$globalLayout = '';
		$wpuPageArgs = get_post_meta( $post_id, SDF_META_KEY, true );
		$pageLevel =  (isset($wpuPageArgs['layout']['toggle_page_layout'])) ? $wpuPageArgs['layout']['toggle_page_layout'] : '0';
		if($pageLevel == '1'){
			$pageLayout = $wpuPageArgs['layout']['theme_layout'];
			$custom_style['theme_page_width'] = sdfGo()->sdf_get_option('layout','page_width');
		}
		else {
			$globalLayout =  sdfGo()->sdf_get_global('layout','theme_layout');
			$custom_style['theme_page_width'] = sdfGo()->sdf_get_global('layout','page_width');
		}
		
		if($globalLayout == 'custom'){
			$tmpRight = sdfGo()->sdf_get_global('layout','custom_right');
			$tmpCenter = sdfGo()->sdf_get_global('layout','custom_center');
			$tmpLeft = sdfGo()->sdf_get_global('layout','custom_left');
			$custom_style['theme_layout_page'] = 'custom';	
		} else if($pageLayout == 'custom'){
			$tmpRight = $wpuPageArgs['layout']['custom_right'];
			$tmpCenter = $wpuPageArgs['layout']['custom_center'];
			$tmpLeft = $wpuPageArgs['layout']['custom_left'];	
			$custom_style['theme_layout_page'] = 'custom';		
		}
		
		if( $custom_style['theme_layout_page'] == 'custom' ) { 
			$custom_style['theme_custom_left'] = $tmpLeft;
			$custom_style['theme_custom_center'] = $tmpCenter;
			$custom_style['theme_custom_right'] = $tmpRight;
		}
		
		$custom_style['header_custom_bg'] = sdfGo()->sdf_get_global('typography','header_custom_bg');
	}
	
	
	
	// necessary typography and navigation styles
	$custom_style['theme_font_wide_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_wide_bg_color'));
	$custom_style['theme_font_wide_bg_image'] = sdfGo()->sdf_get_global('typography','theme_font_wide_bg_image');
	$custom_style['theme_font_wide_bg_repeat'] = sdfGo()->sdf_get_global('typography','theme_font_wide_bg_repeat');
	$custom_style['theme_font_boxed_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_bg_color'));
	$custom_style['theme_font_boxed_bg_image'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_bg_image');
	$custom_style['theme_font_boxed_bg_repeat'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_bg_repeat');
	$custom_style['theme_font_boxed_box_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_box_bg_color'));
	$custom_style['theme_font_boxed_box_bg_image'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_bg_image');
	$custom_style['theme_font_boxed_box_bg_repeat'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_bg_repeat');
	$custom_style['theme_font_boxed_box_margin'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_margin');
	$custom_style['theme_font_boxed_box_padding'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_padding');
	$custom_style['theme_font_boxed_box_shadow'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_shadow');
	$custom_style['theme_font_boxed_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_box_shadow_color'));
	$custom_style['theme_font_boxed_box_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( sdfGo()->sdf_get_global('typography','theme_font_boxed_box_border_color'));
	$custom_style['theme_font_boxed_box_border_width'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_border_width');
	$custom_style['theme_font_boxed_box_border_width_type'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_border_width_type');
	$custom_style['theme_font_boxed_box_border_style'] = sdfGo()->sdf_get_global('typography','theme_font_boxed_box_border_style');
	$custom_style['page_padding_top'] = sdfGo()->sdf_get_option('typography','page_padding_top');
	$custom_style['page_padding_bottom'] = sdfGo()->sdf_get_option('typography','page_padding_bottom');
	
	// navigation settings
	$theme_menu_settings = sdfGo()->sdf_get_global('navigation','menu_settings');
	
	foreach ($theme_menu_settings as $menu_color_key => $menu_color_val) {
		// check if menu colors are set as color preset names and convert to hex color
		if(in_array($menu_color_key, sdfGo()->_wpuGlobal->_menuColorPickers)) {
			$custom_style[$menu_color_key] = sdfGo()->_wpuGlobal->sdf_get_picked_color($menu_color_val, !in_array($menu_color_key, sdfGo()->_wpuGlobal->_menuNonTransparentPickers));
		}
		else {
			$custom_style[$menu_color_key] = $menu_color_val;
		}
	}
	
	return $custom_style;
}


function sdf_get_typography_styles($presetOptionsArr = array()){

	$presetOptionsArr = (is_array($presetOptionsArr) && count($presetOptionsArr)) ? $presetOptionsArr : sdfGo()->_wpuGlobal->_currentSettings;
	
	$custom_style = array();
	$custom_style['theme_tbs_border_radius_base'] = sdfGo()->sdf_get_global('typography','theme_tbs_border_radius_base');
	$custom_style['theme_tbs_grid_float_breakpoint'] = sdfGo()->sdf_get_global('typography','theme_tbs_grid_float_breakpoint');
	$custom_style['theme_tbs_grid_float_breakpoint'] = (strpos($custom_style['theme_tbs_grid_float_breakpoint'], 'px')) ? $custom_style['theme_tbs_grid_float_breakpoint'] : '768px';
	$custom_style['theme_tbs_grid_float_breakpoint_max'] = sdfGo()->sdf_get_global('typography','theme_tbs_grid_float_breakpoint_max');
	$custom_style['theme_tbs_grid_float_breakpoint_max'] = (strpos($custom_style['theme_tbs_grid_float_breakpoint_max'], 'px')) ? $custom_style['theme_tbs_grid_float_breakpoint_max'] : '767px';
	$custom_style['theme_page_width'] = sdfGo()->sdf_get_global('layout','page_width');
	$custom_style['theme_font_wide_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_wide_bg_color']);
	$custom_style['theme_font_wide_bg_image'] = $presetOptionsArr['typography']['theme_font_wide_bg_image'];
	$custom_style['theme_font_wide_bg_repeat'] = $presetOptionsArr['typography']['theme_font_wide_bg_repeat'];
	$custom_style['theme_font_boxed_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_boxed_bg_color']);
	$custom_style['theme_font_boxed_bg_image'] = $presetOptionsArr['typography']['theme_font_boxed_bg_image'];
	$custom_style['theme_font_boxed_bg_repeat'] = $presetOptionsArr['typography']['theme_font_boxed_bg_repeat'];
	$custom_style['theme_font_boxed_box_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_boxed_box_bg_color']);
	$custom_style['theme_font_boxed_box_bg_image'] = $presetOptionsArr['typography']['theme_font_boxed_box_bg_image'];
	$custom_style['theme_font_boxed_box_bg_repeat'] = $presetOptionsArr['typography']['theme_font_boxed_box_bg_repeat'];
	$custom_style['theme_font_boxed_box_margin'] = $presetOptionsArr['typography']['theme_font_boxed_box_margin'];
	$custom_style['theme_font_boxed_box_padding'] = $presetOptionsArr['typography']['theme_font_boxed_box_padding'];
	$custom_style['theme_font_boxed_box_shadow'] = $presetOptionsArr['typography']['theme_font_boxed_box_shadow'];
	$custom_style['theme_font_boxed_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_boxed_box_shadow_color']);
	$custom_style['theme_font_boxed_box_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_boxed_box_border_color']);
	$custom_style['theme_font_boxed_box_border_width'] = $presetOptionsArr['typography']['theme_font_boxed_box_border_width'];
	$custom_style['theme_font_boxed_box_border_width_type'] = $presetOptionsArr['typography']['theme_font_boxed_box_border_width_type'];
	$custom_style['theme_font_boxed_box_border_style'] = $presetOptionsArr['typography']['theme_font_boxed_box_border_style'];

	$custom_style['theme_font_link_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_color'], false );
	$custom_style['theme_font_link_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_bg_color']);
	$custom_style['theme_font_link_text_decoration'] = $presetOptionsArr['typography']['theme_font_link_text_decoration'];
	$custom_style['theme_font_link_box_shadow'] = $presetOptionsArr['typography']['theme_font_link_box_shadow'];
	$custom_style['theme_font_link_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_box_shadow_color']);
	$custom_style['theme_font_link_transition'] = $presetOptionsArr['typography']['theme_font_link_transition'];
	$custom_style['theme_font_link_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_hover_color'], false );
	$custom_style['theme_font_link_hover_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_hover_bg_color']);
	$custom_style['theme_font_link_hover_text_decoration'] = $presetOptionsArr['typography']['theme_font_link_hover_text_decoration'];
	$custom_style['theme_font_link_hover_box_shadow'] = $presetOptionsArr['typography']['theme_font_link_hover_box_shadow'];
	$custom_style['theme_font_link_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_link_hover_box_shadow_color']);
	
	$custom_style['theme_silo_font_size'] = $presetOptionsArr['typography']['theme_silo_font_size'];
	$custom_style['theme_silo_font_size_type'] = $presetOptionsArr['typography']['theme_silo_font_size_type'];
	$custom_style['theme_silo_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_silo_font_color'], false );
	$custom_style['theme_silo_line_height'] = $presetOptionsArr['typography']['theme_silo_line_height'];
				
	$custom_style['theme_font_size'] = ($presetOptionsArr['typography']['theme_font_size']) ? $presetOptionsArr['typography']['theme_font_size'] : '14px';
	$custom_style['theme_font_family'] = $presetOptionsArr['typography']['theme_font_family'];
	$custom_style['theme_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_color'], false );
				
	$custom_style['theme_font_subtitle_size'] = $presetOptionsArr['typography']['theme_font_subtitle_size'];
	$custom_style['theme_font_subtitle_family'] = $presetOptionsArr['typography']['theme_font_subtitle_family'];
	$custom_style['theme_font_subtitle_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_subtitle_color'], false );
	$custom_style['theme_font_subtitle_size_type'] = $presetOptionsArr['typography']['theme_font_subtitle_size_type'];
	
	// default list styling
	$custom_style['theme_font_list_padding'] = $presetOptionsArr['typography']['theme_font_list_padding'];
	$custom_style['theme_font_list_margin'] = $presetOptionsArr['typography']['theme_font_list_margin'];
	$custom_style['theme_font_list_item_line_height'] = $presetOptionsArr['typography']['theme_font_list_item_line_height'];
	$custom_style['theme_font_list_style'] = $presetOptionsArr['typography']['theme_font_list_style'];
	// custom list styling
	$custom_style['theme_font_custom_list_styling'] = $presetOptionsArr['typography']['theme_font_custom_list_styling'];
	$custom_style['theme_font_custom_list_align'] = $presetOptionsArr['typography']['theme_font_custom_list_align'];
	$custom_style['theme_font_custom_list_padding'] = $presetOptionsArr['typography']['theme_font_custom_list_padding'];
	$custom_style['theme_font_custom_list_margin'] = $presetOptionsArr['typography']['theme_font_custom_list_margin'];
	$custom_style['theme_font_custom_list_font_size'] = $presetOptionsArr['typography']['theme_font_custom_list_font_size'];
	$custom_style['theme_font_custom_list_font_size_type'] = $presetOptionsArr['typography']['theme_font_custom_list_font_size_type'];
	$custom_style['theme_font_custom_list_family'] = $presetOptionsArr['typography']['theme_font_custom_list_family'];
	$custom_style['theme_font_custom_list_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_font_color'], false );
	$custom_style['theme_font_custom_list_link_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_link_color'], false );
	$custom_style['theme_font_custom_list_link_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_link_hover_color'], false );
	$custom_style['theme_font_custom_list_link_text_decoration'] = $presetOptionsArr['typography']['theme_font_custom_list_link_text_decoration'];
	$custom_style['theme_font_custom_list_link_hover_text_decoration'] = $presetOptionsArr['typography']['theme_font_custom_list_link_hover_text_decoration'];
	
	$custom_style['theme_font_custom_list_style'] = $presetOptionsArr['typography']['theme_font_custom_list_style'];
	$custom_style['theme_font_custom_list_icon'] = $presetOptionsArr['typography']['theme_font_custom_list_icon'];
	$custom_style['theme_font_custom_list_icon_size'] = $presetOptionsArr['typography']['theme_font_custom_list_icon_size'];
	$custom_style['theme_font_custom_list_icon_size_type'] = $presetOptionsArr['typography']['theme_font_custom_list_icon_size_type'];
	$custom_style['theme_font_custom_list_icon_margin'] = $presetOptionsArr['typography']['theme_font_custom_list_icon_margin'];
	
	$custom_style['theme_font_custom_list_item_padding'] = $presetOptionsArr['typography']['theme_font_custom_list_item_padding'];
	$custom_style['theme_font_custom_list_item_margin'] = $presetOptionsArr['typography']['theme_font_custom_list_item_margin'];
	$custom_style['theme_font_custom_list_item_line_height'] = $presetOptionsArr['typography']['theme_font_custom_list_item_line_height'];
	$custom_style['theme_font_custom_list_item_border_width'] = $presetOptionsArr['typography']['theme_font_custom_list_item_border_width'];
	$custom_style['theme_font_custom_list_item_border_style'] = $presetOptionsArr['typography']['theme_font_custom_list_item_border_style'];
	$custom_style['theme_font_custom_list_item_border_radius'] = $presetOptionsArr['typography']['theme_font_custom_list_item_border_radius'];
	$custom_style['theme_font_custom_list_item_background_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_background_color'] );
	$custom_style['theme_font_custom_list_item_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_border_color'] );
	$custom_style['theme_font_custom_list_item_box_shadow'] = $presetOptionsArr['typography']['theme_font_custom_list_item_box_shadow'];
	$custom_style['theme_font_custom_list_item_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_box_shadow_color'] );
	$custom_style['theme_font_custom_list_item_background_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_background_hover_color'] );
	$custom_style['theme_font_custom_list_item_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_border_hover_color'] );
	$custom_style['theme_font_custom_list_item_hover_box_shadow'] = $presetOptionsArr['typography']['theme_font_custom_list_item_hover_box_shadow'];
	$custom_style['theme_font_custom_list_item_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_custom_list_item_hover_box_shadow_color'] );
	
	// headings styling
	foreach (array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ) as $id) {	
		$custom_style['theme_font_'.$id.'_letter_spacing'] = $presetOptionsArr['typography']['theme_font_'.$id.'_letter_spacing'];
		$custom_style['theme_font_'.$id.'_size'] = $presetOptionsArr['typography']['theme_font_'.$id.'_size'];
		$custom_style['theme_font_'.$id.'_weight'] = $presetOptionsArr['typography']['theme_font_'.$id.'_weight'];
		$custom_style['theme_font_'.$id.'_size_type'] = $presetOptionsArr['typography']['theme_font_'.$id.'_size_type'];
		$custom_style['theme_font_'.$id.'_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_font_'.$id.'_color'], false );
		$custom_style['theme_font_'.$id.'_family'] = $presetOptionsArr['typography']['theme_font_'.$id.'_family'];
		$custom_style['theme_font_'.$id.'_margin_top'] = $presetOptionsArr['typography']['theme_font_'.$id.'_margin_top'];
		$custom_style['theme_font_'.$id.'_margin_bottom'] = $presetOptionsArr['typography']['theme_font_'.$id.'_margin_bottom'];
	}
	
	// default, tabs, pills...
	$custom_style['theme_menu_style'] = $presetOptionsArr['navigation']['menu_style'];
	$custom_style['theme_menu_layout'] = $presetOptionsArr['navigation']['menu_layout'];
	$custom_style['theme_sticky_menu_opacity'] = $presetOptionsArr['navigation']['sticky_menu_opacity'];
	$custom_style['theme_menu_alignment'] = $presetOptionsArr['navigation']['menu_alignment'];
	
	// navigation settings
	$theme_menu_settings = $presetOptionsArr['navigation']['menu_settings'];
	
	foreach ($theme_menu_settings as $menu_color_key => $menu_color_val) {
		// check if menu colors are set as color preset names and convert to hex color
		if(in_array($menu_color_key, sdfGo()->_wpuGlobal->_menuColorPickers)) {
			$custom_style[$menu_color_key] = sdfGo()->_wpuGlobal->sdf_get_picked_color($menu_color_val, !in_array($menu_color_key, sdfGo()->_wpuGlobal->_menuNonTransparentPickers));
		}
		else {
			$custom_style[$menu_color_key] = $menu_color_val;
		}
	}
	
	$custom_style['logo_align'] = $presetOptionsArr['typography']['logo_align'];
	$custom_style['logo_padding_top'] = $presetOptionsArr['typography']['logo_padding_top'];
	$custom_style['logo_padding_bottom'] = $presetOptionsArr['typography']['logo_padding_bottom'];
	$custom_style['logo_tagline_margin'] = $presetOptionsArr['typography']['logo_tagline_margin'];
	$custom_style['logo_tagline_heading'] = $presetOptionsArr['typography']['logo_tagline_heading'];
	$custom_style['logo_tagline_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['logo_tagline_font_color'], false );
	$custom_style['logo_tagline_font_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['logo_tagline_font_hover_color'], false );
	$custom_style['logo_tagline_font_size'] = $presetOptionsArr['typography']['logo_tagline_font_size'];
	$custom_style['logo_tagline_font_weight'] = $presetOptionsArr['typography']['logo_tagline_font_weight'];
	$custom_style['logo_tagline_font_size_type'] = $presetOptionsArr['typography']['logo_tagline_font_size_type'];
	$custom_style['logo_tagline_font_family'] = $presetOptionsArr['typography']['logo_tagline_font_family'];
	
	$custom_style['page_padding_top'] = $presetOptionsArr['typography']['page_padding_top'];
	$custom_style['page_padding_bottom'] = $presetOptionsArr['typography']['page_padding_bottom'];
	
	$custom_style['featured_image_padding_top'] = $presetOptionsArr['typography']['featured_image_padding_top'];
	$custom_style['featured_image_padding_bottom'] = $presetOptionsArr['typography']['featured_image_padding_bottom'];
	
	$custom_style['footer_bottom_alignment'] = $presetOptionsArr['footer']['footer_bottom_alignment'];
	$custom_style['breadcrumbs_alignment'] = $presetOptionsArr['typography']['breadcrumbs_alignment'];
	$custom_style['breadcrumbs_padding_top'] = $presetOptionsArr['typography']['breadcrumbs_padding_top'];
	$custom_style['breadcrumbs_padding_bottom'] = $presetOptionsArr['typography']['breadcrumbs_padding_bottom'];
	$custom_style['breadcrumbs_text_transform'] = $presetOptionsArr['typography']['breadcrumbs_text_transform'];
	
	// background styling
	foreach (array( 'header', 'logo', 'footer', 'footer_bottom', 'page_header', 'breadcrumbs', 'blockquote' ) as $id) {
		$custom_style[$id.'_custom_bg'] = $presetOptionsArr['typography'][$id.'_custom_bg'];
		$custom_style[$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_bg_color']);
		$custom_style[$id.'_bg_image'] = $presetOptionsArr['typography'][$id.'_bg_image'];
		$custom_style[$id.'_bgrepeat'] = $presetOptionsArr['typography'][$id.'_bgrepeat'];
		$custom_style[$id.'_box_shadow'] = $presetOptionsArr['typography'][$id.'_box_shadow'];
		$custom_style[$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_box_shadow_color']);
		$custom_style[$id.'_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_font_color'], false );
		$custom_style[$id.'_font_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_font_hover_color'], false );
		$custom_style[$id.'_link_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color ($presetOptionsArr['typography'][$id.'_link_color'], false );
		$custom_style[$id.'_link_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_link_hover_color'], false );
		$custom_style[$id.'_font_family'] = $presetOptionsArr['typography'][$id.'_font_family'];
		$custom_style[$id.'_font_size'] = $presetOptionsArr['typography'][$id.'_font_size'];
		$custom_style[$id.'_font_weight'] = $presetOptionsArr['typography'][$id.'_font_weight'];
		$custom_style[$id.'_font_size_type'] = $presetOptionsArr['typography'][$id.'_font_size_type'];
	}
	// page title
	$custom_style['page_header_link_text_decoration'] = $presetOptionsArr['typography']['page_header_link_text_decoration'];
	$custom_style['page_header_link_hover_text_decoration'] = $presetOptionsArr['typography']['page_header_link_hover_text_decoration'];
	$custom_style['page_header_padding_top'] = $presetOptionsArr['typography']['page_header_padding_top'];
	$custom_style['page_header_padding_bottom'] = $presetOptionsArr['typography']['page_header_padding_bottom'];
	$custom_style['page_header_font_text_transform'] = $presetOptionsArr['typography']['page_header_font_text_transform'];
	$custom_style['page_header_alignment'] = $presetOptionsArr['typography']['page_header_alignment'];
	$custom_style['page_header_font_subtitle_family'] = $presetOptionsArr['typography']['page_header_font_subtitle_family'];
	$custom_style['page_header_font_subtitle_size'] = $presetOptionsArr['typography']['page_header_font_subtitle_size'];
	$custom_style['page_header_font_subtitle_text_transform'] = $presetOptionsArr['typography']['page_header_font_subtitle_text_transform'];
	$custom_style['page_header_font_subtitle_weight'] = $presetOptionsArr['typography']['page_header_font_subtitle_weight'];
	$custom_style['page_header_font_subtitle_size_type'] = $presetOptionsArr['typography']['page_header_font_subtitle_size_type'];
	$custom_style['page_header_font_subtitle_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['page_header_font_subtitle_color'], false );
	
	
	$custom_style['entry_title_padding_top'] = $presetOptionsArr['typography']['entry_title_padding_top'];
	$custom_style['entry_title_padding_bottom'] = $presetOptionsArr['typography']['entry_title_padding_bottom'];
	
	// blockquote
	$custom_style['blockquote_link_text_decoration'] = $presetOptionsArr['typography']['blockquote_link_text_decoration'];
	$custom_style['blockquote_link_hover_text_decoration'] = $presetOptionsArr['typography']['blockquote_link_hover_text_decoration'];
	$custom_style['blockquote_padding'] = $presetOptionsArr['typography']['blockquote_padding'];
	$custom_style['blockquote_margin'] = $presetOptionsArr['typography']['blockquote_margin'];
	$custom_style['blockquote_font_text_transform'] = $presetOptionsArr['typography']['blockquote_font_text_transform'];
	$custom_style['blockquote_font_author_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['blockquote_font_author_color'], false );
	$custom_style['blockquote_font_author_family'] = $presetOptionsArr['typography']['blockquote_font_author_family'];
	$custom_style['blockquote_font_author_weight'] = $presetOptionsArr['typography']['blockquote_font_author_weight'];
	$custom_style['blockquote_font_author_size'] = $presetOptionsArr['typography']['blockquote_font_author_size'];
	$custom_style['blockquote_font_author_size_type'] = $presetOptionsArr['typography']['blockquote_font_author_size_type'];
	$custom_style['blockquote_font_author_text_transform'] = $presetOptionsArr['typography']['blockquote_font_author_text_transform'];
	$custom_style['blockquote_border_radius'] = $presetOptionsArr['typography']['blockquote_border_radius'];
	$custom_style['blockquote_border_width'] = $presetOptionsArr['typography']['blockquote_border_width'];
	$custom_style['blockquote_border_style'] = $presetOptionsArr['typography']['blockquote_border_style'];
	$custom_style['blockquote_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['blockquote_border_color'] );
	
	// post format buttons styling
	$id = 'post_format_button';
	$custom_style['theme_'.$id.'_icon_toggle'] = $presetOptionsArr['typography']['theme_'.$id.'_icon_toggle'];
	$custom_style['theme_'.$id.'_text_family'] = $presetOptionsArr['typography']['theme_'.$id.'_text_family'];
	$custom_style['theme_'.$id.'_text_size'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size'];
	$custom_style['theme_'.$id.'_text_size_type'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size_type'];
	$custom_style['theme_'.$id.'_text_weight'] = $presetOptionsArr['typography']['theme_'.$id.'_text_weight'];
	$custom_style['theme_'.$id.'_text_transform'] = $presetOptionsArr['typography']['theme_'.$id.'_text_transform'];
	$custom_style['theme_'.$id.'_border_width'] = $presetOptionsArr['typography']['theme_'.$id.'_border_width'];
	$custom_style['theme_'.$id.'_border_style'] = $presetOptionsArr['typography']['theme_'.$id.'_border_style'];
	$custom_style['theme_'.$id.'_border_radius'] = $presetOptionsArr['typography']['theme_'.$id.'_border_radius'];
	$custom_style['theme_'.$id.'_width'] = $presetOptionsArr['typography']['theme_'.$id.'_width'];
	$custom_style['theme_'.$id.'_height'] = $presetOptionsArr['typography']['theme_'.$id.'_height'];
	$custom_style['theme_'.$id.'_margin'] = $presetOptionsArr['typography']['theme_'.$id.'_margin'];
	
	$custom_style['theme_'.$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_color']);
	$custom_style['theme_'.$id.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_color']);
	$custom_style['theme_'.$id.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_hover_color']); 
	$custom_style['theme_'.$id.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_hover_color']); 
	$custom_style['theme_'.$id.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_color'], false );   
	$custom_style['theme_'.$id.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_shadow'];  
	$custom_style['theme_'.$id.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_hover_color'], false );  
	$custom_style['theme_'.$id.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_hover_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_box_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_box_shadow_color']);  
	$custom_style['theme_'.$id.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow'];  
	$custom_style['theme_'.$id.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow_color']);
	$custom_style['theme_'.$id.'_custom_style'] = $presetOptionsArr['typography']['theme_'.$id.'_custom_style'];  
		
	// custom
	foreach ( sdfGo()->_wpuGlobal->_sdfPostFormats as $format_name) {
		$id_var = $format_name.'_'.$id;
		
		$custom_style['theme_'.$id_var.'_custom_text'] = $presetOptionsArr['typography']['theme_'.$id_var.'_custom_text'];
		$custom_style['theme_'.$id_var.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_color']);
		$custom_style['theme_'.$id_var.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_color']);
		$custom_style['theme_'.$id_var.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_hover_color']); 
		$custom_style['theme_'.$id_var.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_hover_color']); 
		$custom_style['theme_'.$id_var.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_color'], false );   
		$custom_style['theme_'.$id_var.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_shadow'];  
		$custom_style['theme_'.$id_var.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_color'], false );  
		$custom_style['theme_'.$id_var.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow_color']);  
		$custom_style['theme_'.$id_var.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow'];  
		$custom_style['theme_'.$id_var.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow_color']); 
	}
	
	// follow buttons styling
	// common
	$id = 'follow_button';
	$custom_style['theme_'.$id.'_icon_toggle'] = $presetOptionsArr['typography']['theme_'.$id.'_icon_toggle'];
	$custom_style['theme_'.$id.'_text_family'] = $presetOptionsArr['typography']['theme_'.$id.'_text_family'];
	$custom_style['theme_'.$id.'_text_size'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size'];
	$custom_style['theme_'.$id.'_text_size_type'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size_type'];
	$custom_style['theme_'.$id.'_text_weight'] = $presetOptionsArr['typography']['theme_'.$id.'_text_weight'];
	$custom_style['theme_'.$id.'_text_transform'] = $presetOptionsArr['typography']['theme_'.$id.'_text_transform'];
	$custom_style['theme_'.$id.'_border_width'] = $presetOptionsArr['typography']['theme_'.$id.'_border_width'];
	$custom_style['theme_'.$id.'_border_style'] = $presetOptionsArr['typography']['theme_'.$id.'_border_style'];
	$custom_style['theme_'.$id.'_border_radius'] = $presetOptionsArr['typography']['theme_'.$id.'_border_radius'];
	$custom_style['theme_'.$id.'_width'] = $presetOptionsArr['typography']['theme_'.$id.'_width'];
	$custom_style['theme_'.$id.'_height'] = $presetOptionsArr['typography']['theme_'.$id.'_height'];
	$custom_style['theme_'.$id.'_margin'] = $presetOptionsArr['typography']['theme_'.$id.'_margin'];
	
	$custom_style['theme_'.$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_color']);
	$custom_style['theme_'.$id.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_color']);
	$custom_style['theme_'.$id.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_hover_color']); 
	$custom_style['theme_'.$id.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_hover_color']); 
	$custom_style['theme_'.$id.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_color'], false );   
	$custom_style['theme_'.$id.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_shadow'];  
	$custom_style['theme_'.$id.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_hover_color'], false );  
	$custom_style['theme_'.$id.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_hover_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_box_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_box_shadow_color']);  
	$custom_style['theme_'.$id.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow'];  
	$custom_style['theme_'.$id.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow_color']);
	$custom_style['theme_'.$id.'_custom_style'] = $presetOptionsArr['typography']['theme_'.$id.'_custom_style'];  
		
	// custom
	foreach ( sdfGo()->_wpuGlobal->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
		$id_var = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_'.$id;
		
		$custom_style['theme_'.$id_var.'_custom_text'] = $presetOptionsArr['typography']['theme_'.$id_var.'_custom_text'];
		$custom_style['theme_'.$id_var.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_color']);
		$custom_style['theme_'.$id_var.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_color']);
		$custom_style['theme_'.$id_var.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_hover_color']); 
		$custom_style['theme_'.$id_var.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_hover_color']); 
		$custom_style['theme_'.$id_var.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_color'], false );   
		$custom_style['theme_'.$id_var.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_shadow'];  
		$custom_style['theme_'.$id_var.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_color'], false );  
		$custom_style['theme_'.$id_var.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow_color']);  
		$custom_style['theme_'.$id_var.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow'];  
		$custom_style['theme_'.$id_var.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow_color']); 
	}
	
	// scroll to top button styling
	$custom_style['theme_scroll_to_top_button_icon_toggle'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_icon_toggle'];
	$custom_style['theme_scroll_to_top_button_icon'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_icon'];
	$custom_style['theme_scroll_to_top_button_icon_size'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_icon_size'];
	$custom_style['theme_scroll_to_top_button_custom_text'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_custom_text'];
	$custom_style['theme_scroll_to_top_button_text_family'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_family'];
	$custom_style['theme_scroll_to_top_button_text_size'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_size'];
	$custom_style['theme_scroll_to_top_button_text_size_type'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_size_type'];
	$custom_style['theme_scroll_to_top_button_text_weight'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_weight'];
	$custom_style['theme_scroll_to_top_button_text_transform'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_transform'];
	$custom_style['theme_scroll_to_top_button_border_width'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_border_width'];
	$custom_style['theme_scroll_to_top_button_border_style'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_border_style'];
	$custom_style['theme_scroll_to_top_button_border_radius'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_border_radius'];
	$custom_style['theme_scroll_to_top_button_width'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_width'];
	$custom_style['theme_scroll_to_top_button_height'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_height'];
	$custom_style['theme_scroll_to_top_button_right'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_right'];
	$custom_style['theme_scroll_to_top_button_bottom'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_bottom'];
	
	$custom_style['theme_scroll_to_top_button_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_bg_color']);
	$custom_style['theme_scroll_to_top_button_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_border_color']);
	$custom_style['theme_scroll_to_top_button_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_bg_hover_color']); 
	$custom_style['theme_scroll_to_top_button_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_border_hover_color']); 
	$custom_style['theme_scroll_to_top_button_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_text_color'], false );   
	$custom_style['theme_scroll_to_top_button_text_shadow'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_shadow'];  
	$custom_style['theme_scroll_to_top_button_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_text_hover_color'], false );  
	$custom_style['theme_scroll_to_top_button_text_hover_shadow'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_text_hover_shadow'];  
	$custom_style['theme_scroll_to_top_button_box_shadow'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_box_shadow'];  
	$custom_style['theme_scroll_to_top_button_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_box_shadow_color']);  
	$custom_style['theme_scroll_to_top_button_hover_box_shadow'] = $presetOptionsArr['typography']['theme_scroll_to_top_button_hover_box_shadow'];  
	$custom_style['theme_scroll_to_top_button_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_scroll_to_top_button_hover_box_shadow_color']); 
	
	// sharing buttons styling
	// common
	$id = 'sharing_button';
	$custom_style['theme_'.$id.'_icon_toggle'] = $presetOptionsArr['typography']['theme_'.$id.'_icon_toggle'];
	$custom_style['theme_'.$id.'_text_family'] = $presetOptionsArr['typography']['theme_'.$id.'_text_family'];
	$custom_style['theme_'.$id.'_text_size'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size'];
	$custom_style['theme_'.$id.'_text_size_type'] = $presetOptionsArr['typography']['theme_'.$id.'_text_size_type'];
	$custom_style['theme_'.$id.'_text_weight'] = $presetOptionsArr['typography']['theme_'.$id.'_text_weight'];
	$custom_style['theme_'.$id.'_text_transform'] = $presetOptionsArr['typography']['theme_'.$id.'_text_transform'];
	$custom_style['theme_'.$id.'_border_width'] = $presetOptionsArr['typography']['theme_'.$id.'_border_width'];
	$custom_style['theme_'.$id.'_border_style'] = $presetOptionsArr['typography']['theme_'.$id.'_border_style'];
	$custom_style['theme_'.$id.'_border_radius'] = $presetOptionsArr['typography']['theme_'.$id.'_border_radius'];
	$custom_style['theme_'.$id.'_width'] = $presetOptionsArr['typography']['theme_'.$id.'_width'];
	$custom_style['theme_'.$id.'_height'] = $presetOptionsArr['typography']['theme_'.$id.'_height'];
	$custom_style['theme_'.$id.'_margin'] = $presetOptionsArr['typography']['theme_'.$id.'_margin'];
	
	$custom_style['theme_'.$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_color']);
	$custom_style['theme_'.$id.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_color']);
	$custom_style['theme_'.$id.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_bg_hover_color']); 
	$custom_style['theme_'.$id.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_border_hover_color']); 
	$custom_style['theme_'.$id.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_color'], false );   
	$custom_style['theme_'.$id.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_shadow'];  
	$custom_style['theme_'.$id.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_text_hover_color'], false );  
	$custom_style['theme_'.$id.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_text_hover_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_box_shadow'];  
	$custom_style['theme_'.$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_box_shadow_color']);  
	$custom_style['theme_'.$id.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow'];  
	$custom_style['theme_'.$id.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_hover_box_shadow_color']);
	$custom_style['theme_'.$id.'_custom_style'] = $presetOptionsArr['typography']['theme_'.$id.'_custom_style'];  
		
	// custom
	foreach ( sdfGo()->_wpuGlobal->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
		$id_var = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_'.$id;
		
		$custom_style['theme_'.$id_var.'_custom_text'] = $presetOptionsArr['typography']['theme_'.$id_var.'_custom_text'];
		$custom_style['theme_'.$id_var.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_color']);
		$custom_style['theme_'.$id_var.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_color']);
		$custom_style['theme_'.$id_var.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_bg_hover_color']); 
		$custom_style['theme_'.$id_var.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_border_hover_color']); 
		$custom_style['theme_'.$id_var.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_color'], false );   
		$custom_style['theme_'.$id_var.'_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_shadow'];  
		$custom_style['theme_'.$id_var.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_color'], false );  
		$custom_style['theme_'.$id_var.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_text_hover_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow'];  
		$custom_style['theme_'.$id_var.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_box_shadow_color']);  
		$custom_style['theme_'.$id_var.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow'];  
		$custom_style['theme_'.$id_var.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id_var.'_hover_box_shadow_color']); 
	}
	
	// buttons styling
	// common
	$custom_style['theme_button_text_family'] = $presetOptionsArr['typography']['theme_button_text_family'];
	$custom_style['theme_button_letter_spacing'] = $presetOptionsArr['typography']['theme_button_letter_spacing'];
	$custom_style['theme_button_text_weight'] = $presetOptionsArr['typography']['theme_button_text_weight'];
	$custom_style['theme_button_text_transform'] = $presetOptionsArr['typography']['theme_button_text_transform'];
	$custom_style['theme_button_border_width'] = $presetOptionsArr['typography']['theme_button_border_width'];
	$custom_style['theme_button_border_style'] = $presetOptionsArr['typography']['theme_button_border_style'];
	$custom_style['theme_button_transition'] = $presetOptionsArr['typography']['theme_button_transition'];
	// custom
	foreach (sdfGo()->_wpuGlobal->_sdfButtons as $id => $name) {
		$custom_style['theme_'.$id.'_button_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_bg_color']);
		$custom_style['theme_'.$id.'_button_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_border_color']);
		$custom_style['theme_'.$id.'_button_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_bg_hover_color']); 
		$custom_style['theme_'.$id.'_button_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_border_hover_color']); 
		$custom_style['theme_'.$id.'_button_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_text_color'], false );   
		$custom_style['theme_'.$id.'_button_text_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_button_text_shadow'];  
		$custom_style['theme_'.$id.'_button_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_text_hover_color'], false );  
		$custom_style['theme_'.$id.'_button_text_hover_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_button_text_hover_shadow'];  
		$custom_style['theme_'.$id.'_button_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_button_box_shadow'];  
		$custom_style['theme_'.$id.'_button_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_box_shadow_color']);  
		$custom_style['theme_'.$id.'_button_hover_box_shadow'] = $presetOptionsArr['typography']['theme_'.$id.'_button_hover_box_shadow'];  
		$custom_style['theme_'.$id.'_button_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_'.$id.'_button_hover_box_shadow_color']); 
	}
	$custom_style['theme_link_button_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_link_button_text_color'], false ); 
	$custom_style['theme_link_button_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_link_button_text_hover_color'], false ); 
	
	// theme colors
	$custom_style['theme_primary_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_primary_color']);  
	
	foreach (sdfGo()->_wpuGlobal->_sdfSidebars as $id => $name) {
	if ($id != 'slider-widget-area') {
		$id = str_replace("-", "_", urlencode(strtolower($id)));
		// widget areas styling
		// used for header and footer areas
		if ( !in_array($id, array('default-widget-area', 'left-widget-area', 'right-widget-area')) ) {
			$custom_style[$id.'_height'] = $presetOptionsArr['typography'][$id.'_height'];
			$custom_style[$id.'_padding_top'] = $presetOptionsArr['typography'][$id.'_padding_top'];
			$custom_style[$id.'_padding_bottom'] = $presetOptionsArr['typography'][$id.'_padding_bottom'];
		}
		$custom_style[$id.'_custom_bg'] = $presetOptionsArr['typography'][$id.'_custom_bg'];
		$custom_style[$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_bg_color']);
		$custom_style[$id.'_bg_image'] = $presetOptionsArr['typography'][$id.'_bg_image'];
		$custom_style[$id.'_bgrepeat'] = $presetOptionsArr['typography'][$id.'_bgrepeat'];
		$custom_style[$id.'_box_shadow'] = $presetOptionsArr['typography'][$id.'_box_shadow'];
		$custom_style[$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_box_shadow_color']);
		// area widgets styling
		$custom_style[$id.'_wg_custom_typo'] = $presetOptionsArr['typography'][$id.'_wg_custom_typo'];
		$custom_style[$id.'_wg_title_align'] = $presetOptionsArr['typography'][$id.'_wg_title_align'];
		$custom_style[$id.'_wg_title_margin_top'] = $presetOptionsArr['typography'][$id.'_wg_title_margin_top'];
		$custom_style[$id.'_wg_title_margin_bottom'] = $presetOptionsArr['typography'][$id.'_wg_title_margin_bottom'];
		$custom_style[$id.'_wg_title_transform'] = $presetOptionsArr['typography'][$id.'_wg_title_transform'];
		$custom_style[$id.'_wg_title_weight'] = $presetOptionsArr['typography'][$id.'_wg_title_weight'];
		$custom_style[$id.'_wg_title_size'] = $presetOptionsArr['typography'][$id.'_wg_title_size'];
		$custom_style[$id.'_wg_title_size_type'] = $presetOptionsArr['typography'][$id.'_wg_title_size_type'];
		$custom_style[$id.'_wg_title_family'] = $presetOptionsArr['typography'][$id.'_wg_title_family'];
		$custom_style[$id.'_wg_title_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_title_color'], false );
		$custom_style[$id.'_wg_title_box_shadow'] = $presetOptionsArr['typography'][$id.'_wg_title_box_shadow'];
		$custom_style[$id.'_wg_title_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_title_box_shadow_color']);
		$custom_style[$id.'_wg_content_align'] = $presetOptionsArr['typography'][$id.'_wg_content_align'];
		$custom_style[$id.'_wg_font_size'] = $presetOptionsArr['typography'][$id.'_wg_font_size'];
		$custom_style[$id.'_wg_font_size_type'] = $presetOptionsArr['typography'][$id.'_wg_font_size_type'];
		$custom_style[$id.'_wg_family'] = $presetOptionsArr['typography'][$id.'_wg_family'];
		$custom_style[$id.'_wg_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_font_color'], false );
		$custom_style[$id.'_wg_link_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_link_color'], false );
		$custom_style[$id.'_wg_link_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_link_hover_color'], false );
		
		$custom_style[$id.'_wg_custom_style'] = $presetOptionsArr['typography'][$id.'_wg_custom_style'];
		$custom_style[$id.'_wg_background_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_background_color']);
		$custom_style[$id.'_wg_border_width'] = $presetOptionsArr['typography'][$id.'_wg_border_width'];
		$custom_style[$id.'_wg_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_border_color']);
		$custom_style[$id.'_wg_border_style'] = $presetOptionsArr['typography'][$id.'_wg_border_style'];
		$custom_style[$id.'_wg_border_radius'] = $presetOptionsArr['typography'][$id.'_wg_border_radius'];
		$custom_style[$id.'_wg_bg_image'] = $presetOptionsArr['typography'][$id.'_wg_bg_image'];
		$custom_style[$id.'_wg_bgrepeat'] = $presetOptionsArr['typography'][$id.'_wg_bgrepeat'];
		$custom_style[$id.'_wg_box_shadow'] = $presetOptionsArr['typography'][$id.'_wg_box_shadow'];
		$custom_style[$id.'_wg_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_box_shadow_color']);
		// widget list styling
		$custom_style[$id.'_wg_custom_list_style'] = $presetOptionsArr['typography'][$id.'_wg_custom_list_style'];
		$custom_style[$id.'_wg_list_align'] = $presetOptionsArr['typography'][$id.'_wg_list_align'];
		$custom_style[$id.'_wg_list_padding'] = $presetOptionsArr['typography'][$id.'_wg_list_padding'];
		$custom_style[$id.'_wg_list_margin'] = $presetOptionsArr['typography'][$id.'_wg_list_margin'];
		$custom_style[$id.'_wg_list_font_size'] = $presetOptionsArr['typography'][$id.'_wg_list_font_size'];
		$custom_style[$id.'_wg_list_font_size_type'] = $presetOptionsArr['typography'][$id.'_wg_list_font_size_type'];
		$custom_style[$id.'_wg_list_family'] = $presetOptionsArr['typography'][$id.'_wg_list_family'];
		$custom_style[$id.'_wg_list_font_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_font_color'], false );
		$custom_style[$id.'_wg_list_link_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_link_color'], false );
		$custom_style[$id.'_wg_list_link_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_link_hover_color'], false );
		$custom_style[$id.'_wg_list_link_text_decoration'] = $presetOptionsArr['typography'][$id.'_wg_list_link_text_decoration'];
		$custom_style[$id.'_wg_list_link_hover_text_decoration'] = $presetOptionsArr['typography'][$id.'_wg_list_link_hover_text_decoration'];
		
		$custom_style[$id.'_wg_list_style'] = $presetOptionsArr['typography'][$id.'_wg_list_style'];
		$custom_style[$id.'_wg_list_icon'] = $presetOptionsArr['typography'][$id.'_wg_list_icon'];
		$custom_style[$id.'_wg_list_icon_size'] = $presetOptionsArr['typography'][$id.'_wg_list_icon_size'];
		$custom_style[$id.'_wg_list_icon_size_type'] = $presetOptionsArr['typography'][$id.'_wg_list_icon_size_type'];
		$custom_style[$id.'_wg_list_icon_margin'] = $presetOptionsArr['typography'][$id.'_wg_list_icon_margin'];
		
		$custom_style[$id.'_wg_list_item_padding'] = $presetOptionsArr['typography'][$id.'_wg_list_item_padding'];
		$custom_style[$id.'_wg_list_item_margin'] = $presetOptionsArr['typography'][$id.'_wg_list_item_margin'];
		$custom_style[$id.'_wg_list_item_line_height'] = $presetOptionsArr['typography'][$id.'_wg_list_item_line_height'];
		$custom_style[$id.'_wg_list_item_border_width'] = $presetOptionsArr['typography'][$id.'_wg_list_item_border_width'];
		$custom_style[$id.'_wg_list_item_border_style'] = $presetOptionsArr['typography'][$id.'_wg_list_item_border_style'];
		$custom_style[$id.'_wg_list_item_border_radius'] = $presetOptionsArr['typography'][$id.'_wg_list_item_border_radius'];
		$custom_style[$id.'_wg_list_item_background_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_background_color'] );
		$custom_style[$id.'_wg_list_item_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_border_color'] );
		$custom_style[$id.'_wg_list_item_box_shadow'] = $presetOptionsArr['typography'][$id.'_wg_list_item_box_shadow'];
		$custom_style[$id.'_wg_list_item_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_box_shadow_color'] );
		$custom_style[$id.'_wg_list_item_background_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_background_hover_color'] );
		$custom_style[$id.'_wg_list_item_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_border_hover_color'] );
		$custom_style[$id.'_wg_list_item_hover_box_shadow'] = $presetOptionsArr['typography'][$id.'_wg_list_item_hover_box_shadow'];
		$custom_style[$id.'_wg_list_item_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography'][$id.'_wg_list_item_hover_box_shadow_color'] );
	}
	}
	
	// grid image transition, transform, overlay color
	$custom_style['theme_grid_image_grayscale'] = $presetOptionsArr['typography']['theme_grid_image_grayscale'];
	$custom_style['theme_grid_image_opacity'] = $presetOptionsArr['typography']['theme_grid_image_opacity'];
	$custom_style['theme_grid_image_opacity_hover'] = $presetOptionsArr['typography']['theme_grid_image_opacity_hover'];
	$custom_style['theme_grid_transition'] = $presetOptionsArr['typography']['theme_grid_transition'];
	$custom_style['theme_grid_transform'] = $presetOptionsArr['typography']['theme_grid_transform'];
	$custom_style['theme_grid_transform_hover'] = $presetOptionsArr['typography']['theme_grid_transform_hover'];
	$custom_style['theme_grid_transform_style'] = $presetOptionsArr['typography']['theme_grid_transform_style'];
	$custom_style['theme_grid_transform_origin'] = $presetOptionsArr['typography']['theme_grid_transform_origin'];
	$custom_style['theme_grid_overlay_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_overlay_color']);

	
	$id = 'item_overlay_button';
	$custom_style['theme_grid_'.$id.'_icon_size'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_icon_size'];
	$custom_style['theme_grid_'.$id.'_text_family'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_family'];
	$custom_style['theme_grid_'.$id.'_text_size'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_size'];
	$custom_style['theme_grid_'.$id.'_text_size_type'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_size_type'];
	$custom_style['theme_grid_'.$id.'_text_weight'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_weight'];
	$custom_style['theme_grid_'.$id.'_text_transform'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_transform'];
	$custom_style['theme_grid_'.$id.'_width'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_width'];
	$custom_style['theme_grid_'.$id.'_line_height'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_line_height'];
	$custom_style['theme_grid_'.$id.'_height'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_height'];
	$custom_style['theme_grid_'.$id.'_margin'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_margin'];
	$custom_style['theme_grid_'.$id.'_padding'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_padding'];
	$custom_style['theme_grid_'.$id.'_border_width'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_border_width'];
	$custom_style['theme_grid_'.$id.'_border_style'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_border_style'];
	$custom_style['theme_grid_'.$id.'_border_radius'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_border_radius'];
	$custom_style['theme_grid_'.$id.'_bg_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_bg_color']);
	$custom_style['theme_grid_'.$id.'_border_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_border_color']);
	$custom_style['theme_grid_'.$id.'_text_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_text_color']);
	$custom_style['theme_grid_'.$id.'_text_shadow'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_shadow'];
	$custom_style['theme_grid_'.$id.'_box_shadow'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_box_shadow'];
	$custom_style['theme_grid_'.$id.'_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_box_shadow_color']);
	$custom_style['theme_grid_'.$id.'_bg_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_bg_hover_color']);
	$custom_style['theme_grid_'.$id.'_border_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_border_hover_color']);
	$custom_style['theme_grid_'.$id.'_text_hover_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_text_hover_color']);
	$custom_style['theme_grid_'.$id.'_text_hover_shadow'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_text_hover_shadow'];
	$custom_style['theme_grid_'.$id.'_hover_box_shadow'] = $presetOptionsArr['typography']['theme_grid_'.$id.'_hover_box_shadow'];
	$custom_style['theme_grid_'.$id.'_hover_box_shadow_color'] = sdfGo()->_wpuGlobal->sdf_get_picked_color( $presetOptionsArr['typography']['theme_grid_'.$id.'_hover_box_shadow_color']);

	return $custom_style;
	
}


function sdf_page_styles() {

$post_id = sdf_page_holder_ID(); 
$sdf_theme_styles = sdf_get_page_styles($post_id);
$custom_page_css ='';
if($sdf_theme_styles['theme_page_width'] == 'boxed'){
	if($sdf_theme_styles['theme_font_boxed_bg_image'] || $sdf_theme_styles['theme_font_boxed_bg_color']){
	$theme_font_boxed_bg_color = ($sdf_theme_styles['theme_font_boxed_bg_color']) ? $sdf_theme_styles['theme_font_boxed_bg_color'] : '';
	$theme_font_boxed_bg_image = ($sdf_theme_styles['theme_font_boxed_bg_image']) ? $sdf_theme_styles['theme_font_boxed_bg_image'] : '';
	
	$custom_page_css .='body{';
	$custom_page_css .= get_background_color_image($sdf_theme_styles['theme_font_boxed_bg_color'], $sdf_theme_styles['theme_font_boxed_bg_image'], $sdf_theme_styles['theme_font_boxed_bg_repeat'], false);
	$custom_page_css .= '}';
	}
	if($sdf_theme_styles['theme_font_boxed_box_bg_image'] || $sdf_theme_styles['theme_font_boxed_box_bg_color']){
	
	$theme_font_boxed_box_bg_color = ($sdf_theme_styles['theme_font_boxed_box_bg_color']) ? $sdf_theme_styles['theme_font_boxed_box_bg_color'] : '';
	$theme_font_boxed_box_bg_image = ($sdf_theme_styles['theme_font_boxed_box_bg_image']) ? $sdf_theme_styles['theme_font_boxed_box_bg_image'] : '';
	
	if(in_array ($sdf_theme_styles['header_custom_bg'], array('yes', 'parallax'))) {
		$custom_page_css .= '#wrap > .row:nth-child(2) {';
	}
	else {
		$custom_page_css .= '#wrap {';
	}
	$custom_page_css .= get_background_color_image($sdf_theme_styles['theme_font_boxed_box_bg_color'], $sdf_theme_styles['theme_font_boxed_box_bg_image'], $sdf_theme_styles['theme_font_boxed_box_bg_repeat'], false);
	$custom_page_css .= '}';
	
	} 
	if($sdf_theme_styles['theme_font_boxed_box_margin'] != ''){
		$custom_page_css .= '#wrap {
		margin-top:'.$sdf_theme_styles['theme_font_boxed_box_margin'].';
		margin-bottom:'.$sdf_theme_styles['theme_font_boxed_box_margin'].';
		}';
	}
	if($sdf_theme_styles['theme_font_boxed_box_padding'] != ''){
		$custom_page_css .= '#wrap #navigation-area, #wrap #navigation-area-2, #wrap #header-area-1, #wrap #header-area-2, #wrap #header-area-3, #wrap #header-area-4, #wrap #content-wrap, #wrap #sdf-footer, #wrap #sdf-copyright {
		padding-left:'.$sdf_theme_styles['theme_font_boxed_box_padding'].';
		padding-right:'.$sdf_theme_styles['theme_font_boxed_box_padding'].';
		}';
	}
}
elseif($sdf_theme_styles['theme_page_width'] == 'wide'){
	if($sdf_theme_styles['theme_font_wide_bg_image'] || $sdf_theme_styles['theme_font_wide_bg_color']){
	
	$theme_font_wide_bg_color = ($sdf_theme_styles['theme_font_wide_bg_color']) ? $sdf_theme_styles['theme_font_wide_bg_color'] : '';
	$theme_font_wide_bg_image = ($sdf_theme_styles['theme_font_wide_bg_image']) ? $sdf_theme_styles['theme_font_wide_bg_image'] : '';
	
	$custom_page_css .='body{';
	$custom_page_css .= get_background_color_image($sdf_theme_styles['theme_font_wide_bg_color'], $sdf_theme_styles['theme_font_wide_bg_image'], $sdf_theme_styles['theme_font_wide_bg_repeat'], false);
	$custom_page_css .= '}';
	}
}
// page level top/bottom padding
if($sdf_theme_styles['page_padding_top'] != '' || $sdf_theme_styles['page_padding_bottom'] != ''){
	$custom_page_css .= '@media (min-width:992px){';
	$custom_page_css .= '#left-widget-area,
#right-widget-area,
#sdf-content {
padding-top: '.$sdf_theme_styles['page_padding_top'].';
padding-bottom: '.$sdf_theme_styles['page_padding_bottom'].';
}
}';
} 
if($sdf_theme_styles['theme_font_boxed_box_border_style'] != 'none'){
$custom_page_css .='#wrap {
  border: '.$sdf_theme_styles['theme_font_boxed_box_border_width'].$sdf_theme_styles['theme_font_boxed_box_border_width_type'].' '.$sdf_theme_styles['theme_font_boxed_box_border_style'].' '.$sdf_theme_styles['theme_font_boxed_box_border_color'].';
}';
}
if($sdf_theme_styles['theme_font_boxed_box_shadow'] != ''){
$custom_page_css .='#wrap {
  -webkit-box-shadow: '.$sdf_theme_styles['theme_font_boxed_box_shadow'].' '.$sdf_theme_styles['theme_font_boxed_box_shadow_color'].';
		box-shadow: '.$sdf_theme_styles['theme_font_boxed_box_shadow'].' '.$sdf_theme_styles['theme_font_boxed_box_shadow_color'].';
}';
}

if($sdf_theme_styles['theme_layout_page'] == 'custom'){

	if((int)$sdf_theme_styles['theme_custom_left'] > 0){
	$custom_page_css .='.c-left {
	float:left;
	padding-left:15px;
	padding-right:15px;
	min-height: 1px;
	width:'.$sdf_theme_styles['theme_custom_left'].';
	}';
	}
	if((int)$sdf_theme_styles['theme_custom_center'] > 0){
	$custom_page_css .='.c-main{
	float:left;
	padding-left:15px;
	padding-right:15px; 
	min-height: 1px;
	width:'.(int)$sdf_theme_styles['theme_custom_center'].'px;
	}';
	}
	if((int)$sdf_theme_styles['theme_custom_right'] > 0){
	$custom_page_css .='.c-right {
	float:left;
	padding-left: 15px;
	padding-right: 15px; 
	min-height: 1px;
	width:'.$sdf_theme_styles['theme_custom_right'].';
	}';
	}
}

  wp_add_inline_style( 'sdf-css-typography', $custom_page_css );
}
add_action( 'wp_enqueue_scripts', 'sdf_page_styles' );

// used in sdf.scripts.php (css ajax call) and global.php (css file creation)
function sdf_typography_styles($presetOptionsArr = array(), $ssl_paths = false) {
	
if (!empty($_REQUEST['nonce']))
{
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "nonce_css_typography")) {
		exit("No naughty business please");
	}

	header("Content-type: text/css; charset=utf-8");
	header("Cache-Control: must-revalidate");

	$exp_str = "Expires: " . gmdate("D, d M Y H:i:s", time() + 3600) . " GMT";
	header($exp_str);
}


	$sdf_css_path = ($ssl_paths) ? preg_replace( "/^http:/i", "https:", SDF_CSS ) : SDF_CSS;


$sdf_theme_styles = sdf_get_typography_styles($presetOptionsArr);

// WooCommerce Static css v2.6
if( is_woocommerce_active() ): ?>
/* WooCommerce layout */
.woocommerce #content div.product .woocommerce-tabs ul.tabs:after,.woocommerce #content div.product .woocommerce-tabs ul.tabs:before,.woocommerce #content div.product div.thumbnails:after,.woocommerce #content div.product div.thumbnails:before,.woocommerce .col2-set:after,.woocommerce .col2-set:before,.woocommerce div.product .woocommerce-tabs ul.tabs:after,.woocommerce div.product .woocommerce-tabs ul.tabs:before,.woocommerce div.product div.thumbnails:after,.woocommerce div.product div.thumbnails:before,.woocommerce-page #content div.product .woocommerce-tabs ul.tabs:after,.woocommerce-page #content div.product .woocommerce-tabs ul.tabs:before,.woocommerce-page #content div.product div.thumbnails:after,.woocommerce-page #content div.product div.thumbnails:before,.woocommerce-page .col2-set:after,.woocommerce-page .col2-set:before,.woocommerce-page div.product .woocommerce-tabs ul.tabs:after,.woocommerce-page div.product .woocommerce-tabs ul.tabs:before,.woocommerce-page div.product div.thumbnails:after,.woocommerce-page div.product div.thumbnails:before{content:" ";display:table}.woocommerce #content div.product .woocommerce-tabs,.woocommerce #content div.product .woocommerce-tabs ul.tabs:after,.woocommerce #content div.product div.thumbnails a.first,.woocommerce #content div.product div.thumbnails:after,.woocommerce .cart-collaterals:after,.woocommerce .col2-set:after,.woocommerce .woocommerce-pagination ul.page-numbers:after,.woocommerce div.product .woocommerce-tabs,.woocommerce div.product .woocommerce-tabs ul.tabs:after,.woocommerce div.product div.thumbnails a.first,.woocommerce div.product div.thumbnails:after,.woocommerce ul.products,.woocommerce ul.products li.first,.woocommerce ul.products:after,.woocommerce-page #content div.product .woocommerce-tabs,.woocommerce-page #content div.product .woocommerce-tabs ul.tabs:after,.woocommerce-page #content div.product div.thumbnails a.first,.woocommerce-page #content div.product div.thumbnails:after,.woocommerce-page .cart-collaterals:after,.woocommerce-page .col2-set:after,.woocommerce-page .woocommerce-pagination ul.page-numbers:after,.woocommerce-page div.product .woocommerce-tabs,.woocommerce-page div.product .woocommerce-tabs ul.tabs:after,.woocommerce-page div.product div.thumbnails a.first,.woocommerce-page div.product div.thumbnails:after,.woocommerce-page ul.products,.woocommerce-page ul.products li.first,.woocommerce-page ul.products:after{clear:both}.woocommerce .col2-set,.woocommerce-page .col2-set{width:100%}.woocommerce .col2-set .col-1,.woocommerce-page .col2-set .col-1{float:left;width:48%}.woocommerce .col2-set .col-2,.woocommerce-page .col2-set .col-2{float:right;width:48%}.woocommerce img,.woocommerce-page img{height:auto;max-width:100%}.woocommerce #content div.product div.images,.woocommerce div.product div.images,.woocommerce-page #content div.product div.images,.woocommerce-page div.product div.images{float:left;width:48%}.woocommerce #content div.product div.thumbnails a,.woocommerce div.product div.thumbnails a,.woocommerce-page #content div.product div.thumbnails a,.woocommerce-page div.product div.thumbnails a{float:left;width:30.75%;margin-right:3.8%;margin-bottom:1em}.woocommerce #content div.product div.thumbnails a.last,.woocommerce div.product div.thumbnails a.last,.woocommerce-page #content div.product div.thumbnails a.last,.woocommerce-page div.product div.thumbnails a.last{margin-right:0}.woocommerce #content div.product div.thumbnails.columns-1 a,.woocommerce div.product div.thumbnails.columns-1 a,.woocommerce-page #content div.product div.thumbnails.columns-1 a,.woocommerce-page div.product div.thumbnails.columns-1 a{width:100%;margin-right:0;float:none}.woocommerce #content div.product div.thumbnails.columns-2 a,.woocommerce div.product div.thumbnails.columns-2 a,.woocommerce-page #content div.product div.thumbnails.columns-2 a,.woocommerce-page div.product div.thumbnails.columns-2 a{width:48%}.woocommerce #content div.product div.thumbnails.columns-4 a,.woocommerce div.product div.thumbnails.columns-4 a,.woocommerce-page #content div.product div.thumbnails.columns-4 a,.woocommerce-page div.product div.thumbnails.columns-4 a{width:22.05%}.woocommerce #content div.product div.thumbnails.columns-5 a,.woocommerce div.product div.thumbnails.columns-5 a,.woocommerce-page #content div.product div.thumbnails.columns-5 a,.woocommerce-page div.product div.thumbnails.columns-5 a{width:16.9%}.woocommerce #content div.product div.summary,.woocommerce div.product div.summary,.woocommerce-page #content div.product div.summary,.woocommerce-page div.product div.summary{float:right;width:48%}.woocommerce #content div.product .woocommerce-tabs ul.tabs li,.woocommerce div.product .woocommerce-tabs ul.tabs li,.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li,.woocommerce-page div.product .woocommerce-tabs ul.tabs li{display:inline-block}.woocommerce #content div.product #reviews .comment:after,.woocommerce #content div.product #reviews .comment:before,.woocommerce .woocommerce-pagination ul.page-numbers:after,.woocommerce .woocommerce-pagination ul.page-numbers:before,.woocommerce div.product #reviews .comment:after,.woocommerce div.product #reviews .comment:before,.woocommerce ul.products:after,.woocommerce ul.products:before,.woocommerce-page #content div.product #reviews .comment:after,.woocommerce-page #content div.product #reviews .comment:before,.woocommerce-page .woocommerce-pagination ul.page-numbers:after,.woocommerce-page .woocommerce-pagination ul.page-numbers:before,.woocommerce-page div.product #reviews .comment:after,.woocommerce-page div.product #reviews .comment:before,.woocommerce-page ul.products:after,.woocommerce-page ul.products:before{content:" ";display:table}.woocommerce #content div.product #reviews .comment:after,.woocommerce div.product #reviews .comment:after,.woocommerce-page #content div.product #reviews .comment:after,.woocommerce-page div.product #reviews .comment:after{clear:both}.woocommerce #content div.product #reviews .comment img,.woocommerce div.product #reviews .comment img,.woocommerce-page #content div.product #reviews .comment img,.woocommerce-page div.product #reviews .comment img{float:right;height:auto}.woocommerce ul.products li.last,.woocommerce-page ul.products li.last{margin-right:0}.woocommerce-page.columns-1 ul.products li.product,.woocommerce.columns-1 ul.products li.product{width:100%;margin-right:0}.woocommerce-page.columns-2 ul.products li.product,.woocommerce.columns-2 ul.products li.product{width:48%}.woocommerce-page.columns-3 ul.products li.product,.woocommerce.columns-3 ul.products li.product{width:30.75%}.woocommerce-page.columns-5 ul.products li.product,.woocommerce.columns-5 ul.products li.product{width:16.95%}.woocommerce-page.columns-6 ul.products li.product,.woocommerce.columns-6 ul.products li.product{width:13.5%}.woocommerce .woocommerce-result-count,.woocommerce-page .woocommerce-result-count{float:left}.woocommerce .woocommerce-ordering,.woocommerce-page .woocommerce-ordering{float:right}.woocommerce .woocommerce-pagination ul.page-numbers li,.woocommerce-page .woocommerce-pagination ul.page-numbers li{display:inline-block}.woocommerce #content table.cart img,.woocommerce table.cart img,.woocommerce-page #content table.cart img,.woocommerce-page table.cart img{height:auto}.woocommerce #content table.cart td.actions,.woocommerce table.cart td.actions,.woocommerce-page #content table.cart td.actions,.woocommerce-page table.cart td.actions{text-align:right}.woocommerce #content table.cart td.actions .input-text,.woocommerce table.cart td.actions .input-text,.woocommerce-page #content table.cart td.actions .input-text,.woocommerce-page table.cart td.actions .input-text{width:80px}.woocommerce #content table.cart td.actions .coupon,.woocommerce table.cart td.actions .coupon,.woocommerce-page #content table.cart td.actions .coupon,.woocommerce-page table.cart td.actions .coupon{float:left}.woocommerce #content table.cart td.actions .coupon label,.woocommerce table.cart td.actions .coupon label,.woocommerce-page #content table.cart td.actions .coupon label,.woocommerce-page table.cart td.actions .coupon label{display:none}.woocommerce .cart-collaterals .shipping_calculator:after,.woocommerce .cart-collaterals .shipping_calculator:before,.woocommerce .cart-collaterals:after,.woocommerce .cart-collaterals:before,.woocommerce form .form-row:after,.woocommerce form .form-row:before,.woocommerce ul.cart_list li:after,.woocommerce ul.cart_list li:before,.woocommerce ul.product_list_widget li:after,.woocommerce ul.product_list_widget li:before,.woocommerce-page .cart-collaterals .shipping_calculator:after,.woocommerce-page .cart-collaterals .shipping_calculator:before,.woocommerce-page .cart-collaterals:after,.woocommerce-page .cart-collaterals:before,.woocommerce-page form .form-row:after,.woocommerce-page form .form-row:before,.woocommerce-page ul.cart_list li:after,.woocommerce-page ul.cart_list li:before,.woocommerce-page ul.product_list_widget li:after,.woocommerce-page ul.product_list_widget li:before{content:" ";display:table}.woocommerce .cart-collaterals,.woocommerce-page .cart-collaterals{width:100%}.woocommerce .cart-collaterals .related,.woocommerce-page .cart-collaterals .related{width:30.75%;float:left}.woocommerce .cart-collaterals .cross-sells,.woocommerce-page .cart-collaterals .cross-sells{width:48%;float:left}.woocommerce .cart-collaterals .cross-sells ul.products,.woocommerce-page .cart-collaterals .cross-sells ul.products{float:none}.woocommerce .cart-collaterals .cross-sells ul.products li,.woocommerce-page .cart-collaterals .cross-sells ul.products li{width:48%}.woocommerce .cart-collaterals .shipping_calculator,.woocommerce-page .cart-collaterals .shipping_calculator{width:48%;clear:right;float:right}.woocommerce .cart-collaterals .shipping_calculator:after,.woocommerce form .form-row-wide,.woocommerce form .form-row:after,.woocommerce ul.cart_list li:after,.woocommerce ul.product_list_widget li:after,.woocommerce-page .cart-collaterals .shipping_calculator:after,.woocommerce-page form .form-row-wide,.woocommerce-page form .form-row:after,.woocommerce-page ul.cart_list li:after,.woocommerce-page ul.product_list_widget li:after{clear:both}.woocommerce .cart-collaterals .shipping_calculator .col2-set .col-1,.woocommerce .cart-collaterals .shipping_calculator .col2-set .col-2,.woocommerce-page .cart-collaterals .shipping_calculator .col2-set .col-1,.woocommerce-page .cart-collaterals .shipping_calculator .col2-set .col-2{width:47%}.woocommerce .cart-collaterals .cart_totals,.woocommerce-page .cart-collaterals .cart_totals{float:right;width:48%}.woocommerce ul.cart_list li img,.woocommerce ul.product_list_widget li img,.woocommerce-page ul.cart_list li img,.woocommerce-page ul.product_list_widget li img{float:right;height:auto}.woocommerce form .form-row label,.woocommerce-page form .form-row label{display:block}.woocommerce form .form-row label.checkbox,.woocommerce-page form .form-row label.checkbox{display:inline}.woocommerce form .form-row select,.woocommerce-page form .form-row select{width:100%}.woocommerce form .form-row .input-text,.woocommerce-page form .form-row .input-text{box-sizing:border-box;width:100%}.woocommerce form .form-row-first,.woocommerce form .form-row-last,.woocommerce-page form .form-row-first,.woocommerce-page form .form-row-last{float:left;width:47%;overflow:visible}.woocommerce form .form-row-last,.woocommerce-page form .form-row-last{float:right}.woocommerce #payment .form-row select,.woocommerce-page #payment .form-row select{width:auto}.woocommerce #payment .terms,.woocommerce #payment .wc-terms-and-conditions,.woocommerce-page #payment .terms,.woocommerce-page #payment .wc-terms-and-conditions{text-align:left;padding:0 1em 0 0;float:left}.woocommerce #payment #place_order,.woocommerce-page #payment #place_order{float:right}.woocommerce-page.left-sidebar #content.twentyeleven{width:58.4%;margin:0 7.6%;float:right}.woocommerce-page.right-sidebar #content.twentyeleven{margin:0 7.6%;width:58.4%;float:left}.twentyfourteen .tfwc{padding:12px 10px 0;max-width:474px;margin:0 auto}.twentyfourteen .tfwc .product .entry-summary{padding:0!important;margin:0 0 1.618em!important}.twentyfourteen .tfwc div.product.hentry.has-post-thumbnail{margin-top:0}.twentyfourteen .tfwc .product .images img{margin-bottom:1em}@media screen and (min-width:673px){.twentyfourteen .tfwc{padding-right:30px;padding-left:30px}}@media screen and (min-width:1040px){.twentyfourteen .tfwc{padding-right:15px;padding-left:15px}}@media screen and (min-width:1110px){.twentyfourteen .tfwc{padding-right:30px;padding-left:30px}}@media screen and (min-width:1218px){.twentyfourteen .tfwc{margin-right:54px}.full-width .twentyfourteen .tfwc{margin-right:auto}}.twentyfifteen .t15wc{padding-left:7.6923%;padding-right:7.6923%;padding-top:7.6923%;margin-bottom:7.6923%;background:#fff;box-shadow:0 0 1px rgba(0,0,0,.15)}.twentyfifteen .t15wc .page-title{margin-left:0}@media screen and (min-width:38.75em){.twentyfifteen .t15wc{margin-right:7.6923%;margin-left:7.6923%;margin-top:8.3333%}}@media screen and (min-width:59.6875em){.twentyfifteen .t15wc{margin-left:8.3333%;margin-right:8.3333%;padding:10%}.single-product .twentyfifteen .entry-summary{padding:0!important}}.twentysixteen .site-main{margin-right:7.6923%;margin-left:7.6923%}.twentysixteen .entry-summary{margin-right:0;margin-left:0}#content .twentysixteen div.product div.images,#content .twentysixteen div.product div.summary{width:46.42857%}@media screen and (min-width:44.375em){.twentysixteen .site-main{margin-right:23.0769%}}@media screen and (min-width:56.875em){.twentysixteen .site-main{margin-right:0;margin-left:0}.no-sidebar .twentysixteen .site-main{margin-right:15%;margin-left:15%}.no-sidebar .twentysixteen .entry-summary{margin-right:0;margin-left:0}}.rtl .woocommerce .col2-set .col-1,.rtl .woocommerce-page .col2-set .col-1{float:right}.rtl .woocommerce .col2-set .col-2,.rtl .woocommerce-page .col2-set .col-2{float:left}.rtl .woocommerce form .form-row-first,.rtl .woocommerce form .form-row-last,.rtl .woocommerce-page form .form-row-first,.rtl .woocommerce-page form .form-row-last{float:right}.rtl .woocommerce form .form-row-last,.rtl .woocommerce-page form .form-row-last{float:left}
/* WooCommerce smallscreen */
@media only screen and (max-width: 768px){ 
.woocommerce table.shop_table_responsive tbody th,.woocommerce table.shop_table_responsive thead,.woocommerce-page table.shop_table_responsive tbody th,.woocommerce-page table.shop_table_responsive thead{display:none}.woocommerce table.shop_table_responsive tbody tr:first-child td:first-child,.woocommerce-page table.shop_table_responsive tbody tr:first-child td:first-child{border-top:0}.woocommerce table.shop_table_responsive tr,.woocommerce-page table.shop_table_responsive tr{display:block}.woocommerce table.shop_table_responsive tr td,.woocommerce-page table.shop_table_responsive tr td{display:block;text-align:right!important}.woocommerce #content table.cart .product-thumbnail,.woocommerce table.cart .product-thumbnail,.woocommerce table.my_account_orders tr td.order-actions:before,.woocommerce table.shop_table_responsive tr td.actions:before,.woocommerce table.shop_table_responsive tr td.product-remove:before,.woocommerce-page #content table.cart .product-thumbnail,.woocommerce-page table.cart .product-thumbnail,.woocommerce-page table.my_account_orders tr td.order-actions:before,.woocommerce-page table.shop_table_responsive tr td.actions:before,.woocommerce-page table.shop_table_responsive tr td.product-remove:before{display:none}.woocommerce table.shop_table_responsive tr td.order-actions,.woocommerce-page table.shop_table_responsive tr td.order-actions{text-align:left!important}.woocommerce table.shop_table_responsive tr td:before,.woocommerce-page table.shop_table_responsive tr td:before{content:attr(data-title) ": ";font-weight:700;float:left}.woocommerce table.shop_table_responsive tr:nth-child(2n) td,.woocommerce-page table.shop_table_responsive tr:nth-child(2n) td{background-color:rgba(0,0,0,.025)}.woocommerce table.my_account_orders tr td.order-actions,.woocommerce-page table.my_account_orders tr td.order-actions{text-align:left}.woocommerce table.my_account_orders tr td.order-actions .button,.woocommerce-page table.my_account_orders tr td.order-actions .button{float:none;margin:.125em .25em .125em 0}.woocommerce .col2-set .col-1,.woocommerce .col2-set .col-2,.woocommerce-page .col2-set .col-1,.woocommerce-page .col2-set .col-2{float:none;width:100%}.woocommerce #content div.product div.images,.woocommerce #content div.product div.summary,.woocommerce div.product div.images,.woocommerce div.product div.summary,.woocommerce-page #content div.product div.images,.woocommerce-page #content div.product div.summary,.woocommerce-page div.product div.images,.woocommerce-page div.product div.summary{float:none;width:100%}.woocommerce #content table.cart td.actions,.woocommerce table.cart td.actions,.woocommerce-page #content table.cart td.actions,.woocommerce-page table.cart td.actions{text-align:left}.woocommerce #content table.cart td.actions .coupon,.woocommerce table.cart td.actions .coupon,.woocommerce-page #content table.cart td.actions .coupon,.woocommerce-page table.cart td.actions .coupon{float:none;padding-bottom:.5em}.woocommerce #content table.cart td.actions .coupon:after,.woocommerce #content table.cart td.actions .coupon:before,.woocommerce table.cart td.actions .coupon:after,.woocommerce table.cart td.actions .coupon:before,.woocommerce-page #content table.cart td.actions .coupon:after,.woocommerce-page #content table.cart td.actions .coupon:before,.woocommerce-page table.cart td.actions .coupon:after,.woocommerce-page table.cart td.actions .coupon:before{content:" ";display:table}.woocommerce #content table.cart td.actions .coupon:after,.woocommerce table.cart td.actions .coupon:after,.woocommerce-page #content table.cart td.actions .coupon:after,.woocommerce-page table.cart td.actions .coupon:after{clear:both}.woocommerce #content table.cart td.actions .coupon .button,.woocommerce #content table.cart td.actions .coupon .input-text,.woocommerce #content table.cart td.actions .coupon input,.woocommerce table.cart td.actions .coupon .button,.woocommerce table.cart td.actions .coupon .input-text,.woocommerce table.cart td.actions .coupon input,.woocommerce-page #content table.cart td.actions .coupon .button,.woocommerce-page #content table.cart td.actions .coupon .input-text,.woocommerce-page #content table.cart td.actions .coupon input,.woocommerce-page table.cart td.actions .coupon .button,.woocommerce-page table.cart td.actions .coupon .input-text,.woocommerce-page table.cart td.actions .coupon input{width:48%;box-sizing:border-box}.woocommerce #content table.cart td.actions .coupon .button.alt,.woocommerce #content table.cart td.actions .coupon .input-text+.button,.woocommerce table.cart td.actions .coupon .button.alt,.woocommerce table.cart td.actions .coupon .input-text+.button,.woocommerce-page #content table.cart td.actions .coupon .button.alt,.woocommerce-page #content table.cart td.actions .coupon .input-text+.button,.woocommerce-page table.cart td.actions .coupon .button.alt,.woocommerce-page table.cart td.actions .coupon .input-text+.button{float:right}.woocommerce #content table.cart td.actions .button,.woocommerce table.cart td.actions .button,.woocommerce-page #content table.cart td.actions .button,.woocommerce-page table.cart td.actions .button{display:block;width:100%}.woocommerce .cart-collaterals .cart_totals,.woocommerce .cart-collaterals .cross-sells,.woocommerce .cart-collaterals .shipping_calculator,.woocommerce-page .cart-collaterals .cart_totals,.woocommerce-page .cart-collaterals .cross-sells,.woocommerce-page .cart-collaterals .shipping_calculator{width:100%;float:none;text-align:left}.woocommerce-page.woocommerce-checkout form.login .form-row,.woocommerce.woocommerce-checkout form.login .form-row{width:100%;float:none}.woocommerce #payment .terms,.woocommerce-page #payment .terms{text-align:left;padding:0}.woocommerce #payment #place_order,.woocommerce-page #payment #place_order{float:none;width:100%;box-sizing:border-box;margin-bottom:1em}.woocommerce .lost_reset_password .form-row-first,.woocommerce .lost_reset_password .form-row-last,.woocommerce-page .lost_reset_password .form-row-first,.woocommerce-page .lost_reset_password .form-row-last{width:100%;float:none;margin-right:0}.single-product .twentythirteen .panel{padding-left:20px!important;padding-right:20px!important}
}
/* WooCommerce general */
@charset "UTF-8";.clear,.woocommerce .woocommerce-breadcrumb:after,.woocommerce .woocommerce-error:after,.woocommerce .woocommerce-info:after,.woocommerce .woocommerce-message:after{clear:both}@-webkit-keyframes spin{100%{-webkit-transform:rotate(360deg)}}@-moz-keyframes spin{100%{-moz-transform:rotate(360deg)}}@keyframes spin{100%{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}@font-face{font-family:star;src:url(woocommerce/assets/fonts/star.eot);src:url(woocommerce/assets/fonts/star.eot?#iefix) format("embedded-opentype"),url(woocommerce/assets/fonts/star.woff) format("woff"),url(woocommerce/assets/fonts/star.ttf) format("truetype"),url(woocommerce/assets/fonts/star.svg#star) format("svg");font-weight:400;font-style:normal}@font-face{font-family:WooCommerce;src:url(woocommerce/assets/fonts/WooCommerce.eot);src:url(woocommerce/assets/fonts/WooCommerce.eot?#iefix) format("embedded-opentype"),url(woocommerce/assets/fonts/WooCommerce.woff) format("woff"),url(woocommerce/assets/fonts/WooCommerce.ttf) format("truetype"),url(woocommerce/assets/fonts/WooCommerce.svg#WooCommerce) format("svg");font-weight:400;font-style:normal}p.demo_store{position:fixed;top:0;left:0;right:0;margin:0;width:100%;font-size:1em;padding:1em 0;text-align:center;background-color:<?php echo $sdf_theme_styles['theme_primary_color']; ?>;color:#fff;z-index:99998;box-shadow:0 1px 1em rgba(0,0,0,.2)}p.demo_store a{color:#fff}.admin-bar p.demo_store{top:32px}.woocommerce .blockUI.blockOverlay{position:relative}.woocommerce .blockUI.blockOverlay:before,.woocommerce .loader:before{height:1em;width:1em;position:absolute;top:50%;left:50%;margin-left:-.5em;margin-top:-.5em;display:block;content:"";-webkit-animation:spin 1s ease-in-out infinite;-moz-animation:spin 1s ease-in-out infinite;animation:spin 1s ease-in-out infinite;background:url(../images/icons/loader.svg) center center;background-size:cover;line-height:1;text-align:center;font-size:2em;color:rgba(0,0,0,.75)}.woocommerce a.remove{display:block;font-size:1.5em;height:1em;width:1em;text-align:center;line-height:1;border-radius:100%;color:red!important;text-decoration:none;font-weight:700;border:0}.woocommerce a.remove:hover{color:#fff!important;background:red}.woocommerce .woocommerce-error,.woocommerce .woocommerce-info,.woocommerce .woocommerce-message{padding:1em 2em 1em 3.5em!important;margin:0 0 2em!important;position:relative;background-color:#f7f6f7;color:#515151;border-top:3px solid <?php echo $sdf_theme_styles['theme_primary_color']; ?>;list-style:none!important;width:auto;word-wrap:break-word}.woocommerce .woocommerce-error:after,.woocommerce .woocommerce-error:before,.woocommerce .woocommerce-info:after,.woocommerce .woocommerce-info:before,.woocommerce .woocommerce-message:after,.woocommerce .woocommerce-message:before{content:" ";display:table}.woocommerce .woocommerce-error:before,.woocommerce .woocommerce-info:before,.woocommerce .woocommerce-message:before{font-family:WooCommerce;content:"\e028";display:inline-block;position:absolute;top:1em;left:1.5em}.woocommerce .woocommerce-error .button,.woocommerce .woocommerce-info .button,.woocommerce .woocommerce-message .button{float:right}.woocommerce .woocommerce-error li,.woocommerce .woocommerce-info li,.woocommerce .woocommerce-message li{list-style:none!important;padding-left:0!important;margin-left:0!important}.woocommerce .woocommerce-message{border-top-color:#8fae1b}.woocommerce .woocommerce-message:before{content:"\e015";color:#8fae1b}.woocommerce .woocommerce-info{border-top-color:#1e85be}.woocommerce .woocommerce-info:before{color:#1e85be}.woocommerce .woocommerce-error{border-top-color:#b81c23}.woocommerce .woocommerce-error:before{content:"\e016";color:#b81c23}.woocommerce small.note{display:block;color:#777;font-size:.857em;margin-top:10px}.woocommerce .woocommerce-breadcrumb{margin:0 0 1em;padding:0;font-size:.92em;color:#777}.woocommerce .woocommerce-breadcrumb:after,.woocommerce .woocommerce-breadcrumb:before{content:" ";display:table}.woocommerce .woocommerce-breadcrumb a{color:#777}.woocommerce .quantity .qty{width:3.631em;text-align:center}.woocommerce div.product{margin-bottom:0;position:relative}.woocommerce div.product .product_title{clear:none;margin-top:0;padding:0}.woocommerce #reviews #comments .add_review:after,.woocommerce .products ul:after,.woocommerce div.product form.cart:after,.woocommerce div.product p.cart:after,.woocommerce nav.woocommerce-pagination ul,.woocommerce ul.products:after{clear:both}.woocommerce div.product p.price,.woocommerce div.product span.price{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>;font-size:1.25em}.woocommerce div.product p.price ins,.woocommerce div.product span.price ins{background:inherit;font-weight:700}.woocommerce div.product p.price del,.woocommerce div.product span.price del{opacity:.5}.woocommerce div.product p.stock{font-size:.92em}.woocommerce div.product .stock{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>}.woocommerce div.product .out-of-stock{color:red}.woocommerce div.product .woocommerce-product-rating{margin-bottom:1.618em}.woocommerce div.product div.images,.woocommerce div.product div.summary{margin-bottom:2em}.woocommerce div.product div.images img{display:block;width:100%;height:auto;box-shadow:none}.woocommerce div.product div.images div.thumbnails{padding-top:1em}.woocommerce div.product div.social{text-align:right;margin:0 0 1em}.woocommerce div.product div.social span{margin:0 0 0 2px}.woocommerce div.product div.social span span{margin:0}.woocommerce div.product div.social span .stButton .chicklets{padding-left:16px;width:0}.woocommerce div.product div.social iframe{float:left;margin-top:3px}.woocommerce div.product .woocommerce-tabs ul.tabs{list-style:none;padding:0 0 0 1em;margin:0 0 1.618em;overflow:hidden;position:relative}.woocommerce div.product .woocommerce-tabs ul.tabs li{border:1px solid #d3ced2;background-color:#ebe9eb;display:inline-block;position:relative;z-index:0;border-radius:4px 4px 0 0;margin:0 -5px;padding:0 1em}.woocommerce div.product .woocommerce-tabs ul.tabs li a{display:inline-block;padding:.5em 0;font-weight:700;color:#515151;text-decoration:none}.woocommerce div.product form.cart:after,.woocommerce div.product form.cart:before,.woocommerce div.product p.cart:after,.woocommerce div.product p.cart:before{display:table;content:" "}.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover{text-decoration:none;color:#6b6b6b}.woocommerce div.product .woocommerce-tabs ul.tabs li.active{background:#fff;z-index:2;border-bottom-color:#fff}.woocommerce div.product .woocommerce-tabs ul.tabs li.active a{color:inherit;text-shadow:inherit}.woocommerce div.product .woocommerce-tabs ul.tabs li.active:before{box-shadow:2px 2px 0 #fff}.woocommerce div.product .woocommerce-tabs ul.tabs li.active:after{box-shadow:-2px 2px 0 #fff}.woocommerce div.product .woocommerce-tabs ul.tabs li:after,.woocommerce div.product .woocommerce-tabs ul.tabs li:before{border:1px solid #d3ced2;position:absolute;bottom:-1px;width:5px;height:5px;content:" "}.woocommerce div.product .woocommerce-tabs ul.tabs li:before{left:-6px;-webkit-border-bottom-right-radius:4px;-moz-border-bottom-right-radius:4px;border-bottom-right-radius:4px;border-width:0 1px 1px 0;box-shadow:2px 2px 0 #ebe9eb}.woocommerce div.product .woocommerce-tabs ul.tabs li:after{right:-6px;-webkit-border-bottom-left-radius:4px;-moz-border-bottom-left-radius:4px;border-bottom-left-radius:4px;border-width:0 0 1px 1px;box-shadow:-2px 2px 0 #ebe9eb}.woocommerce div.product .woocommerce-tabs ul.tabs:before{position:absolute;content:" ";width:100%;bottom:0;left:0;border-bottom:1px solid #d3ced2;z-index:1}.woocommerce div.product .woocommerce-tabs .panel{margin:0 0 2em;padding:0}.woocommerce div.product form.cart,.woocommerce div.product p.cart{margin-bottom:2em}.woocommerce div.product form.cart div.quantity{float:left;margin:0 4px 0 0}.woocommerce div.product form.cart table{border-width:0 0 1px}.woocommerce div.product form.cart table td{padding-left:0}.woocommerce div.product form.cart table div.quantity{float:none;margin:0}.woocommerce div.product form.cart table small.stock{display:block;float:none}.woocommerce div.product form.cart .variations{margin-bottom:1em;border:0;width:100%}.woocommerce div.product form.cart .variations td,.woocommerce div.product form.cart .variations th{border:0;vertical-align:top;line-height:2em}.woocommerce div.product form.cart .variations label{font-weight:700}.woocommerce div.product form.cart .variations select{max-width:100%;min-width:75%;display:inline-block;margin-right:1em}.woocommerce div.product form.cart .variations td.label{padding-right:1em}.woocommerce div.product form.cart .woocommerce-variation-description p{margin-bottom:1em}.woocommerce div.product form.cart .reset_variations{visibility:hidden;font-size:.83em}.woocommerce div.product form.cart .wc-no-matching-variations{display:none}.woocommerce div.product form.cart .button{vertical-align:middle;float:left}.woocommerce div.product form.cart .group_table td.label{padding-right:1em;padding-left:1em}.woocommerce div.product form.cart .group_table td{vertical-align:top;padding-bottom:.5em;border:0}.woocommerce span.onsale{min-height:3.236em;min-width:3.236em;padding:.202em;font-weight:700;position:absolute;text-align:center;line-height:3.236;top:-.5em;left:-.5em;margin:0;border-radius:100%;background-color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>;color:#fff;font-size:.857em;-webkit-font-smoothing:antialiased}.woocommerce .products ul:after,.woocommerce .products ul:before,.woocommerce ul.products:after,.woocommerce ul.products:before{content:" ";display:table}.woocommerce .products ul li,.woocommerce ul.products li{list-style:none}.woocommerce ul.products li.product .onsale{top:0;right:0;left:auto;margin:-.5em -.5em 0 0}.woocommerce ul.products li.product h3{padding:.5em 0;margin:0;font-size:1em}.woocommerce ul.products li.product a{text-decoration:none}.woocommerce ul.products li.product a img{width:100%;height:auto;display:block;margin:0 0 1em;box-shadow:none}.woocommerce ul.products li.product strong{display:block}.woocommerce ul.products li.product .star-rating{font-size:.857em}.woocommerce ul.products li.product .button{margin-top:1em}.woocommerce ul.products li.product .price{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>;display:block;font-weight:400;margin-bottom:.5em;font-size:.857em}.woocommerce ul.products li.product .price del{color:inherit;opacity:.5;display:block}.woocommerce ul.products li.product .price ins{background:0 0;font-weight:700}.woocommerce ul.products li.product .price .from{font-size:.67em;margin:-2px 0 0;text-transform:uppercase;color:rgba(132,132,132,.5)}.woocommerce .woocommerce-ordering,.woocommerce .woocommerce-result-count{margin:0 0 1em}.woocommerce .woocommerce-ordering select{vertical-align:top}.woocommerce nav.woocommerce-pagination{text-align:center}.woocommerce nav.woocommerce-pagination ul{display:inline-block;white-space:nowrap;padding:0;border:1px solid #d3ced2;border-right:0;margin:1px}.woocommerce nav.woocommerce-pagination ul li{border-right:1px solid #d3ced2;padding:0;margin:0;float:left;display:inline;overflow:hidden}.woocommerce nav.woocommerce-pagination ul li a,.woocommerce nav.woocommerce-pagination ul li span{margin:0;text-decoration:none;line-height:1;font-size:1em;font-weight:400;padding:.5em;min-width:1em;display:block}.woocommerce nav.woocommerce-pagination ul li a:focus,.woocommerce nav.woocommerce-pagination ul li a:hover,.woocommerce nav.woocommerce-pagination ul li span.current{background:#ebe9eb;color:#8a7e88}.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button{font-size:100%;margin:0;line-height:1;cursor:pointer;position:relative;text-decoration:none;overflow:visible;padding:.618em 1em;font-weight:700;border-radius:3px;left:auto;color:#515151;background-color:#ebe9eb;border:0;white-space:nowrap;display:inline-block;background-image:none;box-shadow:none;-webkit-box-shadow:none;text-shadow:none}.woocommerce #respond input#submit.loading,.woocommerce a.button.loading,.woocommerce button.button.loading,.woocommerce input.button.loading{opacity:.25;padding-right:2.618em}.woocommerce #respond input#submit.loading:after,.woocommerce a.button.loading:after,.woocommerce button.button.loading:after,.woocommerce input.button.loading:after{font-family:WooCommerce;content:"\e01c";vertical-align:top;-webkit-font-smoothing:antialiased;font-weight:400;position:absolute;top:.618em;right:1em;-webkit-animation:spin 2s linear infinite;-moz-animation:spin 2s linear infinite;animation:spin 2s linear infinite}.woocommerce #respond input#submit.added:after,.woocommerce a.button.added:after,.woocommerce button.button.added:after,.woocommerce input.button.added:after{font-family:WooCommerce;content:"\e017";margin-left:.53em;vertical-align:bottom}.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover{background-color:#dad8da;text-decoration:none;background-image:none;color:#515151}.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt{background-color:<?php echo $sdf_theme_styles['theme_primary_color']; ?>;color:#fff;-webkit-font-smoothing:antialiased}.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover{background-color:#935386;color:#fff}.woocommerce #respond input#submit.alt.disabled,.woocommerce #respond input#submit.alt.disabled:hover,.woocommerce #respond input#submit.alt:disabled,.woocommerce #respond input#submit.alt:disabled:hover,.woocommerce #respond input#submit.alt:disabled[disabled],.woocommerce #respond input#submit.alt:disabled[disabled]:hover,.woocommerce a.button.alt.disabled,.woocommerce a.button.alt.disabled:hover,.woocommerce a.button.alt:disabled,.woocommerce a.button.alt:disabled:hover,.woocommerce a.button.alt:disabled[disabled],.woocommerce a.button.alt:disabled[disabled]:hover,.woocommerce button.button.alt.disabled,.woocommerce button.button.alt.disabled:hover,.woocommerce button.button.alt:disabled,.woocommerce button.button.alt:disabled:hover,.woocommerce button.button.alt:disabled[disabled],.woocommerce button.button.alt:disabled[disabled]:hover,.woocommerce input.button.alt.disabled,.woocommerce input.button.alt.disabled:hover,.woocommerce input.button.alt:disabled,.woocommerce input.button.alt:disabled:hover,.woocommerce input.button.alt:disabled[disabled],.woocommerce input.button.alt:disabled[disabled]:hover{background-color:<?php echo $sdf_theme_styles['theme_primary_color']; ?>;color:#fff}.woocommerce #respond input#submit.disabled,.woocommerce #respond input#submit:disabled,.woocommerce #respond input#submit:disabled[disabled],.woocommerce a.button.disabled,.woocommerce a.button:disabled,.woocommerce a.button:disabled[disabled],.woocommerce button.button.disabled,.woocommerce button.button:disabled,.woocommerce button.button:disabled[disabled],.woocommerce input.button.disabled,.woocommerce input.button:disabled,.woocommerce input.button:disabled[disabled]{color:inherit;cursor:not-allowed;opacity:.5;padding:.618em 1em}.woocommerce #respond input#submit.disabled:hover,.woocommerce #respond input#submit:disabled:hover,.woocommerce #respond input#submit:disabled[disabled]:hover,.woocommerce a.button.disabled:hover,.woocommerce a.button:disabled:hover,.woocommerce a.button:disabled[disabled]:hover,.woocommerce button.button.disabled:hover,.woocommerce button.button:disabled:hover,.woocommerce button.button:disabled[disabled]:hover,.woocommerce input.button.disabled:hover,.woocommerce input.button:disabled:hover,.woocommerce input.button:disabled[disabled]:hover{color:inherit;background-color:#ebe9eb}.woocommerce .cart .button,.woocommerce .cart input.button{float:none}.woocommerce a.added_to_cart{padding-top:.5em;white-space:nowrap;display:inline-block}.woocommerce #reviews #comments .add_review:after,.woocommerce #reviews #comments .add_review:before,.woocommerce #reviews #comments ol.commentlist li .comment-text:after,.woocommerce #reviews #comments ol.commentlist li .comment-text:before,.woocommerce #reviews #comments ol.commentlist:after,.woocommerce #reviews #comments ol.commentlist:before{content:" ";display:table}.woocommerce #reviews h2 small{float:right;color:#777;font-size:15px;margin:10px 0 0}.woocommerce #reviews h2 small a{text-decoration:none;color:#777}.woocommerce #reviews h3{margin:0}.woocommerce #reviews #respond{margin:0;border:0;padding:0}.woocommerce #reviews #comment{height:75px}.woocommerce #reviews #comments h2{clear:none}.woocommerce #review_form #respond:after,.woocommerce #reviews #comments ol.commentlist li .comment-text:after,.woocommerce #reviews #comments ol.commentlist:after,.woocommerce .woocommerce-product-rating:after,.woocommerce td.product-name dl.variation:after{clear:both}.woocommerce #reviews #comments ol.commentlist{margin:0;width:100%;background:0 0;list-style:none}.woocommerce #reviews #comments ol.commentlist li{padding:0;margin:0 0 20px;position:relative;background:0;border:0}.woocommerce #reviews #comments ol.commentlist li .meta{color:#777;font-size:.75em}.woocommerce #reviews #comments ol.commentlist li img.avatar{float:left;position:absolute;top:0;left:0;padding:3px;width:32px;height:auto;background:#ebe9eb;border:1px solid #e4e1e3;margin:0;box-shadow:none}.woocommerce #reviews #comments ol.commentlist li .comment-text{margin:0 0 0 50px;border:1px solid #e4e1e3;border-radius:4px;padding:1em 1em 0}.woocommerce #reviews #comments ol.commentlist li .comment-text p{margin:0 0 1em}.woocommerce #reviews #comments ol.commentlist li .comment-text p.meta{font-size:.83em}.woocommerce #reviews #comments ol.commentlist ul.children{list-style:none;margin:20px 0 0 50px}.woocommerce #reviews #comments ol.commentlist ul.children .star-rating{display:none}.woocommerce #reviews #comments ol.commentlist #respond{border:1px solid #e4e1e3;border-radius:4px;padding:1em 1em 0;margin:20px 0 0 50px}.woocommerce #reviews #comments .commentlist>li:before{content:""}.woocommerce .star-rating{float:right;overflow:hidden;position:relative;height:1em;line-height:1;font-size:1em;width:5.4em;font-family:star}.woocommerce .star-rating:before{content:"\73\73\73\73\73";color:#d3ced2;float:left;top:0;left:0;position:absolute}.woocommerce .star-rating span{overflow:hidden;float:left;top:0;left:0;position:absolute;padding-top:1.5em}.woocommerce .star-rating span:before{content:"\53\53\53\53\53";top:0;position:absolute;left:0}.woocommerce .woocommerce-product-rating{line-height:2;display:block}.woocommerce .woocommerce-product-rating:after,.woocommerce .woocommerce-product-rating:before{content:" ";display:table}.woocommerce .woocommerce-product-rating .star-rating{margin:.5em 4px 0 0;float:left}.woocommerce .products .star-rating{display:block;margin:0 0 .5em;float:none}.woocommerce .hreview-aggregate .star-rating{margin:10px 0 0}.woocommerce #review_form #respond{position:static;margin:0;width:auto;padding:0;background:0 0;border:0}.woocommerce #review_form #respond:after,.woocommerce #review_form #respond:before{content:" ";display:table}.woocommerce p.stars a:before,.woocommerce p.stars a:hover~a:before{content:"\e021"}.woocommerce #review_form #respond p{margin:0 0 10px}.woocommerce #review_form #respond .form-submit input{left:auto}.woocommerce #review_form #respond textarea{box-sizing:border-box;width:100%}.woocommerce p.stars a{position:relative;height:1em;width:1em;text-indent:-999em;display:inline-block;text-decoration:none}.woocommerce p.stars a:before{display:block;position:absolute;top:0;left:0;width:1em;height:1em;line-height:1;font-family:WooCommerce;text-indent:0}.woocommerce table.shop_attributes td,.woocommerce table.shop_attributes th{line-height:1.5;border-bottom:1px dotted rgba(0,0,0,.1);border-top:0;margin:0}.woocommerce p.stars.selected a.active:before,.woocommerce p.stars:hover a:before{content:"\e020"}.woocommerce p.stars.selected a.active~a:before{content:"\e021"}.woocommerce p.stars.selected a:not(.active):before{content:"\e020"}.woocommerce table.shop_attributes{border:0;border-top:1px dotted rgba(0,0,0,.1);margin-bottom:1.618em;width:100%}.woocommerce table.shop_attributes th{width:150px;font-weight:700;padding:8px}.woocommerce table.shop_attributes td{font-style:italic;padding:0}.woocommerce table.shop_attributes td p{margin:0;padding:8px 0}.woocommerce table.shop_attributes .alt td,.woocommerce table.shop_attributes .alt th{background:rgba(0,0,0,.025)}.woocommerce table.shop_table{border:1px solid rgba(0,0,0,.1);margin:0 -1px 24px 0;text-align:left;width:100%;border-collapse:separate;border-radius:5px}.woocommerce table.shop_table th{font-weight:700;padding:9px 12px}.woocommerce table.shop_table td{border-top:1px solid rgba(0,0,0,.1);padding:6px 12px;vertical-align:middle}.woocommerce table.shop_table td small{font-weight:400}.woocommerce table.shop_table tbody:first-child tr:first-child td,.woocommerce table.shop_table tbody:first-child tr:first-child th{border-top:0}.woocommerce table.shop_table tbody th,.woocommerce table.shop_table tfoot td,.woocommerce table.shop_table tfoot th{font-weight:700;border-top:1px solid rgba(0,0,0,.1)}.woocommerce table.my_account_orders{font-size:.85em}.woocommerce table.my_account_orders td,.woocommerce table.my_account_orders th{padding:4px 8px;vertical-align:middle}.woocommerce table.my_account_orders .button{white-space:nowrap}.woocommerce table.my_account_orders .order-actions{text-align:right}.woocommerce table.my_account_orders .order-actions .button{margin:.125em 0 .125em .25em}.woocommerce td.product-name dl.variation{margin:.25em 0}.woocommerce td.product-name dl.variation:after,.woocommerce td.product-name dl.variation:before{content:" ";display:table}.woocommerce td.product-name dl.variation dd,.woocommerce td.product-name dl.variation dt{display:inline-block;float:left;margin-bottom:1em}.woocommerce td.product-name dl.variation dt{font-weight:700;padding:0 0 .25em;margin:0 4px 0 0;clear:left}.woocommerce ul.cart_list li dl:after,.woocommerce ul.cart_list li:after,.woocommerce ul.product_list_widget li dl:after,.woocommerce ul.product_list_widget li:after{clear:both}.woocommerce td.product-name dl.variation dd{padding:0 0 .25em}.woocommerce td.product-name dl.variation dd p:last-child{margin-bottom:0}.woocommerce td.product-name p.backorder_notification{font-size:.83em}.woocommerce td.product-quantity{min-width:80px}.woocommerce ul.cart_list,.woocommerce ul.product_list_widget{list-style:none;padding:0;margin:0}.woocommerce ul.cart_list li,.woocommerce ul.product_list_widget li{padding:4px 0;margin:0;list-style:none}.woocommerce ul.cart_list li:after,.woocommerce ul.cart_list li:before,.woocommerce ul.product_list_widget li:after,.woocommerce ul.product_list_widget li:before{content:" ";display:table}.woocommerce ul.cart_list li a,.woocommerce ul.product_list_widget li a{display:block;font-weight:700}.woocommerce ul.cart_list li img,.woocommerce ul.product_list_widget li img{float:right;margin-left:4px;width:32px;height:auto;box-shadow:none}.woocommerce ul.cart_list li dl,.woocommerce ul.product_list_widget li dl{margin:0;padding-left:1em;border-left:2px solid rgba(0,0,0,.1)}.woocommerce ul.cart_list li dl:after,.woocommerce ul.cart_list li dl:before,.woocommerce ul.product_list_widget li dl:after,.woocommerce ul.product_list_widget li dl:before{content:" ";display:table}.woocommerce ul.cart_list li dl dd,.woocommerce ul.cart_list li dl dt,.woocommerce ul.product_list_widget li dl dd,.woocommerce ul.product_list_widget li dl dt{display:inline-block;float:left;margin-bottom:1em}.woocommerce ul.cart_list li dl dt,.woocommerce ul.product_list_widget li dl dt{font-weight:700;padding:0 0 .25em;margin:0 4px 0 0;clear:left}.woocommerce .order_details:after,.woocommerce .widget_layered_nav ul li:after,.woocommerce .widget_rating_filter ul li:after,.woocommerce .widget_shopping_cart .buttons:after,.woocommerce-account .addresses .title:after,.woocommerce-account .woocommerce:after,.woocommerce-cart .wc-proceed-to-checkout:after,.woocommerce.widget_shopping_cart .buttons:after{clear:both}.woocommerce ul.cart_list li dl dd,.woocommerce ul.product_list_widget li dl dd{padding:0 0 .25em}.woocommerce ul.cart_list li dl dd p:last-child,.woocommerce ul.product_list_widget li dl dd p:last-child{margin-bottom:0}.woocommerce ul.cart_list li .star-rating,.woocommerce ul.product_list_widget li .star-rating{float:none}.woocommerce .widget_shopping_cart .total,.woocommerce.widget_shopping_cart .total{border-top:3px double #ebe9eb;padding:4px 0 0}.woocommerce .widget_shopping_cart .total strong,.woocommerce.widget_shopping_cart .total strong{min-width:40px;display:inline-block}.woocommerce .widget_shopping_cart .cart_list li,.woocommerce.widget_shopping_cart .cart_list li{padding-left:2em;position:relative;padding-top:0}.woocommerce .widget_shopping_cart .cart_list li a.remove,.woocommerce.widget_shopping_cart .cart_list li a.remove{position:absolute;top:0;left:0}.woocommerce .widget_shopping_cart .buttons:after,.woocommerce .widget_shopping_cart .buttons:before,.woocommerce.widget_shopping_cart .buttons:after,.woocommerce.widget_shopping_cart .buttons:before{content:" ";display:table}.woocommerce form .form-row{padding:3px;margin:0 0 6px}.woocommerce form .form-row [placeholder]:focus::-webkit-input-placeholder{-webkit-transition:opacity .5s .5s ease;-moz-transition:opacity .5s .5s ease;transition:opacity .5s .5s ease;opacity:0}.woocommerce form .form-row label{line-height:2}.woocommerce form .form-row label.hidden{visibility:hidden}.woocommerce form .form-row label.inline{display:inline}.woocommerce form .form-row select{cursor:pointer;margin:0}.woocommerce form .form-row .required{color:red;font-weight:700;border:0}.woocommerce form .form-row .input-checkbox{display:inline;margin:-2px 8px 0 0;text-align:center;vertical-align:middle}.woocommerce form .form-row input.input-text,.woocommerce form .form-row textarea{box-sizing:border-box;width:100%;margin:0;outline:0;line-height:1}.woocommerce form .form-row textarea{height:4em;line-height:1.5;display:block;-moz-box-shadow:none;-webkit-box-shadow:none;box-shadow:none}.woocommerce form .form-row .select2-container{width:100%;line-height:2em}.woocommerce form .form-row.woocommerce-invalid label{color:#a00}.woocommerce form .form-row.woocommerce-invalid .select2-container,.woocommerce form .form-row.woocommerce-invalid input.input-text,.woocommerce form .form-row.woocommerce-invalid select{border-color:#a00}.woocommerce form .form-row.woocommerce-validated .select2-container,.woocommerce form .form-row.woocommerce-validated input.input-text,.woocommerce form .form-row.woocommerce-validated select{border-color:#69bf29}.woocommerce form .form-row ::-webkit-input-placeholder{line-height:normal}.woocommerce form .form-row :-moz-placeholder{line-height:normal}.woocommerce form .form-row :-ms-input-placeholder{line-height:normal}.woocommerce form.checkout_coupon,.woocommerce form.login,.woocommerce form.register{border:1px solid #d3ced2;padding:20px;margin:2em 0;text-align:left;border-radius:5px}.woocommerce ul#shipping_method{list-style:none;margin:0;padding:0}.woocommerce ul#shipping_method li{margin:0;padding:.25em 0 .25em 22px;text-indent:-22px;list-style:none}.woocommerce ul#shipping_method li input{margin:3px .5ex}.woocommerce ul#shipping_method li label{display:inline}.woocommerce ul#shipping_method .amount{font-weight:700}.woocommerce p.woocommerce-shipping-contents{margin:0}.woocommerce .order_details{margin:0 0 1.5em;list-style:none}.woocommerce .order_details:after,.woocommerce .order_details:before{content:" ";display:table}.woocommerce .order_details li{float:left;margin-right:2em;text-transform:uppercase;font-size:.715em;line-height:1;border-right:1px dashed #d3ced2;padding-right:2em;margin-left:0;padding-left:0;list-style-type:none}.woocommerce .order_details li strong{display:block;font-size:1.4em;text-transform:none;line-height:1.5}.woocommerce .order_details li:last-of-type{border:none}.woocommerce .widget_layered_nav ul{margin:0;padding:0;border:0;list-style:none}.woocommerce .widget_layered_nav ul li{padding:0 0 1px;list-style:none}.woocommerce .widget_layered_nav ul li:after,.woocommerce .widget_layered_nav ul li:before{content:" ";display:table}.woocommerce .widget_layered_nav ul li.chosen a:before,.woocommerce .widget_layered_nav_filters ul li a:before{font-family:WooCommerce;speak:none;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;content:"";text-decoration:none;font-weight:400;color:#a00}.woocommerce .widget_layered_nav ul li a,.woocommerce .widget_layered_nav ul li span{padding:1px 0}.woocommerce .widget_layered_nav ul li.chosen a:before{margin-right:.618em}.woocommerce .widget_layered_nav_filters ul{margin:0;padding:0;border:0;list-style:none;overflow:hidden;zoom:1}.woocommerce .widget_layered_nav_filters ul li{float:left;padding:0 1px 1px 0;list-style:none}.woocommerce .widget_layered_nav_filters ul li a{text-decoration:none}.woocommerce .widget_layered_nav_filters ul li a:before{margin-right:.618em}.woocommerce .widget_price_filter .price_slider{margin-bottom:1em}.woocommerce .widget_price_filter .price_slider_amount{text-align:right;line-height:2.4;font-size:.8751em}.woocommerce .widget_price_filter .price_slider_amount .button{font-size:1.15em;float:left}.woocommerce .widget_price_filter .ui-slider{position:relative;text-align:left;margin-left:.5em;margin-right:.5em}.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{position:absolute;z-index:2;width:1em;height:1em;background-color:<?php echo $sdf_theme_styles['theme_primary_color']; ?>;border-radius:1em;cursor:ew-resize;outline:0;top:-.3em;margin-left:-.5em}.woocommerce .widget_price_filter .ui-slider .ui-slider-range{position:absolute;z-index:1;font-size:.7em;display:block;border:0;border-radius:1em;background-color:<?php echo $sdf_theme_styles['theme_primary_color']; ?>}.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content{border-radius:1em;background-color:#602053;border:0}.woocommerce .widget_price_filter .ui-slider-horizontal{height:.5em}.woocommerce .widget_price_filter .ui-slider-horizontal .ui-slider-range{top:0;height:100%}.woocommerce .widget_price_filter .ui-slider-horizontal .ui-slider-range-min{left:-1px}.woocommerce .widget_price_filter .ui-slider-horizontal .ui-slider-range-max{right:-1px}.woocommerce .widget_rating_filter ul{margin:0;padding:0;border:0;list-style:none}.woocommerce .widget_rating_filter ul li.chosen a:before,.woocommerce-account ul.digital-downloads li:before{margin-right:.618em;font-family:WooCommerce;speak:none;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;text-decoration:none}.woocommerce .widget_rating_filter ul li{padding:0 0 1px;list-style:none}.woocommerce .widget_rating_filter ul li:after,.woocommerce .widget_rating_filter ul li:before{content:" ";display:table}.woocommerce .widget_rating_filter ul li a{padding:1px 0;text-decoration:none}.woocommerce .widget_rating_filter ul li .star-rating{float:none;display:inline-block}.woocommerce .widget_rating_filter ul li.chosen a:before{font-weight:400;content:"";color:#a00}.woocommerce-account .addresses .title:after,.woocommerce-account .addresses .title:before,.woocommerce-account .woocommerce:after,.woocommerce-account .woocommerce:before{content:" ";display:table}.woocommerce-account .woocommerce-MyAccount-navigation{float:left;width:30%}.woocommerce-account .woocommerce-MyAccount-content{float:right;width:68%}.woocommerce-account .addresses .title h3{float:left}.woocommerce-account .addresses .title .edit,.woocommerce-account ul.digital-downloads li .count{float:right}.woocommerce-account ol.commentlist.notes li.note p.meta{font-weight:700;margin-bottom:0}.woocommerce-account ol.commentlist.notes li.note .description p:last-child{margin-bottom:0}.woocommerce-account ul.digital-downloads{margin-left:0;padding-left:0}.woocommerce-account ul.digital-downloads li{list-style:none;margin-left:0;padding-left:0}.woocommerce-account ul.digital-downloads li:before{font-weight:400;content:""}.woocommerce-cart table.cart .product-thumbnail{min-width:32px}.woocommerce-cart table.cart img{width:32px;box-shadow:none}.woocommerce-cart table.cart td,.woocommerce-cart table.cart th{vertical-align:middle}.woocommerce-cart table.cart td.actions .coupon .input-text{float:left;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:1px solid #d3ced2;padding:6px 6px 5px;margin:0 4px 0 0;outline:0;line-height:1}.woocommerce-cart table.cart input{margin:0;vertical-align:middle;line-height:1}.woocommerce-cart .wc-proceed-to-checkout{padding:1em 0}.woocommerce-cart .wc-proceed-to-checkout:after,.woocommerce-cart .wc-proceed-to-checkout:before{content:" ";display:table}.woocommerce-cart .wc-proceed-to-checkout a.checkout-button{display:block;text-align:center;margin-bottom:1em;font-size:1.25em;padding:1em}.woocommerce-cart .cart-collaterals .shipping_calculator .button{width:100%;float:none;display:block}.woocommerce-cart .cart-collaterals .shipping_calculator .shipping-calculator-button:after{font-family:WooCommerce;speak:none;font-weight:400;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;margin-left:.618em;content:"";text-decoration:none}#add_payment_method #payment ul.payment_methods li:after,#add_payment_method #payment ul.payment_methods li:before,#add_payment_method #payment ul.payment_methods:after,#add_payment_method #payment ul.payment_methods:before,.woocommerce-checkout #payment ul.payment_methods li:after,.woocommerce-checkout #payment ul.payment_methods li:before,.woocommerce-checkout #payment ul.payment_methods:after,.woocommerce-checkout #payment ul.payment_methods:before{content:" ";display:table}.woocommerce-cart .cart-collaterals .cart_totals p small{color:#777;font-size:.83em}.woocommerce-cart .cart-collaterals .cart_totals table{border-collapse:separate;margin:0 0 6px;padding:0}.woocommerce-cart .cart-collaterals .cart_totals table tr:first-child td,.woocommerce-cart .cart-collaterals .cart_totals table tr:first-child th{border-top:0}.woocommerce-cart .cart-collaterals .cart_totals table th{width:40%}.woocommerce-cart .cart-collaterals .cart_totals table td,.woocommerce-cart .cart-collaterals .cart_totals table th{vertical-align:top;border-left:0;border-right:0;line-height:1.5em}.woocommerce-cart .cart-collaterals .cart_totals table small{color:#777}.woocommerce-cart .cart-collaterals .cart_totals table select{width:100%}.woocommerce-cart .cart-collaterals .cart_totals .discount td{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>}.woocommerce-cart .cart-collaterals .cart_totals tr td,.woocommerce-cart .cart-collaterals .cart_totals tr th{border-top:1px solid #ebe9eb}.woocommerce-cart .cart-collaterals .cross-sells ul.products li.product{margin-top:0}#add_payment_method .checkout .col-2 h3#ship-to-different-address,.woocommerce-checkout .checkout .col-2 h3#ship-to-different-address{float:left;clear:none}#add_payment_method .checkout .col-2 .form-row-first,#add_payment_method .checkout .col-2 .notes,.woocommerce-checkout .checkout .col-2 .form-row-first,.woocommerce-checkout .checkout .col-2 .notes{clear:left}#add_payment_method .checkout .create-account small,.woocommerce-checkout .checkout .create-account small{font-size:11px;color:#777;font-weight:400}#add_payment_method .checkout div.shipping-address,.woocommerce-checkout .checkout div.shipping-address{padding:0;clear:left;width:100%}#add_payment_method #payment ul.payment_methods li:after,#add_payment_method #payment ul.payment_methods:after,#add_payment_method .checkout .shipping_address,.single-product .twentythirteen p.stars,.woocommerce-checkout #payment ul.payment_methods li:after,.woocommerce-checkout #payment ul.payment_methods:after,.woocommerce-checkout .checkout .shipping_address{clear:both}#add_payment_method #payment,.woocommerce-checkout #payment{background:#ebe9eb;border-radius:5px}#add_payment_method #payment ul.payment_methods,.woocommerce-checkout #payment ul.payment_methods{text-align:left;padding:1em;border-bottom:1px solid #d3ced2;margin:0;list-style:none}#add_payment_method #payment ul.payment_methods li,.woocommerce-checkout #payment ul.payment_methods li{line-height:2;text-align:left;margin:0;font-weight:400}#add_payment_method #payment ul.payment_methods li input,.woocommerce-checkout #payment ul.payment_methods li input{margin:0 1em 0 0}#add_payment_method #payment ul.payment_methods li img,.woocommerce-checkout #payment ul.payment_methods li img{vertical-align:middle;margin:-2px 0 0 .5em;padding:0;position:relative;box-shadow:none}#add_payment_method #payment ul.payment_methods li img+img,.woocommerce-checkout #payment ul.payment_methods li img+img{margin-left:2px}#add_payment_method #payment div.form-row,.woocommerce-checkout #payment div.form-row{padding:1em}#add_payment_method #payment div.payment_box,.woocommerce-checkout #payment div.payment_box{position:relative;box-sizing:border-box;width:100%;padding:1em;margin:1em 0;font-size:.92em;border-radius:2px;line-height:1.5;background-color:#dfdcde;color:#515151}#add_payment_method #payment div.payment_box input.input-text,#add_payment_method #payment div.payment_box textarea,.woocommerce-checkout #payment div.payment_box input.input-text,.woocommerce-checkout #payment div.payment_box textarea{border-color:#bbb3b9 #c7c1c6 #c7c1c6}#add_payment_method #payment div.payment_box ::-webkit-input-placeholder,.woocommerce-checkout #payment div.payment_box ::-webkit-input-placeholder{color:#bbb3b9}#add_payment_method #payment div.payment_box :-moz-placeholder,.woocommerce-checkout #payment div.payment_box :-moz-placeholder{color:#bbb3b9}#add_payment_method #payment div.payment_box :-ms-input-placeholder,.woocommerce-checkout #payment div.payment_box :-ms-input-placeholder{color:#bbb3b9}#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods{list-style:none;margin:0}#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-new,#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-token,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-new,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-token{margin:0 0 .5em}#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-new label,#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-token label,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-new label,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-token label{cursor:pointer}#add_payment_method #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-tokenInput,.woocommerce-checkout #payment div.payment_box .woocommerce-SavedPaymentMethods .woocommerce-SavedPaymentMethods-tokenInput{vertical-align:middle;margin:-3px 1em 0 0;position:relative}#add_payment_method #payment div.payment_box .wc-credit-card-form,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form{border:0;padding:0;margin:1em 0 0}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number{font-size:1.5em;padding:8px;background-repeat:no-repeat;background-position:right .618em center;background-size:32px 20px}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.visa,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.visa,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.visa,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.visa,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.visa,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.visa{background-image:url(../images/icons/credit-cards/visa.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.mastercard,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.mastercard,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.mastercard,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.mastercard,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.mastercard,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.mastercard{background-image:url(../images/icons/credit-cards/mastercard.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.laser,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.laser,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.laser,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.laser,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.laser,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.laser{background-image:url(../images/icons/credit-cards/laser.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.dinersclub,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.dinersclub,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.dinersclub,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.dinersclub,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.dinersclub,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.dinersclub{background-image:url(../images/icons/credit-cards/diners.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.maestro,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.maestro,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.maestro,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.maestro,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.maestro,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.maestro{background-image:url(../images/icons/credit-cards/maestro.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.jcb,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.jcb,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.jcb,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.jcb,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.jcb,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.jcb{background-image:url(../images/icons/credit-cards/jcb.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.amex,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.amex,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.amex,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.amex,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.amex,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.amex{background-image:url(../images/icons/credit-cards/amex.svg)}#add_payment_method #payment div.payment_box .wc-credit-card-form-card-cvc.discover,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-expiry.discover,#add_payment_method #payment div.payment_box .wc-credit-card-form-card-number.discover,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-cvc.discover,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-expiry.discover,.woocommerce-checkout #payment div.payment_box .wc-credit-card-form-card-number.discover{background-image:url(../images/icons/credit-cards/discover.svg)}#add_payment_method #payment div.payment_box span.help,.woocommerce-checkout #payment div.payment_box span.help{font-size:.857em;color:#777;font-weight:400}#add_payment_method #payment div.payment_box .form-row,.woocommerce-checkout #payment div.payment_box .form-row{margin:0 0 1em}#add_payment_method #payment div.payment_box p:last-child,.woocommerce-checkout #payment div.payment_box p:last-child{margin-bottom:0}#add_payment_method #payment div.payment_box:before,.woocommerce-checkout #payment div.payment_box:before{content:"";display:block;border:1em solid #dfdcde;border-right-color:transparent;border-left-color:transparent;border-top-color:transparent;position:absolute;top:-.75em;left:0;margin:-1em 0 0 2em}#add_payment_method #payment .payment_method_paypal .about_paypal,.woocommerce-checkout #payment .payment_method_paypal .about_paypal{float:right;line-height:52px;font-size:.83em}#add_payment_method #payment .payment_method_paypal img,.woocommerce-checkout #payment .payment_method_paypal img{max-height:52px;vertical-align:middle}.woocommerce-password-strength{text-align:center;font-weight:600;padding:3px .5em;font-size:1em}.woocommerce-password-strength.strong{background-color:#c1e1b9;border-color:#83c373}.woocommerce-password-strength.short{background-color:#f1adad;border-color:#e35b5b}.woocommerce-password-strength.bad{background-color:#fbc5a9;border-color:#f78b53}.woocommerce-password-strength.good{background-color:#ffe399;border-color:#ffc733}.woocommerce-password-hint{margin:.5em 0 0;display:block}.product.has-default-attributes.has-children>.images{opacity:0}#content.twentyeleven .woocommerce-pagination a{font-size:1em;line-height:1}.single-product .twentythirteen #reply-title,.single-product .twentythirteen #respond #commentform,.single-product .twentythirteen .entry-summary{padding:0}.twentythirteen .woocommerce-breadcrumb{padding-top:40px}.twentyfourteen ul.products li.product{margin-top:0!important}body:not(.search-results) .twentysixteen .entry-summary{color:inherit;font-size:inherit;line-height:inherit}.twentysixteen .price ins{background:inherit;color:inherit}
/* SDF Custom Woo CSS */
.woocommerce ul.products li:not(:first-child) {clear: none;}
.woocommerce ul.products li.first {clear: left;}
.woocommerce ul.products li:not(.first) {clear: none;}
.woocommerce table.my_account_orders td hr { display: none;}

.woocommerce #content div.product div.images,
.woocommerce #content div.product div.summary,
.woocommerce div.product div.images,
.woocommerce div.product div.summary,
.woocommerce-page #content div.product div.images,
.woocommerce-page #content div.product div.summary,
.woocommerce-page div.product div.images,
.woocommerce-page div.product div.summary {float:none;width:100%}
<?php endif; ?>
<?php if ( defined( 'ICL_SITEPRESS_VERSION' ) ) :  ?>

/* WPML styles */

/* Clone SDF styles */

#lang_sel li {
	width: auto !important;
}

#lang_sel a {
	border: none  !important;
	line-height: inherit !important;
	padding-left: 15px !important;
	width: auto !important;
	background: none !important;
	<?php get_font_family_weight($sdf_theme_styles['theme_font_family'], ''); ?>
	<?php if($sdf_theme_styles['theme_font_size']): ?>
	font-size:<?php echo $sdf_theme_styles['theme_font_size']; ?>;
	<?php endif; ?>
}

#lang_sel ul ul li,
#lang_sel_click ul ul li {
	width: 100%;
}

#lang_sel_footer > ul > li > a,
#lang_sel > ul > li > a,
#lang_sel_click > ul > li > a {
	z-index: 1000;
}

#lang_sel ul ul,
#lang_sel_click ul ul {
	z-index:1001;
	width: auto !important;
	height: auto !important;
}

#sdf-copyright #lang_sel ul ul,
#sdf-copyright #lang_sel_click ul ul {
	z-index:1002;
}

#sdf-footer #lang_sel ul ul,
#sdf-footer #lang_sel_click ul ul {
	z-index:1003;
}

#below-content-area #lang_sel ul ul,
#below-content-area #lang_sel_click ul ul {
	z-index:1004;
}

#content-wrap #lang_sel ul ul,
#content-wrap #lang_sel_click ul ul {
	z-index:1005;
}

#above-content-area #lang_sel ul ul,
#above-content-area #lang_sel_click ul ul {
	z-index:1006;
}

#sdf-header #lang_sel ul ul,
#sdf-header #lang_sel_click ul ul {
	z-index:1007;
}

#navigation-area #lang_sel > ul > li > a,
#navigation-area-2 #lang_sel > ul > li > a {
	<?php if($sdf_theme_styles['theme_navbar_size'] || $sdf_theme_styles['theme_navbar_family'] || $sdf_theme_styles['theme_navbar_weight']): ?>
		<?php get_font_family_weight($sdf_theme_styles['theme_navbar_family'], $sdf_theme_styles['theme_navbar_weight']); ?>
		<?php if($sdf_theme_styles['theme_navbar_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_navbar_size']; ?><?php echo $sdf_theme_styles['theme_navbar_size_type']; ?>;<?php } ?>
	<?php endif; ?>
	<?php if($sdf_theme_styles['theme_navbar_transform'] != 'no'): ?>
		text-transform: <?php echo $sdf_theme_styles['theme_navbar_transform']; ?>;
	<?php endif; ?>
	color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
	<?php if($sdf_theme_styles['theme_navbar_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_navbar_letter_spacing']; ?>;<?php endif; ?>
	text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_link_text_decoration']) ? $sdf_theme_styles['theme_navbar_link_text_decoration'] : 'none'; ?> !important;
}

#navigation-area #lang_sel > ul > li > a:hover,
#navigation-area #lang_sel > ul > li > a:focus,
#navigation-area-2 #lang_sel > ul > li > a:hover,
#navigation-area-2 #lang_sel > ul > li > a:focus {
	color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_link_hover_bg_color'] : 'transparent'; ?>;
	text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_text_decoration']) ? $sdf_theme_styles['theme_navbar_link_hover_text_decoration'] : 'none'; ?> !important;
}

#lang_sel > ul ul {
	background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_color']) ? $sdf_theme_styles['theme_navbar_drop_color'] : 'transparent'; ?>;
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
	box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
	<?php if($sdf_theme_styles['theme_navbar_drop_border_color']) : ?>border-color:<?php echo $sdf_theme_styles['theme_navbar_drop_border_color']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles['theme_navbar_drop_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_navbar_drop_border_style']; ?>;<?php endif; ?>
	border-width:<?php echo ($sdf_theme_styles['theme_navbar_drop_border_width'] != '') ? $sdf_theme_styles['theme_navbar_drop_border_width'] : '0'; ?>;
	-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
	-moz-border-radius: <?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
	border-radius: <?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
}

#lang_sel > ul ul,
#lang_sel > ul ul a {
  display: block;
<?php if($sdf_theme_styles['theme_navbar_drop_size'] || $sdf_theme_styles['theme_navbar_drop_family'] || $sdf_theme_styles['theme_navbar_drop_weight']): ?>
	<?php if($sdf_theme_styles['theme_navbar_drop_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_navbar_drop_size']; ?><?php echo $sdf_theme_styles['theme_navbar_drop_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_navbar_drop_family'], $sdf_theme_styles['theme_navbar_drop_weight']); ?>
<?php endif;
if($sdf_theme_styles['theme_navbar_drop_transform'] != 'no'): ?>
	text-transform: <?php echo $sdf_theme_styles['theme_navbar_drop_transform']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_drop_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_navbar_drop_letter_spacing']; ?>;<?php endif; ?>
}
#lang_sel > ul ul a {
  display: block;
	white-space: nowrap;
  text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_drop_text_decoration']) ? $sdf_theme_styles['theme_navbar_drop_text_decoration'] : 'none'; ?> !important;
	padding: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_padding']) ? $sdf_theme_styles['theme_navbar_drop_link_padding'] : '0 15px'; ?>;
  height: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_height']) ? $sdf_theme_styles['theme_navbar_drop_link_height'] : '32px'; ?>;
  line-height: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_height']) ? $sdf_theme_styles['theme_navbar_drop_link_height'] : '32px'; ?>;
	<?php if($sdf_theme_styles['theme_navbar_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
<?php endif; ?>
}
#lang_sel > ul > li:hover > ul {
  opacity: 1;
  top: 100%;
  visibility: visible;
  -webkit-transform: scale(1, 1);
  display: block\9;
}

#lang_sel > ul > li > ul {
    margin-top: 0;
    padding-left: 0;
    top: 80%;
}

#lang_sel > ul ul {
	left: 0;
	list-style-type: none;
	margin: 0;
	opacity: 0;
	position: absolute;
	top: 0;
	min-width: 170px;
	-webkit-transform: scale(1, 0.99);
	-webkit-transform-origin: 0 0;
	visibility: hidden;
	-webkit-backface-visibility: hidden;
		<?php if($sdf_theme_styles['theme_navbar_drop_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
<?php endif; ?>
}

#lang_sel > ul ul li {
    padding: 0;
    position: relative;
}

<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
#lang_sel > ul ul {
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
}
#lang_sel > ul ul li:first-child {
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px 0 0;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px 0 0;
}
#lang_sel > ul ul li:last-child {
	-webkit-border-radius: 0 0 <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: 0 0 <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
}
#lang_sel > ul ul a {
  -webkit-border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - 3); ?>px;
  border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - 3); ?>px;
}  
<?php endif; ?>

@media (min-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint']; ?>) {
#lang_sel > ul ul {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_color']) ? $sdf_theme_styles['theme_navbar_drop_color'] : 'transparent'; ?>;
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
}

#lang_sel > ul > li > ul li a {
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?> !important;
}
#lang_sel > ul > li > ul li a:hover {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_hover_color']) ? $sdf_theme_styles['theme_navbar_drop_hover_color'] : 'transparent'; ?>;
  color: <?php echo $sdf_theme_styles['theme_navbar_drop_ahover']; ?> !important;
  text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_drop_hover_text_decoration']) ? $sdf_theme_styles['theme_navbar_drop_hover_text_decoration'] : 'none'; ?> !important;
}

#lang_sel > ul > li > ul li a:focus {
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_ahover']; ?>;
}
}

@media (max-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint_max']; ?>) {
	#lang_sel > ul ul a {
		color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
	}
	#lang_sel > ul ul a:hover,
	#lang_sel > ul ul a:focus,
	#lang_sel > ul ul .current-menu-item a {
		color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
	}

  #lang_sel > ul > li > ul {
    height: auto;
    margin: 0 0 0 20px;
	opacity: 1;
	top: 100%;
	visibility: visible;
	-webkit-transform: scale(1, 1);
	display: block\9;
  }
  #lang_sel > ul > li > ul li ul {
    height: auto;
	 opacity: 1;
  -webkit-transform: scale(1, 1);
  visibility: visible;
  display: block\9;
  }
  #lang_sel > ul > li ul {
    background: none;
    height: auto;
    padding: 0;
    position: relative;
    width: auto;
  }
  #lang_sel > ul > li ul:before,
  #lang_sel > ul > li ul:after {
    display: none;
  }
  #lang_sel > ul > li ul li {
    background: none;
  }
  #lang_sel > ul a {
    display: block;
  }

}

/* End of clone SDF styles */

#lang_sel ul > li a.lang_sel_sel,
#lang_sel_click ul > li a.lang_sel_sel {
	background: 0;
	border: 0;
	padding: 0;
}

.pull-right #lang_sel ul > li a.lang_sel_sel,
.pull-right #lang_sel_click ul > li a.lang_sel_sel {
    text-align: right;
}

.pull-right #lang_sel ul > li a.lang_sel_sel {
    padding-right: 0 !important;
}

#lang_sel ul > li a.lang_sel_sel {
    padding-left: 0 !important;
}

#lang_sel .lang_sel_sel:after,
#lang_sel_click .lang_sel_sel:after {
    content: "\f107";
    font-family: 'FontAwesome', sans-serif;
    margin-left: 5px;
		display: inline-block;
		text-decoration: none !important;
}

#lang_sel img.iclflag,
#lang_sel_click img.iclflag,
#lang_sel_list img.iclflag {
	display: inline;
	float: none;
	top: 0px;
	position: relative;
	margin-right: 5px;
}

#lang_sel_list.lang_sel_list_vertical ul {
    height: auto;
    border-top: none;
}

#lang_sel_list.lang_sel_list_vertical a,
#lang_sel_list.lang_sel_list_vertical a:visited {
    border: none;
    padding: 0;
}

#lang_sel_list.lang_sel_list_vertical a,
#lang_sel_list.lang_sel_list_vertical a:visited,
#lang_sel_list.lang_sel_list_horizontal a,
#lang_sel_list.lang_sel_list_horizontal a:visited {
    background: transparent;
}


#lang_sel_footer ul li a:hover,
footer #lang_sel_list.lang_sel_list_horizontal a:hover,
footer #lang_sel_list.lang_sel_list_vertical a:hover {
	<?php if($sdf_theme_styles['footer_bottom_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_link_hover_color']; ?>;<?php } ?>
}
#lang_sel_footer ul li a,
footer #lang_sel_list.lang_sel_list_horizontal a,
footer #lang_sel_list.lang_sel_list_vertical a {
	<?php if($sdf_theme_styles['footer_bottom_link_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_link_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles['footer_font_family'] || $sdf_theme_styles['footer_font_size']): ?>
		<?php get_font_family_weight($sdf_theme_styles['footer_font_family'], ''); ?>
		<?php if($sdf_theme_styles['footer_font_size']): ?>
		font-size:<?php echo $sdf_theme_styles['footer_font_size']; ?><?php echo $sdf_theme_styles['footer_font_size_type']; ?>;
		<?php endif; ?>
	<?php else: ?>
		<?php get_font_family_weight($sdf_theme_styles['theme_font_family'], ''); ?>
		<?php if($sdf_theme_styles['theme_font_size']): ?>
		font-size:<?php echo $sdf_theme_styles['theme_font_size']; ?>;
		<?php endif; ?>
	<?php endif; ?>
}

#lang_sel_footer {
	background:<?php if($sdf_theme_styles['footer_bottom_bg_color']): echo $sdf_theme_styles['footer_bottom_bg_color']; endif; if($sdf_theme_styles['footer_bottom_bg_image']):
	echo ' url('.$sdf_theme_styles['footer_bottom_bg_image'].') '.$sdf_theme_styles['footer_bottom_bgrepeat']; endif;
	?>;
	border: none;
	z-index: 1000;
	padding: 15px;
	position: relative;
	<?php if($sdf_theme_styles['footer_font_family'] || $sdf_theme_styles['footer_font_size']): ?>
		<?php get_font_family_weight($sdf_theme_styles['footer_font_family'], ''); ?>
		<?php if($sdf_theme_styles['footer_font_size']): ?>
		font-size:<?php echo $sdf_theme_styles['footer_font_size']; ?><?php echo $sdf_theme_styles['footer_font_size_type']; ?>;
		<?php endif; ?>
	<?php else: ?>
		<?php get_font_family_weight($sdf_theme_styles['theme_font_family'], ''); ?>
		<?php if($sdf_theme_styles['theme_font_size']): ?>
		font-size:<?php echo $sdf_theme_styles['theme_font_size']; ?>;
		<?php endif; ?>
	<?php endif; ?>

}

#lang_sel_footer ul li img {
	top: 0px;
	margin-right: 15px;
}
/* End of WPML styles */
<?php endif; ?>

/* Counter module */
.counter_wrap{
display:block;
opacity:0;
filter:alpha(opacity=0);
-webkit-transition:opacity .4s ease 0s;
-moz-transition:opacity .4s ease 0s;
-o-transition:opacity .4s ease 0s;
}
.counter_wrap .counter{
line-height:1em;
display:inline-block!important;
height:1em}

/* 	Retina | iPad | etc.. */
@supports( -webkit-text-size-adjust:none ) and ( not (-ms-accelerator:true) ) {
	html { background-attachment: scroll !important;}
}

/* Image hover styles */

/* Common style */
figure.image-module {
	position: relative;
	float: left;
	overflow: hidden;
	margin: 0;
	width: 100%;
	text-align: center;
}
figure[class*="effect"] {
	background: #3085a3;
}

figure[class*="effect"] img {
	position: relative;
	opacity: 0.8;
}

figure.image-module figcaption {
	padding: 2em;
	color: #fff;
	text-transform: uppercase;
	font-size: 1.25em;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.image-module figcaption::before,
figure.image-module figcaption::after {
	pointer-events: none;
}

figure.image-module figcaption,
figure.image-module figcaption > .view-more,
figure.image-module figcaption > .view-more > a {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
}

/* Anchor will cover the whole item by default */
/* For some effects it will show as a button */
figure.image-module figcaption > .view-more,
figure.image-module figcaption > .view-more > a {
	z-index: 2;
	text-indent: 200%;
	white-space: nowrap;
	font-size: 0;
	opacity: 0;
}

figure.image-module h3 {
	color: #fff;
	word-spacing: -0.15em;
	font-weight: 300;
}

figure.image-module h3 span {
	font-weight: 800;
}

figure.image-module h3,
figure.image-module p {
	margin: 0;
}

figure.image-module p {
	letter-spacing: 1px;
	font-size: 68.5%;
}

/* Individual effects */

/*---------------*/
/***** Lily *****/
/*---------------*/

figure.effect-lily img {
	max-width: none;
	width: -webkit-calc(100% + 50px);
	width: calc(100% + 50px);
	opacity: 0.7;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-40px,0, 0);
	transform: translate3d(-40px,0,0);
}

figure.effect-lily figcaption {
	text-align: left;
}

figure.effect-lily figcaption > div {
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 2em;
	width: 100%;
	height: 50%;
}

figure.effect-lily h3,
figure.effect-lily p {
	-webkit-transform: translate3d(0,40px,0);
	transform: translate3d(0,40px,0);
}

figure.effect-lily h3 {
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-lily p {
	color: rgba(255,255,255,0.8);
	opacity: 0;
	-webkit-transition: opacity 0.2s, -webkit-transform 0.35s;
	transition: opacity 0.2s, transform 0.35s;
}

figure.effect-lily:hover img,
figure.effect-lily:hover p {
	opacity: 1;
}

figure.effect-lily:hover img,
figure.effect-lily:hover h3,
figure.effect-lily:hover p {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-lily:hover p {
	-webkit-transition-delay: 0.05s;
	transition-delay: 0.05s;
	-webkit-transition-duration: 0.35s;
	transition-duration: 0.35s;
}

/*---------------*/
/***** Sadie *****/
/*---------------*/

figure.effect-sadie figcaption::before {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: -webkit-linear-gradient(top, rgba(72,76,97,0) 0%, rgba(72,76,97,0.8) 75%);
	background: linear-gradient(to bottom, rgba(72,76,97,0) 0%, rgba(72,76,97,0.8) 75%);
	content: '';
	opacity: 0;
	-webkit-transform: translate3d(0,50%,0);
	transform: translate3d(0,50%,0);
}

figure.effect-sadie h3 {
	position: absolute;
	top: 50%;
	left: 0;
	width: 100%;
	color: #484c61;
	-webkit-transition: -webkit-transform 0.35s, color 0.35s;
	transition: transform 0.35s, color 0.35s;
	-webkit-transform: translate3d(0,-50%,0);
	transform: translate3d(0,-50%,0);
}

figure.effect-sadie figcaption::before,
figure.effect-sadie p {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-sadie p {
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 2em;
	width: 100%;
	opacity: 0;
	-webkit-transform: translate3d(0,10px,0);
	transform: translate3d(0,10px,0);
}

figure.effect-sadie:hover h3 {
	color: #fff;
	-webkit-transform: translate3d(0,-50%,0) translate3d(0,-40px,0);
	transform: translate3d(0,-50%,0) translate3d(0,-40px,0);
}

figure.effect-sadie:hover figcaption::before ,
figure.effect-sadie:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Roxy *****/
/*---------------*/

figure.effect-roxy {
	background: -webkit-linear-gradient(45deg, #ff89e9 0%, #05abe0 100%);
	background: linear-gradient(45deg, #ff89e9 0%,#05abe0 100%);
}

figure.effect-roxy img {
	max-width: none;
	width: -webkit-calc(100% + 60px);
	width: calc(100% + 60px);
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-50px,0,0);
	transform: translate3d(-50px,0,0);
}

figure.effect-roxy figcaption::before {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	border: 1px solid #fff;
	content: '';
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-20px,0,0);
	transform: translate3d(-20px,0,0);
}

figure.effect-roxy figcaption {
	padding: 3em;
	text-align: left;
}

figure.effect-roxy h3 {
	padding: 30% 0 10px 0;
}

figure.effect-roxy p {
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-10px,0,0);
	transform: translate3d(-10px,0,0);
}

figure.effect-roxy:hover img {
	opacity: 0.7;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-roxy:hover figcaption::before,
figure.effect-roxy:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Bubba *****/
/*---------------*/

figure.effect-bubba {
	background: #9e5406;
}

figure.effect-bubba img {
	opacity: 0.7;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-bubba:hover img {
	opacity: 0.4;
}

figure.effect-bubba figcaption::before,
figure.effect-bubba figcaption::after {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	content: '';
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-bubba figcaption::before {
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	-webkit-transform: scale(0,1);
	transform: scale(0,1);
}

figure.effect-bubba figcaption::after {
	border-right: 1px solid #fff;
	border-left: 1px solid #fff;
	-webkit-transform: scale(1,0);
	transform: scale(1,0);
}

figure.effect-bubba h3 {
	padding-top: 30%;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,-20px,0);
	transform: translate3d(0,-20px,0);
}

figure.effect-bubba p {
	padding: 20px 2.5em;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,20px,0);
	transform: translate3d(0,20px,0);
}

figure.effect-bubba:hover figcaption::before,
figure.effect-bubba:hover figcaption::after {
	opacity: 1;
	-webkit-transform: scale(1);
	transform: scale(1);
}

figure.effect-bubba:hover h3,
figure.effect-bubba:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Romeo *****/
/*---------------*/

figure.effect-romeo {
	-webkit-perspective: 1000px;
	perspective: 1000px;
}

figure.effect-romeo img {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,0,300px);
	transform: translate3d(0,0,300px);
}

figure.effect-romeo:hover img {
	opacity: 0.6;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-romeo figcaption::before,
figure.effect-romeo figcaption::after {
	position: absolute;
	top: 50%;
	left: 50%;
	width: 80%;
	height: 1px;
	background: #fff;
	content: '';
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-50%,-50%,0);
	transform: translate3d(-50%,-50%,0);
}

figure.effect-romeo:hover figcaption::before {
	opacity: 0.5;
	-webkit-transform: translate3d(-50%,-50%,0) rotate(45deg);
	transform: translate3d(-50%,-50%,0) rotate(45deg);
}

figure.effect-romeo:hover figcaption::after {
	opacity: 0.5;
	-webkit-transform: translate3d(-50%,-50%,0) rotate(-45deg);
	transform: translate3d(-50%,-50%,0) rotate(-45deg);
}

figure.effect-romeo h3,
figure.effect-romeo p {
	position: absolute;
	top: 50%;
	left: 0;
	width: 100%;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-romeo h3 {
	-webkit-transform: translate3d(0,-50%,0) translate3d(0,-150%,0);
	transform: translate3d(0,-50%,0) translate3d(0,-150%,0);
}

figure.effect-romeo p {
	padding: 0.25em 2em;
	-webkit-transform: translate3d(0,-50%,0) translate3d(0,150%,0);
	transform: translate3d(0,-50%,0) translate3d(0,150%,0);
}

figure.effect-romeo:hover h3 {
	-webkit-transform: translate3d(0,-50%,0) translate3d(0,-100%,0);
	transform: translate3d(0,-50%,0) translate3d(0,-100%,0);
}

figure.effect-romeo:hover p {
	-webkit-transform: translate3d(0,-50%,0) translate3d(0,100%,0);
	transform: translate3d(0,-50%,0) translate3d(0,100%,0);
}


/*---------------*/
/***** Honey *****/
/*---------------*/

figure.effect-honey {
	background: #4a3753;
}

figure.effect-honey img {
	opacity: 0.9;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-honey:hover img {
	opacity: 0.5;
}

figure.effect-honey figcaption::before {
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 10px;
	background: #fff;
	content: '';
	-webkit-transform: translate3d(0,10px,0);
	transform: translate3d(0,10px,0);
}

figure.effect-honey h3 {
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 1em 1.5em;
	width: 100%;
	text-align: left;
	-webkit-transform: translate3d(0,-30px,0);
	transform: translate3d(0,-30px,0);
}

figure.effect-honey h3 i {
	font-style: normal;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,-30px,0);
	transform: translate3d(0,-30px,0);
}

figure.effect-honey figcaption::before,
figure.effect-honey h3 {
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-honey:hover figcaption::before,
figure.effect-honey:hover h3,
figure.effect-honey:hover h3 i {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Oscar *****/
/*---------------*/

figure.effect-oscar {
	background: -webkit-linear-gradient(45deg, #22682a 0%, #9b4a1b 40%, #3a342a 100%);
	background: linear-gradient(45deg, #22682a 0%,#9b4a1b 40%,#3a342a 100%);
}

figure.effect-oscar img {
	opacity: 0.9;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-oscar figcaption {
	padding: 3em;
	background-color: rgba(58,52,42,0.7);
	-webkit-transition: background-color 0.35s;
	transition: background-color 0.35s;
}

figure.effect-oscar figcaption::before {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	border: 1px solid #fff;
	content: '';
}

figure.effect-oscar h3 {
	margin: 20% 0 10px 0;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,100%,0);
	transform: translate3d(0,100%,0);
}

figure.effect-oscar figcaption::before,
figure.effect-oscar p {
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale(0);
	transform: scale(0);
}

figure.effect-oscar:hover h3 {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-oscar:hover figcaption::before,
figure.effect-oscar:hover p {
	opacity: 1;
	-webkit-transform: scale(1);
	transform: scale(1);
}

figure.effect-oscar:hover figcaption {
	background-color: rgba(58,52,42,0);
}

figure.effect-oscar:hover img {
	opacity: 0.4;
}

/*---------------*/
/***** Marley *****/
/*---------------*/

figure.effect-marley figcaption {
	text-align: right;
}

figure.effect-marley h3,
figure.effect-marley p {
	position: absolute;
	right: 30px;
	left: 30px;
	padding: 10px 0;
}


figure.effect-marley p {
	bottom: 30px;
	line-height: 1.5;
	-webkit-transform: translate3d(0,100%,0);
	transform: translate3d(0,100%,0);
}

figure.effect-marley h3 {
	top: 30px;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,20px,0);
	transform: translate3d(0,20px,0);
}

figure.effect-marley:hover h3 {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-marley h3::after {
	position: absolute;
	top: 100%;
	left: 0;
	width: 100%;
	height: 4px;
	background: #fff;
	content: '';
	-webkit-transform: translate3d(0,40px,0);
	transform: translate3d(0,40px,0);
}

figure.effect-marley h3::after,
figure.effect-marley p {
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-marley:hover h3::after,
figure.effect-marley:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Ruby *****/
/*---------------*/

figure.effect-ruby {
	background-color: #17819c;
}

figure.effect-ruby img {
	opacity: 0.7;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale(1.15);
	transform: scale(1.15);
}

figure.effect-ruby:hover img {
	opacity: 0.5;
	-webkit-transform: scale(1);
	transform: scale(1);
}

figure.effect-ruby h3 {
	margin-top: 20%;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,20px,0);
	transform: translate3d(0,20px,0);
}

figure.effect-ruby p {
	margin: 1em 0 0;
	padding: 3em;
	border: 1px solid #fff;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,20px,0) scale(1.1);
	transform: translate3d(0,20px,0) scale(1.1);
} 

figure.effect-ruby:hover h3 {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-ruby:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0) scale(1);
	transform: translate3d(0,0,0) scale(1);
}

/*---------------*/
/***** Milo *****/
/*---------------*/

figure.effect-milo {
	background: #2e5d5a;
}

figure.effect-milo img {
	max-width: none;
	width: -webkit-calc(100% + 60px);
	width: calc(100% + 60px);
	opacity: 1;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-30px,0,0) scale(1.12);
	transform: translate3d(-30px,0,0) scale(1.12);
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.effect-milo:hover img {
	opacity: 0.5;
	-webkit-transform: translate3d(0,0,0) scale(1);
	transform: translate3d(0,0,0) scale(1);
}

figure.effect-milo h3 {
	position: absolute;
	right: 0;
	bottom: 0;
	padding: 1em 1.2em;
}

figure.effect-milo p {
	padding: 0 10px 0 0;
	width: 50%;
	border-right: 1px solid #fff;
	text-align: right;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-40px,0,0);
	transform: translate3d(-40px,0,0);
}

figure.effect-milo:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Dexter *****/
/*---------------*/

figure.effect-dexter {
	background: -webkit-linear-gradient(top, rgba(37,141,200,1) 0%, rgba(104,60,19,1) 100%);
	background: linear-gradient(to bottom, rgba(37,141,200,1) 0%,rgba(104,60,19,1) 100%); 
}

figure.effect-dexter img {
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-dexter:hover img {
	opacity: 0.4;
}

figure.effect-dexter figcaption::after {
	position: absolute;
	right: 30px;
	bottom: 30px;
	left: 30px;
	height: -webkit-calc(50% - 30px);
	height: calc(50% - 30px);
	border: 7px solid #fff;
	content: '';
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,-100%,0);
	transform: translate3d(0,-100%,0);
}

figure.effect-dexter:hover figcaption::after {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-dexter figcaption {
	padding: 3em;
	text-align: left;
}

figure.effect-dexter p {
	position: absolute;
	right: 60px;
	bottom: 60px;
	left: 60px;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,-100px,0);
	transform: translate3d(0,-100px,0);
}

figure.effect-dexter:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Sarah *****/
/*---------------*/

figure.effect-sarah {
	background: #42b078;
}

figure.effect-sarah img {
	max-width: none;
	width: -webkit-calc(100% + 20px);
	width: calc(100% + 20px);
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-10px,0,0);
	transform: translate3d(-10px,0,0);
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.effect-sarah:hover img {
	opacity: 0.4;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-sarah figcaption {
	text-align: left;
}

figure.effect-sarah h3 {
	position: relative;
	overflow: hidden;
	padding: 0.5em 0;
}

figure.effect-sarah h3::after {
	position: absolute;
	bottom: 0;
	left: 0;
	width: 100%;
	height: 3px;
	background: #fff;
	content: '';
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(-100%,0,0);
	transform: translate3d(-100%,0,0);
}

figure.effect-sarah:hover h3::after {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-sarah p {
	padding: 1em 0;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(100%,0,0);
	transform: translate3d(100%,0,0);
}

figure.effect-sarah:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Chico *****/
/*---------------*/

figure.effect-chico img {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale(1.12);
	transform: scale(1.12);
}

figure.effect-chico:hover img {
	opacity: 0.5;
	-webkit-transform: scale(1);
	transform: scale(1);
}

figure.effect-chico figcaption {
	padding: 3em;
}

figure.effect-chico figcaption::before {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	border: 1px solid #fff;
	content: '';
	-webkit-transform: scale(1.1);
	transform: scale(1.1);
}

figure.effect-chico figcaption::before,
figure.effect-chico p {
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-chico h3 {
	padding: 20% 0 20px 0;
}

figure.effect-chico p {
	margin: 0 auto;
	max-width: 80%;
	-webkit-transform: scale(1.5);
	transform: scale(1.5);
}

figure.effect-chico:hover figcaption::before,
figure.effect-chico:hover p {
	opacity: 1;
	-webkit-transform: scale(1);
	transform: scale(1);
}


/*---------------*/
/***** Julia *****/
/*---------------*/

figure.effect-julia {
	background: #2f3238;
}

figure.effect-julia img {
	max-width: 100%;
	height: auto;
	-webkit-transition: opacity 1s, -webkit-transform 1s;
	transition: opacity 1s, transform 1s;
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.effect-julia figcaption {
	text-align: left;
}

figure.effect-julia h3 {
	position: relative;
	padding: 0.5em 0;
}

figure.effect-julia p {
	display: inline-block;
	margin: 0 0 0.25em;
	padding: 0.4em 1em;
	background: rgba(255,255,255,0.9);
	color: #2f3238;
	text-transform: none;
	font-weight: 500;
	font-size: 75%;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-360px,0,0);
	transform: translate3d(-360px,0,0);
}

figure.effect-julia p:first-child {
	-webkit-transition-delay: 0.15s;
	transition-delay: 0.15s;
}

figure.effect-julia p:nth-of-type(2) {
	-webkit-transition-delay: 0.1s;
	transition-delay: 0.1s;
}

figure.effect-julia p:nth-of-type(3) {
	-webkit-transition-delay: 0.05s;
	transition-delay: 0.05s;
}

figure.effect-julia:hover p:first-child {
	-webkit-transition-delay: 0s;
	transition-delay: 0s;
}

figure.effect-julia:hover p:nth-of-type(2) {
	-webkit-transition-delay: 0.05s;
	transition-delay: 0.05s;
}

figure.effect-julia:hover p:nth-of-type(3) {
	-webkit-transition-delay: 0.1s;
	transition-delay: 0.1s;
}

figure.effect-julia:hover img {
	opacity: 0.4;
	-webkit-transform: scale3d(1.1,1.1,1);
	transform: scale3d(1.1,1.1,1);
}

figure.effect-julia:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*-----------------*/
/***** Goliath *****/
/*-----------------*/

figure.effect-goliath {
	background: #df4e4e;
}

figure.effect-goliath img,
figure.effect-goliath h3 {
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-goliath img {
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.effect-goliath h3,
figure.effect-goliath p {
	position: absolute;
	bottom: 0;
	left: 0;
	padding: 30px;
}

figure.effect-goliath p {
	text-transform: none;
	font-size: 90%;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(0,50px,0);
	transform: translate3d(0,50px,0);
}

figure.effect-goliath:hover img {
	-webkit-transform: translate3d(0,-80px,0);
	transform: translate3d(0,-80px,0);
}

figure.effect-goliath:hover h3 {
	-webkit-transform: translate3d(0,-100px,0);
	transform: translate3d(0,-100px,0);
}

figure.effect-goliath:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*-----------------*/
/***** Selena *****/
/*-----------------*/

figure.effect-selena {
	background: #fff;
}

figure.effect-selena img {
	opacity: 0.95;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform-origin: 50% 50%;
	transform-origin: 50% 50%;
}

figure.effect-selena:hover img {
	-webkit-transform: scale3d(0.95,0.95,1);
	transform: scale3d(0.95,0.95,1);
}

figure.effect-selena h3 {
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(0,20px,0);
	transform: translate3d(0,20px,0);
}

figure.effect-selena p {
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: perspective(1000px) rotate3d(1,0,0,90deg);
	transform: perspective(1000px) rotate3d(1,0,0,90deg);
	-webkit-transform-origin: 50% 0%;
	transform-origin: 50% 0%;
}

figure.effect-selena:hover h3 {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-selena:hover p {
	opacity: 1;
	-webkit-transform: perspective(1000px) rotate3d(1,0,0,0);
	transform: perspective(1000px) rotate3d(1,0,0,0);
}

/*-----------------*/
/***** Apollo *****/
/*-----------------*/

figure.effect-apollo {
	background: #3498db;
}

figure.effect-apollo img {
	opacity: 0.95;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale3d(1.05,1.05,1);
	transform: scale3d(1.05,1.05,1);
}

figure.effect-apollo figcaption::before {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(255,255,255,0.5);
	content: '';
	-webkit-transition: -webkit-transform 0.6s;
	transition: transform 0.6s;
	-webkit-transform: scale3d(1.9,1.4,1) rotate3d(0,0,1,45deg) translate3d(0,-100%,0);
	transform: scale3d(1.9,1.4,1) rotate3d(0,0,1,45deg) translate3d(0,-100%,0);
}

figure.effect-apollo p {
	position: absolute;
	right: 0;
	bottom: 0;
	margin: 3em;
	padding: 0 1em;
	max-width: 150px;
	border-right: 4px solid #fff;
	text-align: right;
	opacity: 0;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-apollo h3 {
	text-align: left;
}

figure.effect-apollo:hover img {
	opacity: 0.6;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

figure.effect-apollo:hover figcaption::before {
	-webkit-transform: scale3d(1.9,1.4,1) rotate3d(0,0,1,45deg) translate3d(0,100%,0);
	transform: scale3d(1.9,1.4,1) rotate3d(0,0,1,45deg) translate3d(0,100%,0);
}

figure.effect-apollo:hover p {
	opacity: 1;
	-webkit-transition-delay: 0.1s;
	transition-delay: 0.1s;
}


/*-----------------*/
/***** Steve *****/
/*-----------------*/

figure.effect-steve {
	z-index: auto;
	overflow: visible;
	background: #000;
}

figure.effect-steve:before,
figure.effect-steve h3:before {
	position: absolute;
	top: 0;
	left: 0;
	z-index: -1;
	width: 100%;
	height: 100%;
	background: #000;
	content: '';
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-steve:before {
	box-shadow: 0 3px 30px rgba(0,0,0,0.8);
	opacity: 0;
}

figure.effect-steve figcaption {
	z-index: 1;
}

figure.effect-steve img {
	opacity: 1;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: perspective(1000px) translate3d(0,0,0);
	transform: perspective(1000px) translate3d(0,0,0);
}

figure.effect-steve h3,
figure.effect-steve p {
	background: #fff;
	color: #2d434e;
}

figure.effect-steve h3 {
	position: relative;
	margin-top: 2em;
	padding: 0.25em;
}

figure.effect-steve h3:before {
	box-shadow: 0 1px 10px rgba(0,0,0,0.5);
}

figure.effect-steve p {
	margin-top: 1em;
	padding: 0.5em;
	font-weight: 800;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale3d(0.9,0.9,1);
	transform: scale3d(0.9,0.9,1);
}

figure.effect-steve:hover:before {
	opacity: 1;
}

figure.effect-steve:hover img {
	-webkit-transform: perspective(1000px) translate3d(0,0,21px);
	transform: perspective(1000px) translate3d(0,0,21px);
}

figure.effect-steve:hover h3:before {
	opacity: 0;
}

figure.effect-steve:hover p {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

/*-----------------*/
/***** Moses *****/
/*-----------------*/

figure.effect-moses {
	background: -webkit-linear-gradient(-45deg, #EC65B7 0%,#05E0D8 100%);
	background: linear-gradient(-45deg, #EC65B7 0%,#05E0D8 100%);
}

figure.effect-moses img {
	opacity: 0.85;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-moses h3,
figure.effect-moses p {
	padding: 20px;
	width: 50%;
	height: 50%;
	border: 2px solid #fff;
}

figure.effect-moses h3 {
	padding: 20px;
	width: 50%;
	height: 50%;
	text-align: left;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(10px,10px,0);
	transform: translate3d(10px,10px,0);
}

figure.effect-moses p {
	float: right;
	padding: 20px;
	text-align: right;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(-50%,-50%,0);
	transform: translate3d(-50%,-50%,0);
}

figure.effect-moses:hover h3 {
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-moses:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-moses:hover img {
	opacity: 0.6;
}

/*---------------*/
/***** Jazz *****/
/*---------------*/

figure.effect-jazz {
	background: -webkit-linear-gradient(-45deg, #f3cf3f 0%,#f33f58 100%);
	background: linear-gradient(-45deg, #f3cf3f 0%,#f33f58 100%);
}

figure.effect-jazz img {
	opacity: 0.9;
}

figure.effect-jazz figcaption::after,
figure.effect-jazz img,
figure.effect-jazz p {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-jazz figcaption::after {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
	content: '';
	opacity: 0;
	-webkit-transform: rotate3d(0,0,1,45deg) scale3d(1,0,1);
	transform: rotate3d(0,0,1,45deg) scale3d(1,0,1);
	-webkit-transform-origin: 50% 50%;
	transform-origin: 50% 50%;
}

figure.effect-jazz h3,
figure.effect-jazz p {
	opacity: 1;
	-webkit-transform: scale3d(0.8,0.8,1);
	transform: scale3d(0.8,0.8,1);
}

figure.effect-jazz h3 {
	padding-top: 26%;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-jazz p {
	padding: 0.5em 2em;
	text-transform: none;
	font-size: 0.85em;
	opacity: 0;
}

figure.effect-jazz:hover img {
	opacity: 0.7;
	-webkit-transform: scale3d(1.05,1.05,1);
	transform: scale3d(1.05,1.05,1);
}

figure.effect-jazz:hover figcaption::after {
	opacity: 1;
	-webkit-transform: rotate3d(0,0,1,45deg) scale3d(1,1,1);
	transform: rotate3d(0,0,1,45deg) scale3d(1,1,1);
}

figure.effect-jazz:hover h3,
figure.effect-jazz:hover p {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

/*---------------*/
/***** Ming *****/
/*---------------*/

figure.effect-ming {
	background: #030c17;
}

figure.effect-ming img {
	opacity: 0.9;
	-webkit-transition: opacity 0.35s;
	transition: opacity 0.35s;
}

figure.effect-ming figcaption::before {
	position: absolute;
	top: 30px;
	right: 30px;
	bottom: 30px;
	left: 30px;
	border: 2px solid #fff;
	box-shadow: 0 0 0 30px rgba(255,255,255,0.2);
	content: '';
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale3d(1.4,1.4,1);
	transform: scale3d(1.4,1.4,1);
}

figure.effect-ming h3 {
	margin: 20% 0 10px 0;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
}

figure.effect-ming p {
	padding: 1em;
	opacity: 0;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: scale(1.5);
	transform: scale(1.5);
}

figure.effect-ming:hover h3 {
	-webkit-transform: scale(0.9);
	transform: scale(0.9);
}

figure.effect-ming:hover figcaption::before,
figure.effect-ming:hover p {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

figure.effect-ming:hover figcaption {
	background-color: rgba(58,52,42,0);
}

figure.effect-ming:hover img {
	opacity: 0.4;
}

/*---------------*/
/***** Lexi *****/
/*---------------*/

figure.effect-lexi {
	background: -webkit-linear-gradient(-45deg, #000 0%,#fff 100%);
	background: linear-gradient(-45deg, #000 0%,#fff 100%);
}

figure.effect-lexi img {
	margin: -10px 0 0 -10px;
	max-width: none;
	width: -webkit-calc(100% + 10px);
	width: calc(100% + 10px);
	opacity: 0.9;
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
	-webkit-transform: translate3d(10px,10px,0);
	transform: translate3d(10px,10px,0);
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
}

figure.effect-lexi figcaption::before,
figure.effect-lexi p {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-lexi figcaption::before {
	position: absolute;
	right: -100px;
	bottom: -100px;
	width: 300px;
	height: 300px;
	border: 2px solid #fff;
	border-radius: 50%;
	box-shadow: 0 0 0 900px rgba(255,255,255,0.2);
	content: '';
	opacity: 0;
	-webkit-transform: scale3d(0.5,0.5,1);
	transform: scale3d(0.5,0.5,1);
	-webkit-transform-origin: 50% 50%;
	transform-origin: 50% 50%;
}

figure.effect-lexi:hover img {
	opacity: 0.6;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

figure.effect-lexi h3 {
	text-align: left;
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: translate3d(5px,5px,0);
	transform: translate3d(5px,5px,0);
}

figure.effect-lexi p {
	position: absolute;
	right: 0;
	bottom: 0;
	padding: 0 1.5em 1.5em 0;
	width: 140px;
	text-align: right;
	opacity: 0;
	-webkit-transform: translate3d(20px,20px,0);
	transform: translate3d(20px,20px,0);
}

figure.effect-lexi:hover figcaption::before {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

figure.effect-lexi:hover h3,
figure.effect-lexi:hover p {
	opacity: 1;
	-webkit-transform: translate3d(0,0,0);
	transform: translate3d(0,0,0);
}

/*---------------*/
/***** Duke *****/
/*---------------*/

figure.effect-duke {
	background: -webkit-linear-gradient(-45deg, #34495e 0%,#cc6055 100%);
	background: linear-gradient(-45deg, #34495e 0%,#cc6055 100%);
}

figure.effect-duke img,
figure.effect-duke p {
	-webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
	transition: opacity 0.35s, transform 0.35s;
}

figure.effect-duke:hover img {
	opacity: 0.1;
	-webkit-transform: scale3d(2,2,1);
	transform: scale3d(2,2,1);
}

figure.effect-duke h3 {
	-webkit-transition: -webkit-transform 0.35s;
	transition: transform 0.35s;
	-webkit-transform: scale3d(0.8,0.8,1);
	transform: scale3d(0.8,0.8,1);
	-webkit-transform-origin: 50% 100%;
	transform-origin: 50% 100%;
}

figure.effect-duke p {
	position: absolute;
	bottom: 0;
	left: 0;
	margin: 20px;
	padding: 30px;
	border: 2px solid #fff;
	text-transform: none;
	font-size: 90%;
	opacity: 0;
	-webkit-transform: scale3d(0.8,0.8,1);
	transform: scale3d(0.8,0.8,1);
	-webkit-transform-origin: 50% -100%;
	transform-origin: 50% -100%;
}

figure.effect-duke:hover h3,
figure.effect-duke:hover p {
	opacity: 1;
	-webkit-transform: scale3d(1,1,1);
	transform: scale3d(1,1,1);
}

@media screen and (max-width: 50em) {
	.content {
		padding: 0 10px;
		text-align: center;
	}
	figure.image-module {
		display: inline-block;
		float: none;
		margin: 10px auto;
		width: 100%;
	}
}

/* End of Image hover styles */

/* Theme Check */ 
.wp-caption {}
.wp-caption-text {}
.sticky {}
.gallery-caption {}
.bypostauthor {}
.alignleft { float:left; margin:0; display: block; position: relative; }
.alignright { float:right;margin:0; display: block; position: relative; }
.aligncenter { clear:both; display:block; margin:0 auto; position: relative; }
img.alignright {float:right; margin:0 0 1em 1em}
img.alignleft {float:left; margin:0 1em 1em 0}
img.aligncenter {display: block; margin-left: auto; margin-right: auto}
a img.alignright {float:right; margin:0 0 1em 1em}
a img.alignleft {float:left; margin:0 1em 1em 0}
a img.aligncenter {display: block; margin-left: auto; margin-right: auto}

.size-auto, 
.size-full,
.size-large,
.size-medium,
.size-thumbnail {
	max-width: 100%;
	height: auto;
}

/* General */
html {
	max-width: 100%;
}
.container-wide:not(.navbar-fixed-top):not(.navbar-fixed-bottom):not(.navbar-sticky) {
	position: relative;
}
.container-wide {
	margin: 0 auto;
}

.dspl-inline {
	display: inline;
}
/* Default List Styles */
ul.list-square { 
	list-style-type: square;
}
ul.list-circle { 
	list-style-type: circle;
}
ul.list-disc { 
	list-style-type: disc;
}
/* TBS fix*/
.fa-li {
	top: auto !important;
	line-height: inherit !important;
}
.embed-responsive .embed-responsive-item, .embed-responsive iframe, .embed-responsive embed, .embed-responsive object {
    height: 100% !important;
    position: absolute !important;
		margin: 0 !important;
}
.sdf-parallax {
	background-attachment: fixed;
	background-position: 50% 0;
	background-repeat: no-repeat;
	position: relative;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}
/* iPads 1-5 and iPad mini in portrait & landscape */
@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  {
	.sdf-parallax {
		background-attachment: scroll;
	}
}
/* iPhone 5 In Portrait & Landscape */
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 568px){ 
	.sdf-parallax {
		background-attachment: scroll;
	}
}
/* iPhone 2G, 3G, 4, 4S In Portrait & Landscape */
@media only screen 
and (min-device-width : 320px) 
and (max-device-width : 480px){ 
	.sdf-parallax {
		background-attachment: scroll;
	}
}
/* iPhone 6 and iPhone 6+ portrait and landscape */
@media only screen and (max-device-width: 640px), only screen and (max-device-width: 667px), only screen and (max-width: 480px){ 
	.sdf-parallax {
		background-attachment: scroll;
	}
}
<?php
$id_var = 'theme_font';
?>

ul,
ol {
<?php if($sdf_theme_styles[$id_var.'_list_margin'] != '') : ?>
	margin: <?php echo $sdf_theme_styles[$id_var.'_list_margin']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_list_padding'] != '') : ?>
	padding: <?php echo $sdf_theme_styles[$id_var.'_list_padding']; ?>;
<?php endif; ?>
}
ul {
<?php if(in_array($sdf_theme_styles[$id_var.'_list_style'], array('square', 'circle', 'disc'))): ?>
	list-style-type: <?php echo $sdf_theme_styles[$id_var.'_list_style']; ?>;
<?php elseif(in_array($sdf_theme_styles[$id_var.'_list_style'], array('none'))): ?>
  list-style: none;
<?php endif; ?>
}
<?php if($sdf_theme_styles[$id_var.'_list_item_line_height']): ?>
ul:not(.option-set):not(.post-meta) > li:not(.media):not(.follow-icon):not(.follow-btn):not(.sharing-icon):not(.sharing-btn) {
	line-height: <?php echo $sdf_theme_styles[$id_var.'_list_item_line_height']; ?>;
}
<?php endif; ?>

<?php if($sdf_theme_styles[$id_var.'_custom_list_styling'] == 'yes'): ?>
/* Custom List Style */
ul.list-custom {
<?php if($sdf_theme_styles[$id_var.'_custom_list_margin'] != '') : ?>
	margin: <?php echo $sdf_theme_styles[$id_var.'_custom_list_margin']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_custom_list_padding'] != '') : ?>
	padding: <?php echo $sdf_theme_styles[$id_var.'_custom_list_padding']; ?>;
<?php endif; ?>
<?php if(in_array($sdf_theme_styles[$id_var.'_custom_list_style'], array('square', 'circle', 'disc'))): ?>
	list-style-type: <?php echo $sdf_theme_styles[$id_var.'_custom_list_style']; ?>;
<?php elseif(in_array($sdf_theme_styles[$id_var.'_custom_list_style'], array('none', 'icon'))): ?>
  list-style: none;
<?php endif; ?>
}
ul.list-custom li { 
	<?php if($sdf_theme_styles[$id_var.'_custom_list_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_custom_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_custom_list_font_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_custom_list_family'], ''); ?>
	<?php if($sdf_theme_styles[$id_var.'_custom_list_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_custom_list_font_color']; ?>;<?php } ?>
	<?php $wg_list_item_margin = ($sdf_theme_styles[$id_var.'_custom_list_item_margin'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_margin'] : '0'; ?>
	margin: <?php echo $wg_list_item_margin; ?>;
	<?php $wg_list_item_padding = ($sdf_theme_styles[$id_var.'_custom_list_item_padding'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_padding'] : '0'; ?>
	padding: <?php echo $wg_list_item_padding; ?>;
	<?php $wg_list_align = ($sdf_theme_styles[$id_var.'_custom_list_align'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_align'] : 'left'; ?>
	text-align: <?php echo $wg_list_align; ?>;
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_line_height']): ?>
	line-height: <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_line_height']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_custom_list_item_border_style']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_custom_list_item_border_color']; ?>;<?php endif; ?>
	border-width:<?php echo ($sdf_theme_styles[$id_var.'_custom_list_item_border_width'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_border_width'] : '0px'; ?>;
	-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] : '0px'; ?>; 
	-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] : '0px'; ?>; 
	border-radius: <?php echo ($sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_item_border_radius'] : '0px'; ?>; 
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_background_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_custom_list_item_background_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_box_shadow_color']; ?>;
}
ul.list-custom li:hover {
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_border_hover_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_custom_list_item_border_hover_color']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_custom_list_item_background_hover_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_custom_list_item_background_hover_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_hover_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_custom_list_item_hover_box_shadow_color']; ?>;
}
ul.list-custom li:before {
	<?php $widget_list_icon = ($sdf_theme_styles[$id_var.'_custom_list_style'] == 'icon') ? '\\'.$sdf_theme_styles[$id_var.'_custom_list_icon'] : ''; ?>
	content: "<?php echo $widget_list_icon; ?>";
	font-family: FontAwesome;
	<?php if($sdf_theme_styles[$id_var.'_custom_list_icon_size']): ?>
	font-size:<?php echo $sdf_theme_styles[$id_var.'_custom_list_icon_size']; ?><?php echo $sdf_theme_styles[$id_var.'_custom_list_icon_size_type']; ?>;
	<?php endif; ?>
	<?php $wg_list_icon_margin = ($sdf_theme_styles[$id_var.'_custom_list_icon_margin'] != '') ? $sdf_theme_styles[$id_var.'_custom_list_icon_margin'] : '0'; ?>
	margin: <?php echo $wg_list_icon_margin; ?>;
	position: absolute;
}
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_color'] || $sdf_theme_styles[$id_var.'_custom_list_family'] || $sdf_theme_styles[$id_var.'_custom_list_link_text_decoration']): ?>
ul.list-custom li a:not(.btn) {
<?php if($sdf_theme_styles[$id_var.'_custom_list_font_size']): ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_custom_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_custom_list_font_size_type']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_custom_list_link_text_decoration']; ?>;<?php endif; ?>
<?php get_font_family_weight($sdf_theme_styles[$id_var.'_custom_list_family'], ''); ?>
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_color']) : ?>color:<?php echo $sdf_theme_styles[$id_var.'_custom_list_link_color']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_hover_color'] || $sdf_theme_styles[$id_var.'_custom_list_link_hover_text_decoration']): ?>
ul.list-custom li a:not(.btn):hover, li a:not(.btn):focus {
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_hover_color']) : ?>color:<?php echo $sdf_theme_styles[$id_var.'_custom_list_link_hover_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_custom_list_link_hover_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_custom_list_link_hover_text_decoration']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php endif; ?>

.fa-ul.line-lg{margin-left:3em}
.fa-ul.line-2x{margin-left:3.4em}
.fa-ul.line-3x{margin-left:4.6em}
.fa-ul.line-4x{margin-left:5.8em}
.fa-ul.line-5x{margin-left:7em}
.line-lg li{line-height:2em;margin:0}
.line-2x li{line-height:2.4em;margin:0}
.line-3x li{line-height:3.6em;margin:0}
.line-4x li{line-height:4.8em;margin:0}
.line-5x li{line-height:6em;margin:0}

.address-widget.fa-ul {
	margin-left: 2.14286em !important;
	padding-left: 0 !important; 
}		
p.address-info {
    margin-top: 10px;
}
.media-image-top{
	margin-bottom: 15px;
}
.media-image-bottom{
	margin-top: 15px;
}
.slider_block_area .widget_revslider { 
	margin: 0;
	padding: 0; 
}
.pull-left.first { clear: left; }
.pull-right.first { clear: right; }

a { outline: 0 none; cursor: pointer;}
iframe {
	border: none;
}
/* Separators */
.separator {
	margin: 0;
	height: 0;
	position: relative;
}
/* Gradient transparent - color - transparent */ 
.separator.style-1,
.separator-text.style-1:before,
.separator-text.style-1:after { 
	border: 0; 
	height: 1px; 
	background: -webkit-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_font_color']; ?>, rgba(0,0,0,0));
	background: -moz-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_font_color']; ?>, rgba(0,0,0,0));
	background: -o-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_font_color']; ?>, rgba(0,0,0,0)); 
	background: linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_font_color']; ?>, rgba(0,0,0,0)); 
}
.separator.style-1.theme-primary,
.separator-text.style-1.theme-primary:before,
.separator-text.style-1.theme-primary:after {  
	border: 0; 
	height: 1px; 
	background: -webkit-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_primary_color']; ?>, rgba(0,0,0,0)); 
	background: -moz-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_primary_color']; ?>, rgba(0,0,0,0)); 
	background: -o-linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_primary_color']; ?>, rgba(0,0,0,0)); 
	background: linear-gradient(left, rgba(0,0,0,0), <?php echo $sdf_theme_styles['theme_primary_color']; ?>, rgba(0,0,0,0)); 
}
.separator.style-2,
.separator-text.style-2:before,
.separator-text.style-2:after { 
border-bottom: 1px solid <?php echo $sdf_theme_styles['theme_font_color']; ?>;
}
.separator.style-2.theme-primary,
.separator-text.style-2.theme-primary:before,
.separator-text.style-2.theme-primary:after { 
border-bottom: 1px solid <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
}
.separator.style-3,
.separator-text.style-3:before,
.separator-text.style-3:after { 
border-top: 1px solid <?php echo $sdf_theme_styles['theme_font_color']; ?>;
border-bottom: 1px solid rgba(0,0,0,0.15);
}
.separator.style-3.theme-primary,
.separator-text.style-3.theme-primary:before,
.separator-text.style-3.theme-primary:after { 
border-top: 1px solid <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
border-bottom: 1px solid rgba(0,0,0,0.15);
}
.separator.style-4,
.separator-text.style-4:before,
.separator-text.style-4:after { 
border-bottom: 1px dashed <?php echo $sdf_theme_styles['theme_font_color']; ?>;
}
.separator.style-4.theme-primary,
.separator-text.style-4.theme-primary:before,
.separator-text.style-4.theme-primary:after { 
border-bottom: 1px dashed <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
}
.separator.style-5,
.separator-text.style-5:before,
.separator-text.style-5:after {
border-bottom: 1px dashed <?php echo $sdf_theme_styles['theme_font_color']; ?>;
background: rgba(0,0,0,0.15);
}
.separator.style-5.theme-primary,
.separator-text.style-5.theme-primary:before,
.separator-text.style-5.theme-primary:after { 
border-bottom: 1px dashed <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
background: rgba(0,0,0,0.15);
}
.separator.style-6,
.separator-text.style-6:before,
.separator-text.style-6:after { 
border-bottom: 1px dotted <?php echo $sdf_theme_styles['theme_font_color']; ?>;
}
.separator.style-6.theme-primary,
.separator-text.style-6.theme-primary:before,
.separator-text.style-6.theme-primary:after { 
border-bottom: 1px dotted <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
}
.separator.style-7,
.separator-text.style-7:before,
.separator-text.style-7:after { 
border-bottom: 1px dotted <?php echo $sdf_theme_styles['theme_font_color']; ?>;
background: rgba(0,0,0,0.15);
}
.separator.style-7.theme-primary,
.separator-text.style-7.theme-primary:before,
.separator-text.style-7.theme-primary:after { 
border-bottom: 1px dotted <?php echo $sdf_theme_styles['theme_primary_color']; ?>;
background: rgba(0,0,0,0.15);
}
/* Double Line */ 
.separator.style-8,
.separator-text.style-8:before,
.separator-text.style-8:after { 
	padding: 0; 
	border: none; 
	border-top: medium double <?php echo $sdf_theme_styles['theme_font_color']; ?>; 
	color: <?php echo $sdf_theme_styles['theme_font_color']; ?>; 
} 
.separator.style-8.theme-primary,
.separator-text.style-8.theme-primary:before,
.separator-text.style-8.theme-primary:after { 
	padding: 0; 
	border: none; 
	border-top: medium double <?php echo $sdf_theme_styles['theme_primary_color']; ?>; 
	color: <?php echo $sdf_theme_styles['theme_primary_color']; ?>; 
} 
/* Drop Shadow */ 
.separator.style-9,
.separator-text.style-9:before,
.separator-text.style-9:after {
	height: 12px; 
	border: 0; 
	box-shadow: inset 0 12px 12px -12px <?php echo $sdf_theme_styles['theme_font_color']; ?>; 
}
.separator.style-9.theme-primary,
.separator-text.style-9.theme-primary:before,
.separator-text.style-9.theme-primary:after {
	height: 12px; 
	border: 0; 
	box-shadow: inset 0 12px 12px -12px <?php echo $sdf_theme_styles['theme_primary_color']; ?>; 
}
/* Cloud */ 
.separator.style-10 { 
	border: 0; 
	box-shadow: 0 0 10px 1px <?php echo $sdf_theme_styles['theme_font_color']; ?>; 
} 
.separator.style-10.theme-primary { 
	border: 0; 
	box-shadow: 0 0 10px 1px <?php echo $sdf_theme_styles['theme_primary_color']; ?>; 
} 
.separator.style-10:after { 
	content: "\00a0"; /* prevent margin collapse */ 
}
/* Radial Shadow */ 
.separator.style-11{
	height:1px;
	overflow:visible;
	border:none;
	background:none;
	background:-webkit-gradient(linear, left top, right top, color-stop(0%, rgba(150, 150, 150, 0)), color-stop(15%, rgba(150, 150, 150, 0)), color-stop(50%, rgba(150, 150, 150, 0.65)), color-stop(85%, rgba(150, 150, 150, 0)), color-stop(100%, rgba(150, 150, 150, 0)));
	background: -webkit-linear-gradient(left, rgba(150, 150, 150, 0) 0%, rgba(150, 150, 150, 0) 15%, rgba(150, 150, 150, 0.65) 50%, rgba(150, 150, 150, 0) 85%, rgba(150, 150, 150, 0) 100%);
	background: -moz-linear-gradient(left, rgba(150, 150, 150, 0) 0%, rgba(150, 150, 150, 0) 15%, rgba(150, 150, 150, 0.65) 50%, rgba(150, 150, 150, 0) 85%, rgba(150, 150, 150, 0) 100%);
	background: -o-linear-gradient(left, rgba(150, 150, 150, 0) 0%, rgba(150, 150, 150, 0) 15%, rgba(150, 150, 150, 0.65) 50%, rgba(150, 150, 150, 0) 85%, rgba(150, 150, 150, 0) 100%);
	background: linear-gradient(left, rgba(150, 150, 150, 0) 0%, rgba(150, 150, 150, 0) 15%, rgba(150, 150, 150, 0.65) 50%, rgba(150, 150, 150, 0) 85%, rgba(150, 150, 150, 0) 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000',endColorstr='#00000000',GradientType=1)
}
.separator.style-11:after{
	display:block;
	margin-top:10px;
	height:6px;
	width:100%;
	content:'';
	background:-webkit-radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_font_color']; ?> 0px, rgba(255, 255, 255, 0) 65%);
	background:-o-radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_font_color']; ?> 0px, rgba(255, 255, 255, 0) 80%);
	background:radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_font_color']; ?> 0px, rgba(255, 255, 255, 0) 65%)
}
body:not(:-moz-handler-blocked) .separator.style-11:after{
	background:radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_font_color']; ?> 0px, rgba(255, 255, 255, 0) 80%)
}
.separator.style-11.theme-primary:after{
	display:block;
	margin-top:10px;
	height:6px;
	width:100%;
	content:'';
	background:-webkit-radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_primary_color']; ?> 0px, rgba(255, 255, 255, 0) 65%);
	background:-o-radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_primary_color']; ?> 0px, rgba(255, 255, 255, 0) 80%);
	background:radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_primary_color']; ?> 0px, rgba(255, 255, 255, 0) 65%)
}
body:not(:-moz-handler-blocked) .separator.style-12.theme-primary:after{
	background:radial-gradient(ellipse at 50% -50%, <?php echo $sdf_theme_styles['theme_primary_color']; ?> 0px, rgba(255, 255, 255, 0) 80%)
}
/* Separator w/ Text */

.separator-t {
  display: table;
}
.separator-text {
  display: table-row;
  white-space: nowrap;
}

.separator-text:before,
.separator-text:after { 
  content: '';
  display: table-cell;
  width: 50%;
}
.separator-text span,
.separator-text span {
  padding: 0 15px;
}
.separator-text.style-2 span,
.separator-text.style-4 span,
.separator-text.style-6 span {
	position: relative;
	top: 0.5em;
}
.separator-text.style-8 span {
	position: relative;
	top: -0.5em;
}
/* Blog */
.comments-area {
	margin-bottom:30px
}
#blog-nav {
	margin-top:30px;
	margin-bottom:30px
}
#respond code { white-space:normal; }
.form-submit {
	display: none;
}
.comment-form-author,
.comment-form-email,
.comment-form-url {
	width: 50%;
}

#captcha_answer { min-width: 120px;}
@media (max-width:991px){
#sdf-header	[class="col-md-12"]:not(:last-child){
	margin-bottom:30px
}
#navigation-area [class*="col-"]:not(:last-child),
#navigation-area-2 [class*="col-"]:not(:last-child){
	margin-bottom:0px
}
#sdf_contact_form [class*="col-"]:not(:last-child){
	margin-bottom:0px
}
}
#sdf-header > .container:last-child {
	margin-bottom: 0;
}
#sdf-header .navbar-sticky-overlay#navigation-area,
#sdf-header .navbar-sticky-overlay#navigation-area-2 {
	position: fixed;
	z-index: 999;
}
#sdf-header .container-wide.navbar-sticky-overlay#navigation-area,
#sdf-header .container-wide.navbar-sticky-overlay#navigation-area-2 {
	width: 100%;
}
#sdf-header > #navigation-area,
#sdf-header > #navigation-area-2,
#sdf-header .navbar-fixed-top#navigation-area .navbar,
#sdf-header .navbar-fixed-top#navigation-area-2 .navbar,
#sdf-header #navigation-area .navbar,
#sdf-header #navigation-area-2 .navbar,
#sdf-header > #h-slider-area {
	margin-top: 0;
	margin-bottom: 0;
}
.navbar-collapse > ul:not(.navbar-nav) {
  list-style: none outside none;
	margin-bottom: 0;
	padding-left: 0;
}
.navbar-collapse > ul:not(.navbar-nav) > li {
	float: left;
}
/* Sticky Menu */
.navbar-sticky {
-webkit-transform: translateY(0%);
	-o-transform: translateY(0%);
	transform: translateY(0%);
	-webkit-transition: transform 1.5s;
	-o-transition: transform 1.5s;
	transition: transform 1.5s;
	z-index: 999;
	position:relative;
}	

.navbar-sticky.sticky-show {
	position: fixed;
	top: 0;
	-webkit-transform: translateY(0%);
	-o-transform: translateY(0%);
	transform: translateY(0%);
	-webkit-transition: transform 1.5s;
	-o-transition: transform 1.5s;
	transition: transform 1.5s;
}
.container-wide.navbar-sticky.sticky-show {
	width: 100%;
}
 /* Breadcrumb */
.breadcrumb {
	background-color: transparent;
	border-radius: 0;
	list-style: none outside none;
	padding: 0;
}

/* Main Content override inline styles */
@media (max-width:767px){
body[style] {
  position: relative !important;
}
.hero_block[style] {
  margin: 0 !important;
  height: auto !important;
}
}
@media (max-width:480px){
.hero_block[style] {
  padding-top: 30px !important;
  padding-bottom: 30px !important;
  margin: 0 !important;
  height: auto !important;
}
}
.entry-content > .container,
.entry-content > .row:last-child,
.entry-content > .hero_block.row,
.xentry-content .container .row:last-child{
	margin-bottom: 0px;
}
.entry-content >.container>.row.no-bottom-margin,
.entry-content >.row.no-bottom-margin,
.entry-content >.hero_block.row.no-bottom-margin{
	margin-bottom: 0px;
}


/* image widget alignment */
.img-left {
	display: inline;
	float: left;
}
.img-right {
	display: inline;
	float: right;
}

.img-responsive.img-center {
  margin: 0 auto;
}

/* button alignment */
.btn-right {
	float: right;
}
.btn-left {
	float: left;
}
.btn-right + .btn-right {
	margin-right: 4px;    
}  
.btn-center  {
	text-align: center;
	margin-bottom: 5px;
}
.btn-center + .btn-center,
.btn-block + .btn-center {
	margin-top: 5px;    
}
.btn.btn-right + .btn.btn-left,
.btn.btn-left + .btn.btn-right {
	margin-top: 0;    
}
.row > [class*="col-"] > img:last-child {
	margin-bottom: 0;
}
.area_placeholder { margin:0; padding: 0; width: 100%; height: auto; display:block }
.area_placeholder h3 { padding: 0 }

.overflowed { overflow: auto; }

/* Blockquote */
<?php 
$id = $id_var = 'blockquote';
?>
<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>

	<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
	<?php echo $id; ?> {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	<?php echo $id; ?>,
	<?php echo $id; ?>.blockquote-reverse{
		<?php if($sdf_theme_styles[$id_var.'_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_border_style']; ?>;<?php endif; ?>
		<?php if($sdf_theme_styles[$id_var.'_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_border_color']; ?>;<?php endif; ?>
		
		border-width:<?php echo ($sdf_theme_styles[$id_var.'_border_width'] != '') ? $sdf_theme_styles[$id_var.'_border_width'] : '0px'; ?>;
		-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_border_radius'] : '0px'; ?>; 
		-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_border_radius'] : '0px'; ?>; 
		border-radius: <?php echo ($sdf_theme_styles[$id_var.'_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_border_radius'] : '0px'; ?>; 
	}
	<?php echo $id; ?>.blockquote-reverse{
		<?php 
		
		$border_width_arr = explode(' ', $sdf_theme_styles[$id_var.'_border_width']);
		// 5px 10px 15px 20px => 10px 5px 20px 15px
		$sdf_theme_styles[$id_var.'_border_width'] = (count($border_width_arr) == 4) ? $border_width_arr[0] . ' ' . $border_width_arr[3] . ' ' . $border_width_arr[2] . ' ' . $border_width_arr[1]  : $sdf_theme_styles[$id_var.'_border_width'];
		
		?>
		
		border-width:<?php echo ($sdf_theme_styles[$id_var.'_border_width'] != '') ? $sdf_theme_styles[$id_var.'_border_width'] : '0px'; ?>;
	}
	
	<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
	
	<?php echo $id; ?>,
	<?php echo $id; ?>.blockquote-reverse{
	background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
	background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
	background-attachment: fixed;
	background-position: 50% 0;
	background-repeat: no-repeat;
	position: relative;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	}
	
	<?php else:  ?>
	
	<?php if($sdf_theme_styles[$id_var.'_bg_color'] || $sdf_theme_styles[$id_var.'_bg_image']): ?>
		<?php echo $id; ?>,
		<?php echo $id; ?>.blockquote-reverse{
			<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
		}
	<?php endif; ?>
	
	<?php endif; ?>

<?php endif; ?>

<?php $blockquote_padding = ($sdf_theme_styles[$id_var.'_padding'] != '') ? $sdf_theme_styles[$id_var.'_padding'] : 0; ?>
<?php $blockquote_margin = ($sdf_theme_styles[$id_var.'_margin'] != '') ? $sdf_theme_styles[$id_var.'_margin'] : 0; ?>
<?php echo $id; ?>,
<?php echo $id; ?>.blockquote-reverse{
padding: <?php echo $blockquote_padding; ?>;
margin: <?php echo $blockquote_margin; ?>;
}
<?php echo $id; ?> .quote-icon{
margin-right: 15px;
}

<?php echo $id; ?>.blockquote-reverse .quote-icon{
margin-left: 15px;
}

<?php echo $id; ?> p,
<?php echo $id; ?> footer {
	<?php if($sdf_theme_styles[$id_var.'_font_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles[$id_var.'_font_text_transform']; ?>;<?php endif; ?>
}

<?php echo $id; ?> p{
	<?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_color']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_font_family'], $sdf_theme_styles[$id_var.'_font_weight']); ?>
}

<?php if($sdf_theme_styles[$id_var.'_font_hover_color']): ?>
<?php echo $id; ?> p:hover {
  <?php if($sdf_theme_styles[$id_var.'_font_hover_color']): ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_hover_color']; ?>;<?php endif; ?>
}
<?php endif; ?>

<?php echo $id; ?> p a{
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_link_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles[$id_var.'_link_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_link_text_decoration']; ?>;<?php endif; ?>
}

<?php echo $id; ?> p a:hover, <?php echo $id; ?> p a:focus{
  <?php if($sdf_theme_styles[$id_var.'_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_hover_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles[$id_var.'_link_hover_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_link_hover_text_decoration']; ?>;<?php endif; ?>
}

<?php if($sdf_theme_styles[$id_var.'_font_author_size'] || $sdf_theme_styles[$id_var.'_font_author_color'] || $sdf_theme_styles[$id_var.'_font_author_weight']): ?>
<?php echo $id; ?> footer{
	<?php if($sdf_theme_styles[$id_var.'_font_author_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles[$id_var.'_font_author_text_transform']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_font_author_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_author_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_author_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_author_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_author_color']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_font_author_family'], $sdf_theme_styles[$id_var.'_font_author_weight']); ?>
}
<?php endif; ?>

/* Social Icons */
ul.social-menu {
	margin: 0;
	padding: 0;
	list-style: none outside none;
	border: 0;
	box-shadow: none;
}
.social-menu li {
	display: inline-block;
	margin: 0;
	padding: 0;
	border: 0;
	box-shadow: none;
}
.social-menu li:last-child {
    margin-right: 0;
}
.social-menu li a {
	display: inline-block;
	overflow: hidden;
	margin: 0;
}
.social-menu li a:hover {
    text-decoration: none;
}
/* Fix styles for the modal */
.modal-body .container {
    width: auto !important;
}
[id^="modal-for-page"] .modal-header {
	border-bottom: 0;
	height: 30px;
}
/* Styles for the modal effects */

/* 
Styles for the html/body for special modal where we want 3d effects
Note that we need a container wrapping all content on the page for the 
perspective effects (not including the modals and the overlay).
*/

.md-modal {
	position: fixed;
	top: 50%;
	left: 50%;
	width: 50%;
	max-width: 630px;
	min-width: 320px;
	height: auto;
	z-index: 2000;
	visibility: hidden;
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	backface-visibility: hidden;
	-webkit-transform: translateX(-50%) translateY(-50%);
	-moz-transform: translateX(-50%) translateY(-50%);
	-ms-transform: translateX(-50%) translateY(-50%);
	transform: translateX(-50%) translateY(-50%);
}

.in {
	visibility: visible;
}

.in ~ .modal {
	opacity: 1;
	visibility: visible;
}


/* Individual modal styles with animations/transitions */

/* Effect 1: Fade in and scale up */
.fade.md-effect-1 .modal-dialog {
	-webkit-transform: scale(0.7);
	-moz-transform: scale(0.7);
	-ms-transform: scale(0.7);
	transform: scale(0.7);
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-1 .modal-dialog {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);
	opacity: 1;
}

/* Effect 2: Slide from the right */
.fade.md-effect-2 .modal-dialog {
	-webkit-transform: translateX(20%);
	-moz-transform: translateX(20%);
	-ms-transform: translateX(20%);
	transform: translateX(20%);
	opacity: 0;
	-webkit-transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
	-moz-transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
	transition: all 0.3s cubic-bezier(0.25, 0.5, 0.5, 0.9);
}

.in.md-effect-2 .modal-dialog {
	-webkit-transform: translateX(0);
	-moz-transform: translateX(0);
	-ms-transform: translateX(0);
	transform: translateX(0);
	opacity: 1;
}

/* Effect 3: Slide from the bottom */
.fade.md-effect-3 .modal-dialog {
	-webkit-transform: translateY(20%);
	-moz-transform: translateY(20%);
	-ms-transform: translateY(20%);
	transform: translateY(20%);
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-3 .modal-dialog {
	-webkit-transform: translateY(0);
	-moz-transform: translateY(0);
	-ms-transform: translateY(0);
	transform: translateY(0);
	opacity: 1;
}

/* Effect 4: Newspaper */
.fade.md-effect-4 .modal-dialog {
	-webkit-transform: scale(0) rotate(720deg);
	-moz-transform: scale(0) rotate(720deg);
	-ms-transform: scale(0) rotate(720deg);
	transform: scale(0) rotate(720deg);
	opacity: 0;
}

.in.md-effect-4 ~ .modal,
.md-effect-4 .modal-dialog {
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
	transition: all 0.5s;
}

.in.md-effect-4 .modal-dialog {
	-webkit-transform: scale(1) rotate(0deg);
	-moz-transform: scale(1) rotate(0deg);
	-ms-transform: scale(1) rotate(0deg);
	transform: scale(1) rotate(0deg);
	opacity: 1;
}

/* Effect 5: fall */
.md-effect-5.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-5 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: translateZ(600px) rotateX(20deg); 
	-moz-transform: translateZ(600px) rotateX(20deg); 
	-ms-transform: translateZ(600px) rotateX(20deg); 
	transform: translateZ(600px) rotateX(20deg); 
	opacity: 0;
}

.in.md-effect-5 .modal-dialog {
	-webkit-transition: all 0.3s ease-in;
	-moz-transition: all 0.3s ease-in;
	transition: all 0.3s ease-in;
	-webkit-transform: translateZ(0px) rotateX(0deg);
	-moz-transform: translateZ(0px) rotateX(0deg);
	-ms-transform: translateZ(0px) rotateX(0deg);
	transform: translateZ(0px) rotateX(0deg); 
	opacity: 1;
}

/* Effect 6: side fall */
.md-effect-6.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-6 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: translate(30%) translateZ(600px) rotate(10deg); 
	-moz-transform: translate(30%) translateZ(600px) rotate(10deg);
	-ms-transform: translate(30%) translateZ(600px) rotate(10deg);
	transform: translate(30%) translateZ(600px) rotate(10deg); 
	opacity: 0;
}

.in.md-effect-6 .modal-dialog {
	-webkit-transition: all 0.3s ease-in;
	-moz-transition: all 0.3s ease-in;
	transition: all 0.3s ease-in;
	-webkit-transform: translate(0%) translateZ(0) rotate(0deg);
	-moz-transform: translate(0%) translateZ(0) rotate(0deg);
	-ms-transform: translate(0%) translateZ(0) rotate(0deg);
	transform: translate(0%) translateZ(0) rotate(0deg);
	opacity: 1;
}

/* Effect 7:  slide and stick to top */
.md-effect-7{
	top: 0;
}

.fade.md-effect-7 .modal-dialog {
	-webkit-transform: translateY(-200%);
	-moz-transform: translateY(-200%);
	-ms-transform: translateY(-200%);
	transform: translateY(-200%);
	-webkit-transition: all .3s;
	-moz-transition: all .3s;
	transition: all .3s;
	opacity: 0;
}

.in.md-effect-7 .modal-dialog {
	-webkit-transform: translateY(0%);
	-moz-transform: translateY(0%);
	-ms-transform: translateY(0%);
	transform: translateY(0%);
	border-radius: 0 0 3px 3px;
	opacity: 1;
}

/* Effect 8: 3D flip horizontal */
.md-effect-8.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-8 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: rotateY(-70deg);
	-moz-transform: rotateY(-70deg);
	-ms-transform: rotateY(-70deg);
	transform: rotateY(-70deg);
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
	opacity: 0;
}

.in.md-effect-8 .modal-dialog {
	-webkit-transform: rotateY(0deg);
	-moz-transform: rotateY(0deg);
	-ms-transform: rotateY(0deg);
	transform: rotateY(0deg);
	opacity: 1;
}

/* Effect 9: 3D flip vertical */
.md-effect-9.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-9 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: rotateX(-70deg);
	-moz-transform: rotateX(-70deg);
	-ms-transform: rotateX(-70deg);
	transform: rotateX(-70deg);
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
	opacity: 0;
}

.in.md-effect-9 .modal-dialog {
	-webkit-transform: rotateX(0deg);
	-moz-transform: rotateX(0deg);
	-ms-transform: rotateX(0deg);
	transform: rotateX(0deg);
	opacity: 1;
}

/* Effect 10: 3D sign */
.md-effect-10.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-10 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: rotateX(-60deg);
	-moz-transform: rotateX(-60deg);
	-ms-transform: rotateX(-60deg);
	transform: rotateX(-60deg);
	-webkit-transform-origin: 50% 0;
	-moz-transform-origin: 50% 0;
	transform-origin: 50% 0;
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-10 .modal-dialog {
	-webkit-transform: rotateX(0deg);
	-moz-transform: rotateX(0deg);
	-ms-transform: rotateX(0deg);
	transform: rotateX(0deg);
	opacity: 1;
}

/* Effect 11: Super scaled */
.fade.md-effect-11 .modal-dialog {
	-webkit-transform: scale(2);
	-moz-transform: scale(2);
	-ms-transform: scale(2);
	transform: scale(2);
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-11 .modal-dialog {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);
	opacity: 1;
}

/* Effect 12:  Just me */
.fade.md-effect-12 .modal-dialog {
	-webkit-transform: scale(0.8);
	-moz-transform: scale(0.8);
	-ms-transform: scale(0.8);
	transform: scale(0.8);
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-12 ~ .modal {
	background: #e74c3c;
} 

.fade.md-effect-12 .modal-dialog {
	background: transparent;
}

.in.md-effect-12 .modal-dialog {
	-webkit-transform: scale(1);
	-moz-transform: scale(1);
	-ms-transform: scale(1);
	transform: scale(1);
	opacity: 1;
}

/* Effect 13: 3D slit */
.md-effect-13.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-13 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: translateZ(-3000px) rotateY(90deg);
	-moz-transform: translateZ(-3000px) rotateY(90deg);
	-ms-transform: translateZ(-3000px) rotateY(90deg);
	transform: translateZ(-3000px) rotateY(90deg);
	opacity: 0;
}

.in.md-effect-13 .modal-dialog {
	-webkit-animation: slit .7s forwards ease-out;
	-moz-animation: slit .7s forwards ease-out;
	animation: slit .7s forwards ease-out;
}

@-webkit-keyframes slit {
	50% { -webkit-transform: translateZ(-250px) rotateY(89deg); opacity: .5; -webkit-animation-timing-function: ease-out;}
	100% { -webkit-transform: translateZ(0) rotateY(0deg); opacity: 1; }
}

@-moz-keyframes slit {
	50% { -moz-transform: translateZ(-250px) rotateY(89deg); opacity: .5; -moz-animation-timing-function: ease-out;}
	100% { -moz-transform: translateZ(0) rotateY(0deg); opacity: 1; }
}

@keyframes slit {
	50% { transform: translateZ(-250px) rotateY(89deg); opacity: 1; animation-timing-function: ease-in;}
	100% { transform: translateZ(0) rotateY(0deg); opacity: 1; }
}

/* Effect 14:  3D Rotate from bottom */
.md-effect-14.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-14 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: translateY(100%) rotateX(90deg);
	-moz-transform: translateY(100%) rotateX(90deg);
	-ms-transform: translateY(100%) rotateX(90deg);
	transform: translateY(100%) rotateX(90deg);
	-webkit-transform-origin: 0 100%;
	-moz-transform-origin: 0 100%;
	transform-origin: 0 100%;
	opacity: 0;
	-webkit-transition: all 0.3s ease-out;
	-moz-transition: all 0.3s ease-out;
	transition: all 0.3s ease-out;
}

.in.md-effect-14 .modal-dialog {
	-webkit-transform: translateY(0%) rotateX(0deg);
	-moz-transform: translateY(0%) rotateX(0deg);
	-ms-transform: translateY(0%) rotateX(0deg);
	transform: translateY(0%) rotateX(0deg);
	opacity: 1;
}

/* Effect 15:  3D Rotate in from left */
.md-effect-15.modal {
	-webkit-perspective: 1300px;
	-moz-perspective: 1300px;
	perspective: 1300px;
}

.fade.md-effect-15 .modal-dialog {
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
	-moz-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
	-ms-transform: translateZ(100px) translateX(-30%) rotateY(90deg);
	transform: translateZ(100px) translateX(-30%) rotateY(90deg);
	-webkit-transform-origin: 0 100%;
	-moz-transform-origin: 0 100%;
	transform-origin: 0 100%;
	opacity: 0;
	-webkit-transition: all 0.3s;
	-moz-transition: all 0.3s;
	transition: all 0.3s;
}

.in.md-effect-15 .modal-dialog {
	-webkit-transform: translateZ(0px) translateX(0%) rotateY(0deg);
	-moz-transform: translateZ(0px) translateX(0%) rotateY(0deg);
	-ms-transform: translateZ(0px) translateX(0%) rotateY(0deg);
	transform: translateZ(0px) translateX(0%) rotateY(0deg);
	opacity: 1;
}

/* builder elements*/
.blank_spacer { min-height: 0}

/* Border Radius */
.border-radius-1 {
	border-radius: 1px;
}
.border-radius-2 {
	border-radius: 2px;
}
.border-radius-3 {
	border-radius: 3px;
}
.border-radius-4 {
	border-radius: 4px;
}
.border-radius-5 {
	border-radius: 5px;
}
.border-radius-6 {
	border-radius: 6px;
}
.border-radius-7 {
	border-radius: 7px;
}
.border-radius-8 {
	border-radius: 8px;
}
.border-radius-9 {
	border-radius: 9px;
}
.border-radius-10 {
	border-radius: 10px;
}
.border-radius-11 {
	border-radius: 11px;
}
.border-radius-12 {
	border-radius: 12px;
}
.border-radius-13 {
	border-radius: 13px;
}
.border-radius-14 {
	border-radius: 14px;
}
.border-radius-15 {
	border-radius: 15px;
}
.border-radius-20 {
	border-radius: 20px;
}
.border-radius-25 {
	border-radius: 25px;
}
.border-radius-30 {
	border-radius: 30px;
}
/* Shapes */
.circle {
	-webkit-border-radius: 50%;
	-o-border-radius: 50%;
	border-radius: 50%;
}
.ellipse {
	-webkit-border-radius: 100%;
	-o-border-radius: 100%;
	border-radius: 100%;
}
.leaf {
	-webkit-border-radius: 3% 85% 3% 85%;
	-o-border-radius: 3% 85% 3% 85%;
	border-radius: 3% 85% 3% 85%;
}
.lemon {
	-webkit-border-radius: 5% 75% 15% 75%;
	-o-border-radius: 5% 75% 15% 75%;
	border-radius: 5% 75% 15% 75%;
}
.bullet { 
	-webkit-border-radius: 5% 80% 80% 5% / 5% 60% 60% 5%; 
	-o-border-radius: 5% 80% 80% 5% / 5% 60% 60% 5%; 
	border-radius: 5% 80% 80% 5% / 5% 60% 60% 5%; 
} 
.mushroom {
	-webkit-border-radius: 50% 50% 50% 50%/60% 60% 30% 30%;
	-o-border-radius: 50% 50% 50% 50%/60% 60% 30% 30%;
	border-radius: 50% 50% 50% 50%/60% 60% 30% 30%;
}
.old-screen { 
	-webkit-border-radius: 75% / 12%;
	-o-border-radius: 75% / 12%;
	border-radius: 75% / 12%;
	position: relative;
}

.sdf-section-anchor {
	position:absolute;
	visibility:hidden;
	top:0;
}

/* Portfolio & Blog Grids */
body.wide-layout { 
   overflow-x: hidden;
}
.wide-layout #sortable-posts-items.no-space-col [class*="col-"],
.wide-layout #sortable-portfolio-items.no-space-col [class*="col-"],
.boxed-layout #sortable-posts-items.no-space-col [class*="col-"],
.boxed-layout #sortable-portfolio-items.no-space-col [class*="col-"],
.container-fluid #sortable-posts-items.no-space-col [class*="col-"],
.container-fluid #sortable-portfolio-items.no-space-col [class*="col-"]{ padding-left:0;  padding-right:0; }

/* Blog/Portfolio Grid */
.visuallyhidden {
    position: absolute;
    overflow: hidden;  
    border: 0;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    clip: rect(0 0 0 0); 
}
article.isotope-item span.image-overlay {
	position: absolute;
	display: inline-block;
	height: 100%;
	width: 100%;
	overflow: hidden;
	left: 0;
	bottom: 0;
	background-color: <?php if($sdf_theme_styles['theme_grid_overlay_color']): echo $sdf_theme_styles['theme_grid_overlay_color']; endif; ?>;
	opacity: 0;
	visibility: visible;
	<?php if($sdf_theme_styles['theme_grid_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-ms-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
<?php endif; ?>
	z-index: 200;
}

.touch article.isotope-item span.image-overlay{
	display: none !important;
	height: 0 !important;
	width: 0 !important;
	opacity: 0 !important;
	visibility: hidden !important;
}
article.isotope-item:hover span.image-overlay{
	height:100% !important;
}

article.isotope-item .item-image:hover span.image-overlay{
	opacity: 1;
	filter: alpha(opacity=100);
}

.item-image { 
	overflow: hidden;
	-webkit-backface-visibility: hidden;
	position:relative;
}
.image-holder { 
	display: block;
	position: relative;
	overflow: hidden;
	width: 100%;
	-moz-transform: translateZ(0px);
	-webkit-transform: translateZ(0px);
	transform: translateZ(0px);
}
.item-image .image-holder {
<?php if($sdf_theme_styles['theme_grid_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-ms-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_grid_transition']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_transform']): ?>
-webkit-transform: <?php echo $sdf_theme_styles['theme_grid_transform']; ?>;
-moz-transform: <?php echo $sdf_theme_styles['theme_grid_transform']; ?>;
-ms-transform: <?php echo $sdf_theme_styles['theme_grid_transform']; ?>;
-o-transform: <?php echo $sdf_theme_styles['theme_grid_transform']; ?>;
transform: <?php echo $sdf_theme_styles['theme_grid_transform']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_transform_style']): ?>
-webkit-transform-style: <?php echo $sdf_theme_styles['theme_grid_transform_style']; ?>;
transform-style: <?php echo $sdf_theme_styles['theme_grid_transform_style']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_transform_origin']): ?>
-webkit-transform-origin: <?php echo $sdf_theme_styles['theme_grid_transform_origin']; ?>;
transform-origin: <?php echo $sdf_theme_styles['theme_grid_transform_origin']; ?>;
<?php endif; ?>
opacity: <?php echo ($sdf_theme_styles['theme_grid_image_opacity'] != '') ? $sdf_theme_styles['theme_grid_image_opacity'] : '1'; ?>;
<?php if($sdf_theme_styles['theme_grid_image_grayscale'] == 'yes'): ?>
	webkit-filter: grayscale(1);
	-webkit-filter: grayscale(100%);
	-moz-filter: grayscale(100%);
	filter: gray;
	filter: grayscale(100%);
	filter: url(../img/desaturate.svg#grayscale);
<?php endif; ?>
}

.isotope-item .item-image:hover .image-holder {
<?php if($sdf_theme_styles['theme_grid_transform_hover']): ?>
-webkit-transform: <?php echo $sdf_theme_styles['theme_grid_transform_hover']; ?>;
-moz-transform: <?php echo $sdf_theme_styles['theme_grid_transform_hover']; ?>;
-ms-transform: <?php echo $sdf_theme_styles['theme_grid_transform_hover']; ?>;
-o-transform: <?php echo $sdf_theme_styles['theme_grid_transform_hover']; ?>;
transform: <?php echo $sdf_theme_styles['theme_grid_transform_hover']; ?>;
<?php endif; ?>
opacity: <?php echo ($sdf_theme_styles['theme_grid_image_opacity_hover'] != '') ? $sdf_theme_styles['theme_grid_image_opacity_hover'] : '1'; ?>;
<?php if($sdf_theme_styles['theme_grid_image_grayscale'] == 'yes'): ?>
    -webkit-filter: grayscale(0);
    filter: none;
<?php endif; ?>
}

article.isotope-item span.image-overlay span.image-text-outer{
	display: table;
	text-align: center;
	vertical-align: middle;
	width: 100%;
	height: 100%;
	overflow:hidden;

}

article.isotope-item span.image-overlay span span.image-text-inner{
	display: table-cell;
	text-align: center;
	vertical-align: middle;
	width: 100%;
	height: 100%;
	margin: 0;
	padding: 0;
}

article.isotope-item span.image-overlay span span.image-text-inner .project-title,
article.isotope-item span.image-overlay span span.image-text-inner .post-title{
    display: block;
		margin-bottom: 20px;
}

article.isotope-item .btn-overlay-holder,
article.isotope-item .btn-overlay-holder .feature_holder_icons{
	display: inline-block;
}

article.isotope-item .btn-overlay-holder{
    width: 100%;
}

.project-data {
	margin-top: 10px;
}
.project-data > ul {
	margin-left: 0;
}
.post-data,
.latest-post-excerpt {
	margin-top: 15px;
}
.post-meta.fa-ul {
    margin-left: 0;
}
.post-meta li {
	margin-right: 10px;
}
.post-meta li:last-child {
	margin-right: 0;
}
.post-meta-item i {
	text-align: center;
	margin-right: 10px;
}

article.isotope-item .btn-overlay-holder .btn-overlay {
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_text_size'] || $sdf_theme_styles['theme_grid_item_overlay_button_text_family'] || $sdf_theme_styles['theme_grid_item_overlay_button_text_weight']): ?>
	<?php if($sdf_theme_styles['theme_grid_item_overlay_button_text_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_text_size']; ?><?php echo $sdf_theme_styles['theme_grid_item_overlay_button_text_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_grid_item_overlay_button_text_family'], $sdf_theme_styles['theme_grid_item_overlay_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_width']) : ?>width:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_width']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_height']) : ?>height:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_line_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_line_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_margin']) : ?>margin:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_margin']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_padding']) : ?>padding:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_padding']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_grid_item_overlay_button_border_width'] != '') ? $sdf_theme_styles['theme_grid_item_overlay_button_border_width'] : '0'; ?>;
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_grid_item_overlay_button_border_radius']) ? $sdf_theme_styles['theme_grid_item_overlay_button_border_radius'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_grid_item_overlay_button_border_radius']) ? $sdf_theme_styles['theme_grid_item_overlay_button_border_radius'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_grid_item_overlay_button_border_radius']) ? $sdf_theme_styles['theme_grid_item_overlay_button_border_radius'] : '0'; ?>; 
text-align: center;
vertical-align: middle;
white-space: nowrap;
display: inline-block;
font-weight: normal;
cursor: pointer;
	color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_text_color']; ?>;
	background-color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_bg_color']; ?>;
	<?php $item_overlay_button_text_shadow = ($sdf_theme_styles['theme_grid_item_overlay_button_text_shadow'] != '') ? $sdf_theme_styles['theme_grid_item_overlay_button_text_shadow'] : 'none'; ?>
	text-shadow: <?php echo $item_overlay_button_text_shadow; ?>;
	<?php if($sdf_theme_styles['theme_grid_item_overlay_button_box_shadow'] != '' || $sdf_theme_styles['theme_grid_item_overlay_button_box_shadow_color'] != ''): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_box_shadow_color']; ?>;
	box-shadow: <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_box_shadow_color']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles['theme_grid_item_overlay_button_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_border_color']; ?>;<?php endif; ?>
}
article.isotope-item .btn-overlay-holder .btn-overlay:last-child {
	margin-right: 0;
}

article.isotope-item .btn-overlay-holder .btn-overlay:hover {
color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_bg_hover_color']; ?>;
<?php $item_overlay_button_text_shadow_hover = ($sdf_theme_styles['theme_grid_item_overlay_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_grid_item_overlay_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $item_overlay_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_grid_item_overlay_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_grid_item_overlay_button_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_grid_item_overlay_button_border_hover_color']; ?>;<?php endif; ?>

<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}
.btn-overlay .fa {
line-height: inherit !important;
vertical-align: middle;
min-width: 1em;
min-height: 1em;
}

.nav-pills.nav-center {
		text-align: center;
}
.nav-pills.nav-center > li {
    float: none;
}
.nav.nav-center > li {
    display: inline-block;
}
/* background video */

.hero_block.yt-bg-video.player{
	display:block;
}
.mb_YTPlayer .buttonBar{
	display: none !important;
}
.sdf-video-control {
	bottom: 30px;
	left: 30px;
	cursor: pointer;
	opacity: 0.85;
	position: fixed;
	z-index: 2;
	text-align: center;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
	-webkit-backface-visibility: hidden;
  width: 30px;
  height: 30px;
  display: block;
}
.sdf-video-control.sdf-video-play {
	left: 75px;
}

/* Photostream */
.widget-photostream {
	margin: 0;
	padding: 0;
	overflow: hidden;
	position: relative;
	display: block;
    background: none;
}
.widget-photostream.image-frame {
	padding: 7px;
}
.widget-photostream:hover { 
	transition: all 0.2s ease-in-out 0s;
}
/* Responsive Image Grid */
.image-grid{
	margin: 0px auto;
	position: relative;
	height: auto;
	width: 100%;
}
.image-grid ul {
	list-style: none;
	display: block;
	width: 100%;
	margin: 0;
	padding: 0 !important;
}

/* Clear floats by Nicolas Gallagher: http://nicolasgallagher.com/micro-clearfix-hack/ */

.image-grid ul:before,
.image-grid ul:after{
	content: '';
    display: table;
}

.image-grid ul:after {
    clear: both;
}

.image-grid ul {
    zoom: 1; /* For IE 6/7 (trigger hasLayout) */
} 

.image-grid ul li {
	margin: 0;
	padding: 0;
	float: left;
	position: relative;
	display: block;
	overflow: hidden;
	opacity: 0.8;
	transition: all 0.3s ease-in-out 0s;
}
.image-grid ul:hover li {
	opacity: 0.5;
}
.image-grid ul li:hover {
	opacity: 1;
}
.image-grid ul li a{
	display: block;
	outline: none;
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	-webkit-backface-visibility: hidden;
	-moz-backface-visibility: hidden;
	-o-backface-visibility: hidden;
	-ms-backface-visibility: hidden;
	backface-visibility: hidden;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	-o-transform-style: preserve-3d;
	-ms-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-background-size: 100% 100%;
	-moz-background-size: 100% 100%;
	background-size: 100% 100%;
	background-position: center center;
	background-repeat: no-repeat;
}
.image-grid-loading:after,
.image-grid-loading:before{
	display: none;
}

.image-grid-loading-image{
	display: none;
}

.image-grid-loading .image-grid-loading-image{
	position: relative;
	width: 30px;
	height: 30px;
	left: 50%;
	margin: 100px 0 0 -15px;
	display: block;
}
/* No JavaScript Fallback */
.no-js .image-grid{
	width: 600px;
}

.no-js .image-grid ul li,
.no-js .image-grid ul li a{
	width: 100px;
	height: 100px;
}
.no-js .image-grid ul li a img {
	width: 100%;
}

body { 
<?php if($sdf_theme_styles['theme_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_font_size']; ?>;<?php } ?>
<?php get_font_family_weight($sdf_theme_styles['theme_font_family'], ''); ?>
<?php if($sdf_theme_styles['theme_font_color']){ ?>color:<?php echo $sdf_theme_styles['theme_font_color']; ?>;<?php } ?>
}

<?php if($sdf_theme_styles['theme_font_link_color']): ?>
a {
color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>;
}
<?php endif; ?>
a {
<?php if($sdf_theme_styles['theme_font_link_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_font_link_transition']; ?>;
	-moz-transition: <?php echo $sdf_theme_styles['theme_font_link_transition']; ?>;
	-o-transition: <?php echo $sdf_theme_styles['theme_font_link_transition']; ?>;
	transition: <?php echo $sdf_theme_styles['theme_font_link_transition']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_font_link_text_decoration']): ?>
text-decoration:<?php echo $sdf_theme_styles['theme_font_link_text_decoration']; ?>;
<?php endif; ?>
}
article .entry-content .clearfix > p a {
<?php if($sdf_theme_styles['theme_font_link_bg_color']): ?>
background-color: <?php echo ($sdf_theme_styles['theme_font_link_bg_color']) ? $sdf_theme_styles['theme_font_link_bg_color'] : 'transparent'; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_font_link_box_shadow'] && $sdf_theme_styles['theme_font_link_box_shadow_color']): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_font_link_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_font_link_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_font_link_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_font_link_box_shadow_color']; ?>;
<?php endif; ?>
<?php if(($sdf_theme_styles['theme_font_link_box_shadow'] && $sdf_theme_styles['theme_font_link_box_shadow_color']) || ($sdf_theme_styles['theme_font_link_hover_box_shadow'] && $sdf_theme_styles['theme_font_link_hover_box_shadow_color'])): ?>
padding: 0.01rem 0.2rem;
<?php endif; ?>
}
<?php if($sdf_theme_styles['theme_font_link_hover_text_decoration']): ?>
a:hover, a:focus{
text-decoration:<?php echo $sdf_theme_styles['theme_font_link_hover_text_decoration']; ?>;
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_font_link_hover_color']): ?>
a:hover  {
color: <?php echo $sdf_theme_styles['theme_font_link_hover_color']; ?>;
}
<?php endif; ?>

article .entry-content .clearfix > p a:hover {
<?php if($sdf_theme_styles['theme_font_link_hover_bg_color']): ?>
background-color: <?php echo ($sdf_theme_styles['theme_font_link_hover_bg_color']) ? $sdf_theme_styles['theme_font_link_hover_bg_color'] : 'transparent'; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_font_link_hover_box_shadow'] && $sdf_theme_styles['theme_font_link_hover_box_shadow_color']): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_font_link_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_font_link_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_font_link_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_font_link_hover_box_shadow_color']; ?>;
<?php endif; ?>
}

<?php 
$id = 'page-header';
$id_var = 'page_header';
?>
/* Page */
@media (min-width:992px){
<?php $page_padding_top = ($sdf_theme_styles['page_padding_top'] != '') ? $sdf_theme_styles['page_padding_top'] : '0'; ?>
<?php $page_padding_bottom = ($sdf_theme_styles['page_padding_bottom'] != '') ? $sdf_theme_styles['page_padding_bottom'] : '0'; ?>
#left-widget-area,
#right-widget-area,
#sdf-content {
padding-top: <?php echo $page_padding_top; ?>;
padding-bottom: <?php echo $page_padding_bottom; ?>;
}
<?php $featured_image_padding_top = ($sdf_theme_styles['featured_image_padding_top'] != '') ? $sdf_theme_styles['featured_image_padding_top'] : '0'; ?>
<?php $featured_image_padding_bottom = ($sdf_theme_styles['featured_image_padding_bottom'] != '') ? $sdf_theme_styles['featured_image_padding_bottom'] : '0'; ?>
.featured-image {
padding-top: <?php echo $featured_image_padding_top; ?>;
padding-bottom: <?php echo $featured_image_padding_bottom; ?>;
}
}
/* Page Header Area */
<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>

	<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
	#<?php echo $id; ?> {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
	
	#<?php echo $id; ?> {
	background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
	background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
	background-attachment: fixed;
	background-position: 50% 0;
	background-repeat: no-repeat;
	position: relative;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	}
	
	<?php else:  ?>
	
	<?php if($sdf_theme_styles[$id_var.'_bg_color'] || $sdf_theme_styles[$id_var.'_bg_image']): ?>
		#<?php echo $id; ?> {
			<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
		}
	<?php endif; ?>
	
	<?php endif; ?>

<?php endif; ?>

<?php $page_header_padding_top = ($sdf_theme_styles['page_header_padding_top'] != '') ? $sdf_theme_styles['page_header_padding_top'] : 0; ?>
<?php $page_header_padding_bottom = ($sdf_theme_styles['page_header_padding_bottom'] != '') ? $sdf_theme_styles['page_header_padding_bottom'] : 0; ?>
#<?php echo $id; ?> {
padding-top: <?php echo $page_header_padding_top; ?>;
padding-bottom: <?php echo $page_header_padding_bottom; ?>;
}


#<?php echo $id; ?> .entry-title,
#<?php echo $id; ?> .entry-subtitle {
  <?php if($sdf_theme_styles[$id_var.'_alignment']): ?>
	text-align: <?php echo $sdf_theme_styles[$id_var.'_alignment']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_font_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles[$id_var.'_font_text_transform']; ?>;<?php endif; ?>
}
 
#<?php echo $id; ?> .entry-title, #<?php echo $id; ?> h1.entry-title, #<?php echo $id; ?> h2.entry-title{
	<?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_color']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_font_family'], $sdf_theme_styles[$id_var.'_font_weight']); ?>
}

<?php if($sdf_theme_styles[$id_var.'_font_hover_color']): ?> 
#<?php echo $id; ?> .entry-title:hover, #<?php echo $id; ?> h1.entry-title:hover, #<?php echo $id; ?> h2.entry-title:hover {
  <?php if($sdf_theme_styles[$id_var.'_font_hover_color']): ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_hover_color']; ?>;<?php endif; ?>
}
<?php endif; ?>

#<?php echo $id; ?> .entry-title a{
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_link_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles[$id_var.'_link_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_link_text_decoration']; ?>;<?php endif; ?>
}

#<?php echo $id; ?> .entry-title a:hover, #<?php echo $id; ?> .entry-title a:focus{
  <?php if($sdf_theme_styles[$id_var.'_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_hover_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles[$id_var.'_link_hover_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_link_hover_text_decoration']; ?>;<?php endif; ?>
}

<?php if($sdf_theme_styles[$id_var.'_font_subtitle_size'] || $sdf_theme_styles[$id_var.'_font_subtitle_color'] || $sdf_theme_styles[$id_var.'_font_subtitle_weight']): ?>
#<?php echo $id; ?> .entry-subtitle{
	<?php if($sdf_theme_styles[$id_var.'_font_subtitle_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles[$id_var.'_font_subtitle_text_transform']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_font_subtitle_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_subtitle_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_subtitle_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_subtitle_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_subtitle_color']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_font_subtitle_family'], $sdf_theme_styles[$id_var.'_font_subtitle_weight']); ?>
}
<?php endif; ?>

<?php $entry_title_padding_top = ($sdf_theme_styles['entry_title_padding_top'] != '') ? $sdf_theme_styles['entry_title_padding_top'] : 0; ?>
<?php $entry_title_padding_bottom = ($sdf_theme_styles['entry_title_padding_bottom'] != '') ? $sdf_theme_styles['entry_title_padding_bottom'] : 0; ?>
header.entry-title {
padding-top: <?php echo $entry_title_padding_top; ?>;
padding-bottom: <?php echo $entry_title_padding_bottom; ?>;
}

.teaser-title h1,
.teaser-title h2,
.teaser-title h3,
.teaser-title h4,
.teaser-title h5,
.teaser-title h6{
	margin: 0;
}

.sdf-teaser-box .teaser-icon {
	text-align: center;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>	
	display: table-cell;
}
/* firefox Fix */
.media-body .img-responsive{
width: 100%;
}
.sdf-teaser-box .teaser-icon.text-center {
	width: auto;
	height: auto;
	display: block;
}
.sdf-teaser-box .teaser-icon .btn {
	text-align: center;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>	
}
<?php 
$id = 'page-breadcrumbs';
$id_var = 'breadcrumbs';
?>
/* Breadcrumbs */
<?php $breadcrumbs_padding_top = ($sdf_theme_styles['breadcrumbs_padding_top'] != '') ? $sdf_theme_styles['breadcrumbs_padding_top'] : 0; ?>
<?php $breadcrumbs_padding_bottom = ($sdf_theme_styles['breadcrumbs_padding_bottom'] != '') ? $sdf_theme_styles['breadcrumbs_padding_bottom'] : 0; ?>
#<?php echo $id; ?> {
padding-top: <?php echo $breadcrumbs_padding_top; ?>;
padding-bottom: <?php echo $breadcrumbs_padding_bottom; ?>;
}

.breadcrumb > li + li:before {
    color: <?php echo $sdf_theme_styles[$id_var.'_font_color']; ?>;
    content: "<?php echo( html_entity_decode( sdfGo()->sdf_get_global( 'breadcrumbs','bread_delim' ), ENT_COMPAT, 'UTF-8')); ?> ";
}
<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>

	<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
	#<?php echo $id; ?> {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
	
	#<?php echo $id; ?> {
	background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
	background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
	background-attachment: fixed;
	background-position: 50% 0;
	background-repeat: no-repeat;
	position: relative;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	}
	
	<?php else:  ?>
	
		<?php if($sdf_theme_styles[$id_var.'_bg_color'] || $sdf_theme_styles[$id_var.'_bg_image']): ?>
			#<?php echo $id; ?> {
				<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
			}
		<?php endif; ?>
	
	<?php endif; ?>

<?php endif; ?>

#<?php echo $id; ?> {
<?php if($sdf_theme_styles[$id_var.'_alignment']): ?>
  text-align: <?php echo $sdf_theme_styles[$id_var.'_alignment']; ?>;
<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles[$id_var.'_text_transform']; ?>;<?php endif; ?>
}
<?php if($sdf_theme_styles[$id_var.'_font_size'] || $sdf_theme_styles[$id_var.'_font_color']): ?>
#<?php echo $id; ?>,
#<?php echo $id; ?> .active {
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_font_family'], ''); ?>
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_color']; ?>;<?php } ?>
}
<?php endif; ?>

<?php if($sdf_theme_styles[$id_var.'_font_size'] || $sdf_theme_styles[$id_var.'_font_hover_color']): ?>
#<?php echo $id; ?>:hover {
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_font_hover_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_font_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>

<?php if($sdf_theme_styles[$id_var.'_font_size'] || $sdf_theme_styles[$id_var.'_link_color']): ?>
#<?php echo $id; ?> a {
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_link_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_color']; ?>;<?php } ?>
}
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_font_size'] || $sdf_theme_styles[$id_var.'_link_hover_color']): ?>
#<?php echo $id; ?> a:hover {
  <?php if($sdf_theme_styles[$id_var.'_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_font_size_type']; ?>;<?php } ?>
  <?php if($sdf_theme_styles[$id_var.'_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_link_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>

/* Headings */
<?php 
foreach (array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ) as $id) :
if($sdf_theme_styles['theme_font_'.$id.'_family'] || $sdf_theme_styles['theme_font_'.$id.'_color'] || $sdf_theme_styles['theme_font_'.$id.'_weight'] || $sdf_theme_styles['theme_font_'.$id.'_margin_top'] || $sdf_theme_styles['theme_font_'.$id.'_margin_bottom']): ?>
<?php echo $id; ?>, .<?php echo $id; ?> {
	<?php get_font_family_weight($sdf_theme_styles['theme_font_'.$id.'_family'], $sdf_theme_styles['theme_font_'.$id.'_weight']); ?>
	<?php if($sdf_theme_styles['theme_font_'.$id.'_color']){ ?>color:<?php echo $sdf_theme_styles['theme_font_'.$id.'_color']; ?>;<?php } ?>
	<?php if($sdf_theme_styles['theme_font_'.$id.'_letter_spacing']){ ?>letter-spacing:<?php echo $sdf_theme_styles['theme_font_'.$id.'_letter_spacing']; ?>;<?php } ?>
	-webkit-font-smoothing: antialiased;
	<?php if($sdf_theme_styles['theme_font_'.$id.'_margin_top']){ ?>margin-top:<?php echo $sdf_theme_styles['theme_font_'.$id.'_margin_top']; ?>;<?php } ?>
	<?php if($sdf_theme_styles['theme_font_'.$id.'_margin_bottom']){ ?>margin-bottom:<?php echo $sdf_theme_styles['theme_font_'.$id.'_margin_bottom']; ?>;<?php } ?>
}
<?php 
endif;
endforeach; 
?>

/* Header */
#sdf-header {
<?php if($sdf_theme_styles['header_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['header_font_size']; ?><?php echo $sdf_theme_styles['header_font_size_type']; ?>;<?php } ?>
<?php if(($sdf_theme_styles['header_custom_bg'] == 'yes') && ($sdf_theme_styles['header_bg_color'] || $sdf_theme_styles['header_bg_image'])): ?>
	<?php get_background_color_image($sdf_theme_styles['header_bg_color'], $sdf_theme_styles['header_bg_image'], $sdf_theme_styles['header_bgrepeat']); ?>
<?php endif; ?>
}
<?php if(($sdf_theme_styles['logo_custom_bg'] == 'yes') && ($sdf_theme_styles['logo_bg_color'] || $sdf_theme_styles['logo_bg_image'])): ?>
#sdf-logo .logo,
#sdf-logo h1.logo {
	background:<?php if($sdf_theme_styles['logo_bg_color']): echo $sdf_theme_styles['logo_bg_color']; endif; if($sdf_theme_styles['logo_bg_image']):
	echo ' url('.$sdf_theme_styles['logo_bg_image'].') '.$sdf_theme_styles['logo_bgrepeat']; endif;
	?>;
}
<?php endif; ?>
#sdf-logo .logo,
#sdf-logo h1.logo,
#sdf-logo h2.logo,
#sdf-logo h3.logo,
#sdf-logo h4.logo,
#sdf-logo h5.logo,
#sdf-logo h6.logo,
#sdf-nav .logo,
#sdf-nav h1.logo,
#sdf-nav h2.logo,
#sdf-nav h3.logo,
#sdf-nav h4.logo,
#sdf-nav h5.logo,
#sdf-nav h6.logo {
<?php $logo_padding_top = ($sdf_theme_styles['logo_padding_top'] != '') ? $sdf_theme_styles['logo_padding_top'] : 0; ?>
padding-top: <?php echo $logo_padding_top; ?>;
<?php $logo_padding_bottom = ($sdf_theme_styles['logo_padding_bottom'] != '') ? $sdf_theme_styles['logo_padding_bottom'] : 0; ?>
padding-bottom: <?php echo $logo_padding_bottom; ?>;
}
#sdf-logo .logo,
#sdf-logo h1.logo,
#sdf-logo h1.logo a,
#sdf-logo h2.logo,
#sdf-logo h2.logo a,
#sdf-logo h3.logo,
#sdf-logo h3.logo a,
#sdf-logo h4.logo,
#sdf-logo h4.logo a,
#sdf-logo h5.logo,
#sdf-logo h5.logo a,
#sdf-logo h6.logo,
#sdf-logo h6.logo a,
#sdf-nav .logo,
#sdf-nav h1.logo,
#sdf-nav h1.logo a,
#sdf-nav h2.logo,
#sdf-nav h2.logo a,
#sdf-nav h3.logo,
#sdf-nav h3.logo a,
#sdf-nav h4.logo,
#sdf-nav h4.logo a,
#sdf-nav h5.logo,
#sdf-nav h5.logo a,
#sdf-nav h6.logo,
#sdf-nav h6.logo a {
<?php if($sdf_theme_styles['logo_font_family'] || $sdf_theme_styles['logo_font_size'] || $sdf_theme_styles['logo_font_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['logo_font_family'], $sdf_theme_styles['logo_font_weight']); ?>
	<?php if($sdf_theme_styles['logo_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['logo_font_size']; ?><?php echo $sdf_theme_styles['logo_font_size_type']; ?>;<?php } ?>
<?php endif; ?>
<?php if($sdf_theme_styles['logo_font_color']){ ?>color:<?php echo $sdf_theme_styles['logo_font_color']; ?>;<?php } ?>
margin: 0;
}
#sdf-logo .logo{
display: block;
<?php if($sdf_theme_styles['logo_align']): ?>
text-align: <?php echo $sdf_theme_styles['logo_align']; ?>;
<?php endif; ?>
line-height: 1
}

.logo a .img-responsive.text-right{
	margin-left: auto;
}
.logo a .img-responsive.text-left{
	margin-right: auto;
}

<?php if($sdf_theme_styles['logo_font_hover_color']): ?>
#sdf-logo:hover {
<?php if($sdf_theme_styles['logo_font_hover_color']){ ?>color:<?php echo $sdf_theme_styles['logo_font_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>
	
#sdf-logo a {
<?php if($sdf_theme_styles['logo_link_color']): ?>
	<?php if($sdf_theme_styles['logo_link_color']){ ?>color:<?php echo $sdf_theme_styles['logo_link_color']; ?>;<?php } ?>
<?php endif; ?>
	text-decoration: none;
}

<?php if($sdf_theme_styles['logo_link_hover_color']): ?>
#sdf-logo a:hover {
<?php if($sdf_theme_styles['logo_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles['logo_link_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>
/* Tagline */
#sdf-logo .tagline {
<?php if($sdf_theme_styles['logo_tagline_font_family'] || $sdf_theme_styles['logo_tagline_font_size'] || $sdf_theme_styles['logo_tagline_font_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['logo_tagline_font_family'], $sdf_theme_styles['logo_tagline_font_weight']); ?>
	<?php if($sdf_theme_styles['logo_tagline_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['logo_tagline_font_size']; ?><?php echo $sdf_theme_styles['logo_tagline_font_size_type']; ?> ;<?php } ?>
<?php endif; ?>
<?php if($sdf_theme_styles['logo_tagline_font_color']){ ?>color:<?php echo $sdf_theme_styles['logo_tagline_font_color']; ?>;<?php } ?>
<?php $tagline_margin = ($sdf_theme_styles['logo_tagline_margin'] != '') ? $sdf_theme_styles['logo_tagline_margin'] : 0; ?>
margin: <?php echo $tagline_margin; ?>;
}

<?php if($sdf_theme_styles['logo_tagline_font_hover_color']): ?>
#sdf-logo .tagline:hover {
<?php if($sdf_theme_styles['logo_tagline_font_hover_color']){ ?>color:<?php echo $sdf_theme_styles['logo_tagline_font_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>

/* Navigation */
.navbar .fa { line-height: 1 !important; }
.navbar-collapse .navbar-nav.navbar-center {
	display: block;
	float:none;
	text-align: center;
}
.navbar-collapse .navbar-nav.navbar-center ul {
	text-align: left;
}
.navbar-nav.navbar-center > li{
    float:none;
	display: inline-block;
}

nav#wpu-navigation .nav,
nav#wpu-navigation .nav a {
<?php if($sdf_theme_styles['theme_navbar_size'] || $sdf_theme_styles['theme_navbar_family'] || $sdf_theme_styles['theme_navbar_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_navbar_family'], $sdf_theme_styles['theme_navbar_weight']); ?>
	<?php if($sdf_theme_styles['theme_navbar_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_navbar_size']; ?><?php echo $sdf_theme_styles['theme_navbar_size_type']; ?>;<?php } ?>
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_transform'] != 'no'): ?>
	text-transform: <?php echo $sdf_theme_styles['theme_navbar_transform']; ?>;
<?php endif; ?>
}

<?php if(sdfGo()->sdf_get_global( 'misc','toggle_admin_bar' ) != '0') : ?>
.logged-in .navbar-sticky.sticky-show,
.logged-in .navbar-fixed-top {
	top: 32px;
}
<?php endif; ?>

/* Default Theme START */

<?php $add_link_margin = $sdf_theme_styles['theme_navbar_link_margin_top'] + $sdf_theme_styles['theme_navbar_link_margin_bottom']; ?>
<?php $add_link_padding = $sdf_theme_styles['theme_navbar_link_padding_top'] + $sdf_theme_styles['theme_navbar_link_padding_bottom']; ?>
<?php $extra_nav_height = $add_link_padding + $add_link_margin; ?>

<?php if($sdf_theme_styles['theme_navbar_border_width'] > 0) : ?>
#navigation-area,
#navigation-area-2 {
	<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
	border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px;
	<?php else: ?> 
	border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px 0 <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px 0;
	<?php endif; ?> 
	<?php if($sdf_theme_styles['theme_navbar_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_navbar_border_style']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_menu_layout'] == 'fixed_top'): ?>
body {
	padding-top: <?php echo (20 + $extra_nav_height)?>px;
}
<?php endif; ?> 

@media (min-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint']; ?>) {
#sdf-header > #navigation-area,
#sdf-header > #navigation-area-2 {
	<?php $navbar_default_margin_top = ($sdf_theme_styles['theme_navbar_margin_top'] > 0) ? $sdf_theme_styles['theme_navbar_margin_top'] : 0; ?>
	margin-top: <?php echo $navbar_default_margin_top; ?>px;
	<?php $navbar_default_margin_bottom = ($sdf_theme_styles['theme_navbar_margin_bottom'] > 0) ? $sdf_theme_styles['theme_navbar_margin_bottom'] : 0; ?>
	margin-bottom: <?php echo $navbar_default_margin_bottom; ?>px;
}
#sdf-header > #navigation-area.navbar-sticky.sticky-show,
#sdf-header > #navigation-area-2.navbar-sticky.sticky-show {
	margin-top: 0;
	margin-bottom: 0;
}
.navbar-default {
    background-color: transparent;
    border: 0;
}
.navbar .nav {
	height: <?php echo (20 + $extra_nav_height); ?>px;
}
.navbar .nav > li {
	height: <?php echo (20 + $extra_nav_height)?>px;
}
.navbar .nav > li > a {
<?php $navbar_link_padding_top = ($sdf_theme_styles['theme_navbar_link_padding_top'] > 0) ? $sdf_theme_styles['theme_navbar_link_padding_top'] : 0; ?>
<?php $navbar_link_padding_left_right = ($sdf_theme_styles['theme_navbar_link_padding_left_right'] > 0) ? $sdf_theme_styles['theme_navbar_link_padding_left_right'] : 0; ?>
<?php $navbar_link_padding_bottom = ($sdf_theme_styles['theme_navbar_link_padding_bottom'] > 0) ? $sdf_theme_styles['theme_navbar_link_padding_bottom'] : 0; ?>
	padding: <?php echo $navbar_link_padding_top; ?>px <?php echo $navbar_link_padding_left_right; ?>px <?php echo $navbar_link_padding_bottom; ?>px;
	<?php $navbar_link_margin_top = ($sdf_theme_styles['theme_navbar_link_margin_top'] > 0) ? $sdf_theme_styles['theme_navbar_link_margin_top'] : 0; ?>
	margin-top: <?php echo $navbar_link_margin_top; ?>px;
	<?php $navbar_link_margin_bottom = ($sdf_theme_styles['theme_navbar_link_margin_bottom'] > 0) ? $sdf_theme_styles['theme_navbar_link_margin_bottom'] : 0; ?>
	margin-bottom: <?php echo $navbar_link_margin_bottom; ?>px;
	<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
	-webkit-border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - $add_link_margin / 2); ?>px;
	border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - $add_link_margin / 2); ?>px;
	<?php endif; ?> 
}
.navbar {
	min-height: <?php echo (20 + $extra_nav_height)?>px;
}
<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
#navigation-area,
#navigation-area-2 {
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
}
<?php endif; ?>
}
#navigation-area,
#navigation-area-2 {
	border-color: <?php echo $sdf_theme_styles['theme_navbar_border_color']; ?>;
	<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	<?php endif; ?>
}	

<?php 
$id_var = 'theme_navbar';
?>
	/* Navigation Area */
	<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>
	
		<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
		#navigation-area,
		#navigation-area-2 {
			-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
				box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
		}
		<?php endif; ?>
		
		<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
		
		#navigation-area,
		#navigation-area-2 {
		background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
		background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
		background-attachment: fixed;
		background-position: 50% 0;
		background-repeat: no-repeat;
		position: relative;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		}
		
		<?php else:  ?>
							
			#navigation-area,
			#navigation-area-2,
			header > #navigation-area,
			header > #navigation-area-2 {
			<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
			}
		
		<?php endif; ?>
	
	<?php endif; ?>

/* Full page area */
<?php 
$full_page_offset = ($sdf_theme_styles['theme_menu_layout'] == 'sticky_overlay') ? 0 : 20 + $extra_nav_height; 
?>
.hero_block.full-page-area{
position:relative;
overflow-x:hidden;
overflow-y:hidden;
min-height:40vh;
height:calc((100vh * (100 / 100)) - <?php echo $full_page_offset; ?>px)
}
@media only screen and (device-aspect-ratio:3/4) and (orientation:landscape){.hero_block.full-page-area{height:calc((672px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (device-aspect-ratio:3/4) and (orientation:portrait){.hero_block.full-page-area{height:calc((928px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (device-aspect-ratio:3/4) and (orientation:portrait){.hero_block.full-page-area.safari-iphone{height:calc((928px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (min-device-width:414px) and (max-device-width:736px) and (orientation:portrait) and (-webkit-device-pixel-ratio:3){.hero_block.full-page-area.safari-iphone{height:calc((634px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (device-aspect-ratio:375/667) and (orientation:portrait) and (-webkit-device-pixel-ratio:2){.hero_block.full-page-area.safari-iphone{height:calc((568px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (device-aspect-ratio:40/71) and (orientation:portrait) and (-webkit-device-pixel-ratio:2){.hero_block.full-page-area.safari-iphone{height:calc((472px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}
@media only screen and (device-aspect-ratio:2/3) and (orientation:portrait) and (-webkit-min-device-pixel-ratio:2){.hero_block.full-page-area.safari-iphone{height:calc((380px * (100 / 100)) - <?php echo $full_page_offset; ?>px)}}

/* Post Format Buttons */
.entry-type {
<?php if($sdf_theme_styles['theme_post_format_button_margin']) : ?>margin:<?php echo $sdf_theme_styles['theme_post_format_button_margin']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_text_size'] || $sdf_theme_styles['theme_post_format_button_text_family'] || $sdf_theme_styles['theme_post_format_button_text_weight']): ?>
	<?php if($sdf_theme_styles['theme_post_format_button_text_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_post_format_button_text_size']; ?><?php echo $sdf_theme_styles['theme_post_format_button_text_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_post_format_button_text_family'], $sdf_theme_styles['theme_post_format_button_text_weight']); ?>
<?php endif;?>

<?php if($sdf_theme_styles['theme_post_format_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_post_format_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_width']) : ?>width:<?php echo $sdf_theme_styles['theme_post_format_button_width']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_height']) : ?>height:<?php echo $sdf_theme_styles['theme_post_format_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_post_format_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_post_format_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_post_format_button_border_width'] != '') ? $sdf_theme_styles['theme_post_format_button_border_width'] : '0'; ?>;
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_post_format_button_border_radius'] != '') ? $sdf_theme_styles['theme_post_format_button_border_radius'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_post_format_button_border_radius'] != '') ? $sdf_theme_styles['theme_post_format_button_border_radius'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_post_format_button_border_radius'] != '') ? $sdf_theme_styles['theme_post_format_button_border_radius'] : '0'; ?>; 
text-align: center;
vertical-align: middle;
white-space: nowrap;
display: inline-block;
font-weight: normal;
cursor: pointer;

color:<?php echo $sdf_theme_styles['theme_post_format_button_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_post_format_button_bg_color']; ?>;
<?php $sharing_button_text_shadow = ($sdf_theme_styles['theme_post_format_button_text_shadow'] != '') ? $sdf_theme_styles['theme_post_format_button_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_post_format_button_box_shadow'] != '' || $sdf_theme_styles['theme_post_format_button_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_post_format_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_post_format_button_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_post_format_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_post_format_button_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_post_format_button_border_color']; ?>;<?php endif; ?>
}
.entry-type:hover {
color:<?php echo $sdf_theme_styles['theme_post_format_button_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_post_format_button_bg_hover_color']; ?>;
<?php $sharing_button_text_shadow = ($sdf_theme_styles['theme_post_format_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_post_format_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_post_format_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_post_format_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_post_format_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_post_format_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_post_format_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_post_format_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_post_format_button_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_post_format_button_border_hover_color']; ?>;<?php endif; ?>
}
.entry-type .fa {
line-height: inherit !important;
vertical-align: middle;
min-width: 1em;
min-height: 1em;
}
<?php 
if ( $sdf_theme_styles['theme_post_format_button_custom_style'] == 'yes' ) {
foreach ( sdfGo()->_wpuGlobal->_sdfPostFormats as $format_slug) {
$id = str_replace( "-", "_", urlencode(strtolower($format_slug)) ).'_post_format_button';
$format_name = ucfirst($format_slug); ?>

.entry-type.<?php echo $format_slug; ?>-format {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_color']; ?>;
<?php $theme_button_text_shadow = ($sdf_theme_styles['theme_'.$id.'_text_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $theme_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
}

.entry-type.<?php echo $format_slug; ?>-format:hover {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_hover_color']; ?>;
<?php $theme_button_text_shadow_hover = ($sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $theme_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_hover_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_hover_color']; ?>;<?php endif; ?>
}
<?php } ?>	
<?php } ?>

/* Social Sharing Icons */
.social-menu.sharing .sharing-icon {
<?php if($sdf_theme_styles['theme_sharing_button_margin']) : ?>margin:<?php echo $sdf_theme_styles['theme_sharing_button_margin']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_sharing_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_sharing_button_height']; ?>;<?php endif; ?>
}
.social-menu.sharing .sharing-btn {
margin: 0 0.7em 0.7em 0;
}
.widget_area .widget .sharing-custom, 
#right-widget-area .widget .sharing-custom, 
#left-widget-area .widget .sharing-custom, 
#footer-widget-area .widget .sharing-custom, 
#footer-bottom-widget-area .widget .sharing-custom, 
#above-content-area .widget .sharing-custom, 
#below-content-area .widget .sharing-custom, 
#header-area-1 .widget .sharing-custom, 
#header-area-2 .widget .sharing-custom, 
#header-area-3 .widget .sharing-custom, 
#header-area-4 .widget .sharing-custom, 
.sharing-custom, 
.share-icons .sharing-custom, 
#sdf-copyright .sharing-custom {
<?php if($sdf_theme_styles['theme_sharing_button_text_size'] || $sdf_theme_styles['theme_sharing_button_text_family'] || $sdf_theme_styles['theme_sharing_button_text_weight']): ?>
	<?php if($sdf_theme_styles['theme_sharing_button_text_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_sharing_button_text_size']; ?><?php echo $sdf_theme_styles['theme_sharing_button_text_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_sharing_button_text_family'], $sdf_theme_styles['theme_sharing_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_sharing_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_sharing_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_sharing_button_width']) : ?>width:<?php echo $sdf_theme_styles['theme_sharing_button_width']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_sharing_button_height']) : ?>height:<?php echo $sdf_theme_styles['theme_sharing_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_sharing_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_sharing_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_sharing_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_sharing_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_sharing_button_border_width'] != '') ? $sdf_theme_styles['theme_sharing_button_border_width'] : '0'; ?>;
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_sharing_button_border_radius']) ? $sdf_theme_styles['theme_sharing_button_border_radius'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_sharing_button_border_radius']) ? $sdf_theme_styles['theme_sharing_button_border_radius'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_sharing_button_border_radius']) ? $sdf_theme_styles['theme_sharing_button_border_radius'] : '0'; ?>; 
text-align: center;
vertical-align: middle;
white-space: nowrap;
display: inline-block;
font-weight: normal;
cursor: pointer;
<?php if ( $sdf_theme_styles['theme_sharing_button_custom_style'] != 'yes' ) : ?>
	color:<?php echo $sdf_theme_styles['theme_sharing_button_text_color']; ?>;
	background-color:<?php echo $sdf_theme_styles['theme_sharing_button_bg_color']; ?>;
	<?php $sharing_button_text_shadow = ($sdf_theme_styles['theme_sharing_button_text_shadow'] != '') ? $sdf_theme_styles['theme_sharing_button_text_shadow'] : 'none'; ?>
	text-shadow: <?php echo $sharing_button_text_shadow; ?>;
	<?php if($sdf_theme_styles['theme_sharing_button_box_shadow'] != '' || $sdf_theme_styles['theme_sharing_button_box_shadow_color'] != ''): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_sharing_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_sharing_button_box_shadow_color']; ?>;
	box-shadow: <?php echo $sdf_theme_styles['theme_sharing_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_sharing_button_box_shadow_color']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles['theme_sharing_button_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_sharing_button_border_color']; ?>;<?php endif; ?>
<?php endif; ?>
}

.widget_area .widget .sharing-custom:hover, 
#right-widget-area .widget .sharing-custom:hover, 
#left-widget-area .widget .sharing-custom:hover, 
#footer-widget-area .widget .sharing-custom:hover, 
#footer-bottom-widget-area .widget .sharing-custom:hover, 
#above-content-area .widget .sharing-custom:hover, 
#below-content-area .widget .sharing-custom:hover, 
#header-area-1 .widget .sharing-custom:hover, 
#header-area-2 .widget .sharing-custom:hover, 
#header-area-3 .widget .sharing-custom:hover, 
#header-area-4 .widget .sharing-custom:hover, 
.sharing-custom:hover, 
.share-icons .sharing-custom:hover, 
#sdf-copyright .sharing-custom:hover {
<?php if ( $sdf_theme_styles['theme_sharing_button_custom_style'] != 'yes' ) : ?>
	color:<?php echo $sdf_theme_styles['theme_sharing_button_text_hover_color']; ?>;
	background-color:<?php echo $sdf_theme_styles['theme_sharing_button_bg_hover_color']; ?>;
	<?php $sharing_button_text_shadow_hover = ($sdf_theme_styles['theme_sharing_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_sharing_button_text_hover_shadow'] : 'none'; ?>
	text-shadow: <?php echo $sharing_button_text_shadow_hover; ?>;
	<?php if($sdf_theme_styles['theme_sharing_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_sharing_button_hover_box_shadow_color'] != ''): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_sharing_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_sharing_button_hover_box_shadow_color']; ?>;
	box-shadow: <?php echo $sdf_theme_styles['theme_sharing_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_sharing_button_hover_box_shadow_color']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles['theme_sharing_button_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_sharing_button_border_hover_color']; ?>;<?php endif; ?>
<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}
.sharing-icon .fa {
line-height: inherit !important;
vertical-align: middle;
min-width: 1em;
min-height: 1em;
}
<?php 
	if ( $sdf_theme_styles['theme_sharing_button_custom_style'] == 'yes' ) {
	foreach ( sdfGo()->_wpuGlobal->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
	
	$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_sharing_button'; 
?>

.sharing-<?php echo $soc_slug; ?> {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_color']; ?>;
<?php $sharing_button_text_shadow = ($sdf_theme_styles['theme_'.$id.'_text_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_color']; ?>;<?php endif; ?>
}

.sharing-<?php echo $soc_slug; ?>:hover {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_hover_color']; ?>;
<?php $sharing_button_text_shadow_hover = ($sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_hover_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_hover_color']; ?>;<?php endif; ?>
}
<?php } ?>	
<?php } ?>
	
/* Social Follow Buttons */
.social-menu.follow .follow-icon {
<?php if($sdf_theme_styles['theme_follow_button_margin']) : ?>margin:<?php echo $sdf_theme_styles['theme_follow_button_margin']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_follow_button_height']; ?>;<?php endif; ?>
}
.social-menu.follow .follow-btn {
margin: 0 0.7em 0 0;
}
.widget_area .widget .follow-custom, 
#right-widget-area .widget .follow-custom, 
#left-widget-area .widget .follow-custom, 
#footer-widget-area .widget .follow-custom, 
#footer-bottom-widget-area .widget .follow-custom, 
#above-content-area .widget .follow-custom, 
#below-content-area .widget .follow-custom, 
#header-area-1 .widget .follow-custom, 
#header-area-2 .widget .follow-custom, 
#header-area-3 .widget .follow-custom, 
#header-area-4 .widget .follow-custom, 
.follow-custom, 
#sdf-copyright .follow-custom {
<?php if($sdf_theme_styles['theme_follow_button_text_size'] || $sdf_theme_styles['theme_follow_button_text_family'] || $sdf_theme_styles['theme_follow_button_text_weight']): ?>
	<?php if($sdf_theme_styles['theme_follow_button_text_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_follow_button_text_size']; ?><?php echo $sdf_theme_styles['theme_follow_button_text_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_follow_button_text_family'], $sdf_theme_styles['theme_follow_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_follow_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_follow_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_width']) : ?>width:<?php echo $sdf_theme_styles['theme_follow_button_width']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_height']) : ?>height:<?php echo $sdf_theme_styles['theme_follow_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_follow_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_follow_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_follow_button_border_width'] != '') ? $sdf_theme_styles['theme_follow_button_border_width'] : '0'; ?>;
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_follow_button_border_radius'] != '') ? $sdf_theme_styles['theme_follow_button_border_radius'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_follow_button_border_radius'] != '') ? $sdf_theme_styles['theme_follow_button_border_radius'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_follow_button_border_radius'] != '') ? $sdf_theme_styles['theme_follow_button_border_radius'] : '0'; ?>; 
text-align: center;
padding: 0;
vertical-align: middle;
white-space: nowrap;
display: inline-block;
cursor: pointer;
color:<?php echo $sdf_theme_styles['theme_follow_button_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_follow_button_bg_color']; ?>;
<?php $follow_button_text_shadow = ($sdf_theme_styles['theme_follow_button_text_shadow'] != '') ? $sdf_theme_styles['theme_follow_button_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $follow_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_follow_button_box_shadow'] != '' || $sdf_theme_styles['theme_follow_button_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_follow_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_follow_button_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_follow_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_follow_button_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_follow_button_border_color']; ?>;<?php endif; ?>
}
#right-widget-area .widget .follow-custom:hover, 
#left-widget-area .widget .follow-custom:hover, 
#footer-widget-area .widget .follow-custom:hover, 
#footer-bottom-widget-area .widget .follow-custom:hover, 
#above-content-area .widget .follow-custom:hover, 
#below-content-area .widget .follow-custom:hover, 
#header-area-1 .widget .follow-custom:hover, 
#header-area-2 .widget .follow-custom:hover, 
#header-area-3 .widget .follow-custom:hover, 
#header-area-4 .widget .follow-custom:hover, 
.follow-custom:hover, 
#sdf-copyright .follow-custom:hover {
color:<?php echo $sdf_theme_styles['theme_follow_button_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_follow_button_bg_hover_color']; ?>;
<?php $follow_button_text_shadow_hover = ($sdf_theme_styles['theme_follow_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_follow_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $follow_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_follow_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_follow_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_follow_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_follow_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_follow_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_follow_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_follow_button_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_follow_button_border_hover_color']; ?>;<?php endif; ?>

<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}
.social-menu li .fa {
line-height: inherit !important;
vertical-align: middle;
min-width: 1em;
min-height: 1em;
}
<?php 
	if ( $sdf_theme_styles['theme_follow_button_custom_style'] == 'yes' ) {
	foreach ( sdfGo()->_wpuGlobal->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
	
	$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ).'_follow_button'; 
?>

.follow-<?php echo $soc_slug; ?> {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_color']; ?>;
<?php $follow_button_text_shadow = ($sdf_theme_styles['theme_'.$id.'_text_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $follow_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_color']; ?>;<?php endif; ?>
}

.follow-<?php echo $soc_slug; ?>:hover {
color:<?php echo $sdf_theme_styles['theme_'.$id.'_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_bg_hover_color']; ?>;
<?php $follow_button_text_shadow_hover = ($sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $follow_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_hover_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_'.$id.'_border_hover_color']; ?>;<?php endif; ?>
}
<?php } ?>	
<?php } ?>

/* Scroll To Top */

#finished-scroll.hiding {
	opacity: 0;
	right: -100px;
}
<?php $scroll_to_top_button_right = ($sdf_theme_styles['theme_scroll_to_top_button_right'] != '') ? $sdf_theme_styles['theme_scroll_to_top_button_right'] : '0'; ?>
<?php $scroll_to_top_button_bottom = ($sdf_theme_styles['theme_scroll_to_top_button_bottom'] != '') ? $sdf_theme_styles['theme_scroll_to_top_button_bottom'] : '0'; ?>
#finished-scroll {
bottom: <?php echo $scroll_to_top_button_bottom; ?>;
right: <?php echo $scroll_to_top_button_right; ?>;
cursor: pointer;
position: fixed;
z-index: 10001;
text-align: center;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
	-webkit-backface-visibility: hidden;
}

a.to-top-button {
<?php if($sdf_theme_styles['theme_scroll_to_top_button_text_size'] || $sdf_theme_styles['theme_scroll_to_top_button_text_family'] || $sdf_theme_styles['theme_scroll_to_top_button_text_weight']): ?>
	<?php if($sdf_theme_styles['theme_scroll_to_top_button_text_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_text_size']; ?><?php echo $sdf_theme_styles['theme_scroll_to_top_button_text_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_scroll_to_top_button_text_family'], $sdf_theme_styles['theme_scroll_to_top_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_scroll_to_top_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_width']) : ?>width:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_width']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_height']) : ?>height:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_height']) : ?>line-height:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_height']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_scroll_to_top_button_border_width'] != '') ? $sdf_theme_styles['theme_scroll_to_top_button_border_width'] : '0'; ?>;
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_scroll_to_top_button_border_radius']) ? $sdf_theme_styles['theme_scroll_to_top_button_border_radius'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_scroll_to_top_button_border_radius']) ? $sdf_theme_styles['theme_scroll_to_top_button_border_radius'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_scroll_to_top_button_border_radius']) ? $sdf_theme_styles['theme_scroll_to_top_button_border_radius'] : '0'; ?>; 
text-align: center;
vertical-align: middle;
white-space: nowrap;
display: inline-block;
cursor: pointer;

color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_text_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_bg_color']; ?>;
<?php $sharing_button_text_shadow = ($sdf_theme_styles['theme_scroll_to_top_button_text_shadow'] != '') ? $sdf_theme_styles['theme_scroll_to_top_button_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_scroll_to_top_button_box_shadow'] != '' || $sdf_theme_styles['theme_scroll_to_top_button_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_scroll_to_top_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_scroll_to_top_button_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_scroll_to_top_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_scroll_to_top_button_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_border_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_border_color']; ?>;<?php endif; ?>

<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}

a.to-top-button:hover {
color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_text_hover_color']; ?>;
background-color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_bg_hover_color']; ?>;
<?php $sharing_button_text_shadow_hover = ($sdf_theme_styles['theme_scroll_to_top_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_scroll_to_top_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $sharing_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_scroll_to_top_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_scroll_to_top_button_border_hover_color']): ?>border-color:<?php echo $sdf_theme_styles['theme_scroll_to_top_button_border_hover_color']; ?>;<?php endif; ?>
text-decoration: none;
}
a.to-top-button .fa {
line-height: inherit !important;
vertical-align: middle;
min-width: 1em;
min-height: 1em;
}

header > #navigation-area .widget ul.social-menu,
header > #navigation-area-2 .widget ul.social-menu {
    <?php $soc_menu_height = (20 + $extra_nav_height + $sdf_theme_styles['theme_navbar_border_width'] * 2); ?>
	height: <?php echo $soc_menu_height; ?>px;
	margin: 0;
}
header > #navigation-area .widget_area li,
header > #navigation-area-2 .widget_area li {
	margin-bottom: 0;
}
header > #navigation-area .widget li,
header > #navigation-area-2 .widget li {
    padding: 0;
	margin-bottom: <?php echo (($soc_menu_height - 20) / 2); ?>px;
	margin-top: <?php echo (($soc_menu_height - 20) / 2); ?>px;
}
}
.navbar-default .navbar-brand {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
}

.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
  background-color: transparent;
}

.navbar-default .navbar-text {
  color: <?php echo $sdf_theme_styles['theme_navbar_text_color']; ?>;
}
.navbar-default .navbar-nav > li > a {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
	<?php if($sdf_theme_styles['theme_navbar_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_navbar_letter_spacing']; ?>;<?php endif; ?>
	text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_link_text_decoration']) ? $sdf_theme_styles['theme_navbar_link_text_decoration'] : 'none'; ?>;
	<?php if($sdf_theme_styles['theme_navbar_link_box_shadow'] && $sdf_theme_styles['theme_navbar_link_box_shadow_color']): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_box_shadow_color']; ?>;
		box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_box_shadow_color']; ?>;
	<?php endif; ?>
}

.navbar-default .navbar-nav > li > a:hover,
.navbar-default .navbar-nav > li > a:focus {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_link_hover_bg_color'] : 'transparent'; ?>;
	text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_text_decoration']) ? $sdf_theme_styles['theme_navbar_link_hover_text_decoration'] : 'none'; ?>;
	<?php if($sdf_theme_styles['theme_navbar_link_hover_box_shadow'] && $sdf_theme_styles['theme_navbar_link_hover_box_shadow_color']): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_hover_box_shadow_color']; ?>;
		box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_hover_box_shadow_color']; ?>;
	<?php endif; ?>
}
.navbar-default .navbar-nav > li.current-menu-item > a,
.navbar-default .navbar-nav > li.current-menu-item > a:hover,
.navbar-default .navbar-nav > li.current-menu-item > a:focus,
.navbar-default .navbar-nav > li.current-menu-parent > a,
.navbar-default .navbar-nav > li.current-menu-parent > a:hover,
.navbar-default .navbar-nav > li.current-menu-parent > a:focus,
.navbar-default .navbar-nav > .active > a,
.navbar-default .navbar-nav > .active > a:hover,
.navbar-default .navbar-nav > .active > a:focus {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_active_color']; ?>;
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_active_bg_color']) ? $sdf_theme_styles['theme_navbar_link_active_bg_color'] : 'transparent'; ?>;
	<?php if($sdf_theme_styles['theme_navbar_link_active_box_shadow'] && $sdf_theme_styles['theme_navbar_link_active_box_shadow_color']): ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_active_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_active_box_shadow_color']; ?>;
		box-shadow: <?php echo $sdf_theme_styles['theme_navbar_link_active_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_link_active_box_shadow_color']; ?>;
	<?php endif; ?>
}

.navbar-default .navbar-toggle {
  border-color: <?php echo $sdf_theme_styles['theme_navbar_toggle_border_color']; ?>;
	margin: 15px;
	margin-top: <?php echo $logo_padding_top; ?>;
}

.navbar-default .navbar-toggle:hover,
.navbar-default .navbar-toggle:focus {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_toggle_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_toggle_hover_bg_color'] : 'transparent'; ?>;
}

.navbar-default .navbar-toggle .icon-bar {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_toggle_icon_bar_bg_color']) ? $sdf_theme_styles['theme_navbar_toggle_icon_bar_bg_color'] : 'transparent'; ?>;
}
.navbar-default .navbar-toggle:hover .icon-bar,
.navbar-default .navbar-toggle:focus .icon-bar {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_toggle_icon_bar_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_toggle_icon_bar_hover_bg_color'] : 'transparent'; ?>;
}
.navbar-default .navbar-collapse,
.navbar-default .navbar-form {
  border-color: #e6e6e6;
}

.navbar-default .navbar-nav > .dropdown > a:hover .caret,
.navbar-default .navbar-nav > .dropdown > a:focus .caret {
  border-top-color: #333333;
  border-bottom-color: #333333;
}

.navbar-default .navbar-nav > .open > a,
.navbar-default .navbar-nav > .open > a:hover,
.navbar-default .navbar-nav > .open > a:focus {
  color: #555555;
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_link_hover_bg_color'] : 'transparent'; ?>;
}

.navbar-default .navbar-nav > .open > a .caret,
.navbar-default .navbar-nav > .open > a:hover .caret,
.navbar-default .navbar-nav > .open > a:focus .caret {
  border-top-color: #555555;
  border-bottom-color: #555555;
}

.navbar-default .navbar-nav > .dropdown > a .caret {
  border-top-color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
  border-bottom-color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
}

@media (max-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint_max']; ?>) {
  .navbar-default {
    background-color: transparent;
    border: 0;
	}
	nav#wpu-navigation .sub-menu a {
		color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
	}
	nav#wpu-navigation .sub-menu a:hover,
	nav#wpu-navigation .sub-menu a:focus,
	nav#wpu-navigation .sub-menu .current-menu-item a {
		color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
	}
}

.navbar-default .navbar-link {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_color']; ?>;
}

.navbar-default .navbar-link:hover {
  color: <?php echo $sdf_theme_styles['theme_navbar_link_hover_color']; ?>;
}

/* Default Theme Submenus */
<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
nav#wpu-navigation .sub-menu {
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
}
nav#wpu-navigation .sub-menu li:first-child {
	-webkit-border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px 0 0;
	border-radius: <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px 0 0;
}
nav#wpu-navigation .sub-menu li:last-child {
	-webkit-border-radius: 0 0 <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
	border-radius: 0 0 <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px <?php echo $sdf_theme_styles['theme_navbar_border_radius']; ?>px;
}
nav#wpu-navigation .sub-menu a {
  -webkit-border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - 3); ?>px;
  border-radius: <?php echo ($sdf_theme_styles['theme_navbar_border_radius'] - 3); ?>px;
}  
<?php endif; ?>

@media (min-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint']; ?>) {
<?php if($sdf_theme_styles['theme_navbar_top_bg_color'] != 'transparent' || $sdf_theme_styles['theme_navbar_bottom_bg_color'] != 'transparent' || $sdf_theme_styles['theme_navbar_bg_color'] != ''): ?>
header > #navigation-area,
header > #navigation-area-2 {
	background-color: <?php echo $sdf_theme_styles['theme_navbar_bg_color']; ?>;
	background-image: -webkit-gradient(linear, left 0%, left 100%, from(<?php echo $sdf_theme_styles['theme_navbar_top_bg_color']; ?>), to(<?php echo $sdf_theme_styles['theme_navbar_bottom_bg_color']; ?>));
	background-image: -webkit-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_top_bg_color']; ?>, 0%, <?php echo $sdf_theme_styles['theme_navbar_bottom_bg_color']; ?>, 100%);
	background-image: -moz-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_bottom_bg_color']; ?> 100%);
	background-image: linear-gradient(to bottom, <?php echo $sdf_theme_styles['theme_navbar_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_bottom_bg_color']; ?> 100%);
	background-repeat: repeat-x;
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_hover_bg_color'] != ''): ?>
#navigation-area:hover,
header > #navigation-area:hover,
#navigation-area-2:hover,
header > #navigation-area-2:hover {
	background-color: <?php echo $sdf_theme_styles['theme_navbar_hover_bg_color']; ?>;
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_sticky_bg_color'] != ''): ?>
#navigation-area.navbar-sticky.sticky-show,
header > #navigation-area.navbar-sticky.sticky-show,
#navigation-area-2.navbar-sticky.sticky-show,
header > #navigation-area-2.navbar-sticky.sticky-show {
	background-color: <?php echo $sdf_theme_styles['theme_navbar_sticky_bg_color']; ?>;
}
<?php endif; ?>

header > #navigation-area.wide .collapse.navbar-collapse,
header > #navigation-area-2.wide .collapse.navbar-collapse {
	background-image: none;
	background-color: transparent;
}
nav#wpu-navigation .sub-menu {
	-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
	box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
	<?php if($sdf_theme_styles['theme_navbar_drop_border_color']) : ?>border-color:<?php echo $sdf_theme_styles['theme_navbar_drop_border_color']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles['theme_navbar_drop_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_navbar_drop_border_style']; ?>;<?php endif; ?>
	border-width:<?php echo ($sdf_theme_styles['theme_navbar_drop_border_width'] != '') ? $sdf_theme_styles['theme_navbar_drop_border_width'] : '0'; ?>;
	-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
	-moz-border-radius: <?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
	border-radius: <?php echo ($sdf_theme_styles['theme_navbar_drop_border_radius']) ? $sdf_theme_styles['theme_navbar_drop_border_radius'] : '0'; ?>; 
}

.navbar .nav > li#menu-item-search > .sdf-dropdown-top,
.navbar .nav > li:hover > .sdf-dropdown-top {
  opacity: 1;
  visibility: visible;
}

<?php if($sdf_theme_styles['theme_navbar_link_divider_width'] > 0): ?>
	<?php if($sdf_theme_styles['theme_menu_alignment'] == 'right'): ?>
	.navbar-default .navbar-nav > li:first-child {
		border-left: <?php echo $sdf_theme_styles['theme_navbar_link_divider_width']; ?>px solid <?php echo $sdf_theme_styles['theme_navbar_link_divider_color']; ?>;
	}
	<?php if($sdf_theme_styles['theme_page_width'] == 'wide'): ?>
	#navigation-area,
	#navigation-area-2 {
		<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
		border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px;
		<?php else: ?> 
		border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px 0;
		<?php endif; ?> 
		<?php if($sdf_theme_styles['theme_navbar_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_navbar_border_style']; ?>;<?php endif; ?>
	}	
	.navbar-default .navbar-nav > li:last-child {
		border-right: <?php echo $sdf_theme_styles['theme_navbar_link_divider_width']; ?>px solid <?php echo $sdf_theme_styles['theme_navbar_link_divider_color']; ?>;
	}
	<?php endif; ?>
	<?php endif; ?>
	.navbar-default .navbar-nav > li + li {
		border-left: <?php echo $sdf_theme_styles['theme_navbar_link_divider_width']; ?>px solid <?php echo $sdf_theme_styles['theme_navbar_link_divider_color']; ?>;
	}
	<?php if($sdf_theme_styles['theme_menu_alignment'] == 'left'): ?>
	.navbar-default .navbar-nav > li:last-child {
		border-right: <?php echo $sdf_theme_styles['theme_navbar_link_divider_width']; ?>px solid <?php echo $sdf_theme_styles['theme_navbar_link_divider_color']; ?>;
	}
	<?php if($sdf_theme_styles['theme_page_width'] == 'wide'): ?>
	#navigation-area,
	#navigation-area-2 {
		<?php if($sdf_theme_styles['theme_navbar_border_radius'] > 0) : ?>
		border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px;
		<?php else: ?> 
		border-width: <?php echo $sdf_theme_styles['theme_navbar_border_width']; ?>px 0;
		<?php endif; ?> 
		<?php if($sdf_theme_styles['theme_navbar_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_navbar_border_style']; ?>;<?php endif; ?>
	}
	.navbar-default .navbar-nav > li:first-child {
		border-left: <?php echo $sdf_theme_styles['theme_navbar_link_divider_width']; ?>px solid <?php echo $sdf_theme_styles['theme_navbar_link_divider_color']; ?>;
	}
	<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
}
<?php if($sdf_theme_styles['theme_navbar_link_top_bg_color'] && $sdf_theme_styles['theme_navbar_link_bottom_bg_color']): ?>
.navbar-nav > li > a {
	background-color: <?php echo $sdf_theme_styles['theme_navbar_link_bg_color']; ?>;
	background-image: -webkit-gradient(linear, left 0%, left 100%, from(<?php echo $sdf_theme_styles['theme_navbar_link_top_bg_color']; ?>), to(<?php echo $sdf_theme_styles['theme_navbar_link_bottom_bg_color']; ?>));
	background-image: -webkit-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_top_bg_color']; ?>, 0%, <?php echo $sdf_theme_styles['theme_navbar_link_bottom_bg_color']; ?>, 100%);
	background-image: -moz-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_bottom_bg_color']; ?> 100%);
	background-image: linear-gradient(to bottom, <?php echo $sdf_theme_styles['theme_navbar_link_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_bottom_bg_color']; ?> 100%);
	background-repeat: repeat-x;
<?php if($sdf_theme_styles['theme_navbar_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
<?php endif; ?>
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_link_hover_top_bg_color'] && $sdf_theme_styles['theme_navbar_link_hover_bottom_bg_color']): ?>
.navbar-nav > li > a:hover,
.navbar-nav > li > a:focus {
	background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_hover_bg_color']) ? $sdf_theme_styles['theme_navbar_link_hover_bg_color'] : 'transparent'; ?>;
	background-image: -webkit-gradient(linear, left 0%, left 100%, from(<?php echo $sdf_theme_styles['theme_navbar_link_hover_top_bg_color']; ?>), to(<?php echo $sdf_theme_styles['theme_navbar_link_hover_bottom_bg_color']; ?>));
	background-image: -webkit-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_hover_top_bg_color']; ?>, 0%, <?php echo $sdf_theme_styles['theme_navbar_link_hover_bottom_bg_color']; ?>, 100%);
	background-image: -moz-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_hover_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_hover_bottom_bg_color']; ?> 100%);
	background-image: linear-gradient(to bottom, <?php echo $sdf_theme_styles['theme_navbar_link_hover_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_hover_bottom_bg_color']; ?> 100%);
	background-repeat: repeat-x;
}
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_link_active_top_bg_color'] && $sdf_theme_styles['theme_navbar_link_active_bottom_bg_color']): ?>
.navbar-default .navbar-nav > li.current-menu-item > a,
.navbar-default .navbar-nav > li.current-menu-item > a:hover,
.navbar-default .navbar-nav > li.current-menu-item > a:focus,
.navbar-nav > .active > a,
.navbar-nav > .active > a:hover,
.navbar-nav > .active > a:focus {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_link_active_bg_color']) ? $sdf_theme_styles['theme_navbar_link_active_bg_color'] : 'transparent'; ?>;
  background-image: -webkit-gradient(linear, left 0%, left 100%, from(<?php echo $sdf_theme_styles['theme_navbar_link_active_top_bg_color']; ?>), to(<?php echo $sdf_theme_styles['theme_navbar_link_active_bottom_bg_color']; ?>));
	background-image: -webkit-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_active_top_bg_color']; ?>, 0%, <?php echo $sdf_theme_styles['theme_navbar_link_active_bottom_bg_color']; ?>, 100%);
	background-image: -moz-linear-gradient(top, <?php echo $sdf_theme_styles['theme_navbar_link_active_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_active_bottom_bg_color']; ?> 100%);
	background-image: linear-gradient(to bottom, <?php echo $sdf_theme_styles['theme_navbar_link_active_top_bg_color']; ?> 0%, <?php echo $sdf_theme_styles['theme_navbar_link_active_bottom_bg_color']; ?> 100%);
	background-repeat: repeat-x;
}
<?php endif; ?>
/* Default Theme END */

/* Bootstrap Submenu */
nav#wpu-navigation .sub-menu,
nav#wpu-navigation .sub-menu a {
  display: block;
<?php if($sdf_theme_styles['theme_navbar_drop_size'] || $sdf_theme_styles['theme_navbar_drop_family'] || $sdf_theme_styles['theme_navbar_drop_weight']): ?>
	<?php if($sdf_theme_styles['theme_navbar_drop_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_navbar_drop_size']; ?><?php echo $sdf_theme_styles['theme_navbar_drop_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_navbar_drop_family'], $sdf_theme_styles['theme_navbar_drop_weight']); ?>
<?php endif;
if($sdf_theme_styles['theme_navbar_drop_transform'] != 'no'): ?>
	text-transform: <?php echo $sdf_theme_styles['theme_navbar_drop_transform']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_navbar_drop_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_navbar_drop_letter_spacing']; ?>;<?php endif; ?>
}
nav#wpu-navigation .sub-menu a {
  display: block;
	white-space: nowrap;
  text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_drop_text_decoration']) ? $sdf_theme_styles['theme_navbar_drop_text_decoration'] : 'none'; ?>;
	padding: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_padding']) ? $sdf_theme_styles['theme_navbar_drop_link_padding'] : '0 15px'; ?>;
  height: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_height']) ? $sdf_theme_styles['theme_navbar_drop_link_height'] : '32px'; ?>;
  line-height: <?php echo ($sdf_theme_styles['theme_navbar_drop_link_height']) ? $sdf_theme_styles['theme_navbar_drop_link_height'] : '32px'; ?>;
	<?php if($sdf_theme_styles['theme_navbar_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-ms-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_navbar_transition']; ?>;
<?php endif; ?>
}


.navbar .nav ul {
	left: 0;
	list-style-type: none;
	margin: 0;
	opacity: 0;
	position: absolute;
	top: 0;
	min-width: 170px;
	-webkit-transform: scale(1, 0.99);
	-webkit-transform-origin: 0 0;
	-ms-transform: scale(1, 0.99);
	-ms-transform-origin: 0 0;
	-o-transform: scale(1, 0.99);
	-o-transform-origin: 0 0;
	transform: scale(1, 0.99);
	transform-origin: 0 0;
	visibility: hidden;
	-webkit-backface-visibility: hidden;
		<?php if($sdf_theme_styles['theme_navbar_drop_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
-ms-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_navbar_drop_transition']; ?>;
<?php endif; ?>
}

/* Search Icon/Form */
.navbar .nav ul.searchform {
	opacity: 1;
  top: 100%;
  visibility: visible;
  z-index: 999;
  -webkit-transform: scale(1, 1);
  -ms-transform: scale(1, 1);
  -o-transform: scale(1, 1);
  transform: scale(1, 1);
  display: block\9;
}
.navbar .nav > li:hover > ul:not(.searchform) {
  opacity: 1;
  top: 100%;
  visibility: visible;
  z-index: 999;
  -webkit-transform: scale(1, 1);
  -ms-transform: scale(1, 1);
  -o-transform: scale(1, 1);
  transform: scale(1, 1);
  display: block\9;
}
.navbar .nav > li > ul {
  margin-top: 0px;
  top: 80%;
  padding-left: 0;
}
.navbar .nav > li > .sdf-dropdown-top {
    bottom: -2px;
    height: 2px;
    opacity: 0;
    position: absolute;
    visibility: hidden;
    width: 100%;
    z-index: 1000;
}
.navbar .nav > li > .sdf-dropdown-top .sdf-dropdown-arrow-wrap {
    display: none;
    width: 10px;
    height: 10px;
    top: -10px;
    left: 15px;
    margin-left: -5px;
    overflow: hidden;
    position: absolute;
}
.navbar .nav > li#menu-item-search > .sdf-dropdown-top .sdf-dropdown-arrow-wrap,
.navbar .nav > li:hover > .sdf-dropdown-top .sdf-dropdown-arrow-wrap {
		display: block;
}
.navbar .nav > li > .sdf-dropdown-top .sdf-dropdown-arrow {
    border: 0;
    width: 10px;
    height: 10px;
    top: 7px;
    left: 50%;
    margin-left: -5px;
    position: absolute;
		-webkit-transform: rotate(45deg);
		-ms-transform: rotate(45deg);
		-o-transform: rotate(45deg);
		transform: rotate(45deg);
		visibility: hidden\9;
		background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_color']) ? $sdf_theme_styles['theme_navbar_drop_color'] : 'transparent'; ?>;
		-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
		box-shadow: <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_navbar_drop_box_shadow_color']; ?>;
		-webkit-transition: 0.3s ease-out;
		-moz-transition: 0.3s ease-out;
		-o-transition: 0.3s ease-out;
		transition: 0.3s ease-out;
		-webkit-backface-visibility: hidden;
}
.navbar .nav > li > ul li:hover ul {
  opacity: 1;
  -webkit-transform: scale(1, 1);
  -ms-transform: scale(1, 1);
  -o-transform: scale(1, 1);
  transform: scale(1, 1);
  visibility: visible;
  display: block\9;
}
.navbar .nav > li > ul li ul,
.navbar .nav > li > ul li ul li ul {
	left: 100%;
	padding-left: 0;
}
.navbar .nav ul li {
	padding: 0;
	position: relative;
}
.navbar .nav .sub-menu.searchform > li {
	padding: 5px;
	line-height: 0;
}
.navbar .nav ul li.active > a,
.navbar .nav ul li.active > a:hover,
.navbar .nav ul li.active > a:focus {
	background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_hover_color']) ? $sdf_theme_styles['theme_navbar_drop_hover_color'] : 'transparent'; ?>;
	padding-left: 9px;
	padding-right: 9px;
}
.navbar .nav ul li.active + li > a {
  padding-left: 9px;
  padding-right: 9px;
}
@media (min-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint']; ?>) {
nav#wpu-navigation .sub-menu {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_color']) ? $sdf_theme_styles['theme_navbar_drop_color'] : 'transparent'; ?>;
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
}
nav#wpu-navigation .sub-menu a {
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
}
nav#wpu-navigation .sub-menu a:hover,
nav#wpu-navigation .sub-menu .current-menu-item a {
  background-color: <?php echo ($sdf_theme_styles['theme_navbar_drop_hover_color']) ? $sdf_theme_styles['theme_navbar_drop_hover_color'] : 'transparent'; ?>;
  color: <?php echo $sdf_theme_styles['theme_navbar_drop_ahover']; ?>;
  text-decoration: <?php echo ($sdf_theme_styles['theme_navbar_drop_hover_text_decoration']) ? $sdf_theme_styles['theme_navbar_drop_hover_text_decoration'] : 'none'; ?>;
}
.navbar .nav > li > ul li a {
	background-color: transparent;
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
}
.navbar .nav > li ul li a {
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_acolor']; ?>;
}
.navbar .nav > li ul li a:hover,
.navbar .nav > li ul li a:focus {
	color: <?php echo $sdf_theme_styles['theme_navbar_drop_ahover']; ?>;
}
.logo-sm {
	display: none;
}
}


@media (max-width: <?php echo $sdf_theme_styles['theme_tbs_grid_float_breakpoint_max']; ?>) {
.navbar .nav > li {
    height: auto;
}
  .navbar a,
  .navbar .navbar-collapse .nav > li > a {
    -webkit-border-radius: 0;
    border-radius: 0;
  }
  .navbar.navbar-default .navbar-collapse {
    width: auto;
  }
  .navbar .btn-navbar {
    float: right;
    display: block;
    margin-right: 0;
    padding-bottom: 0;
  }
  .navbar .nav > li > ul {
    height: auto;
    margin: 0 0 0 20px;
	opacity: 1;
	top: 100%;
	visibility: visible;
	z-index: 100;
	-webkit-transform: scale(1, 1);
	-ms-transform: scale(1, 1);
	-o-transform: scale(1, 1);
	transform: scale(1, 1);
	display: block\9;
  }
  .navbar .nav > li > ul li ul {
    height: auto;
	 opacity: 1;
  -webkit-transform: scale(1, 1);
  -ms-transform: scale(1, 1);
  -o-transform: scale(1, 1);
  transform: scale(1, 1);
  visibility: visible;
  display: block\9;
  }
	.navbar .nav > li > ul li ul,
	.navbar .nav > li > ul li ul li ul {
		left: 0%;
	}
  .navbar .nav > li ul {
    background: none;
    height: auto;
    padding: 0;
    position: relative;
    width: auto;
  }
  .navbar .nav > li ul:before,
  .navbar .nav > li ul:after {
    display: none;
  }
  .navbar .nav > li ul li {
    background: none;
  }
  .navbar .nav a {
    display: block;
  }
  .navbar .navbar-collapse {
    width: 100%;
    *zoom: 1;
  }
  .navbar .navbar-collapse:before,
  .navbar .navbar-collapse:after {
    display: table;
    content: "";
  }
  .navbar .navbar-collapse:after {
    clear: both;
  }
	.logo-lg {
    display: none;
  }
}

<?php 
$id = 'default-widget-area';
$id_var = 'default_widget_area';
?>
	/* Default Widget Area */
	<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>
	
		<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
		.<?php echo $id; ?> {
			-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
				box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
		}
		<?php endif; ?>
		
		<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
		
			.<?php echo $id; ?> {
			background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
			background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
			background-attachment: fixed;
			background-position: 50% 0;
			background-repeat: no-repeat;
			position: relative;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		<?php else:  ?>
		
			<?php if($sdf_theme_styles[$id_var.'_bg_color'] || $sdf_theme_styles[$id_var.'_bg_image']): ?>
			.<?php echo $id; ?> {
				<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
			}
			<?php endif; ?>
		
		
		<?php endif; ?>
	
	<?php endif; ?>

	#navigation-area .widget_area,
	#navigation-area-2 .widget_area {
		padding-top: 0px;
	}
<?php if($sdf_theme_styles[$id_var.'_wg_custom_typo'] == 'yes'): ?>	
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_content_align'])): ?>
	.<?php echo $id; ?> .widget {
	text-align:<?php echo $sdf_theme_styles[$id_var.'_wg_content_align']; ?>;
	}
	<?php endif; ?>
	.<?php echo $id; ?> .widgettitle {
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_title_align'])): ?>text-align:<?php echo $sdf_theme_styles[$id_var.'_wg_title_align']; ?>;<?php endif; ?>
	margin-top:<?php echo ($sdf_theme_styles[$id_var.'_wg_title_margin_top']) ? $sdf_theme_styles[$id_var.'_wg_title_margin_top'] : '0px'; ?>;
	margin-bottom:<?php echo ($sdf_theme_styles[$id_var.'_wg_title_margin_bottom']) ? $sdf_theme_styles[$id_var.'_wg_title_margin_bottom'] : '0px'; ?>;}
	
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_title_weight'])): ?>
	.<?php echo $id; ?> .widgettitle {
		font-weight:<?php echo $sdf_theme_styles[$id_var.'_wg_title_weight']; ?>;
	}
	<?php endif; 

	if($sdf_theme_styles[$id_var.'_wg_title_transform'] != 'no'): ?>
	.<?php echo $id; ?> .widgettitle {
		text-transform: <?php echo $sdf_theme_styles[$id_var.'_wg_title_transform']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_title_box_shadow'] != '' && $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color'] != ''): ?>
	.widgettitle {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color']; ?>;
	}
	<?php endif; ?>
<?php endif; ?>
	
<?php if($sdf_theme_styles[$id_var.'_wg_custom_style'] == 'yes'): ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_box_shadow'] != '' && $sdf_theme_styles[$id_var.'_wg_box_shadow_color'] != ''): ?>
	.<?php echo $id; ?> .widget {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	.<?php echo $id; ?> .widget {
		padding: 15px;
		<?php if($sdf_theme_styles[$id_var.'_wg_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_wg_border_style']; ?>;<?php endif; ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_border_color']; ?>;<?php endif; ?>
		border-width:<?php echo ($sdf_theme_styles[$id_var.'_wg_border_width'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_width'] : '0px'; ?>;
		-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		<?php if($sdf_theme_styles[$id_var.'_wg_background_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_background_color']; ?>;<?php endif; ?>
	}
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_wg_custom_typo'] == 'yes'): ?>

	<?php if($sdf_theme_styles[$id_var.'_wg_font_size'] || $sdf_theme_styles[$id_var.'_wg_family'] || $sdf_theme_styles[$id_var.'_wg_font_color']): ?>
	.<?php echo $id; ?> .widget{
		<?php if($sdf_theme_styles[$id_var.'_wg_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_font_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_font_color']; ?>;<?php } ?>
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_link_color'] || $sdf_theme_styles[$id_var.'_wg_family']): ?>
	.<?php echo $id; ?> .widget a:not(.btn):not(.latest-post-title) {
		<?php if($sdf_theme_styles[$id_var.'_wg_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_font_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_link_color']) ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_link_color']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_link_hover_color']): ?>
	.<?php echo $id; ?> .widget a:not(.btn):not(.latest-post-title):hover {color:<?php echo $sdf_theme_styles[$id_var.'_wg_link_hover_color']; ?>;}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_title_size'] || $sdf_theme_styles[$id_var.'_wg_title_family'] || $sdf_theme_styles[$id_var.'_wg_title_color']): ?>
	.<?php echo $id; ?> .widgettitle {
		<?php if($sdf_theme_styles[$id_var.'_wg_title_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_title_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_title_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_title_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_title_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_title_color']; ?>;<?php } ?>
		}
	<?php endif; ?>
<?php endif; ?>	
	.widget_area.module {
		padding-top: 0;
	}
<?php if($sdf_theme_styles[$id_var.'_wg_custom_list_style'] == 'yes'): ?>
/* Default Widget Area List Styles */
.<?php echo $id; ?> .widget ul:not([class]),
.<?php echo $id; ?> .widget ol {
	<?php $wg_list_margin = ($sdf_theme_styles[$id_var.'_wg_list_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_margin'] : '0'; ?>
	margin: <?php echo $wg_list_margin; ?>;
	<?php $wg_list_padding = ($sdf_theme_styles[$id_var.'_wg_list_padding'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_padding'] : '0'; ?>
	padding: <?php echo $wg_list_padding; ?>;
}
.<?php echo $id; ?> .widget ul:not(.fa-ul) {
<?php if(in_array($sdf_theme_styles[$id_var.'_wg_list_style'], array('square', 'circle', 'disc'))): ?>
	list-style-type: <?php echo $sdf_theme_styles[$id_var.'_wg_list_style']; ?>;
<?php elseif(in_array($sdf_theme_styles[$id_var.'_wg_list_style'], array('none', 'icon'))): ?>
  list-style: none;
<?php endif; ?>
}
.<?php echo $id; ?> .latest-posts-widget .widget ul {
<?php if(in_array($sdf_theme_styles[$id_var.'_wg_list_style'], array('square', 'circle', 'disc'))): ?>
	list-style: none;
<?php endif; ?>
}
.<?php echo $id; ?> .widget_nav_menu li, .<?php echo $id; ?> .widget_categories li, .<?php echo $id; ?> .widget_meta li, .<?php echo $id; ?> .widget .recentcomments, .<?php echo $id; ?> .widget_recent_entries li, .<?php echo $id; ?> .widget_archive li, .<?php echo $id; ?> .widget_pages li, .<?php echo $id; ?> .widget_links li { 
	<?php if($sdf_theme_styles[$id_var.'_wg_list_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_list_family'], ''); ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_color']; ?>;<?php } ?>
	<?php $wg_list_item_margin = ($sdf_theme_styles[$id_var.'_wg_list_item_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_margin'] : '0'; ?>
	margin: <?php echo $wg_list_item_margin; ?>;
	<?php $wg_list_item_padding = ($sdf_theme_styles[$id_var.'_wg_list_item_padding'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_padding'] : '0'; ?>
	padding: <?php echo $wg_list_item_padding; ?>;
	<?php $wg_list_align = ($sdf_theme_styles[$id_var.'_wg_list_align'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_align'] : 'left'; ?>
	text-align: <?php echo $wg_list_align; ?>;
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_line_height']): ?>
	line-height: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_line_height']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_style']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_color']; ?>;<?php endif; ?>
	border-width:<?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_width'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_width'] : '0'; ?>;
	-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0'; ?>; 
	-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0'; ?>; 
	border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0'; ?>; 
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_background_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_background_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow_color']; ?>;
}
.<?php echo $id; ?> .widget_nav_menu li:hover, .<?php echo $id; ?> .widget_categories li:hover, .<?php echo $id; ?> .widget_meta li:hover, .<?php echo $id; ?> .widget .recentcomments:hover, .<?php echo $id; ?> .widget_recent_entries li:hover, .<?php echo $id; ?> .widget_archive li:hover, .<?php echo $id; ?> .widget_pages li:hover, .<?php echo $id; ?> .widget_links li:hover {
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_hover_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_hover_color']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_background_hover_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_background_hover_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow_color']; ?>;
}
.<?php echo $id; ?> .widget_nav_menu li:before, .<?php echo $id; ?> .widget_categories li:before, .<?php echo $id; ?> .widget_meta li:before, .<?php echo $id; ?> .widget .recentcomments:before, .<?php echo $id; ?> .widget_recent_entries li:before, .<?php echo $id; ?> .widget_archive li:before, .<?php echo $id; ?> .widget_pages li:before, .<?php echo $id; ?> .widget_links li:before {
	<?php if($sdf_theme_styles[$id_var.'_wg_list_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_color']; ?>;<?php } ?>
	<?php $widget_list_icon = ($sdf_theme_styles[$id_var.'_wg_list_style'] == 'icon') ? '\\'.$sdf_theme_styles[$id_var.'_wg_list_icon'] : ''; ?>
	content: "<?php echo $widget_list_icon; ?>";
	font-family: FontAwesome;
	<?php if($sdf_theme_styles[$id_var.'_wg_list_icon_size']): ?>
	font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_icon_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_icon_size_type']; ?>;
	<?php endif; ?>
	<?php $wg_list_icon_margin = ($sdf_theme_styles[$id_var.'_wg_list_icon_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_icon_margin'] : '0'; ?>
	margin: <?php echo $wg_list_icon_margin; ?>;
	position: absolute;
}
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_color'] || $sdf_theme_styles[$id_var.'_wg_list_family'] || $sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']): ?>
.<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title) {
<?php if($sdf_theme_styles[$id_var.'_wg_list_font_size']): ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size_type']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']; ?>;<?php endif; ?>
<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_list_family'], ''); ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_color']) ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_color']; ?>;
}
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_hover_color'] || $sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']): ?>
.<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title):hover, .<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title):focus {
color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_hover_color']; ?>;
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php endif; ?>
	
<?php foreach (sdfGo()->_wpuGlobal->_sdfSidebars as $id => $name) :
if ( !in_array( $id, array( 'default-widget-area', 'slider-widget-area' ) ) ) :
		$id_var = str_replace("-", "_", urlencode(strtolower($id)));
?>
	/* <?php echo $name; ?> */

	<?php if(in_array($sdf_theme_styles[$id_var.'_custom_bg'], array( 'yes', 'parallax'))): ?>
	
	<?php if($sdf_theme_styles[$id_var.'_box_shadow'] && $sdf_theme_styles[$id_var.'_box_shadow_color']): ?>
	#<?php echo $id; ?> {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_bg_color'] || $sdf_theme_styles[$id_var.'_bg_image']): ?>
		#<?php echo $id; ?> {
			<?php get_background_color_image($sdf_theme_styles[$id_var.'_bg_color'], $sdf_theme_styles[$id_var.'_bg_image'], $sdf_theme_styles[$id_var.'_bgrepeat']); ?>
		}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_custom_bg'] == 'parallax' && $sdf_theme_styles[$id_var.'_bg_image']): ?>
	#<?php echo $id; ?> {
	background-image:<?php if($sdf_theme_styles[$id_var.'_bg_image']): echo ' url('.$sdf_theme_styles[$id_var.'_bg_image'].')'; endif; ?>;
	background-color:<?php if($sdf_theme_styles[$id_var.'_bg_color']): echo $sdf_theme_styles[$id_var.'_bg_color']; endif; ?>;
	background-attachment: fixed;
    background-position: 50% 0;
    background-repeat: no-repeat;
    position: relative;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	}
	<?php endif; ?>
	<?php endif; ?>
		
	<?php if ( !in_array($id, array('default-widget-area', 'left-widget-area', 'right-widget-area')) ): ?>
	.<?php echo $id; ?>.widget_area {
		<?php if($sdf_theme_styles[$id_var.'_height']){ ?>height:<?php echo $sdf_theme_styles[$id_var.'_height']; ?>;<?php } ?>
		<?php if($sdf_theme_styles[$id_var.'_padding_top']){ ?>padding-top:<?php echo $sdf_theme_styles[$id_var.'_padding_top']; ?>;<?php } ?>
		<?php if($sdf_theme_styles[$id_var.'_padding_bottom']){ ?>padding-bottom:<?php echo $sdf_theme_styles[$id_var.'_padding_bottom']; ?>;<?php } ?>
	}
	<?php endif; ?>
	
	<?php if( in_array($id, array('left-widget-area', 'right-widget-area')) ): ?>
	#<?php echo $id; ?> .widget_area {
		padding-top: 0;
		padding-bottom: 30px;
	}
	<?php endif; ?>
	
<?php if($sdf_theme_styles[$id_var.'_wg_custom_typo'] == 'yes'): ?>
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_content_align'])): ?>
	#<?php echo $id; ?> .widget {
	text-align:<?php echo $sdf_theme_styles[$id_var.'_wg_content_align']; ?>;}
	<?php endif; ?>
	
	#<?php echo $id; ?> .widgettitle {
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_title_align'])): ?>text-align:<?php echo $sdf_theme_styles[$id_var.'_wg_title_align']; ?>;<?php endif; ?>
	margin-top:<?php echo ($sdf_theme_styles[$id_var.'_wg_title_margin_top']) ? $sdf_theme_styles[$id_var.'_wg_title_margin_top'] : '0px'; ?>;
	margin-bottom:<?php echo ($sdf_theme_styles[$id_var.'_wg_title_margin_bottom']) ? $sdf_theme_styles[$id_var.'_wg_title_margin_bottom'] : '0px'; ?>;
	<?php if(isset($sdf_theme_styles[$id_var.'_wg_title_weight'])): ?>
		font-weight:<?php echo $sdf_theme_styles[$id_var.'_wg_title_weight']; ?>;
	<?php endif; ?> 
	<?php if($sdf_theme_styles[$id_var.'_wg_title_size'] || $sdf_theme_styles[$id_var.'_wg_title_family'] || $sdf_theme_styles[$id_var.'_wg_title_color']): ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_title_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_title_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_title_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_title_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_title_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_title_color']; ?>;<?php } ?>
	<?php endif; ?>	
	}

	<?php if($sdf_theme_styles[$id_var.'_wg_title_transform'] != 'no'): ?>
	.<?php echo $id; ?> .widgettitle {
		text-transform: <?php echo $sdf_theme_styles[$id_var.'_wg_title_transform']; ?>;
	}
	<?php endif; ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_title_box_shadow'] != '' && $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color'] != ''): ?>
	.<?php echo $id; ?> .widgettitle {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_title_box_shadow_color']; ?>;
	}
	<?php endif; ?>
<?php endif; ?>
	
<?php if($sdf_theme_styles[$id_var.'_wg_custom_style'] == 'yes'): ?>
	
	<?php if($sdf_theme_styles[$id_var.'_wg_box_shadow'] != '' && $sdf_theme_styles[$id_var.'_wg_box_shadow_color'] != ''): ?>
	.<?php echo $id; ?> .widget {
		-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_box_shadow_color']; ?>;
	}
	<?php endif; ?>
	
	.<?php echo $id; ?> .widget {
		padding: 15px;
		<?php if($sdf_theme_styles[$id_var.'_wg_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_wg_border_style']; ?>;<?php endif; ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_border_color']; ?>;<?php endif; ?>
		border-width:<?php echo ($sdf_theme_styles[$id_var.'_wg_border_width'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_width'] : '0px'; ?>;
		-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_border_radius'] : '0px'; ?>; 
		<?php if($sdf_theme_styles[$id_var.'_wg_background_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_background_color']; ?>;<?php endif; ?>
	}
<?php endif; ?>

	<?php if($sdf_theme_styles[$id_var.'_wg_font_size'] || $sdf_theme_styles[$id_var.'_wg_family'] || $sdf_theme_styles[$id_var.'_wg_font_color']): ?>
	
	.<?php echo $id; ?> .widget {
		<?php if($sdf_theme_styles[$id_var.'_wg_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_font_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_font_color']; ?>;<?php } ?>
	}
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_link_color'] || $sdf_theme_styles[$id_var.'_wg_family']): ?>
	.<?php echo $id; ?> .widget a:not(.btn):not(.latest-post-title) {
		<?php if($sdf_theme_styles[$id_var.'_wg_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_font_size_type']; ?>;<?php } ?>
		<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_family'], ''); ?>
		<?php if($sdf_theme_styles[$id_var.'_wg_link_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_link_color']; ?>;<?php } ?>
	}
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_link_hover_color']): ?>
	.<?php echo $id; ?> .widget a:not(.btn):not(.latest-post-title):hover {color:<?php echo $sdf_theme_styles[$id_var.'_wg_link_hover_color']; ?>;}
	<?php endif; ?>

	
<?php if($sdf_theme_styles[$id_var.'_wg_custom_list_style'] == 'yes'): ?>
/* <?php echo $name; ?> List Styles */

#<?php echo $id; ?> .widget ul:not([class]),
#<?php echo $id; ?> .widget ol {
	<?php $wg_list_margin = ($sdf_theme_styles[$id_var.'_wg_list_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_margin'] : '0'; ?>
	margin: <?php echo $wg_list_margin; ?>;
	<?php $wg_list_padding = ($sdf_theme_styles[$id_var.'_wg_list_padding'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_padding'] : '0'; ?>
	padding: <?php echo $wg_list_padding; ?>;
}
#<?php echo $id; ?> .widget ul:not(.fa-ul) {
<?php if(in_array($sdf_theme_styles[$id_var.'_wg_list_style'], array('square', 'circle', 'disc'))): ?>
	list-style-type: <?php echo $sdf_theme_styles[$id_var.'_wg_list_style']; ?>;
<?php elseif(in_array($sdf_theme_styles[$id_var.'_wg_list_style'], array('none', 'icon'))): ?>
  list-style: none;
<?php endif; ?>
}
#<?php echo $id; ?> .widget_nav_menu li, #<?php echo $id; ?> .widget_categories li, #<?php echo $id; ?> .widget_meta li, #<?php echo $id; ?> .widget .recentcomments, #<?php echo $id; ?> .widget_recent_entries li, #<?php echo $id; ?> .widget_archive li, #<?php echo $id; ?> .widget_pages li, #<?php echo $id; ?> .widget_links li { 
	<?php if($sdf_theme_styles[$id_var.'_wg_list_font_size']){ ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size_type']; ?>;<?php } ?>
	<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_list_family'], ''); ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_font_color']){ ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_color']; ?>;<?php } ?>
	<?php $wg_list_item_margin = ($sdf_theme_styles[$id_var.'_wg_list_item_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_margin'] : '0'; ?>
	margin: <?php echo $wg_list_item_margin; ?>;
	<?php $wg_list_item_padding = ($sdf_theme_styles[$id_var.'_wg_list_item_padding'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_padding'] : '0'; ?>
	padding: <?php echo $wg_list_item_padding; ?>;
	<?php $wg_list_align = ($sdf_theme_styles[$id_var.'_wg_list_align'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_align'] : 'left'; ?>
	text-align: <?php echo $wg_list_align; ?>;
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_line_height']): ?>
	line-height: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_line_height']; ?>;
	<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_style']) : ?>border-style:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_style']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_color']; ?>;<?php endif; ?>
	border-width:<?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_width'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_width'] : '0px'; ?>;
	-webkit-border-radius:<?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0px'; ?>; 
	-moz-border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0px'; ?>; 
	border-radius: <?php echo ($sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_item_border_radius'] : '0px'; ?>; 
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_background_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_background_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_box_shadow_color']; ?>;
}
#<?php echo $id; ?> .widget_nav_menu li:hover, #<?php echo $id; ?> .widget_categories li:hover, #<?php echo $id; ?> .widget_meta li:hover, #<?php echo $id; ?> .widget .recentcomments:hover, #<?php echo $id; ?> .widget_recent_entries li:hover, #<?php echo $id; ?> .widget_archive li:hover, #<?php echo $id; ?> .widget_pages li:hover, #<?php echo $id; ?> .widget_links li:hover {
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_border_hover_color']) : ?>border-color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_border_hover_color']; ?>;<?php endif; ?>
	<?php if($sdf_theme_styles[$id_var.'_wg_list_item_background_hover_color']) : ?>background:<?php echo $sdf_theme_styles[$id_var.'_wg_list_item_background_hover_color']; ?>;<?php endif; ?>
	-webkit-box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow_color']; ?>;
			box-shadow: <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow']; ?> <?php echo $sdf_theme_styles[$id_var.'_wg_list_item_hover_box_shadow_color']; ?>;
}
#<?php echo $id; ?> .widget_nav_menu li:before, #<?php echo $id; ?> .widget_categories li:before, #<?php echo $id; ?> .widget_meta li:before, #<?php echo $id; ?> .widget .recentcomments:before, #<?php echo $id; ?> .widget_recent_entries li:before, #<?php echo $id; ?> .widget_archive li:before, #<?php echo $id; ?> .widget_pages li:before, #<?php echo $id; ?> .widget_links li:before {
	<?php $widget_list_icon = ($sdf_theme_styles[$id_var.'_wg_list_style'] == 'icon') ? '\\'.$sdf_theme_styles[$id_var.'_wg_list_icon'] : ''; ?>
	content: "<?php echo $widget_list_icon; ?>";
	font-family: FontAwesome;
	<?php if($sdf_theme_styles[$id_var.'_wg_list_icon_size']): ?>
	font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_icon_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_icon_size_type']; ?>;
	<?php endif; ?>
	<?php $wg_list_icon_margin = ($sdf_theme_styles[$id_var.'_wg_list_icon_margin'] != '') ? $sdf_theme_styles[$id_var.'_wg_list_icon_margin'] : '0'; ?>
	margin: <?php echo $wg_list_icon_margin; ?>;
	position: absolute;
}
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_color'] || $sdf_theme_styles[$id_var.'_wg_list_family'] || $sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']): ?>
#<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title) {
<?php if($sdf_theme_styles[$id_var.'_wg_list_font_size']) : ?>font-size:<?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size']; ?><?php echo $sdf_theme_styles[$id_var.'_wg_list_font_size_type']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_text_decoration']; ?>;<?php endif; ?>
<?php get_font_family_weight($sdf_theme_styles[$id_var.'_wg_list_family'], ''); ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_color']) ?>color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_color']; ?>;
}
<?php endif; ?>
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_hover_color'] || $sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']): ?>
#<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title):hover, .<?php echo $id; ?> .widget li a:not(.btn):not(.latest-post-title):focus {
color:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_hover_color']; ?>;
<?php if($sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']): ?>text-decoration:<?php echo $sdf_theme_styles[$id_var.'_wg_list_link_hover_text_decoration']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php endif; ?>
	
<?php endif; ?>
<?php endforeach; ?>

/* Footer */
#sdf-extras-footer {
  height: 0;
}
iframe[name='google_conversion_frame'] { 
    height: 0 !important;
    width: 0 !important; 
    line-height: 0 !important; 
    font-size: 0 !important;
    margin-top: -13px;
    float: left;
}
.inline_menu.widget_nav_menu .menu {
  padding-left: 0;
  margin-left: -5px;
  list-style: none;
}
.inline_menu.widget_nav_menu .menu > li {
  display: inline-block;
  padding-right: 5px;
  padding-left: 5px;
}

<?php if($sdf_theme_styles['footer_custom_bg'] == 'yes' && ($sdf_theme_styles['footer_bg_color'] || $sdf_theme_styles['footer_bg_image'])): ?>
#sdf-footer {
	background:<?php if($sdf_theme_styles['footer_bg_color']): echo $sdf_theme_styles['footer_bg_color']; endif; if($sdf_theme_styles['footer_bg_image']):
	echo ' url('.$sdf_theme_styles['footer_bg_image'].') '.$sdf_theme_styles['footer_bgrepeat']; endif;
	?>;
}
<?php endif; ?>

<?php if($sdf_theme_styles['footer_font_family'] || $sdf_theme_styles['footer_font_size']): ?>
#sdf-footer {
	<?php get_font_family_weight($sdf_theme_styles['footer_font_family'], ''); ?>
	<?php if($sdf_theme_styles['footer_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['footer_font_size']; ?><?php echo $sdf_theme_styles['footer_font_size_type']; ?>;<?php } ?>
}
<?php endif; ?>

<?php 
if($sdf_theme_styles['footer_font_color']): ?>

#sdf-footer {

<?php if($sdf_theme_styles['footer_font_color']){ ?>color:<?php echo $sdf_theme_styles['footer_font_color']; ?>;<?php } ?>
}
<?php endif; ?>
<?php 
if($sdf_theme_styles['footer_font_hover_color']): ?>

#sdf-footer:hover {

<?php if($sdf_theme_styles['footer_font_hover_color']){ ?>color:<?php echo $sdf_theme_styles['footer_font_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>	
<?php if($sdf_theme_styles['footer_link_color']): ?>
#sdf-footer a:not(.btn) {
	<?php if($sdf_theme_styles['footer_link_color']){ ?>color:<?php echo $sdf_theme_styles['footer_link_color']; ?>;<?php } ?>
}
<?php endif; ?>
<?php 
if($sdf_theme_styles['footer_link_hover_color']): ?>

#sdf-footer a:hover {

<?php if($sdf_theme_styles['footer_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles['footer_link_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>

<?php if($sdf_theme_styles['footer_bottom_font_family'] || $sdf_theme_styles['footer_bottom_font_size']): ?>
#sdf-copyright {
	<?php get_font_family_weight($sdf_theme_styles['footer_bottom_font_family'], ''); ?>
	<?php if($sdf_theme_styles['footer_bottom_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['footer_bottom_font_size']; ?><?php echo $sdf_theme_styles['footer_bottom_font_size_type']; ?>;<?php } ?>
}
<?php endif; ?>
<?php if($sdf_theme_styles['footer_bottom_custom_bg'] == 'yes'): ?>
<?php if($sdf_theme_styles['footer_bottom_bg_color'] || $sdf_theme_styles['footer_bottom_bg_image']): ?>
#sdf-copyright {

	background:<?php if($sdf_theme_styles['footer_bottom_bg_color']): echo $sdf_theme_styles['footer_bottom_bg_color']; endif; if($sdf_theme_styles['footer_bottom_bg_image']):
	echo ' url('.$sdf_theme_styles['footer_bottom_bg_image'].') '.$sdf_theme_styles['footer_bottom_bgrepeat']; endif;
	?>;

}
<?php endif; ?>
<?php endif; ?>
<?php if($sdf_theme_styles['footer_bottom_font_color']): ?>
#sdf-copyright {

<?php if($sdf_theme_styles['footer_bottom_font_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_font_color']; ?>;<?php } ?>
}
<?php endif; ?>
<?php 
if($sdf_theme_styles['footer_bottom_font_hover_color']): ?>

#sdf-copyright:hover {

<?php if($sdf_theme_styles['footer_bottom_font_hover_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_font_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>	
<?php if($sdf_theme_styles['footer_bottom_link_color']): ?>
#sdf-copyright a{
	<?php if($sdf_theme_styles['footer_bottom_link_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_link_color']; ?>;<?php } ?>
}
<?php endif; ?>
<?php 
if($sdf_theme_styles['footer_bottom_link_hover_color']): ?>
#sdf-copyright a:hover {
<?php if($sdf_theme_styles['footer_bottom_link_hover_color']){ ?>color:<?php echo $sdf_theme_styles['footer_bottom_link_hover_color']; ?>;<?php } ?>
}
<?php endif; ?>

<?php if($sdf_theme_styles['theme_silo_font_color'] || $sdf_theme_styles['theme_silo_font_size'] || $sdf_theme_styles['theme_silo_line_height']): ?>
.widget_v_advanced_silo, .widget_v_advanced_silo a,.widget-area .widget.widget_v_advanced_silo a {
	<?php if($sdf_theme_styles['theme_silo_font_color']) echo "color: ".$sdf_theme_styles['theme_silo_font_color']. ";"; ?>
	 <?php if($sdf_theme_styles['theme_silo_font_size']) echo "font-size: ".$sdf_theme_styles['theme_silo_font_size']. $sdf_theme_styles['theme_silo_font_size_type']. ";"; ?>
	 <?php if($sdf_theme_styles['theme_silo_line_height']) echo "line-height: ".$sdf_theme_styles['theme_silo_line_height']. ";"; ?>
	}
<?php endif; ?>	

<?php if($sdf_theme_styles['theme_primary_button_bg_color'] || $sdf_theme_styles['theme_primary_button_text_color']): ?>
::selection {
	background:<?php echo $sdf_theme_styles['theme_primary_button_bg_color']; ?>;
	color:<?php echo $sdf_theme_styles['theme_primary_button_text_color']; ?>;
}
::-moz-selection {
	background:<?php echo $sdf_theme_styles['theme_primary_button_bg_color']; ?>;
	color:<?php echo $sdf_theme_styles['theme_primary_button_text_color']; ?>;
}
<?php endif; ?>

/* Buttons */

.btn > i.icon-left-side {
 margin-right: 0.3em;
}
.btn > i.icon-right-side {
 margin-left: 0.3em;
}

.btn {
<?php if($sdf_theme_styles['theme_button_text_family'] || $sdf_theme_styles['theme_button_text_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_button_text_family'], $sdf_theme_styles['theme_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_button_letter_spacing']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_button_border_width'] != '') ? $sdf_theme_styles['theme_button_border_width'] : '1px'; ?>;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}
.woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt {
-webkit-border-radius:<?php echo ($sdf_theme_styles['theme_tbs_border_radius_base']) ? $sdf_theme_styles['theme_tbs_border_radius_base'] : '0'; ?>; 
-moz-border-radius: <?php echo ($sdf_theme_styles['theme_tbs_border_radius_base']) ? $sdf_theme_styles['theme_tbs_border_radius_base'] : '0'; ?>; 
border-radius: <?php echo ($sdf_theme_styles['theme_tbs_border_radius_base']) ? $sdf_theme_styles['theme_tbs_border_radius_base'] : '0'; ?>;
<?php if($sdf_theme_styles['theme_button_text_family'] || $sdf_theme_styles['theme_button_text_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_button_text_family'], $sdf_theme_styles['theme_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_button_letter_spacing']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_button_border_width'] != '') ? $sdf_theme_styles['theme_button_border_width'] : '1px'; ?>;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
}
.woocommerce-shipping-calculator .panel {margin-top: 10px;}
.woocommerce-cart .cart-collaterals .cart_totals table td, .woocommerce-cart .cart-collaterals .cart_totals table th {
    padding: 6px;
}
.woocommerce form .form-row input.input-text, .woocommerce form .form-row textarea { line-height: inherit;}
.woocommerce .product .onsale, .woocommerce .product .single-onsale {
<?php if($sdf_theme_styles['theme_primary_button_text_color']): ?>color:<?php echo $sdf_theme_styles['theme_primary_button_text_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_primary_button_bg_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_primary_button_bg_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_text_family'] || $sdf_theme_styles['theme_button_text_weight']): ?>
	<?php get_font_family_weight($sdf_theme_styles['theme_button_text_family'], $sdf_theme_styles['theme_button_text_weight']); ?>
<?php endif;?>
<?php if($sdf_theme_styles['theme_button_text_transform'] != 'no'): ?>text-transform: <?php echo $sdf_theme_styles['theme_button_text_transform']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_letter_spacing']): ?>letter-spacing: <?php echo $sdf_theme_styles['theme_button_letter_spacing']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_primary_button_border_color']) : ?>border-color:<?php echo $sdf_theme_styles['theme_primary_button_border_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_button_border_width'] != '') ? $sdf_theme_styles['theme_button_border_width'] : '1px'; ?>;
<?php if($sdf_theme_styles['theme_button_transition']): ?>
-webkit-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-moz-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
-o-transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
transition: <?php echo $sdf_theme_styles['theme_button_transition']; ?>;
<?php endif; ?>
<?php if($sdf_theme_styles['theme_font_size']){ ?>font-size:<?php echo $sdf_theme_styles['theme_font_size']; ?>;<?php } ?>
    border-radius: 50%;
    height: 4.5333em;
    width: 4.5333em;
    line-height: 4.5333em;
		padding: 0;
    position: absolute;
    text-align: center;
    top: 12px;
    left: 27px;
    z-index: 100;
}
.woocommerce .product .single-onsale {
    top: 20px;
    left: 35px;
}
.woocommerce ul.products,.woocommerce-page ul.products{padding:0}
.woocommerce div.product .stock{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>}
.woocommerce div.product .out-of-stock{color:<?php echo $sdf_theme_styles['theme_font_link_color']; ?>}
.products > li.product{margin-bottom: 30px;}

<?php foreach (array( 'default', 'primary', 'secondary', 'success', 'info', 'link', 'warning', 'danger', 'black', 'grey', 'white' ) as $id) : ?>
<?php if( $sdf_theme_styles['theme_'.$id.'_button_text_color'] != '' || $sdf_theme_styles['theme_'.$id.'_button_bg_color'] != 'transparent' || $sdf_theme_styles['theme_'.$id.'_button_border_color'] != 'transparent' ): ?>

<?php if(is_woocommerce_active() && $id == 'primary'): ?>
.woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,<?php endif; ?>
.btn-<?php echo $id; ?>{
<?php if($sdf_theme_styles['theme_'.$id.'_button_text_color']): ?>color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_text_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_button_bg_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_bg_color']; ?>;<?php endif; ?>
<?php $theme_button_text_shadow = ($sdf_theme_styles['theme_'.$id.'_button_text_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_button_text_shadow'] : 'none'; ?>
text-shadow: <?php echo $theme_button_text_shadow; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_button_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_button_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_box_shadow_color']; ?>;
<?php endif; ?>
<?php $theme_button_border_color = ($sdf_theme_styles['theme_'.$id.'_button_border_color'] != '') ? $sdf_theme_styles['theme_'.$id.'_button_border_color'] : $sdf_theme_styles['theme_'.$id.'_button_bg_color']; ?>
border-color:<?php echo $theme_button_border_color; ?>;
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
}

<?php if($id == 'primary'): ?>
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle{
<?php if($sdf_theme_styles['theme_'.$id.'_button_bg_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_bg_color']; ?>;<?php endif; ?>
border-width:<?php echo ($sdf_theme_styles['theme_button_border_width'] != '') ? $sdf_theme_styles['theme_button_border_width'] : '1px'; ?>;
border-color:<?php echo $theme_button_border_color; ?>;
<?php if($sdf_theme_styles['theme_button_border_style']) : ?>border-style:<?php echo $sdf_theme_styles['theme_button_border_style']; ?>;<?php endif; ?>
}
<?php endif; ?>

<?php if(is_woocommerce_active() && $id == 'primary'): ?>
.woocommerce a.button:hover, .woocommerce-page a.button:hover, .woocommerce button.button:hover, .woocommerce-page button.button:hover, .woocommerce input.button:hover, .woocommerce-page input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce-page #respond input#submit:hover, .woocommerce #content input.button:hover, .woocommerce-page #content input.button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover,<?php endif; ?>
.btn-<?php echo $id; ?>:hover,.btn-<?php echo $id; ?>:focus,.btn-<?php echo $id; ?>:active,.btn-<?php echo $id; ?>.active,.open .dropdown-toggle.btn-<?php echo $id; ?>{
<?php if($sdf_theme_styles['theme_'.$id.'_button_text_hover_color']): ?>color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_text_hover_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_button_bg_hover_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_bg_hover_color']; ?>;<?php endif; ?>
<?php $theme_button_text_shadow_hover = ($sdf_theme_styles['theme_'.$id.'_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $theme_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
<?php $theme_button_border_hover_color = ($sdf_theme_styles['theme_'.$id.'_button_border_hover_color'] != '') ? $sdf_theme_styles['theme_'.$id.'_button_border_hover_color'] : $sdf_theme_styles['theme_'.$id.'_button_bg_color']; ?>
border-color:<?php echo $theme_button_border_hover_color; ?>;
}
.btn-<?php echo $id; ?>:active,.btn-<?php echo $id; ?>.active,.open .dropdown-toggle.btn-<?php echo $id; ?>{
background-image:none
}
.btn-<?php echo $id; ?>.disabled,.btn-<?php echo $id; ?>[disabled],fieldset[disabled] .btn-<?php echo $id; ?>,.btn-<?php echo $id; ?>.disabled:hover,.btn-<?php echo $id; ?>[disabled]:hover,fieldset[disabled] .btn-<?php echo $id; ?>:hover,.btn-<?php echo $id; ?>.disabled:focus,.btn-<?php echo $id; ?>[disabled]:focus,fieldset[disabled] .btn-<?php echo $id; ?>:focus,.btn-<?php echo $id; ?>.disabled:active,.btn-<?php echo $id; ?>[disabled]:active,fieldset[disabled] .btn-<?php echo $id; ?>:active,.btn-<?php echo $id; ?>.disabled.active,.btn-<?php echo $id; ?>[disabled].active,fieldset[disabled] .btn-<?php echo $id; ?>.active{
<?php if($sdf_theme_styles['theme_'.$id.'_button_bg_hover_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_bg_hover_color']; ?>;<?php endif; ?>
<?php $theme_button_text_shadow_hover = ($sdf_theme_styles['theme_'.$id.'_button_text_hover_shadow'] != '') ? $sdf_theme_styles['theme_'.$id.'_button_text_hover_shadow'] : 'none'; ?>
text-shadow: <?php echo $theme_button_text_shadow_hover; ?>;
<?php if($sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow'] != '' || $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color'] != ''): ?>
-webkit-box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color']; ?>;
box-shadow: <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow']; ?> <?php echo $sdf_theme_styles['theme_'.$id.'_button_hover_box_shadow_color']; ?>;
<?php endif; ?>
border-color:<?php echo $theme_button_border_hover_color; ?>;
}
.btn-<?php echo $id; ?> .badge{
<?php if($sdf_theme_styles['theme_'.$id.'_button_text_color']): ?>color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_text_color']; ?>;<?php endif; ?>
<?php if($sdf_theme_styles['theme_'.$id.'_button_bg_color']): ?>background-color:<?php echo $sdf_theme_styles['theme_'.$id.'_button_bg_color']; ?>;<?php endif; ?>
}
<?php endif; ?>
<?php endforeach;

	// child theme css
	if ( function_exists( 'sdf_get_child_typography_styles' ) ) {
		$sdf_child_typography_styles = sdf_get_child_typography_styles($sdf_theme_styles);
		echo $sdf_child_typography_styles;
	}

}

function get_font_family_weight($sdf_font_family, $sdf_font_weight) {

	$font_family = '';
	$font_weight = '';
	if($sdf_font_family){
		$font_args = explode(':', $sdf_font_family);
		$font_family = $font_args[0];
		$font_weight = (count($font_args) > 1) ? $font_args[1] : $sdf_font_weight;
	}
	
	$fontsList = array();

	if(isset(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families']) && is_array(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families'])){
		foreach(sdfGo()->_wpuGlobal->_currentSettings['typography']['theme_font_families'] as $t_font){
			$t_font_family = explode(",", $t_font);
			$fontsList[] = $t_font_family[0];
		}
	}

	if(isset(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font']) && is_array(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font'])){
		foreach(sdfGo()->_wpuGlobal->_currentSettings['typography']['google_web_font'] as $g_font){
			$g_font_family = explode(':', $g_font);
			$fontsList[] = $g_font_family[0];
		}
	}
	
	$exp_font_family = explode(",", $font_family);
	$first_font_family = $exp_font_family[0];

	if($font_family && in_array($first_font_family, $fontsList)){ echo 'font-family:' . $font_family . ';';}
	if($font_weight){ echo 'font-weight:' . $font_weight . ';';}
}

function get_filter_gradient_color($gradient_color) {

	$filter_gradient_color = '';
	if ( in_array( $gradient_color, array('', 'transparent'))) {
		$filter_gradient_color .= '00ffffff';
	}
	else{
		$filter_gradient_color .= 'ff';
		$filter_gradient_color .= preg_replace("/#{1}/i", "", $gradient_color);
	}
	
	echo $filter_gradient_color;
}

function get_background_color_image($bg_color, $bg_image, $bg_repeat, $echoe = true) {

	$bg_css = '';
	
	if($bg_color || $bg_image){
	
		$theme_bg_color = ($bg_color) ? $bg_color : '';
		$theme_bg_image = ($bg_image) ? $bg_image : '';
		if($theme_bg_image || $theme_bg_color) {
		
			if($bg_repeat == "cover"){
				
					$theme_bg_image = ($theme_bg_image) ? ' url('.$theme_bg_image.') no-repeat center center fixed;' : ';';
					$bg_css .='background:'.$theme_bg_color.$theme_bg_image .' -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;';
			} 
			elseif($bg_repeat == "contain"){
				$bg_css .= ($theme_bg_image) ? 'background-image: url('.$theme_bg_image.');' : '';
				$bg_css .= ($theme_bg_color) ? 'background-color:'.$theme_bg_color.';' : '';
				$bg_css .= 'background-position: center top; background-repeat: no-repeat; background-size: contain;';
			}
			else {
				if($theme_bg_image){
					$theme_bg_image = ' url('.$theme_bg_image.') '.$bg_repeat;
				}
				$bg_css .= 'background:'.$theme_bg_color.$theme_bg_image.';';
			}
			
		}
	}

	if($echoe) {
		echo $bg_css;
	}
	else{
		return $bg_css;
	}
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
