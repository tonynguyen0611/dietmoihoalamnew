( function() {
	"use strict";

	var sdfLightboxShortcode = function( editor, url ) {
		
		editor.addButton('sdf_tinymce_lightbox', function() {
			var values = [];

			var lightbox_pages = tinyMCE.activeEditor.getParam('lightbox_pages');
			for(var i in lightbox_pages) {
				values.push({text: i, value: lightbox_pages[i]});
			}
	 
			return {
				type: 'listbox',
				text: 'Lightbox',
				fixedWidth: true,
				onselect: function(e) {
					if (e) {
						var value = this.value();
						var ed = tinyMCE.activeEditor;
                ed.selection.setContent('[sdf_lightbox page_id="' + value + '" modal_animation="" modal_size="" auto_trigger_time="" modal_bottom="" modal_position=""]' + ed.selection.getContent() + '[/sdf_lightbox]');
					}		
					return false;
				},
				values: values,
			};
		});
	};
	tinymce.PluginManager.add( 'sdf_tinymce_lightbox', sdfLightboxShortcode );
	
	
	
} )();