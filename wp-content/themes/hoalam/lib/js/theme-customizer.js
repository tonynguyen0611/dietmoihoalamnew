/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and 
 * then make any necessary changes to the page using jQuery.
 */
( function( $ ) {
	//BACKGROUND
	//Update site background color in real time...
	wp.customize( 'sdf_theme_bgcolor', function( value ) {
		value.bind( function( newval ) { 
			var origstyle = $('body').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body').attr('style', origstyle + 'background-color: ' + newval+'!important; ' );
		} );
	} );

	//Update site background image...
	wp.customize( 'sdf_theme_background', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body').attr('style', origstyle + 'background-image:'+newval+'!important; ' );
		} );
	} );
	
	//Update site background image repeat...
	wp.customize( 'sdf_theme_bgrepeat', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body').attr('style', origstyle + 'background-repeat:'+newval+'!important; ' );
		} );
	} );
	
	//FONTS
	wp.customize( 'sdf_theme_line_height', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body').attr('style', origstyle + 'line-height:'+newval+'!important; ' );
		} );
	} );
	
	wp.customize( 'sdf_theme_font_size', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body, p').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body, p').attr('style', origstyle + 'font-size:'+newval+'px!important; ' );
			wp.customize( 'sdf_size_type', function(typevalue){
				typevalue.bind( function( type ) {
					$('body, p').attr('style', origstyle + 'font-size:'+newval+type+'!important; ' );
				});
			});
			
		} );
	} );
	
	wp.customize( 'sdf_theme_font_family', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body, p').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body, p').attr('style', origstyle + 'font-family:'+newval+'!important; ' );
		} );
	} );
	
	wp.customize( 'sdf_theme_font_color', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body, p').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('body, p').attr('style', origstyle + 'color:'+newval+'!important; ' );
		} );
	} );
	
	//LINKS
	wp.customize( 'sdf_theme_link_color', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('a, .container a, .container-fluid a').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('a, .container a, .container-fluid a').css('cssText', 'color:'+newval+'!important');
		} );
	} );
	
	/*wp.customize( 'sdf_theme_ahover_color', function( value ) {
		value.bind( function( newval ) {
			var origstyle = $('body').attr('style');
			if(origstyle == undefined) origstyle = '';
			$('a:hover, .container a:hover, .container-fluid a:hover').css('color',newva );
		} );
	} );*/
	
} )( jQuery );