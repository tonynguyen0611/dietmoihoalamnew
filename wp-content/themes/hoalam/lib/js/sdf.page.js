/*global jQuery:false */
(function($) { 
    "use strict";
	jQuery(document).ready(function($) {
	
	// custom page layout
	$('button','#sdf-page-layout').on('click', function () {
		var button = $(this);
		if( button.val() == '1'){
			$("#page-custom-layout").slideDown(300);
		}else{
			$("#page-custom-layout").slideUp(300);
		}
	});
	
	var customLayout = $('[name="sdf_toggle_page_layout"]').val();
	if( customLayout == '1'){
		$("#page-custom-layout").slideDown(300);
	}else{
		$("#page-custom-layout").slideUp(300);
	}
	
	// custom page header
	$('button','#sdf-page-header').on('click', function () {
		var button = $(this);
		if( button.val() == '1'){
			$("#page-custom-header").slideDown(300);
		}else{
			$("#page-custom-header").slideUp(300);
		}
	});
	
	var customHeader = $('[name="theme_pageHeader"]').val();
	if( customHeader == '1'){
		$("#page-custom-header").slideDown(300);
	}else{
		$("#page-custom-header").slideUp(300);
	}
	
	// custom Page Navigation
	$('button','#wpu-page-nav').on('click', function () {
		var button = $(this);
		if( button.val() == '1'){
			$("#page-menu-list").slideDown(300);
		}else{
			$("#page-menu-list").slideUp(300);
		}
	});
	
	var customHeader = $('[name="theme_pageNav"]').val();
	if( customHeader == '1'){
		$("#page-menu-list").slideDown(300);
	}else{
		$("#page-menu-list").slideUp(300);
	};
	
 	});
	
})(jQuery);		