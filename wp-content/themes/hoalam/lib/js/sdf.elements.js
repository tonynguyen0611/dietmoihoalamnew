(function($){
	"use strict";
	
	$.SDF = $.SDF || {};
	
	//select elements conditionals
	$.SDF.conditional_selects = function()
	{
		var the_body = $("body");
	
		the_body.on('change', '.sdf-style select, .sdf-style radio, .sdf-style input[type=checkbox], .sdf-style input[type=hidden]', function()
		{
			var current 	= $(this), 
				scope	= current.parents('.sdf-modal:eq(0)');
			
			if(!scope.length) scope = the_body;
			
			var id			= this.id.replace(/sdfTB/g,""),
				conditional	= scope.find('.sdf-form-element-container[data-check-element="'+id+'"]'), 
				value1		= this.value,
				is_hidden	= current.parents('.sdf-form-element-container:eq(0)').is('.hide');
				
				if(!conditional.length) return;
				
				conditional.each(function()
				{
					var current		= $(this), 
						check_data	= current.data(), 
						value2		= check_data.checkValue, 
						show		= false;
						
						if(!is_hidden)
						{
							switch(check_data.checkComparison)
							{
								case 'equals': 			if(value1 == value2) show = true; break;
								case 'not': 			if(value1 != value2) show = true; break;
								case 'is_larger': 		if(value1 >  value2) show = true; break;
								case 'is_smaller': 		if(value1 <  value2) show = true; break;
								case 'contains': 		if(value1.indexOf(value2) != -1) show = true; break;
								case 'doesnt_contain':  if(value1.indexOf(value2) == -1) show = true; break;
								case 'is_empty_or':  	if(value1 == "" || value1 == value2) show = true; break;
								case 'not_empty_and':  	if(value1 != "" && value1 != value2) show = true; break;
								
								
							}
						}
						
						if(show == true && current.is('.hide'))
						{
							current.css({display:'none'}).removeClass('hide').find('select, radio, input[type=checkbox]').trigger('change');
							current.slideDown(300);
						}
						else if(show == false  && !current.is('.hide'))
						{
							current.css({display:'block'}).addClass('hide').find('select, radio, input[type=checkbox]').trigger('change');
							current.slideUp(300);
						}
				});
		});
	}
	
})(jQuery);	