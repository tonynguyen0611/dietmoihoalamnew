/*global jQuery:false, alert */
(function($) { 
    "use strict";

jQuery(document).ready(function($) {

    $('.content').on('click', '.filter li[class!=active] a', function(e) { 
	
		//scrollTo
		$('.content').scrollTo($(".filter"));
		// Remove the active class
		$('.filter li').removeClass('active');
		// Apply the 'active' class to the clicked link
		$(this).parent().addClass('active');
			
		var slug = $(this).attr('data-filter');
		var cch_c_sortable_portfolio_items = $('.sortable-portfolio-items');
		var per_page = cch_c_sortable_portfolio_items.attr('data-count');
		var colcount = cch_c_sortable_portfolio_items.attr('data-colcount');
		var page_id = cch_c_sortable_portfolio_items.attr('data-pageid');
		
		$.ajax({  
		type: 'post',
		dataType: 'json',
		url: sdf_ajax_script.ajaxurl,  
		data: {  
			action: 'ajax_thumbs_nav',
			p_paged: '1',  
			p_page_id: page_id,
			p_per_page: per_page,
			p_colcount: colcount,		
			p_slug: slug 
		},  
		beforeSend: function() {
			cch_c_sortable_portfolio_items.empty().removeAttr("style").append('<div class="loader" class="inpage-scale0"><div class="bar"><i class="sphere"></i></div></div>');
			cch_c_sortable_portfolio_items.find('.loader').show().addClass('inpage-scale1');
		},  
		success: function(data){
			cch_c_sortable_portfolio_items.hide().empty().removeAttr("style").html($(data.thumbs)).show();
			
			setTimeout(function(){
				cch_c_sortable_portfolio_items.masonry( 'reload' );
			}, 250);
			// animate portfolio images
			setTimeout(function() {
				if($.fn.animate_multi_content){ 
					$(".sortable-portfolio-items:in-viewport").animate_multi_content(".portfolio-item", "animated fadeInDown", "fadeInDown", 0, 200);
				}
			}, 500);
			
			$('#maininner .sortable-portfolio-items > li > div.portfolio-item > div.hover-item').each( function() { $(this).hoverdir(); } );
			$('.pagination').empty(); 
			$('.pagination').html($(data.nav));
			
			$('a.pretty-view[data-rel]').each(function() {
				$(this).attr('rel', $(this).data('rel'));
			});
			$("a.pretty-view[data-rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast', slideshow:4500, hideflash: true});
			
		},  
		error: function(MLHttpRequest, errorThrown){  
			alert(errorThrown);
		}  
		});  
		e.preventDefault();
	});

});
 
})(jQuery);