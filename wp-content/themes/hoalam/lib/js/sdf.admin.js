/*global jQuery:false */
(function($) { 
    "use strict";
	jQuery(document).ready(function($) {
	
	// fixed elements modal open state fix for right padding
	$(window).load(function(){
		var oldSSB = $.fn.modal.Constructor.prototype.setScrollbar;
			$.fn.modal.Constructor.prototype.setScrollbar = function () 
			{
					oldSSB.apply(this);
					if(this.bodyIsOverflowing && this.scrollbarWidth) 
					{
							$('#wp-admin-bar-top-secondary').css('padding-right', this.scrollbarWidth);
					}       
			}

			var oldRSB = $.fn.modal.Constructor.prototype.resetScrollbar;
			$.fn.modal.Constructor.prototype.resetScrollbar = function () 
			{
					oldRSB.apply(this);
					$('#wp-admin-bar-top-secondary').css('padding-right', '');
			}

	});

// bootstrap collapse active state class for heading
// initial
$('.panel-collapse').each(function () {
	if( $(this).hasClass('in') ) { 
		$(this).parent().find('.panel-heading').addClass('active');
	} else {
		$(this).parent().find('.panel-heading').removeClass('active');
	}
});

// disable style inputs
$('#sdf_theme_font_settings .panel-collapse').find('input[name^=sdf_]').attr('disabled', true);
// SPEEDUP - enable/init shown style inputs due to a large number of inputs/pickers
$('#sdf_theme_font_settings .panel-collapse').on('shown.bs.collapse', function(e) {
	var root = $(this);
	// Enabling miniColors
	$(this).find('input.color-picker', root).minicolors({
		defaultValue: '',
		theme: 'bootstrap',
		opacity: true,
		position: 'bottom right',
		change: function(hex, opacity) {
				if( !hex ) return;
				if( opacity ) hex += ', ' + opacity;
				try {
					console.log(hex);
				} catch(e) {}
			},
	});
	//$(this).find('input[name^=sdf_]').removeAttr('disabled');
	$('input[name^=sdf_]', this).removeAttr('disabled');
	//$('input[name^=sdf_]', root).not(root.find(".tab-content input[name^=sdf_]")).removeAttr('disabled');
});

// change and remember opened
$('.panel-collapse').on('shown.bs.collapse', function(e) {
	$(e.currentTarget).parent().find('.panel-heading').addClass('active'); 
	$('input[name="sdf_panel_collapse_shown"]').attr('value', $(this).attr('id'));
});
$('.panel-collapse').on('hidden.bs.collapse', function(e){
	$(e.currentTarget).parent().find('.panel-heading').removeClass('active'); 
	$('input[name="sdf_panel_collapse_shown"]').attr('value', '');
});

// Opening accordion based on URL
var url = document.location.toString();
if ( url.match('#') && url.split('#')[1] == 'collapsett5' ) {
    $('#'+url.split('#')[1]).collapse('show');
		$('#'+url.split('#')[1]).parent().find('.panel-heading').addClass('active');
}

// expand/collapse all collapsibles
  var transition = false;
  var $active = true;

  $('.panel-title > a').click(function(e) {
    e.preventDefault();
  });

  $('#accordion').on('show.bs.collapse',function(){
    if($active){
        $('#accordion .in').collapse('hide');
    }
  });

  $('#accordion').on('hidden.bs.collapse',function(){
    if(transition){
        transition = false;
        $('.panel-collapse').collapse('show');
    }
  });

	$('.wpu-open-close').on('click', function() {
	$('.wpu-open-close').attr('disabled',true);
	if(!$active) {
		$active = true;
		$('.panel-title > a').attr('data-toggle', 'collapse');
		$('.panel-collapse').collapse('hide');
		$(this).html('+ Expand All');
	} else {
		$active = false;
		if($('.panel-collapse.in').length){
		transition = true;
		$('.panel-collapse.in').collapse('hide');       
		}
		else{
		$('.panel-collapse').collapse('show');
		}
		$('.panel-title > a').attr('data-toggle','');
		$(this).html('- Collapse All');
	}
	setTimeout(function(){
		$('.wpu-open-close').removeAttr('disabled');
	},800);
	});
    
	//menu picker
	$('body').on( 'change', 'select[name="sdf_menu_style"]', function() {
		var selected = $(this).val();
		$('.menu_options').hide();
		$('.menu_options_'+selected).each(function(){ $(this).removeClass('hidden').slideDown(); });
	});
	
	// tooltip
	$('[data-toggle="tooltip"]').tooltip();
	// popover
	$('[data-toggle="popover"]').popover();

	// Widget Areas Custom Background and Parallax
	//wp_localize_script data (sdf_admin_js_obj)
	$.each( sdf_admin_js_obj.sdf_sidebars, function( key, value ) {
	
		var styled_area = $('input[id="sdf_'+value+'_custom_bg"]').val();
		if(styled_area != "no"){
			$('#sdf-'+key+'-custom-bg').slideDown();
				if(styled_area == "parallax"){
					$('#sdf_'+value+'_par_bg_ratio_option').slideDown();
					$('#sdf_'+value+'_bgrepeat_option').slideUp();
				}else{
					$('#sdf_'+value+'_par_bg_ratio_option').slideUp();
					$('#sdf_'+value+'_bgrepeat_option').slideDown();
				}
		}else{
			$('#sdf-'+key+'-custom-bg').slideUp();
		}
		
		//change
		$('[id="area_'+value+'"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_custom_bg"]', function(){
			var styled_area = $(this).val();
			if(styled_area != "no"){
				$('#sdf-'+key+'-custom-bg').slideDown();
				if(styled_area == "parallax"){
					$('#sdf_'+value+'_par_bg_ratio_option').slideDown();
					$('#sdf_'+value+'_bgrepeat_option').slideUp();
				}else{
					$('#sdf_'+value+'_par_bg_ratio_option').slideUp();
					$('#sdf_'+value+'_bgrepeat_option').slideDown();
				}
			}else{
				$('#sdf-'+key+'-custom-bg').slideUp();
			}
		});
	
		// Area Widgets Style Customization
		var styled_wg = $('input[id="sdf_'+value+'_wg_custom_style"]').val();
		if(styled_wg != "no"){
			$('#sdf-'+key+'-custom-wg-styles').slideDown();
		}else{
			$('#sdf-'+key+'-custom-wg-styles').slideUp();
		}
		
		//change
		$('[id="widget_style_'+value+'"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_wg_custom_style"]', function(){
			var styled_wg = $(this).val();
			if(styled_wg != "no"){
				$('#sdf-'+key+'-custom-wg-styles').slideDown();
			}else{
				$('#sdf-'+key+'-custom-wg-styles').slideUp();
			}
		});
	
		// Area Widgets Typography Customization
		var styled_wg = $('input[id="sdf_'+value+'_wg_custom_typo"]').val();
		if(styled_wg != "no"){
			$('#sdf-'+key+'-custom-wg-typography').slideDown();
		}else{
			$('#sdf-'+key+'-custom-wg-typography').slideUp();
		}
		
		//change
		$('[id="widget_typo_'+value+'"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_wg_custom_typo"]', function(){
			var styled_wg = $(this).val();
			if(styled_wg != "no"){
				$('#sdf-'+key+'-custom-wg-typography').slideDown();
			}else{
				$('#sdf-'+key+'-custom-wg-typography').slideUp();
			}
		});
	
		// Area Widgets List Customization
		var styled_wg = $('input[id="sdf_'+value+'_wg_custom_list_style"]').val();
		if(styled_wg != "no"){
			$('#sdf-'+key+'-custom-wg-list-styles').slideDown();
		}else{
			$('#sdf-'+key+'-custom-wg-list-styles').slideUp();
		}
		
		//change
		$('[id="widget_list_'+value+'"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_wg_custom_list_style"]', function(){
			var styled_wg = $(this).val();
			if(styled_wg != "no"){
				$('#sdf-'+key+'-custom-wg-list-styles').slideDown();
			}else{
				$('#sdf-'+key+'-custom-wg-list-styles').slideUp();
			}
		});
		
		// Area Widgets List FA Icon
		var wg_list_style = $('input[id="sdf_'+value+'_wg_list_style"]').val();
		if(wg_list_style == "icon"){
			$('#sdf-'+key+'-custom-wg-list-icon').slideDown();
		}else{
			$('#sdf-'+key+'-custom-wg-list-icon').slideUp();
		}
		
		//change
		$('[id="widget_list_'+value+'"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_wg_list_style"]', function(){
			var wg_list_style = $(this).val();
			if(wg_list_style == "icon"){
				$('#sdf-'+key+'-custom-wg-list-icon').slideDown();
			}else{
				$('#sdf-'+key+'-custom-wg-list-icon').slideUp();
			}
		});
	
		
	});
	
	// Default List Customization
		var styled_list = $('input[id="sdf_theme_font_custom_list_styling"]').val();
		if(styled_list != "no"){
			$('#sdf-theme-font-custom-list-styles').slideDown();
		}else{
			$('#sdf-theme-font-custom-list-styles').slideUp();
		}
		
		//change
		$('[id="collapfont1"]').on('hiddenToggleChanged', 'input[id="sdf_theme_font_custom_list_styling"]', function(){
			var styled_list = $(this).val();
			if(styled_list != "no"){
				$('#sdf-theme-font-custom-list-styles').slideDown();
			}else{
				$('#sdf-theme-font-custom-list-styles').slideUp();
			}
		});
		
		// Default List FA Icon
		var wg_list_style = $('input[id="sdf_theme_font_custom_list_style"]').val();
		if(wg_list_style == "icon"){
			$('#sdf-theme-font-custom-list-icon').slideDown();
		}else{
			$('#sdf-theme-font-custom-list-icon').slideUp();
		}
		
		//change
		$('[id="sdf-theme-font-custom-list-styles"]').on('hiddenToggleChanged', 'input[id="sdf_theme_font_custom_list_style"]', function(){
			var wg_list_style = $(this).val();
			if(wg_list_style == "icon"){
				$('#sdf-theme-font-custom-list-icon').slideDown();
			}else{
				$('#sdf-theme-font-custom-list-icon').slideUp();
			}
		});
		
	
	// Follow Buttons Customization
	var styled_fo_btn = $('input[id="sdf_theme_follow_button_custom_style"]').val();
	if(styled_fo_btn != "no"){
		$('#sdf-theme-custom-follow-button-styles').slideDown();
	}else{
		$('#sdf-theme-custom-follow-button-styles').slideUp();
	}
	
	//change
	$('[id="theme_follow_button_custom_style_cont"]').on('hiddenToggleChanged', 'input[id="sdf_theme_follow_button_custom_style"]', function(){
		var styled_fo_btn = $(this).val();
		if(styled_fo_btn != "no"){
			$('#sdf-theme-custom-follow-button-styles').slideDown();
		}else{
			$('#sdf-theme-custom-follow-button-styles').slideUp();
		}
	});
	
	// Sharing Buttons Customization
	var styled_fo_btn = $('input[id="sdf_theme_sharing_button_custom_style"]').val();
	if(styled_fo_btn != "no"){
		$('#sdf-theme-custom-sharing-button-styles').slideDown();
	}else{
		$('#sdf-theme-custom-sharing-button-styles').slideUp();
	}
	
	//change
	$('[id="theme_sharing_button_custom_style_cont"]').on('hiddenToggleChanged', 'input[id="sdf_theme_sharing_button_custom_style"]', function(){
		var styled_fo_btn = $(this).val();
		if(styled_fo_btn != "no"){
			$('#sdf-theme-custom-sharing-button-styles').slideDown();
		}else{
			$('#sdf-theme-custom-sharing-button-styles').slideUp();
		}
	});
	
	// Post Format Buttons Customization
	var styled_fo_btn = $('input[id="sdf_theme_post_format_button_custom_style"]').val();
	if(styled_fo_btn != "no"){
		$('#sdf-theme-custom-post-format-button-styles').slideDown();
	}else{
		$('#sdf-theme-custom-post-format-button-styles').slideUp();
	}
	
	//change
	$('[id="theme_post_format_button_custom_style_cont"]').on('hiddenToggleChanged', 'input[id="sdf_theme_post_format_button_custom_style"]', function(){
		var styled_fo_btn = $(this).val();
		if(styled_fo_btn != "no"){
			$('#sdf-theme-custom-post-format-button-styles').slideDown();
		}else{
			$('#sdf-theme-custom-post-format-button-styles').slideUp();
		}
	});
	
	// Custom Background and Parallax for logo, header, page-header, footer, footer-bottom
	var area_ids = {
		'logo' : 'logo', 
		'header' : 'header', 
		'page-header' : 'page_header', 
		'footer' : 'footer', 
		'footer-bottom' : 'footer_bottom',
		'theme-navbar' : 'theme_navbar',
		'breadcrumbs' : 'breadcrumbs',
		'blockquote' : 'blockquote'
	};	
	
	$.each( area_ids, function( key, value ) {
	
		var styled_area = $('input[id="sdf_'+value+'_custom_bg"]').val();
		if(styled_area != "no"){
			$('#sdf-'+key+'-custom-bg').slideDown();
				if(styled_area == "parallax"){
					$('#sdf_'+value+'_par_bg_ratio_option').slideDown();
					$('#sdf_'+value+'_bgrepeat_option').slideUp();
				}else{
					$('#sdf_'+value+'_par_bg_ratio_option').slideUp();
					$('#sdf_'+value+'_bgrepeat_option').slideDown();
				}
		}else{
			$('#sdf-'+key+'-custom-bg').slideUp();
		}
		//change
		$('[id="wpu-font-acc"]').on('hiddenToggleChanged', 'input[id="sdf_'+value+'_custom_bg"]', function(){
			var styled_area = $(this).val();
			if(styled_area != "no"){
				$('#sdf-'+key+'-custom-bg').slideDown();
				if(styled_area == "parallax"){
					$('#sdf_'+value+'_par_bg_ratio_option').slideDown();
					$('#sdf_'+value+'_bgrepeat_option').slideUp();
				}else{
					$('#sdf_'+value+'_par_bg_ratio_option').slideUp();
					$('#sdf_'+value+'_bgrepeat_option').slideDown();
				}
			}else{
				$('#sdf-'+key+'-custom-bg').slideUp();
			}
		});
	
	});

	// sdf toggle buttons set hidden value and trigger 'hiddenToggleChanged'
	$('.sdf_toggle').on('click', 'button', function () {
		var button = $(this);
		button.parent().find('button.active').removeClass('active btn-sdf-grey').addClass('btn-default');
		button.removeClass('btn-default').addClass('btn-sdf-grey active');
		var hidden  = button.parent().next( 'input:hidden' );
		hidden.val(button.val());
		hidden.trigger('hiddenToggleChanged')
	}); 
			
	// text/image logo toggle
	var selectedVal = $('input[name="logo-options"]').val();
	if(	selectedVal == 'off'){
		$(".logo-text").each(function(){ $(this).show(); });
		$(".logo-path").each(function(){ $(this).hide(); });
	}else{
		$(".logo-text").each(function(){ $(this).hide(); });
		$(".logo-path").each(function(){ $(this).show(); });
	}

	$('button','#logo-option').on('click', function () {
		var button = $(this);
			var selectedVal =  button.val();
		
		if(selectedVal == 'off'){
			$(".logo-text").each(function(){ $(this).show(); });
			$(".logo-path").each(function(){ $(this).hide(); });
		}else{
			$(".logo-text").each(function(){ $(this).hide(); });
			$(".logo-path").each(function(){ $(this).show(); });
		}
	});
				
	// export/import/reset toggle
	var selectedVal = $('input[name="sdf_theme_options_io"]').val();
	if(	selectedVal == 1){
		$(".json-import").each(function(){ $(this).show(); });
		$(".json-export").each(function(){ $(this).hide(); });
		$(".json-reset").each(function(){ $(this).hide(); });
	}else if(selectedVal == 0){
		$(".json-import").each(function(){ $(this).hide(); });
		$(".json-export").each(function(){ $(this).show(); });
		$(".json-reset").each(function(){ $(this).hide(); });
	}else if(selectedVal == 2){
		$(".json-import").each(function(){ $(this).hide(); });
		$(".json-export").each(function(){ $(this).hide(); });
		$(".json-reset").each(function(){ $(this).show(); });
	}

	$('button','#theme_options_io_option').on('click', function () {
		var button = $(this);
			var selectedVal =  button.val();
		
		if(selectedVal == 1){
			$(".json-import").each(function(){ $(this).show(); });
			$(".json-export").each(function(){ $(this).hide(); });
			$(".json-reset").each(function(){ $(this).hide(); });
		}else if(selectedVal == 0){
			$(".json-import").each(function(){ $(this).hide(); });
			$(".json-export").each(function(){ $(this).show(); });
			$(".json-reset").each(function(){ $(this).hide(); });
		}else if(selectedVal == 2){
			$(".json-import").each(function(){ $(this).hide(); });
			$(".json-export").each(function(){ $(this).hide(); });
			$(".json-reset").each(function(){ $(this).show(); });
		}
	});
	  
	// Shortcode generator
		
	$("table.wpu-tbl").each(function(index){	
		var currentTable = $(this).find("tr");
		var rowCount = currentTable.length-2;

		if(rowCount < 1){
					var master = $(this);
					
					// Get a new row based on the prototype row
					var prot = master.find(".wpu-row").clone();
					prot.attr("class", "").addClass("active");
					prot.find(".index").html(rowCount);
					
					master.find("tbody").append(prot);
		}	
	});
	function createRow(i){
	
		var currentTable = $(i).parents("table.wpu-tbl");
		var currentTableTR = currentTable.find("tr");
		var rowCount = currentTableTR.length-2;
		var master = currentTable;
		
		// Get a new row based on the prototype row
		var prot = master.find(".wpu-row").clone();
		prot.attr("class", "")
		if(rowCount%2){ prot.addClass("active");}else{ prot.removeClass("active")};
		prot.find(".index").html(rowCount);
		
		master.find("tbody").append(prot);
				
	}
	
	function updateRow(x) {
		var rowIndex = 1;
		var currentTable = $(x).parents("table.wpu-tbl");
		currentTable.find("tr").each(function(index){
			if (rowIndex % 2) {
				$(this).addClass('active');
			}else{
				$(this).removeClass('active');
			}
			rowIndex++;
		})
		
		currentTable.find("th.index").each(function(i){
			$(this).html(i);
		});
	}
	
	// Add button functionality
	$("a.wpu-add").on('click',function() {
			createRow(this);
			updateRow(this);
	});
		
	// Remove button functionality
	$('body').on( 'click', 'a.wpu-remove', function() {
		var currentTable = $(this).parents("table.wpu-tbl");
		var currentTableTR = currentTable.find("tr");
		var rowCount = currentTableTR.length-2;
			
		if(rowCount >= 1){
			$(this).parents("tr:first").remove();
			var rowIndex = 1;
		
			currentTable.find("tr").each(function(index){
				if (rowIndex % 2) {
					$(this).addClass('active');
				}else{
					$(this).removeClass('active');
				}
				rowIndex++;
			});
		
			currentTable.find("th.index").each(function(i){
				$(this).html(i);
			});
		}
	});
	
	// Timed/Bottom Modal Add/Remove
		
	$(".ads-rows").each(function(index){	
		var currentTable = $(this).find("tr");
		var rowCount = currentTable.length-2;

		if(rowCount < 1){
					var master = $(this);
					
					// Get a new row based on the prototype row
					var prot = master.find(".ads-row").clone();
					prot.attr("class", "").addClass("active");
					prot.find(".index").html(rowCount);
					
					master.find("tbody").append(prot);
		}	
	});
	function createAdsRow(i){
	
		var currentGroup = $(i).parents(".modal-ads");
		var currentGroupRows = currentGroup.find('.ads-row');
		var rowCount = currentGroupRows.length+1;
		var master = currentGroup;
		
		// Get a new row based on the prototype row
		var prot = master.find(".ads-row-prototype").clone();
		prot.attr("class", "form-group ads-row")
		if(rowCount%2){ prot.addClass("active");}else{ prot.removeClass("active")};
		prot.find(".row-index").html(rowCount + '.');
		prot.insertBefore(master.find('.add-ads-holder'));
				
	}
	
	function updateAdsRow(x) {
		var rowIndex = 1;
		x.find('.ads-row').each(function(index){
			if (rowIndex % 2) {
				$(this).addClass('active');
			}else{
				$(this).removeClass('active');
			}
			$(this).find(".row-index").html(rowIndex + '.');
			rowIndex++;
		})
		
	}
	
	// Add button functionality
	$('.add-ads-holder').on('click', 'a.ads-add', function() {
			createAdsRow(this);
			updateAdsRow($(this).parents(".modal-ads"));
	});
		
	// Remove button functionality
	$('.modal-ads').on( 'click', 'a.ads-remove', function() {
		var currentGroup = $(this).parents(".modal-ads");
		var currentGroupRow = $(this).parents(".ads-row");
		var rowCount = currentGroupRow.length+1;
			
		if(rowCount >= 1){
			currentGroupRow.remove();
			updateAdsRow(currentGroup);
		}
	});

	// Header Builder
	var headerActiveList = $('#sdf-block-header-active');
	if(headerActiveList){
		headerActiveList.sortable({
			cancel: ".sdf-box-btn",
			update: function(event, ui) {
					var activelist = headerActiveList.sortable('toArray').toString();
					$("#sdf_theme_header_order").val(activelist);	
			},
			forcePlaceholderSize: true, 
			placeholder: 'ui-state-highlight',
			connectWith: ".connectedHeaderList"
		});
		headerActiveList.disableSelection();
	}
	var headerInactiveList = $('#sdf-block-header-inactive');
	if(headerInactiveList){
		headerInactiveList.sortable({
			cancel: ".sdf-box-btn",
			update: function(event, ui) {
					var inactivelist = headerInactiveList.sortable('toArray').toString();
					$("#sdf_theme_header_order_inactive").val(inactivelist);	
			},
			forcePlaceholderSize: true, 
			placeholder: 'ui-state-highlight',
			connectWith: ".connectedHeaderList"
		});
		headerInactiveList.disableSelection();
	}

	var widths = {
		'1/12' : 'col-md-1',
		'1/6' : 'col-md-2',
		'1/4' : 'col-md-3',
		'1/3' : 'col-md-4',
		'5/12' : 'col-md-5',
		'1/2' : 'col-md-6',
		'7/12' : 'col-md-7',
		'2/3' : 'col-md-8',
		'3/4' : 'col-md-9',
		'5/6' : 'col-md-10',
		'11/12' : 'col-md-11',
		'1/1' : 'col-md-12'
	};	
	var box_sizes = ['1/12', '1/6', '1/4', '1/3', '5/12', '1/2', '7/12', '2/3', '3/4', '5/6', '11/12', '1/1'];	

	// increase box size
	$('.block-builder-box-size-inc').click(function(){
		var box = $(this).parents('.block-builder-box');
		
		for( var i = 0; i < box_sizes.length-1; i++ ){
		
			if( ! box.hasClass( widths[box_sizes[i]] ) ) continue;
			
			box
				.removeClass( widths[box_sizes[i]] )
				.addClass( widths[box_sizes[i+1]] )
				.find('.block-builder-box-size').val( box_sizes[i+1] );
			
			box.find('.block-builder-box-width').text( box_sizes[i+1] );
	
			break;
		}	
	});

	// decrease size
	$('.block-builder-box-size-dec').click(function(){
		var box = $(this).parents('.block-builder-box');
		
		for( var i = 1; i < box_sizes.length; i++ ){
			
			if( ! box.hasClass( widths[box_sizes[i]] ) ) continue;
			
			box
				.removeClass( widths[box_sizes[i]] )
				.addClass( widths[box_sizes[i-1]] )
				.find('.block-builder-box-size').val( box_sizes[i-1]);
			
			box.find('.block-builder-box-width').text( box_sizes[i-1] );
			
			break;
		}		
	});

	// global custom page layout
	var customLayout = $("#sdf_custom_layout").is(':checked');
	
	if( customLayout){
		$("#wpu-sidebar-width").slideUp(300);
		$("#custom-layout").slideDown(300);
	}else{
		$("#custom-layout").slideUp(300);
		$("#wpu-sidebar-width").slideDown(300);
	}
	
	$(".sdf_page_layout").change(function () {
		var customLayout = $("#sdf_custom_layout").is(':checked');
			
			if( customLayout){
				$("#wpu-sidebar-width").slideUp(300);
				$("#custom-layout").slideDown(300);
			}else{
				$("#custom-layout").slideUp(300);
				$("#wpu-sidebar-width").slideDown(300);
			}
	});

	// add/clone/remove/rename style presets
	// first show and hide "after" load
	$('.style-preset-control-btns').delay(1500).fadeIn(400).removeClass('hide');
	var preset_val = $('select[name="sdf_theme_style_preset"]').val();
	var preset_txt = $('select[name="sdf_theme_style_preset"] option:selected').text(); 
	var preset_rename = $('.theme-preset-rename'); 
	var preset_remove = $('.theme-preset-remove'); 
	var preset_add = $('.theme-preset-add'); 
	var preset_clone = $('.theme-preset-clone'); 
	var go_rename = $('.go-preset-rename'); 
	var go_remove = $('.go-preset-remove'); 
	var go_add = $('.go-preset-add'); 
	var go_clone = $('.go-preset-clone'); 
	var go_cancel = $('.go-preset-cancel'); 
	
	$('input[name="sdf_input_preset_rename"]').attr('value', preset_txt);
	$('input[name="sdf_input_preset_rename_val"]').attr('value', preset_val);
	$('button[name="sdf_submit_preset_remove"]').text('Remove Skin "'+preset_txt+'"?');
	$('input[name="sdf_input_preset_remove_val"]').attr('value', preset_val);
	
	preset_rename.each(function(){ $(this).hide(); });
	preset_remove.each(function(){ $(this).hide(); });
	preset_add.each(function(){ $(this).hide(); });
	preset_clone.each(function(){ $(this).hide(); });
	go_cancel.hide(50);
		
    $('select[name="sdf_theme_style_preset"]').on('change', function(){
		var preset_val = $('select[name="sdf_theme_style_preset"]').val();
		var preset_txt = $('select[name="sdf_theme_style_preset"] option:selected').html();
		
		$('input[name="sdf_input_preset_rename"]').attr('value', preset_txt);
		$('input[name="sdf_input_preset_rename_val"]').attr('value', preset_val);
		$('button[name="sdf_submit_preset_remove"]').text('Remove Skin "'+preset_txt+'"');
		$('input[name="sdf_input_preset_remove_val"]').attr('value', preset_val);
    });
	
	go_cancel.on('click', function(e){
		e.preventDefault();
		$('.go-preset-rename').animate_show_el(300);
		$('.go-preset-remove').animate_show_el(300);
		$('.go-preset-add').animate_show_el(300);
		$('.go-preset-clone').animate_show_el(300);
		$(".theme-preset-rename").each(function(){ $(this).animate_hide_el(300); });
		$(".theme-preset-remove").each(function(){ $(this).animate_hide_el(300); });
		$(".theme-preset-add").each(function(){ $(this).animate_hide_el(300); });
		$(".theme-preset-clone").each(function(){ $(this).animate_hide_el(300); });
		go_cancel.animate_hide_el(300);
	});
	
	go_rename.on('click', function(e){
		e.preventDefault();
		go_rename.animate_hide_el(300);
		go_remove.animate_hide_el(300);
		go_add.animate_hide_el(300);
		go_clone.animate_hide_el(300);
		preset_rename.each(function(){ $(this).animate_show_el(300); });
		preset_remove.each(function(){ $(this).animate_hide_el(300); });
		preset_add.each(function(){ $(this).animate_hide_el(300); });
		preset_clone.each(function(){ $(this).animate_hide_el(300); });
		go_cancel.animate_show_el(300);
	});
	go_remove.on('click', function(e){
		e.preventDefault();
		go_remove.animate_hide_el(300);
		go_rename.animate_hide_el(300);
		go_add.animate_hide_el(300);
		go_clone.animate_hide_el(300);
		preset_remove.each(function(){ $(this).animate_show_el(300); });
		preset_rename.each(function(){ $(this).animate_hide_el(300); });
		preset_add.each(function(){ $(this).animate_hide_el(300); });
		preset_clone.each(function(){ $(this).animate_hide_el(300); });
		go_cancel.animate_show_el(300);
	});
	go_add.on('click', function(e){
		e.preventDefault();
		go_remove.animate_hide_el(300);
		go_rename.animate_hide_el(300);
		go_add.animate_hide_el(300);
		go_clone.animate_hide_el(300);
		preset_add.each(function(){ $(this).animate_show_el(300); });
		preset_clone.each(function(){ $(this).animate_hide_el(300); });
		preset_rename.each(function(){ $(this).animate_hide_el(300); });
		preset_remove.each(function(){ $(this).animate_hide_el(300); });
		go_cancel.animate_show_el(300);
	});
	go_clone.on('click', function(e){
		e.preventDefault();
		go_remove.animate_hide_el(300);
		go_rename.animate_hide_el(300);
		go_add.animate_hide_el(300);
		go_clone.animate_hide_el(300);
		preset_clone.each(function(){ $(this).animate_show_el(300); });
		preset_add.each(function(){ $(this).animate_hide_el(300); });
		preset_rename.each(function(){ $(this).animate_hide_el(300); });
		preset_remove.each(function(){ $(this).animate_hide_el(300); });
		go_cancel.animate_show_el(300);
	});

		
	//wp_localize_script data (sdf_admin_js_obj)
	var preset_colors_ul = '<li data-color="" data-preset="Transparent"><a href="#">Transparent<span class="preset-swatch"><span class="preset-swatch-color" style="background-color: transparent;"></span></span></a></li>';
	$.each( sdf_admin_js_obj.sdf_preset_colors, function( key, value ) {
		var hex_opacity_arr = value.split(':');
		var hex = hex_opacity_arr[0];
		var opacity = hex_opacity_arr[1];
		var hex_p = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
		var rgb = {
			r: hex_p >> 16,
			g: (hex_p & 0x00FF00) >> 8,
			b: (hex_p & 0x0000FF)
		};
		if( !rgb ) return null;
		if( opacity === undefined ) opacity = 1;
		var rgba = 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ' + parseFloat(opacity) + ')';
		preset_colors_ul += '<li data-color="'+hex+'" data-preset="'+key+'" data-opacity="'+opacity+'"><a href="#">'+key+' <span class="preset-swatch"><span class="preset-swatch-color" style="background-color: '+rgba+';"></span></span></a></li>';
	});
	$('.preset_colors_dd').on('click', function(e){
		e.preventDefault();
		$(this).parent().find('.preset_colors').html(preset_colors_ul);
		
		$('ul.preset_colors li a').on('click', function(e){
			e.preventDefault();
			// visible
			var preset_opacity = $(this).parent().data( "opacity" );
			$(this).first().parents().eq(3).find('input.color-picker').minicolors('opacity',preset_opacity);
			var preset_color = $(this).parent().data( "color" );
			$(this).first().parents().eq(3).find('input.color-picker').minicolors('value',preset_color);
			// hidden
			var preset_name = $(this).parent().data( "preset" );
			$(this).first().parents().eq(3).find('input.color-preset').attr('value', preset_name);
		});
	});		
	
	// add/remove/change preset colors
	var color_change = $('.theme-color-change'); 
	var color_remove = $('.theme-color-remove');
	var color_change_val = $('[name="sdf_input_color_change_val"]');
	var color_remove_val = $('[name="sdf_input_color_remove_val"]');
	var go_color_change = $('.go-color-change'); 
	var go_color_remove = $('.go-color-remove');
	var go_color_cancel = $('.go-color-cancel'); 
	
	color_change.each(function(){ $(this).animate_hide_el(0); });
	color_remove.each(function(){ $(this).animate_hide_el(0); });
	
	go_color_cancel.on('click', function(e){
		e.preventDefault();
		$('.go-color-change').animate_show_el(150);
		$('.go-color-remove').animate_show_el(150);
		var parent_go_color_cancel = $(this).parent().parent();
		parent_go_color_cancel.find('input').each(function(){ $(this).attr('disabled', true); });
		parent_go_color_cancel.find(".theme-color-change").each(function(){ $(this).animate_hide_el(300); });
		parent_go_color_cancel.find(".theme-color-remove").each(function(){ $(this).animate_hide_el(300); });
	});
	
	go_color_change.on('click', function(e){
		e.preventDefault();
		var parent_go_color_change = $(this).parent();
		// get key of color to change
		var color_change_preset = parent_go_color_change.parent().find('[name="preset_color_names[]"]').val();
		color_change_val.attr('value', color_change_preset);
		// disable all inputs
		$('input.color-preset-name').attr('disabled', true);
		$('[name="preset_color_names[]"]').attr('disabled', true);
		$('[name="preset_color_hexs[]"]').attr('disabled', true);
		// set all change/remove buttons to defaults
		color_change.each(function(){ $(this).animate_hide_el(300); });
		color_remove.each(function(){ $(this).animate_hide_el(300); });
		$('.go-color-change').animate_show_el(150);
		$('.go-color-remove').animate_show_el(150);
		// enable current color inputs
		parent_go_color_change.parent().find('input').each(function(){ $(this).attr('disabled', false); });
		// setting current change/remove field buttons
		parent_go_color_change.find('.go-color-remove').animate_hide_el(300);
		parent_go_color_change.find('.go-color-change').animate_hide_el(300);
		parent_go_color_change.parent().find('.theme-color-change').each(function(){ $(this).animate_show_el(150); });
		parent_go_color_change.parent().find('.theme-color-remove').each(function(){ $(this).animate_hide_el(300); });
		
	});
	go_color_remove.on('click', function(e){
		e.preventDefault();
		// get key of color to remove
		var color_remove_preset = $(this).parent().parent().find('[name="preset_color_names[]"]').val();
		color_remove_val.attr('value', color_remove_preset);
		// disable all inputs
		$('[name="preset_color_names[]"]').attr('disabled', true);
		$('[name="preset_color_hexs[]"]').attr('disabled', true);
		// set all buttons to defaults
		color_remove.each(function(){ $(this).animate_hide_el(300); });
		color_change.each(function(){ $(this).animate_hide_el(300); });
		$('.go-color-change').animate_show_el(150);
		$('.go-color-remove').animate_show_el(150);
		// setting current field buttons
		$(this).parent().find('.go-color-remove').animate_hide_el(300);
		$(this).parent().find('.go-color-change').animate_hide_el(300);
		$(this).parent().parent().find('.theme-color-remove').each(function(){ $(this).css('display','inline-block').animate_show_el(150); });
		$(this).parent().parent().find('.theme-color-change').each(function(){ $(this).animate_hide_el(300); });
	});

});

	$.fn.animate_show_el = function(anim_duration) 
	{		
		$(this).css( {overflow:"hidden"}).animate({height:"auto", opacity:1}, anim_duration, 
			function(){ $(this).css({display:"inline-block", overflow:"visible", visibility:"visible", position:"relative", height:"auto"}); });
	};

	$.fn.animate_hide_el = function(anim_duration) 
	{		
		$(this).css({overflow:"hidden"}).animate({height:0, opacity:0}, anim_duration,
			function(){ $(this).css({display:"none", overflow:"visible", visibility:"hidden", position:"absolute"}); });
	};
	
})(jQuery);