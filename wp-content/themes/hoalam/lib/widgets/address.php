<?php
/*
 *
 * Widget Address
 * 
 */
if (!class_exists('Sdf_Address_Widget'))
{
class Sdf_Address_Widget extends WP_Widget {

	function Sdf_Address_Widget() {
		$widget_ops = array('description' => 'Display your company address info (address, work hours, phone, mobile, fax, email, link)');
		parent::__construct(false, '<i></i>SDF Address', $widget_ops);      
	}

	function widget($args, $instance) {  
		
		global $wpdb, $wp_scripts;
		
		extract ( $args, EXTR_SKIP );
		
		$unique_wid = $args['widget_id'];
		$address_icons_list = array ('Location' => 'map-marker', 'Work Hours' => 'clock-o', 'Phone' => 'phone', 'Mobile' => 'mobile', 'Fax' => 'print', 'Email' => 'envelope', 'Website' => 'link');
		
		
		if (!isset($instance['title'])) $instance['title'] = "Address";
		if (!isset($instance['icon_size'])) $instance['icon_size'] = "1x";
		if (!isset($instance['allow_icons'])) $instance['allow_icons'] = "yes";
		
		$type = ($instance['allow_icons'] == 'yes') ? ' fa-ul' : ' list-unstyled';
		
		echo $before_widget;

		if ($instance['title']) {
			echo $before_title . $instance['title'] . $after_title;
		}
		
		$output = '<div id="'.$unique_wid.'" class="clearfix">';
		
		if ($instance['info']) {
			$output .= '<p class="address-info">'.$instance['info'].'</p>';
		}
		$output .= '<ul class="address-widget'.$type.' line-'.$instance['icon_size'].'">';
		
		foreach ( $address_icons_list as $address_name => $address_icon ) {
		
			if (!isset($instance[$address_icon])) $instance[$address_icon] = '';
			
			if (isset($instance[$address_icon]) && $instance[$address_icon] != '')  
			{
				if ($address_icon == 'envelope') {
					$output .= '<li>';
					if ( $instance['allow_icons'] == 'yes' ) {
						$output .= '<i class="fa-li fa fa-'.$address_icon.' fa-'.$instance['icon_size'].'"></i> ';
					}
					$output .= '<a href="mailto:'. antispambot( $instance[$address_icon] ) .'">'. antispambot( $instance[$address_icon] ) .'</a></li>';
				}
				elseif ($address_icon == 'link') {
					$output .= '<li>';
					if ( $instance['allow_icons'] == 'yes' ) {
						$output .= '<i class="fa-li fa fa-'.$address_icon.' fa-'.$instance['icon_size'].'"></i> ';
					}
					$output .= '<a  href="http://'.$instance[$address_icon].'">'.$instance[$address_icon].'</a></li>';
				}
				else {
					$output .= '<li>';
					if ( $instance['allow_icons'] == 'yes' ) {
						$output .= '<i class="fa-li fa fa-'.$address_icon.' fa-'.$instance['icon_size'].'"></i> ';
					}
					$output .= $instance[$address_icon].'</li>';
				}
			}
		}

		$output .= '</ul></div>';
		$output .= '<script type="text/javascript">
		/* <![CDATA[ */ 
		/*global jQuery:false */
		(function($) { 
			"use strict";
			jQuery(document).ready(function($) {
			// animate address info
			if($.fn.animate_multi_content){ 
				$("#'.$unique_wid.':in-viewport").animate_multi_content("li", "animated05 fadeInLeft", "fadeInLeft", 500, 200);
				$(window).scroll(function() {
					$("#'.$unique_wid.':in-viewport").animate_multi_content("li", "animated05 fadeInLeft", "fadeInLeft", 500, 200);
				});
			}	
			});
			
		})(jQuery);
		/* ]]> */
		</script>';
		
		echo $output;

		echo $after_widget;

	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
	
		if (!isset($instance['title'])) $instance['title'] = "Address";
		$title = esc_attr($instance['title']);
		if (!isset($instance['tooltip_pos'])) $instance['tooltip_pos'] = "top";
		if (!isset($instance['icon_size'])) $instance['icon_size'] = "1x";
		if (!isset($instance['allow_icons'])) $instance['allow_icons'] = "yes";
		
		$address_icons_list = array ('Info text' => 'info', 'Location' => 'map-marker', 'Work Hours' => 'clock-o', 'Phone' => 'phone', 'Mobile' => 'mobile', 'Fax' => 'print', 'Email' => 'envelope', 'Website' => 'link');
		 
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'allow_icons' ); ?>"><?php _e( 'Allow Icons', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'allow_icons' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'allow_icons' ); ?>">
				<option value="yes"<?php selected( $instance['allow_icons'], 'yes' ); ?>><?php _e('Yes',SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['allow_icons'], 'no' ); ?>><?php _e('No',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'icon_size' ); ?>"><?php _e( 'Choose Icon Size', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'icon_size' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'icon_size' ); ?>">
				<option value="1x"<?php selected( $instance['icon_size'], '1x' ); ?>><?php _e('1em',SDF_TXT_DOMAIN); ?></option>
				<option value="lg"<?php selected( $instance['icon_size'], 'lg' ); ?>><?php _e('1.33em',SDF_TXT_DOMAIN); ?></option>
				<option value="2x"<?php selected( $instance['icon_size'], '2x' ); ?>><?php _e('2em',SDF_TXT_DOMAIN); ?></option>
				<option value="3x"<?php selected( $instance['icon_size'], '3x' ); ?>><?php _e('3em',SDF_TXT_DOMAIN); ?></option>
				<option value="4x"<?php selected( $instance['icon_size'], '4x' ); ?>><?php _e('4em',SDF_TXT_DOMAIN); ?></option>
				<option value="5x"<?php selected( $instance['icon_size'], '5x' ); ?>><?php _e('5em',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<?php 
		foreach ( $address_icons_list as $address_name => $address_icon ) {
		
			if (!isset($instance[$address_icon])) $instance[$address_icon] = '';
			
			if ($address_icon == 'info') {
				echo '<p>
						<label for="'.$this->get_field_id($address_icon).'">'. $address_name.':'.'</label>
						<textarea id="'.$this->get_field_id($address_icon).'" name="'.$this->get_field_name($address_icon).'" class="widefat" rows="2" cols="20">'.$instance[$address_icon].'</textarea>
					</p>';
			} else {
				echo '<p>
						<label for="'.$this->get_field_id($address_icon).'">'. $address_name.':'.'</label>
						<input type="text" name="'.$this->get_field_name($address_icon).'"  value="'.$instance[$address_icon].'" class="widefat" id="'.$this->get_field_id($address_icon).'" />
					</p>';
			}
		} 
		?>
		
<?php
	} // End form()

} // End Class

function address_widget_init() {
	register_widget("Sdf_Address_Widget");
}
add_action('widgets_init','address_widget_init');
}