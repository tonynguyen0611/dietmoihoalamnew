<?php

if (!class_exists('SDFSocShareIcons_Widget'))
{
class SDFSocShareIcons_Widget extends WP_Widget {

	public function __construct(){
		$widget_ops = array('classname' => 'sdf_social_sharing_buttons_widget', 'description' => 'Enable your website visitors to easily share your content with their social media connections and networks.');
		parent::__construct('sdf-social-sharing-widget', '<i></i>SDF Social Media Share Buttons', $widget_ops);		
	}

	function widget($args, $instance) {  
		extract ( $args, EXTR_SKIP );
		
		$unique_wid = $args['widget_id'];
		
		if ( !isset($instance['title']) ) $instance['title'] = "Share";
		$title = esc_attr($instance['title']);
		if ( !isset($instance['title_pos']) ) $instance['title_pos'] = "top";
		if ( !isset($instance['tooltip_pos']) ) $instance['tooltip_pos'] = "top";
		if ( !isset($instance['icon_name']) ) $instance['icon_name'] = array();
		if ( !isset($instance['icon_size']) ) $instance['icon_size'] = '';
		if ( !isset($instance['button_style']) ) $instance['button_style'] = '';
		// shortcode call
		if ( !is_array($instance['icon_name']) ) {
			$instance['icon_name'] = array_map('trim',explode(",",$instance['icon_name']));
		}
		
		$page_ID = sdf_page_holder_ID();
		$share_title = get_the_title( $page_ID );
		$share_permalink = get_permalink( $page_ID );
		
		$the_post_by_id = get_post($page_ID);
		$text = $the_post_by_id->post_content;
		// remove widget shortcode to avoid infinite loop from share icons widget
		$text = preg_replace ('/\[sdf_widget[^\]]*\](.*)\[\/sdf_widget\]/', '', $text);
		$raw_excerpt = $text;
		
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);
		$excerpt_length = apply_filters('excerpt_length', sdfGo()->sdf_get_global('blog','excerpt_length'));
		$text = wp_trim_words( $text, $excerpt_length ); //since wp3.3
		$share_excerpt = apply_filters('wp_trim_excerpt', $text, $raw_excerpt); //since wp3.3
	
		$share_thumbnail = '';
		if ( has_post_thumbnail()) {
			$share_thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id( $page_ID ));
			$share_thumbnail = $share_thumbnail[0];
		}
		
		
		$twitter_handle = (sdfGo()->sdf_get_global('social_media','social_share_twitter_handle')) ? sdfGo()->sdf_get_global('social_media','social_share_twitter_handle') : '';
				
		$this->socialShareLinks = array(
			'google-plus' => 'https://plus.google.com/share?url='.urlencode($share_permalink),
			'facebook' => 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($share_permalink).'&amp;t='.urlencode($share_title),
			'twitter' => 'https://twitter.com/intent/tweet?text='.urlencode($share_title).'&amp;url='.urlencode($share_permalink).'&amp;via='.urlencode($twitter_handle),
			'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&amp;url='.urlencode($share_permalink).'&amp;title='.urlencode($share_title).'&amp;summary='.urlencode($share_excerpt),
			'pinterest' => 'https://pinterest.com/pin/create/button/?url='.urlencode($share_permalink).'&amp;description='.urlencode($share_excerpt).'&amp;media='.urlencode($share_thumbnail),
			'tumblr' => 'http://www.tumblr.com/share/link?url='.urlencode($share_permalink).'&amp;name='.urlencode($share_title).'&amp;description='.urlencode($share_excerpt),
			'vk' => 'https://vk.com/share.php?url='.urlencode($share_permalink).'&amp;title='.urlencode($share_title).'&amp;description='.urlencode($share_excerpt).'&amp;image='.urlencode($share_thumbnail).'&amp;noparse=true',
			'reddit' 	=> 'http://reddit.com/submit?url='.urlencode($share_permalink).'&amp;title='.urlencode($share_title),
			'envelope' 		=> 'mailto:?subject='.urlencode($share_title).'&amp;body='.urlencode($share_permalink),
			);

		echo $before_widget;
		$output = '';
		if ( ($instance['title_pos'] == 'top') && !empty($title) ) {
			$output .= $before_title . $title . $after_title;
		}
			
		$output .= '<ul id="'.$unique_wid.'" class="social-menu sharing">';
		foreach ( sdfGo()->_wpuGlobal->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
			if (is_array($instance['icon_name']) & in_array($soc_slug, $instance['icon_name'])) {
				$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) );
				$soc_link = $this->socialShareLinks[$soc_slug];
				$icon_text = sdfGo()->sdf_get_global('typography','theme_'.$id.'_sharing_button_custom_text');
				$toggle_icon = sdfGo()->sdf_get_global('typography','theme_sharing_button_icon_toggle');
				$soc_icon = ($toggle_icon) ? '<i class="fa fa-'.$soc_slug.' '.$instance['icon_size'].'"></i>' : '';
				$button_style = ($instance['button_style'] == '') ? ' sharing-custom sharing-'.$soc_slug : (($instance['button_style'] == 'plain') ? '' : ' btn btn-'.$instance['button_style']);
				$li_style = ($instance['button_style'] == '') ? 'sharing-icon' : 'sharing-btn';
				
				$output .= '<li class="'.$li_style.'">
						<a class="'.$button_style.' social-tooltip" data-original-title="'.$soc_name.'" data-placement="'.$instance['tooltip_pos'].'" data-toggle="tooltip" href="'.$soc_link.'" target="_blank" rel="nosharing">'.$soc_icon.' '.$icon_text.'</a>
						</li>';
			}
		}
		$output .= '</ul>';
		echo $output;
		if ( $instance['title_pos'] == 'bottom' ) {
			echo $before_title . $title . $after_title;
		}
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
	
		if (!isset($instance['title'])) $instance['title'] = "Share";
		$title = esc_attr($instance['title']);
		if (!isset($instance['title_pos'])) $instance['title_pos'] = "top";
		if (!isset($instance['tooltip_pos'])) $instance['tooltip_pos'] = "top";
		if (!isset($instance['icon_name']) ) $instance['icon_name'] = array();
		if ( !isset($instance['icon_size']) ) $instance['icon_size'] = '';
		if ( !isset($instance['button_style']) ) $instance['button_style'] = '';

		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title_pos' ); ?>"><?php _e( 'Choose Title Position', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'title_pos' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title_pos' ); ?>">
				<option value="top"<?php selected( $instance['title_pos'], 'top' ); ?>><?php _e('Top',SDF_TXT_DOMAIN); ?></option>
				<option value="bottom"<?php selected( $instance['title_pos'], 'bottom' ); ?>><?php _e('Bottom',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tooltip_pos' ); ?>"><?php _e( 'Choose Tooltip Position', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'tooltip_pos' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'tooltip_pos' ); ?>">
				<option value="top"<?php selected( $instance['tooltip_pos'], 'top' ); ?>><?php _e('Top',SDF_TXT_DOMAIN); ?></option>
				<option value="right"<?php selected( $instance['tooltip_pos'], 'right' ); ?>><?php _e('Right',SDF_TXT_DOMAIN); ?></option>
				<option value="bottom"<?php selected( $instance['tooltip_pos'], 'bottom' ); ?>><?php _e('Bottom',SDF_TXT_DOMAIN); ?></option>
				<option value="left"<?php selected( $instance['tooltip_pos'], 'left' ); ?>><?php _e('Left',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'button_style' ); ?>"><?php _e( 'Choose Button Style', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'button_style' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'button_style' ); ?>">
				<option value="plain"<?php selected( $instance['button_style'], 'plain' ); ?>><?php _e('Plain Link',SDF_TXT_DOMAIN); ?></option>
				<option value=""<?php selected( $instance['button_style'], '' ); ?>><?php _e('Custom Styled',SDF_TXT_DOMAIN); ?></option>
			<?php foreach ( sdfGo()->_wpuGlobal->_sdfButtons as $btn_key => $btn_val) { ?>
				<option value="<?php echo $btn_key; ?>"<?php selected( $instance['button_style'], $btn_key ); ?>><?php echo $btn_val; ?></option>
			<?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'icon_size' ); ?>"><?php _e( 'Choose Icon Size', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'icon_size' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'icon_size' ); ?>">
				<option value=""<?php selected( $instance['icon_size'], '' ); ?>><?php _e('Default',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-lg"<?php selected( $instance['icon_size'], 'fa-lg' ); ?>><?php _e('Large',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-2x"<?php selected( $instance['icon_size'], 'fa-2x' ); ?>><?php _e('2X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-3x"<?php selected( $instance['icon_size'], 'fa-3x' ); ?>><?php _e('3X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-4x"<?php selected( $instance['icon_size'], 'fa-4x' ); ?>><?php _e('4X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-5x"<?php selected( $instance['icon_size'], 'fa-5x' ); ?>><?php _e('5X',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('icon-name'); ?>"><?php _e('Choose Social Media:', SDF_TXT_DOMAIN); ?></label>
				<ul class="list:icon-name categorychecklist">
				<?php foreach ( sdfGo()->_wpuGlobal->_sdfSocialSharingMedia as $soc_slug => $soc_name) { ?>
					<li class="popular-category">
					<?php
					$option = '<input type="checkbox" id="'.$this->get_field_id('icon-name').'" name="'.$this->get_field_name( 'icon_name' ).'[]" ';
					if (is_array($instance['icon_name'])) {
						foreach ($instance['icon_name'] as $icon_slug) {
							if($icon_slug == $soc_slug) {
								$option .= ' checked="checked"';
							}
						}
					}
					$option .= ' value="'.$soc_slug.'"/>  '.$soc_name;
					echo $option; ?>
					</li>
				<?php } ?>
				</ul>
		</p>
		
<?php
	} // End form()

} // End Class


add_action('widgets_init', create_function('', 'return register_widget("SDFSocShareIcons_Widget");'));
}