$(function(){ 
	$(document).ready(function() {
		$('#' + Widget_Photostream_Script_Params.uniquewid).firm_photostream({user: Widget_Photostream_Script_Params.user, limit: Widget_Photostream_Script_Params.limit, social_network: Widget_Photostream_Script_Params.social_network});
	
		setTimeout(function(){
		$('#' + Widget_Photostream_Script_Params.uniquewid).gridrotator({
		animType	: "fadeInOut",
		animSpeed	: 1000,
		interval	: 3500,
		step : 1,
		rows : Widget_Photostream_Script_Params.rows, 
		columns : Widget_Photostream_Script_Params.columns,
		w1024 : { rows : 2, columns : 6 },
		w768 : {rows : 2,columns : 5 },
		w480 : {rows : 2,columns : 4 },
		w320 : {rows : 2,columns : 3 },
		w240 : {rows : 2,columns : 2 },
		preventClick : false
		});
		}, 1500);
	});
});