<?php
/*
Plugin Name: SDF Photostream Widget
Description: Showing your photostream from Dribbble, Flickr, Pinterest, Instagram in the sidebar
Author: Brankic1979
Version: 1.3
Author URI: http://www.brankic1979.com/
Requires at least: 3.0
Tested up to: 3.4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Changed by FirmThemes.com
 */
if (!class_exists('SDF_Photostream_Widget'))
{
class SDF_Photostream_Widget extends WP_Widget
{
	function SDF_Photostream_Widget() {
		$widget_ops = array( 'description' 	=>		'Showing photostream from Dribbble, Flickr, Pinterest or Instagram' );
		parent::__construct(false, '<i></i>SDF Photostream', $widget_ops);
	}
	
	function widget( $args, $instance ) {
		extract ( $args, EXTR_SKIP );
        if (!isset($instance['title'])) $instance['title'] = "Photostream";
			$title = esc_attr($instance['title']);
        if (!isset($instance['user'])) $instance['user'] = "";  
			$user = esc_attr($instance['user']);
        if (!isset($instance['rows'])) $instance['rows'] = 1; 
			$rows = $instance['rows'];
		if (!isset($instance['columns'])) $instance['columns'] = 3; 
			$columns = $instance['columns']; 
		if (!isset($instance['social_network'])) $instance['social_network'] = "instagram";	
			$social_network = $instance['social_network']; 
			
		$unique_wid = $args['widget_id'];
		$limit = ceil ($rows * $columns * 2);
        
		// enqueue scripts and styles
		if ( is_active_widget(false, false, $this->id_base) )
		{
			wp_register_script( 'widget_script_image_grid', get_template_directory_uri() . '/lib/js/jquery.gridrotator.js', array(), '1.2.0', true);
			wp_enqueue_script( 'widget_script_image_grid' );
			
			wp_register_script( 'widget_script_photostream', get_template_directory_uri() . '/lib/widgets/photostream/photostream_widget.js', array(),'1.2.0',true);
			wp_enqueue_script( 'widget_script_photostream' );
			
			// inline script
			//wp_register_script( 'widget_script_photostream_inline', get_template_directory_uri() . '/lib/widgets/photostream/photostream_widget_inline.js', array(),'1.0.0',true);
			//wp_enqueue_script( 'widget_script_photostream_inline' );
			/*
			wp_enqueue_script('widget_script_photostream_inline', get_template_directory_uri() . '/lib/widgets/photostream/photostream_widget_inline.js', array(),'1.0.0',true);
			
			$widget_photostream_params = array(
			  'social_network' => $social_network,
			  'user' => $user,
			  'limit' => $limit,
			  'rows' => $rows,
			  'columns' => $columns,
			  'uniquewid' => $unique_wid
			);
			wp_localize_script( 'widget_script_photostream_inline', 'Widget_Photostream_Script_Params', $widget_photostream_params );
			*/
		}

		
		echo $before_widget;

		if ($title) {
			echo $before_title . $title . $after_title;
		}
		
		$output = '<div class="widget-photostream">
					<div class="image-grid"><img class="image-grid-loading-image" src="' . get_template_directory_uri() . '/lib/widgets/photostream/loading-grid.gif" alt="loading" /></div></div>';
		$output .= '<script type="text/javascript">
/* <![CDATA[ */ 
/*global jQuery:false */
(function($){ 
	"use strict";
	jQuery(document).ready(function($) {
		$("#'.$unique_wid.' .image-grid").firm_photostream({user: "'.$instance['user'].'", limit: "'.$limit.'", social_network: "'.$social_network.'"});
	
		setTimeout(function(){
		$("#'.$unique_wid.' .image-grid").gridrotator({
		animType	: "fadeInOut",
		animSpeed	: 1000,
		interval	: 3500,
		step : 1,
		rows : '.$rows.', 
		columns : '.$columns.',
		w1024 : { rows : '.$rows.', columns : '.$columns.' },
		w768 : {rows : '.$rows.',columns : '.$columns.' },
		w480 : {rows : '.$rows.',columns : '.$columns.' },
		w320 : {rows : 3,columns : 3 },
		w240 : {rows : 2,columns : 2 },
		preventClick : false
		});
		}, 4000);
	});
})(jQuery);
/* ]]> */ 
</script>';
		echo $output;

		echo $after_widget;
}
        	
	function form( $instance ) {
         
		if (!isset($instance['title'])) $instance['title'] = "Photostream";
			$title = esc_attr($instance['title']);
        if (!isset($instance['user'])) $instance['user'] = "";  
			$user = esc_attr($instance['user']);
        if (!isset($instance['rows'])) $instance['rows'] = 1; 
			$rows = $instance['rows'];
		if (!isset($instance['columns'])) $instance['columns'] = 3; 
			$columns = $instance['columns'];
		if (!isset($instance['social_network'])) $instance['social_network'] = "instagram";	
			$social_network = $instance['social_network']; 
         
        ?>

		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','warp'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('user'); ?>"><?php _e('Photostream user:','warp'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('user'); ?>"  value="<?php echo $user; ?>" class="widefat" id="<?php echo $this->get_field_id('user'); ?>" />
		</p>
        <p>
        <label for="<?php echo $this->get_field_id('social_network'); ?>"><?php _e('Social Network:','warp'); ?></label>
        <select name="<?php echo $this->get_field_name('social_network'); ?>" 
                  id="<?php echo $this->get_field_id('social_network'); ?>"
                  class="">
            <option value="dribbble" <?php if ($instance['social_network'] == "dribbble") echo 'selected="selected"' ?>><?php _e('Dribbble','warp'); ?></option>
            <option value="pinterest" <?php if ($instance['social_network'] == "pinterest") echo 'selected="selected"' ?>><?php _e('Pinterest','warp'); ?></option>
            <option value="flickr" <?php if ($instance['social_network'] == "flickr") echo 'selected="selected"' ?>><?php _e('Flickr','warp'); ?></option>
            <option value="instagram" <?php if ($instance['social_network'] == "instagram") echo 'selected="selected"' ?>><?php _e('Instagram','warp'); ?></option>
        </select>
        </p>
		<p>
			<label for="<?php echo $this->get_field_id('rows'); ?>"><?php _e('Rows:','warp'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('rows'); ?>"  value="<?php echo $rows; ?>" class="widefat" id="<?php echo $this->get_field_id('rows'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('columns'); ?>"><?php _e('Columns:','warp'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('columns'); ?>"  value="<?php echo $columns; ?>" class="widefat" id="<?php echo $this->get_field_id('columns'); ?>" />
		</p>

		<?php 
	}
	
}
	
function photostream_widget_init() {
	register_widget("SDF_Photostream_Widget");
}
add_action('widgets_init','photostream_widget_init');
}