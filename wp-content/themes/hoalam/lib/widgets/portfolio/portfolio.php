<?php
/*
	Portfolio Widget
*/


if (!function_exists('add_action')) {
  die('Please don\'t open this file directly!');
}


define('PF_WG_VER', '1.0');
require_once 'portfolio-widget.php';


class PFW {
  // hook everything up
   static function init() {
      if (is_admin()) {
        // check if minimal required WP version is used
        self::check_wp_version(3.5);

        // enqueue admin scripts
        add_action('admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue_scripts'));
      }
  } // init

  // initialize widgets
  static function widgets_init() {
    register_widget('Sdf_Portfolio_Widget');
  } // widgets_init



  // check if user has the minimal WP version required by the plugin
  static function check_wp_version($min_version) {
    if (!version_compare(get_bloginfo('version'), $min_version,  '>=')) {
        add_action('admin_notices', array(__CLASS__, 'min_version_error'));
    }
  } // check_wp_version


  // display error message if WP version is too low
  static function min_version_error() {
    echo '<div class="error"><p>' . sprintf('Google Maps Widget <b>requires WordPress version 3.3</b> or higher to function properly. You are using WordPress version %s. Please <a href="%s">update it</a>.', get_bloginfo('version'), admin_url('update-core.php')) . '</p></div>';
  } // min_version_error

    // enqueue CSS and JS scripts on widgets page
    static function admin_enqueue_scripts() {
      if (self::is_plugin_admin_page()) {
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('pfw-cookie', SDF_WIDGETS_URL . "/portfolio/js/jquery.cookie.js", array('jquery'), PF_WG_VER, true);
        wp_enqueue_script('pfw-admin', SDF_WIDGETS_URL . "/portfolio/js/pfw-admin.js", array('jquery'), PF_WG_VER, true);
        wp_enqueue_style('pfw-admin', SDF_WIDGETS_URL . "/portfolio/css/pfw-admin.css", array(), PF_WG_VER);
      } // if
    } // admin_enqueue_scripts


    // check if plugin's admin page is shown
    static function is_plugin_admin_page() {
      $current_screen = get_current_screen();

      if ($current_screen->id == 'widgets') {
        return true;
      } else {
        return false;
      }
    } // is_plugin_admin_page


    // helper function for creating dropdowns
    static function create_select_options($options, $selected = null, $output = true) {
        $out = "\n";

        foreach ($options as $tmp) {
            if ($selected == $tmp['val']) {
                $out .= "<option selected=\"selected\" value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            } else {
                $out .= "<option value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            }
        } // foreach

        if ($output) {
            echo $out;
        } else {
            return $out;
        }
    } // create_select_options


  // track a few things on plugin activation
  // NO DATA is sent anywhere!
  static function activate() {
    $options = get_option('pfw_options');

    if (!isset($options['first_version']) || !isset($options['first_install'])) {
      $options['first_version'] = PF_WG_VER;
      $options['first_install'] = current_time('timestamp');
      update_option('pfw_options', $options);
    }
  } // activate
} // class PFW


// hook everything up
register_activation_hook(__FILE__, array('PFW', 'activate'));
add_action('init', array('PFW', 'init'));
add_action('plugins_loaded', array('PFW', 'plugins_loaded'));
add_action('widgets_init', array('PFW', 'widgets_init'));