<?php

if (!class_exists('SDFSocIcons_Widget'))
{
class SDFSocIcons_Widget extends WP_Widget {

	public function __construct(){
		$widget_ops = array('classname' => 'sdf_social_follow_buttons_widget', 'description' => 'Display Your Company Social Links');
		parent::__construct('sdf-social-follow-widget', '<i></i>SDF Social Media Follow Buttons', $widget_ops);		
	}

	function widget($args, $instance) {  
		extract ( $args, EXTR_SKIP );
		
		$unique_wid = $args['widget_id'];
		
		if ( !isset($instance['title']) ) $instance['title'] = "Follow Us";
		$title = esc_attr($instance['title']);
		if ( !isset($instance['title_pos']) ) $instance['title_pos'] = "top";
		if ( !isset($instance['tooltip_pos']) ) $instance['tooltip_pos'] = "top";
		if ( !isset($instance['icon_name']) ) $instance['icon_name'] = array();
		if ( !isset($instance['icon_size']) ) $instance['icon_size'] = '';
		if ( !isset($instance['button_style']) ) $instance['button_style'] = '';
		// shortcode call
		if ( !is_array($instance['icon_name']) ) {
			$instance['icon_name'] = array_map('trim',explode(",",$instance['icon_name']));
		}

		echo $before_widget;
		$output = '';
		if ( ($instance['title_pos'] == 'top') && !empty($title) ) {
			$output .= $before_title . $title . $after_title;
		}
      
		$output .= '<ul id="'.$unique_wid.'" class="social-menu follow">';
		foreach ( sdfGo()->_wpuGlobal->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
			if (is_array($instance['icon_name']) & in_array($soc_slug, $instance['icon_name'])) {
				$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) );
				$soc_link = sdfGo()->sdf_get_global('social_media','social_media_'.$id);
				$icon_text = sdfGo()->sdf_get_global('typography','theme_'.$id.'_follow_button_custom_text');
				$toggle_icon = sdfGo()->sdf_get_global('typography','theme_follow_button_icon_toggle');
				$soc_icon = ($toggle_icon) ? '<i class="fa fa-'.$soc_slug.' '.$instance['icon_size'].'"></i>' : '';
				$button_style = ($instance['button_style'] == '') ? ' follow-custom follow-'.$soc_slug : (($instance['button_style'] == 'plain') ? '' : ' btn btn-'.$instance['button_style']);
				$li_style = ($instance['button_style'] == '') ? 'follow-icon' : 'follow-btn';
				
				$output .= '<li class="'.$li_style.'">
						<a class="'.$button_style.' social-tooltip" data-original-title="'.$soc_name.'" data-placement="'.$instance['tooltip_pos'].'" data-toggle="tooltip" href="'.$soc_link.'" target="_blank" rel="nofollow">'.$soc_icon.' '.$icon_text.'</a>
						</li>';
			}
		}
		$output .= '</ul>';
		echo $output;
		if ( $instance['title_pos'] == 'bottom' ) {
			echo $before_title . $title . $after_title;
		}
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
	
		if (!isset($instance['title'])) $instance['title'] = "Follow Us";
		$title = esc_attr($instance['title']);
		if (!isset($instance['title_pos'])) $instance['title_pos'] = "top";
		if (!isset($instance['tooltip_pos'])) $instance['tooltip_pos'] = "top";
		if (!isset($instance['icon_name']) ) $instance['icon_name'] = array();
		if ( !isset($instance['icon_size']) ) $instance['icon_size'] = '';
		if ( !isset($instance['button_style']) ) $instance['button_style'] = '';

		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'title_pos' ); ?>"><?php _e( 'Choose Title Position', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'title_pos' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'title_pos' ); ?>">
				<option value="top"<?php selected( $instance['title_pos'], 'top' ); ?>><?php _e('Top',SDF_TXT_DOMAIN); ?></option>
				<option value="bottom"<?php selected( $instance['title_pos'], 'bottom' ); ?>><?php _e('Bottom',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tooltip_pos' ); ?>"><?php _e( 'Choose Tooltip Position', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'tooltip_pos' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'tooltip_pos' ); ?>">
				<option value="top"<?php selected( $instance['tooltip_pos'], 'top' ); ?>><?php _e('Top',SDF_TXT_DOMAIN); ?></option>
				<option value="right"<?php selected( $instance['tooltip_pos'], 'right' ); ?>><?php _e('Right',SDF_TXT_DOMAIN); ?></option>
				<option value="bottom"<?php selected( $instance['tooltip_pos'], 'bottom' ); ?>><?php _e('Bottom',SDF_TXT_DOMAIN); ?></option>
				<option value="left"<?php selected( $instance['tooltip_pos'], 'left' ); ?>><?php _e('Left',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'button_style' ); ?>"><?php _e( 'Choose Button Style', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'button_style' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'button_style' ); ?>">
				<option value="plain"<?php selected( $instance['button_style'], 'plain' ); ?>><?php _e('Plain Link',SDF_TXT_DOMAIN); ?></option>
				<option value=""<?php selected( $instance['button_style'], '' ); ?>><?php _e('Custom Styled',SDF_TXT_DOMAIN); ?></option>
			<?php foreach ( sdfGo()->_wpuGlobal->_sdfButtons as $btn_key => $btn_val) { ?>
				<option value="<?php echo $btn_key; ?>"<?php selected( $instance['button_style'], $btn_key ); ?>><?php echo $btn_val; ?></option>
			<?php } ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'icon_size' ); ?>"><?php _e( 'Choose Icon Size', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'icon_size' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'icon_size' ); ?>">
				<option value=""<?php selected( $instance['icon_size'], '' ); ?>><?php _e('Default',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-lg"<?php selected( $instance['icon_size'], 'fa-lg' ); ?>><?php _e('Large',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-2x"<?php selected( $instance['icon_size'], 'fa-2x' ); ?>><?php _e('2X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-3x"<?php selected( $instance['icon_size'], 'fa-3x' ); ?>><?php _e('3X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-4x"<?php selected( $instance['icon_size'], 'fa-4x' ); ?>><?php _e('4X',SDF_TXT_DOMAIN); ?></option>
				<option value="fa-5x"<?php selected( $instance['icon_size'], 'fa-5x' ); ?>><?php _e('5X',SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('icon-name'); ?>"><?php _e('Choose Social Media:', SDF_TXT_DOMAIN); ?></label>
				<ul class="list:icon-name categorychecklist">
				<?php foreach ( sdfGo()->_wpuGlobal->_sdfSocialFollowMedia as $soc_slug => $soc_name) { ?>
					<li class="popular-category">
					<?php
					$option = '<input type="checkbox" id="'.$this->get_field_id('icon-name').'" name="'.$this->get_field_name( 'icon_name' ).'[]" ';
					if (is_array($instance['icon_name'])) {
						foreach ($instance['icon_name'] as $icon_slug) {
							if($icon_slug == $soc_slug) {
								$option .= ' checked="checked"';
							}
						}
					}
					$option .= ' value="'.$soc_slug.'"/>  '.$soc_name;
					echo $option; ?>
					</li>
				<?php } ?>
				</ul>
		</p>
		
<?php
	} // End form()

} // End Class


add_action('widgets_init', create_function('', 'return register_widget("SDFSocIcons_Widget");'));
}