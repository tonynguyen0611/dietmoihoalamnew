<?php
/*
 *
 * Widget Contact Form
 *
 * 
 */
if (!class_exists('Sdf_Contact_Form_Widget'))
{ 
class Sdf_Contact_Form_Widget extends WP_Widget {

	function Sdf_Contact_Form_Widget() {
		$widget_ops = array('description' => 'Display Your Contact Form');
		parent::__construct(false, '<i></i>SDF Contact Form', $widget_ops);      
	}

	function widget($args, $instance) {  
		
		global $wpdb, $wp_session;
		
		extract ( $args, EXTR_SKIP );

		if ( !isset($instance['title']) ) $instance['title'] = "";
		$title = esc_attr($instance['title']); 
		if ( !isset($instance['layout']) ) $instance['layout'] = "stacked";
		if ( !isset($instance['sc_contact']) ) $instance['sc_contact'] = false;
		if ( !isset($instance['toggle_captcha']) ) $instance['toggle_captcha'] = false; 
		$toggle_captcha = filter_var( $instance['toggle_captcha'], FILTER_VALIDATE_BOOLEAN );
		if ( !isset($instance['toggle_confirmation']) ) $instance['toggle_confirmation'] = false; 
		$toggle_confirmation = filter_var( $instance['toggle_confirmation'], FILTER_VALIDATE_BOOLEAN );
		if ( !isset($instance['recipient_email']) ) $instance['recipient_email'] = "";
		if ( !isset($instance['confirmation_subject']) ) $instance['confirmation_subject'] = "";
		if ( !isset($instance['confirmation_message']) ) $instance['confirmation_message'] = "";
		
		$form_layout = ($instance['layout'] == 'stacked') ? '12' : '4';

		echo $before_widget;

		if ( !empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}
		
		$wp_session['sdf_contact_form'] = $instance;
		
		$wp_session['sdf_contact_form']['s1'] = rand(1,9);
		$wp_session['sdf_contact_form']['s2'] = rand(1,9);
		$unique_wid = $args['widget_id'];			
		// Tokens
		$unique_str = md5(uniqid(rand(), TRUE));
		$str_unique = strrev($unique_str);

		?>
<form role="form" name="sdf_contact_form" id="sdf_contact_form" action="" method="post">
<input type="hidden" name="unique_str" value="<?php echo $unique_str; ?>"><input type="hidden" name="str_unique" value="<?php echo $str_unique; ?>"><input type="hidden" name="unique_wid" value="<?php echo $unique_wid; ?>"><input type="hidden" name="sc_contact" value="<?php echo $instance['sc_contact']; ?>"><div class="row">
<div class="col-md-12 hide" id="contact_sending_status">
<div class="form-group">
<div class="alert alert-success hide alert-dismissable"> <?php _e('You have successfully read this important alert message.', SDF_TXT_DOMAIN); ?> </div>
<div class="alert alert-danger hide alert-dismissable"> <?php _e('Change a few things up and try submitting again.', SDF_TXT_DOMAIN); ?> </div>
</div>
</div>
<div class="col-md-<?php echo $form_layout; ?>">
<div class="form-group">
<input type="text" class="form-control" name="contact_name" id="contact_name" placeholder="<?php _e('Your Name', SDF_TXT_DOMAIN); ?>">
</div>
</div>
<div class="col-md-<?php echo $form_layout; ?>">
<div class="form-group">
<input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="<?php _e('Your Email', SDF_TXT_DOMAIN); ?>">
</div>
</div>
<div class="col-md-<?php echo $form_layout; ?>">
<div class="form-group">
<input type="text" class="form-control" name="contact_subject" id="contact_subject" placeholder="<?php _e('Message Subject', SDF_TXT_DOMAIN); ?>">
</div>
</div>
<div class="col-md-12">
<div class="form-group">
<textarea rows="6" class="form-control" name="contact_message" id="contact_message" placeholder="<?php _e('Message', SDF_TXT_DOMAIN); ?>"></textarea>
</div>
</div>
<?php 
if($toggle_captcha == true){
?>
<div class="col-md-12">
<div class="form-group"><pre><?php _e('Write Your answer:', SDF_TXT_DOMAIN); ?> <span id="s1"><?php echo $wp_session['sdf_contact_form']['s1'];?></span> + <span id="s2"><?php echo $wp_session['sdf_contact_form']['s2'];?></span> =</pre><input type="text" name="captcha_answer" id="captcha_answer" class="form-control" placeholder="<?php _e('Answer', SDF_TXT_DOMAIN); ?>"></div>
</div>
<?php
}
?>
<div class="col-md-12">
<div class="form-group">
<button type="submit" name="contact_submit" id="contact_submit" class="btn btn-primary"><?php _e('Send Message', SDF_TXT_DOMAIN); ?></button>
</div>
</div>
</div>
</form>
		<?php
			
		echo $after_widget;

	}

	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	function form($instance) { 
		
		if ( !isset($instance['title']) ) $instance['title'] = "";
		$title = esc_attr($instance['title']); 
		if ( !isset($instance['layout']) ) $instance['layout'] = "stacked";
		if ( !isset($instance['toggle_captcha']) ) $instance['toggle_captcha'] = false; 
		$instance['toggle_captcha'] = filter_var( $instance['toggle_captcha'], FILTER_VALIDATE_BOOLEAN );
		if ( !isset($instance['toggle_confirmation']) ) $instance['toggle_confirmation'] = false; 
		$instance['toggle_confirmation'] = filter_var( $instance['toggle_confirmation'], FILTER_VALIDATE_BOOLEAN );
		if ( !isset($instance['recipient_email']) ) $instance['recipient_email'] = "";
		if ( !isset($instance['confirmation_subject']) ) $instance['confirmation_subject'] = "";
		if ( !isset($instance['confirmation_message']) ) $instance['confirmation_message'] = "";

		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ); ?>"><?php _e( 'Choose Form Layout:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'layout' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'layout' ); ?>">
				<option value="stacked"<?php selected( $instance['layout'], 'stacked' ); ?>><?php _e('Stacked Inputs', SDF_TXT_DOMAIN); ?></option>
				<option value="inline"<?php selected( $instance['layout'], 'inline' ); ?>><?php _e('Inline Inputs', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('recipient_email'); ?>"><?php _e('Recipient Email:', SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('recipient_email'); ?>"  value="<?php echo $instance['recipient_email']; ?>" class="widefat" id="<?php echo $this->get_field_id('recipient_email'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('toggle_captcha'); ?>"><?php _e('Enable Captcha:', SDF_TXT_DOMAIN); ?></label>
			<input type="checkbox" name="<?php echo $this->get_field_name('toggle_captcha'); ?>" <?php echo $instance['toggle_captcha'] === true ? ' checked="checked"' : ''?> value="true" class="widefat" id="<?php echo $this->get_field_id('toggle_captcha'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('toggle_confirmation'); ?>"><?php _e('Enable Confirmation Email:', SDF_TXT_DOMAIN); ?></label>
			<input type="checkbox" name="<?php echo $this->get_field_name('toggle_confirmation'); ?>"  value="true" <?php echo $instance['toggle_confirmation'] === true ? ' checked="checked"' : ''?> class="widefat" id="<?php echo $this->get_field_id('toggle_confirmation'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('confirmation_subject'); ?>"><?php _e('Confirmation Subject:', SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('confirmation_subject'); ?>"  value="<?php echo $instance['confirmation_subject']; ?>" class="widefat" id="<?php echo $this->get_field_id('confirmation_subject'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('confirmation_message'); ?>"><?php _e('Confirmation Message:', SDF_TXT_DOMAIN); ?></label>
			<textarea name="<?php echo $this->get_field_name('confirmation_message'); ?>" class="widefat" id="<?php echo $this->get_field_id('confirmation_message'); ?>"><?php echo $instance['confirmation_message']; ?></textarea>
		</p>
		
		
		
<?php 
	} // End form()

} // End Class 
}
	function contact_form_widget_init() {
		register_widget("Sdf_Contact_Form_Widget");
	}
	add_action('widgets_init','contact_form_widget_init');

	function sdf_register_sources_contact_form() {
	
		if (!is_admin()) {
		// let users change the session cookie name
		if( ! defined( 'WP_SESSION_COOKIE' ) )
			define( 'WP_SESSION_COOKIE', '_wp_session' );

		if ( ! class_exists( 'Recursive_ArrayAccess' ) ) {
			require_once( SDF_WIDGETS . '/contact-form/class-recursive-arrayaccess.php' );
		}

		// Only include the functionality if it's not pre-defined.
		if ( ! class_exists( 'WP_Session' ) ) {
			require_once( SDF_WIDGETS . '/contact-form/class-wp-session.php' );
			require_once( SDF_WIDGETS . '/contact-form/wp-session.php' );
		}
		global $wp_session;
		$wp_session = WP_Session::get_instance();
		
		wp_register_script('sdf_contact_js', SDF_WIDGETS_URL.'/contact-form/js/contact-form.js', array('jquery'), '1.0.0', true);
		wp_enqueue_script('sdf_contact_js');
		// pass data to sdf_contact_js
		// pass submit ajaxurl
		$nonce_contact_form = wp_create_nonce("nonce_contact_form");	
		$sdf_js_localized_data = array(
			'contact_submit_url' => admin_url('admin-ajax.php').'?action=contact_form_submit&nonce='.$nonce_contact_form
		);
		sdf_pass_data_to_scripts( $sdf_js_localized_data, 'sdf_contact_js' );
		}
	}		
	add_action('init', 'sdf_register_sources_contact_form');

	function sdf_contact_form_submit() {
	
		global $wp_session, $wp_registered_widgets;
		
		if ( empty($_POST) || !wp_verify_nonce( $_REQUEST['nonce'], "nonce_contact_form")) {
			exit('No naughty business please');
		} 	

		$success_msg = __('Your Message has been sent. Thank You!', SDF_TXT_DOMAIN);
		$error_msg = '';
		$error_ids = '';
		$error = 0;	
		$is_sent = false;

		// process form data
		$contact_name = trim($_POST['contact_name']);
		$contact_email = trim($_POST['contact_email']);
		$contact_subject = trim($_POST['contact_subject']);
		$contact_message = trim($_POST['contact_message']);
		$captcha_answer = trim($_POST['captcha_answer']);
		$unique_wid = trim($_POST['unique_wid']);
		$sc_contact = trim($_POST['sc_contact']);
		// verify Tokens
		if(empty($_POST['unique_str']) || $_POST['unique_str'] != strrev($_POST['str_unique'])){
		 exit('Oops! An Error Occurred.');
		}	
		
		// get widget instance data
		if($sc_contact){
			$instance = $wp_session['sdf_contact_form'];
		}
		else{
			$wg_object = $wp_registered_widgets[$unique_wid];
			$wg_count = $wg_object['params'][0]['number'];
			$wg_option = get_option($wg_object['callback'][0]->option_name);
			$instance = $wg_option[$wg_count];
		}
		
		if(empty($contact_name)) {
			$error_msg .= __('Please enter your name', SDF_TXT_DOMAIN);
			$error_ids .= '#contact_name';
			$error++;
		}
		if(empty($contact_email)) {
			$error_msg .= ($error > 0) ? '<br>' : '';
			$error_ids .= ($error > 0) ? ', #contact_email' : '#contact_email';
			$error_msg .= __('Please enter your email', SDF_TXT_DOMAIN);
			$error++;
		}
		else if(!preg_match("!^\w[\w|\.|\-]+@\w[\w|\.|\-]+\.[a-zA-Z]{2,4}$!", urldecode($contact_email))) {
			$error_msg .= ($error > 0) ? '<br>' : '';
			$error_ids .= ($error > 0) ? ', #contact_email' : '#contact_email';
			$error_msg .= __('Please enter valid email address', SDF_TXT_DOMAIN);
			$error++;
		}
		if(empty($contact_subject)) {
			$error_msg .= ($error > 0) ? '<br>' : '';
			$error_ids .= ($error > 0) ? ', #contact_subject' : '#contact_subject';
			$error_msg .= __('Please enter your message subject', SDF_TXT_DOMAIN);
			$error++;
		}
		if(empty($contact_message)) {
			$error_msg .= ($error > 0) ? '<br>' : '';
			$error_ids .= ($error > 0) ? ', #contact_message' : '#contact_message';
			$error_msg .= __('Please enter your message', SDF_TXT_DOMAIN);
			$error++;
		}
		if($instance['toggle_captcha']){
			if($captcha_answer != $wp_session['sdf_contact_form']['s1'] + $wp_session['sdf_contact_form']['s2'] ){
				$error_msg .= ($error > 0) ? '<br>' : '';
			  $error_ids .= ($error > 0) ? ', #captcha_answer' : '#captcha_answer';
				$error_msg .= __('Invalid Captcha Value', SDF_TXT_DOMAIN);
				$error++;
			}
		}

		if(!$error) {
			$headers  = 'From: '.$contact_name.' <'.strip_tags($contact_email).'>' . "\r\n";
			$headers .= 'Reply-To: '.$contact_name.' <'.strip_tags($contact_email).'>' . "\r\n";
			$headers .= 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

			$message  = 'Hello Sir/Madam, '.'<br/>';
			$message.= 'I am '.$contact_name.'<br/>';
			$message.= $contact_message;

			$is_sent = mail($instance['recipient_email'], $contact_subject, $message, $headers);

			if(isset($instance['toggle_confirmation'])){
				if($instance['toggle_confirmation']){
					$headers  = "From: ".urldecode(get_bloginfo('name'))." <".strip_tags($instance['recipient_email'])."> \r\n";
					$headers .= 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$message = 'Hello, '.$contact_name.'<br/><br/>';
					$message.= urldecode($instance['confirmation_message']);
					$reply_sub = urldecode($instance['confirmation_subject']);
					mail($contact_email, $reply_sub, $message, $headers);
				}
			}
		}
		//$wp_session['sdf_contact_form']['s1'] = rand(1,9);
		//$wp_session['sdf_contact_form']['s2'] = rand(1,9);
		$result = array("is_sent" => $is_sent, "success_msg" => $success_msg, "error_msg" => $error_msg, "error_ids" => $error_ids, "s1" => $wp_session['sdf_contact_form']['s1'], "s2" => $wp_session['sdf_contact_form']['s2']);

		die (json_encode($result));
	}
	add_action('wp_ajax_contact_form_submit', 'sdf_contact_form_submit');
