<?php
if (!class_exists('Archive_Category_Widget'))
{
add_action('widgets_init', create_function('', 'return register_widget("Archive_Category_Widget");'));

class Archive_Category_Widget extends WP_Widget {

	function Archive_Category_Widget() {
		$widget_ops = array('classname' => SDF_TXT_DOMAIN, 'description' => __('Lists the archives for selected categories', SDF_TXT_DOMAIN) );
		parent::__construct('category_archive', __('<i></i>SDF Category Archives', SDF_TXT_DOMAIN), $widget_ops);
	}
	
	function customarchives_where( $x ) {

		global $wpdb;

		$s = $x;

		$s =  $s . " AND $wpdb->posts.ID IN ";

		$s = $s . "(";
		$s = $s . "SELECT $wpdb->posts.ID FROM $wpdb->posts INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) WHERE $wpdb->term_taxonomy.taxonomy = 'category'";

		$comma_separated = implode(",", $_SESSION['category_ids']);
		//$comma_separated = '11,14';

		$s = $s . " AND $wpdb->term_taxonomy.term_id IN ($comma_separated)";
		$s = $s . ")";

		return $s;

	}
	

	function widget($args, $instance) {
	
		extract($args, EXTR_SKIP);

		$category_ids = isset($instance['category_ids']) ? $instance['category_ids'] : array('1');
		$show_posts_count = isset($instance['show_posts_count']) ? $instance['show_posts_count'] : 0;
		$excluded_categories = isset($instance['excluded_categories']) ? $instance['excluded_categories'] : '';

		$title = apply_filters('widget_title', isset($instance['title'] ) ?  $instance['title'] :__('Archives', SDF_TXT_DOMAIN), $instance, $this->id_base);
		
		$_SESSION['category_ids'] = $category_ids;
		add_filter( 'getarchives_where', array($this, 'customarchives_where') );
		
		echo $before_widget;

		echo $before_title.$title.$after_title;

		echo '<ul>';
			wp_get_archives(apply_filters('widget_archives_args', array('type' => 'monthly', 'show_post_count' => $show_posts_count)));
		echo '</ul>';
		
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {

		$instance = $old_instance;

		$instance['title'] = trim(strip_tags($new_instance['title']));
		$instance['category_ids'] = $new_instance['category_ids'];
		$instance['show_posts_count'] = (int) $new_instance['show_posts_count'];
		$instance['excluded_categories'] = trim(strip_tags($new_instance['excluded_categories']));

		return $instance;
	}

	function form($instance) {

		$title = (isset($instance['title'])) ? esc_attr($instance['title']) : __('Archives', SDF_TXT_DOMAIN);
		$category_ids = (isset($instance['category_ids'])) ?  $instance['category_ids']: array('1');
		$show_posts_count = (isset($instance['show_posts_count'])) ? (int) $instance['show_posts_count'] : 0;
		$hide_empty_categories = (isset($instance['hide_empty_categories'])) ? (int) $instance['hide_empty_categories'] : 0;
		$excluded_categories = (isset($instance['excluded_categories'])) ? esc_attr($instance['excluded_categories']) : '';

		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SDF_TXT_DOMAIN); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" type="text" class="widefat" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('category_ids'); ?>"><?php _e('Parent Category:', SDF_TXT_DOMAIN); ?></label>
			<select id="<?php echo $this->get_field_id('category_ids'); ?>" name="<?php echo $this->get_field_name('category_ids'); ?>[]" size="5" multiple="multiple">
				<?php
					$categories = get_categories(array('hide_empty' => 0, 'parent' => 0));
					foreach ($categories as $cat) {
						if ( in_array( $cat->cat_ID, $category_ids ) !== FALSE ) {
							$option = '<option selected="selected" value="' . $cat->cat_ID . '">';
						} else {
							$option = '<option value="' . $cat->cat_ID . '">';
						}
						$option .= $cat->cat_name . '</option>';
						echo $option;
					}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('show_posts_count'); ?>"><?php _e('Show Posts Count', SDF_TXT_DOMAIN); ?></label>
			<input id="<?php echo $this->get_field_id('show_posts_count'); ?>" name="<?php echo $this->get_field_name('show_posts_count'); ?>" type="checkbox" value="1" <?php if ($show_posts_count) echo 'checked="checked"'; ?>/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'excluded_categories' ); ?>"> <?php _e('Categories to exclude (comma separated IDs):'); ?> </label>
			<input id="<?php echo $this->get_field_id( 'excluded_categories' ); ?>" name="<?php echo $this->get_field_name( 'excluded_categories' ); ?>" value="<?php echo $excluded_categories; ?>"/>
		</p>
		<?php
	}
}
}