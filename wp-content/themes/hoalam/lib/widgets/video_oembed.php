<?php
/*
 *
 * Widget oEmbed
 * 
 */
if (!class_exists('SDF_Video_oEmbed_Widget'))
{
class SDF_Video_oEmbed_Widget extends WP_Widget {

	function SDF_Video_oEmbed_Widget() {
		$widget_ops = array('description' => 'Embed by URL (videos, images, tweets, audio, and other) content into your site');
		parent::__construct(false, '<i></i>SDF Video/oEmbed', $widget_ops);      
	}

	function widget($args, $instance) {  
		
		global $wpdb;
		
		extract ( $args, EXTR_SKIP );

		if (!isset($instance['title'])) $instance['title'] = ""; 
		$title = $instance['title'];
		if (!isset($instance['video_oembed_url'])) $instance['video_oembed_url'] = ""; 
		$video_oembed_url = $instance['video_oembed_url'];
		if (!isset($instance['video_oembed_aspect_ratio'])) $instance['video_oembed_aspect_ratio'] = "16by9"; 
		$video_oembed_aspect_ratio = $instance['video_oembed_aspect_ratio'];
		if (!isset($instance['description'])) $instance['description'] = ""; 
		$description = $instance['description'];
		
		echo $before_widget;

		if ($title) {
			echo $before_title . $title . $after_title;
		}
		if ($description) {
			echo '<p>' . $description . '</p>';
		}

	if ( $video_oembed_url == '' ) {
		return null;
	}

	$output = '';

	$vid_width = 1140;
	$vid_ratio = ($video_oembed_aspect_ratio == '16by9') ? 1.77 : 1.33;
	$vid_height = $vid_width / $vid_ratio;
	
	global $wp_embed;
	$embed = $wp_embed->run_shortcode( '[embed width="' . $vid_width . '" height="' . $vid_height . '"]' . $video_oembed_url . '[/embed]' );

	$output .= '<div class="oembed-container"><div class="embed-responsive embed-responsive-'.$video_oembed_aspect_ratio.'">' . $embed . '</div></div>';

	echo $output;
		
	echo $after_widget;

	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
		if (!isset($instance['title'])) $instance['title'] = "";
		$title = esc_attr($instance['title']);
		if (!isset($instance['video_oembed_url'])) $instance['video_oembed_url'] = ""; 
		$video_oembed_url = $instance['video_oembed_url'];
		if (!isset($instance['video_oembed_aspect_ratio'])) $instance['video_oembed_aspect_ratio'] = "16by9"; 
		$video_oembed_aspect_ratio = $instance['video_oembed_aspect_ratio'];
		if (!isset($instance['description'])) $instance['description'] = ""; 
		$description = $instance['description'];
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p class="description">*use full Video/oEmbed URL. Check about supported formats at <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">WordPress Codex Page</a></p>
		<p>
			<label for="<?php echo $this->get_field_id('video_oembed_url'); ?>"><?php _e('Video/oEmbed URL:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('video_oembed_url'); ?>" value="<?php echo $video_oembed_url; ?>" class="widefat" id="<?php echo $this->get_field_id('video_oembed_url'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'video_oembed_aspect_ratio' ); ?>"><?php _e( 'Video/oEmbed Aspect Ratio:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'video_oembed_aspect_ratio' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'video_oembed_aspect_ratio' ); ?>">
				<option value="16by9"<?php selected( $video_oembed_aspect_ratio, '16by9' ); ?>>16:9</option>
				<option value="4by3"<?php selected( $video_oembed_aspect_ratio, '4by3' ); ?>>4:3</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description:',SDF_TXT_DOMAIN); ?></label>
			<textarea id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" class="widefat" rows="2" cols="20"><?php echo $description; ?></textarea>
		</p>
<?php
	} // End form()

} // End Class

function oembed_widget_init() {
	register_widget("SDF_Video_oEmbed_Widget");
}
add_action('widgets_init','oembed_widget_init');
}