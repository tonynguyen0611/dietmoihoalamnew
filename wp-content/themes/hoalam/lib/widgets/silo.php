<?php
/*
 *
 * Widget V_Advanced_Silo
 * 
 */
if (!class_exists('V_Advanced_Silo'))
{ 
class V_Advanced_Silo extends WP_Widget
{

	public function V_Advanced_Silo() {
		$widget_ops = array('description' => 'Display your Silos');
		parent::__construct(false, '<i></i>SDF Silo Builder', $widget_ops);      
	}
	
	/**
	 * Front-end display of widget
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget($args, $instance)
	{
		$title = apply_filters('widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base);
		
		$home_title = empty( $instance['home_title'] ) ? 'Home' : $instance['home_title'];
		
		if (empty($instance['number']) || ! $number = absint($instance['number']))
 			$number = 10;
		
		$show_thumbnails = 0;
		
		echo $args['before_widget'];
		if ($title) echo $args['before_title'] . $title . $args['after_title'];
		if(displaySilo(false, true, $number, $show_thumbnails, $home_title)) echo displaySilo(false, true, $number, $show_thumbnails, $home_title);
		echo $args['after_widget'];
	}
	
	/**
	 * Back-end widget form
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form($instance)
	{
		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$home_title = isset($instance['home_title']) ? esc_attr($instance['home_title']) : 'Home';
		$number = isset($instance['number']) ? absint($instance['number']) : 10;
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('home_title'); ?>"><?php _e('Home Page Title:'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('home_title'); ?>" name="<?php echo $this->get_field_name('home_title'); ?>" type="text" value="<?php echo $home_title; ?>" /></p>
		
		<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of links to show:'); ?></label>
		<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
		<?php
	}
	
	/**
	 * Sanitize widget form values as they are saved
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['home_title'] = strip_tags($new_instance['home_title']);
		$instance['number'] = absint($new_instance['number']);
		
		return $instance;
	}
}

function silo_widget_init() {
	register_widget("V_Advanced_Silo");
}
add_action('widgets_init','silo_widget_init');
}