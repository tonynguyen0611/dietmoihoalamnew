<?php
/*
 * SDF Latest Posts
 * Widget definition, admin GUI and front-end functions
 */


// include only file
if (!defined('ABSPATH')) {
  die();
}

if (!class_exists('Sdf_Latest_Posts_Widget'))
{
class Sdf_Latest_Posts_Widget extends WP_Widget {
	
	function Sdf_Latest_Posts_Widget() {
		$widget_ops = array('classname' => 'latest-posts-widget', 'description' => __('Display Your Latest Posts available in Grid or List Layout.', 'latest-posts-widget'));
		parent::__construct(false, __('<i></i>SDF Latest Posts', 'latest-posts-widget'), $widget_ops);      
	}

  function form($instance) {
    $instance = wp_parse_args((array) $instance,
                              array('title' => __('Latest Posts', 'latest-posts-widget'),
                                    'lpw_layout_type' => 'list',
                                    'grid_post_cats' => array(),
                                    'list_post_cats' => array(),
                                    'rows' => '1',
                                    'columns' => '3',
                                    'grid_layout' => 'grid_default',
                                    'posts_filter' => 'yes',
                                    'list_item_title_heading' => 'h4',
                                    'grid_item_title_heading' => 'h4',
                                    'grid_title_position' => 'above',
                                    'list_item_image_position' => 'top',
                                    'grid_info_display' => 'hide-all',
                                    'list_info_display' => 'show-info',
                                    'show_item_link_button' => 'yes',
                                    'show_prettyphoto_button' => 'yes',
                                    'show_custom_button' => 'no',
                                    'custom_button_icon' => '',
                                    'custom_button_text' => '',
                                    'custom_button_link' => '',
                                    'custom_button_link_target' => '',
                                    'grid_thumbnails_shape' => 'rectangle',
                                    'grid_thumbnails_size' => 'sdf-image-md-12',
                                    'grid_entrance_animation' => 'No',
                                    'grid_entrance_animation_duration' => 'animated',
                                    'list_thumbnails_shape' => 'rectangle',
                                    'list_thumbnails_size' => 'sdf-image-md-12',
                                    'list_entrance_animation' => 'No',
                                    'list_entrance_animation_duration' => 'animated',
                                    'posts_count' => '5',
                                    'list_word_count' => '15',
                                    'grid_word_count' => '15',
                                    'list_posts_offset' => '0',
                                    'grid_posts_offset' => '0',
                                    'show_images' => 'yes'));

		
		$entranceAnimations = sdfGo()->_wpuGlobal->_ultEntranceAnimations['animation_types'];
		$entranceAnimationDuration = sdfGo()->_wpuGlobal->_ultEntranceAnimations['animation_duration'];																
		$faIcons = sdfGo()->_wpuGlobal->_ultIcons;		
		$linkTargets = array( 'None' => '', 'Same window' => '_self', 'New window' => '_blank' );	
		$imageSizes = array_flip(sdfGo()->_wpuGlobal->_ultImages['sizes']);		
																		
    $title = esc_attr($instance['title']);
    $lpw_layout_type = $instance['lpw_layout_type'];
		$rows = $instance['rows']; 
		$columns = $instance['columns'];
		$posts_count = $instance['posts_count'];

    if(!$lpw_layout_type) {
      $lpw_layout_type = 'list';
    }

   

    echo '<p><label for="' . $this->get_field_id('title') . '">' . __('Title', 'latest-posts-widget') . ':</label><input class="widefat" id="' . $this->get_field_id('title') . '" name="' . $this->get_field_name('title') . '" type="text" value="' . $title . '" /></p>';
		
		echo '<input class="regular-text" id="' . $this->get_field_id('lpw_layout_type') . '" name="' . $this->get_field_name('lpw_layout_type') . '" type="hidden" value="' . esc_attr($lpw_layout_type) . '" />';

    echo '<div class="lpw-tabs" id="tab-' . $this->id . '"><ul><li><a href="#lpw-list">' . __('List Layout', 'latest-posts-widget') . '</a></li><li><a href="#lpw-grid">' . __('Grid Layout', 'latest-posts-widget') . '</a></li></ul>';
    echo '<div id="lpw-list">';
		 ?>
		<p>
			<label for="<?php echo $this->get_field_id('list_post_cats'); ?>"><?php _e('Choose Categories:', SDF_TXT_DOMAIN); ?></label>
			<?php $categories = get_terms('category', array( 'hide_empty' => 0 ));
						
					if(is_array( $categories ) && !empty( $categories )) { ?>
						<ul class="list:category categorychecklist">
						<?php foreach ( $categories as $term ) { ?>
					
							<li class="popular-category">
							<?php
							$option = '<input type="checkbox" id="'.$this->get_field_id('list_post_cats').'" name="'.$this->get_field_name( 'list_post_cats' ).'[]" ';
							
							if (is_array($instance['list_post_cats'])) {
								foreach ($instance['list_post_cats'] as $cats) {
									if($cats == $term->slug) {
										$option .= ' checked="checked"';
									}
								}
							}
							$option .= ' value="'.$term->slug.'"/>  '.$term->name;
							
							echo $option; ?>
							</li>
						
						<?php } ?>
						</ul>
					<?php } else {
						echo '<div class="alert alert-error">' . _e( 'You haven\'t set any Posts Categories!', SDF_TXT_DOMAIN ) . '</div>'; 
					} ?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('posts_count'); ?>"><?php _e('Posts Count:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('posts_count'); ?>"  value="<?php echo $posts_count; ?>" class="widefat" id="<?php echo $this->get_field_id('posts_count'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_info_display' ); ?>"><?php _e( 'Info Display:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_info_display' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_info_display' ); ?>">
				<option value="show-title"<?php selected( $instance['list_info_display'], 'show-title' ); ?>><?php _e('Show Title', SDF_TXT_DOMAIN); ?></option>
				<option value="show-title-excerpt"<?php selected( $instance['list_info_display'], 'show-title-excerpt' ); ?>><?php _e('Show Title and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-info"<?php selected( $instance['list_info_display'], 'show-info' ); ?>><?php _e('Show Title and Meta Data', SDF_TXT_DOMAIN); ?></option>
				<option value="show-meta-excerpt"<?php selected( $instance['list_info_display'], 'show-meta-excerpt' ); ?>><?php _e('Show Meta Data and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-excerpt"<?php selected( $instance['list_info_display'], 'show-excerpt' ); ?>><?php _e('Show Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-all"<?php selected( $instance['list_info_display'], 'show-all' ); ?>><?php _e('Show Title, Meta Data and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="hide-all"<?php selected( $instance['list_info_display'], 'hide-all' ); ?>><?php _e('Hide All', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_item_title_heading' ); ?>"><?php _e( 'Title Heading:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_item_title_heading' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_item_title_heading' ); ?>">
				<option value="h1"<?php selected( $instance['list_item_title_heading'], 'h1' ); ?>><?php _e('H1', SDF_TXT_DOMAIN); ?></option>
				<option value="h2"<?php selected( $instance['list_item_title_heading'], 'h2' ); ?>><?php _e('H2', SDF_TXT_DOMAIN); ?></option>
				<option value="h3"<?php selected( $instance['list_item_title_heading'], 'h3' ); ?>><?php _e('H3', SDF_TXT_DOMAIN); ?></option>
				<option value="h4"<?php selected( $instance['list_item_title_heading'], 'h4' ); ?>><?php _e('H4', SDF_TXT_DOMAIN); ?></option>
				<option value="h5"<?php selected( $instance['list_item_title_heading'], 'h5' ); ?>><?php _e('H5', SDF_TXT_DOMAIN); ?></option>
				<option value="h6"<?php selected( $instance['list_item_title_heading'], 'h6' ); ?>><?php _e('H6', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_item_image_position' ); ?>"><?php _e( 'List Item Image Position:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_item_image_position' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_item_image_position' ); ?>">
				<option value="top"<?php selected( $instance['list_item_image_position'], 'top' ); ?>><?php _e('Top', SDF_TXT_DOMAIN); ?></option>
				<option value="bottom"<?php selected( $instance['list_item_image_position'], 'bottom' ); ?>><?php _e('Bottom', SDF_TXT_DOMAIN); ?></option>
				<option value="left"<?php selected( $instance['list_item_image_position'], 'left' ); ?>><?php _e('Left', SDF_TXT_DOMAIN); ?></option>
				<option value="right"<?php selected( $instance['list_item_image_position'], 'right' ); ?>><?php _e('Right', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('list_word_count'); ?>"><?php _e('Word Count for Post Excerpts:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('list_word_count'); ?>"  value="<?php echo $instance['list_word_count']; ?>" class="widefat" id="<?php echo $this->get_field_id('list_word_count'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('list_posts_offset'); ?>"><?php _e('Posts Offset:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('list_posts_offset'); ?>"  value="<?php echo $instance['list_posts_offset']; ?>" class="widefat" id="<?php echo $this->get_field_id('list_posts_offset'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_images' ); ?>"><?php _e( 'Show Images:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'show_images' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'show_images' ); ?>">
				<option value="yes"<?php selected( $instance['show_images'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['show_images'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_thumbnails_size' ); ?>"><?php _e( 'Choose Thumbnails Size:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_thumbnails_size' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_thumbnails_size' ); ?>">
				<?php foreach ( $imageSizes as $size_id => $size_name ) : ?>
				<option value="<?php echo $size_id; ?>"<?php selected( $instance['list_thumbnails_size'], $size_id ); ?>><?php echo $size_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_thumbnails_shape' ); ?>"><?php _e( 'Choose Thumbnails Shape:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_thumbnails_shape' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_thumbnails_shape' ); ?>">
				<option value="full"<?php selected( $instance['list_thumbnails_shape'], 'full' ); ?>><?php _e('Full Size', SDF_TXT_DOMAIN); ?></option>
				<option value="large"<?php selected( $instance['list_thumbnails_shape'], 'large' ); ?>><?php _e('Large Size', SDF_TXT_DOMAIN); ?></option>
				<option value="square"<?php selected( $instance['list_thumbnails_shape'], 'square' ); ?>><?php _e('Square', SDF_TXT_DOMAIN); ?></option>
				<option value="rectangle"<?php selected( $instance['list_thumbnails_shape'], 'rectangle' ); ?>><?php _e('Rectangle', SDF_TXT_DOMAIN); ?></option>
				<option value="circle"<?php selected( $instance['list_thumbnails_shape'], 'circle' ); ?>><?php _e('Circle', SDF_TXT_DOMAIN); ?></option>
				<option value="ellipse"<?php selected( $instance['list_thumbnails_shape'], 'ellipse' ); ?>><?php _e('Ellipse', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_entrance_animation' ); ?>"><?php _e( 'Enable Items Entrance Animation on Page Load:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_entrance_animation' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_entrance_animation' ); ?>">
				<?php foreach ( $entranceAnimations as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['list_entrance_animation'], $anim_name ); ?>><?php echo $anim_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'list_entrance_animation_duration' ); ?>"><?php _e( 'Set Entrance Animation Duration:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'list_entrance_animation_duration' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'list_entrance_animation_duration' ); ?>">
				<?php foreach ( $entranceAnimationDuration as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['list_entrance_animation_duration'], $anim_name ); ?>><?php echo $anim_id; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		
		<?php
    echo '</div>'; // list tab
    echo '<div id="lpw-grid">';

   		?>

		<p>
			<label for="<?php echo $this->get_field_id('grid_post_cats'); ?>"><?php _e('Choose Categories:', SDF_TXT_DOMAIN); ?></label>
			<?php $categories = get_terms('category', array( 'hide_empty' => 0 ));
						
					if(is_array( $categories ) && !empty( $categories )) { ?>
						<ul class="list:category categorychecklist">
						<?php foreach ( $categories as $term ) { ?>
					
							<li class="popular-category">
							<?php
							$option = '<input type="checkbox" id="'.$this->get_field_id('grid_post_cats').'" name="'.$this->get_field_name( 'grid_post_cats' ).'[]" ';
							
							if (is_array($instance['grid_post_cats'])) {
								foreach ($instance['grid_post_cats'] as $cats) {
									if($cats == $term->slug) {
										$option .= ' checked="checked"';
									}
								}
							}
							$option .= ' value="'.$term->slug.'"/>  '.$term->name;
							
							echo $option; ?>
							</li>
						
						<?php } ?>
						</ul>
					<?php } else {
						echo '<div class="alert alert-error">' . _e( 'You haven\'t set any Post Categories!', SDF_TXT_DOMAIN ) . '</div>'; 
					} ?>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_layout' ); ?>"><?php _e( 'Choose Grid Layout:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_layout' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_layout' ); ?>">
				<option value="grid_default"<?php selected( $instance['grid_layout'], 'grid_default' ); ?>><?php _e('Default Grid with Spacing ', SDF_TXT_DOMAIN); ?></option>
				<option value="grid_no_space"<?php selected( $instance['grid_layout'], 'grid_no_space' ); ?>><?php _e('Grid without Spacing', SDF_TXT_DOMAIN); ?></option>
				<option value="masonry_default"<?php selected( $instance['grid_layout'], 'masonry_default' ); ?>><?php _e('Masonry with Spacing ', SDF_TXT_DOMAIN); ?></option>
				<option value="masonry_no_space"<?php selected( $instance['grid_layout'], 'masonry_no_space' ); ?>><?php _e('Masonry without Spacing', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_title_position' ); ?>"><?php _e( 'Grid Item Title Position:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_title_position' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_title_position' ); ?>">
				<option value="above"<?php selected( $instance['grid_title_position'], 'above' ); ?>><?php _e('Above Image', SDF_TXT_DOMAIN); ?></option>
				<option value="below"<?php selected( $instance['grid_title_position'], 'below' ); ?>><?php _e('Below Image', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_thumbnails_size' ); ?>"><?php _e( 'Choose Thumbnails Size:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_thumbnails_size' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_thumbnails_size' ); ?>">
				<?php foreach ( $imageSizes as $size_id => $size_name ) : ?>
				<option value="<?php echo $size_id; ?>"<?php selected( $instance['grid_thumbnails_size'], $size_id ); ?>><?php echo $size_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_thumbnails_shape' ); ?>"><?php _e( 'Choose Thumbnails Shape:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_thumbnails_shape' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_thumbnails_shape' ); ?>">
				<option value="full"<?php selected( $instance['grid_thumbnails_shape'], 'full' ); ?>><?php _e('Full Size', SDF_TXT_DOMAIN); ?></option>
				<option value="large"<?php selected( $instance['grid_thumbnails_shape'], 'large' ); ?>><?php _e('Large Size', SDF_TXT_DOMAIN); ?></option>
				<option value="square"<?php selected( $instance['grid_thumbnails_shape'], 'square' ); ?>><?php _e('Square', SDF_TXT_DOMAIN); ?></option>
				<option value="rectangle"<?php selected( $instance['grid_thumbnails_shape'], 'rectangle' ); ?>><?php _e('Rectangle', SDF_TXT_DOMAIN); ?></option>
				<option value="circle"<?php selected( $instance['grid_thumbnails_shape'], 'circle' ); ?>><?php _e('Circle', SDF_TXT_DOMAIN); ?></option>
				<option value="ellipse"<?php selected( $instance['grid_thumbnails_shape'], 'ellipse' ); ?>><?php _e('Ellipse', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('rows'); ?>"><?php _e('Rows:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('rows'); ?>"  value="<?php echo $rows; ?>" class="widefat" id="<?php echo $this->get_field_id('rows'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('columns'); ?>"><?php _e('Columns:',SDF_TXT_DOMAIN); ?></label>
			<select name="<?php echo $this->get_field_name( 'columns' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'columns' ); ?>">
				<option value="1"<?php selected( $columns, '1' ); ?>>1</option>
				<option value="2"<?php selected( $columns, '2' ); ?>>2</option>
				<option value="3"<?php selected( $columns, '3' ); ?>>3</option>
				<option value="4"<?php selected( $columns, '4' ); ?>>4</option>
				<option value="6"<?php selected( $columns, '6' ); ?>>6</option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_item_title_heading' ); ?>"><?php _e( 'Title Heading:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_item_title_heading' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_item_title_heading' ); ?>">
				<option value="h1"<?php selected( $instance['grid_item_title_heading'], 'h1' ); ?>><?php _e('H1', SDF_TXT_DOMAIN); ?></option>
				<option value="h2"<?php selected( $instance['grid_item_title_heading'], 'h2' ); ?>><?php _e('H2', SDF_TXT_DOMAIN); ?></option>
				<option value="h3"<?php selected( $instance['grid_item_title_heading'], 'h3' ); ?>><?php _e('H3', SDF_TXT_DOMAIN); ?></option>
				<option value="h4"<?php selected( $instance['grid_item_title_heading'], 'h4' ); ?>><?php _e('H4', SDF_TXT_DOMAIN); ?></option>
				<option value="h5"<?php selected( $instance['grid_item_title_heading'], 'h5' ); ?>><?php _e('H5', SDF_TXT_DOMAIN); ?></option>
				<option value="h6"<?php selected( $instance['grid_item_title_heading'], 'h6' ); ?>><?php _e('H6', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_item_link_button' ); ?>"><?php _e( 'Show Item Link Button:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'show_item_link_button' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'show_item_link_button' ); ?>">
				<option value="yes"<?php selected( $instance['show_item_link_button'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['show_item_link_button'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_prettyphoto_button' ); ?>"><?php _e( 'Show PrettyPhoto Button:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'show_prettyphoto_button' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'show_prettyphoto_button' ); ?>">
				<option value="yes"<?php selected( $instance['show_prettyphoto_button'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['show_prettyphoto_button'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_custom_button' ); ?>"><?php _e( 'Show Custom Button:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'show_custom_button' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'show_custom_button' ); ?>">
				<option value="yes"<?php selected( $instance['show_custom_button'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['show_custom_button'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_button_icon' ); ?>"><?php _e( 'Set Custom Button Icon:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'custom_button_icon' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'custom_button_icon' ); ?>">
				<?php foreach ( $faIcons as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['custom_button_icon'], $anim_name ); ?>><?php echo $anim_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('custom_button_text'); ?>"><?php _e('Set Custom Button Text:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('custom_button_text'); ?>"  value="<?php echo $instance['custom_button_text']; ?>" class="widefat" id="<?php echo $this->get_field_id('custom_button_text'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('custom_button_link'); ?>"><?php _e('Custom Button Link:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('custom_button_link'); ?>"  value="<?php echo $instance['custom_button_link']; ?>" class="widefat" id="<?php echo $this->get_field_id('custom_button_link'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'custom_button_link_target' ); ?>"><?php _e( 'Custom Button Link Target:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'custom_button_link_target' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'custom_button_link_target' ); ?>">
				<?php foreach ( $linkTargets as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['custom_button_link_target'], $anim_name ); ?>><?php echo $anim_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'show_overlay_title' ); ?>"><?php _e( 'Show Overlay Title:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'show_overlay_title' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'show_overlay_title' ); ?>">
				<option value="yes"<?php selected( $instance['show_overlay_title'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['show_overlay_title'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'posts_filter' ); ?>"><?php _e( 'Show Posts Filter:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'posts_filter' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'posts_filter' ); ?>">
				<option value="yes"<?php selected( $instance['posts_filter'], 'yes' ); ?>><?php _e('Yes', SDF_TXT_DOMAIN); ?></option>
				<option value="no"<?php selected( $instance['posts_filter'], 'no' ); ?>><?php _e('No', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_info_display' ); ?>"><?php _e( 'Info Display:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_info_display' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_info_display' ); ?>">
				<option value="show-title"<?php selected( $instance['grid_info_display'], 'show-title' ); ?>><?php _e('Show Title', SDF_TXT_DOMAIN); ?></option>
				<option value="show-title-excerpt"<?php selected( $instance['grid_info_display'], 'show-title-excerpt' ); ?>><?php _e('Show Title and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-info"<?php selected( $instance['grid_info_display'], 'show-info' ); ?>><?php _e('Show Title and Meta Data', SDF_TXT_DOMAIN); ?></option>
				<option value="show-meta-excerpt"<?php selected( $instance['grid_info_display'], 'show-meta-excerpt' ); ?>><?php _e('Show Meta Data and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-excerpt"<?php selected( $instance['grid_info_display'], 'show-excerpt' ); ?>><?php _e('Show Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="show-all"<?php selected( $instance['grid_info_display'], 'show-all' ); ?>><?php _e('Show Title, Meta Data and Post Excerpt', SDF_TXT_DOMAIN); ?></option>
				<option value="hide-all"<?php selected( $instance['grid_info_display'], 'hide-all' ); ?>><?php _e('Hide All', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('grid_word_count'); ?>"><?php _e('Word Count for Post Excerpts:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('grid_word_count'); ?>"  value="<?php echo $instance['grid_word_count']; ?>" class="widefat" id="<?php echo $this->get_field_id('grid_word_count'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('grid_posts_offset'); ?>"><?php _e('Posts Offset:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('grid_posts_offset'); ?>"  value="<?php echo $instance['grid_posts_offset']; ?>" class="widefat" id="<?php echo $this->get_field_id('grid_posts_offset'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_entrance_animation' ); ?>"><?php _e( 'Enable Items Entrance Animation on Page Load:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_entrance_animation' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_entrance_animation' ); ?>">
				<?php foreach ( $entranceAnimations as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['grid_entrance_animation'], $anim_name ); ?>><?php echo $anim_name; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'grid_entrance_animation_duration' ); ?>"><?php _e( 'Set Entrance Animation Duration:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'grid_entrance_animation_duration' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'grid_entrance_animation_duration' ); ?>">
				<?php foreach ( $entranceAnimationDuration as $anim_id => $anim_name ) : ?>
				<option value="<?php echo $anim_name; ?>"<?php selected( $instance['grid_entrance_animation_duration'], $anim_name ); ?>><?php echo $anim_id; ?></option>
				<?php endforeach; ?>
			</select>
		</p>
		
		<?php
    echo '</div>'; // grid tab
    echo '</div>'; // tabs
  }

  function update($new_instance, $old_instance) {
    $instance = $old_instance;

    $instance['title'] = $new_instance['title'];
    $instance['lpw_layout_type'] = $new_instance['lpw_layout_type'];
		$instance['list_post_cats'] = $new_instance['list_post_cats'];
		$instance['grid_post_cats'] = $new_instance['grid_post_cats'];
		$instance['rows'] = $new_instance['rows'];
		$instance['columns'] = $new_instance['columns'];
		$instance['grid_layout'] = $new_instance['grid_layout'];
		$instance['posts_filter'] = $new_instance['posts_filter'];
		$instance['grid_info_display'] = $new_instance['grid_info_display'];
		$instance['list_info_display'] = $new_instance['list_info_display'];
		$instance['grid_thumbnails_shape'] = $new_instance['grid_thumbnails_shape'];
		$instance['grid_thumbnails_size'] = $new_instance['grid_thumbnails_size'];
		$instance['grid_entrance_animation'] = $new_instance['grid_entrance_animation'];
		$instance['grid_entrance_animation_duration'] = $new_instance['grid_entrance_animation_duration'];
		$instance['list_thumbnails_shape'] = $new_instance['list_thumbnails_shape'];
		$instance['list_thumbnails_size'] = $new_instance['list_thumbnails_size'];
		$instance['list_entrance_animation'] = $new_instance['list_entrance_animation'];
		$instance['list_entrance_animation_duration'] = $new_instance['list_entrance_animation_duration'];
		$instance['posts_count'] = $new_instance['posts_count'];
		$instance['show_images'] = $new_instance['show_images'];
		$instance['list_word_count'] = $new_instance['list_word_count'];
		$instance['grid_word_count'] = $new_instance['grid_word_count'];
		$instance['list_posts_offset'] = $new_instance['list_posts_offset'];
		$instance['grid_posts_offset'] = $new_instance['grid_posts_offset'];
		$instance['list_item_title_heading'] = $new_instance['list_item_title_heading'];
		$instance['grid_item_title_heading'] = $new_instance['grid_item_title_heading'];
		$instance['grid_title_position'] = $new_instance['grid_title_position'];
		$instance['list_item_image_position'] = $new_instance['list_item_image_position'];
		$instance['show_prettyphoto_button'] = $new_instance['show_prettyphoto_button'];
		$instance['show_item_link_button'] = $new_instance['show_item_link_button'];
		$instance['show_custom_button'] = $new_instance['show_custom_button'];
		
    return $instance;
  }

  function widget($args, $instance) {
    $output = $tmp = '';
    extract($args, EXTR_SKIP);
		
		if ( !isset($instance['title']) ) $instance['title'] = "Latest Posts";
		$title = esc_attr($instance['title']);
		if ( !isset($instance['lpw_layout_type']) ) $instance['lpw_layout_type'] = 'list';
		if ( !isset($instance['list_post_cats']) ) $instance['list_post_cats'] = array(); 
		if ( !isset($instance['grid_post_cats']) ) $instance['grid_post_cats'] = array(); 
		if ( !isset($instance['rows']) ) $instance['rows'] = 1; 
		$rows = $instance['rows'];
		if ( !isset($instance['columns']) ) $instance['columns'] = 3; 
		$columns = $instance['columns'];
		if ( !isset($instance['grid_layout']) ) $instance['grid_layout'] = "grid_default";
		if ( !isset($instance['posts_filter']) ) $instance['posts_filter'] = "yes";
		if ( !isset($instance['grid_info_display']) ) $instance['grid_info_display'] = "hide-all";
		if ( !isset($instance['list_info_display']) ) $instance['list_info_display'] = "show-info";
		if ( !isset($instance['show_overlay_title']) ) $instance['show_overlay_title'] = "yes";
		if ( !isset($instance['grid_thumbnails_shape']) ) $instance['grid_thumbnails_shape'] = "rectangle";
		if ( !isset($instance['grid_thumbnails_size']) ) $instance['grid_thumbnails_size'] = "sdf-image-md-12";
		if ( !isset($instance['grid_entrance_animation']) ) $instance['grid_entrance_animation'] = "No";
		if ( !isset($instance['grid_entrance_animation_duration']) ) $instance['grid_entrance_animation_duration'] = "animated";
		if ( !isset($instance['list_thumbnails_shape']) ) $instance['list_thumbnails_shape'] = "rectangle";
		if ( !isset($instance['list_thumbnails_size']) ) $instance['list_thumbnails_size'] = "sdf-image-md-12";
		if ( !isset($instance['list_entrance_animation']) ) $instance['list_entrance_animation'] = "No";
		if ( !isset($instance['list_entrance_animation_duration']) ) $instance['list_entrance_animation_duration'] = "animated";
		if ( !isset($instance['show_images']) ) $instance['show_images'] = "yes";
		if ( !isset($instance['grid_word_count']) ) $instance['grid_word_count'] = "15";
		if ( !isset($instance['list_word_count']) ) $instance['list_word_count'] = "15";
		if ( !isset($instance['grid_posts_offset']) ) $instance['grid_posts_offset'] = "0";
		if ( !isset($instance['list_posts_offset']) ) $instance['list_posts_offset'] = "0";
		if ( !isset($instance['posts_count']) ) $instance['posts_count'] = "5";
		if ( !isset($instance['list_item_title_heading']) ) $instance['list_item_title_heading'] = "h4";
		if ( !isset($instance['grid_item_title_heading']) ) $instance['grid_item_title_heading'] = "h4";
		if ( !isset($instance['grid_title_position']) ) $instance['grid_title_position'] = "above";
		if ( !isset($instance['list_item_image_position']) ) $instance['list_item_image_position'] = "top";
		if ( !isset($instance['show_prettyphoto_button']) ) $instance['show_prettyphoto_button'] = "yes";
		if ( !isset($instance['show_item_link_button']) ) $instance['show_item_link_button'] = "yes";
		if ( !isset($instance['show_custom_button']) ) $instance['show_custom_button'] = "no";
		
		$unique_wid = $args['widget_id'];
		$no_space_col = ( in_array( $instance['grid_layout'], array( 'masonry_no_space', 'grid_no_space' ) ) ) ? ' no-space-col' : '';
		$info_display_bottom_space = ( !in_array( $instance['grid_info_display'], array( 'show-title', 'show-title-excerpt', 'show-info', 'show-meta-excerpt', 'show-excerpt', 'show-all' ) ) ) ? ' no-info-display' : '';

    $output .= $before_widget;
	
    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
    if (!empty($title)) {
      $output .= $before_title . $title . $after_title;
    }
		
		if ($instance['lpw_layout_type'] == 'grid'){
			$limit = $rows * $columns;
		}
		elseif ($instance['lpw_layout_type'] == 'list'){
			$limit = $instance['posts_count'];
		}
		
		$terms = get_terms('category', array( 'hide_empty' => 0 ));	
					

		if ($instance['lpw_layout_type'] == 'grid'){
		
			$post_cats = $instance['grid_post_cats'];
			$posts_offset = $instance['grid_posts_offset'];
			
			// prepare for the right thumbnail (square proportions)
			$thumb_size = 'sdf-image-md-12';
			$thumb_square_shapes = array("circle", "leaf", "lemon", "square");
			if ( $instance["grid_thumbnails_size"] != '' && $instance["grid_thumbnails_shape"] != '') {
				$thumb_size = $instance["grid_thumbnails_size"];
				if ( in_array( $instance["grid_thumbnails_shape"], $thumb_square_shapes) && !in_array($instance["grid_thumbnails_size"], array('full', 'large', 'medium', 'thumbnail')) ) {	
					$thumb_size .= '-sq';
				}
			}
			$image_shape = ( in_array( $instance["grid_thumbnails_size"], array( 'full', 'large', 'medium', 'thumbnail' ) ) ) ? '' : $instance["grid_thumbnails_shape"];
			
			$title_text_align = '';
			$center_square_shapes = array("circle", "leaf", "lemon", "egg", "ellipse");
			if ( isset ( $instance["grid_thumbnails_shape"] ) && ( in_array( $instance["grid_thumbnails_shape"], $center_square_shapes) ) ) {	
				$title_text_align = ' text-center';
			}
			
			$grid_entrance_animation =  ( $instance["grid_entrance_animation"] == "No" ) ? '' : ' viewport_animate animate_now one_way';
			$grid_data_animation = ( $instance["grid_entrance_animation"] == "No" ) ? '' : ' data-animation="'.$instance["grid_entrance_animation"].'"';	
			$grid_data_duration = "";
			if ( $instance["grid_entrance_animation"] != "No" ){
				$grid_data_duration = (sdf_is ($instance["grid_entrance_animation_duration"], "")) ? ' data-duration="animated"' : ' data-duration="'.$instance["grid_entrance_animation_duration"].'"';	
			}
			
			$filter_len = 0;
			$filter_nav = '';
			
			if(is_array( $terms ) && !empty( $terms )) {
			
				$filter_cats = array();
				$types_len = count($post_cats);
				$activate_holder = ( $types_len < 2 ) ? ' class="active"' : '';
				foreach ( $terms as $term ) :
					if ( array_key_exists ( $term->slug, array_flip( $post_cats ))) {
					$filter_nav.= '<li' . $activate_holder . ' data-option-value=".'.$term->slug.'"><a href="#">'.$term->name.'</a></li>';
					$filter_cats[] = $term->slug;			
					}
				endforeach; 
				$filter_len = count($filter_cats);
				
			}
			
			$filter_display =  (sdf_is ($instance["posts_filter"], "yes")) ? '' : ' visuallyhidden';
			$tmp.= '<section id="latest-posts-options" class="'.$filter_display.'">';
			$tmp.= '<ul id="filters" class="option-set nav nav-pills nav-center" data-option-key="filter">';
			if ( $filter_len >= 2 ) {
				$tmp.= '<li class="active" data-option-value="*"><a href="#">All</a></li>';
			}
			$tmp.= $filter_nav;
			$tmp.= '</ul>';
			$tmp.= '</section>';

			$tmp.= '<div id="'.$unique_wid.'" class="widget-posts">';
			$tmp.= '<div id="sortable-posts-items" class="isotope row'.$no_space_col.$info_display_bottom_space.'" data-colcount="'.$columns.'">';
		}
		elseif ($instance['lpw_layout_type'] == 'list') {
		
			$post_cats = $instance['list_post_cats'];
			$posts_offset = $instance['list_posts_offset'];
			
			$title_text_align = '';
			$center_square_shapes = array("circle", "leaf", "lemon", "egg", "ellipse");
			if ( isset ( $instance["list_thumbnails_shape"] ) && ( in_array( $instance["list_thumbnails_shape"], $center_square_shapes) ) ) {	
				$title_text_align = ' text-center';
			}
		
			// prepare for the right thumbnail (square proportions)
			$thumb_size = 'sdf-image-md-12';
			$thumb_square_shapes = array("circle", "leaf", "lemon", "square");
			if ( $instance["list_thumbnails_size"] != '' && $instance["list_thumbnails_shape"] != '') {
				$thumb_size = $instance["list_thumbnails_size"];
				if ( in_array($instance['list_item_image_position'], array('left', 'right')) && in_array($instance['list_thumbnails_size'], array('full', 'large', 'sdf-image-md-12')) ) {	
					$thumb_size = 'sdf-image-md-2';
				}
				if ( in_array( $instance["list_thumbnails_shape"], $thumb_square_shapes) && !in_array($thumb_size, array('full', 'large', 'medium', 'thumbnail')) ) {	
					$thumb_size .= '-sq';
				}
			}
			$image_shape = ( in_array( $instance["grid_thumbnails_size"], array( 'full', 'large', 'medium', 'thumbnail' ) ) ) ? '' : $instance["list_thumbnails_shape"];
			
			$list_entrance_animation =  ( $instance["list_entrance_animation"] == "No" ) ? '' : ' viewport_animate animate_now one_way';
			$list_data_animation = ( $instance["list_entrance_animation"] == "No" ) ? '' : ' data-animation="'.$instance["list_entrance_animation"].'"';	
			$list_data_duration = "";
			if ( $instance["list_entrance_animation"] != "No" ){
				$list_data_duration = (sdf_is ($instance["list_entrance_animation_duration"], "")) ? ' data-duration="animated"' : ' data-duration="'.$instance["list_entrance_animation_duration"].'"';	
			}
			
			$tmp.= '<ul class="media-list">';
		}
		

		$latest_query = array(
			'post_type' => 'post',
			'posts_per_page' => $limit,
			'order'          => 'DESC',
			'orderby'        => 'date',
			'post_status'    => 'publish',
			'offset' => $posts_offset,
			'post__not_in' => array( get_the_ID() ),
			'meta_query' => array(
													array(
														 'key' => '_wpu_silo_dws',
														 'compare' => 'NOT EXISTS'
													)
												)
		);
				
		$latest_query['tax_query'][] = array(
			'taxonomy' 	=> 'category',
			'field' 	=> 'slug',
			'terms' 	=> $post_cats
		);

		$latest_loop = new WP_Query($latest_query);
		
		
		// Begin The Loop
			if( $latest_loop->have_posts() ) :
			while ( $latest_loop->have_posts() ) : $latest_loop->the_post();
				
				// the terms of the taxonomy 'category' that are attached to the post
				$terms = get_the_terms( get_the_ID(), 'category' ); 
				
				// Output the title of each post item
				$title= str_ireplace('"', '', trim(get_the_title()));
				
				// check post type
				$post_format_icon = '';
				$post_format = get_post_format();
				$post_format_class = strtolower($post_format);
				$post_format_title = ($post_format) ? ucfirst($post_format) : 'Standard';
				switch ($post_format) {
					case 'video':
						$post_format_icon = 'video-camera';
						break;
					case 'audio':
						$post_format_icon = 'music';
						break;
					case 'gallery':
						$post_format_icon = 'picture-o';
						break;
					case 'image':
						$post_format_icon = 'picture-o';
						break;
					case 'quote':
						$post_format_icon = 'quote-left';
						break;
					case 'link':
						$post_format_icon = 'link';
						break;
					case NULL:
						$post_format_icon = 'pencil';
						$post_format_class = 'standard';
						break;						
				}
				
				if ($instance['lpw_layout_type'] == 'grid'){
					
					$grid_columns = 12 / $columns;
					
					$data_type = "";
					foreach ($terms as $term) { 
						$data_type.= strtolower(preg_replace('/\s+/', '-', $term->name)). ' '; 
					}

					//Apply a data-id for unique indentity, 
					//and loop through the taxonomy and assign the terms of the post item to a data-type

					$tmp.= '<article class="isotope-item col-md-'.$grid_columns.' col-sm-'.$grid_columns.' col-xs-12 '.$instance['grid_layout'].' '.$image_shape.' '.$data_type.'" data-id="'.get_the_ID().'" data-type="'.$data_type.'" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">';
							
					if ( isset ($instance['grid_info_display']) &&  $instance['grid_info_display'] != 'hide-all' ){
						if ( isset ( $instance['grid_info_display'] ) && ( !in_array( $instance['grid_info_display'], array( 'show-meta-excerpt', 'show-excerpt' ))) && $instance['grid_title_position'] == 'above' ) {
							$tmp .= '<'.$instance['grid_item_title_heading'].' class="post-title'.$title_text_align.'"><a href="'.get_permalink().'">'.$title.'</a></'.$instance['grid_item_title_heading'].'>';
						}
					}
					
					// Check if wordpress supports featured images, and if so output the thumbnail
					if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) {
						// Output the featured image
					$tmp .= '<div class="item-image clearfix '.$image_shape.'">';
						
						// standard or masonry layout
						
						$pretty_view ='';
						$pretty_view =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'large', false, '' );
						$pretty_view = $pretty_view[0];
				
						$item_view = '';
						
						if( in_array($instance['grid_layout'], array('grid_default', 'grid_no_space', 'standard') ) ) {
							$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $thumb_size, false, '' );
							$pin_view = $pin_view[0];
						}
						else {
							$thumb_size = ($thumb_size == 'full') ? 'full' : 'large';
							$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $thumb_size, false, '' );
							$pin_view = $pin_view[0];
						}
					
						if($pin_view){
							$item_view = '<a href="'.get_permalink().'" class="item-link-touch"><span class="image-holder clearfix"><img itemprop="image" class="img-responsive '.$image_shape.$grid_entrance_animation.'"'.$grid_data_animation.$grid_data_duration.' alt="'.$title.'" src="'.$pin_view.'"></span></a>';
						}
						
						$tmp .= $item_view;
						$tmp .= '<span class="image-overlay '.$image_shape.'"><span class="image-text-outer"><span class="image-text-inner">';
						
						if ( isset ($instance['show_overlay_title']) &&  $instance['show_overlay_title'] == 'yes') {
							$tmp .= '<'.$instance['grid_item_title_heading'].' class="post-title'.$title_text_align.'" itemprop="headline"><a href="'.get_permalink().'">'.$title.'</a></'.$instance['grid_item_title_heading'].'>';
						}
						
						$tmp .= '<span class="btn-overlay-holder">';
						
						$grid_item_overlay_button_icon_size = sdfGo()->sdf_get_global('typography','theme_grid_item_overlay_button_icon_size');
		
						if($instance['show_prettyphoto_button'] == 'yes') {
							$grid_item_overlay_prettyphoto_button_icon = sdfGo()->sdf_get_global('typography','theme_grid_item_overlay_prettyphoto_button_icon');
							$grid_item_overlay_prettyphoto_button_icon = ( $grid_item_overlay_prettyphoto_button_icon != 'no' ) ? '<i class="fa '.$grid_item_overlay_prettyphoto_button_icon.' '.$grid_item_overlay_button_icon_size.'"></i> ' : '';
							$grid_item_overlay_prettyphoto_button_custom_text = sdfGo()->sdf_get_global('typography','theme_grid_item_overlay_prettyphoto_button_custom_text');
							if( $grid_item_overlay_prettyphoto_button_custom_text != '' || $grid_item_overlay_prettyphoto_button_icon != '' ) {
							$tmp .= '<a href="'.$pretty_view.'" class="btn-overlay pretty-view" data-rel="prettyPhoto[]" title="'.$title.'">'.$grid_item_overlay_prettyphoto_button_icon.$grid_item_overlay_prettyphoto_button_custom_text.'</a>';
							}
						}
						
						if($instance['show_item_link_button'] == 'yes') {
							$grid_item_overlay_item_button_icon = sdfGo()->sdf_get_global('typography','theme_grid_item_overlay_item_button_icon');
							$grid_item_overlay_item_button_icon = ( $grid_item_overlay_item_button_icon != 'no' ) ? '<i class="fa '.$grid_item_overlay_item_button_icon.' '.$grid_item_overlay_button_icon_size.'"></i> ' : '';
							$grid_item_overlay_item_button_custom_text = sdfGo()->sdf_get_global('typography','theme_grid_item_overlay_item_button_custom_text');
							if( $grid_item_overlay_item_button_custom_text != '' || $grid_item_overlay_item_button_icon != '' ) {
							$tmp .= '<a href="'.get_permalink().'" title="'.__('View Item', SDF_TXT_DOMAIN).'" class="btn-overlay" data-id="'.get_the_ID().'">'.$grid_item_overlay_item_button_icon.$grid_item_overlay_item_button_custom_text.'</a>';
							}
						}
						
						if($instance['show_custom_button'] == 'yes') {
							$grid_item_overlay_custom_button_icon = $instance['custom_button_icon'];
							$grid_item_overlay_custom_button_icon = ( $grid_item_overlay_custom_button_icon != 'no' ) ? '<i class="fa '.$grid_item_overlay_custom_button_icon.' '.$grid_item_overlay_button_icon_size.'"></i> ' : '';
							$grid_item_overlay_custom_button_custom_text = $instance['custom_button_text'];
							$grid_item_overlay_custom_button_link = $instance['custom_button_link'];
							$grid_item_overlay_custom_button_custom_link_target = $instance['custom_button_link_target'];
							$grid_item_overlay_custom_button_custom_link_target = ($grid_item_overlay_custom_button_custom_link_target != '') ? ' target="'. $instance['custom_button_link_target'] . '"' : '';
							if( $grid_item_overlay_custom_button_custom_text != '' || $grid_item_overlay_custom_button_icon != '' ) {
							$tmp .= '<a href="'.$grid_item_overlay_custom_button_link.'" title="'.$grid_item_overlay_custom_button_custom_text.'" class="btn-overlay"'.$grid_item_overlay_custom_button_custom_link_target.'>'.$grid_item_overlay_custom_button_icon.$grid_item_overlay_custom_button_custom_text.'</a>';
							}
						}
						
						$tmp .= '</span>';
						
						$tmp .= '</span></span></span>';
						$tmp .= '</div>';
					}
				
					if ( isset ($instance['grid_info_display']) &&  $instance['grid_info_display'] != 'hide-all' ){
						if ( isset ( $instance['grid_info_display'] ) && ( !in_array( $instance['grid_info_display'], array( 'show-meta-excerpt', 'show-excerpt' ))) && $instance['grid_title_position'] == 'below' ) {
							$tmp .= '<'.$instance['grid_item_title_heading'].' class="post-title'.$title_text_align.'"><a href="'.get_permalink().'">'.$title.'</a></'.$instance['grid_item_title_heading'].'>';
						}
						
						if ( isset ($instance['grid_info_display']) &&  ( in_array( $instance['grid_info_display'], array( 'show-info', 'show-meta-excerpt', 'show-all' )))){
							$tmp .= '<div class="post-data">';
						}
						else {
							$tmp .= '<div class="visuallyhidden">';
						}
						
						$entry_date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark" class="post-time">' .
							'<time itemprop="datePublished" datetime="%3$s">%4$s</time>' .
							'<time class="modify-date hide hidden updated" datetime="%5$s">%6$s</time>' .
							'</a>',
							esc_url( get_permalink() ),
							esc_attr( get_the_title() ),
							esc_attr( get_the_date( 'c' ) ),
							esc_html( get_the_date() ),
										esc_html( get_the_modified_date( 'c' ) ),
										esc_html( get_the_modified_date() )
						);

						$tmp .= '<ul class="post-meta clearfix fa-ul">';
						$tmp .= '<li><div class="post-meta-item"><i class="fa fa-calendar"></i> '.$entry_date.'</div></li>';
						$tmp .= '<li><div class="post-meta-item"><i class="fa fa-'.$post_format_icon.'"></i> '.$post_format_title.'</div></li>';
						$tmp .= '</ul>';
						$tmp .= '</div>';
						
						if ( isset ($instance['grid_info_display']) && ( in_array( $instance['grid_info_display'], array( 'show-meta-excerpt', 'show-title-excerpt', 'show-excerpt', 'show-all' ))) ) {
						$raw_excerpt = get_the_content('');
						$post_text = ($instance['grid_word_count'] != 0) ? get_excerpt_by_id(get_the_ID(), $instance['grid_word_count']) : $raw_excerpt;
							$tmp .= '<p class="latest-post-excerpt">'.$post_text.'</p>';
						}
					}
					
					$tmp .= '</article>';
				}
				elseif ($instance['lpw_layout_type'] == 'list'){
						
					$raw_excerpt = get_the_content('');
					$post_text = ($instance['list_word_count'] != 0) ? get_excerpt_by_id(get_the_ID(), $instance['list_word_count']) : $raw_excerpt;
					
					if(in_array($instance['list_item_image_position'], array('left', 'right'))){
					
						$item_view ='';
						if($instance['show_images'] == 'yes'){
							$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $thumb_size, false, '' );
							$img_src = $pin_view[0];
							$img_width = $pin_view[1];
							$img_height = $pin_view[2];
							if($img_src){
								$item_view = '<div class="media-'.$instance['list_item_image_position'].'"><a href="'.get_permalink().'" title="' . $title . '"><img itemprop="image" class="media-object img-responsive '.$image_shape.$list_entrance_animation.'"'.$list_data_animation.$list_data_duration.' alt="'.$title.'" src="'.$img_src.'" style="max-width:'.$img_width.'px;max-height:'.$img_height.'px;"></a></div>';
							}
						}
						
						$tmp .= '<li class="media">';
						if($instance['list_item_image_position'] == 'left'){
							$tmp .= $item_view;
						}
						
						if ( isset ($instance['list_info_display']) &&  $instance['list_info_display'] != 'hide-all' ){
							$tmp .= '<div class="media-body">';
							
							if ( isset ($instance['list_info_display']) &&  ( in_array( $instance['list_info_display'], array( 'show-title', 'show-title-excerpt', 'show-excerpt' )))){
								$tmp .= '<div class="visuallyhidden">';
							}
							else {
								$tmp .= '<div class="list-meta">';
							}
							
							$tmp .= '<time class="list-time" datetime="'.get_the_time('Y/m/d H:i').'"><span>'.get_the_time('m/d/y').'</span> <span>'.get_the_time('H:i').'</span></time>'.
							'</div>'.
							'<div class="list-excerpt">';
							
							if ( isset ( $instance['list_info_display'] ) && ( !in_array( $instance['list_info_display'], array( 'show-meta-excerpt', 'show-excerpt' ))) ) {
								$tmp .= '<'.$instance['list_item_title_heading'].' class="media-heading'.$title_text_align.'"><a href="'.get_permalink().'" title="' . $title . '" class="latest-post-title">'.$title.'</a></'.$instance['list_item_title_heading'].'>';
							}
							
							if ( isset ($instance['list_info_display']) && ( in_array( $instance['list_info_display'], array( 'show-meta-excerpt', 'show-title-excerpt', 'show-excerpt', 'show-all' ))) ) {
							$raw_excerpt = get_the_content('');
							$post_text = ($instance['list_word_count'] != 0) ? get_excerpt_by_id(get_the_ID(), $instance['list_word_count']) : $raw_excerpt;
								$tmp .= '<p>'.$post_text.'</p>';
							}
							
							$tmp .= '<div class="list-icon">'.
							'<i class="'.$post_format_icon.'"></i>'.
							'</div>'.
							'</div>'.
							'</div>';
							
						}
						
						if($instance['list_item_image_position'] == 'right'){
							$tmp .= $item_view;
						}
						$tmp .= '</li>';
						
					}
					elseif(in_array($instance['list_item_image_position'], array('top', 'bottom'))){
					
						$item_view ='';
						if($instance['show_images'] == 'yes'){
							$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), $thumb_size, false, '' );
							$img_src = $pin_view[0];
							$img_width = $pin_view[1];
							$img_height = $pin_view[2];
							if($img_src){
								$item_view = '<a class="media-image-'.$instance['list_item_image_position'].' clearfix" href="'.get_permalink().'" title="' . $title . '"><img itemprop="image" class="media-object img-responsive '.$image_shape.$list_entrance_animation.'"'.$list_data_animation.$list_data_duration.' alt="'.$title.'" src="'.$img_src.'" style="max-width:'.$img_width.'px;max-height:'.$img_height.'px;"></a>';
							}
						}
						
						$tmp .= '<li class="media">';
						
						if($instance['list_item_image_position'] == 'top'){
							$tmp .= $item_view;
						}
						
					if ( isset ($instance['list_info_display']) &&  $instance['list_info_display'] != 'hide-all' ){
						$tmp .= '<div class="media-body">';
						
						if ( isset ($instance['list_info_display']) &&  ( in_array( $instance['list_info_display'], array( 'show-title', 'show-title-excerpt', 'show-excerpt' )))){
							$tmp .= '<div class="visuallyhidden">';
						}
						else {
							$tmp .= '<div class="list-meta">';
						}
						
						$tmp .= '<time class="list-time" datetime="'.get_the_time('Y/m/d H:i').'"><span>'.get_the_time('m/d/y').'</span> <span>'.get_the_time('H:i').'</span></time>'.
						'</div>'.
						'<div class="list-excerpt">';
						
						if ( isset ( $instance['list_info_display'] ) && ( !in_array( $instance['list_info_display'], array( 'show-meta-excerpt', 'show-excerpt' ))) ) {
							$tmp .= '<'.$instance['list_item_title_heading'].' class="media-heading'.$title_text_align.'"><a href="'.get_permalink().'" title="' . $title . '">'.$title.'</a></'.$instance['list_item_title_heading'].'>';
						}
						
						if ( isset ($instance['list_info_display']) && ( in_array( $instance['list_info_display'], array( 'show-meta-excerpt', 'show-title-excerpt', 'show-excerpt', 'show-all' ))) ) {
						$raw_excerpt = get_the_content('');
						$post_text = ($instance['list_word_count'] != 0) ? get_excerpt_by_id(get_the_ID(), $instance['list_word_count']) : $raw_excerpt;
							$tmp .= '<p>'.$post_text.'</p>';
						}
						
						$tmp .= '<div class="list-icon">'.
						'<i class="'.$post_format_icon.'"></i>'.
						'</div>'.
						'</div>'.
						'</div>';
						
					}
					
						
						if($instance['list_item_image_position'] == 'bottom'){
							$tmp .= $item_view;
						}
						
						$tmp .= '</li>';
					
					}
				}
				
			endwhile;
			
			if ($instance['lpw_layout_type'] == 'grid'){
				$tmp .='</div></div>';
			}
			elseif ($instance['lpw_layout_type'] == 'list'){
				$tmp .='</ul>';
			}
			
			endif;
			// END the Loop
			wp_reset_postdata(); // reset the Loop
		
    $output .= apply_filters('latest_posts_widget_content', $tmp);

    $output .= $after_widget;

    echo $output;
  }
} // class Sdf_Latest_Posts_Widget

function latest_posts_widget_init() {
	register_widget("Sdf_Latest_Posts_Widget");
}
add_action('widgets_init','latest_posts_widget_init');
}