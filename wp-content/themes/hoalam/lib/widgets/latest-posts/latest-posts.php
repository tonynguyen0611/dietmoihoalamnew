<?php
/*
	Latest Posts Widget
*/


if (!function_exists('add_action')) {
  die('Please don\'t open this file directly!');
}


define('LP_WG_VER', '1.0');
require_once 'latest-posts-widget.php';


class LPW {
  // hook everything up
   static function init() {
      if (is_admin()) {
        // check if minimal required WP version is used
        self::check_wp_version(3.5);

        // enqueue admin scripts
        add_action('admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue_scripts'));
      } else {
        // enqueue frontend scripts
        //add_action('wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'));
        //add_action('wp_footer', array(__CLASS__, 'dialogs_markup'));
      }
  } // init

  // initialize widgets
  static function widgets_init() {
    register_widget('Sdf_Latest_Posts_Widget');
  } // widgets_init



  // check if user has the minimal WP version required by the plugin
  static function check_wp_version($min_version) {
    if (!version_compare(get_bloginfo('version'), $min_version,  '>=')) {
        add_action('admin_notices', array(__CLASS__, 'min_version_error'));
    }
  } // check_wp_version


  // display error message if WP version is too low
  static function min_version_error() {
    echo '<div class="error"><p>' . sprintf('Google Maps Widget <b>requires WordPress version 3.3</b> or higher to function properly. You are using WordPress version %s. Please <a href="%s">update it</a>.', get_bloginfo('version'), admin_url('update-core.php')) . '</p></div>';
  } // min_version_error


   // enqueue frontend scripts if necessary
   static function enqueue_scripts() {
     if (is_active_widget(false, false, 'googlemapswidget', true)) {
       //wp_enqueue_script('lpw', SDF_WIDGETS_URL . "/latest-posts/js/lpw.js", array(), LP_WG_VER, true);
     }
    } // enqueue_scripts


    // enqueue CSS and JS scripts on widgets page
    static function admin_enqueue_scripts() {
      if (self::is_plugin_admin_page()) {
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('lpw-cookie', SDF_WIDGETS_URL . "/latest-posts/js/jquery.cookie.js", array('jquery'), LP_WG_VER, true);
        wp_enqueue_script('lpw-admin', SDF_WIDGETS_URL . "/latest-posts/js/lpw-admin.js", array('jquery'), LP_WG_VER, true);
        wp_enqueue_style('lpw-admin', SDF_WIDGETS_URL . "/latest-posts/css/lpw-admin.css", array(), LP_WG_VER);
      } // if
    } // admin_enqueue_scripts


    // check if plugin's admin page is shown
    static function is_plugin_admin_page() {
      $current_screen = get_current_screen();

      if ($current_screen->id == 'widgets') {
        return true;
      } else {
        return false;
      }
    } // is_plugin_admin_page


    // helper function for creating dropdowns
    static function create_select_options($options, $selected = null, $output = true) {
        $out = "\n";

        foreach ($options as $tmp) {
            if ($selected == $tmp['val']) {
                $out .= "<option selected=\"selected\" value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            } else {
                $out .= "<option value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            }
        } // foreach

        if ($output) {
            echo $out;
        } else {
            return $out;
        }
    } // create_select_options


  // track a few things on plugin activation
  // NO DATA is sent anywhere!
  static function activate() {
    $options = get_option('lpw_options');

    if (!isset($options['first_version']) || !isset($options['first_install'])) {
      $options['first_version'] = LP_WG_VER;
      $options['first_install'] = current_time('timestamp');
      update_option('lpw_options', $options);
    }
  } // activate
} // class LPW


// hook everything up
register_activation_hook(__FILE__, array('LPW', 'activate'));
add_action('init', array('LPW', 'init'));
add_action('plugins_loaded', array('LPW', 'plugins_loaded'));
add_action('widgets_init', array('LPW', 'widgets_init'));