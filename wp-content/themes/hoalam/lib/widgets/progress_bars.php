<?php
/*
 *
 * Widget Team Skills
 * Firm Themes
 * www.firmthemes.com
 * 
 */

class Firm_Team_Skills_Widget extends WP_Widget {

	function Firm_Team_Skills_Widget() {
		$widget_ops = array('description' => 'Display your team skills');
		parent::__construct(false, 'Firm :: Team Skills', $widget_ops);      
	}

	function widget($args, $instance) {  
		
		global $wpdb;
		
		extract ( $args, EXTR_SKIP );

		if (!isset($instance['title'])) $instance['title'] = ""; 
		$title = $instance['title'];
		if (!isset($instance['page_id'])) $instance['page_id'] = 0; 
		$page_id = $instance['page_id'];
		
		echo $before_widget;

		if ($title) {
			echo $before_title . $title . $after_title;
		}
		
		$output = '<div class="progress-bars">';
					
				
				$skills = get_post_meta($page_id, '_wpalch_team-skills', TRUE);
				
					if(is_array($skills)) 
					{
						foreach( $skills as $skill ) 
						{
							
						$output .= '<div class="progress" data-skill="'.$skill['skill_level'].'">
								<div class="bar">'.$skill['skill_name'].' <strong>'.$skill['skill_level'].'%</strong></div>
								</div>';
							
						}
					}
					$output .= '</div>';
			
		
		echo $output;

		echo $after_widget;

	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
		if (!isset($instance['title'])) $instance['title'] = "";
		$title = esc_attr($instance['title']);
		if (!isset($instance['page_id'])) $instance['page_id'] = 0; 
		$page_id = $instance['page_id'];
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','Warp'); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
<?php
	} // End form()

} // End Class

function team_skills_widget_init() {
	register_widget("Firm_Team_Skills_Widget");
}
add_action('widgets_init','team_skills_widget_init');