<?php
/*
 *
 * Widget Audio
 * 
 */
if (!class_exists('Sfd_Audio_Widget'))
{
class Sfd_Audio_Widget extends WP_Widget {

	function Sfd_Audio_Widget() {
		$widget_ops = array('description' => 'Display your SoundCloud, MP3 or OGG audio');
		parent::__construct(false, '<i></i>SDF Audio', $widget_ops);      
	}

	function widget($args, $instance) {  
		
		global $wpdb;
		
		extract ( $args, EXTR_SKIP );

		if (!isset($instance['title'])) $instance['title'] = "Audio"; 
		$title = $instance['title'];
		if (!isset($instance['audio_type'])) $instance['audio_type'] = "soundcloud"; 
		$audio_type = $instance['audio_type'];
		if (!isset($instance['audio_url'])) $instance['audio_url'] = "http://www."; 
		$audio_url = $instance['audio_url'];
		if (!isset($instance['description'])) $instance['description'] = ""; 
		$description = $instance['description'];
		
		$unique_wid = $args['widget_id'];
		
		echo $before_widget;

		if ($title) {
			echo $before_title . $title . $after_title;
		}

		$audio_part = '';

		// SoundCloud
		if(sdf_is($audio_type, 'soundcloud'))
		$audio_part .= '<div id="'.$unique_wid.'" class="audio-container">'.do_shortcode('[sdf_soundcloud params="show_comments=false"]'.$audio_url.'[/sdf_soundcloud]').'</div>';

		// MP3 or OGG
		if (sdf_is($audio_type, 'mp3') || sdf_is($audio_type, 'ogg')) {

			$source_type = '';
			
			if(sdf_is($audio_type, 'mp3'))  { 
				$source_type = 'audio/mp3';  
			}
			elseif(sdf_is($audio_type, 'ogg'))  {  
				$source_type = 'audio/ogg';  
			}
				
				
			// Media Player
			$audio_part .= '<div id="'.$unique_wid.'" class="audio-container">'.do_shortcode('[audio type="'.$source_type.'"  src="'.$audio_url.'" controls="controls"]').'</div>';                
		}
		
		$audio_part .= '<p>'.$description.'</p>';

		echo $audio_part;
		
		echo $after_widget;

	}

	function update($new_instance, $old_instance) {                
		return $new_instance;
	}

	function form($instance) { 
		if (!isset($instance['title'])) $instance['title'] = "Audio";
		$title = esc_attr($instance['title']);
		if (!isset($instance['audio_type'])) $instance['audio_type'] = "soundcloud"; 
		$audio_type = $instance['audio_type'];
		if (!isset($instance['audio_url'])) $instance['audio_url'] = ""; 
		$audio_url = $instance['audio_url'];
		if (!isset($instance['description'])) $instance['description'] = ""; 
		$description = $instance['description'];
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'audio_type' ); ?>"><?php _e( 'Enable Box Frame:', SDF_TXT_DOMAIN ); ?></label>
			<select name="<?php echo $this->get_field_name( 'audio_type' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'audio_type' ); ?>">
				<option value="soundcloud"<?php selected( $audio_type, 'soundcloud' ); ?>><?php _e('Soundcloud Audio', SDF_TXT_DOMAIN); ?></option>
				<option value="mp3"<?php selected( $audio_type, 'mp3' ); ?>><?php _e('MP3 Audio', SDF_TXT_DOMAIN); ?></option>
				<option value="ogg"<?php selected( $audio_type, 'ogg' ); ?>><?php _e('OGG Audio', SDF_TXT_DOMAIN); ?></option>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('audio_url'); ?>"><?php _e('Audio URL:',SDF_TXT_DOMAIN); ?></label>
			<input type="text" name="<?php echo $this->get_field_name('audio_url'); ?>" value="<?php echo $audio_url; ?>" class="widefat" id="<?php echo $this->get_field_id('audio_url'); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description:',SDF_TXT_DOMAIN); ?></label>
			<textarea id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" class="widefat" rows="2" cols="20"><?php echo $description; ?></textarea>
		</p>
<?php
	} // End form()

} // End Class

function audio_widget_init() {
	register_widget("Sfd_Audio_Widget");
}
add_action('widgets_init','audio_widget_init');
}