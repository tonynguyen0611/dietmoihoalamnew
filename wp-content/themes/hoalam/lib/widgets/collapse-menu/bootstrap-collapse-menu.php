<?php

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
 * SDF_Collapse_Menu class
 **/
if (!class_exists('SDF_Collapse_Menu'))
{ 
class SDF_Collapse_Menu extends WP_Widget {

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {

		// load plugin text domain
		add_action( 'init', array( $this, 'widget_textdomain' ) );

		parent::__construct(
			'bootstrap-collapse-menu',
			__( '<i></i>SDF Collapse Menu', SDF_TXT_DOMAIN ),
			array(
				'classname'		=>	'SDF_Collapse_Menu',
				'description'	=>	__( 'Add a WordPress custom menu using the Bootstrap Collapse Layout.', SDF_TXT_DOMAIN )
			)
		);

		require_once( plugin_dir_path( __FILE__ ) . '/bootstrap-collapse-nav-walker.php' );

	} // end constructor

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param	array	args		The array of form elements
	 * @param	array	instance	The current instance of the widget
	 */
	public function widget( $args, $instance ) {

		extract( $args, EXTR_SKIP );

		echo $before_widget;

		echo $before_title . strip_tags( $instance['title'] ) . $after_title;

		// TODO:	Here is where you manipulate your widget's values based on their input fields

		include( plugin_dir_path( __FILE__ ) . 'views/widget.php' );

		echo $after_widget;

	} // end widget

	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param	array	new_instance	The new instance of values to be generated via the update.
	 * @param	array	old_instance	The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['menu']  = strip_tags( $new_instance['menu'] );
		$instance['panel_style']  = strip_tags( $new_instance['panel_style'] );

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param	array	instance	The array of keys and values for the widget.
	 */
	public function form( $instance ) {

    	// TODO:	Define default values for your variables
		$instance = wp_parse_args(
			(array) $instance,
			array( 'menu' => '', 'panel_style' => 'default', 'title' => '' )
		);

		// Display the admin form
		include( plugin_dir_path(__FILE__) . 'views/admin.php' );

	} // end form

	/*--------------------------------------------------*/
	/* Public Functions
	/*--------------------------------------------------*/

	/**
	 * Loads the Widget's text domain for localization and translation.
	 */
	public function widget_textdomain() {

		// TODO be sure to change 'widget-name' to the name of *your* plugin
		load_plugin_textdomain( SDF_TXT_DOMAIN, false, plugin_dir_path( __FILE__ ) . 'lang/' );

	} // end widget_textdomain

} // end class

// Load the widget on widgets_init
function sdf_collapse_menu_widget() {
	register_widget('SDF_Collapse_Menu');
}
add_action('widgets_init', 'sdf_collapse_menu_widget');
}