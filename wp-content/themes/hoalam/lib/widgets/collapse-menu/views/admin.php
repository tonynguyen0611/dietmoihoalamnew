<?php
$title = strip_tags( $instance['title'] );
$menu  = strip_tags( $instance['menu'] );
$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
$panel_style  = strip_tags( $instance['panel_style'] );
$panel_styles = array(
'primary' => 'Primary',
'default' => 'Default',
'success' => 'Success',
'info' => 'Info',
'warning' => 'Warning',
'danger' => 'Danger'
);

?>			
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', SDF_TXT_DOMAIN ); ?>: 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</label>
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'panel_style' ); ?>"><?php _e( 'Choose Panel Style', SDF_TXT_DOMAIN ); ?>: 
		<select id="<?php echo $this->get_field_id( 'panel_style' ); ?>" name="<?php echo $this->get_field_name( 'panel_style' ); ?>">

			<?php foreach( $panel_styles as $style_id => $style_name ): ?>

				<option value="<?php echo $style_id; ?>" <?php if( $panel_style == $style_id ): ?>selected="selected"<?php endif; ?>><?php echo $style_name; ?></option>

			<?php endforeach; ?>

		</select>
	</label>
</p>

<p>
	<label for="<?php echo $this->get_field_id( 'menu' ); ?>"><?php _e( 'Choose menu', SDF_TXT_DOMAIN ); ?>: 
		<select id="<?php echo $this->get_field_id( 'menu' ); ?>" name="<?php echo $this->get_field_name( 'menu' ); ?>">

			<?php foreach( $menus as $custom_menu ): ?>

				<option value="<?php echo $custom_menu->term_id; ?>" <?php if( $menu == $custom_menu->term_id ): ?>selected="selected"<?php endif; ?>><?php echo $custom_menu->name; ?></option>

			<?php endforeach; ?>

		</select>
	</label>
</p>