<?php
/*
Plugin Name: Google Maps Widget
Plugin URI: http://www.googlemapswidget.com/
Description: Display a single-image super-fast loading Google map in a widget. A larger, full featured map is available on click in a lightbox.
Author: Web factory Ltd
Version: 1.30
Author URI: http://www.webfactoryltd.com/

  Copyright 2013 - 2014  Web factory Ltd  (email : info@webfactoryltd.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


if (!function_exists('add_action')) {
  die('Please don\'t open this file directly!');
}


define('GM_WG_VER', '1.30');
require_once 'gmw-widget.php';


class GMW {
  // hook everything up
   static function init() {
      if (is_admin()) {
        // check if minimal required WP version is used
        self::check_wp_version(3.3);

        // enqueue admin scripts
        add_action('admin_enqueue_scripts', array(__CLASS__, 'admin_enqueue_scripts'));
      } else {
        // enqueue frontend scripts
        add_action('wp_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'));
        add_action('wp_footer', array(__CLASS__, 'dialogs_markup'));
      }
  } // init

  // initialize widgets
  static function widgets_init() {
    register_widget('GoogleMapsWidget');
  } // widgets_init



  // check if user has the minimal WP version required by the plugin
  static function check_wp_version($min_version) {
    if (!version_compare(get_bloginfo('version'), $min_version,  '>=')) {
        add_action('admin_notices', array(__CLASS__, 'min_version_error'));
    }
  } // check_wp_version


  // display error message if WP version is too low
  static function min_version_error() {
    echo '<div class="error"><p>' . sprintf('Google Maps Widget <b>requires WordPress version 3.3</b> or higher to function properly. You are using WordPress version %s. Please <a href="%s">update it</a>.', get_bloginfo('version'), admin_url('update-core.php')) . '</p></div>';
  } // min_version_error


  // print dialogs markup in footer
  static function dialogs_markup() {
       $out = '';
       $widgets = GoogleMapsWidget::$widgets;

       if (!$widgets) {
         wp_dequeue_script('gmw');
         return;
       }

       foreach ($widgets as $widget) {
         if ($widget['bubble']) {
           $iwloc = 'addr';
         } else {
           $iwloc = 'near';
         }
         if ($widget['ll']) {
           $ll = '&amp;ll=' . $widget['ll'];
         } else {
           $ll = '';
         }

         $lang = substr(@$_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
         if (!$lang) {
           $lang = 'en';
         }

         $map_url = '//maps.google.com/maps?hl=' . $lang . '&amp;ie=utf8&amp;output=embed&amp;iwloc=' . $iwloc . '&amp;iwd=1&amp;mrt=loc&amp;t=' . $widget['type'] . '&amp;q=' . urlencode(remove_accents($widget['address'])) . '&amp;z=' . urlencode($widget['zoom']) . $ll;

         $out .= '<div class="gmw-dialog clearfix hide" style="width:' . $widget['width'] . 'px;height:' . $widget['height'] . 'px" data-map-skin="' . $widget['skin'] . '" id="gmw-dialog-' . $widget['id'] . '" title="' . esc_attr($widget['title']) . '">';
         if ($widget['header']) {
          $out .= '<div class="gmw-header"><i>' . do_shortcode($widget['header']) . '</i></div>';
         }
         $out .= '<div class="gmw-map"><iframe width="' . $widget['width'] . 'px" height="' . $widget['height'] . 'px" src="' . $map_url . '"></iframe></div>';
         if ($widget['footer']) {
          $out .= '<div class="gmw-footer"><i>' . do_shortcode($widget['footer']) . '</i></div>';
         }
         $out .= "</div>\n";
       } // foreach $widgets

       echo $out;
   } // run_scroller


   // enqueue frontend scripts if necessary
   static function enqueue_scripts() {
     if (is_active_widget(false, false, 'googlemapswidget', true)) {
       wp_enqueue_style('gmw', SDF_WIDGETS_URL . "/google-maps/css/gmw.css", array(), GM_WG_VER);
       wp_enqueue_script('gmw', SDF_WIDGETS_URL . "/google-maps/js/gmw.js", array(), GM_WG_VER, true);
     }
    } // enqueue_scripts


    // enqueue CSS and JS scripts on widgets page
    static function admin_enqueue_scripts() {
      if (self::is_plugin_admin_page()) {
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('gmw-cookie', SDF_WIDGETS_URL . "/google-maps/js/jquery.cookie.js", array('jquery'), GM_WG_VER, true);
        wp_enqueue_script('gmw-admin', SDF_WIDGETS_URL . "/google-maps/js/gmw-admin.js", array('jquery'), GM_WG_VER, true);
        wp_enqueue_style('gmw-admin', SDF_WIDGETS_URL . "/google-maps/css/gmw-admin.css", array(), GM_WG_VER);
      } // if
    } // admin_enqueue_scripts


    // check if plugin's admin page is shown
    static function is_plugin_admin_page() {
      $current_screen = get_current_screen();

      if ($current_screen->id == 'widgets') {
        return true;
      } else {
        return false;
      }
    } // is_plugin_admin_page


    // helper function for creating dropdowns
    static function create_select_options($options, $selected = null, $output = true) {
        $out = "\n";

        foreach ($options as $tmp) {
            if ($selected == $tmp['val']) {
                $out .= "<option selected=\"selected\" value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            } else {
                $out .= "<option value=\"{$tmp['val']}\">{$tmp['label']}&nbsp;</option>\n";
            }
        } // foreach

        if ($output) {
            echo $out;
        } else {
            return $out;
        }
    } // create_select_options


  static function get_coordinates($address, $force_refresh = false) {
    $address_hash = md5('gmw' . $address);
    if ($force_refresh || ($coordinates = get_transient($address_hash)) === false) {
      $url = 'https://maps.googleapis.com/maps/api/geocode/xml?address=' . urlencode($address) . '&sensor=false';

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $xml = curl_exec($ch);
      $ch_info = curl_getinfo($ch);
      curl_close($ch);

      if ($ch_info['http_code'] == 200) {
        $data = new SimpleXMLElement($xml);
        if ($data->status == 'OK') {
          $cache_value['lat']     = (string) $data->result->geometry->location->lat;
          $cache_value['lng']     = (string) $data->result->geometry->location->lng;
          $cache_value['address'] = (string) $data->result->formatted_address;

          // cache coordinates for 3 months
          set_transient($address_hash, $cache_value, 3600*24*30*3);
          $data = $cache_value;
        } elseif (!$data->status) {
          return false;
        } else {
          return false;
        }
      } else {
         return false;
      }
    } else {
       // data is cached, get it
       $data = get_transient($address_hash);
    }

    return $data;
  } // get_coordinates

  // track a few things on plugin activation
  // NO DATA is sent anywhere!
  static function activate() {
    $options = get_option('gmw_options');

    if (!isset($options['first_version']) || !isset($options['first_install'])) {
      $options['first_version'] = GM_WG_VER;
      $options['first_install'] = current_time('timestamp');
      update_option('gmw_options', $options);
    }
  } // activate
} // class GMW


// hook everything up
register_activation_hook(__FILE__, array('GMW', 'activate'));
add_action('init', array('GMW', 'init'));
add_action('plugins_loaded', array('GMW', 'plugins_loaded'));
add_action('widgets_init', array('GMW', 'widgets_init'));