;(function($) { 
    "use strict";
jQuery(document).ready(function($) {

	$('a.gmw-thumbnail-map.prettyphoto-map[data-rel]').each(function() {
		$(this).attr('rel', $(this).data('rel'));
		var theme = $(this).data("theme");
		var content = $($(this).data("mapid")).html();
		$(this).prettyPhoto({
			custom_markup: content, 
			theme: theme,
			show_title: false, 
			animation_speed:'fast',
			slideshow:4500, 
			hideflash: true, 
			social_tools: false
		});
	});
	
});
}(jQuery));