<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: sdf_include.php
 * DESCRIPTIONS: Theme Core Includes.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: ULTIncludes
 * Descripton: Implementation of Theme Includes Options    
 */
 
 
if ( !class_exists('ULTIncludes') ) {  
class ULTIncludes
{

//Constructor
public function __construct() { 
}

protected $_sdfGlobalArgs = array(
	'ads' => array(),
	'portfolio' => array(),
	'breadcrumbs' => array(),
	'social_media' => array(),
	'favtouch_icons' => array(),
	'animations' => array(),
	'footer' => array(),
	'layout' => array(),
	'header' => array(),
	'title' => array(),
	'lightbox' => array(),
	'logo'=> array(),
	'navigation' => array(),
	'preset' => array(),
	'preset_colors' => array(),
	'slider' => array(),
	'typography' => array(),
	'misc' => array(),
	'blog' => array()
);	

public function setupGlobalArgs(){
	$this->_sdfGlobalArgs['preset'] = $this->_ultPreset;
	$this->_sdfGlobalArgs['ads'] = $this->_ultAds;
	$this->_sdfGlobalArgs['portfolio'] = $this->_ultPortfolio;
	$this->_sdfGlobalArgs['breadcrumbs'] = $this->_ultBreadcrumbs;
	$this->_sdfGlobalArgs['social_media'] = $this->_ultSocialMedia;
	$this->sdfFollowIcons();
	$this->_sdfGlobalArgs['favtouch_icons'] = $this->_ultFavTouchIcons;
	$this->_sdfGlobalArgs['animations'] = $this->_ultEntranceAnimations;
	$this->_sdfGlobalArgs['footer'] = $this->_ultFooter;
	$this->_sdfGlobalArgs['layout'] = $this->_ultLayout;
	$this->_sdfGlobalArgs['header'] = $this->_ultHeader;
	$this->_sdfGlobalArgs['title'] = $this->_ultTitle;
	$this->_sdfGlobalArgs['lightbox'] = $this->_ultLightbox;
	$this->_sdfGlobalArgs['logo'] = $this->_ultLogo;
	$this->_sdfGlobalArgs['navigation'] = $this->_ultNav;
	$this->_sdfGlobalArgs['misc'] = $this->_ultMisc;
	$this->_sdfGlobalArgs['blog'] = $this->_ultBlog;
	$this->sidebarsTypography();
	$this->tbsTypography();
	$this->buttonsStyling();
	$this->followButtonsStyling();
	$this->sharingButtonsStyling();
	$this->gridImagesStyling();
	$this->postFormatButtonsStyling();
	$this->scrollToTopButtonStyling();
	$this->_sdfGlobalArgs['typography'] = $this->_ultTypography;
	ksort($this->_ultIcons);
	$this->_ultIcons = array_merge( array('No Icon' => 'no'), $this->_ultIcons );
	
}

public $_ultPreset = array(
'theme_preset' => '1',
'theme_presets' => array()
);

public $_ultAds = array();
public $_ultPortfolio = array(
	'slug' => 'portfolio'
);

public $_ultBreadcrumbs = array(
'toggle_breadcrumbs' => '1',
'bread_delim' => '&raquo;',
'toggle_bread_home' => '1',
'bread_home_text' => 'Home'
);

public $_ultSocialMedia = array(
'toggle_social_share' => '',
'social_share_position' => 'below',
'social_share_animation_type' => '',
'social_share_animation_duration' => '',
'social_share_title' => '',
'social_share_twitter_handle' => '',
'toggle_google_plus_share' => '',
'toggle_facebook_share' => '',
'toggle_twitter_share' => '',
'toggle_linkedin_share' => '',
'toggle_pinterest_share' => '',
'toggle_tumblr_share' => '',
'toggle_vk_share' => '',
'toggle_reddit_share' => '',
'toggle_envelope_share' => ''
);

public $_ultFavTouchIcons = array(
'favicon' => '',
'apple_touch_icon_57' => '',
'apple_touch_icon_72' => '',
'apple_touch_icon_114' => '',
'apple_touch_icon_144' => ''
);

public $_ultEntranceAnimations = array(
'toggle_animations' => '1',
'default_animation_duration' => '',
'feat_img_animate' => '1',
'feat_img_animation_type' => '',
'feat_img_animation_duration' => '',
'animation_types' => array ('No','bounce', 'bounceIn', 'bounceInUp', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceOut', 'bounceOutUp', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'rotateIn', 'rotateInUpLeft', 'rotateInDownLeft', 'rotateInUpRight', 'rotateInDownRight', 'rotateOut', 'rotateOutUpLeft', 'rotateOutDownLeft', 'rotateOutUpRight', 'rotateOutDownRight', 'rollIn', 'rollOut', 'lightSpeedIn', 'lightSpeedOut', 'tada', 'swing', 'wobble', 'pulse', 'flip', 'flipInX', 'flipOutX', 'flipInY', 'flipOutY', 'fadeIn', 'fadeInUp', 'fadeInDown', 'fadeInLeft', 'fadeInRight', 'fadeInUpBig', 'fadeInDownBig', 'fadeInLeftBig', 'fadeInRightBig', 'fadeOut', 'fadeOutUp', 'fadeOutDown', 'fadeOutLeft', 'fadeOutRight', 'fadeOutUpBig', 'fadeOutDownBig', 'fadeOutLeftBig', 'fadeOutRightBig', 'slideInDown', 'slideInLeft', 'slideInRight', 'slideOutLeft', 'slideOutRight', 'slideOutUp', 'hinge', 'flash', 'shake'),
'animation_duration' => array (
'Default' => '',
'0.2 seconds' => 'animated02',
'0.3 seconds' => 'animated03',
'0.4 seconds' => 'animated04',
'0.5 seconds' => 'animated05',
'0.6 seconds' => 'animated06',
'0.7 seconds' => 'animated07',
'0.8 seconds' => 'animated08',
'0.9 seconds' => 'animated09',
'1 second' => 'animated',
'1.2 seconds' => 'animated12',
'1.4 seconds' => 'animated14',
'1.6 seconds' => 'animated16',
'1.8 seconds' => 'animated18',
'2 seconds' => 'animated20'
)
);

public $_ultFooter = array(
'toggle_footer' => '',
'toggle_custom_footer' => '',
'custom_nav_name' => 'wpu-select-default',
'footer_bottom_alignment' => '',
'copyright_footer' => ''
);	 

public $_ultLayout = array(
'page_width' => 'wide',
'theme_layout' => 'col_right_2',
'page_sidebars'=> '3',
'custom_layout' => '',
'custom_left'=>'',
'custom_center'=>'',
'custom_right'=>'',
'toggle_page_layout'=>'global'
);

public $_smWidths = array (
'1/12' => 'col-md-1',
'1/6' => 'col-md-2',
'1/4' => 'col-md-3',
'1/3' => 'col-md-4',
'5/12' => 'col-md-5',
'1/2' => 'col-md-6',
'7/12' => 'col-md-7',
'2/3' => 'col-md-8',
'3/4' => 'col-md-9',
'5/6' => 'col-md-10',
'11/12' => 'col-md-11',
'1/1' => 'col-md-12'
);

public $_ultHeader = array(
'toggle_header' => '1',
'toggle_nav' => '1',
'toggle_custom_nav' => '0',
'custom_nav_name' => 'wpu-select-default',
'header_order' => array(),
'box_data' => array(
			'listItem_1'=> '1/2',
			'listItem_2'=> '1/2',
			'listItem_3'=> '1/2',
			'listItem_4'=> '1/2',
			'listItem_5'=> '1/4',
			'listItem_6'=> '3/4',
			'listItem_7'=> '1/1',
			'listItem_8'=> '1/2'
			),
'box_data_classes' => array(
			'listItem_1'=> '',
			'listItem_2'=> '',
			'listItem_3'=> '',
			'listItem_4'=> '',
			'listItem_5'=> '',
			'listItem_6'=> '',
			'listItem_7'=> '',
			'listItem_8'=> ''
			)
);							
public $_ultTitle = array(
'toggle_page_title' => '1',
'title_header' => 'h1',
'toggle_custom_title' => '',
'title_custom_link' => '',
'title_custom_text'=>'',
'subtitle_header' => '',
'subtitle_text' => ''
);

public $_ultLightbox = array(
'toggle_lightbox' => '',
'lightbox_animation_type' => '',
'lightbox_animation_types' => array ( '1' => 'Fade in & Scale', '2' => 'Slide in (right)', '3' => 'Slide in (bottom)', '4' => 'Newspaper', '5' => 'Fall', '6' => 'Side Fall', '7' => 'Sticky Up', '8' => '3D Flip (horizontal)', '9' => '3D Flip (vertical)', '10' => '3D Sign', '11' => 'Super Scaled', '12' => 'Just Me', '13' => '3D Slit', '14' => '3D Rotate Bottom', '15' => '3D Rotate In Left' ),
'lightbox_sizes' => array( 'lg' => 'Large', 'sm' => 'Small' ),
'lightbox_size' => '',
'modal_lightbox_positions' => array( '' => 'Default', 'modal-vertical-center' => 'Vertically Centered', 'modal-bottom-right' => 'Bottom Right', 'modal-bottom-center' => 'Bottom Center', 'modal-bottom-left' => 'Bottom Left' )
);

public $_ultLogo = array(
'toggle_tagline' => 'off',
'logo_type' => 'off',
'logo_path' => array('file'=>'','url'=>'')
);

public $_ultNav = array(
'menu_layout'=>'sticky_menu',
'sticky_menu_opacity'=>'0.9',
'menu_alignment'=>'right',
'menu_alignment_2'=>'right',
'menu_style'=>'default',
'menu_styles' => array( 'default' => 'Default' ),
'menu_settings' => array(
	'theme_navbar_append_search' => '',
	'theme_navbar_box_shadow' => '',
	'theme_navbar_box_shadow_color' => '',
	'theme_navbar_transform'=> '',
	'theme_navbar_weight'=> '',
	'theme_navbar_size'=> '',
	'theme_navbar_size_type'=> '',
	'theme_navbar_letter_spacing'=> '',
	'theme_navbar_family'=> '',
	'theme_navbar_custom_bg' => 'no',
	'theme_navbar_bg_color' => '',
	'theme_navbar_hover_bg_color' => '',
	'theme_navbar_sticky_bg_color' => '',
	'theme_navbar_top_bg_color' => '',
	'theme_navbar_bottom_bg_color' => '',
	'theme_navbar_bg_image' => '',
	'theme_navbar_bgrepeat' => '',
	'theme_navbar_par_bg_ratio' => '',
	'theme_navbar_border_style' => '',
	'theme_navbar_border_radius' => '',
	'theme_navbar_border_width' => '',
	'theme_navbar_border_color' => '',
	'theme_navbar_margin_top' => '',
	'theme_navbar_margin_bottom' => '',
	'theme_navbar_transition' => '',
	'theme_navbar_link_margin_top' => '',
	'theme_navbar_link_margin_bottom' => '',
	'theme_navbar_link_padding_top' => '',
	'theme_navbar_link_padding_bottom' => '',
	'theme_navbar_link_padding_left_right' => '',
	'theme_navbar_link_divider_width' => '',
	'theme_navbar_link_divider_color' => '',
	'theme_navbar_text_color' => '',
	'theme_navbar_link_color' => '',
	'theme_navbar_link_text_decoration' => '',
	'theme_navbar_link_bg_color' => '',
	'theme_navbar_link_top_bg_color' => '',
	'theme_navbar_link_bottom_bg_color' => '',
	'theme_navbar_link_box_shadow' => '',
	'theme_navbar_link_box_shadow_color' => '',
	'theme_navbar_link_hover_color' => '',
	'theme_navbar_link_hover_text_decoration' => '',
	'theme_navbar_link_hover_bg_color' => '',
	'theme_navbar_link_hover_top_bg_color' => '',
	'theme_navbar_link_hover_bottom_bg_color' => '',
	'theme_navbar_link_hover_box_shadow' => '',
	'theme_navbar_link_hover_box_shadow_color' => '',
	'theme_navbar_link_active_color' => '',
	'theme_navbar_link_active_bg_color' => '',
	'theme_navbar_link_active_top_bg_color' => '',
	'theme_navbar_link_active_bottom_bg_color' => '',
	'theme_navbar_link_active_box_shadow' => '',
	'theme_navbar_link_active_box_shadow_color' => '',
	'theme_navbar_toggle_hover_bg_color' => '',
	'theme_navbar_toggle_icon_bar_bg_color' => '',
	'theme_navbar_toggle_icon_bar_hover_bg_color' => '',
	'theme_navbar_toggle_border_color' => '', 
	'theme_navbar_drop_link_padding' => '',
	'theme_navbar_drop_link_height' => '',
	'theme_navbar_drop_color' => '',
	'theme_navbar_drop_hover_color' => '',
	'theme_navbar_drop_text_decoration' => '',
	'theme_navbar_drop_hover_text_decoration' => '',
	'theme_navbar_drop_acolor' => '',
	'theme_navbar_drop_ahover' => '',
	'theme_navbar_drop_box_shadow' => '',
	'theme_navbar_drop_box_shadow_color' => '',
	'theme_navbar_drop_border_style'=> '',
	'theme_navbar_drop_border_radius'=> '',
	'theme_navbar_drop_border_width'=> '',
	'theme_navbar_drop_border_color'=> '',
	'theme_navbar_drop_transform'=> '',
	'theme_navbar_drop_weight'=> '',
	'theme_navbar_drop_letter_spacing'=> '',
	'theme_navbar_drop_size'=> '',
	'theme_navbar_drop_size_type'=> '',
	'theme_navbar_drop_family'=> '',
	'theme_navbar_drop_transition'=> ''
)
);

public $_ultMisc = array(
'toggle_placeholders' =>'',
'toggle_maintenance_mode' =>'',
'theme_maintenance_page' =>'',
'theme_short_codes'=> array('code'=>array(),'content'=>array()),
'theme_analytics'=>'',
'custom_analytic'=>'',
'favicon'=>'',
'theme_options_io' => '',
'custom_js_header' => '',
'custom_js_footer' => '',
'toggle_nicescroll' => '',
'toggle_scrollto' => '',
'scrolltotop_easing_type' => '',
'scrolltotop_duration' => '',
'toggle_yt_bg_video' => '',
'toggle_autosave' => '',
'auto_update_username' => '',
'auto_update_email' => '',
'toggle_admin_bar' => '',
'toggle_design_toolbar_menu' => 1,
'woo_columns' => 3,
'woo_products_per_page' => 9,
'woo_related_columns' => 4,
'woo_related_products_per_page' => 4,
'toggle_h5bp_htaccess' => '',
'toggle_5g_htaccess' => '',
'image_size_12_w' => '1200',
'image_size_12_h' => '640',
'image_size_6_w' => '570',
'image_size_6_h' => '320',
'image_size_4_w' => '380',
'image_size_4_h' => '215',
'image_size_3_w' => '285',
'image_size_3_h' => '160',
'image_size_2_w' => '190',
'image_size_2_h' => '110'
);

public $_ultBlog = array(
'excerpt_length' => '',
'read_more_text' => '',
'toggle_full_posts' => 0,
'toggle_featured_image' => 1,
'toggle_single_featured_image' => 1,
'toggle_page_featured_image' => 1,
'toggle_single_comments' => 1,
'toggle_page_comments' => 0,
'toggle_portfolio_comments' => 0,
'post_title_position' => 0,
'toggle_related_posts' => 1,
'related_posts_count' => 5,
'toggle_post_format_button' => '',
'toggle_post_author_box' => '',
'toggle_post_meta_time' => '',
'toggle_post_meta_categories' => '',
'toggle_post_meta_author' => '',
'toggle_post_meta_tags' => '',
'toggle_post_meta_comments' => ''
);

public $_ultTextDecoration = array(
'None' => 'none',
'Underline' => 'underline',
'Overline' => 'overline',
'Line Through' => 'line-through'
);

public $_ultTypography = array(
'preset_colors' => array(),
'google_web_font'=>array(),
'theme_font_families'=> array(
			'Arial,Helvetica,sans-serif',
			'Arial Black,Gadget,sans-serif',
			'Comic Sans MS,cursive',
			'Courier New,Courier,monospace',
			'Georgia,serif',
			'Helvetica Neue,Helvetica,Arial,sans-serif',
			'Impact,Charcoal,sans-serif',
			'Lucida Console,Monaco,monospace',
			'Lucida Sans Unicode,Lucida Grande,sans-serif',
			'Palatino Linotype,Book Antiqua,Palatino,serif',
			'Tahoma,Geneva,sans-serif',
			'Times New Roman,Times,serif',
			'Trebuchet MS,Helvetica,sans-serif',
			'Verdana,Geneva,sans-serif' 
			),
'theme_font_wide_bg_color'=>'',
'theme_font_wide_bg_image'=>'',
'theme_font_wide_bg_repeat'=> '',
'theme_font_boxed_bg_color'=>'',
'theme_font_boxed_bg_image' => '',
'theme_font_boxed_bg_repeat'=>'',
'theme_font_boxed_box_bg_color'=>'',
'theme_font_boxed_box_bg_image' => '',
'theme_font_boxed_box_bg_repeat'=>'',
'theme_font_boxed_box_margin'=>'',
'theme_font_boxed_box_padding'=>'',
'theme_font_boxed_box_shadow'=>'',
'theme_font_boxed_box_shadow_color'=>'',
'theme_font_boxed_box_border_color'=>'',
'theme_font_boxed_box_border_width'=>'',
'theme_font_boxed_box_border_width_type'=>'',
'theme_font_boxed_box_border_style'=>'',
'theme_font_ahover'=> '',
'theme_font_anchor'=> '',
'theme_font_color'=> '',
'theme_font_link_color' => '',
'theme_font_link_bg_color' => '',
'theme_font_link_text_decoration' => '',
'theme_font_link_box_shadow' => '',
'theme_font_link_box_shadow_color' => '',
'theme_font_link_transition' => '',
'theme_font_link_hover_color' => '',
'theme_font_link_hover_bg_color' => '',
'theme_font_link_hover_text_decoration' => '',
'theme_font_link_hover_box_shadow' => '',
'theme_font_link_hover_box_shadow_color' => '',
'theme_font_size'=> '',
'theme_font_family'=> '',
'theme_font_title_size' => '',
'theme_font_title_weight' => '',
'theme_font_title_color' => '',
'theme_font_title_size_type'=> '',
'theme_font_subtitle_size_type'=> '',
'theme_font_subtitle_size'=> '',
'theme_font_subtitle_weight'=> '',
'theme_font_subtitle_family'=> '',
'theme_font_subtitle_color'=> '',
'theme_font_h1_margin_top'=> '',
'theme_font_h2_margin_top'=> '',
'theme_font_h3_margin_top'=> '',
'theme_font_h4_margin_top'=> '',
'theme_font_h5_margin_top'=> '',
'theme_font_h6_margin_top'=> '',
'theme_font_h1_margin_bottom'=> '',
'theme_font_h2_margin_bottom'=> '',
'theme_font_h3_margin_bottom'=> '',
'theme_font_h4_margin_bottom'=> '',
'theme_font_h5_margin_bottom'=> '',
'theme_font_h6_margin_bottom'=> '',
'theme_font_h1_family'=> '',
'theme_font_h2_family'=> '',
'theme_font_h3_family'=> '',
'theme_font_h4_family'=> '',
'theme_font_h5_family'=> '',
'theme_font_h6_family'=> '',
'theme_font_h1_color'=> '',
'theme_font_h2_color'=> '',
'theme_font_h3_color'=> '',
'theme_font_h4_color'=> '',
'theme_font_h5_color'=> '',
'theme_font_h6_color'=> '',
'theme_font_h1_letter_spacing'=> '',
'theme_font_h2_letter_spacing'=> '',
'theme_font_h3_letter_spacing'=> '',
'theme_font_h4_letter_spacing'=> '',
'theme_font_h5_letter_spacing'=> '',
'theme_font_h6_letter_spacing'=> '',
'theme_font_h1_size'=> '',
'theme_font_h2_size'=> '',
'theme_font_h3_size'=> '',
'theme_font_h4_size'=> '',
'theme_font_h5_size'=> '',
'theme_font_h6_size'=> '',
'theme_font_h1_size_type'=> '',
'theme_font_h2_size_type'=> '',
'theme_font_h3_size_type'=> '',
'theme_font_h4_size_type'=> '',
'theme_font_h5_size_type'=> '',
'theme_font_h6_size_type'=> '',
'theme_font_h1_weight'=> '',
'theme_font_h2_weight'=> '',
'theme_font_h3_weight'=> '',		 
'theme_font_h4_weight'=> '',		 
'theme_font_h5_weight'=> '',		 
'theme_font_h6_weight'=> '',	
'theme_font_list_padding' => '',
'theme_font_list_margin' => '',
'theme_font_list_item_line_height' => '',
'theme_font_list_style' => 'default',
'theme_font_custom_list_styling' => 'no',
'theme_font_custom_list_padding' => '',
'theme_font_custom_list_margin' => '',
'theme_font_custom_list_align' => '',
'theme_font_custom_list_font_size' => '',
'theme_font_custom_list_font_size_type' => '',
'theme_font_custom_list_family' => '',
'theme_font_custom_list_font_color' => '',
'theme_font_custom_list_link_color' => '',
'theme_font_custom_list_link_hover_color' => '',
'theme_font_custom_list_link_text_decoration' => '',
'theme_font_custom_list_link_hover_text_decoration' => '',
'theme_font_custom_list_style' => 'default',
'theme_font_custom_list_icon' => '',
'theme_font_custom_list_icon_size' => '',
'theme_font_custom_list_icon_size_type' => '',
'theme_font_custom_list_icon_margin' => '',
'theme_font_custom_list_item_padding' => '',
'theme_font_custom_list_item_margin' => '',
'theme_font_custom_list_item_line_height' => '',
'theme_font_custom_list_item_border_width' => '',
'theme_font_custom_list_item_border_style' => '',
'theme_font_custom_list_item_border_radius' => '',
'theme_font_custom_list_item_background_color' => '',
'theme_font_custom_list_item_border_color' => '',
'theme_font_custom_list_item_box_shadow' => '',
'theme_font_custom_list_item_box_shadow_color' => '',
'theme_font_custom_list_item_background_hover_color' => '',
'theme_font_custom_list_item_border_hover_color' => '',
'theme_font_custom_list_item_hover_box_shadow' => '',
'theme_font_custom_list_item_hover_box_shadow_color' => '',	 
'footer_equal_heights'=> 'off',
'footer_link_color'=> '',
'footer_link_bg_color'=> '',
'footer_link_box_shadow'=> '',
'footer_link_box_shadow_color'=> '',
'footer_link_hover_color' => '',
'footer_link_hover_bg_color' => '',
'footer_link_hover_box_shadow' => '',
'footer_link_hover_box_shadow_color' => '',
'footer_font_weight'=> '',
'footer_font_size'=> '',
'footer_font_size_type'=> '',
'footer_font_family'=> '', 
'footer_font_color'=> '',
'footer_font_hover_color' => '',
'footer_custom_bg' => 'no',
'footer_bg_color' => '',
'footer_bg_image' => '',
'footer_par_bg_ratio' => '',
'footer_bgrepeat' => '',
'footer_box_shadow' => '',
'footer_box_shadow_color' => '',
'footer_bottom_custom_bg' => 'no',
'footer_bottom_bg_color'=> '',
'footer_bottom_bg_image'=>'',
'footer_bottom_par_bg_ratio' => '',
'footer_bottom_bgrepeat'=>'',
'footer_bottom_box_shadow'=>'',
'footer_bottom_box_shadow_color'=>'',
'footer_bottom_font_color'=> '',
'footer_bottom_font_hover_color' => '',
'footer_bottom_link_color'=> '',
'footer_bottom_link_hover_color' => '',
'footer_bottom_font_size'=> '',
'footer_bottom_font_size_type'=> '',
'footer_bottom_font_family'=> '', 
'footer_bottom_font_weight'=> '', 
'header_font_color'=> '',
'header_font_hover_color' => '',
'header_link_color'=> '',
'header_link_hover_color' => '',
'header_font_size'=> '',
'header_font_size_type'=> '',
'header_font_family'=> '',
'header_font_weight'=> '',
'theme_silo_font_size'=> '',
'theme_silo_font_size_type'=> '',
'theme_silo_font_color'=> '',
'theme_silo_line_height' =>'',
'logo_heading'=> 'none',
'logo_font_color'=> '',
'logo_font_hover_color' => '',
'logo_link_color'=> '',
'logo_link_hover_color' => '',
'logo_font_weight'=> '',
'logo_font_size'=> '',
'logo_font_size_type'=> '',
'logo_font_family'=> '',
'logo_align'=> 'left',
'logo_padding_top'=> '',
'logo_padding_bottom'=> '',
'logo_custom_bg' => 'no',
'logo_bg_color' => '',
'logo_bg_image' => '',
'logo_par_bg_ratio' => '',
'logo_bgrepeat' => '',
'logo_box_shadow' => '',
'logo_box_shadow_color' => '',
'logo_tagline_margin'=> '',
'logo_tagline_heading'=> '',
'logo_tagline_font_color'=> '',
'logo_tagline_font_hover_color' => '',
'logo_tagline_font_weight'=> '',
'logo_tagline_font_size'=> '',
'logo_tagline_font_size_type'=> '',
'logo_tagline_font_family'=> '',
'header_custom_bg' => 'no',
'header_bg_color' => '',
'header_bg_image' => '',
'header_bgrepeat' => '',
'header_par_bg_ratio' => '',
'header_box_shadow' => '',
'header_box_shadow_color' => '',
'entry_title_padding_top'=> '',
'entry_title_padding_bottom'=> '',
'page_header_padding_top'=> '',
'page_header_padding_bottom'=> '',
'page_header_alignment' => 'left',
'page_header_link_text_decoration' => '',
'page_header_link_hover_text_decoration' => '',
'page_header_font_family' => '',
'page_header_font_weight' => '',
'page_header_font_size_type' => '',
'page_header_font_size' => '',
'page_header_font_text_transform' => 'no',
'page_header_font_color' => '',
'page_header_font_hover_color' => '',
'page_header_link_color' => '',
'page_header_link_hover_color' => '',
'page_header_font_subtitle_family' => '',
'page_header_font_subtitle_weight' => '',
'page_header_font_subtitle_size_type' => '',
'page_header_font_subtitle_size' => '',
'page_header_font_subtitle_text_transform' => 'no',
'page_header_font_subtitle_color' => '',
'page_header_custom_bg' => 'no',
'page_header_bg_color' => '',
'page_header_bg_image' => '',
'page_header_bgrepeat' => '',
'page_header_par_bg_ratio' => '',
'page_header_box_shadow' => '',
'page_header_box_shadow_color' => '',
'blockquote_padding'=> '',
'blockquote_margin'=> '',
'blockquote_link_text_decoration' => '',
'blockquote_link_hover_text_decoration' => '',
'blockquote_font_family' => '',
'blockquote_font_weight' => '',
'blockquote_font_size_type' => '',
'blockquote_font_size' => '',
'blockquote_font_text_transform' => 'no',
'blockquote_font_color' => '',
'blockquote_font_hover_color' => '',
'blockquote_link_color' => '',
'blockquote_link_hover_color' => '',
'blockquote_font_author_family' => '',
'blockquote_font_author_weight' => '',
'blockquote_font_author_size_type' => '',
'blockquote_font_author_size' => '',
'blockquote_font_author_text_transform' => 'no',
'blockquote_font_author_color' => '',
'blockquote_custom_bg' => 'no',
'blockquote_bg_color' => '',
'blockquote_bg_image' => '',
'blockquote_bgrepeat' => '',
'blockquote_par_bg_ratio' => '',
'blockquote_box_shadow' => '',
'blockquote_box_shadow_color' => '',
'blockquote_border_radius' => '',
'blockquote_border_style' => '',
'blockquote_border_width' => '',
'blockquote_border_color' => '',
'breadcrumbs_alignment' => '',
'breadcrumbs_font_family' => '',
'breadcrumbs_font_size_type' => '',
'breadcrumbs_font_size' => '',
'breadcrumbs_font_color' => '',
'breadcrumbs_font_weight' => '',
'breadcrumbs_font_hover_color' => '',
'breadcrumbs_link_color' => '',
'breadcrumbs_link_hover_color' => '',
'breadcrumbs_custom_bg' => 'no',
'breadcrumbs_bg_color' => '',
'breadcrumbs_bg_image' => '',
'breadcrumbs_bgrepeat' => '',
'breadcrumbs_par_bg_ratio' => '',
'breadcrumbs_box_shadow' => '',
'breadcrumbs_box_shadow_color' => '',
'theme_button_text_family' => '',
'theme_button_letter_spacing' => '',
'theme_button_text_weight' => '',
'theme_button_text_transform' => 'no',
'theme_button_border_style' => '',
'theme_button_border_width' => '',
'theme_button_transition' => '',
'theme_post_format_button_icon_toggle' => '',
'theme_post_format_button_icon_size' => '',
'theme_post_format_button_text_family' => '',
'theme_post_format_button_text_size' => '',
'theme_post_format_button_text_size_type' => '',
'theme_post_format_button_text_weight' => '',
'theme_post_format_button_text_transform' => 'no',
'theme_post_format_button_border_style' => '',
'theme_post_format_button_border_width' => '',
'theme_post_format_button_border_radius' => '',
'theme_post_format_button_padding' => '',
'theme_post_format_button_margin' => '',
'theme_link_button_text_color' => '',
'theme_link_button_text_hover_color' => '',
'theme_primary_color' => '',
'page_padding_top'=> '',
'page_padding_bottom'=> '',
'featured_image_padding_top'=> '',
'featured_image_padding_bottom'=> '',
'breadcrumbs_padding_top'=> '',
'breadcrumbs_padding_bottom'=> '',
'breadcrumbs_text_transform'=> 'no',
'theme_grid_image_grayscale'=> 'no',
'theme_grid_image_opacity'=> '',
'theme_grid_image_opacity_hover'=> '',
'theme_grid_transition'=> '',
'theme_grid_transform'=> '',
'theme_grid_transform_hover'=> '',
'theme_grid_transform_style'=> '',
'theme_grid_transform_origin'=> '',
'theme_grid_overlay_color'=> ''
);

function buttonsStyling () {

	foreach ($this->_sdfButtons as $id => $name) {
		$this->_ultTypography['theme_'.$id.'_button_bg_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_bg_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_border_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_border_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_text_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_text_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_button_text_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_text_hover_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_button_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_button_box_shadow_color'] = '';
		$this->_ultTypography['theme_'.$id.'_button_hover_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_button_hover_box_shadow_color'] = '';
	}
	
}

function postFormatButtonsStyling () {

	$type = 'post_format_button';
	// common
	$this->_ultTypography['theme_'.$type.'_icon_toggle'] = '';
	$this->_ultTypography['theme_'.$type.'_icon_size'] = '';
	$this->_ultTypography['theme_'.$type.'_text_family'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size_type'] = '';
	$this->_ultTypography['theme_'.$type.'_text_weight'] = '';
	$this->_ultTypography['theme_'.$type.'_text_transform'] = 'no';
	$this->_ultTypography['theme_'.$type.'_border_style'] = '';
	$this->_ultTypography['theme_'.$type.'_border_width'] = '';
	$this->_ultTypography['theme_'.$type.'_border_radius'] = '';
	$this->_ultTypography['theme_'.$type.'_width'] = '';
	$this->_ultTypography['theme_'.$type.'_height'] = '';
	$this->_ultTypography['theme_'.$type.'_margin'] = '';

	$this->_ultTypography['theme_'.$type.'_bg_color'] = '';
	$this->_ultTypography['theme_'.$type.'_bg_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_custom_style'] = 'no';
	
	foreach ($this->_sdfPostFormats as $format_slug) {
		$id = str_replace( "-", "_", urlencode(strtolower($format_slug)) ) . '_' . $type;
	
		$this->_ultTypography['theme_'.$id.'_custom_text'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_color'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow_color'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow_color'] = '';
	}
}

function followButtonsStyling () {

	$type = 'follow_button';
	// common
	$this->_ultTypography['theme_'.$type.'_icon_toggle'] = '';
	$this->_ultTypography['theme_'.$type.'_text_family'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size_type'] = '';
	$this->_ultTypography['theme_'.$type.'_text_weight'] = '';
	$this->_ultTypography['theme_'.$type.'_text_transform'] = 'no';
	$this->_ultTypography['theme_'.$type.'_border_style'] = '';
	$this->_ultTypography['theme_'.$type.'_border_width'] = '';
	$this->_ultTypography['theme_'.$type.'_border_radius'] = '';
	$this->_ultTypography['theme_'.$type.'_width'] = '';
	$this->_ultTypography['theme_'.$type.'_height'] = '';
	$this->_ultTypography['theme_'.$type.'_margin'] = '';

	$this->_ultTypography['theme_'.$type.'_bg_color'] = '';
	$this->_ultTypography['theme_'.$type.'_bg_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_custom_style'] = 'no';
	
	foreach ($this->_sdfSocialFollowMedia as $soc_slug => $soc_name) {
		$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ) . '_' . $type;
	
		$this->_ultTypography['theme_'.$id.'_custom_text'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_color'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow_color'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow_color'] = '';
	}
}

function sharingButtonsStyling () {

	$type = 'sharing_button';
	// common
	$this->_ultTypography['theme_'.$type.'_icon_toggle'] = '';
	$this->_ultTypography['theme_'.$type.'_icon_size'] = '';
	$this->_ultTypography['theme_'.$type.'_text_family'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size'] = '';
	$this->_ultTypography['theme_'.$type.'_text_size_type'] = '';
	$this->_ultTypography['theme_'.$type.'_text_weight'] = '';
	$this->_ultTypography['theme_'.$type.'_text_transform'] = 'no';
	$this->_ultTypography['theme_'.$type.'_border_style'] = '';
	$this->_ultTypography['theme_'.$type.'_border_width'] = '';
	$this->_ultTypography['theme_'.$type.'_border_radius'] = '';
	$this->_ultTypography['theme_'.$type.'_width'] = '';
	$this->_ultTypography['theme_'.$type.'_height'] = '';
	$this->_ultTypography['theme_'.$type.'_margin'] = '';

	$this->_ultTypography['theme_'.$type.'_bg_color'] = '';
	$this->_ultTypography['theme_'.$type.'_bg_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_color'] = '';
	$this->_ultTypography['theme_'.$type.'_border_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_color'] = '';
	$this->_ultTypography['theme_'.$type.'_text_hover_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow'] = '';
	$this->_ultTypography['theme_'.$type.'_hover_box_shadow_color'] = '';
	$this->_ultTypography['theme_'.$type.'_custom_style'] = 'no';
	
	foreach ($this->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
		$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) ) . '_' . $type;
	
		$this->_ultTypography['theme_'.$id.'_custom_text'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_color'] = '';
		$this->_ultTypography['theme_'.$id.'_bg_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_color'] = '';
		$this->_ultTypography['theme_'.$id.'_border_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_color'] = '';
		$this->_ultTypography['theme_'.$id.'_text_hover_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_box_shadow_color'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow'] = '';
		$this->_ultTypography['theme_'.$id.'_hover_box_shadow_color'] = '';
	}
}

function gridImagesStyling () {

	$this->_ultTypography['theme_grid_overlay_color'] = '#ffffff:0.70';
	$this->_ultTypography['theme_grid_transition'] = 'all 0.5s ease-in-out';
	$this->_ultTypography['theme_grid_transform'] = '';
	$this->_ultTypography['theme_grid_transform_hover'] = 'scale(1.07) rotate(0.5deg)';
	$this->_ultTypography['theme_grid_transform_origin'] = '';
	$this->_ultTypography['theme_grid_transform_style'] = '';
	$this->_ultTypography['theme_grid_image_opacity'] = '';
	$this->_ultTypography['theme_grid_image_opacity_hover'] = '';
	$this->_ultTypography['theme_grid_image_grayscale'] = '';
	
	$id = 'item_overlay_button';
	$this->_ultTypography['theme_grid_'.$id.'_icon_size'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_family'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_size'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_size_type'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_weight'] = 'normal';
	$this->_ultTypography['theme_grid_'.$id.'_text_transform'] = 'no';
	$this->_ultTypography['theme_grid_'.$id.'_width'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_height'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_line_height'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_margin'] = '0 10px 0 0';
	$this->_ultTypography['theme_grid_'.$id.'_padding'] = '6px 15px';
	$this->_ultTypography['theme_grid_'.$id.'_border_width'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_border_style'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_border_radius'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_bg_color'] = '#333333:0.90';
	$this->_ultTypography['theme_grid_'.$id.'_border_color'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_color'] = '#ffffff:0.90';
	$this->_ultTypography['theme_grid_'.$id.'_text_shadow'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_box_shadow'] = '';
	
	$this->_ultTypography['theme_grid_'.$id.'_box_shadow_color'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_bg_hover_color'] = '#333333:1';
	$this->_ultTypography['theme_grid_'.$id.'_border_hover_color'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_text_hover_color'] = '#ffffff:1';
	$this->_ultTypography['theme_grid_'.$id.'_text_hover_shadow'] = '';
	$this->_ultTypography['theme_grid_'.$id.'_hover_box_shadow'] = '';
	
	$this->_ultTypography['theme_grid_'.$id.'_hover_box_shadow_color'] = '';
		$id = 'item_overlay';
		
		$this->_ultTypography['theme_grid_'.$id.'_prettyphoto_button_icon'] = 'fa-expand';
		$this->_ultTypography['theme_grid_'.$id.'_prettyphoto_button_custom_text'] = '';
		$this->_ultTypography['theme_grid_'.$id.'_item_button_icon'] = 'fa-external-link';
		$this->_ultTypography['theme_grid_'.$id.'_item_button_custom_text'] = '';
}

function scrollToTopButtonStyling () {

	// common
	$this->_ultTypography['theme_scroll_to_top_button_icon_toggle'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_icon'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_icon_size'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_custom_text'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_family'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_size'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_size_type'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_weight'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_transform'] = 'no';
	$this->_ultTypography['theme_scroll_to_top_button_border_style'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_border_width'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_border_radius'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_width'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_height'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_right'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_bottom'] = '';

	$this->_ultTypography['theme_scroll_to_top_button_bg_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_bg_hover_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_border_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_border_hover_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_shadow'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_hover_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_text_hover_shadow'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_box_shadow'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_box_shadow_color'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_hover_box_shadow'] = '';
	$this->_ultTypography['theme_scroll_to_top_button_hover_box_shadow_color'] = '';
}

function sidebarsTypography () {

	foreach ($this->_sdfSidebars as $id => $name) {
		if ($id != 'slider-widget-area') {
		$id_var = str_replace("-", "_", urlencode(strtolower($id))); 

		$this->_ultTypography[$id_var.'_height'] = '';
		$this->_ultTypography[$id_var.'_padding_top'] = '';
		$this->_ultTypography[$id_var.'_padding_bottom'] = '';
		$this->_ultTypography[$id_var.'_custom_bg'] = 'no';
		$this->_ultTypography[$id_var.'_bg_color'] = '';
		$this->_ultTypography[$id_var.'_bg_image'] = '';
		$this->_ultTypography[$id_var.'_bgrepeat'] = '';
		$this->_ultTypography[$id_var.'_equal_heights'] = 'off';
		$this->_ultTypography[$id_var.'_par_bg_ratio'] = '';
		$this->_ultTypography[$id_var.'_box_shadow'] = '';
		$this->_ultTypography[$id_var.'_box_shadow_color'] = '';
		$this->_ultTypography[$id_var.'_wg_align'] = '';
		
		$this->_ultTypography[$id_var.'_wg_custom_typo'] = 'no';
		$this->_ultTypography[$id_var.'_wg_title_align'] = '';
		$this->_ultTypography[$id_var.'_wg_title_margin_top'] = '';
		$this->_ultTypography[$id_var.'_wg_title_margin_bottom'] = '';
		$this->_ultTypography[$id_var.'_wg_title_transform'] = '';
		$this->_ultTypography[$id_var.'_wg_title_weight'] = '';
		$this->_ultTypography[$id_var.'_wg_title_size'] = '';
		$this->_ultTypography[$id_var.'_wg_title_size_type'] = '';
		$this->_ultTypography[$id_var.'_wg_title_family'] = '';
		$this->_ultTypography[$id_var.'_wg_title_color'] = '';
		$this->_ultTypography[$id_var.'_wg_title_box_shadow'] = '';
		$this->_ultTypography[$id_var.'_wg_title_box_shadow_color'] = '';

		$this->_ultTypography[$id_var.'_wg_content_align'] = '';
		$this->_ultTypography[$id_var.'_wg_font_size'] = '';
		$this->_ultTypography[$id_var.'_wg_font_size_type'] = '';
		$this->_ultTypography[$id_var.'_wg_family'] = '';
		$this->_ultTypography[$id_var.'_wg_font_color'] = '';
		$this->_ultTypography[$id_var.'_wg_link_color'] = '';
		$this->_ultTypography[$id_var.'_wg_link_hover_color'] = '';

		$this->_ultTypography[$id_var.'_wg_custom_style'] = 'no';
		$this->_ultTypography[$id_var.'_wg_background_color'] = '';
		$this->_ultTypography[$id_var.'_wg_border_width'] = '';
		$this->_ultTypography[$id_var.'_wg_border_color'] = '';
		$this->_ultTypography[$id_var.'_wg_border_style'] = '';
		$this->_ultTypography[$id_var.'_wg_border_radius'] = '';
		$this->_ultTypography[$id_var.'_wg_bg_image'] = '';
		$this->_ultTypography[$id_var.'_wg_bgrepeat'] = '';
		$this->_ultTypography[$id_var.'_wg_box_shadow'] = '';
		$this->_ultTypography[$id_var.'_wg_box_shadow_color'] = '';
		
		$this->_ultTypography[$id_var.'_wg_custom_list_style'] = 'no';
		$this->_ultTypography[$id_var.'_wg_list_padding'] = '';
		$this->_ultTypography[$id_var.'_wg_list_margin'] = '';
		$this->_ultTypography[$id_var.'_wg_list_align'] = '';
		$this->_ultTypography[$id_var.'_wg_list_font_size'] = '';
		$this->_ultTypography[$id_var.'_wg_list_font_size_type'] = '';
		$this->_ultTypography[$id_var.'_wg_list_family'] = '';
		$this->_ultTypography[$id_var.'_wg_list_font_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_link_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_link_hover_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_link_text_decoration'] = '';
		$this->_ultTypography[$id_var.'_wg_list_link_hover_text_decoration'] = '';
		$this->_ultTypography[$id_var.'_wg_list_style'] = 'default';
		$this->_ultTypography[$id_var.'_wg_list_icon'] = '';
		$this->_ultTypography[$id_var.'_wg_list_icon_size'] = '';
		$this->_ultTypography[$id_var.'_wg_list_icon_size_type'] = '';
		$this->_ultTypography[$id_var.'_wg_list_icon_margin'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_padding'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_margin'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_line_height'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_border_width'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_border_style'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_border_radius'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_background_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_border_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_box_shadow'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_box_shadow_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_background_hover_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_border_hover_color'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_hover_box_shadow'] = '';
		$this->_ultTypography[$id_var.'_wg_list_item_hover_box_shadow_color'] = '';
		}
	}
}


public $_sdfSocialFollowMedia = array(
'behance'				=> 'Behance',
'codepen'				=> 'Codepen',
'digg'          => 'Digg',
'dribbble'      => 'Dribbble',
'dropbox'       => 'Dropbox',
'facebook'      => 'Facebook',
'flickr'        => 'Flickr',
'foursquare'    => 'Foursquare',
'github'        => 'GitHub',
'gittip'        => 'Gittip',
'google-plus'   => 'Google Plus',
'instagram'     => 'Instagram',
'linkedin'      => 'LinkedIn',
'pinterest'     => 'Pinterest',
'reddit'     		=> 'Reddit',
'renren'        => 'Renren',
'skype'         => 'Skype',
'stackexchange' => 'StackExchange',
'soundcloud'    => 'Soundcloud',
'spotify'      	=> 'Spotify',
'trello'        => 'Trello',
'tumblr'        => 'Tumblr',
'twitter'       => 'Twitter',
'vimeo-square'  => 'Vimeo',
'vk'            => 'VK',
'weibo'         => 'Weibo',
'xing'          => 'Xing',
'yelp'          => 'Yelp',
'youtube'       => 'YouTube',
'rss' 					=> 'RSS'
);
function sdfFollowIcons () {

	foreach ($this->_sdfSocialFollowMedia as $fa_key => $fa_val) {
		$fa_key_id = str_replace("-", "_", urlencode(strtolower($fa_key)));
		$this->_sdfGlobalArgs['social_media']['social_media_'.$fa_key_id] = '';
	}
}
public $_sdfSocialSharingMedia = array(
'google-plus' => 'Google Plus',
'facebook' 		=> 'Facebook',
'twitter' 		=> 'Twitter',
'linkedin' 		=> 'LinkedIn',
'pinterest' 	=> 'Pinterest',
'tumblr' 			=> 'Tumblr',
'vk' 					=> 'VK',
'reddit'	 		=> 'Reddit',
'envelope' 		=> 'Email'
);

public $_sdfSocialBrandColors = array(
'google-plus' => '#dd4b39',
'facebook' => '#3b5998',
'twitter' => '#55acee',
'linkedin' => '#0976b4',
'skype' => '#00aff0',
'github' => '#333',
'pinterest' => '#cc2127',
'instagram' => '#3f729b',
'dribbble' => '#ea4c89',
'foursquare' => '#0072b1',
'trello' => '#256a92',
'flickr' => '#ff0084',
'youtube' => '#e52d27',
'vimeo-square' => '#1ab7ea',
'tumblr' => '#35465c',
'vk' => '#45668e',
'reddit' => '#ff4500',
'rss' => '#f26522'
);

public $_sdfSidebars = array(
'default-widget-area' => 'Default Widget Block',// used for default areas styling
'right-widget-area' => 'Right Sidebar',
'left-widget-area' => 'Left Sidebar',
'above-content-area' => 'Above Content Block',
'below-content-area' => 'Below Content Block',
'footer-widget-area' => 'Footer Block',
'footer-bottom-widget-area' => 'Footer Bottom Block',
'header-area-1' => 'Header Block 1',
'header-area-2' => 'Header Block 2',
'header-area-3' => 'Header Block 3',
'header-area-4' => 'Header Block 4',
'slider-widget-area' => 'Slider Block'
);

public $_sdfButtons = array(
'primary' => 'Primary Button',
'secondary' => 'Secondary Button',
'white' => 'White Button',
'grey' => 'Grey Button',
'black' => 'Black Button',
'default' => 'Default Button',
'success' => 'Success Button',
'info' => 'Info Button',
'warning' => 'Warning Button',
'danger' => 'Danger Button',
'link' => 'Link Button'
);
public $_sdfPostFormats = array( 'standard', 'image', 'audio', 'video', 'quote', 'link', 'gallery' );

public $_boxNames = array (
'listItem_1' => array ('name' => 'Header Block 1'),
'listItem_2' => array ('name' => 'Header Block 2'),
'listItem_3' => array ('name' => 'Header Block 3'),
'listItem_4' => array ('name' => 'Header Block 4'),
'listItem_5' => array ('name' => 'Logo'),
'listItem_6' => array ('name' => 'Navigation'),
'listItem_7' => array ('name' => 'Slider Block'),
'listItem_8' => array ('name' => 'Secondary Navigation')
);
public $_ultBgRepeatType = array(
'center center no-repeat' => 'Center No-Repeat',
'left top no-repeat' => 'Left Top No-Repeat',
'center top no-repeat' => 'Center Top No-Repeat',
'center center repeat' => 'Center Repeat',
'left top repeat' => 'Left Top Repeat',
'left top repeat-x' => 'Left Top Repeat Horizontally',
'center top repeat-y' => 'Center Top Repeat Vertically',
'center top no-repeat fixed' => 'Center No-Repeat Fixed',
'contain' => 'Contain',
'cover' => 'Cover (Fullscreen)'
);	
public $_ultSpacerHeights = array( '',  '0', '5px', '10px', '15px', '20px', '25px', '30px', '35px', '40px', '45px', '50px', '60px', '70px', '80px', '90px', '100px', '120px', '140px', '160px', '180px', '200px', '220px', '250px', '270px', '300px' );

public $_ultMenuLayouts = array(
'names' => array(
				'Default Menu' => 'default',
				'Sticky Menu' => 'sticky_menu',
				'Fixed to Top Menu' => 'fixed_top',
				'Fixed to Bottom Menu' => 'fixed_bottom'
),
'classes' => array(
				'default' => 'navbar-default',
				'sticky_menu' => 'navbar-sticky',
				'sticky_overlay' => 'navbar-sticky navbar-sticky-overlay',
				'fixed_top' => 'navbar-fixed-top',
				'fixed_bottom' => 'navbar-fixed-bottom'
)
);
public $_ultImages = array(
'shapes' => array(
				'Square' => 'square',
				'Rectangle' => 'rectangle',
				'Circle' => 'circle',
				'Ellipse' => 'ellipse',
				'Leaf' => 'leaf',
				'Lemon' => 'lemon',
				'Bullet' => 'bullet',
				'Mushroom' => 'mushroom',
				'Old Screen' => 'old-screen'
),
'sizes' => array(
				'Full Grid Width Size' => 'sdf-image-md-12',
				'One Half Size' => 'sdf-image-md-6',
				'One Third Size' => 'sdf-image-md-4',
				'One Quarter Size' => 'sdf-image-md-3',
				'One Sixth Size' => 'sdf-image-md-2',
				'WP Full Size' => 'full',
				'WP Large Size' => 'large',
				'WP Medium Size' => 'medium',
				'WP Thumbnail Size' => 'thumbnail'
)
);

public $_ultIconSizes = array(
	'Default' => '',
	'1.33X (Large)'  => 'fa-lg',
	'2X' => 'fa-2x',
	'3X' => 'fa-3x',
	'4X' => 'fa-4x',
	'5X' => 'fa-5x'
);

public $_headingsList = array(
	'h1' => 'H1', 
	'h2' => 'H2', 
	'h3' => 'H3', 
	'h4' => 'H4', 
	'h5' => 'H5', 
	'h6' => 'H6'
);


public $_ultTbsVars = array(
	'callout_colors' => array(
								'callout_text' => '<h4>Colors</h4><p>Gray and brand colors for use across Bootstrap.</p>',
								'type' => 'callout',
							),
	'gray-darker' => array(
								'name' => 'Gray Darker (@gray-darker)',
								'placeholder' => 'lighten(#000, 13.5%)',
								'help' => '',
								'type' => 'color'
							),
	'gray-dark' => array(
								'name' => 'Gray Dark (@gray-dark)',
								'placeholder' => 'lighten(#000, 20%)',
								'help' => '',
								'type' => 'color'
							),
	'gray' => array(
								'name' => 'Gray (@gray)',
								'placeholder' => 'lighten(#000, 33.5%)',
								'help' => '',
								'type' => 'color'
							),
	'gray-light' => array(
								'name' => 'Gray Light (@gray-light)',
								'placeholder' => 'lighten(#000, 60%)',
								'help' => '',
								'type' => 'color'
							),
	'gray-lighter' => array(
								'name' => 'Gray Lighter (@gray-lighter)',
								'placeholder' => 'lighten(#000, 93.5%)',
								'help' => '',
								'type' => 'color'
							),
	'brand-success' => array(
								'name' => 'Brand Success (@brand-success)',
								'placeholder' => '#5cb85c',
								'help' => '',
								'type' => 'color'
							),
	'brand-info' => array(
								'name' => 'Brand Info (@brand-info)',
								'placeholder' => '#5bc0de',
								'help' => '',
								'type' => 'color'
							),
	'brand-warning' => array(
								'name' => 'Brand Warning (@brand-warning)',
								'placeholder' => '#f0ad4e',
								'help' => '',
								'type' => 'color'
							),
	'brand-danger' => array(
								'name' => 'Brand Danger (@brand-danger)',
								'placeholder' => '#d9534f',
								'help' => '',
								'type' => 'color'
							),
	'callout_typo' => array(
								'callout_text' => '<h4>Typography</h4><p>Font, line-height, and color for body text, headings, and more.</p>',
								'type' => 'callout',
							),
	'font-family-sans-serif' => array(
								'name' => 'Sans Serif Font Family',
								'placeholder' => '&quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif',
								'help' => '',
								'type' => 'text'
							),
	'font-family-serif' => array(
								'name' => 'Serif Font Family',
								'placeholder' => 'Georgia, &quot;Times New Roman&quot;, Times, serif',
								'help' => '',
								'type' => 'text'
							),
	'font-family-monospace' => array(
								'name' => 'Monospace Font Family',
								'placeholder' => 'Menlo, Monaco, Consolas, "Courier New", monospace',
								'help' => 'Default monospace fonts for <code>&lt;code&gt;</code>, <code>&lt;kbd&gt;</code>, and <code>&lt;pre&gt;</code>',
								'type' => 'text'
							),
	'font-family-base' => array(
								'name' => 'Base Font Family',
								'placeholder' => '@font-family-sans-serif',
								'help' => 'Used as a fallback font if we don\'t have Google Web Fonts available.',
								'type' => 'text'
							),
	'font-size-large' => array(
								'name' => 'Font Size Large',
								'placeholder' => 'ceil((@font-size-base * 1.25))',
								'help' => '',
								'type' => 'text'
							),
	'font-size-small' => array(
								'name' => 'Font Size Small',
								'placeholder' => 'ceil((@font-size-base * 0.85))',
								'help' => '',
								'type' => 'text'
							),
	'line-height-base' => array(
								'name' => 'Line Height Base',
								'placeholder' => '1.428571429',
								'help' => 'Unit-less line-height for use in components like buttons.',
								'type' => 'text'
							),		
	'line-height-computed' => array(
								'name' => 'Line Height Computed',
								'placeholder' => 'floor((@font-size-base * @line-height-base))',
								'help' => 'Computed "line-height" (font-size * line-height) for use with margin, padding, etc.',
								'type' => 'text'
							),	
	'headings-line-height' => array(
								'name' => 'Headings Line Height',
								'placeholder' => '1.1',
								'help' => '',
								'type' => 'text'
							),
	'callout_grid' => array(
								'callout_text' => '<h4>Grid system</h4><p>Define your custom responsive grid settings.</p>',
								'type' => 'callout',
							),
	'grid-gutter-width' => array(
								'name' => 'Grid Gutter Width',
								'placeholder' => '30px',
								'help' => 'Padding between columns. Gets divided in half for the left and right.',
								'type' => 'text'
							),
	'grid-float-breakpoint' => array(
								'name' => 'Grid Float Breakpoint',
								'placeholder' => '768px',
								'help' =>'Point at which the navbar begins collapsing.',
								'type' => 'text'
							),
	'grid-float-breakpoint-max' => array(
								'name' => 'Grid Float Breakpoint Max',
								'placeholder' => '767px',
								'help' => 'Point at which the navbar becomes uncollapsed.',
								'type' => 'text'
							),
	'callout_cont' => array(
								'callout_text' => '<h4>Container sizes</h4><p>Define the maximum width of <code>.container</code> for different screen sizes.</p>',
								'type' => 'callout',
							),
	'container-tablet' => array(
								'name' => 'Container Tablet',
								'placeholder' => '((720px + @grid-gutter-width))',
								'help' =>'',
								'type' => 'text'
							),
	'container-sm' => array(
								'name' => 'Container sm',
								'placeholder' => '@container-tablet',
								'help' =>'For <code>@screen-sm-min</code> and up.',
								'type' => 'text'
							),
	'container-desktop' => array(
								'name' => 'Container Desktop',
								'placeholder' => '((940px + @grid-gutter-width))',
								'help' =>'',
								'type' => 'text'
							),
	'container-md' => array(
								'name' => 'Container md',
								'placeholder' => '@container-desktop',
								'help' =>'For <code>@screen-md-min</code> and up.',
								'type' => 'text'
							),
	'container-large-desktop' => array(
								'name' => 'Container Large Desktop',
								'placeholder' => '((1140px + @grid-gutter-width))',
								'help' =>'',
								'type' => 'text'
							),
	'container-lg' => array(
								'name' => 'Container Large Desktop',
								'placeholder' => '@container-large-desktop',
								'help' =>'For <code>@screen-lg-min</code> and up.',
								'type' => 'text'
							),
	'callout_comp' => array(
								'callout_text' => '<h4>Components</h4><p>Define common Bootstrap components padding and border radius sizes and more. Values based on 14px text and 1.428 line-height (~20px to start).</p>',
								'type' => 'callout',
							),	
	'callout_padd' => array(
								'callout_text' => 'Padding',
								'type' => 'callout',
							),	
	'padding-base-vertical' => array(
								'name' => 'Padding Base Vertical',
								'placeholder' => '6px',
								'help' => 'Define common vertical padding used on text inputs and buttons.',
								'type' => 'text'
							),		
	'padding-base-horizontal' => array(
								'name' => 'Padding Base Horizontal',
								'placeholder' => '12px',
								'help' => 'Define common horizontal padding used on text inputs and buttons.',
								'type' => 'text'
							),	
	'padding-large-vertical' => array(
								'name' => 'Padding Large Vertical',
								'placeholder' => '10px',
								'help' => 'Define common vertical padding used on large text inputs and buttons.',
								'type' => 'text'
							),		
	'padding-large-horizontal' => array(
								'name' => 'Padding Large Horizontal',
								'placeholder' => '16px',
								'help' => 'Define common horizontal padding used on large text inputs and buttons.',
								'type' => 'text'
							),	
	'padding-small-vertical' => array(
								'name' => 'Padding Small Vertical',
								'placeholder' => '5px',
								'help' => 'Define common vertical padding used on small text inputs and buttons.',
								'type' => 'text'
							),		
	'padding-small-horizontal' => array(
								'name' => 'Padding Small Horizontal',
								'placeholder' => '10px',
								'help' => 'Define common horizontal padding used on small text inputs and buttons.',
								'type' => 'text'
							),	
	'padding-xs-vertical' => array(
								'name' => 'Padding XS Vertical',
								'placeholder' => '1px',
								'help' => 'Define common vertical padding used on xs text inputs and buttons.',
								'type' => 'text'
							),		
	'padding-xs-horizontal' => array(
								'name' => 'Padding XS Horizontal',
								'placeholder' => '5px',
								'help' => 'Define common horizontal padding used on xs text inputs and buttons.',
								'type' => 'text'
							),	
	'callout_lineh' => array(
								'callout_text' => 'Line Height',
								'type' => 'callout',
							),			
	'line-height-large' => array(
								'name' => 'Line Height Large',
								'placeholder' => '1.33',
								'help' => 'Unit-less line-height for use in components like large buttons.',
								'type' => 'text'
							),	
	'line-height-small' => array(
								'name' => 'Line Height Small',
								'placeholder' => '1.5',
								'help' => 'Unit-less line-height for use in components like small buttons.',
								'type' => 'text'
							),
	'callout_borderrad' => array(
								'callout_text' => 'Border Radius',
								'type' => 'callout',
							),	
	'border-radius-base' => array(
								'name' => 'Border Radius Base',
								'placeholder' => '4px',
								'type' => 'text'
							),	
	'border-radius-large' => array(
								'name' => 'Border Radius Large',
								'placeholder' => '6px',
								'type' => 'text'
							),	
	'border-radius-small' => array(
								'name' => 'Border Radius Small',
								'placeholder' => '3px',
								'type' => 'text'
							),
	'component-active-color' => array(
								'name' => 'Component Active Color',
								'placeholder' => '#fff',
								'help' => 'Global color for active items (e.g., navs or dropdowns).',
								'type' => 'color'
							),
	'component-active-bg' => array(
								'name' => 'Component Active Background Color',
								'placeholder' => '@brand-primary',
								'help' => 'Global background color for active items (e.g., navs or dropdowns).',
								'type' => 'color'
							),		
	'callout_forms' => array(
								'callout_text' => '<h4>Forms</h4>',
								'type' => 'callout',
							),
	'input-bg' => array(
								'name' => 'Input Background Color',
								'placeholder' => '#ffffff',
								'help' => '',
								'type' => 'color'
							),	
	'input-bg-disabled' => array(
								'name' => 'Disabled Input Background Color',
								'placeholder' => '@gray-lighter',
								'help' => '',
								'type' => 'color'
							),		
	'input-color' => array(
								'name' => 'Input Text Color',
								'placeholder' => '@gray',
								'help' => '',
								'type' => 'color'
							),
	'input-border' => array(
								'name' => 'Input Border Color',
								'placeholder' => '#cccccc',
								'help' => '',
								'type' => 'color'
							),	
	'input-border-radius' => array(
								'name' => 'Input Border Radius',
								'placeholder' => '@border-radius-base',
								'help' => '',
								'type' => 'text'
							),	
	'input-border-focus' => array(
								'name' => 'Input Border Focus',
								'placeholder' => '#66afe9',
								'help' => 'Border color for inputs on focus',
								'type' => 'color'
							),	
	'input-color-placeholder' => array(
								'name' => 'Placeholder Text Color',
								'placeholder' => '@gray-light',
								'help' => '',
								'type' => 'color'
							),		
	'input-height-base' => array(
								'name' => 'Base Input Height',
								'placeholder' => '(@line-height-computed + (@padding-base-vertical * 2) + 2)',
								'help' => 'Default <code>.form-control</code> height',
								'type' => 'text'
							),	
	'input-height-large' => array(
								'name' => 'Large Input Height',
								'placeholder' => '(ceil(@font-size-large * @line-height-large) + (@padding-large-vertical * 2) + 2)',
								'help' => 'Large <code>.form-control</code> height',
								'type' => 'text'
							),		
	'input-height-small' => array(
								'name' => 'Small Input Height',
								'placeholder' => '(floor(@font-size-small * @line-height-small) + (@padding-small-vertical * 2) + 2)',
								'help' => 'Small <code>.form-control</code> height',
								'type' => 'text'
							),		
	'legend-color' => array(
								'name' => 'Legend Color',
								'placeholder' => '@gray-dark',
								'help' => '',
								'type' => 'color'
							),		
	'legend-border-color' => array(
								'name' => 'Legend Border Color',
								'placeholder' => '#e5e5e5',
								'help' => '',
								'type' => 'color'
							),		
	'input-group-addon-bg' => array(
								'name' => 'Input Group Addon Background Color',
								'placeholder' => '@gray-lighter',
								'help' => 'Background color for textual input addons',
								'type' => 'color'
							),	
	'input-group-addon-border-color' => array(
								'name' => 'Input Group Addon Border Color',
								'placeholder' => '@input-border',
								'help' => 'Border color for textual input addons',
								'type' => 'color'
							),		
	'callout_tabs' => array(
								'callout_text' => '<h4>Tabs</h4>',
								'type' => 'callout',
							),	
	'nav-tabs-border-color' => array(
								'name' => 'Nav Tabs Border Color',
								'placeholder' => '#ddd',
								'help' => 'Border color for tabs',
								'type' => 'color'
							),		
	'nav-tabs-link-hover-border-color' => array(
								'name' => 'Nav Tabs Link Hover Border Color',
								'placeholder' => '@gray-lighter',
								'type' => 'color'
							),	
	'nav-tabs-active-link-hover-bg' => array(
								'name' => 'Nav Tabs Active Link Hover Background',
								'placeholder' => '@body-bg',
								'type' => 'color'
							),	
	'nav-tabs-active-link-hover-color' => array(
								'name' => 'Nav Tabs Active Link Hover Color',
								'placeholder' => '@gray',
								'type' => 'color'
							),
	'nav-tabs-active-link-hover-border-color' => array(
								'name' => 'Nav Tabs Active Link Hover Border Color',
								'placeholder' => '#ddd',
								'type' => 'color'
							),
	'nav-tabs-justified-link-border-color' => array(
								'name' => 'Nav Tabs Justified Link Border Color',
								'placeholder' => '#ddd',
								'type' => 'color'
							),
	'nav-tabs-justified-active-link-border-color' => array(
								'name' => 'Nav Tabs Justified Active Link Border Color',
								'placeholder' => '@body-bg',
								'type' => 'color'
							),		
	'callout_pills' => array(
								'callout_text' => '<h4>Pills</h4>',
								'type' => 'callout',
							),
	'nav-pills-border-radius' => array(
								'name' => 'Nav Pills Border Radius',
								'placeholder' => '@border-radius-base',
								'type' => 'text'
							),
	'nav-pills-active-link-hover-bg' => array(
								'name' => 'Nav Pills Active Link Hover Background',
								'placeholder' => '@component-active-bg',
								'type' => 'color'
							),
	'nav-pills-active-link-hover-color' => array(
								'name' => 'Nav Pills Active Link Hover Color',
								'placeholder' => '@component-active-color',
								'type' => 'color'
							),		
	'callout_states' => array(
								'callout_text' => '<h4>Form states and alerts</h4><p>Define colors for form feedback states and, by default, alerts.</p>',
								'type' => 'callout',
							),
	'state-success-text' => array(
								'name' => 'State Success Text',
								'placeholder' => '#3c763d',
								'type' => 'color'
							),
	'state-success-bg' => array(
								'name' => 'State Success Background Color',
								'placeholder' => '#dff0d8',
								'type' => 'color'
							),
	'state-success-border' => array(
								'name' => 'State Success Border Color',
								'placeholder' => 'darken(spin(@state-success-bg, -10), 5%)',
								'type' => 'color'
							),	
	'state-info-text' => array(
								'name' => 'State Info Text',
								'placeholder' => '#31708f',
								'type' => 'color'
							),
	'state-info-bg' => array(
								'name' => 'State Info Background Color',
								'placeholder' => '#d9edf7',
								'type' => 'color'
							),
	'state-info-border' => array(
								'name' => 'State Info Border Color',
								'placeholder' => 'darken(spin(@state-info-bg, -10), 7%)',
								'type' => 'color'
							),	
	'state-warning-text' => array(
								'name' => 'State Warning Text',
								'placeholder' => '#8a6d3b',
								'type' => 'color'
							),
	'state-warning-bg' => array(
								'name' => 'State Warning Background Color',
								'placeholder' => '#fcf8e3',
								'type' => 'color'
							),
	'state-warning-border' => array(
								'name' => 'State Warning Border Color',
								'placeholder' => 'darken(spin(@state-warning-bg, -10), 5%)',
								'type' => 'color'
							),	
	'state-danger-text' => array(
								'name' => 'State Danger Text',
								'placeholder' => '#a94442',
								'type' => 'color'
							),
	'state-danger-bg' => array(
								'name' => 'State Danger Background Color',
								'placeholder' => '#f2dede',
								'type' => 'color'
							),
	'state-danger-border' => array(
								'name' => 'State Danger Border Color',
								'placeholder' => 'darken(spin(@state-danger-bg, -10), 5%)',
								'type' => 'color'
							),		
	'callout_panels' => array(
								'callout_text' => '<h4>Panels</h4>',
								'type' => 'callout',
							),	
	'panel-bg' => array(
								'name' => 'Panel Background Color',
								'placeholder' => '#fff',
								'type' => 'color'
							),
	'panel-body-padding' => array(
								'name' => 'Panel Body Padding',
								'placeholder' => '15px',
								'type' => 'text'
							),	
	'panel-heading-padding' => array(
								'name' => 'Panel Heading Padding',
								'placeholder' => '10px 15px',
								'type' => 'text'
							),
	'panel-footer-padding' => array(
								'name' => 'Panel Footer Padding',
								'placeholder' => '@panel-heading-padding',
								'type' => 'text'
							),
	'panel-border-radius' => array(
								'name' => 'Panel Border Radius',
								'placeholder' => '@border-radius-base',
								'type' => 'text'
							),	
	'panel-inner-border' => array(
								'name' => 'Panel Inner Border Color',
								'placeholder' => '#ddd',
								'help' => 'Border color for elements within panels',
								'type' => 'color'
							),
	'panel-footer-bg' => array(
								'name' => 'Panel Footer Background Color',
								'placeholder' => '#f5f5f5',
								'type' => 'color'
							),
	'panel-default-text' => array(
								'name' => 'Panel Default Text Color',
								'placeholder' => '@gray-dark',
								'type' => 'color'
							),
	'panel-default-border' => array(
								'name' => 'Panel Default Border Color',
								'placeholder' => '#ddd',
								'type' => 'color'
							),
	'panel-default-heading-bg' => array(
								'name' => 'Panel Default Heading Background Color',
								'placeholder' => '#f5f5f5',
								'type' => 'color'
							),
	'panel-primary-text' => array(
								'name' => 'Panel Primary Text Color',
								'placeholder' => '#fff',
								'type' => 'color'
							),
	'panel-primary-border' => array(
								'name' => 'Panel Primary Border Color',
								'placeholder' => '@brand-primary',
								'type' => 'color'
							),
	'panel-primary-heading-bg' => array(
								'name' => 'Panel Primary Heading Background Color',
								'placeholder' => '@brand-primary',
								'type' => 'color'
							),
	'panel-success-text' => array(
								'name' => 'Panel Success Text Color',
								'placeholder' => '@state-success-text',
								'type' => 'color'
							),
	'panel-success-border' => array(
								'name' => 'Panel Success Border Color',
								'placeholder' => '@state-success-border',
								'type' => 'color'
							),
	'panel-success-heading-bg' => array(
								'name' => 'Panel Success Heading Background Color',
								'placeholder' => '@state-success-bg',
								'type' => 'color'
							),
	'panel-info-text' => array(
								'name' => 'Panel Info Text Color',
								'placeholder' => '@state-info-text',
								'type' => 'color'
							),
	'panel-info-border' => array(
								'name' => 'Panel Info Border Color',
								'placeholder' => '@state-info-border',
								'type' => 'color'
							),
	'panel-info-heading-bg' => array(
								'name' => 'Panel Info Heading Background Color',
								'placeholder' => '@state-info-bg',
								'type' => 'color'
							),
	'panel-warning-text' => array(
								'name' => 'Panel Warning Text Color',
								'placeholder' => '@state-warning-text',
								'type' => 'color'
							),
	'panel-warning-border' => array(
								'name' => 'Panel Warning Border Color',
								'placeholder' => '@state-warning-border',
								'type' => 'color'
							),
	'panel-warning-heading-bg' => array(
								'name' => 'Panel Warning Heading Background Color',
								'placeholder' => '@state-warning-bg',
								'type' => 'color'
							),
	'panel-danger-text' => array(
								'name' => 'Panel Danger Text Color',
								'placeholder' => '@state-danger-text',
								'type' => 'color'
							),
	'panel-danger-border' => array(
								'name' => 'Panel Danger Border Color',
								'placeholder' => '@state-danger-border',
								'type' => 'color'
							),
	'panel-danger-heading-bg' => array(
								'name' => 'Panel Danger Heading Background Color',
								'placeholder' => '@state-danger-bg',
								'type' => 'color'
							),
);

function tbsTypography () {

	foreach ($this->_ultTbsVars as $tbs_key => $tbs_val) {
		if ($tbs_val['type'] != 'callout') {
			$tbs_key_id = str_replace("-", "_", urlencode(strtolower($tbs_key)));
			$this->_ultTypography['theme_tbs_'.$tbs_key_id] = '';
		}
	}
}

/**
 * Easing effects
 */
public $_ultEasingEffects = array(
		'linear'    				=> 'linear',
		'swing'     				=> 'swing',
		'easeInQuad'    		=> 'easeInQuad',
		'easeOutQuad'   		=> 'easeOutQuad',
		'easeInOutQuad'     => 'easeInOutQuad',
		'easeInCubic'       => 'easeInCubic',
		'easeOutCubic'      => 'easeOutCubic',
		'easeInOutCubic'    => 'easeInOutCubic',
		'easeInQuart'       => 'easeOutQuart',
		'easeInOutQuart'    => 'easeInOutQuart',
		'easeInQuint'       => 'easeInQuint',
		'easeOutQuint'      => 'easeOutQuint',
		'easeInOutQuint'    => 'easeInOutQuint',
		'easeInSine'   			=> 'easeInSine',
		'easeOutSine'   		=> 'easeOutSine',
		'easeInOutSine'   	=> 'easeInOutSine',
		'easeInExpo'   			=> 'easeInExpo',
		'easeOutExpo'   		=> 'easeOutExpo',
		'easeInOutExpo'   	=> 'easeInOutExpo',
		'easeInCirc'   			=> 'easeInCirc',
		'easeOutCirc'   		=> 'easeOutCirc',
		'easeInOutCirc'   	=> 'easeInOutCirc',
		'easeInElastic'   	=> 'easeInElastic',
		'easeOutElastic'   	=> 'easeOutElastic',
		'easeInOutElastic'  => 'easeInOutElastic',
		'easeInBounce'   		=> 'easeInBounce',
		'easeOutBounce'   	=> 'easeOutBounce',
		'easeInOutBounce'  	=> 'easeInOutBounce',
		'easeInBack'  			=> 'easeInBack',
		'easeOutBack'  			=> 'easeOutBack',
		'easeInOutBack' 		=> 'easeInOutBack'
);

public $_ultIconsUnicode = array ( 'fa-caret-right' => 'f0da', 'fa-caret-square-o-right' => 'f152', 'fa-angle-double-right' => 'f101', 'fa-angle-right' => 'f105', 'fa-arrow-circle-o-right' => 'f18e', 'fa-arrow-circle-right' => 'f0a9', 'fa-arrow-right' => 'f061', ' fa-asterisk' => 'f069', 'fa-certificate' => 'f0a3', 'fa-check' => 'f00c', 'fa-check-circle' => 'f058', 'fa-check-circle-o' => 'f05d', 'fa-check-square' => 'f14a', 'fa-check-square-o' => 'f046', 'fa-chevron-circle-right' => 'f138', 'fa-chevron-right' => 'f054', 'fa-circle' => 'f111', 'fa-circle-o' => 'f10c' );

public $_ultIcons = array ( 'Glass' => 'fa-glass', 'Music' => 'fa-music', 'Search' => 'fa-search', 'Envelope Outlined' => 'fa-envelope-o', 'Heart' => 'fa-heart', 'Star' => 'fa-star', 'Star Outlined' => 'fa-star-o', 'User' => 'fa-user', 'Film' => 'fa-film', 'Th Large' => 'fa-th-large', 'Th' => 'fa-th', 'Th List' => 'fa-th-list', 'Check' => 'fa-check', 'Times' => 'fa-times', 'Search Plus' => 'fa-search-plus', 'Search Minus' => 'fa-search-minus', 'Power Off' => 'fa-power-off', 'Signal' => 'fa-signal', 'Cog' => 'fa-cog', 'Trash Outlined' => 'fa-trash-o', 'Home' => 'fa-home', 'File Outlined' => 'fa-file-o', 'Clock Outlined' => 'fa-clock-o', 'Road' => 'fa-road', 'Download' => 'fa-download', 'Arrow Circle Outlined (Down)' => 'fa-arrow-circle-o-down', 'Arrow Circle Outlined (Up)' => 'fa-arrow-circle-o-up', 'Inbox' => 'fa-inbox', 'Play Circle Outlined' => 'fa-play-circle-o', 'Repeat' => 'fa-repeat', 'Refresh' => 'fa-refresh', 'List Alt' => 'fa-list-alt', 'Lock' => 'fa-lock', 'Flag' => 'fa-flag', 'Headphones' => 'fa-headphones', 'Volume Off' => 'fa-volume-off', 'Volume (Down)' => 'fa-volume-down', 'Volume (Up)' => 'fa-volume-up', 'Qrcode' => 'fa-qrcode', 'Barcode' => 'fa-barcode', 'Tag' => 'fa-tag', 'Tags' => 'fa-tags', 'Book' => 'fa-book', 'Bookmark' => 'fa-bookmark', 'Print' => 'fa-print', 'Camera' => 'fa-camera', 'Font' => 'fa-font', 'Bold' => 'fa-bold', 'Italic' => 'fa-italic', 'Text Height' => 'fa-text-height', 'Text Width' => 'fa-text-width', 'Align (Left)' => 'fa-align-left', 'Align Center' => 'fa-align-center', 'Align (Right)' => 'fa-align-right', 'Align Justify' => 'fa-align-justify', 'List' => 'fa-list', 'Outdent' => 'fa-outdent', 'Indent' => 'fa-indent', 'Video Camera' => 'fa-video-camera', 'Picture Outlined' => 'fa-picture-o', 'Pencil' => 'fa-pencil', 'Map Marker' => 'fa-map-marker', 'Adjust' => 'fa-adjust', 'Tint' => 'fa-tint', 'Pencil Square Outlined' => 'fa-pencil-square-o', 'Share Square Outlined' => 'fa-share-square-o', 'Check Square Outlined' => 'fa-check-square-o', 'Arrows' => 'fa-arrows', 'Step Backward' => 'fa-step-backward', 'Fast Backward' => 'fa-fast-backward', 'Backward' => 'fa-backward', 'Play' => 'fa-play', 'Pause' => 'fa-pause', 'Stop' => 'fa-stop', 'Forward' => 'fa-forward', 'Fast Forward' => 'fa-fast-forward', 'Step Forward' => 'fa-step-forward', 'Eject' => 'fa-eject', 'Chevron (Left)' => 'fa-chevron-left', 'Chevron (Right)' => 'fa-chevron-right', 'Plus Circle' => 'fa-plus-circle', 'Minus Circle' => 'fa-minus-circle', 'Times Circle' => 'fa-times-circle', 'Check Circle' => 'fa-check-circle', 'Question Circle' => 'fa-question-circle', 'Info Circle' => 'fa-info-circle', 'Crosshairs' => 'fa-crosshairs', 'Times Circle Outlined' => 'fa-times-circle-o', 'Check Circle Outlined' => 'fa-check-circle-o', 'Ban' => 'fa-ban', 'Arrow (Left)' => 'fa-arrow-left', 'Arrow (Right)' => 'fa-arrow-right', 'Arrow (Up)' => 'fa-arrow-up', 'Arrow (Down)' => 'fa-arrow-down', 'Share' => 'fa-share', 'Expand' => 'fa-expand', 'Compress' => 'fa-compress', 'Plus' => 'fa-plus', 'Minus' => 'fa-minus', 'Asterisk' => 'fa-asterisk', 'Exclamation Circle' => 'fa-exclamation-circle', 'Gift' => 'fa-gift', 'Leaf' => 'fa-leaf', 'Fire' => 'fa-fire', 'Eye' => 'fa-eye', 'Eye Slash' => 'fa-eye-slash', 'Exclamation Triangle' => 'fa-exclamation-triangle', 'Plane' => 'fa-plane', 'Calendar' => 'fa-calendar', 'Random' => 'fa-random', 'Comment' => 'fa-comment', 'Magnet' => 'fa-magnet', 'Chevron (Up)' => 'fa-chevron-up', 'Chevron (Down)' => 'fa-chevron-down', 'Retweet' => 'fa-retweet', 'Shopping Cart' => 'fa-shopping-cart', 'Folder' => 'fa-folder', 'Folder Open' => 'fa-folder-open', 'Arrows V' => 'fa-arrows-v', 'Arrows H' => 'fa-arrows-h', 'Bar Chart' => 'fa-bar-chart', 'Twitter Square' => 'fa-twitter-square', 'Facebook Square' => 'fa-facebook-square', 'Camera Retro' => 'fa-camera-retro', 'Key' => 'fa-key', 'Cogs' => 'fa-cogs', 'Comments' => 'fa-comments', 'Thumbs Outlined (Up)' => 'fa-thumbs-o-up', 'Thumbs Outlined (Down)' => 'fa-thumbs-o-down', 'Star Half' => 'fa-star-half', 'Heart Outlined' => 'fa-heart-o', 'Sign Out' => 'fa-sign-out', 'Linkedin Square' => 'fa-linkedin-square', 'Thumb Tack' => 'fa-thumb-tack', 'External Link' => 'fa-external-link', 'Sign In' => 'fa-sign-in', 'Trophy' => 'fa-trophy', 'Github Square' => 'fa-github-square', 'Upload' => 'fa-upload', 'Lemon Outlined' => 'fa-lemon-o', 'Phone' => 'fa-phone', 'Square Outlined' => 'fa-square-o', 'Bookmark Outlined' => 'fa-bookmark-o', 'Phone Square' => 'fa-phone-square', 'Twitter' => 'fa-twitter', 'Facebook' => 'fa-facebook', 'Github' => 'fa-github', 'Unlock' => 'fa-unlock', 'Credit Card' => 'fa-credit-card', 'Rss' => 'fa-rss', 'Hdd Outlined' => 'fa-hdd-o', 'Bullhorn' => 'fa-bullhorn', 'Bell' => 'fa-bell', 'Certificate' => 'fa-certificate', 'Hand Outlined (Right)' => 'fa-hand-o-right', 'Hand Outlined (Left)' => 'fa-hand-o-left', 'Hand Outlined (Up)' => 'fa-hand-o-up', 'Hand Outlined (Down)' => 'fa-hand-o-down', 'Arrow Circle (Left)' => 'fa-arrow-circle-left', 'Arrow Circle (Right)' => 'fa-arrow-circle-right', 'Arrow Circle (Up)' => 'fa-arrow-circle-up', 'Arrow Circle (Down)' => 'fa-arrow-circle-down', 'Globe' => 'fa-globe', 'Wrench' => 'fa-wrench', 'Tasks' => 'fa-tasks', 'Filter' => 'fa-filter', 'Briefcase' => 'fa-briefcase', 'Arrows Alt' => 'fa-arrows-alt', 'Users' => 'fa-users', 'Link' => 'fa-link', 'Cloud' => 'fa-cloud', 'Flask' => 'fa-flask', 'Scissors' => 'fa-scissors', 'Files Outlined' => 'fa-files-o', 'Paperclip' => 'fa-paperclip', 'Floppy Outlined' => 'fa-floppy-o', 'Square' => 'fa-square', 'Bars' => 'fa-bars', 'List Ul' => 'fa-list-ul', 'List Ol' => 'fa-list-ol', 'Strikethrough' => 'fa-strikethrough', 'Underline' => 'fa-underline', 'Table' => 'fa-table', 'Magic' => 'fa-magic', 'Truck' => 'fa-truck', 'Pinterest' => 'fa-pinterest', 'Pinterest Square' => 'fa-pinterest-square', 'Google Plus Square' => 'fa-google-plus-square', 'Google Plus' => 'fa-google-plus', 'Money' => 'fa-money', 'Caret (Down)' => 'fa-caret-down', 'Caret (Up)' => 'fa-caret-up', 'Caret (Left)' => 'fa-caret-left', 'Caret (Right)' => 'fa-caret-right', 'Columns' => 'fa-columns', 'Sort' => 'fa-sort', 'Sort Desc' => 'fa-sort-desc', 'Sort Asc' => 'fa-sort-asc', 'Envelope' => 'fa-envelope', 'Linkedin' => 'fa-linkedin', 'Undo' => 'fa-undo', 'Gavel' => 'fa-gavel', 'Tachometer' => 'fa-tachometer', 'Comment Outlined' => 'fa-comment-o', 'Comments Outlined' => 'fa-comments-o', 'Bolt' => 'fa-bolt', 'Sitemap' => 'fa-sitemap', 'Umbrella' => 'fa-umbrella', 'Clipboard' => 'fa-clipboard', 'Lightbulb Outlined' => 'fa-lightbulb-o', 'Exchange' => 'fa-exchange', 'Cloud Download' => 'fa-cloud-download', 'Cloud Upload' => 'fa-cloud-upload', 'User Md' => 'fa-user-md', 'Stethoscope' => 'fa-stethoscope', 'Suitcase' => 'fa-suitcase', 'Bell Outlined' => 'fa-bell-o', 'Coffee' => 'fa-coffee', 'Cutlery' => 'fa-cutlery', 'File Text Outlined' => 'fa-file-text-o', 'Building Outlined' => 'fa-building-o', 'Hospital Outlined' => 'fa-hospital-o', 'Ambulance' => 'fa-ambulance', 'Medkit' => 'fa-medkit', 'Fighter Jet' => 'fa-fighter-jet', 'Beer' => 'fa-beer', 'H Square' => 'fa-h-square', 'Plus Square' => 'fa-plus-square', 'Angle Double (Left)' => 'fa-angle-double-left', 'Angle Double (Right)' => 'fa-angle-double-right', 'Angle Double (Up)' => 'fa-angle-double-up', 'Angle Double (Down)' => 'fa-angle-double-down', 'Angle (Left)' => 'fa-angle-left', 'Angle (Right)' => 'fa-angle-right', 'Angle (Up)' => 'fa-angle-up', 'Angle (Down)' => 'fa-angle-down', 'Desktop' => 'fa-desktop', 'Laptop' => 'fa-laptop', 'Tablet' => 'fa-tablet', 'Mobile' => 'fa-mobile', 'Circle Outlined' => 'fa-circle-o', 'Quote (Left)' => 'fa-quote-left', 'Quote (Right)' => 'fa-quote-right', 'Spinner' => 'fa-spinner', 'Circle' => 'fa-circle', 'Reply' => 'fa-reply', 'Github Alt' => 'fa-github-alt', 'Folder Outlined' => 'fa-folder-o', 'Folder Open Outlined' => 'fa-folder-open-o', 'Smile Outlined' => 'fa-smile-o', 'Frown Outlined' => 'fa-frown-o', 'Meh Outlined' => 'fa-meh-o', 'Gamepad' => 'fa-gamepad', 'Keyboard Outlined' => 'fa-keyboard-o', 'Flag Outlined' => 'fa-flag-o', 'Flag Checkered' => 'fa-flag-checkered', 'Terminal' => 'fa-terminal', 'Code' => 'fa-code', 'Reply All' => 'fa-reply-all', 'Star Half Outlined' => 'fa-star-half-o', 'Location Arrow' => 'fa-location-arrow', 'Crop' => 'fa-crop', 'Code Fork' => 'fa-code-fork', 'Chain Broken' => 'fa-chain-broken', 'Question' => 'fa-question', 'Info' => 'fa-info', 'Exclamation' => 'fa-exclamation', 'Superscript' => 'fa-superscript', 'Subscript' => 'fa-subscript', 'Eraser' => 'fa-eraser', 'Puzzle Piece' => 'fa-puzzle-piece', 'Microphone' => 'fa-microphone', 'Microphone Slash' => 'fa-microphone-slash', 'Shield' => 'fa-shield', 'Calendar Outlined' => 'fa-calendar-o', 'Fire Extinguisher' => 'fa-fire-extinguisher', 'Rocket' => 'fa-rocket', 'Maxcdn' => 'fa-maxcdn', 'Chevron Circle (Left)' => 'fa-chevron-circle-left', 'Chevron Circle (Right)' => 'fa-chevron-circle-right', 'Chevron Circle (Up)' => 'fa-chevron-circle-up', 'Chevron Circle (Down)' => 'fa-chevron-circle-down', 'Html5' => 'fa-html5', 'Css3' => 'fa-css3', 'Anchor' => 'fa-anchor', 'Unlock Alt' => 'fa-unlock-alt', 'Bullseye' => 'fa-bullseye', 'Ellipsis H' => 'fa-ellipsis-h', 'Ellipsis V' => 'fa-ellipsis-v', 'Rss Square' => 'fa-rss-square', 'Play Circle' => 'fa-play-circle', 'Ticket' => 'fa-ticket', 'Minus Square' => 'fa-minus-square', 'Minus Square Outlined' => 'fa-minus-square-o', 'Level (Up)' => 'fa-level-up', 'Level (Down)' => 'fa-level-down', 'Check Square' => 'fa-check-square', 'Pencil Square' => 'fa-pencil-square', 'External Link Square' => 'fa-external-link-square', 'Share Square' => 'fa-share-square', 'Compass' => 'fa-compass', 'Caret Square Outlined (Down)' => 'fa-caret-square-o-down', 'Caret Square Outlined (Up)' => 'fa-caret-square-o-up', 'Caret Square Outlined (Right)' => 'fa-caret-square-o-right', 'Eur' => 'fa-eur', 'Gbp' => 'fa-gbp', 'Usd' => 'fa-usd', 'Inr' => 'fa-inr', 'Jpy' => 'fa-jpy', 'Rub' => 'fa-rub', 'Krw' => 'fa-krw', 'Btc' => 'fa-btc', 'File' => 'fa-file', 'File Text' => 'fa-file-text', 'Sort Alpha Asc' => 'fa-sort-alpha-asc', 'Sort Alpha Desc' => 'fa-sort-alpha-desc', 'Sort Amount Asc' => 'fa-sort-amount-asc', 'Sort Amount Desc' => 'fa-sort-amount-desc', 'Sort Numeric Asc' => 'fa-sort-numeric-asc', 'Sort Numeric Desc' => 'fa-sort-numeric-desc', 'Thumbs (Up)' => 'fa-thumbs-up', 'Thumbs (Down)' => 'fa-thumbs-down', 'Youtube Square' => 'fa-youtube-square', 'Youtube' => 'fa-youtube', 'Xing' => 'fa-xing', 'Xing Square' => 'fa-xing-square', 'Youtube Play' => 'fa-youtube-play', 'Dropbox' => 'fa-dropbox', 'Stack Overflow' => 'fa-stack-overflow', 'Instagram' => 'fa-instagram', 'Flickr' => 'fa-flickr', 'Adn' => 'fa-adn', 'Bitbucket' => 'fa-bitbucket', 'Bitbucket Square' => 'fa-bitbucket-square', 'Tumblr' => 'fa-tumblr', 'Tumblr Square' => 'fa-tumblr-square', 'Long Arrow (Down)' => 'fa-long-arrow-down', 'Long Arrow (Up)' => 'fa-long-arrow-up', 'Long Arrow (Left)' => 'fa-long-arrow-left', 'Long Arrow (Right)' => 'fa-long-arrow-right', 'Apple' => 'fa-apple', 'Windows' => 'fa-windows', 'Android' => 'fa-android', 'Linux' => 'fa-linux', 'Dribbble' => 'fa-dribbble', 'Skype' => 'fa-skype', 'Foursquare' => 'fa-foursquare', 'Trello' => 'fa-trello', 'Female' => 'fa-female', 'Male' => 'fa-male', 'Gratipay' => 'fa-gratipay', 'Sun Outlined' => 'fa-sun-o', 'Moon Outlined' => 'fa-moon-o', 'Archive' => 'fa-archive', 'Bug' => 'fa-bug', 'Vk' => 'fa-vk', 'Weibo' => 'fa-weibo', 'Renren' => 'fa-renren', 'Pagelines' => 'fa-pagelines', 'Stack Exchange' => 'fa-stack-exchange', 'Arrow Circle Outlined (Right)' => 'fa-arrow-circle-o-right', 'Arrow Circle Outlined (Left)' => 'fa-arrow-circle-o-left', 'Caret Square Outlined (Left)' => 'fa-caret-square-o-left', 'Dot Circle Outlined' => 'fa-dot-circle-o', 'Wheelchair' => 'fa-wheelchair', 'Vimeo Square' => 'fa-vimeo-square', 'Try' => 'fa-try', 'Plus Square Outlined' => 'fa-plus-square-o', 'Space Shuttle' => 'fa-space-shuttle', 'Slack' => 'fa-slack', 'Envelope Square' => 'fa-envelope-square', 'Wordpress' => 'fa-wordpress', 'Openid' => 'fa-openid', 'University' => 'fa-university', 'Graduation Cap' => 'fa-graduation-cap', 'Yahoo' => 'fa-yahoo', 'Google' => 'fa-google', 'Reddit' => 'fa-reddit', 'Reddit Square' => 'fa-reddit-square', 'Stumbleupon Circle' => 'fa-stumbleupon-circle', 'Stumbleupon' => 'fa-stumbleupon', 'Delicious' => 'fa-delicious', 'Digg' => 'fa-digg', 'Pied Piper Pp' => 'fa-pied-piper-pp', 'Pied Piper Alt' => 'fa-pied-piper-alt', 'Drupal' => 'fa-drupal', 'Joomla' => 'fa-joomla', 'Language' => 'fa-language', 'Fax' => 'fa-fax', 'Building' => 'fa-building', 'Child' => 'fa-child', 'Paw' => 'fa-paw', 'Spoon' => 'fa-spoon', 'Cube' => 'fa-cube', 'Cubes' => 'fa-cubes', 'Behance' => 'fa-behance', 'Behance Square' => 'fa-behance-square', 'Steam' => 'fa-steam', 'Steam Square' => 'fa-steam-square', 'Recycle' => 'fa-recycle', 'Car' => 'fa-car', 'Taxi' => 'fa-taxi', 'Tree' => 'fa-tree', 'Spotify' => 'fa-spotify', 'Deviantart' => 'fa-deviantart', 'Soundcloud' => 'fa-soundcloud', 'Database' => 'fa-database', 'File Pdf Outlined' => 'fa-file-pdf-o', 'File Word Outlined' => 'fa-file-word-o', 'File Excel Outlined' => 'fa-file-excel-o', 'File Powerpoint Outlined' => 'fa-file-powerpoint-o', 'File Image Outlined' => 'fa-file-image-o', 'File Archive Outlined' => 'fa-file-archive-o', 'File Audio Outlined' => 'fa-file-audio-o', 'File Video Outlined' => 'fa-file-video-o', 'File Code Outlined' => 'fa-file-code-o', 'Vine' => 'fa-vine', 'Codepen' => 'fa-codepen', 'Jsfiddle' => 'fa-jsfiddle', 'Life Ring' => 'fa-life-ring', 'Circle Outlined Notch' => 'fa-circle-o-notch', 'Rebel' => 'fa-rebel', 'Empire' => 'fa-empire', 'Git Square' => 'fa-git-square', 'Git' => 'fa-git', 'Hacker News' => 'fa-hacker-news', 'Tencent Weibo' => 'fa-tencent-weibo', 'Qq' => 'fa-qq', 'Weixin' => 'fa-weixin', 'Paper Plane' => 'fa-paper-plane', 'Paper Plane Outlined' => 'fa-paper-plane-o', 'History' => 'fa-history', 'Circle Thin' => 'fa-circle-thin', 'Header' => 'fa-header', 'Paragraph' => 'fa-paragraph', 'Sliders' => 'fa-sliders', 'Share Alt' => 'fa-share-alt', 'Share Alt Square' => 'fa-share-alt-square', 'Bomb' => 'fa-bomb', 'Futbol Outlined' => 'fa-futbol-o', 'Tty' => 'fa-tty', 'Binoculars' => 'fa-binoculars', 'Plug' => 'fa-plug', 'Slideshare' => 'fa-slideshare', 'Twitch' => 'fa-twitch', 'Yelp' => 'fa-yelp', 'Newspaper Outlined' => 'fa-newspaper-o', 'Wifi' => 'fa-wifi', 'Calculator' => 'fa-calculator', 'Paypal' => 'fa-paypal', 'Google Wallet' => 'fa-google-wallet', 'Cc Visa' => 'fa-cc-visa', 'Cc Mastercard' => 'fa-cc-mastercard', 'Cc Discover' => 'fa-cc-discover', 'Cc Amex' => 'fa-cc-amex', 'Cc Paypal' => 'fa-cc-paypal', 'Cc Stripe' => 'fa-cc-stripe', 'Bell Slash' => 'fa-bell-slash', 'Bell Slash Outlined' => 'fa-bell-slash-o', 'Trash' => 'fa-trash', 'Copy(Right)' => 'fa-copyright', 'At' => 'fa-at', 'Eyedropper' => 'fa-eyedropper', 'Paint Brush' => 'fa-paint-brush', 'Birthday Cake' => 'fa-birthday-cake', 'Area Chart' => 'fa-area-chart', 'Pie Chart' => 'fa-pie-chart', 'Line Chart' => 'fa-line-chart', 'Lastfm' => 'fa-lastfm', 'Lastfm Square' => 'fa-lastfm-square', 'Toggle Off' => 'fa-toggle-off', 'Toggle On' => 'fa-toggle-on', 'Bicycle' => 'fa-bicycle', 'Bus' => 'fa-bus', 'Ioxhost' => 'fa-ioxhost', 'Angellist' => 'fa-angellist', 'Cc' => 'fa-cc', 'Ils' => 'fa-ils', 'Meanpath' => 'fa-meanpath', 'Buysellads' => 'fa-buysellads', 'Connectdevelop' => 'fa-connectdevelop', 'Dashcube' => 'fa-dashcube', 'Forumbee' => 'fa-forumbee', 'Leanpub' => 'fa-leanpub', 'Sellsy' => 'fa-sellsy', 'Shirtsinbulk' => 'fa-shirtsinbulk', 'Simplybuilt' => 'fa-simplybuilt', 'Skyatlas' => 'fa-skyatlas', 'Cart Plus' => 'fa-cart-plus', 'Cart Arrow (Down)' => 'fa-cart-arrow-down', 'Diamond' => 'fa-diamond', 'Ship' => 'fa-ship', 'User Secret' => 'fa-user-secret', 'Motorcycle' => 'fa-motorcycle', 'Street View' => 'fa-street-view', 'Heartbeat' => 'fa-heartbeat', 'Venus' => 'fa-venus', 'Mars' => 'fa-mars', 'Mercury' => 'fa-mercury', 'Transgender' => 'fa-transgender', 'Transgender Alt' => 'fa-transgender-alt', 'Venus Double' => 'fa-venus-double', 'Mars Double' => 'fa-mars-double', 'Venus Mars' => 'fa-venus-mars', 'Mars Stroke' => 'fa-mars-stroke', 'Mars Stroke V' => 'fa-mars-stroke-v', 'Mars Stroke H' => 'fa-mars-stroke-h', 'Neuter' => 'fa-neuter', 'Genderless' => 'fa-genderless', 'Facebook Official' => 'fa-facebook-official', 'Pinterest P' => 'fa-pinterest-p', 'Whatsapp' => 'fa-whatsapp', 'Server' => 'fa-server', 'User Plus' => 'fa-user-plus', 'User Times' => 'fa-user-times', 'Bed' => 'fa-bed', 'Viacoin' => 'fa-viacoin', 'Train' => 'fa-train', 'Subway' => 'fa-subway', 'Medium' => 'fa-medium', 'Y Combinator' => 'fa-y-combinator', 'Optin Monster' => 'fa-optin-monster', 'Opencart' => 'fa-opencart', 'Expeditedssl' => 'fa-expeditedssl', 'Battery Full' => 'fa-battery-full', 'Battery Three Quarters' => 'fa-battery-three-quarters', 'Battery Half' => 'fa-battery-half', 'Battery Quarter' => 'fa-battery-quarter', 'Battery Empty' => 'fa-battery-empty', 'Mouse Pointer' => 'fa-mouse-pointer', 'I Cursor' => 'fa-i-cursor', 'Object Gro(Up)' => 'fa-object-group', 'Object Ungro(Up)' => 'fa-object-ungroup', 'Sticky Note' => 'fa-sticky-note', 'Sticky Note Outlined' => 'fa-sticky-note-o', 'Cc Jcb' => 'fa-cc-jcb', 'Cc Diners Club' => 'fa-cc-diners-club', 'Clone' => 'fa-clone', 'Balance Scale' => 'fa-balance-scale', 'Hourglass Outlined' => 'fa-hourglass-o', 'Hourglass Start' => 'fa-hourglass-start', 'Hourglass Half' => 'fa-hourglass-half', 'Hourglass End' => 'fa-hourglass-end', 'Hourglass' => 'fa-hourglass', 'Hand Rock Outlined' => 'fa-hand-rock-o', 'Hand Paper Outlined' => 'fa-hand-paper-o', 'Hand Scissors Outlined' => 'fa-hand-scissors-o', 'Hand Lizard Outlined' => 'fa-hand-lizard-o', 'Hand Spock Outlined' => 'fa-hand-spock-o', 'Hand Pointer Outlined' => 'fa-hand-pointer-o', 'Hand Peace Outlined' => 'fa-hand-peace-o', 'Trademark' => 'fa-trademark', 'Registered' => 'fa-registered', 'Creative Commons' => 'fa-creative-commons', 'Gg' => 'fa-gg', 'Gg Circle' => 'fa-gg-circle', 'Tripadvisor' => 'fa-tripadvisor', 'Odnoklassniki' => 'fa-odnoklassniki', 'Odnoklassniki Square' => 'fa-odnoklassniki-square', 'Get Pocket' => 'fa-get-pocket', 'Wikipedia W' => 'fa-wikipedia-w', 'Safari' => 'fa-safari', 'Chrome' => 'fa-chrome', 'Firefox' => 'fa-firefox', 'Opera' => 'fa-opera', 'Internet Explorer' => 'fa-internet-explorer', 'Television' => 'fa-television', 'Contao' => 'fa-contao', '500px' => 'fa-500px', 'Amazon' => 'fa-amazon', 'Calendar Plus Outlined' => 'fa-calendar-plus-o', 'Calendar Minus Outlined' => 'fa-calendar-minus-o', 'Calendar Times Outlined' => 'fa-calendar-times-o', 'Calendar Check Outlined' => 'fa-calendar-check-o', 'Industry' => 'fa-industry', 'Map Pin' => 'fa-map-pin', 'Map Signs' => 'fa-map-signs', 'Map Outlined' => 'fa-map-o', 'Map' => 'fa-map', 'Commenting' => 'fa-commenting', 'Commenting Outlined' => 'fa-commenting-o', 'Houzz' => 'fa-houzz', 'Vimeo' => 'fa-vimeo', 'Black Tie' => 'fa-black-tie', 'Fonticons' => 'fa-fonticons', 'Reddit Alien' => 'fa-reddit-alien', 'Edge' => 'fa-edge', 'Credit Card Alt' => 'fa-credit-card-alt', 'Codiepie' => 'fa-codiepie', 'Modx' => 'fa-modx', 'Fort Awesome' => 'fa-fort-awesome', 'Usb' => 'fa-usb', 'Product Hunt' => 'fa-product-hunt', 'Mixcloud' => 'fa-mixcloud', 'Scribd' => 'fa-scribd', 'Pause Circle' => 'fa-pause-circle', 'Pause Circle Outlined' => 'fa-pause-circle-o', 'Stop Circle' => 'fa-stop-circle', 'Stop Circle Outlined' => 'fa-stop-circle-o', 'Shopping Bag' => 'fa-shopping-bag', 'Shopping Basket' => 'fa-shopping-basket', 'Hashtag' => 'fa-hashtag', 'Bluetooth' => 'fa-bluetooth', 'Bluetooth B' => 'fa-bluetooth-b', 'Percent' => 'fa-percent', 'Gitlab' => 'fa-gitlab', 'Wpbeginner' => 'fa-wpbeginner', 'Wpforms' => 'fa-wpforms', 'Envira' => 'fa-envira', 'Universal Access' => 'fa-universal-access', 'Wheelchair Alt' => 'fa-wheelchair-alt', 'Question Circle Outlined' => 'fa-question-circle-o', 'Blind' => 'fa-blind', 'Audio Description' => 'fa-audio-description', 'Volume Control Phone' => 'fa-volume-control-phone', 'Braille' => 'fa-braille', 'Assistive Listening Systems' => 'fa-assistive-listening-systems', 'American Sign Language Interpreting' => 'fa-american-sign-language-interpreting', 'Deaf' => 'fa-deaf', 'Glide' => 'fa-glide', 'Glide G' => 'fa-glide-g', 'Sign Language' => 'fa-sign-language', 'Low Vision' => 'fa-low-vision', 'Viadeo' => 'fa-viadeo', 'Viadeo Square' => 'fa-viadeo-square', 'Snapchat' => 'fa-snapchat', 'Snapchat Ghost' => 'fa-snapchat-ghost', 'Snapchat Square' => 'fa-snapchat-square', 'Pied Piper' => 'fa-pied-piper', 'First Order' => 'fa-first-order', 'Yoast' => 'fa-yoast', 'Themeisle' => 'fa-themeisle', 'Google Plus Official' => 'fa-google-plus-official', 'Font Awesome' => 'fa-font-awesome', );
								
public $_ultSettings = array(
'menu_settings' => array(
		'callout_link' => array(
									'callout_text' => '<p>Menu Link Styling</p>',
									'type' => 'callout',
								),
		'theme_navbar_link_margin_top' => array(
									'name' => 'Menu Link Margin Top',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_margin_bottom' => array(
									'name' => 'Menu Link Margin Bottom',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_padding_top' => array(
									'name' => 'Menu Link Padding Top',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_padding_bottom' => array(
									'name' => 'Menu Link Padding Bottom',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_padding_left_right' => array(
									'name' => 'Menu Link Padding Left/Right',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_divider_width' => array(
									'name' => 'Menu Link Divider Width',
									'type' => 'text',
									'help' => 'Please enter just integer value and the unit is pixel'
								),
		'theme_navbar_link_divider_color' => array(
									'name' => 'Menu Link Divider Color',
									'type' => 'color',
								),   
		'theme_navbar_text_color' => array(
									'name' => 'Menu Text Color',
									'type' => 'color',
								),  
		'theme_navbar_link_color' => array(
									'name' => 'Menu Link Color',
									'type' => 'color',
								), 
		'theme_navbar_link_bg_color' => array(
									'name' => 'Menu Link Background Color',
									'type' => 'color',
								), 
		'theme_navbar_link_top_bg_color' => array(
									'name' => 'Menu Link Background Color Top',
									'type' => 'color',
									'help' => 'Use these values to create a linear vertical gradient for menu items. Gradient is a blend of top and bottom colors.'
								), 
		'theme_navbar_link_bottom_bg_color' => array(
									'name' => 'Menu Link Background Color Bottom',
									'type' => 'color',
									'help' => 'Use these values to create a linear vertical gradient for menu items. Gradient is a blend of top and bottom colors.'
								),
		'theme_navbar_link_box_shadow' => array(
									'name' => 'Menu Link Box Shadow',
									'type' => 'text',
									'help' => 'Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")',
								),  
		'theme_navbar_link_box_shadow_color' => array(
									'name' => 'Menu Link Box Shadow Color',
									'type' => 'color',
								),
		'callout_link_hover' => array(
									'callout_text' => '<p>Menu Link Hover Styling</p>',
									'type' => 'callout',
								),
		'theme_navbar_link_hover_color' => array(
									'name' => 'Menu Link Hover Color',
									'type' => 'color',
								), 
		'theme_navbar_link_hover_bg_color' => array(
									'name' => 'Menu Link Hover Background Color',
									'type' => 'color',
								), 
		'theme_navbar_link_hover_top_bg_color' => array(
									'name' => 'Menu Link Hover Background Color Top',
									'type' => 'color',
									'help' => 'Use these values to create a linear vertical gradient for menu items hover state. Gradient is a blend of top and bottom colors.'
								), 
		'theme_navbar_link_hover_bottom_bg_color' => array(
									'name' => 'Menu Link Hover Background Color Bottom',
									'type' => 'color',
									'help' => 'Use these values to create a linear vertical gradient for menu items hover state. Gradient is a blend of top and bottom colors.'
								), 
		'theme_navbar_link_hover_box_shadow' => array(
									'name' => 'Menu Link Hover Box Shadow',
									'type' => 'text',
									'help' => 'Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")',
								),  
		'theme_navbar_link_hover_box_shadow_color' => array(
									'name' => 'Menu Link Hover Box Shadow Color',
									'type' => 'color',
								),
		'callout_link_active' => array(
									'callout_text' => '<p>Menu Link Active State Styling</p>',
									'type' => 'callout',
								),
		'theme_navbar_link_active_color' => array(
									'name' => 'Menu Link Active Color',
									'type' => 'color',
								), 
		'theme_navbar_link_active_bg_color' => array(
									'name' => 'Menu Link Active Background Color',
									'type' => 'color',
								), 
		'theme_navbar_link_active_top_bg_color' => array(
									'name' => 'Menu Link Active Background Color Top',
									'type' => 'color',
								), 
		'theme_navbar_link_active_bottom_bg_color' => array(
									'name' => 'Menu Link Active Background Color Bottom',
									'type' => 'color',
								), 
		'theme_navbar_link_active_box_shadow' => array(
									'name' => 'Menu Link Active Box Shadow',
									'type' => 'text',
									'help' => 'Specifying in order the horizontal offset, vertical offset, optional blur distance and optional spread distance of the shadow (e.g. "0 1px 5px" or in case of inset shadow "inset 0 1px 5px")',
								),  
		'theme_navbar_link_active_box_shadow_color' => array(
									'name' => 'Menu Link Active Box Shadow Color',
									'type' => 'color',
								),
		'callout_menu_toggle' => array(
									'callout_text' => '<p>Menu Toggle Styling</p>',
									'type' => 'callout',
								),
		'theme_navbar_toggle_hover_bg_color' => array(
									'name' => 'Menu Toggle Hover Background Color',
									'type' => 'color',
								), 
		'theme_navbar_toggle_icon_bar_bg_color' => array(
									'name' => 'Menu Toggle Icon Background Color',
									'type' => 'color',
								),
		'theme_navbar_toggle_icon_bar_hover_bg_color' => array(
									'name' => 'Menu Toggle Icon Hover Background Color',
									'type' => 'color',
								), 
		'theme_navbar_toggle_border_color' => array(
									'name' => 'Menu Toggle Border Color',
									'type' => 'color',
								)		
)
);

// used for merge stuff
public $_menuColorPickers = array(
	'theme_navbar_box_shadow_color',
	'theme_navbar_bg_color',
	'theme_navbar_hover_bg_color',
	'theme_navbar_sticky_bg_color',
	'theme_navbar_border_color',
	'theme_navbar_link_divider_color',
	'theme_navbar_text_color',
	'theme_navbar_link_color',
	'theme_navbar_link_bg_color',
	'theme_navbar_link_top_bg_color',
	'theme_navbar_link_bottom_bg_color',
	'theme_navbar_link_hover_color',
	'theme_navbar_link_hover_bg_color',
	'theme_navbar_link_hover_top_bg_color',
	'theme_navbar_link_hover_bottom_bg_color',
	'theme_navbar_link_active_color',
	'theme_navbar_link_active_bg_color',
	'theme_navbar_link_active_top_bg_color',
	'theme_navbar_link_active_bottom_bg_color',
	'theme_navbar_toggle_hover_bg_color',
	'theme_navbar_toggle_icon_bar_bg_color',
	'theme_navbar_toggle_icon_bar_hover_bg_color',
	'theme_navbar_toggle_border_color',
	'theme_navbar_top_bg_color',
	'theme_navbar_bottom_bg_color',
	'theme_navbar_drop_box_shadow_color',
	'theme_navbar_drop_border_color',
	'theme_navbar_drop_color',
	'theme_navbar_drop_hover_color',
	'theme_navbar_drop_acolor',
	'theme_navbar_drop_ahover',
	'theme_navbar_link_box_shadow_color',
	'theme_navbar_link_hover_box_shadow_color',
	'theme_navbar_link_active_box_shadow_color'
);

// transparency disabled pickers
public $_menuNonTransparentPickers = array(
	'theme_navbar_text_color',
	'theme_navbar_link_color',
	'theme_navbar_link_hover_color',
	'theme_navbar_link_active_color',
	'theme_navbar_toggle_icon_bar_bg_color',
	'theme_navbar_toggle_icon_bar_hover_bg_color',
	'theme_navbar_drop_acolor',
	'theme_navbar_drop_ahover'
);
	
public static function sdf_core_theme_options (){
	$core_theme_options = 'YToxNTp7czo2OiJwcmVzZXQiO2E6Mjp7czoxMzoidGhlbWVfcHJlc2V0cyI7YToxOntpOjE7czoxMToiRGVmYXVsdCBTREYiO31zOjEyOiJ0aGVtZV9wcmVzZXQiO3M6MToiMSI7fXM6MTA6Im5hdmlnYXRpb24iO2E6MTp7aToxO2E6ODp7czoxNzoidG9nZ2xlX2N1c3RvbV9uYXYiO3M6MToiMCI7czoxNToiY3VzdG9tX25hdl9uYW1lIjtzOjE4OiJ3cHUtc2VsZWN0LWRlZmF1bHQiO3M6MTE6Im1lbnVfbGF5b3V0IjtzOjExOiJzdGlja3lfbWVudSI7czoxOToic3RpY2t5X21lbnVfb3BhY2l0eSI7czowOiIiO3M6MTQ6Im1lbnVfYWxpZ25tZW50IjtzOjU6InJpZ2h0IjtzOjEwOiJtZW51X3N0eWxlIjtzOjc6ImRlZmF1bHQiO3M6MTE6Im1lbnVfc3R5bGVzIjthOjE6e3M6NzoiZGVmYXVsdCI7czo3OiJEZWZhdWx0Ijt9czoxMzoibWVudV9zZXR0aW5ncyI7YTo3NTp7czoyNjoidGhlbWVfbmF2YmFyX2FwcGVuZF9zZWFyY2giO3M6Mjoibm8iO3M6MjM6InRoZW1lX25hdmJhcl9ib3hfc2hhZG93IjtzOjA6IiI7czoyOToidGhlbWVfbmF2YmFyX2JveF9zaGFkb3dfY29sb3IiO3M6MTI6IiMwMDAwMDA6MC4yNSI7czoyMjoidGhlbWVfbmF2YmFyX3RyYW5zZm9ybSI7czo5OiJ1cHBlcmNhc2UiO3M6MTk6InRoZW1lX25hdmJhcl93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjE3OiJ0aGVtZV9uYXZiYXJfc2l6ZSI7czoyOiIxMCI7czoyMjoidGhlbWVfbmF2YmFyX3NpemVfdHlwZSI7czoyOiJweCI7czoxOToidGhlbWVfbmF2YmFyX2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjcwMCI7czoyMjoidGhlbWVfbmF2YmFyX2N1c3RvbV9iZyI7czozOiJ5ZXMiO3M6MjE6InRoZW1lX25hdmJhcl9iZ19jb2xvciI7czo2OiJOYXYgQmciO3M6MjU6InRoZW1lX25hdmJhcl90b3BfYmdfY29sb3IiO3M6NToiOjAuNzQiO3M6Mjg6InRoZW1lX25hdmJhcl9ib3R0b21fYmdfY29sb3IiO3M6MDoiIjtzOjIxOiJ0aGVtZV9uYXZiYXJfYmdfaW1hZ2UiO3M6MDoiIjtzOjIxOiJ0aGVtZV9uYXZiYXJfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI1OiJ0aGVtZV9uYXZiYXJfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoyNjoidGhlbWVfbmF2YmFyX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjI1OiJ0aGVtZV9uYXZiYXJfYm9yZGVyX3dpZHRoIjtzOjA6IiI7czoyNToidGhlbWVfbmF2YmFyX2JvcmRlcl9jb2xvciI7czowOiIiO3M6MjM6InRoZW1lX25hdmJhcl9tYXJnaW5fdG9wIjtzOjA6IiI7czoyNjoidGhlbWVfbmF2YmFyX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjM4OiJ0aGVtZV9uYXZiYXJfY3VzdG9tX21lbnVfbGlua19zZXR0aW5ncyI7czozOiJ5ZXMiO3M6Mjg6InRoZW1lX25hdmJhcl9saW5rX21hcmdpbl90b3AiO3M6MDoiIjtzOjMxOiJ0aGVtZV9uYXZiYXJfbGlua19tYXJnaW5fYm90dG9tIjtzOjA6IiI7czoyOToidGhlbWVfbmF2YmFyX2xpbmtfcGFkZGluZ190b3AiO3M6MjoiMzAiO3M6MzI6InRoZW1lX25hdmJhcl9saW5rX3BhZGRpbmdfYm90dG9tIjtzOjI6IjMwIjtzOjM2OiJ0aGVtZV9uYXZiYXJfbGlua19wYWRkaW5nX2xlZnRfcmlnaHQiO3M6MjoiMTUiO3M6MzE6InRoZW1lX25hdmJhcl9saW5rX2RpdmlkZXJfd2lkdGgiO3M6MDoiIjtzOjMxOiJ0aGVtZV9uYXZiYXJfbGlua19kaXZpZGVyX2NvbG9yIjtzOjA6IiI7czoyMzoidGhlbWVfbmF2YmFyX3RleHRfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6MjM6InRoZW1lX25hdmJhcl9saW5rX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjI2OiJ0aGVtZV9uYXZiYXJfbGlua19iZ19jb2xvciI7czoxMToiVHJhbnNwYXJlbnQiO3M6MzA6InRoZW1lX25hdmJhcl9saW5rX3RvcF9iZ19jb2xvciI7czowOiIiO3M6MzM6InRoZW1lX25hdmJhcl9saW5rX2JvdHRvbV9iZ19jb2xvciI7czowOiIiO3M6Mjg6InRoZW1lX25hdmJhcl9saW5rX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM0OiJ0aGVtZV9uYXZiYXJfbGlua19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyOToidGhlbWVfbmF2YmFyX2xpbmtfaG92ZXJfY29sb3IiO3M6OToiTmF2IEhvdmVyIjtzOjMyOiJ0aGVtZV9uYXZiYXJfbGlua19ob3Zlcl9iZ19jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX25hdmJhcl9saW5rX2hvdmVyX3RvcF9iZ19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX25hdmJhcl9saW5rX2hvdmVyX2JvdHRvbV9iZ19jb2xvciI7czowOiIiO3M6MzQ6InRoZW1lX25hdmJhcl9saW5rX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQwOiJ0aGVtZV9uYXZiYXJfbGlua19ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozMDoidGhlbWVfbmF2YmFyX2xpbmtfYWN0aXZlX2NvbG9yIjtzOjk6Ik5hdiBIb3ZlciI7czozMzoidGhlbWVfbmF2YmFyX2xpbmtfYWN0aXZlX2JnX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfbmF2YmFyX2xpbmtfYWN0aXZlX3RvcF9iZ19jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX25hdmJhcl9saW5rX2FjdGl2ZV9ib3R0b21fYmdfY29sb3IiO3M6MDoiIjtzOjM1OiJ0aGVtZV9uYXZiYXJfbGlua19hY3RpdmVfYm94X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX25hdmJhcl9saW5rX2FjdGl2ZV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfbmF2YmFyX3RvZ2dsZV9ob3Zlcl9iZ19jb2xvciI7czo3OiIjNDA0MDQwIjtzOjM3OiJ0aGVtZV9uYXZiYXJfdG9nZ2xlX2ljb25fYmFyX2JnX2NvbG9yIjtzOjU6IldoaXRlIjtzOjQzOiJ0aGVtZV9uYXZiYXJfdG9nZ2xlX2ljb25fYmFyX2hvdmVyX2JnX2NvbG9yIjtzOjk6Ik5hdiBIb3ZlciI7czozMjoidGhlbWVfbmF2YmFyX3RvZ2dsZV9ib3JkZXJfY29sb3IiO3M6NToiV2hpdGUiO3M6MjM6InRoZW1lX25hdmJhcl9kcm9wX2NvbG9yIjtzOjY6Ik5hdiBCZyI7czoyOToidGhlbWVfbmF2YmFyX2Ryb3BfaG92ZXJfY29sb3IiO3M6NjoiTmF2IEJnIjtzOjI0OiJ0aGVtZV9uYXZiYXJfZHJvcF9hY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6MjQ6InRoZW1lX25hdmJhcl9kcm9wX2Fob3ZlciI7czo5OiJOYXYgSG92ZXIiO3M6Mjg6InRoZW1lX25hdmJhcl9kcm9wX2JveF9zaGFkb3ciO3M6MTA6IjAgOHB4IDE1cHgiO3M6MzQ6InRoZW1lX25hdmJhcl9kcm9wX2JveF9zaGFkb3dfY29sb3IiO3M6MTI6IiMwMDAwMDA6MC4xMCI7czoyNzoidGhlbWVfbmF2YmFyX2Ryb3BfdHJhbnNmb3JtIjtzOjk6InVwcGVyY2FzZSI7czoyNDoidGhlbWVfbmF2YmFyX2Ryb3Bfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czoyMjoidGhlbWVfbmF2YmFyX2Ryb3Bfc2l6ZSI7czoyOiIxMCI7czoyNzoidGhlbWVfbmF2YmFyX2Ryb3Bfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI0OiJ0aGVtZV9uYXZiYXJfZHJvcF9mYW1pbHkiO3M6MTQ6Ik1vbnRzZXJyYXQ6NDAwIjtzOjI3OiJ0aGVtZV9uYXZiYXJfaG92ZXJfYmdfY29sb3IiO3M6NjoiTmF2IEJnIjtzOjI4OiJ0aGVtZV9uYXZiYXJfc3RpY2t5X2JnX2NvbG9yIjtzOjY6Ik5hdiBCZyI7czozMDoidGhlbWVfbmF2YmFyX2Ryb3BfbGlua19wYWRkaW5nIjtzOjY6IjAgMTVweCI7czoyOToidGhlbWVfbmF2YmFyX2Ryb3BfbGlua19oZWlnaHQiO3M6NDoiMzZweCI7czoyMzoidGhlbWVfbmF2YmFyX3RyYW5zaXRpb24iO3M6MjU6ImJhY2tncm91bmQgLjRzLCBjb2xvciAuMnMiO3M6Mjg6InRoZW1lX25hdmJhcl9kcm9wX3RyYW5zaXRpb24iO3M6MTQ6IjAuMjVzIGVhc2Utb3V0IjtzOjI3OiJ0aGVtZV9uYXZiYXJfbGV0dGVyX3NwYWNpbmciO3M6MzoiM3B4IjtzOjMyOiJ0aGVtZV9uYXZiYXJfZHJvcF9sZXR0ZXJfc3BhY2luZyI7czozOiIzcHgiO3M6MjU6InRoZW1lX25hdmJhcl9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzA6InRoZW1lX25hdmJhcl9kcm9wX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozMToidGhlbWVfbmF2YmFyX2Ryb3BfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MzA6InRoZW1lX25hdmJhcl9kcm9wX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzA6InRoZW1lX25hdmJhcl9kcm9wX2JvcmRlcl9jb2xvciI7czo3OiJQcmltYXJ5Ijt9fX1zOjEwOiJ0eXBvZ3JhcGh5IjthOjE6e2k6MTthOjIxMDI6e3M6MTM6InByZXNldF9jb2xvcnMiO2E6MTA6e3M6MTE6IlRpdGxlIENvbG9yIjtzOjEyOiIjMzMzMzMzOjEuMDAiO3M6MTA6IlRleHQgQ29sb3IiO3M6MTI6IiMzMzMzMzM6MS4wMCI7czo1OiJXaGl0ZSI7czoxMjoiI2ZmZmZmZjoxLjAwIjtzOjQ6IkdyZXkiO3M6MTI6IiM2MzY2Njk6MS4wMCI7czo1OiJCbGFjayI7czoxMjoiIzExMTExMToxLjAwIjtzOjEwOiJMaWdodCBHcmV5IjtzOjEyOiIjZWNmMGYxOjEuMDAiO3M6MTI6IkxpZ2h0ZXIgR3JleSI7czoxMjoiI2Y3ZjdmNzoxLjAwIjtzOjY6Ik5hdiBCZyI7czoxMjoiIzI4MjgyODoxLjAwIjtzOjg6Ik5hdiBMaW5rIjtzOjEyOiIjNzc3Nzc3OjEuMDAiO3M6OToiTmF2IEhvdmVyIjtzOjEyOiIjZDVkNWQ1OjEuMDAiO31zOjE1OiJnb29nbGVfd2ViX2ZvbnQiO2E6NTp7aTowO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO2k6MTtzOjEzOiJPcGVuIFNhbnM6MzAwIjtpOjI7czoxNDoiTW9udHNlcnJhdDo3MDAiO2k6MztzOjE0OiJNb250c2VycmF0OjQwMCI7aTo0O3M6MTM6Ik9wZW4gU2Fuczo3MDAiO31zOjE5OiJ0aGVtZV9mb250X2ZhbWlsaWVzIjthOjE0OntpOjA7czoyNjoiQXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWYiO2k6MTtzOjI5OiJBcmlhbCBCbGFjayxHYWRnZXQsc2Fucy1zZXJpZiI7aToyO3M6MjE6IkNvbWljIFNhbnMgTVMsY3Vyc2l2ZSI7aTozO3M6Mjk6IkNvdXJpZXIgTmV3LENvdXJpZXIsbW9ub3NwYWNlIjtpOjQ7czoxMzoiR2VvcmdpYSxzZXJpZiI7aTo1O3M6NDE6IkhlbHZldGljYSBOZXVlLEhlbHZldGljYSxBcmlhbCxzYW5zLXNlcmlmIjtpOjY7czoyNjoiSW1wYWN0LENoYXJjb2FsLHNhbnMtc2VyaWYiO2k6NztzOjMxOiJMdWNpZGEgQ29uc29sZSxNb25hY28sbW9ub3NwYWNlIjtpOjg7czo0NDoiTHVjaWRhIFNhbnMgVW5pY29kZSxMdWNpZGEgR3JhbmRlLHNhbnMtc2VyaWYiO2k6OTtzOjQ1OiJQYWxhdGlubyBMaW5vdHlwZSxCb29rIEFudGlxdWEsUGFsYXRpbm8sc2VyaWYiO2k6MTA7czoyNDoiVGFob21hLEdlbmV2YSxzYW5zLXNlcmlmIjtpOjExO3M6Mjc6IlRpbWVzIE5ldyBSb21hbixUaW1lcyxzZXJpZiI7aToxMjtzOjMzOiJUcmVidWNoZXQgTVMsSGVsdmV0aWNhLHNhbnMtc2VyaWYiO2k6MTM7czoyNToiVmVyZGFuYSxHZW5ldmEsc2Fucy1zZXJpZiI7fXM6MjQ6InRoZW1lX2ZvbnRfd2lkZV9iZ19jb2xvciI7czowOiIiO3M6MjQ6InRoZW1lX2ZvbnRfd2lkZV9iZ19pbWFnZSI7czowOiIiO3M6MjU6InRoZW1lX2ZvbnRfd2lkZV9iZ19yZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI1OiJ0aGVtZV9mb250X2JveGVkX2JnX2NvbG9yIjtzOjEwOiJMaWdodCBHcmV5IjtzOjI1OiJ0aGVtZV9mb250X2JveGVkX2JnX2ltYWdlIjtzOjA6IiI7czoyNjoidGhlbWVfZm9udF9ib3hlZF9iZ19yZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI5OiJ0aGVtZV9mb250X2JveGVkX2JveF9iZ19jb2xvciI7czo1OiJXaGl0ZSI7czoyOToidGhlbWVfZm9udF9ib3hlZF9ib3hfYmdfaW1hZ2UiO3M6MDoiIjtzOjMwOiJ0aGVtZV9mb250X2JveGVkX2JveF9iZ19yZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI3OiJ0aGVtZV9mb250X2JveGVkX2JveF9tYXJnaW4iO3M6NDoiMjBweCI7czoyNzoidGhlbWVfZm9udF9ib3hlZF9ib3hfc2hhZG93IjtzOjk6IjAgM3B4IDlweCI7czozMzoidGhlbWVfZm9udF9ib3hlZF9ib3hfc2hhZG93X2NvbG9yIjtzOjQ6IkdyZXkiO3M6MzM6InRoZW1lX2ZvbnRfYm94ZWRfYm94X2JvcmRlcl9jb2xvciI7czowOiIiO3M6MzM6InRoZW1lX2ZvbnRfYm94ZWRfYm94X2JvcmRlcl93aWR0aCI7czowOiIiO3M6Mzg6InRoZW1lX2ZvbnRfYm94ZWRfYm94X2JvcmRlcl93aWR0aF90eXBlIjtzOjA6IiI7czozMzoidGhlbWVfZm9udF9ib3hlZF9ib3hfYm9yZGVyX3N0eWxlIjtzOjA6IiI7czoxNzoidGhlbWVfZm9udF9haG92ZXIiO3M6MDoiIjtzOjE3OiJ0aGVtZV9mb250X2FuY2hvciI7czowOiIiO3M6MTY6InRoZW1lX2ZvbnRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MjE6InRoZW1lX2ZvbnRfbGlua19jb2xvciI7czo3OiJQcmltYXJ5IjtzOjI0OiJ0aGVtZV9mb250X2xpbmtfYmdfY29sb3IiO3M6MDoiIjtzOjI2OiJ0aGVtZV9mb250X2xpbmtfYm94X3NoYWRvdyI7czowOiIiO3M6MzI6InRoZW1lX2ZvbnRfbGlua19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyNjoidGhlbWVfZm9udF9saW5rX3RyYW5zaXRpb24iO3M6MjA6ImFsbCAwLjJzIGVhc2UtaW4tb3V0IjtzOjI3OiJ0aGVtZV9mb250X2xpbmtfaG92ZXJfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzA6InRoZW1lX2ZvbnRfbGlua19ob3Zlcl9iZ19jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX2ZvbnRfbGlua19ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czozODoidGhlbWVfZm9udF9saW5rX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjE1OiJ0aGVtZV9mb250X3NpemUiO3M6NDoiMTRweCI7czoxNzoidGhlbWVfZm9udF9mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6MjA6InRoZW1lX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjIxOiJ0aGVtZV9mb250X3RpdGxlX3NpemUiO3M6MDoiIjtzOjIzOiJ0aGVtZV9mb250X3RpdGxlX3dlaWdodCI7czowOiIiO3M6MjI6InRoZW1lX2ZvbnRfdGl0bGVfY29sb3IiO3M6MDoiIjtzOjI2OiJ0aGVtZV9mb250X3RpdGxlX3NpemVfdHlwZSI7czowOiIiO3M6Mjk6InRoZW1lX2ZvbnRfc3VidGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI0OiJ0aGVtZV9mb250X3N1YnRpdGxlX3NpemUiO3M6MDoiIjtzOjI2OiJ0aGVtZV9mb250X3N1YnRpdGxlX3dlaWdodCI7czo2OiJub3JtYWwiO3M6MjY6InRoZW1lX2ZvbnRfc3VidGl0bGVfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjI1OiJ0aGVtZV9mb250X3N1YnRpdGxlX2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjIwOiJ0aGVtZV9mb250X2gxX2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czoyMDoidGhlbWVfZm9udF9oMl9mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6MjA6InRoZW1lX2ZvbnRfaDNfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjIwOiJ0aGVtZV9mb250X2g0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czoyMDoidGhlbWVfZm9udF9oNV9mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6MjA6InRoZW1lX2ZvbnRfaDZfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjE5OiJ0aGVtZV9mb250X2gxX2NvbG9yIjtzOjExOiJUaXRsZSBDb2xvciI7czoxOToidGhlbWVfZm9udF9oMl9jb2xvciI7czoxMToiVGl0bGUgQ29sb3IiO3M6MTk6InRoZW1lX2ZvbnRfaDNfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjE5OiJ0aGVtZV9mb250X2g0X2NvbG9yIjtzOjExOiJUaXRsZSBDb2xvciI7czoxOToidGhlbWVfZm9udF9oNV9jb2xvciI7czoxMToiVGl0bGUgQ29sb3IiO3M6MTk6InRoZW1lX2ZvbnRfaDZfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjE4OiJ0aGVtZV9mb250X2gxX3NpemUiO3M6MjoiMzYiO3M6MTg6InRoZW1lX2ZvbnRfaDJfc2l6ZSI7czoyOiIyOCI7czoxODoidGhlbWVfZm9udF9oM19zaXplIjtzOjI6IjE5IjtzOjE4OiJ0aGVtZV9mb250X2g0X3NpemUiO3M6MjoiMTciO3M6MTg6InRoZW1lX2ZvbnRfaDVfc2l6ZSI7czoyOiIxNCI7czoxODoidGhlbWVfZm9udF9oNl9zaXplIjtzOjI6IjEyIjtzOjIzOiJ0aGVtZV9mb250X2gxX3NpemVfdHlwZSI7czoyOiJweCI7czoyMzoidGhlbWVfZm9udF9oMl9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjM6InRoZW1lX2ZvbnRfaDNfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjIzOiJ0aGVtZV9mb250X2g0X3NpemVfdHlwZSI7czoyOiJweCI7czoyMzoidGhlbWVfZm9udF9oNV9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjM6InRoZW1lX2ZvbnRfaDZfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjIwOiJ0aGVtZV9mb250X2gxX3dlaWdodCI7czo2OiJub3JtYWwiO3M6MjA6InRoZW1lX2ZvbnRfaDJfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czoyMDoidGhlbWVfZm9udF9oM193ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjIwOiJ0aGVtZV9mb250X2g0X3dlaWdodCI7czo2OiJub3JtYWwiO3M6MjA6InRoZW1lX2ZvbnRfaDVfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czoyMDoidGhlbWVfZm9udF9oNl93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjIwOiJmb290ZXJfZXF1YWxfaGVpZ2h0cyI7czowOiIiO3M6MTc6ImZvb3Rlcl9saW5rX2NvbG9yIjtzOjU6IldoaXRlIjtzOjIwOiJmb290ZXJfbGlua19iZ19jb2xvciI7czowOiIiO3M6MjI6ImZvb3Rlcl9saW5rX2JveF9zaGFkb3ciO3M6MDoiIjtzOjI4OiJmb290ZXJfbGlua19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMzoiZm9vdGVyX2xpbmtfaG92ZXJfY29sb3IiO3M6OToiTmF2IEhvdmVyIjtzOjI2OiJmb290ZXJfbGlua19ob3Zlcl9iZ19jb2xvciI7czowOiIiO3M6Mjg6ImZvb3Rlcl9saW5rX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM0OiJmb290ZXJfbGlua19ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoxODoiZm9vdGVyX2ZvbnRfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czoxNjoiZm9vdGVyX2ZvbnRfc2l6ZSI7czowOiIiO3M6MjE6ImZvb3Rlcl9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoxODoiZm9vdGVyX2ZvbnRfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjE3OiJmb290ZXJfZm9udF9jb2xvciI7czo1OiJXaGl0ZSI7czoyMzoiZm9vdGVyX2ZvbnRfaG92ZXJfY29sb3IiO3M6NToiV2hpdGUiO3M6MTY6ImZvb3Rlcl9jdXN0b21fYmciO3M6MzoieWVzIjtzOjE1OiJmb290ZXJfYmdfY29sb3IiO3M6NjoiTmF2IEJnIjtzOjE1OiJmb290ZXJfYmdfaW1hZ2UiO3M6MDoiIjtzOjE5OiJmb290ZXJfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoxNToiZm9vdGVyX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czoxNzoiZm9vdGVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjIzOiJmb290ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjM6ImZvb3Rlcl9ib3R0b21fY3VzdG9tX2JnIjtzOjM6InllcyI7czoyMjoiZm9vdGVyX2JvdHRvbV9iZ19jb2xvciI7czo2OiJOYXYgQmciO3M6MjI6ImZvb3Rlcl9ib3R0b21fYmdfaW1hZ2UiO3M6MDoiIjtzOjI2OiJmb290ZXJfYm90dG9tX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6MjI6ImZvb3Rlcl9ib3R0b21fYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI0OiJmb290ZXJfYm90dG9tX2JveF9zaGFkb3ciO3M6MDoiIjtzOjMwOiJmb290ZXJfYm90dG9tX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI0OiJmb290ZXJfYm90dG9tX2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6MzA6ImZvb3Rlcl9ib3R0b21fZm9udF9ob3Zlcl9jb2xvciI7czo1OiJXaGl0ZSI7czoyNDoiZm9vdGVyX2JvdHRvbV9saW5rX2NvbG9yIjtzOjU6IldoaXRlIjtzOjMwOiJmb290ZXJfYm90dG9tX2xpbmtfaG92ZXJfY29sb3IiO3M6OToiTmF2IEhvdmVyIjtzOjIzOiJmb290ZXJfYm90dG9tX2ZvbnRfc2l6ZSI7czowOiIiO3M6Mjg6ImZvb3Rlcl9ib3R0b21fZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjU6ImZvb3Rlcl9ib3R0b21fZm9udF9mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6MjU6ImZvb3Rlcl9ib3R0b21fZm9udF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjE3OiJoZWFkZXJfZm9udF9jb2xvciI7czowOiIiO3M6MjM6ImhlYWRlcl9mb250X2hvdmVyX2NvbG9yIjtzOjA6IiI7czoxNzoiaGVhZGVyX2xpbmtfY29sb3IiO3M6MDoiIjtzOjIzOiJoZWFkZXJfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6MTY6ImhlYWRlcl9mb250X3NpemUiO3M6MjoiMTQiO3M6MjE6ImhlYWRlcl9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoxODoiaGVhZGVyX2ZvbnRfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjE4OiJoZWFkZXJfZm9udF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjIwOiJ0aGVtZV9zaWxvX2ZvbnRfc2l6ZSI7czowOiIiO3M6MjU6InRoZW1lX3NpbG9fZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjE6InRoZW1lX3NpbG9fZm9udF9jb2xvciI7czowOiIiO3M6MjI6InRoZW1lX3NpbG9fbGluZV9oZWlnaHQiO3M6MDoiIjtzOjE1OiJsb2dvX2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6MjE6ImxvZ29fZm9udF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czoxNToibG9nb19saW5rX2NvbG9yIjtzOjU6IldoaXRlIjtzOjIxOiJsb2dvX2xpbmtfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6MTY6ImxvZ29fZm9udF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjE0OiJsb2dvX2ZvbnRfc2l6ZSI7czoyOiIxOCI7czoxOToibG9nb19mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoxNjoibG9nb19mb250X2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo3MDAiO3M6MTY6ImxvZ29fcGFkZGluZ190b3AiO3M6NDoiMjdweCI7czoxOToibG9nb19wYWRkaW5nX2JvdHRvbSI7czowOiIiO3M6MTQ6ImxvZ29fY3VzdG9tX2JnIjtzOjI6Im5vIjtzOjEzOiJsb2dvX2JnX2NvbG9yIjtzOjA6IiI7czoxMzoibG9nb19iZ19pbWFnZSI7czowOiIiO3M6MTc6ImxvZ29fcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoxMzoibG9nb19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MTU6ImxvZ29fYm94X3NoYWRvdyI7czowOiIiO3M6MjE6ImxvZ29fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MTY6ImhlYWRlcl9jdXN0b21fYmciO3M6Mjoibm8iO3M6MTU6ImhlYWRlcl9iZ19jb2xvciI7czowOiIiO3M6MTU6ImhlYWRlcl9iZ19pbWFnZSI7czowOiIiO3M6MTU6ImhlYWRlcl9iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MTk6ImhlYWRlcl9wYXJfYmdfcmF0aW8iO3M6MDoiIjtzOjE3OiJoZWFkZXJfYm94X3NoYWRvdyI7czowOiIiO3M6MjM6ImhlYWRlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMToicGFnZV9oZWFkZXJfYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6MjM6InBhZ2VfaGVhZGVyX2ZvbnRfZmFtaWx5IjtzOjE0OiJNb250c2VycmF0OjcwMCI7czoyMzoicGFnZV9oZWFkZXJfZm9udF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjI2OiJwYWdlX2hlYWRlcl9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyMToicGFnZV9oZWFkZXJfZm9udF9zaXplIjtzOjI6IjMwIjtzOjIyOiJwYWdlX2hlYWRlcl9mb250X2NvbG9yIjtzOjExOiJUaXRsZSBDb2xvciI7czoyODoicGFnZV9oZWFkZXJfZm9udF9ob3Zlcl9jb2xvciI7czoxMToiVGl0bGUgQ29sb3IiO3M6MjI6InBhZ2VfaGVhZGVyX2xpbmtfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjI4OiJwYWdlX2hlYWRlcl9saW5rX2hvdmVyX2NvbG9yIjtzOjExOiJUaXRsZSBDb2xvciI7czozMjoicGFnZV9oZWFkZXJfZm9udF9zdWJ0aXRsZV9mYW1pbHkiO3M6MTQ6Ik1vbnRzZXJyYXQ6NDAwIjtzOjMyOiJwYWdlX2hlYWRlcl9mb250X3N1YnRpdGxlX3dlaWdodCI7czo2OiJub3JtYWwiO3M6MzU6InBhZ2VfaGVhZGVyX2ZvbnRfc3VidGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjMwOiJwYWdlX2hlYWRlcl9mb250X3N1YnRpdGxlX3NpemUiO3M6MjoiMjAiO3M6MzE6InBhZ2VfaGVhZGVyX2ZvbnRfc3VidGl0bGVfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjIxOiJwYWdlX2hlYWRlcl9jdXN0b21fYmciO3M6Mjoibm8iO3M6MjA6InBhZ2VfaGVhZGVyX2JnX2NvbG9yIjtzOjA6IiI7czoyMDoicGFnZV9oZWFkZXJfYmdfaW1hZ2UiO3M6MDoiIjtzOjIwOiJwYWdlX2hlYWRlcl9iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MjQ6InBhZ2VfaGVhZGVyX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6MjI6InBhZ2VfaGVhZGVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjI4OiJwYWdlX2hlYWRlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMToiYnJlYWRjcnVtYnNfYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6MjM6ImJyZWFkY3J1bWJzX2ZvbnRfZmFtaWx5IjtzOjE0OiJNb250c2VycmF0OjQwMCI7czoyNjoiYnJlYWRjcnVtYnNfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjE6ImJyZWFkY3J1bWJzX2ZvbnRfc2l6ZSI7czo0OiIxM3B4IjtzOjIyOiJicmVhZGNydW1ic19mb250X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjIzOiJicmVhZGNydW1ic19mb250X3dlaWdodCI7czo2OiJub3JtYWwiO3M6Mjg6ImJyZWFkY3J1bWJzX2ZvbnRfaG92ZXJfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MjI6ImJyZWFkY3J1bWJzX2xpbmtfY29sb3IiO3M6NzoiUHJpbWFyeSI7czoyODoiYnJlYWRjcnVtYnNfbGlua19ob3Zlcl9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czoyMToiYnJlYWRjcnVtYnNfY3VzdG9tX2JnIjtzOjI6Im5vIjtzOjIwOiJicmVhZGNydW1ic19iZ19jb2xvciI7czowOiIiO3M6MjA6ImJyZWFkY3J1bWJzX2JnX2ltYWdlIjtzOjA6IiI7czoyMDoiYnJlYWRjcnVtYnNfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI0OiJicmVhZGNydW1ic19wYXJfYmdfcmF0aW8iO3M6MDoiIjtzOjIyOiJicmVhZGNydW1ic19ib3hfc2hhZG93IjtzOjA6IiI7czoyODoiYnJlYWRjcnVtYnNfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjQ6InRoZW1lX2J1dHRvbl90ZXh0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czoyMjoidGhlbWVfYnV0dG9uX3RleHRfc2l6ZSI7czowOiIiO3M6Mjc6InRoZW1lX2J1dHRvbl90ZXh0X3NpemVfdHlwZSI7czoyOiJweCI7czoyNDoidGhlbWVfYnV0dG9uX3RleHRfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czoyNzoidGhlbWVfYnV0dG9uX3RleHRfdHJhbnNmb3JtIjtzOjk6InVwcGVyY2FzZSI7czoyNToidGhlbWVfYnV0dG9uX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czoyNToidGhlbWVfYnV0dG9uX2JvcmRlcl93aWR0aCI7czozOiIxcHgiO3M6MzY6InRoZW1lX3Bvc3RfZm9ybWF0X2J1dHRvbl9pY29uX3RvZ2dsZSI7czoyOiJvbiI7czozNDoidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX2ljb25fc2l6ZSI7czo1OiJmYS1sZyI7czozNjoidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfZmFtaWx5IjtzOjA6IiI7czozNDoidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfc2l6ZSI7czowOiIiO3M6Mzk6InRoZW1lX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X3NpemVfdHlwZSI7czoyOiJweCI7czozNjoidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czozOToidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfdHJhbnNmb3JtIjtzOjI6Im5vIjtzOjM3OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjM3OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX3dpZHRoIjtzOjM6IjBweCI7czozODoidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjMyOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fcGFkZGluZyI7czozOiI1cHgiO3M6MzE6InRoZW1lX3Bvc3RfZm9ybWF0X2J1dHRvbl9tYXJnaW4iO3M6MTA6IjAgMTBweCAwIDAiO3M6Mjg6InRoZW1lX2xpbmtfYnV0dG9uX3RleHRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzQ6InRoZW1lX2xpbmtfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6NzoiUHJpbWFyeSI7czoxOToidGhlbWVfcHJpbWFyeV9jb2xvciI7czoxMjoiIzI4MjgyODoxLjAwIjtzOjE2OiJwYWdlX3BhZGRpbmdfdG9wIjtzOjA6IiI7czoxOToicGFnZV9wYWRkaW5nX2JvdHRvbSI7czowOiIiO3M6MjY6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfaGVpZ2h0IjtzOjA6IiI7czozMToiZGVmYXVsdF93aWRnZXRfYXJlYV9wYWRkaW5nX3RvcCI7czowOiIiO3M6MzQ6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfcGFkZGluZ19ib3R0b20iO3M6MDoiIjtzOjI5OiJkZWZhdWx0X3dpZGdldF9hcmVhX2N1c3RvbV9iZyI7czoyOiJubyI7czoyODoiZGVmYXVsdF93aWRnZXRfYXJlYV9iZ19jb2xvciI7czowOiIiO3M6Mjg6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfYmdfaW1hZ2UiO3M6MDoiIjtzOjI4OiJkZWZhdWx0X3dpZGdldF9hcmVhX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czozMzoiZGVmYXVsdF93aWRnZXRfYXJlYV9lcXVhbF9oZWlnaHRzIjtzOjg6InBlcl9hcmVhIjtzOjMyOiJkZWZhdWx0X3dpZGdldF9hcmVhX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6MzA6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfYm94X3NoYWRvdyI7czowOiIiO3M6MzY6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjg6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfYWxpZ24iO3M6NDoibGVmdCI7czozNDoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z190aXRsZV9hbGlnbiI7czo0OiJsZWZ0IjtzOjM5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl90b3AiO3M6NDoiMjBweCI7czo0MjoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z190aXRsZV9tYXJnaW5fYm90dG9tIjtzOjQ6IjIwcHgiO3M6Mzg6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfdHJhbnNmb3JtIjtzOjk6InVwcGVyY2FzZSI7czozNToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjMzOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX3RpdGxlX3NpemUiO3M6MjoiMTciO3M6Mzg6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM1OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo3MDAiO3M6MzQ6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjM5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ1OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM2OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2NvbnRlbnRfYWxpZ24iO3M6NDoibGVmdCI7czozMjoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19mb250X3NpemUiO3M6MDoiIjtzOjM3OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czozMzoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19mb250X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjMzOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpbmtfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6Mzk6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlua19ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czozNToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6Mzk6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6MzU6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3dpZHRoIjtzOjA6IiI7czozNToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM1OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozNjoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czozMToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6MzE6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjMzOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI0OiJyaWdodF93aWRnZXRfYXJlYV9oZWlnaHQiO3M6MDoiIjtzOjI5OiJyaWdodF93aWRnZXRfYXJlYV9wYWRkaW5nX3RvcCI7czowOiIiO3M6MzI6InJpZ2h0X3dpZGdldF9hcmVhX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czoyNzoicmlnaHRfd2lkZ2V0X2FyZWFfY3VzdG9tX2JnIjtzOjI6Im5vIjtzOjI2OiJyaWdodF93aWRnZXRfYXJlYV9iZ19jb2xvciI7czowOiIiO3M6MjY6InJpZ2h0X3dpZGdldF9hcmVhX2JnX2ltYWdlIjtzOjA6IiI7czoyNjoicmlnaHRfd2lkZ2V0X2FyZWFfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjMxOiJyaWdodF93aWRnZXRfYXJlYV9lcXVhbF9oZWlnaHRzIjtzOjg6InBlcl9hcmVhIjtzOjMwOiJyaWdodF93aWRnZXRfYXJlYV9wYXJfYmdfcmF0aW8iO3M6MDoiIjtzOjI4OiJyaWdodF93aWRnZXRfYXJlYV9ib3hfc2hhZG93IjtzOjA6IiI7czozNDoicmlnaHRfd2lkZ2V0X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjY6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2FsaWduIjtzOjQ6ImxlZnQiO3M6MzI6InJpZ2h0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2FsaWduIjtzOjQ6ImxlZnQiO3M6Mzc6InJpZ2h0X3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl90b3AiO3M6NDoiMjBweCI7czo0MDoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfbWFyZ2luX2JvdHRvbSI7czo0OiIyMHB4IjtzOjM2OiJyaWdodF93aWRnZXRfYXJlYV93Z190aXRsZV90cmFuc2Zvcm0iO3M6OToidXBwZXJjYXNlIjtzOjMzOiJyaWdodF93aWRnZXRfYXJlYV93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjMxOiJyaWdodF93aWRnZXRfYXJlYV93Z190aXRsZV9zaXplIjtzOjI6IjE3IjtzOjM2OiJyaWdodF93aWRnZXRfYXJlYV93Z190aXRsZV9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo3MDAiO3M6MzI6InJpZ2h0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2NvbG9yIjtzOjExOiJUaXRsZSBDb2xvciI7czozNzoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM0OiJyaWdodF93aWRnZXRfYXJlYV93Z19jb250ZW50X2FsaWduIjtzOjQ6ImxlZnQiO3M6MzA6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2ZvbnRfc2l6ZSI7czowOiIiO3M6MzU6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI3OiJyaWdodF93aWRnZXRfYXJlYV93Z19mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6MzE6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2ZvbnRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzE6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpbmtfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6Mzc6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpbmtfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2N1c3RvbV9zdHlsZSI7czoyOiJubyI7czozNzoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2JvcmRlcl9jb2xvciI7czowOiIiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozNDoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6Mjk6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2JnX2ltYWdlIjtzOjA6IiI7czoyOToicmlnaHRfd2lkZ2V0X2FyZWFfd2dfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjMxOiJyaWdodF93aWRnZXRfYXJlYV93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozNzoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjM6ImxlZnRfd2lkZ2V0X2FyZWFfaGVpZ2h0IjtzOjA6IiI7czoyODoibGVmdF93aWRnZXRfYXJlYV9wYWRkaW5nX3RvcCI7czowOiIiO3M6MzE6ImxlZnRfd2lkZ2V0X2FyZWFfcGFkZGluZ19ib3R0b20iO3M6MDoiIjtzOjI2OiJsZWZ0X3dpZGdldF9hcmVhX2N1c3RvbV9iZyI7czoyOiJubyI7czoyNToibGVmdF93aWRnZXRfYXJlYV9iZ19jb2xvciI7czowOiIiO3M6MjU6ImxlZnRfd2lkZ2V0X2FyZWFfYmdfaW1hZ2UiO3M6MDoiIjtzOjI1OiJsZWZ0X3dpZGdldF9hcmVhX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czozMDoibGVmdF93aWRnZXRfYXJlYV9lcXVhbF9oZWlnaHRzIjtzOjg6InBlcl9hcmVhIjtzOjI5OiJsZWZ0X3dpZGdldF9hcmVhX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6Mjc6ImxlZnRfd2lkZ2V0X2FyZWFfYm94X3NoYWRvdyI7czowOiIiO3M6MzM6ImxlZnRfd2lkZ2V0X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjU6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfYWxpZ24iO3M6NDoibGVmdCI7czozMToibGVmdF93aWRnZXRfYXJlYV93Z190aXRsZV9hbGlnbiI7czo0OiJsZWZ0IjtzOjM2OiJsZWZ0X3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl90b3AiO3M6NDoiMjBweCI7czozOToibGVmdF93aWRnZXRfYXJlYV93Z190aXRsZV9tYXJnaW5fYm90dG9tIjtzOjQ6IjIwcHgiO3M6MzU6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfdHJhbnNmb3JtIjtzOjk6InVwcGVyY2FzZSI7czozMjoibGVmdF93aWRnZXRfYXJlYV93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjMwOiJsZWZ0X3dpZGdldF9hcmVhX3dnX3RpdGxlX3NpemUiO3M6MjoiMTciO3M6MzU6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjMyOiJsZWZ0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo3MDAiO3M6MzE6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfY29sb3IiO3M6MTE6IlRpdGxlIENvbG9yIjtzOjM2OiJsZWZ0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQyOiJsZWZ0X3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMzOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2NvbnRlbnRfYWxpZ24iO3M6NDoibGVmdCI7czoyOToibGVmdF93aWRnZXRfYXJlYV93Z19mb250X3NpemUiO3M6MDoiIjtzOjM0OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI2OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czozMDoibGVmdF93aWRnZXRfYXJlYV93Z19mb250X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjMwOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpbmtfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzY6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlua19ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czozMjoibGVmdF93aWRnZXRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6MzY6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6MzI6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3dpZHRoIjtzOjA6IiI7czozMjoibGVmdF93aWRnZXRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjMyOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozMzoibGVmdF93aWRnZXRfYXJlYV93Z19ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czoyODoibGVmdF93aWRnZXRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6Mjg6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjMwOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM2OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI1OiJhYm92ZV9jb250ZW50X2FyZWFfaGVpZ2h0IjtzOjA6IiI7czozMDoiYWJvdmVfY29udGVudF9hcmVhX3BhZGRpbmdfdG9wIjtzOjA6IiI7czozMzoiYWJvdmVfY29udGVudF9hcmVhX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czoyODoiYWJvdmVfY29udGVudF9hcmVhX2N1c3RvbV9iZyI7czoyOiJubyI7czoyNzoiYWJvdmVfY29udGVudF9hcmVhX2JnX2NvbG9yIjtzOjA6IiI7czoyNzoiYWJvdmVfY29udGVudF9hcmVhX2JnX2ltYWdlIjtzOjA6IiI7czoyNzoiYWJvdmVfY29udGVudF9hcmVhX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czozMjoiYWJvdmVfY29udGVudF9hcmVhX2VxdWFsX2hlaWdodHMiO3M6Mzoib2ZmIjtzOjMxOiJhYm92ZV9jb250ZW50X2FyZWFfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoyOToiYWJvdmVfY29udGVudF9hcmVhX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM1OiJhYm92ZV9jb250ZW50X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjc6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19hbGlnbiI7czowOiIiO3M6MzM6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z190aXRsZV9hbGlnbiI7czowOiIiO3M6Mzg6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z190aXRsZV9tYXJnaW5fdG9wIjtzOjA6IiI7czo0MToiYWJvdmVfY29udGVudF9hcmVhX3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjM3OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfdGl0bGVfdHJhbnNmb3JtIjtzOjI6Im5vIjtzOjM0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfdGl0bGVfd2VpZ2h0IjtzOjA6IiI7czozMjoiYWJvdmVfY29udGVudF9hcmVhX3dnX3RpdGxlX3NpemUiO3M6MDoiIjtzOjM3OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfdGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfdGl0bGVfZmFtaWx5IjtzOjA6IiI7czozMzoiYWJvdmVfY29udGVudF9hcmVhX3dnX3RpdGxlX2NvbG9yIjtzOjA6IiI7czozODoiYWJvdmVfY29udGVudF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfdGl0bGVfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzU6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19jb250ZW50X2FsaWduIjtzOjA6IiI7czozMToiYWJvdmVfY29udGVudF9hcmVhX3dnX2ZvbnRfc2l6ZSI7czowOiIiO3M6MzY6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiYWJvdmVfY29udGVudF9hcmVhX3dnX2ZhbWlseSI7czowOiIiO3M6MzI6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19mb250X2NvbG9yIjtzOjA6IiI7czozMjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpbmtfY29sb3IiO3M6MDoiIjtzOjM4OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzQ6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6Mzg6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czozNDoiYWJvdmVfY29udGVudF9hcmVhX3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzQ6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjM1OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MzA6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6MzA6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MzI6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozODoiYWJvdmVfY29udGVudF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI1OiJiZWxvd19jb250ZW50X2FyZWFfaGVpZ2h0IjtzOjA6IiI7czozMDoiYmVsb3dfY29udGVudF9hcmVhX3BhZGRpbmdfdG9wIjtzOjA6IiI7czozMzoiYmVsb3dfY29udGVudF9hcmVhX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czoyODoiYmVsb3dfY29udGVudF9hcmVhX2N1c3RvbV9iZyI7czoyOiJubyI7czoyNzoiYmVsb3dfY29udGVudF9hcmVhX2JnX2NvbG9yIjtzOjA6IiI7czoyNzoiYmVsb3dfY29udGVudF9hcmVhX2JnX2ltYWdlIjtzOjA6IiI7czoyNzoiYmVsb3dfY29udGVudF9hcmVhX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czozMjoiYmVsb3dfY29udGVudF9hcmVhX2VxdWFsX2hlaWdodHMiO3M6Mzoib2ZmIjtzOjMxOiJiZWxvd19jb250ZW50X2FyZWFfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoyOToiYmVsb3dfY29udGVudF9hcmVhX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM1OiJiZWxvd19jb250ZW50X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjc6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19hbGlnbiI7czowOiIiO3M6MzM6ImJlbG93X2NvbnRlbnRfYXJlYV93Z190aXRsZV9hbGlnbiI7czowOiIiO3M6Mzg6ImJlbG93X2NvbnRlbnRfYXJlYV93Z190aXRsZV9tYXJnaW5fdG9wIjtzOjA6IiI7czo0MToiYmVsb3dfY29udGVudF9hcmVhX3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjM3OiJiZWxvd19jb250ZW50X2FyZWFfd2dfdGl0bGVfdHJhbnNmb3JtIjtzOjI6Im5vIjtzOjM0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfdGl0bGVfd2VpZ2h0IjtzOjA6IiI7czozMjoiYmVsb3dfY29udGVudF9hcmVhX3dnX3RpdGxlX3NpemUiO3M6MDoiIjtzOjM3OiJiZWxvd19jb250ZW50X2FyZWFfd2dfdGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfdGl0bGVfZmFtaWx5IjtzOjA6IiI7czozMzoiYmVsb3dfY29udGVudF9hcmVhX3dnX3RpdGxlX2NvbG9yIjtzOjA6IiI7czozODoiYmVsb3dfY29udGVudF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfdGl0bGVfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzU6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19jb250ZW50X2FsaWduIjtzOjA6IiI7czozMToiYmVsb3dfY29udGVudF9hcmVhX3dnX2ZvbnRfc2l6ZSI7czowOiIiO3M6MzY6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiYmVsb3dfY29udGVudF9hcmVhX3dnX2ZhbWlseSI7czowOiIiO3M6MzI6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19mb250X2NvbG9yIjtzOjA6IiI7czozMjoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpbmtfY29sb3IiO3M6MDoiIjtzOjM4OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzQ6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6Mzg6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czozNDoiYmVsb3dfY29udGVudF9hcmVhX3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzQ6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjM1OiJiZWxvd19jb250ZW50X2FyZWFfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MzA6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6MzA6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MzI6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozODoiYmVsb3dfY29udGVudF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI1OiJmb290ZXJfd2lkZ2V0X2FyZWFfaGVpZ2h0IjtzOjA6IiI7czozMDoiZm9vdGVyX3dpZGdldF9hcmVhX3BhZGRpbmdfdG9wIjtzOjQ6IjMwcHgiO3M6MzM6ImZvb3Rlcl93aWRnZXRfYXJlYV9wYWRkaW5nX2JvdHRvbSI7czo0OiIzMHB4IjtzOjI4OiJmb290ZXJfd2lkZ2V0X2FyZWFfY3VzdG9tX2JnIjtzOjI6Im5vIjtzOjI3OiJmb290ZXJfd2lkZ2V0X2FyZWFfYmdfY29sb3IiO3M6MDoiIjtzOjI3OiJmb290ZXJfd2lkZ2V0X2FyZWFfYmdfaW1hZ2UiO3M6MDoiIjtzOjI3OiJmb290ZXJfd2lkZ2V0X2FyZWFfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjMyOiJmb290ZXJfd2lkZ2V0X2FyZWFfZXF1YWxfaGVpZ2h0cyI7czo4OiJwZXJfYXJlYSI7czozMToiZm9vdGVyX3dpZGdldF9hcmVhX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6Mjk6ImZvb3Rlcl93aWRnZXRfYXJlYV9ib3hfc2hhZG93IjtzOjA6IiI7czozNToiZm9vdGVyX3dpZGdldF9hcmVhX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI3OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfYWxpZ24iO3M6NDoibGVmdCI7czozMzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX2FsaWduIjtzOjQ6ImxlZnQiO3M6Mzg6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z190aXRsZV9tYXJnaW5fdG9wIjtzOjA6IiI7czo0MToiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6NDoiMzBweCI7czozNzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX3RyYW5zZm9ybSI7czo5OiJ1cHBlcmNhc2UiO3M6MzQ6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjMyOiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfc2l6ZSI7czoyOiIxNyI7czozNzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX3NpemVfdHlwZSI7czoyOiJweCI7czozNDoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo3MDAiO3M6MzM6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z190aXRsZV9jb2xvciI7czo1OiJXaGl0ZSI7czozODoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ0OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfdGl0bGVfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzU6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19jb250ZW50X2FsaWduIjtzOjQ6ImxlZnQiO3M6MzE6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19mb250X3NpemUiO3M6MjoiMTMiO3M6MzY6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czozMjoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6MzI6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saW5rX2NvbG9yIjtzOjU6IldoaXRlIjtzOjM4OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlua19ob3Zlcl9jb2xvciI7czo5OiJOYXYgSG92ZXIiO3M6MzQ6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6Mzg6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czozNDoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzQ6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM0OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjM1OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MzA6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6MzA6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MzI6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozODoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMyOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX2hlaWdodCI7czowOiIiO3M6Mzc6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfcGFkZGluZ190b3AiO3M6NDoiMTBweCI7czo0MDoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV9wYWRkaW5nX2JvdHRvbSI7czo0OiIxMHB4IjtzOjM1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX2N1c3RvbV9iZyI7czoyOiJubyI7czozNDoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV9iZ19jb2xvciI7czowOiIiO3M6MzQ6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfYmdfaW1hZ2UiO3M6MDoiIjtzOjM0OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czozOToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV9lcXVhbF9oZWlnaHRzIjtzOjg6InBlcl9hcmVhIjtzOjM4OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6MzY6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfYm94X3NoYWRvdyI7czowOiIiO3M6NDI6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzQ6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfYWxpZ24iO3M6NDoibGVmdCI7czo0MDoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z190aXRsZV9hbGlnbiI7czo0OiJsZWZ0IjtzOjQ1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl90b3AiO3M6MDoiIjtzOjQ4OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjQ0OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX3RyYW5zZm9ybSI7czo5OiJ1cHBlcmNhc2UiO3M6NDE6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfdGl0bGVfd2VpZ2h0IjtzOjA6IiI7czozOToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z190aXRsZV9zaXplIjtzOjI6IjE3IjtzOjQ0OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX3NpemVfdHlwZSI7czoyOiJweCI7czo0MToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z190aXRsZV9mYW1pbHkiO3M6MTQ6Ik1vbnRzZXJyYXQ6NzAwIjtzOjQwOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX2NvbG9yIjtzOjU6IldoaXRlIjtzOjQ1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX3RpdGxlX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQyOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2NvbnRlbnRfYWxpZ24iO3M6MDoiIjtzOjM4OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2ZvbnRfc2l6ZSI7czowOiIiO3M6NDM6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzU6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjM5OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6Mzk6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlua19jb2xvciI7czo1OiJXaGl0ZSI7czo0NToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saW5rX2hvdmVyX2NvbG9yIjtzOjk6Ik5hdiBIb3ZlciI7czo0MToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6NDU6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6NDE6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czo0MjoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czozNzoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19iZ19pbWFnZSI7czowOiIiO3M6Mzc6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjM5OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjIwOiJoZWFkZXJfYXJlYV8xX2hlaWdodCI7czowOiIiO3M6MjU6ImhlYWRlcl9hcmVhXzFfcGFkZGluZ190b3AiO3M6MDoiIjtzOjI4OiJoZWFkZXJfYXJlYV8xX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czoyMzoiaGVhZGVyX2FyZWFfMV9jdXN0b21fYmciO3M6MzoieWVzIjtzOjIyOiJoZWFkZXJfYXJlYV8xX2JnX2NvbG9yIjtzOjU6IjoxLjAwIjtzOjIyOiJoZWFkZXJfYXJlYV8xX2JnX2ltYWdlIjtzOjA6IiI7czoyMjoiaGVhZGVyX2FyZWFfMV9iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6Mjc6ImhlYWRlcl9hcmVhXzFfZXF1YWxfaGVpZ2h0cyI7czo4OiJwZXJfYXJlYSI7czoyNjoiaGVhZGVyX2FyZWFfMV9wYXJfYmdfcmF0aW8iO3M6MDoiIjtzOjI0OiJoZWFkZXJfYXJlYV8xX2JveF9zaGFkb3ciO3M6MDoiIjtzOjMwOiJoZWFkZXJfYXJlYV8xX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjIyOiJoZWFkZXJfYXJlYV8xX3dnX2FsaWduIjtzOjQ6ImxlZnQiO3M6Mjg6ImhlYWRlcl9hcmVhXzFfd2dfdGl0bGVfYWxpZ24iO3M6MDoiIjtzOjMzOiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX21hcmdpbl90b3AiO3M6MDoiIjtzOjM2OiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX3RyYW5zZm9ybSI7czoyOiJubyI7czoyOToiaGVhZGVyX2FyZWFfMV93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX3NpemVfdHlwZSI7czoyOiJweCI7czoyOToiaGVhZGVyX2FyZWFfMV93Z190aXRsZV9mYW1pbHkiO3M6MDoiIjtzOjI4OiJoZWFkZXJfYXJlYV8xX3dnX3RpdGxlX2NvbG9yIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfMV93Z190aXRsZV9ib3hfc2hhZG93IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfMV93Z190aXRsZV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozMDoiaGVhZGVyX2FyZWFfMV93Z19jb250ZW50X2FsaWduIjtzOjA6IiI7czoyNjoiaGVhZGVyX2FyZWFfMV93Z19mb250X3NpemUiO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV8xX3dnX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjIzOiJoZWFkZXJfYXJlYV8xX3dnX2ZhbWlseSI7czowOiIiO3M6Mjc6ImhlYWRlcl9hcmVhXzFfd2dfZm9udF9jb2xvciI7czowOiIiO3M6Mjc6ImhlYWRlcl9hcmVhXzFfd2dfbGlua19jb2xvciI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzFfd2dfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzFfd2dfY3VzdG9tX3N0eWxlIjtzOjI6Im5vIjtzOjMzOiJoZWFkZXJfYXJlYV8xX3dnX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjI5OiJoZWFkZXJfYXJlYV8xX3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzFfd2dfYm9yZGVyX2NvbG9yIjtzOjA6IiI7czoyOToiaGVhZGVyX2FyZWFfMV93Z19ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzA6ImhlYWRlcl9hcmVhXzFfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MjU6ImhlYWRlcl9hcmVhXzFfd2dfYmdfaW1hZ2UiO3M6MDoiIjtzOjI1OiJoZWFkZXJfYXJlYV8xX3dnX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czoyNzoiaGVhZGVyX2FyZWFfMV93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfMV93Z19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMDoiaGVhZGVyX2FyZWFfMl9oZWlnaHQiO3M6MDoiIjtzOjI1OiJoZWFkZXJfYXJlYV8yX3BhZGRpbmdfdG9wIjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfMl9wYWRkaW5nX2JvdHRvbSI7czowOiIiO3M6MjM6ImhlYWRlcl9hcmVhXzJfY3VzdG9tX2JnIjtzOjI6Im5vIjtzOjIyOiJoZWFkZXJfYXJlYV8yX2JnX2NvbG9yIjtzOjA6IiI7czoyMjoiaGVhZGVyX2FyZWFfMl9iZ19pbWFnZSI7czowOiIiO3M6MjI6ImhlYWRlcl9hcmVhXzJfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI3OiJoZWFkZXJfYXJlYV8yX2VxdWFsX2hlaWdodHMiO3M6Mzoib2ZmIjtzOjI2OiJoZWFkZXJfYXJlYV8yX3Bhcl9iZ19yYXRpbyI7czowOiIiO3M6MjQ6ImhlYWRlcl9hcmVhXzJfYm94X3NoYWRvdyI7czowOiIiO3M6MzA6ImhlYWRlcl9hcmVhXzJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjI6ImhlYWRlcl9hcmVhXzJfd2dfYWxpZ24iO3M6NDoibGVmdCI7czoyODoiaGVhZGVyX2FyZWFfMl93Z190aXRsZV9hbGlnbiI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfbWFyZ2luX3RvcCI7czowOiIiO3M6MzY6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfbWFyZ2luX2JvdHRvbSI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfdHJhbnNmb3JtIjtzOjI6Im5vIjtzOjI5OiJoZWFkZXJfYXJlYV8yX3dnX3RpdGxlX3dlaWdodCI7czowOiIiO3M6Mjc6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfc2l6ZSI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI5OiJoZWFkZXJfYXJlYV8yX3dnX3RpdGxlX2ZhbWlseSI7czowOiIiO3M6Mjg6ImhlYWRlcl9hcmVhXzJfd2dfdGl0bGVfY29sb3IiO3M6MDoiIjtzOjMzOiJoZWFkZXJfYXJlYV8yX3dnX3RpdGxlX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM5OiJoZWFkZXJfYXJlYV8yX3dnX3RpdGxlX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMwOiJoZWFkZXJfYXJlYV8yX3dnX2NvbnRlbnRfYWxpZ24iO3M6MDoiIjtzOjI2OiJoZWFkZXJfYXJlYV8yX3dnX2ZvbnRfc2l6ZSI7czowOiIiO3M6MzE6ImhlYWRlcl9hcmVhXzJfd2dfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MjM6ImhlYWRlcl9hcmVhXzJfd2dfZmFtaWx5IjtzOjA6IiI7czoyNzoiaGVhZGVyX2FyZWFfMl93Z19mb250X2NvbG9yIjtzOjA6IiI7czoyNzoiaGVhZGVyX2FyZWFfMl93Z19saW5rX2NvbG9yIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfMl93Z19saW5rX2hvdmVyX2NvbG9yIjtzOjA6IiI7czoyOToiaGVhZGVyX2FyZWFfMl93Z19jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6MzM6ImhlYWRlcl9hcmVhXzJfd2dfYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzJfd2dfYm9yZGVyX3dpZHRoIjtzOjA6IiI7czoyOToiaGVhZGVyX2FyZWFfMl93Z19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjI5OiJoZWFkZXJfYXJlYV8yX3dnX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozMDoiaGVhZGVyX2FyZWFfMl93Z19ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czoyNToiaGVhZGVyX2FyZWFfMl93Z19iZ19pbWFnZSI7czowOiIiO3M6MjU6ImhlYWRlcl9hcmVhXzJfd2dfYmdyZXBlYXQiO3M6MjM6ImNlbnRlciBjZW50ZXIgbm8tcmVwZWF0IjtzOjI3OiJoZWFkZXJfYXJlYV8yX3dnX2JveF9zaGFkb3ciO3M6MDoiIjtzOjMzOiJoZWFkZXJfYXJlYV8yX3dnX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjIwOiJoZWFkZXJfYXJlYV8zX2hlaWdodCI7czowOiIiO3M6MjU6ImhlYWRlcl9hcmVhXzNfcGFkZGluZ190b3AiO3M6MDoiIjtzOjI4OiJoZWFkZXJfYXJlYV8zX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czoyMzoiaGVhZGVyX2FyZWFfM19jdXN0b21fYmciO3M6Mjoibm8iO3M6MjI6ImhlYWRlcl9hcmVhXzNfYmdfY29sb3IiO3M6MDoiIjtzOjIyOiJoZWFkZXJfYXJlYV8zX2JnX2ltYWdlIjtzOjA6IiI7czoyMjoiaGVhZGVyX2FyZWFfM19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6Mjc6ImhlYWRlcl9hcmVhXzNfZXF1YWxfaGVpZ2h0cyI7czozOiJvZmYiO3M6MjY6ImhlYWRlcl9hcmVhXzNfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoyNDoiaGVhZGVyX2FyZWFfM19ib3hfc2hhZG93IjtzOjA6IiI7czozMDoiaGVhZGVyX2FyZWFfM19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMjoiaGVhZGVyX2FyZWFfM193Z19hbGlnbiI7czo0OiJsZWZ0IjtzOjI4OiJoZWFkZXJfYXJlYV8zX3dnX3RpdGxlX2FsaWduIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfM193Z190aXRsZV9tYXJnaW5fdG9wIjtzOjA6IiI7czozNjoiaGVhZGVyX2FyZWFfM193Z190aXRsZV9tYXJnaW5fYm90dG9tIjtzOjA6IiI7czozMjoiaGVhZGVyX2FyZWFfM193Z190aXRsZV90cmFuc2Zvcm0iO3M6Mjoibm8iO3M6Mjk6ImhlYWRlcl9hcmVhXzNfd2dfdGl0bGVfd2VpZ2h0IjtzOjA6IiI7czoyNzoiaGVhZGVyX2FyZWFfM193Z190aXRsZV9zaXplIjtzOjA6IiI7czozMjoiaGVhZGVyX2FyZWFfM193Z190aXRsZV9zaXplX3R5cGUiO3M6MjoicHgiO3M6Mjk6ImhlYWRlcl9hcmVhXzNfd2dfdGl0bGVfZmFtaWx5IjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfM193Z190aXRsZV9jb2xvciI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzNfd2dfdGl0bGVfYm94X3NoYWRvdyI7czowOiIiO3M6Mzk6ImhlYWRlcl9hcmVhXzNfd2dfdGl0bGVfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzA6ImhlYWRlcl9hcmVhXzNfd2dfY29udGVudF9hbGlnbiI7czowOiIiO3M6MjY6ImhlYWRlcl9hcmVhXzNfd2dfZm9udF9zaXplIjtzOjA6IiI7czozMToiaGVhZGVyX2FyZWFfM193Z19mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyMzoiaGVhZGVyX2FyZWFfM193Z19mYW1pbHkiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8zX3dnX2ZvbnRfY29sb3IiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8zX3dnX2xpbmtfY29sb3IiO3M6MDoiIjtzOjMzOiJoZWFkZXJfYXJlYV8zX3dnX2xpbmtfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI5OiJoZWFkZXJfYXJlYV8zX3dnX2N1c3RvbV9zdHlsZSI7czoyOiJubyI7czozMzoiaGVhZGVyX2FyZWFfM193Z19iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czoyOToiaGVhZGVyX2FyZWFfM193Z19ib3JkZXJfd2lkdGgiO3M6MDoiIjtzOjI5OiJoZWFkZXJfYXJlYV8zX3dnX2JvcmRlcl9jb2xvciI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzNfd2dfYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjMwOiJoZWFkZXJfYXJlYV8zX3dnX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjI1OiJoZWFkZXJfYXJlYV8zX3dnX2JnX2ltYWdlIjtzOjA6IiI7czoyNToiaGVhZGVyX2FyZWFfM193Z19iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6Mjc6ImhlYWRlcl9hcmVhXzNfd2dfYm94X3NoYWRvdyI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzNfd2dfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjA6ImhlYWRlcl9hcmVhXzRfaGVpZ2h0IjtzOjA6IiI7czoyNToiaGVhZGVyX2FyZWFfNF9wYWRkaW5nX3RvcCI7czowOiIiO3M6Mjg6ImhlYWRlcl9hcmVhXzRfcGFkZGluZ19ib3R0b20iO3M6MDoiIjtzOjIzOiJoZWFkZXJfYXJlYV80X2N1c3RvbV9iZyI7czoyOiJubyI7czoyMjoiaGVhZGVyX2FyZWFfNF9iZ19jb2xvciI7czowOiIiO3M6MjI6ImhlYWRlcl9hcmVhXzRfYmdfaW1hZ2UiO3M6MDoiIjtzOjIyOiJoZWFkZXJfYXJlYV80X2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czoyNzoiaGVhZGVyX2FyZWFfNF9lcXVhbF9oZWlnaHRzIjtzOjM6Im9mZiI7czoyNjoiaGVhZGVyX2FyZWFfNF9wYXJfYmdfcmF0aW8iO3M6MDoiIjtzOjI0OiJoZWFkZXJfYXJlYV80X2JveF9zaGFkb3ciO3M6MDoiIjtzOjMwOiJoZWFkZXJfYXJlYV80X2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjIyOiJoZWFkZXJfYXJlYV80X3dnX2FsaWduIjtzOjQ6ImxlZnQiO3M6Mjg6ImhlYWRlcl9hcmVhXzRfd2dfdGl0bGVfYWxpZ24iO3M6MDoiIjtzOjMzOiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX21hcmdpbl90b3AiO3M6MDoiIjtzOjM2OiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX21hcmdpbl9ib3R0b20iO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX3RyYW5zZm9ybSI7czoyOiJubyI7czoyOToiaGVhZGVyX2FyZWFfNF93Z190aXRsZV93ZWlnaHQiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX3NpemVfdHlwZSI7czoyOiJweCI7czoyOToiaGVhZGVyX2FyZWFfNF93Z190aXRsZV9mYW1pbHkiO3M6MDoiIjtzOjI4OiJoZWFkZXJfYXJlYV80X3dnX3RpdGxlX2NvbG9yIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfNF93Z190aXRsZV9ib3hfc2hhZG93IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfNF93Z190aXRsZV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozMDoiaGVhZGVyX2FyZWFfNF93Z19jb250ZW50X2FsaWduIjtzOjA6IiI7czoyNjoiaGVhZGVyX2FyZWFfNF93Z19mb250X3NpemUiO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV80X3dnX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjIzOiJoZWFkZXJfYXJlYV80X3dnX2ZhbWlseSI7czowOiIiO3M6Mjc6ImhlYWRlcl9hcmVhXzRfd2dfZm9udF9jb2xvciI7czowOiIiO3M6Mjc6ImhlYWRlcl9hcmVhXzRfd2dfbGlua19jb2xvciI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzRfd2dfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzRfd2dfY3VzdG9tX3N0eWxlIjtzOjI6Im5vIjtzOjMzOiJoZWFkZXJfYXJlYV80X3dnX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjI5OiJoZWFkZXJfYXJlYV80X3dnX2JvcmRlcl93aWR0aCI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzRfd2dfYm9yZGVyX2NvbG9yIjtzOjA6IiI7czoyOToiaGVhZGVyX2FyZWFfNF93Z19ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzA6ImhlYWRlcl9hcmVhXzRfd2dfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6MjU6ImhlYWRlcl9hcmVhXzRfd2dfYmdfaW1hZ2UiO3M6MDoiIjtzOjI1OiJoZWFkZXJfYXJlYV80X3dnX2JncmVwZWF0IjtzOjIzOiJjZW50ZXIgY2VudGVyIG5vLXJlcGVhdCI7czoyNzoiaGVhZGVyX2FyZWFfNF93Z19ib3hfc2hhZG93IjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfNF93Z19ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMToidGhlbWVfdGJzX2dyYXlfZGFya2VyIjtzOjA6IiI7czoxOToidGhlbWVfdGJzX2dyYXlfZGFyayI7czowOiIiO3M6MTQ6InRoZW1lX3Ric19ncmF5IjtzOjA6IiI7czoyMDoidGhlbWVfdGJzX2dyYXlfbGlnaHQiO3M6MDoiIjtzOjIyOiJ0aGVtZV90YnNfZ3JheV9saWdodGVyIjtzOjA6IiI7czoyMzoidGhlbWVfdGJzX2JyYW5kX3N1Y2Nlc3MiO3M6MDoiIjtzOjIwOiJ0aGVtZV90YnNfYnJhbmRfaW5mbyI7czowOiIiO3M6MjM6InRoZW1lX3Ric19icmFuZF93YXJuaW5nIjtzOjA6IiI7czoyMjoidGhlbWVfdGJzX2JyYW5kX2RhbmdlciI7czowOiIiO3M6Mjc6InRoZW1lX3Ric19ncmlkX2d1dHRlcl93aWR0aCI7czowOiIiO3M6MzE6InRoZW1lX3Ric19ncmlkX2Zsb2F0X2JyZWFrcG9pbnQiO3M6MDoiIjtzOjM1OiJ0aGVtZV90YnNfZ3JpZF9mbG9hdF9icmVha3BvaW50X21heCI7czowOiIiO3M6MzE6InRoZW1lX3Ric19wYWRkaW5nX2Jhc2VfdmVydGljYWwiO3M6MzoiOHB4IjtzOjMzOiJ0aGVtZV90YnNfcGFkZGluZ19iYXNlX2hvcml6b250YWwiO3M6NDoiMTZweCI7czozMjoidGhlbWVfdGJzX3BhZGRpbmdfbGFyZ2VfdmVydGljYWwiO3M6MDoiIjtzOjM0OiJ0aGVtZV90YnNfcGFkZGluZ19sYXJnZV9ob3Jpem9udGFsIjtzOjQ6IjIwcHgiO3M6MzI6InRoZW1lX3Ric19wYWRkaW5nX3NtYWxsX3ZlcnRpY2FsIjtzOjA6IiI7czozNDoidGhlbWVfdGJzX3BhZGRpbmdfc21hbGxfaG9yaXpvbnRhbCI7czowOiIiO3M6Mjk6InRoZW1lX3Ric19wYWRkaW5nX3hzX3ZlcnRpY2FsIjtzOjA6IiI7czozMToidGhlbWVfdGJzX3BhZGRpbmdfeHNfaG9yaXpvbnRhbCI7czowOiIiO3M6MjY6InRoZW1lX3Ric19saW5lX2hlaWdodF9iYXNlIjtzOjA6IiI7czozMDoidGhlbWVfdGJzX2xpbmVfaGVpZ2h0X2NvbXB1dGVkIjtzOjA6IiI7czoyNzoidGhlbWVfdGJzX2xpbmVfaGVpZ2h0X2xhcmdlIjtzOjA6IiI7czoyNzoidGhlbWVfdGJzX2xpbmVfaGVpZ2h0X3NtYWxsIjtzOjA6IiI7czozMDoidGhlbWVfdGJzX2hlYWRpbmdzX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czoyODoidGhlbWVfdGJzX2JvcmRlcl9yYWRpdXNfYmFzZSI7czozOiIwcHgiO3M6Mjk6InRoZW1lX3Ric19ib3JkZXJfcmFkaXVzX2xhcmdlIjtzOjM6IjBweCI7czoyOToidGhlbWVfdGJzX2JvcmRlcl9yYWRpdXNfc21hbGwiO3M6MzoiMHB4IjtzOjMyOiJ0aGVtZV90YnNfY29tcG9uZW50X2FjdGl2ZV9jb2xvciI7czo1OiI6MS4wMCI7czoyOToidGhlbWVfdGJzX2NvbXBvbmVudF9hY3RpdmVfYmciO3M6NzoiUHJpbWFyeSI7czoxODoidGhlbWVfdGJzX2lucHV0X2JnIjtzOjA6IiI7czoyNzoidGhlbWVfdGJzX2lucHV0X2JnX2Rpc2FibGVkIjtzOjA6IiI7czoyMToidGhlbWVfdGJzX2lucHV0X2NvbG9yIjtzOjA6IiI7czoyMjoidGhlbWVfdGJzX2lucHV0X2JvcmRlciI7czowOiIiO3M6Mjk6InRoZW1lX3Ric19pbnB1dF9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czoyODoidGhlbWVfdGJzX2lucHV0X2JvcmRlcl9mb2N1cyI7czo3OiJQcmltYXJ5IjtzOjMzOiJ0aGVtZV90YnNfaW5wdXRfY29sb3JfcGxhY2Vob2xkZXIiO3M6MDoiIjtzOjI3OiJ0aGVtZV90YnNfaW5wdXRfaGVpZ2h0X2Jhc2UiO3M6MDoiIjtzOjI4OiJ0aGVtZV90YnNfaW5wdXRfaGVpZ2h0X2xhcmdlIjtzOjA6IiI7czoyODoidGhlbWVfdGJzX2lucHV0X2hlaWdodF9zbWFsbCI7czowOiIiO3M6MjI6InRoZW1lX3Ric19sZWdlbmRfY29sb3IiO3M6MDoiIjtzOjI5OiJ0aGVtZV90YnNfbGVnZW5kX2JvcmRlcl9jb2xvciI7czowOiIiO3M6MzA6InRoZW1lX3Ric19pbnB1dF9ncm91cF9hZGRvbl9iZyI7czowOiIiO3M6NDA6InRoZW1lX3Ric19pbnB1dF9ncm91cF9hZGRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjI5OiJ0aGVtZV9kZWZhdWx0X2J1dHRvbl9iZ19jb2xvciI7czoxMjoiI2ZmNzkxZjoxLjAwIjtzOjM1OiJ0aGVtZV9kZWZhdWx0X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czo1OiJXaGl0ZSI7czozMzoidGhlbWVfZGVmYXVsdF9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjU6IldoaXRlIjtzOjM5OiJ0aGVtZV9kZWZhdWx0X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MTI6IiNmZjc5MWY6MS4wMCI7czozMToidGhlbWVfZGVmYXVsdF9idXR0b25fdGV4dF9jb2xvciI7czo1OiJXaGl0ZSI7czozMjoidGhlbWVfZGVmYXVsdF9idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9kZWZhdWx0X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjEyOiIjZmY3OTFmOjEuMDAiO3M6Mzg6InRoZW1lX2RlZmF1bHRfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozMToidGhlbWVfZGVmYXVsdF9idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6Mzc6InRoZW1lX2RlZmF1bHRfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV9kZWZhdWx0X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfZGVmYXVsdF9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjk6InRoZW1lX3ByaW1hcnlfYnV0dG9uX2JnX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6MzU6InRoZW1lX3ByaW1hcnlfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjU6IldoaXRlIjtzOjMzOiJ0aGVtZV9wcmltYXJ5X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6NzoiUHJpbWFyeSI7czozOToidGhlbWVfcHJpbWFyeV9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6MzE6InRoZW1lX3ByaW1hcnlfYnV0dG9uX3RleHRfY29sb3IiO3M6NToiV2hpdGUiO3M6MzI6InRoZW1lX3ByaW1hcnlfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czozNzoidGhlbWVfcHJpbWFyeV9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo3OiJQcmltYXJ5IjtzOjM4OiJ0aGVtZV9wcmltYXJ5X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6MzE6InRoZW1lX3ByaW1hcnlfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9wcmltYXJ5X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfcHJpbWFyeV9idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX3ByaW1hcnlfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMxOiJ0aGVtZV9zZWNvbmRhcnlfYnV0dG9uX2JnX2NvbG9yIjtzOjEyOiIjMmUzMjUyOjEuMDAiO3M6Mzc6InRoZW1lX3NlY29uZGFyeV9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6NToiV2hpdGUiO3M6MzU6InRoZW1lX3NlY29uZGFyeV9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjEyOiIjMmUzMjUyOjEuMDAiO3M6NDE6InRoZW1lX3NlY29uZGFyeV9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjEyOiIjMmUzMjUyOjEuMDAiO3M6MzM6InRoZW1lX3NlY29uZGFyeV9idXR0b25fdGV4dF9jb2xvciI7czo1OiJXaGl0ZSI7czozNDoidGhlbWVfc2Vjb25kYXJ5X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6Mzk6InRoZW1lX3NlY29uZGFyeV9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czoxMjoiIzJlMzI1MjoxLjAwIjtzOjQwOiJ0aGVtZV9zZWNvbmRhcnlfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozMzoidGhlbWVfc2Vjb25kYXJ5X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czozOToidGhlbWVfc2Vjb25kYXJ5X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfc2Vjb25kYXJ5X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfc2Vjb25kYXJ5X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyOToidGhlbWVfc3VjY2Vzc19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjM1OiJ0aGVtZV9zdWNjZXNzX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzM6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6MzE6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX3RleHRfY29sb3IiO3M6MDoiIjtzOjMyOiJ0aGVtZV9zdWNjZXNzX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6Mzc6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV9zdWNjZXNzX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6MzE6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9zdWNjZXNzX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfc3VjY2Vzc19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX3N1Y2Nlc3NfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI2OiJ0aGVtZV9pbmZvX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX2luZm9fYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozMDoidGhlbWVfaW5mb19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czozNjoidGhlbWVfaW5mb19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czoyODoidGhlbWVfaW5mb19idXR0b25fdGV4dF9jb2xvciI7czowOiIiO3M6Mjk6InRoZW1lX2luZm9fYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czozNDoidGhlbWVfaW5mb19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czowOiIiO3M6MzU6InRoZW1lX2luZm9fYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czoyODoidGhlbWVfaW5mb19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6MzQ6InRoZW1lX2luZm9fYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM0OiJ0aGVtZV9pbmZvX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MDoidGhlbWVfaW5mb19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjk6InRoZW1lX3dhcm5pbmdfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfd2FybmluZ19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMzOiJ0aGVtZV93YXJuaW5nX2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV93YXJuaW5nX2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMxOiJ0aGVtZV93YXJuaW5nX2J1dHRvbl90ZXh0X2NvbG9yIjtzOjA6IiI7czozMjoidGhlbWVfd2FybmluZ19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV93YXJuaW5nX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfd2FybmluZ19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjMxOiJ0aGVtZV93YXJuaW5nX2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czozNzoidGhlbWVfd2FybmluZ19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzc6InRoZW1lX3dhcm5pbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV93YXJuaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyODoidGhlbWVfZGFuZ2VyX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6MzQ6InRoZW1lX2Rhbmdlcl9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMyOiJ0aGVtZV9kYW5nZXJfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6Mzg6InRoZW1lX2Rhbmdlcl9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozMDoidGhlbWVfZGFuZ2VyX2J1dHRvbl90ZXh0X2NvbG9yIjtzOjA6IiI7czozMToidGhlbWVfZGFuZ2VyX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6MzY6InRoZW1lX2Rhbmdlcl9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzc6InRoZW1lX2Rhbmdlcl9idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjMwOiJ0aGVtZV9kYW5nZXJfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM2OiJ0aGVtZV9kYW5nZXJfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM2OiJ0aGVtZV9kYW5nZXJfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQyOiJ0aGVtZV9kYW5nZXJfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMxOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2ljb25fdG9nZ2xlIjtzOjI6Im9uIjtzOjMxOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX3RleHRfZmFtaWx5IjtzOjA6IiI7czoyOToidGhlbWVfZm9sbG93X2J1dHRvbl90ZXh0X3NpemUiO3M6MDoiIjtzOjM0OiJ0aGVtZV9mb2xsb3dfYnV0dG9uX3RleHRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjMxOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX3RleHRfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czozNDoidGhlbWVfZm9sbG93X2J1dHRvbl90ZXh0X3RyYW5zZm9ybSI7czoyOiJubyI7czozMjoidGhlbWVfZm9sbG93X2J1dHRvbl9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzI6InRoZW1lX2ZvbGxvd19idXR0b25fYm9yZGVyX3dpZHRoIjtzOjM6IjBweCI7czozMzoidGhlbWVfZm9sbG93X2J1dHRvbl9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czoyNzoidGhlbWVfZm9sbG93X2J1dHRvbl9wYWRkaW5nIjtzOjA6IiI7czoyNjoidGhlbWVfZm9sbG93X2J1dHRvbl9tYXJnaW4iO3M6MDoiIjtzOjI4OiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMwOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6NToiOjEuMDAiO3M6MzE6InRoZW1lX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjM2OiJ0aGVtZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6NToiOjEuMDAiO3M6Mzc6InRoZW1lX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjMwOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM2OiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM2OiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQyOiJ0aGVtZV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI5OiJ0aGVtZV9mb2xsb3dfYnRuX2N1c3RvbV9zdHlsZSI7czowOiIiO3M6NDM6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjQwOiJ0aGVtZV9nb29nbGVfcGx1c19mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfZ29vZ2xlX3BsdXNfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDQ6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo1MDoidGhlbWVfZ29vZ2xlX3BsdXNfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9nb29nbGVfcGx1c19mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiNkZDRiMzk6MS4wMCI7czo0MzoidGhlbWVfZ29vZ2xlX3BsdXNfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDg6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0OToidGhlbWVfZ29vZ2xlX3BsdXNfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDI6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDg6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDg6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTQ6InRoZW1lX2dvb2dsZV9wbHVzX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM3OiJ0aGVtZV9mYWNlYm9va19mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfZmFjZWJvb2tfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfZmFjZWJvb2tfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV9mYWNlYm9va19mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiMzYjU5OTg6MS4wMCI7czo0MDoidGhlbWVfZmFjZWJvb2tfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDU6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NjoidGhlbWVfZmFjZWJvb2tfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzk6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDU6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDU6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTE6InRoZW1lX2ZhY2Vib29rX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzY6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV90d2l0dGVyX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfdHdpdHRlcl9mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiM1NWFjZWU6MS4wMCI7czozOToidGhlbWVfdHdpdHRlcl9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfdHdpdHRlcl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfdHdpdHRlcl9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV90d2l0dGVyX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX3R3aXR0ZXJfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6Mzc6InRoZW1lX2xpbmtlZGluX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9saW5rZWRpbl9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ3OiJ0aGVtZV9saW5rZWRpbl9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX2xpbmtlZGluX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzA5NzZiNDoxLjAwIjtzOjQwOiJ0aGVtZV9saW5rZWRpbl9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ2OiJ0aGVtZV9saW5rZWRpbl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozOToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MToidGhlbWVfbGlua2VkaW5fZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzQ6InRoZW1lX3NreXBlX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV9za3lwZV9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9za3lwZV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX3NreXBlX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzAwYWZmMDoxLjAwIjtzOjM3OiJ0aGVtZV9za3lwZV9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0MjoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQzOiJ0aGVtZV9za3lwZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNjoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MjoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0ODoidGhlbWVfc2t5cGVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM1OiJ0aGVtZV9naXRodWJfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2dpdGh1Yl9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzMzMzMzMzoxLjAwIjtzOjM4OiJ0aGVtZV9naXRodWJfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX2dpdGh1Yl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDQ6InRoZW1lX2dpdGh1Yl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNzoidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX2dpdGh1Yl9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9naXRodWJfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfZ2l0aHViX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozODoidGhlbWVfcGludGVyZXN0X2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9waW50ZXJlc3RfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDg6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiNjYzIxMjc6MS4wMCI7czo0MToidGhlbWVfcGludGVyZXN0X2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9waW50ZXJlc3RfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ3OiJ0aGVtZV9waW50ZXJlc3RfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9waW50ZXJlc3RfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfcGludGVyZXN0X2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTI6InRoZW1lX3BpbnRlcmVzdF9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6Mzg6InRoZW1lX2luc3RhZ3JhbV9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0NDoidGhlbWVfaW5zdGFncmFtX2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjM2Y3MjliOjEuMDAiO3M6NDE6InRoZW1lX2luc3RhZ3JhbV9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NjoidGhlbWVfaW5zdGFncmFtX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NzoidGhlbWVfaW5zdGFncmFtX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQwOiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NjoidGhlbWVfaW5zdGFncmFtX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX2luc3RhZ3JhbV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUyOiJ0aGVtZV9pbnN0YWdyYW1fZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6Mzc6InRoZW1lX2RyaWJiYmxlX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9kcmliYmJsZV9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ3OiJ0aGVtZV9kcmliYmJsZV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX2RyaWJiYmxlX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiI2VhNGM4OToxLjAwIjtzOjQwOiJ0aGVtZV9kcmliYmJsZV9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ2OiJ0aGVtZV9kcmliYmJsZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozOToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MToidGhlbWVfZHJpYmJibGVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozOToidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDk6InRoZW1lX2ZvdXJzcXVhcmVfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV9mb3Vyc3F1YXJlX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzAwNzJiMToxLjAwIjtzOjQyOiJ0aGVtZV9mb3Vyc3F1YXJlX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9mb3Vyc3F1YXJlX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0ODoidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0MToidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9mb3Vyc3F1YXJlX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDc6InRoZW1lX2ZvdXJzcXVhcmVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MzoidGhlbWVfZm91cnNxdWFyZV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzU6InRoZW1lX3RyZWxsb19mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfdHJlbGxvX2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjMjU2YTkyOjEuMDAiO3M6Mzg6InRoZW1lX3RyZWxsb19mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfdHJlbGxvX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NDoidGhlbWVfdHJlbGxvX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfdHJlbGxvX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDM6InRoZW1lX3RyZWxsb19mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ5OiJ0aGVtZV90cmVsbG9fZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM1OiJ0aGVtZV9mbGlja3JfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2ZsaWNrcl9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiI2ZmMDA4NDoxLjAwIjtzOjM4OiJ0aGVtZV9mbGlja3JfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX2ZsaWNrcl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDQ6InRoZW1lX2ZsaWNrcl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNzoidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX2ZsaWNrcl9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9mbGlja3JfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfZmxpY2tyX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzY6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV95b3V0dWJlX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfeW91dHViZV9mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiNlNTJkMjc6MS4wMCI7czozOToidGhlbWVfeW91dHViZV9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfeW91dHViZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfeW91dHViZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV95b3V0dWJlX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX3lvdXR1YmVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfcnNzX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjMyOiJ0aGVtZV9yc3NfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6Mzg6InRoZW1lX3Jzc19mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNjoidGhlbWVfcnNzX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfcnNzX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfcnNzX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiI2YyNjUyMjoxLjAwIjtzOjM1OiJ0aGVtZV9yc3NfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX3Jzc19mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDE6InRoZW1lX3Jzc19mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNDoidGhlbWVfcnNzX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX3Jzc19mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV9yc3NfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NjoidGhlbWVfcnNzX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2ljb25fdG9nZ2xlIjtzOjI6Im9uIjtzOjMwOiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl9pY29uX3NpemUiO3M6NToiZmEtbGciO3M6MzI6InRoZW1lX3NoYXJpbmdfYnV0dG9uX3RleHRfZmFtaWx5IjtzOjA6IiI7czozMDoidGhlbWVfc2hhcmluZ19idXR0b25fdGV4dF9zaXplIjtzOjA6IiI7czozNToidGhlbWVfc2hhcmluZ19idXR0b25fdGV4dF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzI6InRoZW1lX3NoYXJpbmdfYnV0dG9uX3RleHRfd2VpZ2h0IjtzOjY6Im5vcm1hbCI7czozNToidGhlbWVfc2hhcmluZ19idXR0b25fdGV4dF90cmFuc2Zvcm0iO3M6Mjoibm8iO3M6MzM6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozMzoidGhlbWVfc2hhcmluZ19idXR0b25fYm9yZGVyX3dpZHRoIjtzOjM6IjBweCI7czozNDoidGhlbWVfc2hhcmluZ19idXR0b25fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6Mjg6InRoZW1lX3NoYXJpbmdfYnV0dG9uX3BhZGRpbmciO3M6MzoiOHB4IjtzOjI3OiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl9tYXJnaW4iO3M6MTY6IjEwcHggMTBweCAxMHB4IDAiO3M6Mjk6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfc2hhcmluZ19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMzOiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6NToiOjEuMDAiO3M6Mzk6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czo1OiI6MS4wMCI7czozMToidGhlbWVfc2hhcmluZ19idXR0b25fdGV4dF9jb2xvciI7czo1OiI6MS4wMCI7czozMjoidGhlbWVfc2hhcmluZ19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjU6IjoxLjAwIjtzOjM4OiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6MzE6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX3NoYXJpbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjMwOiJ0aGVtZV9zaGFyaW5nX2J0bl9jdXN0b21fc3R5bGUiO3M6Mjoibm8iO3M6NDQ6InRoZW1lX2dvb2dsZV9wbHVzX3NoYXJpbmdfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czo0MToidGhlbWVfZ29vZ2xlX3BsdXNfc2hhcmluZ19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ3OiJ0aGVtZV9nb29nbGVfcGx1c19zaGFyaW5nX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDU6InRoZW1lX2dvb2dsZV9wbHVzX3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9jb2xvciI7czo1OiI6MS4wMCI7czo1MToidGhlbWVfZ29vZ2xlX3BsdXNfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjU6IjoxLjAwIjtzOjQzOiJ0aGVtZV9nb29nbGVfcGx1c19zaGFyaW5nX2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjZGQ0YjM5OjEuMDAiO3M6NDQ6InRoZW1lX2dvb2dsZV9wbHVzX3NoYXJpbmdfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfZ29vZ2xlX3BsdXNfc2hhcmluZ19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo1MDoidGhlbWVfZ29vZ2xlX3BsdXNfc2hhcmluZ19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9nb29nbGVfcGx1c19zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfZ29vZ2xlX3BsdXNfc2hhcmluZ19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6InRoZW1lX2dvb2dsZV9wbHVzX3NoYXJpbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU1OiJ0aGVtZV9nb29nbGVfcGx1c19zaGFyaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM4OiJ0aGVtZV9mYWNlYm9va19zaGFyaW5nX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDQ6InRoZW1lX2ZhY2Vib29rX3NoYXJpbmdfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0ODoidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzNiNTk5ODoxLjAwIjtzOjQxOiJ0aGVtZV9mYWNlYm9va19zaGFyaW5nX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDY6InRoZW1lX2ZhY2Vib29rX3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDc6InRoZW1lX2ZhY2Vib29rX3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0MDoidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDY6InRoZW1lX2ZhY2Vib29rX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV9mYWNlYm9va19zaGFyaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MjoidGhlbWVfZmFjZWJvb2tfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM3OiJ0aGVtZV90d2l0dGVyX3NoYXJpbmdfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfdHdpdHRlcl9zaGFyaW5nX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfdHdpdHRlcl9zaGFyaW5nX2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV90d2l0dGVyX3NoYXJpbmdfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiM1NWFjZWU6MS4wMCI7czo0MDoidGhlbWVfdHdpdHRlcl9zaGFyaW5nX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDU6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NjoidGhlbWVfdHdpdHRlcl9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzk6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDU6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDU6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTE6InRoZW1lX3R3aXR0ZXJfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozODoidGhlbWVfbGlua2VkaW5fc2hhcmluZ19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9saW5rZWRpbl9zaGFyaW5nX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDg6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiMwOTc2YjQ6MS4wMCI7czo0MToidGhlbWVfbGlua2VkaW5fc2hhcmluZ19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9saW5rZWRpbl9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ3OiJ0aGVtZV9saW5rZWRpbl9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9saW5rZWRpbl9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfbGlua2VkaW5fc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTI6InRoZW1lX2xpbmtlZGluX3NoYXJpbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM5OiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0OToidGhlbWVfcGludGVyZXN0X3NoYXJpbmdfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3BpbnRlcmVzdF9zaGFyaW5nX2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjY2MyMTI3OjEuMDAiO3M6NDI6InRoZW1lX3BpbnRlcmVzdF9zaGFyaW5nX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX3BpbnRlcmVzdF9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ4OiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQxOiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX3BpbnRlcmVzdF9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfcGludGVyZXN0X3NoYXJpbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUzOiJ0aGVtZV9waW50ZXJlc3Rfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzY6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV90dW1ibHJfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfdHVtYmxyX3NoYXJpbmdfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiMzNTQ2NWM6MS4wMCI7czozOToidGhlbWVfdHVtYmxyX3NoYXJpbmdfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfdHVtYmxyX3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfdHVtYmxyX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV90dW1ibHJfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX3R1bWJscl9zaGFyaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfdmtfc2hhcmluZ19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjMyOiJ0aGVtZV92a19zaGFyaW5nX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6Mzg6InRoZW1lX3ZrX3NoYXJpbmdfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNjoidGhlbWVfdmtfc2hhcmluZ19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfdmtfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfdmtfc2hhcmluZ19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzQ1NjY4ZToxLjAwIjtzOjM1OiJ0aGVtZV92a19zaGFyaW5nX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX3ZrX3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDE6InRoZW1lX3ZrX3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNDoidGhlbWVfdmtfc2hhcmluZ19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX3ZrX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV92a19zaGFyaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NjoidGhlbWVfdmtfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzY6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV9yZWRkaXRfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfcmVkZGl0X3NoYXJpbmdfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiNmZjQ1MDA6MS4wMCI7czozOToidGhlbWVfcmVkZGl0X3NoYXJpbmdfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfcmVkZGl0X3NoYXJpbmdfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfcmVkZGl0X3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9yZWRkaXRfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX3JlZGRpdF9zaGFyaW5nX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM4OiJ0aGVtZV9lbnZlbG9wZV9zaGFyaW5nX2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDQ6InRoZW1lX2VudmVsb3BlX3NoYXJpbmdfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0ODoidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czo0MToidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9lbnZlbG9wZV9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ3OiJ0aGVtZV9lbnZlbG9wZV9zaGFyaW5nX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX2VudmVsb3BlX3NoYXJpbmdfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9lbnZlbG9wZV9zaGFyaW5nX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfZW52ZWxvcGVfc2hhcmluZ19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTI6InRoZW1lX2VudmVsb3BlX3NoYXJpbmdfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9zdGFuZGFyZF9wb3N0X2Zvcm1hdF9idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjQyOiJ0aGVtZV9zdGFuZGFyZF9wb3N0X2Zvcm1hdF9idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ4OiJ0aGVtZV9zdGFuZGFyZF9wb3N0X2Zvcm1hdF9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV9zdGFuZGFyZF9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6NTI6InRoZW1lX3N0YW5kYXJkX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6NzoiUHJpbWFyeSI7czo0NDoidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfY29sb3IiO3M6NzoiUHJpbWFyeSI7czo0NToidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo1MDoidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6NzoiUHJpbWFyeSI7czo1MToidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUwOiJ0aGVtZV9zdGFuZGFyZF9wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NTA6InRoZW1lX3N0YW5kYXJkX3Bvc3RfZm9ybWF0X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1NjoidGhlbWVfc3RhbmRhcmRfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM5OiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0OToidGhlbWVfaW1hZ2VfcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2ltYWdlX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfaW1hZ2VfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NzoidGhlbWVfaW1hZ2VfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQxOiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX2ltYWdlX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfaW1hZ2VfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUzOiJ0aGVtZV9pbWFnZV9wb3N0X2Zvcm1hdF9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6Mzk6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDU6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDM6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ5OiJ0aGVtZV9hdWRpb19wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfYXVkaW9fcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9hdWRpb19wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9hdWRpb19wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDg6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NzoidGhlbWVfYXVkaW9fcG9zdF9mb3JtYXRfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ3OiJ0aGVtZV9hdWRpb19wb3N0X2Zvcm1hdF9idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTM6InRoZW1lX2F1ZGlvX3Bvc3RfZm9ybWF0X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozOToidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDk6InRoZW1lX3ZpZGVvX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV92aWRlb19wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3ZpZGVvX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX3ZpZGVvX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0ODoidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0MToidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV92aWRlb19wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDc6InRoZW1lX3ZpZGVvX3Bvc3RfZm9ybWF0X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MzoidGhlbWVfdmlkZW9fcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM5OiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0OToidGhlbWVfcXVvdGVfcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3F1b3RlX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfcXVvdGVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NzoidGhlbWVfcXVvdGVfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQxOiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX3F1b3RlX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfcXVvdGVfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUzOiJ0aGVtZV9xdW90ZV9wb3N0X2Zvcm1hdF9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozODoidGhlbWVfbGlua19wb3N0X2Zvcm1hdF9idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9saW5rX3Bvc3RfZm9ybWF0X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDg6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDA6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV9saW5rX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDY6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ3OiJ0aGVtZV9saW5rX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ2OiJ0aGVtZV9saW5rX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfbGlua19wb3N0X2Zvcm1hdF9idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTI6InRoZW1lX2xpbmtfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6NDE6InRoZW1lX2dhbGxlcnlfcG9zdF9mb3JtYXRfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0NzoidGhlbWVfZ2FsbGVyeV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjUxOiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjA6IiI7czo0NDoidGhlbWVfZ2FsbGVyeV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ5OiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjA6IiI7czo1MDoidGhlbWVfZ2FsbGVyeV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfZ2FsbGVyeV9wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6InRoZW1lX2dhbGxlcnlfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU1OiJ0aGVtZV9nYWxsZXJ5X3Bvc3RfZm9ybWF0X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25faWNvbl90b2dnbGUiO3M6Mjoib24iO3M6MzE6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2ljb24iO3M6MTE6ImZhLWFuZ2xlLXVwIjtzOjM2OiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl9pY29uX3NpemUiO3M6NToiZmEtMngiO3M6Mzg6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozODoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fdGV4dF9mYW1pbHkiO3M6MDoiIjtzOjM2OiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl90ZXh0X3NpemUiO3M6MDoiIjtzOjQxOiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl90ZXh0X3NpemVfdHlwZSI7czoyOiJweCI7czozODoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fdGV4dF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjQxOiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl90ZXh0X3RyYW5zZm9ybSI7czoyOiJubyI7czozOToidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjM5OiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl9ib3JkZXJfd2lkdGgiO3M6MDoiIjtzOjQwOiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czozNDoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fcGFkZGluZyI7czozOiI3cHgiO3M6MzI6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX3JpZ2h0IjtzOjQ6IjMwcHgiO3M6MzM6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2JvdHRvbSI7czo0OiI1MHB4IjtzOjM1OiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl9iZ19jb2xvciI7czoxMToiMTIxMjEyOjAuODUiO3M6NDE6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjEyOiIjMTIxMjEyOjAuOTUiO3M6Mzk6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2JvcmRlcl9jb2xvciI7czoxMToiVHJhbnNwYXJlbnQiO3M6NDU6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czoxMToiVHJhbnNwYXJlbnQiO3M6Mzc6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX3RleHRfY29sb3IiO3M6MTI6IiNhYWFhYWE6MS4wMCI7czozODoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6NDQ6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozNzoidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDM6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9zY3JvbGxfdG9fdG9wX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToidGhlbWVfc2Nyb2xsX3RvX3RvcF9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjM6InBhZ2VfaGVhZGVyX3BhZGRpbmdfdG9wIjtzOjA6IiI7czoyNjoicGFnZV9oZWFkZXJfcGFkZGluZ19ib3R0b20iO3M6MDoiIjtzOjIzOiJicmVhZGNydW1ic19wYWRkaW5nX3RvcCI7czowOiIiO3M6MjY6ImJyZWFkY3J1bWJzX3BhZGRpbmdfYm90dG9tIjtzOjA6IiI7czozMToicGFnZV9oZWFkZXJfZm9udF90ZXh0X3RyYW5zZm9ybSI7czo5OiJ1cHBlcmNhc2UiO3M6NDA6InBhZ2VfaGVhZGVyX2ZvbnRfc3VidGl0bGVfdGV4dF90cmFuc2Zvcm0iO3M6OToidXBwZXJjYXNlIjtzOjMxOiJ0aGVtZV90YnNfbmF2X3RhYnNfYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfdGJzX25hdl90YWJzX2xpbmtfaG92ZXJfYm9yZGVyX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfdGJzX25hdl90YWJzX2FjdGl2ZV9saW5rX2hvdmVyX2JnIjtzOjA6IiI7czo0MjoidGhlbWVfdGJzX25hdl90YWJzX2FjdGl2ZV9saW5rX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0OToidGhlbWVfdGJzX25hdl90YWJzX2FjdGl2ZV9saW5rX2hvdmVyX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX3Ric19uYXZfdGFic19qdXN0aWZpZWRfbGlua19ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjUzOiJ0aGVtZV90YnNfbmF2X3RhYnNfanVzdGlmaWVkX2FjdGl2ZV9saW5rX2JvcmRlcl9jb2xvciI7czowOiIiO3M6MzM6InRoZW1lX3Ric19uYXZfcGlsbHNfYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDA6InRoZW1lX3Ric19uYXZfcGlsbHNfYWN0aXZlX2xpbmtfaG92ZXJfYmciO3M6MDoiIjtzOjQzOiJ0aGVtZV90YnNfbmF2X3BpbGxzX2FjdGl2ZV9saW5rX2hvdmVyX2NvbG9yIjtzOjA6IiI7czoyODoidGhlbWVfZm9udF9oMV9sZXR0ZXJfc3BhY2luZyI7czowOiIiO3M6Mjg6InRoZW1lX2ZvbnRfaDJfbGV0dGVyX3NwYWNpbmciO3M6MDoiIjtzOjI4OiJ0aGVtZV9mb250X2gzX2xldHRlcl9zcGFjaW5nIjtzOjA6IiI7czoyODoidGhlbWVfZm9udF9oNF9sZXR0ZXJfc3BhY2luZyI7czowOiIiO3M6Mjg6InRoZW1lX2ZvbnRfaDVfbGV0dGVyX3NwYWNpbmciO3M6MDoiIjtzOjI4OiJ0aGVtZV9mb250X2g2X2xldHRlcl9zcGFjaW5nIjtzOjA6IiI7czoyMzoidGhlbWVfYnV0dG9uX3RyYW5zaXRpb24iO3M6Mjc6ImJhY2tncm91bmQgMC40cywgY29sb3IgMC4ycyI7czoyNzoibG9nb190YWdsaW5lX2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI0OiJsb2dvX3RhZ2xpbmVfZm9udF9mYW1pbHkiO3M6MTQ6Ik1vbnRzZXJyYXQ6NDAwIjtzOjMwOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fd2lkdGgiO3M6NDoiNDBweCI7czozMToidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX2hlaWdodCI7czo0OiI0MHB4IjtzOjM3OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fY3VzdG9tX3N0eWxlIjtzOjI6Im5vIjtzOjMzOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQzOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6MzU6InRoZW1lX3Bvc3RfZm9ybWF0X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjM2OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQxOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo3OiJQcmltYXJ5IjtzOjQyOiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM1OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3Bvc3RfZm9ybWF0X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfcG9zdF9mb3JtYXRfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9wb3N0X2Zvcm1hdF9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX2ZvbGxvd19idXR0b25fY3VzdG9tX3N0eWxlIjtzOjM6InllcyI7czozMzoidGhlbWVfc2hhcmluZ19idXR0b25fY3VzdG9tX3N0eWxlIjtzOjM6InllcyI7czoyNjoidGhlbWVfc2hhcmluZ19idXR0b25fd2lkdGgiO3M6NDoiMzBweCI7czoyNzoidGhlbWVfc2hhcmluZ19idXR0b25faGVpZ2h0IjtzOjQ6IjMwcHgiO3M6MzI6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX3dpZHRoIjtzOjQ6IjQwcHgiO3M6MzM6InRoZW1lX3Njcm9sbF90b190b3BfYnV0dG9uX2hlaWdodCI7czo0OiI0MHB4IjtzOjI3OiJ0aGVtZV9idXR0b25fbGV0dGVyX3NwYWNpbmciO3M6MzoiMnB4IjtzOjMxOiJ0aGVtZV9mb250X2xpbmtfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6Mzc6InRoZW1lX2ZvbnRfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czoyODoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9zdHlsZSI7czo2OiJzcXVhcmUiO3M6MjE6InRoZW1lX2ZvbnRfbGlzdF9hbGlnbiI7czo0OiJsZWZ0IjtzOjIzOiJ0aGVtZV9mb250X2xpc3RfcGFkZGluZyI7czoxMDoiMCAwIDAgMTVweCI7czoyMjoidGhlbWVfZm9udF9saXN0X21hcmdpbiI7czoxMDoiMCAwIDE1cHggMCI7czoyNToidGhlbWVfZm9udF9saXN0X2ZvbnRfc2l6ZSI7czowOiIiO3M6MzA6InRoZW1lX2ZvbnRfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyMjoidGhlbWVfZm9udF9saXN0X2ZhbWlseSI7czoxMToiUmFsZXdheTo0MDAiO3M6MjY6InRoZW1lX2ZvbnRfbGlzdF9mb250X2NvbG9yIjtzOjA6IiI7czoyNjoidGhlbWVfZm9udF9saXN0X2xpbmtfY29sb3IiO3M6MDoiIjtzOjMyOiJ0aGVtZV9mb250X2xpc3RfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX2ZvbnRfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjQyOiJ0aGVtZV9mb250X2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czoyMToidGhlbWVfZm9udF9saXN0X3N0eWxlIjtzOjQ6Im5vbmUiO3M6MjA6InRoZW1lX2ZvbnRfbGlzdF9pY29uIjtzOjQ6ImYxMDUiO3M6MjU6InRoZW1lX2ZvbnRfbGlzdF9pY29uX3NpemUiO3M6MDoiIjtzOjMwOiJ0aGVtZV9mb250X2xpc3RfaWNvbl9zaXplX3R5cGUiO3M6MjoicHgiO3M6Mjc6InRoZW1lX2ZvbnRfbGlzdF9pY29uX21hcmdpbiI7czoxMToiMCAwIDAgLTEycHgiO3M6Mjg6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX3BhZGRpbmciO3M6NjoiMCAxMnB4IjtzOjI3OiJ0aGVtZV9mb250X2xpc3RfaXRlbV9tYXJnaW4iO3M6NToiNXB4IDAiO3M6MzI6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjQ6IjMwcHgiO3M6MzM6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2JvcmRlcl93aWR0aCI7czowOiIiO3M6MzM6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czozNDoidGhlbWVfZm9udF9saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6Mzc6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjMzOiJ0aGVtZV9mb250X2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjMxOiJ0aGVtZV9mb250X2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjc6IjAgMXB4IDAiO3M6Mzc6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2JveF9zaGFkb3dfY29sb3IiO3M6MTI6IiNlZmVmZWY6MS4wMCI7czo0MzoidGhlbWVfZm9udF9saXN0X2l0ZW1fYmFja2dyb3VuZF9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzc6InRoZW1lX2ZvbnRfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9mb250X2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozMjoicGFnZV9oZWFkZXJfbGlua190ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozODoicGFnZV9oZWFkZXJfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czo0MjoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM0OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfZmFtaWx5IjtzOjEzOiJPcGVuIFNhbnM6NDAwIjtzOjQ4OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua190ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czo1NDoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NDI6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pY29uX3NpemVfdHlwZSI7czoyOiJweCI7czo0NToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjQwOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjMyOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czo0NjoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjUyOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NDA6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplX3R5cGUiO3M6MjoicHgiO3M6NDM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6Mzk6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czozMToibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czo0NToibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NTE6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX2hvdmVyX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjM5OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplX3R5cGUiO3M6MjoicHgiO3M6NDI6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czo0MToiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzM6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDc6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2xpbmtfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NTM6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NDE6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjQ0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czo0MToiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzM6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDc6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2xpbmtfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NTM6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NDE6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjQ0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9zdHlsZSI7czo1OiJzb2xpZCI7czo0MToiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6MzM6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czo0NzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua190ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czo1MzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czo0MToiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplX3R5cGUiO3M6MjoicHgiO3M6NDQ6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjQ4OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9zaXplX3R5cGUiO3M6MjoicHgiO3M6NDA6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9mYW1pbHkiO3M6MTM6Ik9wZW4gU2Fuczo0MDAiO3M6NTQ6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjYwOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czo0ODoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjUxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzY6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDI6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjQ4OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozNjoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM5OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzY6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDI6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjQ4OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozNjoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM5OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzY6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiaGVhZGVyX2FyZWFfM193Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDI6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjQ4OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozNjoiaGVhZGVyX2FyZWFfM193Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM5OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6MzY6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyODoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2ZhbWlseSI7czowOiIiO3M6NDI6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9saW5rX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjQ4OiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozNjoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2ljb25fc2l6ZV90eXBlIjtzOjI6InB4IjtzOjM5OiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6NDA6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6MzoieWVzIjtzOjMzOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfYWxpZ24iO3M6NDoibGVmdCI7czozNToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X3BhZGRpbmciO3M6MTA6IjAgMCAwIDE1cHgiO3M6MzQ6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9tYXJnaW4iO3M6MzoiMHB4IjtzOjM3OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9zaXplIjtzOjA6IiI7czozODoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6Mzg6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQ0OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czozMzoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X3N0eWxlIjtzOjY6InNxdWFyZSI7czozMjoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2ljb24iO3M6NDoiZjEwNSI7czozNzoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2ljb25fc2l6ZSI7czowOiIiO3M6Mzk6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pY29uX21hcmdpbiI7czoxMToiMCAwIDAgLTEycHgiO3M6NDA6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX3BhZGRpbmciO3M6MDoiIjtzOjM5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9tYXJnaW4iO3M6NToiNXB4IDAiO3M6NDQ6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjQ6IjMwcHgiO3M6NDU6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl93aWR0aCI7czowOiIiO3M6NDY6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjQ5OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czo0NToiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX2NvbG9yIjtzOjU6IjoxLjAwIjtzOjQzOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjc6IjAgMXB4IDAiO3M6NDk6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JveF9zaGFkb3dfY29sb3IiO3M6MTA6IkxpZ2h0IEdyZXkiO3M6NTU6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjUxOiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6NToiOjEuMDAiO3M6NDk6ImRlZmF1bHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU1OiJkZWZhdWx0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6MzoieWVzIjtzOjMxOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2FsaWduIjtzOjQ6ImxlZnQiO3M6MzM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfcGFkZGluZyI7czoxMDoiMCAwIDAgMTVweCI7czozMjoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9tYXJnaW4iO3M6MDoiIjtzOjM1OiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfc2l6ZSI7czowOiIiO3M6MzY6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozNjoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQyOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6MzE6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3Rfc3R5bGUiO3M6Njoic3F1YXJlIjtzOjMwOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2ljb24iO3M6MDoiIjtzOjM1OiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2ljb25fc2l6ZSI7czowOiIiO3M6Mzc6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9tYXJnaW4iO3M6MDoiIjtzOjM4OiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6Mzc6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9tYXJnaW4iO3M6MDoiIjtzOjQyOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fbGluZV9oZWlnaHQiO3M6MDoiIjtzOjQzOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0NDoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjQ3OiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6NDM6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQxOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm94X3NoYWRvdyI7czowOiIiO3M6NDc6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo1MzoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ5OiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0NzoicmlnaHRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUzOiJyaWdodF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzc6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6MzoieWVzIjtzOjMwOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfYWxpZ24iO3M6NDoibGVmdCI7czozMjoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X3BhZGRpbmciO3M6MTA6IjAgMCAwIDE1cHgiO3M6MzE6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9tYXJnaW4iO3M6MDoiIjtzOjM0OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9zaXplIjtzOjA6IiI7czozNToibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzU6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQxOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czozMDoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X3N0eWxlIjtzOjY6InNxdWFyZSI7czoyOToibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2ljb24iO3M6MDoiIjtzOjM0OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozNjoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2ljb25fbWFyZ2luIjtzOjA6IiI7czozNzoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6MzY6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6NDE6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czo0MjoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MzoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDY6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjQyOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQwOiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0NjoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NTI6ImxlZnRfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJsZWZ0X3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MjoibGVmdF93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mzk6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19jdXN0b21fbGlzdF9zdHlsZSI7czoyOiJubyI7czozMjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfYWxpZ24iO3M6MDoiIjtzOjM0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9wYWRkaW5nIjtzOjA6IiI7czozMzoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfbWFyZ2luIjtzOjA6IiI7czozNjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfZm9udF9zaXplIjtzOjA6IiI7czozNzoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfZm9udF9jb2xvciI7czowOiIiO3M6Mzc6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2xpbmtfY29sb3IiO3M6MDoiIjtzOjQzOiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9saW5rX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozMjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3Rfc3R5bGUiO3M6Njoic3F1YXJlIjtzOjMxOiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pY29uIjtzOjA6IiI7czozNjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozODoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaWNvbl9tYXJnaW4iO3M6MDoiIjtzOjM5OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX3BhZGRpbmciO3M6MDoiIjtzOjM4OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6NDM6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fbGluZV9oZWlnaHQiO3M6MDoiIjtzOjQ0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl93aWR0aCI7czowOiIiO3M6NDU6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDg6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6NDQ6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0MjoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0ODoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo1NDoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9iYWNrZ3JvdW5kX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo1MDoiYWJvdmVfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU0OiJhYm92ZV9jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM5OiJiZWxvd19jb250ZW50X2FyZWFfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6Mjoibm8iO3M6MzI6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2FsaWduIjtzOjA6IiI7czozNDoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfcGFkZGluZyI7czowOiIiO3M6MzM6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzY6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2ZvbnRfc2l6ZSI7czowOiIiO3M6Mzc6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2ZvbnRfY29sb3IiO3M6MDoiIjtzOjM3OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjA6IiI7czo0MzoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl9jb2xvciI7czowOiIiO3M6MzI6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X3N0eWxlIjtzOjc6ImRlZmF1bHQiO3M6MzE6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2ljb24iO3M6MDoiIjtzOjM2OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pY29uX3NpemUiO3M6MDoiIjtzOjM4OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pY29uX21hcmdpbiI7czowOiIiO3M6Mzk6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6Mzg6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fbWFyZ2luIjtzOjA6IiI7czo0MzoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9saW5lX2hlaWdodCI7czowOiIiO3M6NDQ6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0NToiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czo0ODoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9iYWNrZ3JvdW5kX2NvbG9yIjtzOjA6IiI7czo0NDoiYmVsb3dfY29udGVudF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQyOiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ4OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjU0OiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjUwOiJiZWxvd19jb250ZW50X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDg6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTQ6ImJlbG93X2NvbnRlbnRfYXJlYV93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzA6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3Rfc3R5bGluZyI7czozOiJ5ZXMiO3M6MzA6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfcGFkZGluZyI7czoxMDoiMCAwIDAgMTVweCI7czoyOToidGhlbWVfZm9udF9jdXN0b21fbGlzdF9tYXJnaW4iO3M6MDoiIjtzOjM5OiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2l0ZW1fbGluZV9oZWlnaHQiO3M6NDoiMzBweCI7czozMjoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9mb250X3NpemUiO3M6MDoiIjtzOjM3OiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2ZvbnRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjI5OiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2ZhbWlseSI7czoxMzoiT3BlbiBTYW5zOjQwMCI7czoyODoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9hbGlnbiI7czo0OiJsZWZ0IjtzOjMzOiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2ZvbnRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzM6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfbGlua19jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozOToidGhlbWVfZm9udF9jdXN0b21fbGlzdF9saW5rX2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQzOiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2xpbmtfdGV4dF9kZWNvcmF0aW9uIjtzOjQ6Im5vbmUiO3M6NDk6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfbGlua19ob3Zlcl90ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czoyNzoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pY29uIjtzOjQ6ImYxMDUiO3M6MzI6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozNzoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pY29uX3NpemVfdHlwZSI7czoyOiJweCI7czozNDoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pY29uX21hcmdpbiI7czoxMToiMCAwIDAgLTEycHgiO3M6MzU6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaXRlbV9wYWRkaW5nIjtzOjA6IiI7czozNDoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pdGVtX21hcmdpbiI7czo1OiI1cHggMCI7czo0MDoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pdGVtX2JvcmRlcl93aWR0aCI7czowOiIiO3M6NDA6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaXRlbV9ib3JkZXJfc3R5bGUiO3M6NToic29saWQiO3M6NDE6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaXRlbV9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czo0NDoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2l0ZW1fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pdGVtX2JveF9zaGFkb3ciO3M6NzoiMCAxcHggMCI7czo0NDoidGhlbWVfZm9udF9jdXN0b21fbGlzdF9pdGVtX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjUwOiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2l0ZW1fYmFja2dyb3VuZF9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9mb250X2N1c3RvbV9saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX2ZvbnRfY3VzdG9tX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNDoiZGVmYXVsdF93aWRnZXRfYXJlYV93Z19jdXN0b21fdHlwbyI7czozOiJ5ZXMiO3M6MzI6InJpZ2h0X3dpZGdldF9hcmVhX3dnX2N1c3RvbV90eXBvIjtzOjM6InllcyI7czozMToibGVmdF93aWRnZXRfYXJlYV93Z19jdXN0b21fdHlwbyI7czozOiJ5ZXMiO3M6MzM6ImFib3ZlX2NvbnRlbnRfYXJlYV93Z19jdXN0b21fdHlwbyI7czoyOiJubyI7czozMzoiYmVsb3dfY29udGVudF9hcmVhX3dnX2N1c3RvbV90eXBvIjtzOjI6Im5vIjtzOjIwOiJibG9ja3F1b3RlX2N1c3RvbV9iZyI7czozOiJ5ZXMiO3M6MTk6ImJsb2NrcXVvdGVfYmdfY29sb3IiO3M6MDoiIjtzOjE5OiJibG9ja3F1b3RlX2JnX2ltYWdlIjtzOjA6IiI7czoxOToiYmxvY2txdW90ZV9iZ3JlcGVhdCI7czoyMzoiY2VudGVyIGNlbnRlciBuby1yZXBlYXQiO3M6MjM6ImJsb2NrcXVvdGVfcGFyX2JnX3JhdGlvIjtzOjA6IiI7czoyMToiYmxvY2txdW90ZV9ib3hfc2hhZG93IjtzOjA6IiI7czoyNzoiYmxvY2txdW90ZV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyMToiYmxvY2txdW90ZV9mb250X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjI3OiJibG9ja3F1b3RlX2ZvbnRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjIxOiJibG9ja3F1b3RlX2xpbmtfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6Mjc6ImJsb2NrcXVvdGVfbGlua19ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czoyMDoiYmxvY2txdW90ZV9mb250X3NpemUiO3M6MDoiIjtzOjIyOiJibG9ja3F1b3RlX2ZvbnRfd2VpZ2h0IjtzOjA6IiI7czoyNToiYmxvY2txdW90ZV9mb250X3NpemVfdHlwZSI7czoyOiJweCI7czoyMjoiYmxvY2txdW90ZV9mb250X2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo0MDAiO3M6MzE6ImJsb2NrcXVvdGVfbGlua190ZXh0X2RlY29yYXRpb24iO3M6NDoibm9uZSI7czozNzoiYmxvY2txdW90ZV9saW5rX2hvdmVyX3RleHRfZGVjb3JhdGlvbiI7czo0OiJub25lIjtzOjE4OiJibG9ja3F1b3RlX3BhZGRpbmciO3M6OToiMTVweCAzMHB4IjtzOjE3OiJibG9ja3F1b3RlX21hcmdpbiI7czowOiIiO3M6MzA6ImJsb2NrcXVvdGVfZm9udF90ZXh0X3RyYW5zZm9ybSI7czoxMDoiY2FwaXRhbGl6ZSI7czoyODoiYmxvY2txdW90ZV9mb250X2F1dGhvcl9jb2xvciI7czo0OiJHcmV5IjtzOjI5OiJibG9ja3F1b3RlX2ZvbnRfYXV0aG9yX2ZhbWlseSI7czoxNDoiTW9udHNlcnJhdDo0MDAiO3M6Mjk6ImJsb2NrcXVvdGVfZm9udF9hdXRob3Jfd2VpZ2h0IjtzOjA6IiI7czoyNzoiYmxvY2txdW90ZV9mb250X2F1dGhvcl9zaXplIjtzOjA6IiI7czozMjoiYmxvY2txdW90ZV9mb250X2F1dGhvcl9zaXplX3R5cGUiO3M6MjoicHgiO3M6Mzc6ImJsb2NrcXVvdGVfZm9udF9hdXRob3JfdGV4dF90cmFuc2Zvcm0iO3M6Mjoibm8iO3M6MjM6ImJsb2NrcXVvdGVfYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjI0OiJibG9ja3F1b3RlX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjIzOiJibG9ja3F1b3RlX2JvcmRlcl93aWR0aCI7czo5OiIwIDAgMCAycHgiO3M6MjM6ImJsb2NrcXVvdGVfYm9yZGVyX2NvbG9yIjtzOjc6IlByaW1hcnkiO3M6Mjg6ImhlYWRlcl9hcmVhXzFfd2dfY3VzdG9tX3R5cG8iO3M6Mjoibm8iO3M6MzQ6ImhlYWRlcl9hcmVhXzFfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6Mjoibm8iO3M6Mjc6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9hbGlnbiI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9wYWRkaW5nIjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfMV93Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzE6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9mb250X3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfZm9udF9jb2xvciI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjA6IiI7czozODoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3Rfc3R5bGUiO3M6NzoiZGVmYXVsdCI7czoyNjoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2ljb24iO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2ljb25fbWFyZ2luIjtzOjA6IiI7czozNDoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6Mzg6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfMV93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MDoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDM6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjM5OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoiaGVhZGVyX2FyZWFfMV93Z19saXN0X2l0ZW1fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6ImhlYWRlcl9hcmVhXzFfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJoZWFkZXJfYXJlYV8xX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToiaGVhZGVyX2FyZWFfMV93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjg6ImhlYWRlcl9hcmVhXzJfd2dfY3VzdG9tX3R5cG8iO3M6Mjoibm8iO3M6MzQ6ImhlYWRlcl9hcmVhXzJfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6Mjoibm8iO3M6Mjc6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9hbGlnbiI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9wYWRkaW5nIjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfMl93Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzE6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9mb250X3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfZm9udF9jb2xvciI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjA6IiI7czozODoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3Rfc3R5bGUiO3M6NzoiZGVmYXVsdCI7czoyNjoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2ljb24iO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2ljb25fbWFyZ2luIjtzOjA6IiI7czozNDoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6Mzg6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfMl93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MDoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDM6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjM5OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoiaGVhZGVyX2FyZWFfMl93Z19saXN0X2l0ZW1fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6ImhlYWRlcl9hcmVhXzJfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJoZWFkZXJfYXJlYV8yX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToiaGVhZGVyX2FyZWFfMl93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjg6ImhlYWRlcl9hcmVhXzNfd2dfY3VzdG9tX3R5cG8iO3M6Mjoibm8iO3M6MzQ6ImhlYWRlcl9hcmVhXzNfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6Mjoibm8iO3M6Mjc6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9hbGlnbiI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9wYWRkaW5nIjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfM193Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzE6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9mb250X3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfZm9udF9jb2xvciI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjA6IiI7czozODoiaGVhZGVyX2FyZWFfM193Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3Rfc3R5bGUiO3M6NzoiZGVmYXVsdCI7czoyNjoiaGVhZGVyX2FyZWFfM193Z19saXN0X2ljb24iO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfM193Z19saXN0X2ljb25fbWFyZ2luIjtzOjA6IiI7czozNDoiaGVhZGVyX2FyZWFfM193Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6Mzg6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfM193Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MDoiaGVhZGVyX2FyZWFfM193Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDM6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjM5OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoiaGVhZGVyX2FyZWFfM193Z19saXN0X2l0ZW1fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6ImhlYWRlcl9hcmVhXzNfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJoZWFkZXJfYXJlYV8zX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToiaGVhZGVyX2FyZWFfM193Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjg6ImhlYWRlcl9hcmVhXzRfd2dfY3VzdG9tX3R5cG8iO3M6Mjoibm8iO3M6MzQ6ImhlYWRlcl9hcmVhXzRfd2dfY3VzdG9tX2xpc3Rfc3R5bGUiO3M6Mjoibm8iO3M6Mjc6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9hbGlnbiI7czowOiIiO3M6Mjk6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9wYWRkaW5nIjtzOjA6IiI7czoyODoiaGVhZGVyX2FyZWFfNF93Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzE6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9mb250X3NpemUiO3M6MDoiIjtzOjMyOiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfZm9udF9jb2xvciI7czowOiIiO3M6MzI6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjA6IiI7czozODoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI3OiJoZWFkZXJfYXJlYV80X3dnX2xpc3Rfc3R5bGUiO3M6NzoiZGVmYXVsdCI7czoyNjoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2ljb24iO3M6MDoiIjtzOjMxOiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozMzoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2ljb25fbWFyZ2luIjtzOjA6IiI7czozNDoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2l0ZW1fcGFkZGluZyI7czowOiIiO3M6MzM6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6Mzg6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9pdGVtX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czozOToiaGVhZGVyX2FyZWFfNF93Z19saXN0X2l0ZW1fYm9yZGVyX3dpZHRoIjtzOjA6IiI7czo0MDoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDM6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfY29sb3IiO3M6MDoiIjtzOjM5OiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaXRlbV9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoiaGVhZGVyX2FyZWFfNF93Z19saXN0X2l0ZW1fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDk6ImhlYWRlcl9hcmVhXzRfd2dfbGlzdF9pdGVtX2JhY2tncm91bmRfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJoZWFkZXJfYXJlYV80X3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0OToiaGVhZGVyX2FyZWFfNF93Z19saXN0X2l0ZW1faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjU6InRoZW1lX2ZvbGxvd19idXR0b25fd2lkdGgiO3M6NDoiNDBweCI7czoyNjoidGhlbWVfZm9sbG93X2J1dHRvbl9oZWlnaHQiO3M6NDoiNDBweCI7czo0NDoidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjQxOiJ0aGVtZV92aW1lb19zcXVhcmVfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDc6InRoZW1lX3ZpbWVvX3NxdWFyZV9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo1MToidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzFhYjdlYToxLjAwIjtzOjQ0OiJ0aGVtZV92aW1lb19zcXVhcmVfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDk6InRoZW1lX3ZpbWVvX3NxdWFyZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NTA6InRoZW1lX3ZpbWVvX3NxdWFyZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDk6InRoZW1lX3ZpbWVvX3NxdWFyZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ5OiJ0aGVtZV92aW1lb19zcXVhcmVfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1NToidGhlbWVfdmltZW9fc3F1YXJlX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6Mjc6InRoZW1lX3doaXRlX2J1dHRvbl9iZ19jb2xvciI7czo1OiJXaGl0ZSI7czozMzoidGhlbWVfd2hpdGVfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjU6IkJsYWNrIjtzOjMxOiJ0aGVtZV93aGl0ZV9idXR0b25fYm9yZGVyX2NvbG9yIjtzOjU6IkJsYWNrIjtzOjM3OiJ0aGVtZV93aGl0ZV9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjU6IldoaXRlIjtzOjI5OiJ0aGVtZV93aGl0ZV9idXR0b25fdGV4dF9jb2xvciI7czo1OiJCbGFjayI7czozMDoidGhlbWVfd2hpdGVfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czozNToidGhlbWVfd2hpdGVfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6NToiV2hpdGUiO3M6MzY6InRoZW1lX3doaXRlX2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mjk6InRoZW1lX3doaXRlX2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czozNToidGhlbWVfd2hpdGVfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM1OiJ0aGVtZV93aGl0ZV9idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3doaXRlX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czoyNjoidGhlbWVfZ3JleV9idXR0b25fYmdfY29sb3IiO3M6NDoiR3JleSI7czozMjoidGhlbWVfZ3JleV9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6NToiV2hpdGUiO3M6MzA6InRoZW1lX2dyZXlfYnV0dG9uX2JvcmRlcl9jb2xvciI7czo1OiJXaGl0ZSI7czozNjoidGhlbWVfZ3JleV9idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjQ6IkdyZXkiO3M6Mjg6InRoZW1lX2dyZXlfYnV0dG9uX3RleHRfY29sb3IiO3M6NToiV2hpdGUiO3M6Mjk6InRoZW1lX2dyZXlfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czozNDoidGhlbWVfZ3JleV9idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo0OiJHcmV5IjtzOjM1OiJ0aGVtZV9ncmV5X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mjg6InRoZW1lX2dyZXlfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjM0OiJ0aGVtZV9ncmV5X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfZ3JleV9idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDA6InRoZW1lX2dyZXlfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjI3OiJ0aGVtZV9ibGFja19idXR0b25fYmdfY29sb3IiO3M6NToiQmxhY2siO3M6MzM6InRoZW1lX2JsYWNrX2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czo1OiJXaGl0ZSI7czozMToidGhlbWVfYmxhY2tfYnV0dG9uX2JvcmRlcl9jb2xvciI7czo1OiJXaGl0ZSI7czozNzoidGhlbWVfYmxhY2tfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czo1OiJCbGFjayI7czoyOToidGhlbWVfYmxhY2tfYnV0dG9uX3RleHRfY29sb3IiO3M6NToiV2hpdGUiO3M6MzA6InRoZW1lX2JsYWNrX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6MzU6InRoZW1lX2JsYWNrX2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjU6IkJsYWNrIjtzOjM2OiJ0aGVtZV9ibGFja19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjI5OiJ0aGVtZV9ibGFja19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6MzU6InRoZW1lX2JsYWNrX2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfYmxhY2tfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQxOiJ0aGVtZV9ibGFja19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjY6InRoZW1lX2xpbmtfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czozMjoidGhlbWVfbGlua19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjMwOiJ0aGVtZV9saW5rX2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjM2OiJ0aGVtZV9saW5rX2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjI5OiJ0aGVtZV9saW5rX2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6MzU6InRoZW1lX2xpbmtfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czoyODoidGhlbWVfbGlua19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6MzQ6InRoZW1lX2xpbmtfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM0OiJ0aGVtZV9saW5rX2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MDoidGhlbWVfbGlua19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzI6InRoZW1lX3Ric19mb250X2ZhbWlseV9zYW5zX3NlcmlmIjtzOjA6IiI7czoyNzoidGhlbWVfdGJzX2ZvbnRfZmFtaWx5X3NlcmlmIjtzOjA6IiI7czozMToidGhlbWVfdGJzX2ZvbnRfZmFtaWx5X21vbm9zcGFjZSI7czowOiIiO3M6MjY6InRoZW1lX3Ric19mb250X2ZhbWlseV9iYXNlIjtzOjA6IiI7czoyNToidGhlbWVfdGJzX2ZvbnRfc2l6ZV9sYXJnZSI7czowOiIiO3M6MjU6InRoZW1lX3Ric19mb250X3NpemVfc21hbGwiO3M6MDoiIjtzOjI2OiJ0aGVtZV90YnNfY29udGFpbmVyX3RhYmxldCI7czowOiIiO3M6MjI6InRoZW1lX3Ric19jb250YWluZXJfc20iO3M6MDoiIjtzOjI3OiJ0aGVtZV90YnNfY29udGFpbmVyX2Rlc2t0b3AiO3M6MDoiIjtzOjIyOiJ0aGVtZV90YnNfY29udGFpbmVyX21kIjtzOjA6IiI7czozMzoidGhlbWVfdGJzX2NvbnRhaW5lcl9sYXJnZV9kZXNrdG9wIjtzOjA6IiI7czoyMjoidGhlbWVfdGJzX2NvbnRhaW5lcl9sZyI7czowOiIiO3M6Mjg6InRoZW1lX3Ric19zdGF0ZV9zdWNjZXNzX3RleHQiO3M6MDoiIjtzOjI2OiJ0aGVtZV90YnNfc3RhdGVfc3VjY2Vzc19iZyI7czowOiIiO3M6MzA6InRoZW1lX3Ric19zdGF0ZV9zdWNjZXNzX2JvcmRlciI7czowOiIiO3M6MjU6InRoZW1lX3Ric19zdGF0ZV9pbmZvX3RleHQiO3M6MDoiIjtzOjIzOiJ0aGVtZV90YnNfc3RhdGVfaW5mb19iZyI7czowOiIiO3M6Mjc6InRoZW1lX3Ric19zdGF0ZV9pbmZvX2JvcmRlciI7czowOiIiO3M6Mjg6InRoZW1lX3Ric19zdGF0ZV93YXJuaW5nX3RleHQiO3M6MDoiIjtzOjI2OiJ0aGVtZV90YnNfc3RhdGVfd2FybmluZ19iZyI7czowOiIiO3M6MzA6InRoZW1lX3Ric19zdGF0ZV93YXJuaW5nX2JvcmRlciI7czowOiIiO3M6Mjc6InRoZW1lX3Ric19zdGF0ZV9kYW5nZXJfdGV4dCI7czowOiIiO3M6MjU6InRoZW1lX3Ric19zdGF0ZV9kYW5nZXJfYmciO3M6MDoiIjtzOjI5OiJ0aGVtZV90YnNfc3RhdGVfZGFuZ2VyX2JvcmRlciI7czowOiIiO3M6MTg6InRoZW1lX3Ric19wYW5lbF9iZyI7czowOiIiO3M6Mjg6InRoZW1lX3Ric19wYW5lbF9ib2R5X3BhZGRpbmciO3M6MDoiIjtzOjMxOiJ0aGVtZV90YnNfcGFuZWxfaGVhZGluZ19wYWRkaW5nIjtzOjA6IiI7czozMDoidGhlbWVfdGJzX3BhbmVsX2Zvb3Rlcl9wYWRkaW5nIjtzOjA6IiI7czoyOToidGhlbWVfdGJzX3BhbmVsX2JvcmRlcl9yYWRpdXMiO3M6MDoiIjtzOjI4OiJ0aGVtZV90YnNfcGFuZWxfaW5uZXJfYm9yZGVyIjtzOjA6IiI7czoyNToidGhlbWVfdGJzX3BhbmVsX2Zvb3Rlcl9iZyI7czowOiIiO3M6Mjg6InRoZW1lX3Ric19wYW5lbF9kZWZhdWx0X3RleHQiO3M6MDoiIjtzOjMwOiJ0aGVtZV90YnNfcGFuZWxfZGVmYXVsdF9ib3JkZXIiO3M6MDoiIjtzOjM0OiJ0aGVtZV90YnNfcGFuZWxfZGVmYXVsdF9oZWFkaW5nX2JnIjtzOjA6IiI7czoyODoidGhlbWVfdGJzX3BhbmVsX3ByaW1hcnlfdGV4dCI7czowOiIiO3M6MzA6InRoZW1lX3Ric19wYW5lbF9wcmltYXJ5X2JvcmRlciI7czowOiIiO3M6MzQ6InRoZW1lX3Ric19wYW5lbF9wcmltYXJ5X2hlYWRpbmdfYmciO3M6MDoiIjtzOjI4OiJ0aGVtZV90YnNfcGFuZWxfc3VjY2Vzc190ZXh0IjtzOjA6IiI7czozMDoidGhlbWVfdGJzX3BhbmVsX3N1Y2Nlc3NfYm9yZGVyIjtzOjA6IiI7czozNDoidGhlbWVfdGJzX3BhbmVsX3N1Y2Nlc3NfaGVhZGluZ19iZyI7czowOiIiO3M6MjU6InRoZW1lX3Ric19wYW5lbF9pbmZvX3RleHQiO3M6MDoiIjtzOjI3OiJ0aGVtZV90YnNfcGFuZWxfaW5mb19ib3JkZXIiO3M6MDoiIjtzOjMxOiJ0aGVtZV90YnNfcGFuZWxfaW5mb19oZWFkaW5nX2JnIjtzOjA6IiI7czoyODoidGhlbWVfdGJzX3BhbmVsX3dhcm5pbmdfdGV4dCI7czowOiIiO3M6MzA6InRoZW1lX3Ric19wYW5lbF93YXJuaW5nX2JvcmRlciI7czowOiIiO3M6MzQ6InRoZW1lX3Ric19wYW5lbF93YXJuaW5nX2hlYWRpbmdfYmciO3M6MDoiIjtzOjI3OiJ0aGVtZV90YnNfcGFuZWxfZGFuZ2VyX3RleHQiO3M6MDoiIjtzOjI5OiJ0aGVtZV90YnNfcGFuZWxfZGFuZ2VyX2JvcmRlciI7czowOiIiO3M6MzM6InRoZW1lX3Ric19wYW5lbF9kYW5nZXJfaGVhZGluZ19iZyI7czowOiIiO3M6MTA6ImxvZ29fYWxpZ24iO3M6NDoibGVmdCI7czoxOToibG9nb190YWdsaW5lX21hcmdpbiI7czowOiIiO3M6MjA6ImxvZ29fdGFnbGluZV9oZWFkaW5nIjtzOjA6IiI7czoyMzoibG9nb190YWdsaW5lX2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6Mjk6ImxvZ29fdGFnbGluZV9mb250X2hvdmVyX2NvbG9yIjtzOjU6IldoaXRlIjtzOjIyOiJsb2dvX3RhZ2xpbmVfZm9udF9zaXplIjtzOjA6IiI7czoyNDoibG9nb190YWdsaW5lX2ZvbnRfd2VpZ2h0IjtzOjA6IiI7czozMzoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2N1c3RvbV90eXBvIjtzOjM6InllcyI7czozOToiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2N1c3RvbV9saXN0X3N0eWxlIjtzOjM6InllcyI7czozMjoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfYWxpZ24iO3M6NDoibGVmdCI7czozNDoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfcGFkZGluZyI7czowOiIiO3M6MzM6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X21hcmdpbiI7czowOiIiO3M6MzY6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfc2l6ZSI7czowOiIiO3M6Mzc6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6Mzc6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfY29sb3IiO3M6NToiV2hpdGUiO3M6NDM6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2xpbmtfaG92ZXJfY29sb3IiO3M6OToiTmF2IEhvdmVyIjtzOjMyOiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9zdHlsZSI7czo0OiJub25lIjtzOjMxOiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pY29uIjtzOjA6IiI7czozNjoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9zaXplIjtzOjA6IiI7czozODoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9tYXJnaW4iO3M6MDoiIjtzOjM5OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX3BhZGRpbmciO3M6MDoiIjtzOjM4OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX21hcmdpbiI7czowOiIiO3M6NDM6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fbGluZV9oZWlnaHQiO3M6MDoiIjtzOjQ0OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl93aWR0aCI7czowOiIiO3M6NDU6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6NDg6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6NDQ6ImZvb3Rlcl93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0MjoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93IjtzOjA6IiI7czo0ODoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo1NDoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9iYWNrZ3JvdW5kX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo1MDoiZm9vdGVyX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ4OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU0OiJmb290ZXJfd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQwOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2N1c3RvbV90eXBvIjtzOjM6InllcyI7czo0NjoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19jdXN0b21fbGlzdF9zdHlsZSI7czozOiJ5ZXMiO3M6Mzk6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9hbGlnbiI7czo0OiJsZWZ0IjtzOjQxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfcGFkZGluZyI7czowOiIiO3M6NDA6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9tYXJnaW4iO3M6MDoiIjtzOjQzOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfZm9udF9zaXplIjtzOjA6IiI7czo0NDoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2ZvbnRfY29sb3IiO3M6NToiV2hpdGUiO3M6NDQ6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9saW5rX2NvbG9yIjtzOjU6IldoaXRlIjtzOjUwOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfbGlua19ob3Zlcl9jb2xvciI7czo5OiJOYXYgSG92ZXIiO3M6Mzk6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9zdHlsZSI7czo0OiJub25lIjtzOjM4OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbiI7czowOiIiO3M6NDM6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9pY29uX3NpemUiO3M6MDoiIjtzOjQ1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaWNvbl9tYXJnaW4iO3M6MDoiIjtzOjQ2OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9wYWRkaW5nIjtzOjA6IiI7czo0NToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fbWFyZ2luIjtzOjA6IiI7czo1MDoiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fbGluZV9oZWlnaHQiO3M6MDoiIjtzOjUxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfd2lkdGgiO3M6MDoiIjtzOjUyOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3JkZXJfcmFkaXVzIjtzOjA6IiI7czo1NToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYmFja2dyb3VuZF9jb2xvciI7czowOiIiO3M6NTE6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDk6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JveF9zaGFkb3ciO3M6MDoiIjtzOjU1OiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo2MToiZm9vdGVyX2JvdHRvbV93aWRnZXRfYXJlYV93Z19saXN0X2l0ZW1fYmFja2dyb3VuZF9ob3Zlcl9jb2xvciI7czowOiIiO3M6NTc6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NTU6ImZvb3Rlcl9ib3R0b21fd2lkZ2V0X2FyZWFfd2dfbGlzdF9pdGVtX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjYxOiJmb290ZXJfYm90dG9tX3dpZGdldF9hcmVhX3dnX2xpc3RfaXRlbV9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozNjoidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX2JlaGFuY2VfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV9iZWhhbmNlX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozOToidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX2JlaGFuY2VfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX2JlaGFuY2VfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfYmVoYW5jZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9iZWhhbmNlX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX2JlaGFuY2VfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozNjoidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX2NvZGVwZW5fZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV9jb2RlcGVuX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozOToidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX2NvZGVwZW5fZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX2NvZGVwZW5fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfY29kZXBlbl9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9jb2RlcGVuX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX2NvZGVwZW5fZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNjoidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozMzoidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDM6InRoZW1lX2RpZ2dfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM1OiJ0aGVtZV9kaWdnX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozNjoidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0MToidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDI6InRoZW1lX2RpZ2dfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6MzU6InRoZW1lX2RpZ2dfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MToidGhlbWVfZGlnZ19mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV9kaWdnX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDc6InRoZW1lX2RpZ2dfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czozNjoidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MjoidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czo0MDoidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX2JvcmRlcl9jb2xvciI7czowOiIiO3M6NDY6InRoZW1lX2Ryb3Bib3hfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV9kcm9wYm94X2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozOToidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfY29sb3IiO3M6ODoiTmF2IExpbmsiO3M6NDU6InRoZW1lX2Ryb3Bib3hfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzg6InRoZW1lX2Ryb3Bib3hfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfZHJvcGJveF9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV9kcm9wYm94X2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NTA6InRoZW1lX2Ryb3Bib3hfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM1OiJ0aGVtZV9naXR0aXBfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX2dpdHRpcF9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozODoidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9naXR0aXBfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ0OiJ0aGVtZV9naXR0aXBfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzc6InRoZW1lX2dpdHRpcF9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9naXR0aXBfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfZ2l0dGlwX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDk6InRoZW1lX2dpdHRpcF9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzU6InRoZW1lX3JlZGRpdF9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfcmVkZGl0X2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjZmY0NTAwOjEuMDAiO3M6Mzg6InRoZW1lX3JlZGRpdF9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfcmVkZGl0X2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NDoidGhlbWVfcmVkZGl0X2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfcmVkZGl0X2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDM6InRoZW1lX3JlZGRpdF9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ5OiJ0aGVtZV9yZWRkaXRfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM1OiJ0aGVtZV9yZW5yZW5fZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3JlbnJlbl9mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NToidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozODoidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9yZW5yZW5fZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQ0OiJ0aGVtZV9yZW5yZW5fZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6Mzc6InRoZW1lX3JlbnJlbl9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQzOiJ0aGVtZV9yZW5yZW5fZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MzoidGhlbWVfcmVucmVuX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDk6InRoZW1lX3JlbnJlbl9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjQyOiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ4OiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQ2OiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo1MjoidGhlbWVfc3RhY2tleGNoYW5nZV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDQ6InRoZW1lX3N0YWNrZXhjaGFuZ2VfZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQ1OiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjUwOiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo1MToidGhlbWVfc3RhY2tleGNoYW5nZV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0NDoidGhlbWVfc3RhY2tleGNoYW5nZV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUwOiJ0aGVtZV9zdGFja2V4Y2hhbmdlX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NTA6InRoZW1lX3N0YWNrZXhjaGFuZ2VfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1NjoidGhlbWVfc3RhY2tleGNoYW5nZV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM5OiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0OToidGhlbWVfc291bmRjbG91ZF9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6NDE6InRoZW1lX3NvdW5kY2xvdWRfZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjQyOiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0ODoidGhlbWVfc291bmRjbG91ZF9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0MToidGhlbWVfc291bmRjbG91ZF9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV9zb3VuZGNsb3VkX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDc6InRoZW1lX3NvdW5kY2xvdWRfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MzoidGhlbWVfc291bmRjbG91ZF9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fY3VzdG9tX3RleHQiO3M6MDoiIjtzOjM2OiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQyOiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fYm9yZGVyX2NvbG9yIjtzOjA6IiI7czo0NjoidGhlbWVfc3BvdGlmeV9mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzg6InRoZW1lX3Nwb3RpZnlfZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEwOiJUZXh0IENvbG9yIjtzOjM5OiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fdGV4dF9zaGFkb3ciO3M6MDoiIjtzOjQ0OiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NToidGhlbWVfc3BvdGlmeV9mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozODoidGhlbWVfc3BvdGlmeV9mb2xsb3dfYnV0dG9uX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ0OiJ0aGVtZV9zcG90aWZ5X2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDQ6InRoZW1lX3Nwb3RpZnlfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo1MDoidGhlbWVfc3BvdGlmeV9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjM4OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzU6InRoZW1lX3R1bWJscl9mb2xsb3dfYnV0dG9uX2JnX2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfdHVtYmxyX2ZvbGxvd19idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM5OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl90ZXh0X2NvbG9yIjtzOjEyOiIjMzU0NjVjOjEuMDAiO3M6Mzg6InRoZW1lX3R1bWJscl9mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfdHVtYmxyX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0NDoidGhlbWVfdHVtYmxyX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM3OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0MzoidGhlbWVfdHVtYmxyX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDM6InRoZW1lX3R1bWJscl9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ5OiJ0aGVtZV90dW1ibHJfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNDoidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzE6InRoZW1lX3ZrX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjM3OiJ0aGVtZV92a19mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQxOiJ0aGVtZV92a19mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6MzM6InRoZW1lX3ZrX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMjoiIzQ1NjY4ZToxLjAwIjtzOjM0OiJ0aGVtZV92a19mb2xsb3dfYnV0dG9uX3RleHRfc2hhZG93IjtzOjA6IiI7czozOToidGhlbWVfdmtfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQwOiJ0aGVtZV92a19mb2xsb3dfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czozMzoidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czozOToidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozOToidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NToidGhlbWVfdmtfZm9sbG93X2J1dHRvbl9ob3Zlcl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czozNzoidGhlbWVfd2VpYm9fZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzQ6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25fYmdfY29sb3IiO3M6MDoiIjtzOjQwOiJ0aGVtZV93ZWlib19mb2xsb3dfYnV0dG9uX2JnX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozODoidGhlbWVfd2VpYm9fZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQ0OiJ0aGVtZV93ZWlib19mb2xsb3dfYnV0dG9uX2JvcmRlcl9ob3Zlcl9jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25fdGV4dF9jb2xvciI7czoxMDoiVGV4dCBDb2xvciI7czozNzoidGhlbWVfd2VpYm9fZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDI6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9jb2xvciI7czo4OiJOYXYgTGluayI7czo0MzoidGhlbWVfd2VpYm9fZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX3NoYWRvdyI7czowOiIiO3M6MzY6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDI6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25fYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDI6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvdyI7czowOiIiO3M6NDg6InRoZW1lX3dlaWJvX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzM6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzc6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV94aW5nX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfeGluZ19mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzY6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQyOiJ0aGVtZV94aW5nX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM1OiJ0aGVtZV94aW5nX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3hpbmdfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfeGluZ19mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV94aW5nX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MzY6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6MzM6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl9iZ19jb2xvciI7czowOiIiO3M6Mzk6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl9iZ19ob3Zlcl9jb2xvciI7czowOiIiO3M6Mzc6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl9ib3JkZXJfY29sb3IiO3M6MDoiIjtzOjQzOiJ0aGVtZV95ZWxwX2ZvbGxvd19idXR0b25fYm9yZGVyX2hvdmVyX2NvbG9yIjtzOjA6IiI7czozNToidGhlbWVfeWVscF9mb2xsb3dfYnV0dG9uX3RleHRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6MzY6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjg6Ik5hdiBMaW5rIjtzOjQyOiJ0aGVtZV95ZWxwX2ZvbGxvd19idXR0b25fdGV4dF9ob3Zlcl9zaGFkb3ciO3M6MDoiIjtzOjM1OiJ0aGVtZV95ZWxwX2ZvbGxvd19idXR0b25fYm94X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX3llbHBfZm9sbG93X2J1dHRvbl9ib3hfc2hhZG93X2NvbG9yIjtzOjA6IiI7czo0MToidGhlbWVfeWVscF9mb2xsb3dfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjQ3OiJ0aGVtZV95ZWxwX2ZvbGxvd19idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6MjQ6InRoZW1lX2dyaWRfb3ZlcmxheV9jb2xvciI7czoxMToiVHJhbnNwYXJlbnQiO3M6MjY6InRoZW1lX2dyaWRfaW1hZ2VfZ3JheXNjYWxlIjtzOjI6Im5vIjtzOjI0OiJ0aGVtZV9ncmlkX2ltYWdlX29wYWNpdHkiO3M6MDoiIjtzOjMwOiJ0aGVtZV9ncmlkX2ltYWdlX29wYWNpdHlfaG92ZXIiO3M6MDoiIjtzOjIxOiJ0aGVtZV9ncmlkX3RyYW5zaXRpb24iO3M6MjA6ImFsbCAwLjVzIGVhc2UtaW4tb3V0IjtzOjIwOiJ0aGVtZV9ncmlkX3RyYW5zZm9ybSI7czowOiIiO3M6MjY6InRoZW1lX2dyaWRfdHJhbnNmb3JtX2hvdmVyIjtzOjI2OiJzY2FsZSgxLjA3KSByb3RhdGUoMC41ZGVnKSI7czoyNjoidGhlbWVfZ3JpZF90cmFuc2Zvcm1fc3R5bGUiO3M6MDoiIjtzOjI3OiJ0aGVtZV9ncmlkX3RyYW5zZm9ybV9vcmlnaW4iO3M6MDoiIjtzOjQwOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25faWNvbl9zaXplIjtzOjU6ImZhLWxnIjtzOjQyOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fdGV4dF9mYW1pbHkiO3M6MDoiIjtzOjQwOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fdGV4dF9zaXplIjtzOjA6IiI7czo0NToidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX3RleHRfc2l6ZV90eXBlIjtzOjI6InB4IjtzOjQyOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fdGV4dF93ZWlnaHQiO3M6Njoibm9ybWFsIjtzOjQ1OiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fdGV4dF90cmFuc2Zvcm0iO3M6OToidXBwZXJjYXNlIjtzOjM2OiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fd2lkdGgiO3M6MDoiIjtzOjM3OiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25faGVpZ2h0IjtzOjA6IiI7czo0MjoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX2xpbmVfaGVpZ2h0IjtzOjA6IiI7czozNzoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX21hcmdpbiI7czoxMDoiMCAxMHB4IDAgMCI7czozODoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX3BhZGRpbmciO3M6ODoiOHB4IDE2cHgiO3M6NDM6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl9ib3JkZXJfd2lkdGgiO3M6MDoiIjtzOjQzOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fYm9yZGVyX3N0eWxlIjtzOjU6InNvbGlkIjtzOjQ0OiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fYm9yZGVyX3JhZGl1cyI7czowOiIiO3M6Mzk6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl9iZ19jb2xvciI7czo1OiJXaGl0ZSI7czo0MzoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX2JvcmRlcl9jb2xvciI7czo1OiJXaGl0ZSI7czo0MToidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX3RleHRfY29sb3IiO3M6MTA6IlRleHQgQ29sb3IiO3M6NDI6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl90ZXh0X3NoYWRvdyI7czowOiIiO3M6NDE6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl9ib3hfc2hhZG93IjtzOjA6IiI7czo0NzoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX2JveF9zaGFkb3dfY29sb3IiO3M6MDoiIjtzOjQ1OiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25fYmdfaG92ZXJfY29sb3IiO3M6NToiQmxhY2siO3M6NDk6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl9ib3JkZXJfaG92ZXJfY29sb3IiO3M6NToiQmxhY2siO3M6NDc6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2J1dHRvbl90ZXh0X2hvdmVyX2NvbG9yIjtzOjk6IiNmZmZmZmY6MSI7czo0ODoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX3RleHRfaG92ZXJfc2hhZG93IjtzOjA6IiI7czo0NzoidGhlbWVfZ3JpZF9pdGVtX292ZXJsYXlfYnV0dG9uX2hvdmVyX2JveF9zaGFkb3ciO3M6MDoiIjtzOjUzOiJ0aGVtZV9ncmlkX2l0ZW1fb3ZlcmxheV9idXR0b25faG92ZXJfYm94X3NoYWRvd19jb2xvciI7czowOiIiO3M6NDc6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X3ByZXR0eXBob3RvX2J1dHRvbl9pY29uIjtzOjEyOiJmYS1waWN0dXJlLW8iO3M6NTQ6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X3ByZXR0eXBob3RvX2J1dHRvbl9jdXN0b21fdGV4dCI7czowOiIiO3M6NDA6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2l0ZW1fYnV0dG9uX2ljb24iO3M6MTY6ImZhLWV4dGVybmFsLWxpbmsiO3M6NDc6InRoZW1lX2dyaWRfaXRlbV9vdmVybGF5X2l0ZW1fYnV0dG9uX2N1c3RvbV90ZXh0IjtzOjA6IiI7czoyODoidGhlbWVfZm9udF9ib3hlZF9ib3hfcGFkZGluZyI7czowOiIiO3M6MjQ6InRoZW1lX2ZvbnRfaDFfbWFyZ2luX3RvcCI7czo0OiIxNXB4IjtzOjI3OiJ0aGVtZV9mb250X2gxX21hcmdpbl9ib3R0b20iO3M6NDoiMjVweCI7czoyNDoidGhlbWVfZm9udF9oMl9tYXJnaW5fdG9wIjtzOjQ6IjE1cHgiO3M6Mjc6InRoZW1lX2ZvbnRfaDJfbWFyZ2luX2JvdHRvbSI7czo0OiIyNXB4IjtzOjI0OiJ0aGVtZV9mb250X2gzX21hcmdpbl90b3AiO3M6NDoiMTBweCI7czoyNzoidGhlbWVfZm9udF9oM19tYXJnaW5fYm90dG9tIjtzOjQ6IjIwcHgiO3M6MjQ6InRoZW1lX2ZvbnRfaDRfbWFyZ2luX3RvcCI7czo0OiIxMHB4IjtzOjI3OiJ0aGVtZV9mb250X2g0X21hcmdpbl9ib3R0b20iO3M6NDoiMjBweCI7czoyNDoidGhlbWVfZm9udF9oNV9tYXJnaW5fdG9wIjtzOjA6IiI7czoyNzoidGhlbWVfZm9udF9oNV9tYXJnaW5fYm90dG9tIjtzOjA6IiI7czoyNDoidGhlbWVfZm9udF9oNl9tYXJnaW5fdG9wIjtzOjA6IiI7czoyNzoidGhlbWVfZm9udF9oNl9tYXJnaW5fYm90dG9tIjtzOjA6IiI7czoyMzoiZW50cnlfdGl0bGVfcGFkZGluZ190b3AiO3M6MDoiIjtzOjI2OiJlbnRyeV90aXRsZV9wYWRkaW5nX2JvdHRvbSI7czowOiIiO3M6MTI6ImxvZ29faGVhZGluZyI7czo0OiJub25lIjt9fXM6NjoiaGVhZGVyIjthOjQ6e3M6MTI6ImhlYWRlcl9vcmRlciI7YTozOntpOjA7czoxMDoibGlzdEl0ZW1fNSI7aToxO3M6MTA6Imxpc3RJdGVtXzYiO2k6MjtzOjEwOiJsaXN0SXRlbV83Ijt9czo4OiJib3hfZGF0YSI7YTo3OntzOjEwOiJsaXN0SXRlbV81IjtzOjM6IjEvNCI7czoxMDoibGlzdEl0ZW1fNiI7czozOiIzLzQiO3M6MTA6Imxpc3RJdGVtXzciO3M6MzoiMS8xIjtzOjEwOiJsaXN0SXRlbV8xIjtzOjM6IjEvMyI7czoxMDoibGlzdEl0ZW1fMiI7czozOiIxLzMiO3M6MTA6Imxpc3RJdGVtXzMiO3M6MzoiMS8zIjtzOjEwOiJsaXN0SXRlbV80IjtzOjM6IjEvMyI7fXM6MTY6ImJveF9kYXRhX2NsYXNzZXMiO2E6Nzp7czoxMDoibGlzdEl0ZW1fMSI7czowOiIiO3M6MTA6Imxpc3RJdGVtXzIiO3M6MDoiIjtzOjEwOiJsaXN0SXRlbV8zIjtzOjA6IiI7czoxMDoibGlzdEl0ZW1fNCI7czowOiIiO3M6MTA6Imxpc3RJdGVtXzUiO3M6MDoiIjtzOjEwOiJsaXN0SXRlbV82IjtzOjA6IiI7czoxMDoibGlzdEl0ZW1fNyI7czowOiIiO31zOjEwOiJ0b2dnbGVfbmF2IjtzOjE6IjEiO31zOjQ6Im1pc2MiO2E6MzI6e3M6MTY6ImN1c3RvbV9qc19oZWFkZXIiO3M6MjM0ODoiPHN0eWxlIHR5cGU9XCd0ZXh0L2Nzc1wnPg0KQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDMwcHgpIHsNCi5oZWFkbGluZSBoMXtmb250LXNpemU6IDI1cHggIWltcG9ydGFudDt9DQouaGVhZGxpbmUgaDJ7Zm9udC1zaXplOiAyMnB4ICFpbXBvcnRhbnQ7fQ0KLmJvcmRlci1oZWFkaW5new0KICAgIGZvbnQtc2l6ZTogMjVweCAhaW1wb3J0YW50O30NCn0NCi5uYXZiYXIgLm5hdiA+IGxpID4gLnNkZi1kcm9wZG93bi10b3Agew0KZGlzcGxheTogbm9uZTsNCn0NCi5uYXZiYXIgLm5hdiA+IGxpI21lbnUtaXRlbS1zZWFyY2ggPiAuc2RmLWRyb3Bkb3duLXRvcCwgDQoubmF2YmFyIC5uYXYgPiBsaTpob3ZlciA+IC5zZGYtZHJvcGRvd24tdG9wIHsNCiAgICBvcGFjaXR5OiAwOw0KfQ0KLmRlZmF1bHQtd2lkZ2V0LWFyZWEgLndpZGdldHRpdGxlIHsNCiAgICBsZXR0ZXItc3BhY2luZzogNXB4O30NCmhlYWRlci5lbnRyeS10aXRsZSBoMi5lbnRyeS10aXRsZSBhew0KICAgIHRleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTsNCiAgICBsZXR0ZXItc3BhY2luZzogOHB4Ow0KICAgIGZvbnQtZmFtaWx5OiBNb250c2VycmF0Ow0KICAgIGZvbnQtd2VpZ2h0OiA3MDA7fQ0KaGVhZGVyLmVudHJ5LXRpdGxlIGgyLmVudHJ5LXRpdGxlIGE6YWZ0ZXJ7DQogIGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7DQogICAgY29udGVudDogdXJsKFwnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFETUFBQUFOQ0FZQUFBQUUwVmIzQUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUFFSkpSRUZVZU5waVpHQmcrTTh3UE1BeUpvWmhCQmlIdWdmKy8wY2tyR0VWTTZPZUdmWE1xR2RHUFRNOEFBc1FHMVBaekxNRFdXbitwNEdabzVVbXBRQWd3QUJ5UGdtNUMxNEdsQUFBQUFCSlJVNUVya0pnZ2c9PVwnKTsNCiAgICBkaXNwbGF5OiBibG9jazsNCiAgICBoZWlnaHQ6IGF1dG87DQogICAgbWFyZ2luOjMwcHggYXV0byAzMHB4Ow0KICAgIHdpZHRoOmF1dG87DQogICAgcG9zaXRpb246IHJlbGF0aXZlOw0KfQ0KI3BhZ2UtaGVhZGVyIC5lbnRyeS10aXRsZTphZnRlciwNCi51bmRlcmxpbmUtdGl0bGU6YWZ0ZXIgew0KICAgIGJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7DQogICAgY29udGVudDogdXJsKFwnZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFETUFBQUFaQ0FZQUFBQ2NsaFo2QUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUFHUkpSRUZVZU5yczJNRUpBQ0VNUk5GUjdNaWV0eHRic1lYb3plc2VGSEg0QTE0REQ0TUpGa2toajN4WlJrbXZBeUpXWTFuZERCZ3dZTUI0cE14VE45ZHNONGRtSEtqSjBOelJab2tIQUF3WU1HREFnUG0zZWxRVFN6K3htOTJLMTFmVEVHQUF5Vk1NbTRjNVAyWUFBQUFBU1VWT1JLNUNZSUk9XCcpOw0KICAgIGRpc3BsYXk6IGJsb2NrOw0KICAgIGhlaWdodDogYXV0bzsNCiAgICBtYXJnaW46MzBweCBhdXRvIDYwcHg7DQogICAgd2lkdGg6YXV0bzsNCiAgICBwb3NpdGlvbjogcmVsYXRpdmU7DQp9DQouYm9yZGVyLWhlYWRpbmd7Y29sb3I6IzMzMztib3JkZXI6MTBweCBzb2xpZDtib3JkZXItY29sb3I6IzI4MjgyODtwYWRkaW5nOjI1cHggNTBweDt9DQojYWNjb3JkaW9uIC5wYW5lbCB7DQogICAgbWFyZ2luLWJvdHRvbTogLTRweCAhaW1wb3J0YW50Ow0KICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7DQogICAgYm9yZGVyOiAwOw0KICAgIGNvbG9yOiAjZmZmOw0KICAgIGZvbnQtd2VpZ2h0OiAzMDA7DQogICAgZm9udC1zaXplOiAxM3B4Ow0KfQ0KI2FjY29yZGlvbiAucGFuZWwgLnBhbmVsLWhlYWRpbmcgLnBhbmVsLXRpdGxlIGEuYWNjb3JkaW9uLXRvZ2dsZSB7DQogICAgZm9udC1zaXplOiAxNHB4Ow0KICAgIGZvbnQtd2VpZ2h0OiAzMDA7DQogICBwb3NpdGlvbjpyZWxhdGl2ZTsNCiAgIGRpc3BsYXk6YmxvY2s7DQp9DQojYWNjb3JkaW9uIC5wYW5lbCAucGFuZWwtaGVhZGluZyAucGFuZWwtdGl0bGUgYS5hY2NvcmRpb24tdG9nZ2xlIGl7ICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsNCiAgICByaWdodDogMHB4Ow0KICAgIGRpc3BsYXk6IGJsb2NrOw0KICAgIHRvcDogMHB4Ow0KY29sb3I6I0FDQUNBQzsNCn0NCg0KLnNlYy1hY2NvcmRpb24taGVybyAjYWNjb3JkaW9uIC5wYW5lbCAucGFuZWwtY29sbGFwc2UgLnBhbmVsLWJvZHl7DQpjb2xvcjojMzMzMzMzOw0KfQ0KI3BhZ2UtaGVhZGVyIC5lbnRyeS10aXRsZSBhe2xldHRlci1zcGFjaW5nOjE1cHg7fQ0KLmltZy1mbG9hdHtmbG9hdDpub25lICFpbXBvcnRhbnQ7fQ0KPC9zdHlsZT4iO3M6MTk6InRvZ2dsZV9wbGFjZWhvbGRlcnMiO2k6MDtzOjE2OiJ0b2dnbGVfYWRtaW5fYmFyIjtzOjE6IjEiO3M6MTU6InRvZ2dsZV9hdXRvc2F2ZSI7czowOiIiO3M6MTc6InRvZ2dsZV9uaWNlc2Nyb2xsIjtzOjE6IjIiO3M6MTU6InRvZ2dsZV9zY3JvbGx0byI7czoxOiIxIjtzOjIzOiJzY3JvbGx0b3RvcF9lYXNpbmdfdHlwZSI7czoxMzoiZWFzZUluT3V0U2luZSI7czoyMDoic2Nyb2xsdG90b3BfZHVyYXRpb24iO3M6MzoiODAwIjtzOjIwOiJhdXRvX3VwZGF0ZV91c2VybmFtZSI7czowOiIiO3M6MTc6ImF1dG9fdXBkYXRlX2VtYWlsIjtzOjA6IiI7czoxODoidG9nZ2xlX3l0X2JnX3ZpZGVvIjtzOjE6IjEiO3M6MTY6ImN1c3RvbV9qc19mb290ZXIiO3M6NjkzOiI8c2NyaXB0IHR5cGU9XCJ0ZXh0L2phdmFzY3JpcHRcIj4NCmpRdWVyeShmdW5jdGlvbiAoJCkgew0KICAgIHZhciAkYWN0aXZlID0gJChcJyNhY2NvcmRpb24gLnBhbmVsLWNvbGxhcHNlLmluXCcpLnByZXYoKS5hZGRDbGFzcyhcJ2FjdGl2ZVwnKTsNCiAgICAkYWN0aXZlLmZpbmQoXCdhXCcpLnByZXBlbmQoXCc8aSBjbGFzcz1cImdseXBoaWNvbiBnbHlwaGljb24tbWludXNcIj48L2k+XCcpOw0KICAgICQoXCcjYWNjb3JkaW9uIC5wYW5lbC1oZWFkaW5nXCcpLm5vdCgkYWN0aXZlKS5maW5kKFwnYVwnKS5wcmVwZW5kKFwnPGkgY2xhc3M9XCJnbHlwaGljb24gZ2x5cGhpY29uLXBsdXNcIj48L2k+XCcpOw0KICAgICQoXCcjYWNjb3JkaW9uXCcpLm9uKFwnc2hvdy5icy5jb2xsYXBzZVwnLCBmdW5jdGlvbiAoZSkgew0KICAgICAgICAkKFwnI2FjY29yZGlvbiAucGFuZWwtaGVhZGluZy5hY3RpdmVcJykucmVtb3ZlQ2xhc3MoXCdhY3RpdmVcJykuZmluZChcJy5nbHlwaGljb25cJykudG9nZ2xlQ2xhc3MoXCdnbHlwaGljb24tcGx1cyBnbHlwaGljb24tbWludXNcJyk7DQogICAgICAgICQoZS50YXJnZXQpLnByZXYoKS5hZGRDbGFzcyhcJ2FjdGl2ZVwnKS5maW5kKFwnLmdseXBoaWNvblwnKS50b2dnbGVDbGFzcyhcJ2dseXBoaWNvbi1wbHVzIGdseXBoaWNvbi1taW51c1wnKTsNCiAgICB9KQ0KfSk7DQo8L3NjcmlwdD4iO3M6MTU6InRoZW1lX2FuYWx5dGljcyI7czowOiIiO3M6MjA6InRvZ2dsZV9oNWJwX2h0YWNjZXNzIjtzOjA6IiI7czoxODoidG9nZ2xlXzVnX2h0YWNjZXNzIjtzOjA6IiI7czoxNToiaW1hZ2Vfc2l6ZV8xMl93IjtzOjQ6IjEyMDAiO3M6MTU6ImltYWdlX3NpemVfMTJfaCI7czozOiI2NDAiO3M6MTQ6ImltYWdlX3NpemVfNl93IjtzOjM6IjU3MCI7czoxNDoiaW1hZ2Vfc2l6ZV82X2giO3M6MzoiMzIwIjtzOjE0OiJpbWFnZV9zaXplXzRfdyI7czozOiIzODAiO3M6MTQ6ImltYWdlX3NpemVfNF9oIjtzOjM6IjIxNSI7czoxNDoiaW1hZ2Vfc2l6ZV8zX3ciO3M6MzoiMjg1IjtzOjE0OiJpbWFnZV9zaXplXzNfaCI7czozOiIxNjAiO3M6MTQ6ImltYWdlX3NpemVfMl93IjtzOjM6IjE5MCI7czoxNDoiaW1hZ2Vfc2l6ZV8yX2giO3M6MzoiMTEwIjtzOjIyOiJ0aGVtZV9tYWludGVuYW5jZV9wYWdlIjtzOjM6IjUxMyI7czoyMzoidG9nZ2xlX21haW50ZW5hbmNlX21vZGUiO3M6MDoiIjtzOjExOiJ3b29fY29sdW1ucyI7czoxOiIzIjtzOjIxOiJ3b29fcHJvZHVjdHNfcGVyX3BhZ2UiO3M6MToiOSI7czoxOToid29vX3JlbGF0ZWRfY29sdW1ucyI7czoxOiI0IjtzOjI5OiJ3b29fcmVsYXRlZF9wcm9kdWN0c19wZXJfcGFnZSI7czoxOiI0IjtzOjI2OiJ0b2dnbGVfZGVzaWduX3Rvb2xiYXJfbWVudSI7czoxOiIxIjt9czo0OiJsb2dvIjthOjM6e3M6MTQ6InRvZ2dsZV90YWdsaW5lIjtzOjM6Im9mZiI7czo5OiJsb2dvX3R5cGUiO3M6Mzoib2ZmIjtzOjk6ImxvZ29fcGF0aCI7czowOiIiO31zOjU6InRpdGxlIjthOjI6e3M6MTc6InRvZ2dsZV9wYWdlX3RpdGxlIjtzOjE6IjEiO3M6MTI6InRpdGxlX2hlYWRlciI7czoyOiJoMSI7fXM6MTE6ImJyZWFkY3J1bWJzIjthOjQ6e3M6MTg6InRvZ2dsZV9icmVhZGNydW1icyI7czoxOiIxIjtzOjExOiJicmVhZF9kZWxpbSI7czoyOiLCuyI7czoxNzoidG9nZ2xlX2JyZWFkX2hvbWUiO3M6MToiMSI7czoxNToiYnJlYWRfaG9tZV90ZXh0IjtzOjQ6IkhvbWUiO31zOjQ6ImJsb2ciO2E6MTk6e3M6MTQ6InJlYWRfbW9yZV90ZXh0IjtzOjUyOiJSZWFkIE1vcmUgPGkgY2xhc3M9ImZhIGZhLWNoZXZyb24tY2lyY2xlLXJpZ2h0Ij48L2k+IjtzOjE0OiJleGNlcnB0X2xlbmd0aCI7czoyOiI1NSI7czoxNzoidG9nZ2xlX2Z1bGxfcG9zdHMiO3M6MDoiIjtzOjIxOiJ0b2dnbGVfZmVhdHVyZWRfaW1hZ2UiO3M6MToiMSI7czoyNToidG9nZ2xlX3Bvc3RfZm9ybWF0X2J1dHRvbiI7czoxOiIxIjtzOjIyOiJ0b2dnbGVfcG9zdF9hdXRob3JfYm94IjtzOjE6IjEiO3M6MjE6InRvZ2dsZV9wb3N0X21ldGFfdGltZSI7czoxOiIxIjtzOjI3OiJ0b2dnbGVfcG9zdF9tZXRhX2NhdGVnb3JpZXMiO3M6MToiMSI7czoyMzoidG9nZ2xlX3Bvc3RfbWV0YV9hdXRob3IiO3M6MToiMSI7czoyMToidG9nZ2xlX3Bvc3RfbWV0YV90YWdzIjtzOjE6IjEiO3M6MjU6InRvZ2dsZV9wb3N0X21ldGFfY29tbWVudHMiO3M6MToiMSI7czoyODoidG9nZ2xlX3NpbmdsZV9mZWF0dXJlZF9pbWFnZSI7czoxOiIxIjtzOjE5OiJwb3N0X3RpdGxlX3Bvc2l0aW9uIjtzOjE6IjEiO3M6MTk6InJlbGF0ZWRfcG9zdHNfY291bnQiO3M6MToiNSI7czoyMDoidG9nZ2xlX3JlbGF0ZWRfcG9zdHMiO3M6MToiMSI7czoyNjoidG9nZ2xlX3BhZ2VfZmVhdHVyZWRfaW1hZ2UiO3M6MToiMSI7czoyMjoidG9nZ2xlX3NpbmdsZV9jb21tZW50cyI7czoxOiIxIjtzOjIwOiJ0b2dnbGVfcGFnZV9jb21tZW50cyI7czoxOiIwIjtzOjI1OiJ0b2dnbGVfcG9ydGZvbGlvX2NvbW1lbnRzIjtzOjE6IjAiO31zOjg6ImxpZ2h0Ym94IjthOjQ6e3M6MTU6InRvZ2dsZV9saWdodGJveCI7czowOiIiO3M6MjM6ImxpZ2h0Ym94X2FuaW1hdGlvbl90eXBlIjtzOjc6ImRlZmF1bHQiO3M6Mjc6ImxpZ2h0Ym94X2FuaW1hdGlvbl9kdXJhdGlvbiI7czowOiIiO3M6MTM6ImxpZ2h0Ym94X3NpemUiO3M6MDoiIjt9czoxMDoiYW5pbWF0aW9ucyI7YTo1OntzOjE3OiJ0b2dnbGVfYW5pbWF0aW9ucyI7czoxOiIxIjtzOjE2OiJmZWF0X2ltZ19hbmltYXRlIjtzOjE6IjEiO3M6MjY6ImRlZmF1bHRfYW5pbWF0aW9uX2R1cmF0aW9uIjtzOjA6IiI7czoyMzoiZmVhdF9pbWdfYW5pbWF0aW9uX3R5cGUiO3M6NjoiZmFkZUluIjtzOjI3OiJmZWF0X2ltZ19hbmltYXRpb25fZHVyYXRpb24iO3M6MDoiIjt9czoxMjoic29jaWFsX21lZGlhIjthOjU5OntzOjE5OiJ0b2dnbGVfc29jaWFsX3NoYXJlIjtzOjE6IjEiO3M6Mjc6InNvY2lhbF9zaGFyZV9hbmltYXRpb25fdHlwZSI7czo2OiJmYWRlSW4iO3M6MzE6InNvY2lhbF9zaGFyZV9hbmltYXRpb25fZHVyYXRpb24iO3M6MDoiIjtzOjE4OiJzb2NpYWxfc2hhcmVfdGl0bGUiO3M6NToiU2hhcmUiO3M6Mjc6InNvY2lhbF9zaGFyZV90d2l0dGVyX2hhbmRsZSI7czowOiIiO3M6MjM6InNvY2lhbF9tZWRpYWdvb2dsZV9wbHVzIjtzOjA6IiI7czoyMDoic29jaWFsX21lZGlhZmFjZWJvb2siO3M6MDoiIjtzOjE5OiJzb2NpYWxfbWVkaWF0d2l0dGVyIjtzOjA6IiI7czoyMDoic29jaWFsX21lZGlhbGlua2VkaW4iO3M6MDoiIjtzOjE3OiJzb2NpYWxfbWVkaWFza3lwZSI7czowOiIiO3M6MTg6InNvY2lhbF9tZWRpYWdpdGh1YiI7czowOiIiO3M6MjE6InNvY2lhbF9tZWRpYXBpbnRlcmVzdCI7czowOiIiO3M6MjE6InNvY2lhbF9tZWRpYWluc3RhZ3JhbSI7czowOiIiO3M6MjA6InNvY2lhbF9tZWRpYWRyaWJiYmxlIjtzOjA6IiI7czoyMjoic29jaWFsX21lZGlhZm91cnNxdWFyZSI7czowOiIiO3M6MTg6InNvY2lhbF9tZWRpYXRyZWxsbyI7czowOiIiO3M6MTg6InNvY2lhbF9tZWRpYWZsaWNrciI7czowOiIiO3M6MTk6InNvY2lhbF9tZWRpYXlvdXR1YmUiO3M6MDoiIjtzOjE1OiJzb2NpYWxfbWVkaWFyc3MiO3M6MDoiIjtzOjI0OiJ0b2dnbGVfZ29vZ2xlX3BsdXNfc2hhcmUiO3M6MToiMSI7czoyMToidG9nZ2xlX2ZhY2Vib29rX3NoYXJlIjtzOjE6IjEiO3M6MjA6InRvZ2dsZV90d2l0dGVyX3NoYXJlIjtzOjE6IjEiO3M6MjE6InRvZ2dsZV9saW5rZWRpbl9zaGFyZSI7czoxOiIxIjtzOjIyOiJ0b2dnbGVfcGludGVyZXN0X3NoYXJlIjtzOjE6IjAiO3M6MTk6InRvZ2dsZV90dW1ibHJfc2hhcmUiO3M6MToiMCI7czoxNToidG9nZ2xlX3ZrX3NoYXJlIjtzOjE6IjAiO3M6MTk6InRvZ2dsZV9yZWRkaXRfc2hhcmUiO3M6MToiMCI7czoyMToidG9nZ2xlX2VudmVsb3BlX3NoYXJlIjtzOjE6IjAiO3M6MjQ6InNvY2lhbF9tZWRpYV9nb29nbGVfcGx1cyI7czoxOiIjIjtzOjIxOiJzb2NpYWxfbWVkaWFfZmFjZWJvb2siO3M6MToiIyI7czoyMDoic29jaWFsX21lZGlhX3R3aXR0ZXIiO3M6MToiIyI7czoyMToic29jaWFsX21lZGlhX2xpbmtlZGluIjtzOjE6IiMiO3M6MTg6InNvY2lhbF9tZWRpYV9za3lwZSI7czowOiIiO3M6MTk6InNvY2lhbF9tZWRpYV9naXRodWIiO3M6MDoiIjtzOjIyOiJzb2NpYWxfbWVkaWFfcGludGVyZXN0IjtzOjA6IiI7czoyMjoic29jaWFsX21lZGlhX2luc3RhZ3JhbSI7czowOiIiO3M6MjE6InNvY2lhbF9tZWRpYV9kcmliYmJsZSI7czowOiIiO3M6MjM6InNvY2lhbF9tZWRpYV9mb3Vyc3F1YXJlIjtzOjA6IiI7czoxOToic29jaWFsX21lZGlhX3RyZWxsbyI7czowOiIiO3M6MTk6InNvY2lhbF9tZWRpYV9mbGlja3IiO3M6MDoiIjtzOjIwOiJzb2NpYWxfbWVkaWFfeW91dHViZSI7czowOiIiO3M6MTY6InNvY2lhbF9tZWRpYV9yc3MiO3M6MDoiIjtzOjIwOiJzb2NpYWxfbWVkaWFfYmVoYW5jZSI7czowOiIiO3M6MjA6InNvY2lhbF9tZWRpYV9jb2RlcGVuIjtzOjA6IiI7czoxNzoic29jaWFsX21lZGlhX2RpZ2ciO3M6MDoiIjtzOjIwOiJzb2NpYWxfbWVkaWFfZHJvcGJveCI7czowOiIiO3M6MTk6InNvY2lhbF9tZWRpYV9naXR0aXAiO3M6MDoiIjtzOjE5OiJzb2NpYWxfbWVkaWFfcmVkZGl0IjtzOjA6IiI7czoxOToic29jaWFsX21lZGlhX3JlbnJlbiI7czowOiIiO3M6MjY6InNvY2lhbF9tZWRpYV9zdGFja2V4Y2hhbmdlIjtzOjA6IiI7czoyMzoic29jaWFsX21lZGlhX3NvdW5kY2xvdWQiO3M6MDoiIjtzOjIwOiJzb2NpYWxfbWVkaWFfc3BvdGlmeSI7czowOiIiO3M6MTk6InNvY2lhbF9tZWRpYV90dW1ibHIiO3M6MDoiIjtzOjI1OiJzb2NpYWxfbWVkaWFfdmltZW9fc3F1YXJlIjtzOjA6IiI7czoxNToic29jaWFsX21lZGlhX3ZrIjtzOjA6IiI7czoxODoic29jaWFsX21lZGlhX3dlaWJvIjtzOjA6IiI7czoxNzoic29jaWFsX21lZGlhX3hpbmciO3M6MDoiIjtzOjE3OiJzb2NpYWxfbWVkaWFfeWVscCI7czowOiIiO3M6MjE6InNvY2lhbF9zaGFyZV9wb3NpdGlvbiI7czo1OiJiZWxvdyI7fXM6MTQ6ImZhdnRvdWNoX2ljb25zIjthOjU6e3M6NzoiZmF2aWNvbiI7czowOiIiO3M6MTk6ImFwcGxlX3RvdWNoX2ljb25fNTciO3M6MDoiIjtzOjE5OiJhcHBsZV90b3VjaF9pY29uXzcyIjtzOjA6IiI7czoyMDoiYXBwbGVfdG91Y2hfaWNvbl8xMTQiO3M6MDoiIjtzOjIwOiJhcHBsZV90b3VjaF9pY29uXzE0NCI7czowOiIiO31zOjY6ImxheW91dCI7YTo2OntzOjEwOiJwYWdlX3dpZHRoIjtzOjQ6IndpZGUiO3M6MTI6InRoZW1lX2xheW91dCI7czoxMToiY29sX3JpZ2h0XzIiO3M6MTM6InBhZ2Vfc2lkZWJhcnMiO3M6MToiMyI7czoxMToiY3VzdG9tX2xlZnQiO3M6NToiMjAwcHgiO3M6MTM6ImN1c3RvbV9jZW50ZXIiO3M6NToiNTQwcHgiO3M6MTI6ImN1c3RvbV9yaWdodCI7czo1OiIyMDBweCI7fXM6NjoiZm9vdGVyIjthOjQ6e3M6MTM6InRvZ2dsZV9mb290ZXIiO3M6MToiMSI7czoyMDoidG9nZ2xlX2N1c3RvbV9mb290ZXIiO2k6MTtzOjE2OiJjb3B5cmlnaHRfZm9vdGVyIjtzOjc4OiJDb3B5cmlnaHQgJmNvcHk7IFtzZGZfY3VycmVudF95ZWFyXSBTRU8gRGVzaWduIEZyYW1ld29yay4gQWxsIHJpZ2h0cyByZXNlcnZlZC4iO3M6MjM6ImZvb3Rlcl9ib3R0b21fYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7fX0=';
	return $core_theme_options;
}

public function mergeSettings($_sdfGlobalArgs, &$_currentSettings, $diff){
				
	foreach($diff as $key => $val){
	
		if(!isset($_currentSettings[$key])){
			$_currentSettings[$key] = $_sdfGlobalArgs[$key];
		}
		elseif(is_array($diff[$key]) && ($key != 'theme_presets')){
			$_currentSettings[$key] = $this->mergeSettings($_sdfGlobalArgs[$key], $_currentSettings[$key], $diff[$key]);
		}
		
	}
	
	return $_currentSettings;
}

/**
 * $a1 array your static array.
 * $a2 array array we want to test.
 * @return array difference between arrays. empty result means $a1 and $a2 has the same structure.
 */ 
function array_diff_key_recursive($a1, $a2)
{
		$r = array();

		foreach ($a1 as $k => $v)
		{
				if (is_array($v))
				{
						if (!isset($a2[$k]) || !is_array($a2[$k]))
						{
								$r[$k] = $a1[$k];
						}
						else
						{
								if ($diff = $this->array_diff_key_recursive($a1[$k], $a2[$k]))
								{
										$r[$k] = $diff;
								}
						}
				}
				else
				{
						if (!isset($a2[$k]) || is_array($a2[$k]))
						{
								$r[$k] = $v;
						}
				}
		}

		return $r;
}
	
}//end class ULTIncludes
}
