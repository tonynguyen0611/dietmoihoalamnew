<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: ultimate.php
 * DESCRIPTIONS: Theme Global Controls.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: SDF_Options
 * Descripton: Implementation of Theme Global Options    
 */

add_action( 'edit_form_after_title', 'sdf_builder' );

// head&header calls
add_action('sdf_meta_title','sdf_do_meta_title');
add_action('sdf_head','sdf_do_head');
add_action('sdf_head', 'sdf_do_js_head');

add_action('sdf_before_header','sdf_do_before_header');
add_action('sdf_header','sdf_do_header');
add_action('sdf_after_header','sdf_do_after_header');

add_action('sdf_nav_before','sdf_do_nav_before');
add_action('sdf_nav','sdf_do_nav');
add_action('sdf_nav_sec','sdf_do_nav_sec');
add_action('sdf_nav_after','sdf_do_nav_after');
// Feature
add_action('sdf_before_feature','sdf_do_before_feature');
add_action('sdf_feature','sdf_do_feature');
add_action('sdf_after_feature','sdf_do_after_feature');

// Sidebar
add_action('sdf_before_sidebar_right','sdf_do_before_sidebar_right');
add_action('sdf_sidebar_right','sdf_do_sidebar_right');
add_action('sdf_after_sidebar_right','sdf_do_after_sidebar_right');

add_action('sdf_before_sidebar_left','sdf_do_before_sidebar_left');
add_action('sdf_sidebar_left','sdf_do_sidebar_left');
add_action('sdf_after_sidebar_left','sdf_do_after_sidebar_left');

// Content
add_action('sdf_before_content','sdf_do_before_content');
add_action('sdf_the_content','sdf_do_the_content');
add_action('sdf_after_content','sdf_do_after_content');
// footer
add_action('sdf_before_footer','sdf_do_before_footer');
add_action('sdf_footer','sdf_do_footer');
add_action('sdf_after_footer','sdf_do_after_footer');
// Layout
add_action('sdf_before_feature','sdf_do_quarterd_before');
add_action('sdf_after_content','sdf_do_quarterd_after');

if ( !class_exists('SDF_Options') ) {  
	class SDF_Options
	{
		public $_wpuPage;
		public $_wpuGlobal;
		public $_sdfPageArgs;
		public $_sdfGlobalArgs;
		//Constructor
		public function __construct() 
		{ 				
			// Init Global
			$this->getGlobal();	
			// Setup Global Args
			$this->_sdfGlobalArgs = $this->_wpuGlobal->_currentSettings;
			if (!empty($this->_sdfGlobalArgs) && is_array($this->_sdfGlobalArgs)) {
				// setup Page Args
				$this->getPage($this->_sdfGlobalArgs);
				add_action('wp',array(&$this, 'setupPageArgs'));
			}
					
		}
		function setupPageArgs(){
			global $post;
				if ( is_singular() ) {
					$postid = (isset($post->ID)) ? $post->ID : false;
					
					if($postid) {
						$this->_sdfPageArgs = get_post_meta( $postid, SDF_META_KEY, true );
					}
				}
				elseif( is_home() && get_option( 'page_for_posts' ) ) { 
					$page_ID = get_option( 'page_for_posts' ); 
					$this->_sdfPageArgs = get_post_meta( $page_ID, SDF_META_KEY, true );
				}	
				elseif(is_woocommerce_active() && is_shop()){
					global $woocommerce;
					// woocommerce page id
					$is_woocommerce=false;
					if(function_exists("is_woocommerce")) {
						$is_woocommerce = is_woocommerce();
						if($is_woocommerce){
							$page_ID = get_option('woocommerce_shop_page_id');
						}
					}
					$this->_sdfPageArgs = get_post_meta( $page_ID, SDF_META_KEY, true );
				}				
		}
		public function & getPage($args)
		{                                 
			if($this->_wpuPage === null){ $this->_wpuPage = new SDF_Page($args); }
			return $this->_wpuPage;
		} // getPage
		public function & getGlobal()
		{                                 
			if($this->_wpuGlobal === null){ $this->_wpuGlobal = new SDF_Global(); }
			return $this->_wpuGlobal;
		} // getGlobal
		
		function hasPageLevel($setting){	
			if(!isset($setting)  || $setting === "" || $setting == 'global'){
				return false;
			}else{
				return true;
			}
		}
		function sdf_global($grp){
			return $this->_sdfGlobalArgs[$grp];
		}
		function sdf_global_check($grp, $opt){
			if($this->_sdfPageArgs[$grp][$opt] == 'global'):
				return true;
			else:
				return false;
			endif;
		}
			
		function sdf_page($grp){
			return $this->_sdfPageArgs[$grp];
		}
		function sdf_get_global($grp, $opt){
			if (isset($this->_sdfGlobalArgs[$grp][$opt])):
				$global_option = (!empty($this->_sdfGlobalArgs[$grp][$opt])) ? $this->_sdfGlobalArgs[$grp][$opt] : '';
				if($global_option != ''):
					return $global_option;
				else:
					return false;
				endif;
			else:
				return false;
			endif;
		}
		function sdf_get_page($grp, $opt){
			if (isset($this->_sdfPageArgs[$grp][$opt])):	
				$page_option = $this->_sdfPageArgs[$grp][$opt];
				if($this->hasPageLevel($page_option)):
					return $page_option;
				else:
					return false;
				endif;
			else:
				return false;
			endif;
		}
		function sdf_get_option($grp, $opt){
			if(isset($this->_sdfPageArgs[$grp][$opt])): 
				$page_toggle = $this->_sdfPageArgs[$grp][$opt];
					if($this->hasPageLevel($page_toggle)):
							return $this->_sdfPageArgs[$grp][$opt];
					else:
						if(isset($this->_sdfGlobalArgs[$grp][$opt])):
							$global_toggle = $this->_sdfGlobalArgs[$grp][$opt];
								return $this->_sdfGlobalArgs[$grp][$opt];
						endif;
					endif;
			elseif(isset($this->_sdfGlobalArgs[$grp][$opt])):
				$global_toggle = $this->_sdfGlobalArgs[$grp][$opt];
					return $this->_sdfGlobalArgs[$grp][$opt];
			endif;
		}

		function sdf_is_permissible($grp, $opt){
			if(isset($this->_sdfPageArgs[$grp][$opt])): 
				$page_toggle = $this->_sdfPageArgs[$grp][$opt];
					if($this->hasPageLevel($page_toggle)):
						if(intval($page_toggle) == 1):
							return true;
						else:
							return false;
						endif;
					else:
						if(isset($this->_sdfGlobalArgs[$grp][$opt])):
							$global_toggle = $this->_sdfGlobalArgs[$grp][$opt];
							if(intval($global_toggle) == 1):
								return true;
							else:
								return false;
							endif;
						endif;
					endif;
			elseif(isset($this->_sdfGlobalArgs[$grp][$opt])):
				$global_toggle = $this->_sdfGlobalArgs[$grp][$opt];
				if(intval($global_toggle) == 1):
					return true;
				else:
					return false;
				endif;
			endif;
		}
		// header methods Begins
	} // end CLASS SDF_Options
}

/**
 * @return SDF Options instance
 */
function sdfGo() {
	static $SDF = null;

	if ($SDF === null) {
		$SDF = new SDF_Options();
	}

	return $SDF;
}

sdfGo();

function sdf_encode($value) {

  $fnctn = 'base6' . '4_encode';
  return $fnctn($value);
  
}

function sdf_decode($value) {

  $fnctn = 'base6' . '4_decode';
  return $fnctn($value);
  
}
function prepare_toggle($post_var){

	$tmpPostVar = $post_var;
	if(!$tmpPostVar):
		$tmpPostVar = 0;
	endif;

	return $tmpPostVar;
}
function dropdown( $id, $dropArray, $value ) {
	echo "<select name=\"".$id."\" id=\"".$id."\">";
	foreach ($dropArray as $key => $dropItem){
		echo "<option value=\"".$key."\"";
		if($value == $key) echo " selected=\"yes\"";
		echo ">".$dropItem."</option>";
	}
	echo "</select>";
}

function sdf_builder() { 
    include( SDF_ADMIN . '/sdf_builder.php'); 
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
