<?php  
/* 
Plugin Name: WP Ultimate Silo Builder
Plugin URI: http://www.SiloBlogBuilder.com
Description: Plugin for importing and creating theme silo structures within WordPress. Advanced Silo works with Domain Web Studios.
Version: 1.6.3
Author: Mary Cadwell and Network Empire
Author URI: http://www.networkempire.com
*/  

define('WPUS_THEME_NAME', 'Silo');
define('WPUS_PLUGIN_OPTIONS','_wpu_silo_builder');
define('WPUS_PLUGIN_STYLES','_wpu_silo_styles');
define('WPUS_TXT_DOMAIN', SDF_TXT_DOMAIN);
define('WPUS_THEME_SUBMITTED_FIELD', 'wpu_silo_submitted');
define('WPUS_PLUGIN_URL', SDF_SILO_URL); 
define('WPUS_PLUGIN_DIR', SDF_SILO);

require_once(WPUS_PLUGIN_DIR.'/includes/wpu_category_order.php');
require_once(WPUS_PLUGIN_DIR.'/includes/ultimate_silo.php');

?>