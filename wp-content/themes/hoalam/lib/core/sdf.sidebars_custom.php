<?php

/**
 *
 * SDF Sidebar
 * Class for adding custom sidebars
 * 
 */

if( ! class_exists( 'sdf_sidebar' ) ){
	
	class sdf_sidebar{
	
		var $sidebars  = array();
		var $stored    = "";
	    
		// load needed stuff on widget page
		function __construct(){
			$this->stored	= 'sdf_sidebarss';
			$this->title = __('Create Unlimited Sidebars','sdf');
		    
			add_action('load-widgets.php', array(&$this, 'load_assets') , 5 );
			add_action('widgets_init', array(&$this, 'register_custom_sidebars') , 1000 );
			add_action('wp_ajax_sdf_ajax_delete_custom_sidebar', array(&$this, 'delete_sidebar_area') , 1000 );
		}
		
		//load css, js and add hooks to the widget page
		function load_assets(){
			add_action('admin_print_scripts', array(&$this, 'template_add_widget_field') );
			add_action('load-widgets.php', array(&$this, 'add_sidebar_area'), 100);
			
			wp_enqueue_script('sdf_sidebars' , SDF_JS . '/sdf.sidebars_custom.js');  
			wp_enqueue_style( 'sdf_sidebars' , SDF_CSS . '/sdf.sidebars_custom.css');
		}
		
		//widget form template
		function template_add_widget_field(){
			$nonce =  wp_create_nonce ('sdf-delete-sidebar');
			$nonce = '<input type="hidden" name="sdf-delete-sidebar" value="'.$nonce.'" />';

			echo "\n<script type='text/html' id='sdf-add-widget'>";
			echo "\n  <div id='widgets-right'><div class='sidebars-column-1'>";
			echo "\n  <div class='widgets-holder-wrap sdf-add-widget'>";
			echo "\n  <form method='POST'>";
			echo "\n  <h3>". $this->title ."</h3> *For Use With SDF Builder";
			echo "\n    <p><input class='widefat' type='text' value='' placeholder = '".__('New Sidebar Name')."' name='sdf-add-widget' /></p>";
			echo "\n    <p><input class='button button-primary' type='submit' value='".__('Create Custom Sidebar')."' /></p>";
			echo "\n    ".$nonce;
			echo "\n  </form>";
			echo "\n  </div>";
			echo "\n  </div></div>";
			echo "\n</script>\n";
		}

		//add sidebar area to the db
		function add_sidebar_area(){
			if(!empty($_POST['sdf-add-widget'])){
				$this->sidebars = get_option($this->stored);
				$name = $this->get_name($_POST['sdf-add-widget']);
				$name = stripslashes(str_replace(array("\n","\r","\t"),'', $name));

				if(empty($this->sidebars)){
					$this->sidebars = array($name);
				}
				else{
					$this->sidebars = array_unique( array_merge( $this->sidebars, array($name) ) );
				}

				update_option($this->stored, $this->sidebars);
				wp_redirect( admin_url('widgets.php') );
				die();
			}
		}
		
		//delete sidebar area from the db
		function delete_sidebar_area(){
			check_ajax_referer('sdf-delete-sidebar');

			if(!empty($_POST['name'])){
				$name = stripslashes(str_replace(array("\n","\r","\t"),'', $_POST['name']));
				$this->sidebars = get_option($this->stored);

				if(($key = array_search($name, $this->sidebars)) !== false){
					unset($this->sidebars[$key]);
					update_option($this->stored, $this->sidebars);
					echo "sidebar-deleted";
				}
			}

			die();
		}
		
		
		//checks the user submitted name and makes sure that there are no collisions
		function get_name($name){
			if(empty($GLOBALS['wp_registered_sidebars'])) return $name;

			$taken = array();
			foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ){
				$taken[] = $sidebar['name'];
			}

			if(empty($this->sidebars)) $this->sidebars = array();
			$taken = array_merge($taken, $this->sidebars);

			if(in_array($name, $taken)){
				$counter  = substr($name, -1);  
				$new_name = "";

				if(!is_numeric($counter)){
					$new_name = $name . " 1";
				}
				else{
					$new_name = substr($name, 0, -1) . ((int) $counter + 1);
				}

				$name = $this->get_name($new_name);
			}

			return $name;
		}
		
		
		
		//register custom sidebar areas
		function register_custom_sidebars(){
		
			if(empty($this->sidebars)) $this->sidebars = get_option($this->stored);

			$args = array(
			'before_widget'  =>   '<div id="%1$s" class="widget clearfix %2$s">',
			'after_widget'   =>   '</div>',
			'before_title'   =>   '<h3 class="title widgettitle">',
			'after_title'    =>   '</h3>'
			);
				
			$args = apply_filters('sdf_custom_widget_args', $args);

			if(is_array($this->sidebars)){
				foreach ($this->sidebars as $sidebar){	
					$args['name'] = $sidebar;
					$args['id'] = sanitize_key($sidebar);
					$args['class'] = 'sdf-custom';
					register_sidebar($args);
				}
			}
		}		
	}
}
