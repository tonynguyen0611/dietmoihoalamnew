<?php
add_action('admin_head', 'sdf_add_mce_button');
function sdf_add_mce_button() {
	// check user permissions
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		if(sdfGo()->sdf_get_option('lightbox', 'toggle_lightbox') && get_post_type() != 'ads'):
			add_filter('tiny_mce_before_init', 'init_lightbox_pages' );
		endif;
		add_filter('mce_external_plugins', 'sdf_add_mce_plugin');
		add_filter('mce_buttons', 'sdf_register_mce_button');
	}
}
// Register new button in the editor
function sdf_register_mce_button($buttons) {
   if(sdfGo()->sdf_get_option('lightbox', 'toggle_lightbox')):
     array_push($buttons, "sdf_tinymce_lightbox");
   endif;
   return $buttons;
}
// Declare script for new button
function sdf_add_mce_plugin($plugin_array) {
   if(sdfGo()->sdf_get_option('lightbox', 'toggle_lightbox')):
     $plugin_array['sdf_tinymce_lightbox'] = get_bloginfo('template_url').'/lib/js/sdf.tinymce.lightbox.js';
   endif;
   return $plugin_array;
}
function init_lightbox_pages($in) {
  $ads = get_posts(array('post_type' => 'ads'));
  $pages_hash = array();
  foreach($ads as $ad){
    $pages_hash[$ad->post_title] = $ad->ID;
  }
  $in['lightbox_pages'] = json_encode($pages_hash);
  return $in;
}

