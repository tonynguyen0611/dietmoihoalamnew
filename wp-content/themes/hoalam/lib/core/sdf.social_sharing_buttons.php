<?php
if(!class_exists('sdf_social_sharing_buttons'))
{
	class sdf_social_sharing_buttons {
	
		var $options;
		var $output  = "";
		var $socialShareLinks = array();
		var $counter = 0;

		function __construct($options = array()){
			
			$this->options['title'] = (sdfGo()->sdf_get_global('social_media','social_share_title')) ? sdfGo()->sdf_get_global('social_media','social_share_title') : '';
			$this->options['title'] = esc_attr($this->options['title']);
			$animation_type = (sdfGo()->sdf_get_global('social_media','social_share_animation_type')) ? sdfGo()->sdf_get_global('social_media','social_share_animation_type') : '';
			$animation_duration = (sdfGo()->sdf_get_global('social_media','social_share_animation_duration')) ? sdfGo()->sdf_get_global('social_media','social_share_animation_duration') : '';
			$this->options['data_animation'] = '';
			$this->options['data_duration'] = '';
			$this->options['classes'] = '';
			$this->options['data_animation'] = ( sdf_is_not($animation_type, 'No') ) ? ' data-animation="'.$animation_type.'"' : '';
			$this->options['data_duration'] = ( sdf_is_not($animation_duration, '') && $this->options['data_animation'] != '') ? ' data-duration="'.$animation_duration.'"' : '';
			$this->options['classes'] .= ($this->options['data_animation'] != '') ? ' viewport_animate animate_now' : '';
			$this->options['twitter_handle'] = (sdfGo()->sdf_get_global('social_media','social_share_twitter_handle')) ? sdfGo()->sdf_get_global('social_media','social_share_twitter_handle') : '';
			// pulled from style options
			$this->options['icon_size'] = (sdfGo()->sdf_get_global('typography','theme_sharing_button_icon_size')) ? sdfGo()->sdf_get_global('typography','theme_sharing_button_icon_size') : '';
			
			$page_ID = sdf_page_holder_ID();
			$share_title = get_the_title( $page_ID );
			$share_permalink = get_permalink( $page_ID );
			
			$the_post_by_id = get_post($page_ID);
			$text = $the_post_by_id->post_content;
			// remove widget shortcode to avoid infinite loop from share icons widget
			$text = preg_replace ('/\[sdf_widget[^\]]*\](.*)\[\/sdf_widget\]/', '', $text);
			$text = preg_replace ('/\[rev_slider[^\]]*\]/', '', $text);
			$raw_excerpt = $text;
			$text = apply_filters('the_content', $text);
			$text = str_replace(']]>', ']]&gt;', $text);
			$text = strip_tags($text);
			$excerpt_length = apply_filters('excerpt_length', sdfGo()->sdf_get_global('blog','excerpt_length'));
			$text = wp_trim_words( $text, $excerpt_length ); //since wp3.3
			$share_excerpt = apply_filters('wp_trim_excerpt', $text, $raw_excerpt); //since wp3.3
			
			$share_thumbnail = '';
			if ( has_post_thumbnail()) {
				$share_thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id( $page_ID ));
				$share_thumbnail = $share_thumbnail[0];
			}
					
			$this->socialShareLinks = array(
				'google-plus' => 'https://plus.google.com/share?url='.$share_permalink,
				'facebook' => 'https://www.facebook.com/sharer/sharer.php?u='.$share_permalink.'&amp;t='.$share_title,
				'twitter' => 'https://twitter.com/intent/tweet?text='.$share_title.'&amp;url='.$share_permalink.'&amp;via='.$this->options['twitter_handle'],
				'linkedin' => 'https://www.linkedin.com/shareArticle?mini=true&amp;url='.$share_permalink.'&amp;title='.$share_title.'&amp;summary='.$share_excerpt,
				'pinterest' => 'https://pinterest.com/pin/create/button/?url='.$share_permalink.'&amp;description='.$share_excerpt.'&amp;media='.$share_thumbnail,
				'tumblr' => 'http://www.tumblr.com/share/link?url='.$share_permalink.'&amp;name='.$share_title.'&amp;description='.$share_excerpt,
				'vk' => 'https://vk.com/share.php?url='.$share_permalink.'&amp;title='.$share_title.'&amp;description='.$share_excerpt.'&amp;image='.$share_thumbnail.'&amp;noparse=true',
				'reddit' 	=> 'http://reddit.com/submit?url='.$share_permalink.'&amp;title='.$share_title,
				'envelope' 		=> 'mailto:?subject='.$share_title.'&amp;body='.$share_permalink,
				);
		}
		
		function html_output(){
			$this->output = '<div class="share-icons">';
			if ( !empty($this->options['title']) ) {
				$this->output .= '<h5 class="media-heading">'.$this->options['title'].'</h5>';
			}
				
			$this->output .= '<ul id="" class="social-menu sharing'.$this->options['classes'].'" '.$this->options['data_animation'].$this->options['data_duration'].'>';
			foreach ( sdfGo()->_wpuGlobal->_sdfSocialSharingMedia as $soc_slug => $soc_name) {
				$id = str_replace( "-", "_", urlencode(strtolower($soc_slug)) );
				$toggle_icon = sdfGo()->sdf_get_global('social_media', 'toggle_'.$id.'_share');
				if ( sdf_is_not($toggle_icon, '0') ) {
					$soc_link = $this->socialShareLinks[$soc_slug];
					$icon_text = sdfGo()->sdf_get_global('typography','theme_'.$id.'_sharing_button_custom_text');
					$toggle_icon = sdfGo()->sdf_get_global('typography','theme_sharing_button_icon_toggle');
					$soc_icon = ($toggle_icon) ? '<i class="fa fa-'.$soc_slug.' '.$this->options['icon_size'].'"></i>' : '';
					$button_style = ' sharing-custom sharing-'.$soc_slug;
				
					$this->output .= '<li class="sharing-icon"><a class="'.$button_style.' social-tooltip" data-original-title="'.$soc_name.'" data-placement="top" data-toggle="tooltip" href="'.$soc_link.'" target="_blank" rel="nofollow">'.$soc_icon.' '.$icon_text.'</a></li>';
				}
			}
			$this->output .= '</ul></div>';
			return $this->output;
		}

	} // End Class
}

if(!function_exists('sdf_social_sharing_buttons'))
{
	function sdf_social_sharing_buttons($echo = true)
	{
		$sharing_buttons = new sdf_social_sharing_buttons();
		
		if($echo){	
			echo $sharing_buttons->html_output();
		}
		else{
			return $sharing_buttons->html_output();
		}
	}
}
