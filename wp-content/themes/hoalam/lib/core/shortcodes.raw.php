<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: shortcodes.raw.php
 * DESCRIPTIONS: Raw Shortcodes
 *
 * AUTHOR:  SEO Design Framework
 */ 
 
global $sdf_raw_chunks;

$sdf_raw_chunks=array();

/**
 * Extract content from raw shortcodes 
 * 
 * @global array $sdf_raw_chunks Used to store the extracted content.
 * 
 * @param string $raw_text The input content to filter.
 * @param bool $keep_tags Store both the tagged content and the tags themselves. Defaults to false - storing only the content. 
 * @return string Filtered content.
 */
function sdf_get_raws($raw_text, $keep_tags = false){
	global $sdf_raw_chunks, $wp_current_filter;
	//Note to self: The regexp version was much shorter, but it had problems with big posts.
	$tags = array(
								array('/\[sdf_raw_html[^\]]*id="([^"]*)"[^\]]*\]/i', '/\[\/sdf_raw_html\]/i'), 
								array('/\[sdf_raw_js[^\]]*id="([^"]*)"[^\]]*\]/i', '/\[\/sdf_raw_js\]/i'), 
								array('/\[sdf_pre_code[^\]]*id="([^"]*)"[^\]]*\]/i', '/\[\/sdf_pre_code\]/i'), 
								array('/\[sdf_short[^\]]*id="([^"]*)"[^\]]*\]/i', '/\[\/sdf_short\]/i'), 
	);

	foreach ($tags as $tag_pair){
		list($start_regex, $fin_regex) = $tag_pair;
		
		//Find the start tag
		//$start = stripos($raw_text, $start_tag, 0);
		$find_start = preg_match($start_regex, $raw_text, $matches, PREG_OFFSET_CAPTURE, 0);
		$start = ($find_start) ? $matches[0][1] : false;
		$start_tag = ($find_start) ? $matches[0][0] : '';
		
		while($start !== false){
			$content_start = $start + strlen($start_tag);
			
			if($fin_regex != ''){
				//find the end tag
				//$fin = stripos($raw_text, $fin_tag, $content_start);
				$find_end = preg_match($fin_regex, $raw_text, $matches, PREG_OFFSET_CAPTURE, $content_start);
				$fin = ($find_end) ? $matches[0][1] : false;
				$fin_tag = ($find_end) ? $matches[0][0] : '';
				
				//break if there's no end tag
				if ($fin == false) break;
				
				//extract the content between the tags
				$content = substr($raw_text, $content_start,$fin-$content_start);
				if(!is_admin()){
					if($fin_tag == '[/sdf_pre_code]'){
						$content = htmlentities( trim( $content ), ENT_NOQUOTES, 'UTF-8', false );
					}
					$content = do_shortcode($start_tag.$content.$fin_tag);
				}
			}
			else{
				if(is_admin()){
					$content = $start_tag;
				}
				else{
					$content = do_shortcode($start_tag);
				}
				$fin = $content_start;
				$fin_tag = '';
			}
			
			if ( (array_search('get_the_excerpt', $wp_current_filter) !== false) || (array_search('the_excerpt', $wp_current_filter) !== false) ){
				//Strip out the raw blocks when displaying an excerpt
				$replacement = '';
			} else {
				//Store the content and replace it with a marker
				if ( $keep_tags ){
					$sdf_raw_chunks[] = $start_tag . $content . $fin_tag;
				} else {
					$sdf_raw_chunks[] = $content;
				}				
				$replacement = "!SDFRAWEL".(count($sdf_raw_chunks)-1)."!";
			}

			$raw_text = substr_replace( $raw_text, $replacement, $start, $fin+strlen($fin_tag)-$start );

			//Have we reached the end of the string yet?
			if ($start + strlen($replacement) > strlen($raw_text)) break;
			
			//Find the next start tag
			//$start = stripos($raw_text, $start_tag, $start + strlen($replacement));
			$find_start = preg_match($start_regex, $raw_text, $matches, PREG_OFFSET_CAPTURE, $start + strlen($replacement));
			$start = ($find_start) ? $matches[0][1] : false;
			$start_tag = ($find_start) ? $matches[0][0] : '';
		}
	}
	return $raw_text;
}

/**
 * Replace the placeholders created by sdf_get_raws() with the original content.
 *
 * @global array $sdf_raw_chunks Used to check if there is anything to restore.
 *
 * @param string $raw_text The input content to filter.
 * @param callable|string $placeholder_callback Optional. The callback that will be used to process each placeholder.
 * @return string Filtered content.
 */
function sdf_restore_raws($raw_text, $placeholder_callback = 'sdf_restore_callback'){
	global $sdf_raw_chunks;
	if(!isset($sdf_raw_chunks)) return $raw_text;
	return preg_replace_callback('/(<p>)?!SDFRAWEL(?P<index>\d+?)!(\s*?<\/p>)?/', $placeholder_callback, $raw_text);
}

/**
 * Get the original content associated with a placeholder.
 *
 * @param array $matches Regex matches for a specific placeholder. @see sdf_restore_raws()
 * @return string Original content.
 */
function sdf_get_block_from_matches($matches) {
	global $sdf_raw_chunks;

	if ( isset($matches['index']) ) {
		$index = $matches['index'];
	} else if ( isset($matches[2]) ) {
		$index = $matches[2];
	} else {
		return '{Invalid RAW block}';
	}

	$index = intval($index);
	return $sdf_raw_chunks[$index];
}

/**
 * Regex callback for sdf_restore_raws. Returns the extracted content 
 * corresponding to a matched placeholder.
 * 
 * @param array $matches Regex matches.
 * @return string Replacement string for this match.
 */
function sdf_restore_callback($matches){
	$openingParagraph = isset($matches[1]) ? $matches[1] : '';
	$closingParagraph = isset($matches[3]) ? $matches[3] : '';
	$restore_code = sdf_get_block_from_matches($matches);

	//If the [raw] block is wrapped in its own paragraph, strip the <p>...</p> tags. If there's
	//only one of <p>|</p> tag present, keep it - it's probably part of a larger paragraph.
	if ( empty($openingParagraph) || empty($closingParagraph) ) {
		$restore_code = $openingParagraph . $restore_code . $closingParagraph;
	}
	return $restore_code;
}

function sdf_setup_content_filters() {
	//Extract the tagged content before WP can get to it, then re-restore it later.
	add_filter('the_content', 'sdf_get_raws', 2);

	//A workaround for WP-Syntax. If we run our restore callback at the normal, extra-late
	//priority, WP-Syntax will see the wrong content when it runs its own content substitution hook.
	//We adapt to that by running our callback slightly earlier than WP-Syntax's.
	$wp_syntax_priority = has_filter('the_content', 'wp_syntax_after_filter');
	if ( $wp_syntax_priority === false && class_exists('WP_Syntax') ) {
		//Newer versions of WP-Syntax use a class with static methods instead of plain functions.
		$wp_syntax_priority = has_filter('the_content', array('WP_Syntax', 'afterFilter'));
	}
	if ( $wp_syntax_priority !== false ) {
		$rawhtml_priority = $wp_syntax_priority - 2;
	} else {
		$rawhtml_priority = 11;
	}
	add_filter('the_content', 'sdf_restore_raws', $rawhtml_priority);
}
add_action('init', 'sdf_setup_content_filters', 11);

/* 
 * Override the the_editor_content filter on the post/page editor.
 */
  
function sdf_get_raws_for_editor($raw_text){
	return sdf_get_raws($raw_text, true);
}

function sdf_restore_raws_for_editor($raw_text){
	return sdf_restore_raws($raw_text, 'sdf_restore_callback_for_editor');
}

function sdf_restore_callback_for_editor($matches){
	$restore_code = sdf_get_block_from_matches($matches);
	return htmlspecialchars($restore_code, ENT_NOQUOTES);
}

add_filter('the_editor_content', 'sdf_get_raws_for_editor', 2);
add_filter('the_editor_content', 'sdf_restore_raws_for_editor', 1001);
