<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: sfd.cpt.php
 * DESCRIPTIONS: Ads CPT
 * @package wpultimate
 *
 * AUTHOR:  SEO Design Framework
 */ 
 
add_action('init', 'ads_register');
function ads_register() {
    
	// Create The Labels (Output) For The Post Type
	$labels = 
	array(
		// The plural form of the name of your post type.
		'name' => __( 'Ads', SDF_TXT_DOMAIN), 
		'all_items' => __( 'All Ads', SDF_TXT_DOMAIN), 
		// The singular form of the name of your post type.
		'singular_name' => __('Ad', SDF_TXT_DOMAIN),
			
		// The menu item for adding a new post.
		'add_new' => _x( 'Add New', 'ads', SDF_TXT_DOMAIN),
		'add_new_item' => __( 'Add New Advertisement', SDF_TXT_DOMAIN),
		
		// The header shown when editing a post.
		'edit_item' => __('Edit Ads Item', SDF_TXT_DOMAIN),
		
		// Shown in the favourites menu in the admin header.
		'new_item' => __('New Ads Item', SDF_TXT_DOMAIN), 
		
		// Shown alongside the permalink on the edit post screen.
		'view_item' => __('View Ads', SDF_TXT_DOMAIN),
		
		// Button text for the search box on the edit posts screen.
		'search_items' => __('Search Ads', SDF_TXT_DOMAIN), 
		
		// Text to display when no posts are found through search in the admin.
		'not_found' =>  __('No Ads Items Found', SDF_TXT_DOMAIN),
		
		// Text to display when no posts are in the trash.
		'not_found_in_trash' => __('No Ads Items Found In Trash', SDF_TXT_DOMAIN),
		 
		// Used as a label for a parent post on the edit posts screen. Only useful for hierarchical post types.
		'parent_item_colon' => '' 
	);
	
	$taxonomies = array();
 
	$supports = array( 'title', 'editor' );

	$post_type_args = array(
		'labels'            => $labels,
		'singular_label' => __('Ad', SDF_TXT_DOMAIN),
		'public'            => true,
		'show_ui'           => true,
		'menu_icon'         => 'dashicons-welcome-view-site',
		'publicly_queryable'=> true,
		'query_var'         => 'ads',
		'capability_type'   => 'post',
		'has_archive'       => false,
		'hierarchical'      => false,
		'rewrite'           => array('slug' => 'ads', 'with_front' => false ),
		'supports'          => $supports,
		'menu_position'     => 5,
		'taxonomies'        => $taxonomies
	 );
    register_post_type( 'ads' , $post_type_args );
}

$args = array(
	"hierarchical" => true, 
	"label" => "Ad Types", 
	"singular_label" => "Ad Type", 
	"rewrite" => array( 'slug' => 'ad-type', 'hierarchical' => true)
);
register_taxonomy( 'ad-type', array( 'ads' ), $args );

$parent_term = term_exists( 'ad-type', 'ad-type' );
$parent_term_id = $parent_term['term_id'];
wp_insert_term('Lightbox', 'ad-type', array( 'slug' => 'lightbox', 'parent'=> $parent_term_id ));
			
add_filter("manage_edit-ads_columns", "ad_edit_columns");
function ad_edit_columns($columns){
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Ad Title",
            "description" => "Description",
            "type" => "Type of Ad",
        );
        return $columns;
}
add_action("manage_ads_posts_custom_column",  "ad_custom_columns");
function ad_custom_columns($column){
        global $post, $wpdb, $wpalch_item_media;
        switch ($column)
        {
            case "description":
                the_excerpt();
                break;
            case "type":
                echo get_the_term_list($post->ID, 'ad-type', '', ', ','');
                break;
        }
}

if ( ! function_exists( 'get_the_ads_category_by_tax' ) ) {
	function get_the_ads_category_by_tax( $id = false, $tcat = 'ad-type' ) {
		$categories = get_the_terms( $id, $tcat );
		if ( ! $categories )
			$categories = array();

		$categories = array_values( $categories );

		foreach ( array_keys( $categories ) as $key ) {
			_make_cat_compat( $categories[$key] );
		}

		return apply_filters( 'get_the_categories', $categories );
	}
} // End IF Statement