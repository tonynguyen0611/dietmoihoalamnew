<?php
/**
* THEME NAME: SEO Design Framework 
* VERSION: 1.0
*
* TYPE: header.php
* DESCRIPTIONS: Theme Header Options.
*
* AUTHOR:  SEO Design Framework
*
* Name: ULT Header
* Descripton: Implementation of Theme Header Options    
*/
function sdf_do_before_header(){
	echo apply_filters('sdf_page_wrap_before','<header id="sdf-header" role="banner" itemscope="itemscope" itemtype="https://schema.org/WPHeader">');
}
function sdf_do_after_header(){	
	echo apply_filters('sdf_container_before','</header>');
}
function sdf_get_logo_image(){
	if (sdfGo()->sdf_get_global('logo', 'logo_type') != 'off') {
		$logo_img = sdfGo()->sdf_get_global('logo', 'logo_path');
		return ($logo_img != '') ? htmlentities($logo_img['url']) : false;
	} 
	else
		return false;
} // end sdf_get_logo_image

function sdf_get_logo($sidebar_id, $_smWidths, $box_data, $box_data_classes, $box_id){

	$theme_logo_heading = sdfGo()->sdf_get_global('typography', 'logo_heading');
	if (in_array($theme_logo_heading, array('', 'none'))) { 
		$theme_logo_heading = 'div';
	}
	
	$output = '<div'.$sidebar_id.' class="' . $box_data_classes[$box_id] . ' ' . $_smWidths[$box_data[$box_id]].' logo-lg"><div id="sdf-logo" class="row">';
	
	if ($logo_img = sdf_get_logo_image()) {
		$logo_img_class = (sdfGo()->sdf_get_global('typography', 'logo_align') == 'center') ? ' center-block' : ' text-'.sdfGo()->sdf_get_global('typography', 'logo_align');
		$output .= '<div class="logo col-md-12"><a href="'.home_url( '/' ).'" title="'.esc_html( get_bloginfo('name'), 1 ).'" rel="home"><img src="'.$logo_img.'" class="img-responsive'.$logo_img_class.'" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" /></a></div>';
	} 
	else {
		$output .= '<'.$theme_logo_heading.' class="logo col-md-12"><a href="'.home_url( '/' ).'" title="'.esc_html( get_bloginfo('name'), 1 ).'" rel="home">'.get_bloginfo( 'name' ).'</a></'.$theme_logo_heading.'>';
	}
	
	if (sdfGo()->sdf_get_global('logo', 'toggle_tagline') == 'on') {
		if ($tagln = get_bloginfo( 'description' )) {
			$tagline_heading = (sdfGo()->sdf_get_global('typography', 'logo_tagline_heading')) ? sdfGo()->sdf_get_global('typography', 'logo_tagline_heading') : 'p';
			$output .= '<'.$tagline_heading.' class="tagline col-md-12">'.$tagln.'</'.$tagline_heading.'>';   
		}
	}
	$output .= '</div></div>';
	
	return $output;

} // end sdf_get_logo

function sdf_get_header_box($sidebar_id, $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row, $secondary_nav){
	$output = '';
	if($box_id):
		switch($box_id){
			case "listItem_1":
				$output .= sdf_do_header_area('header-area-1', 'header-area-1 widget_area clearfix', $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row);
			break;
			case "listItem_2":
				$output .= sdf_do_header_area('header-area-2', 'header-area-2 widget_area clearfix', $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row);
			break;
			case "listItem_3": 
				$output .= sdf_do_header_area('header-area-3', 'header-area-3 widget_area clearfix', $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row); 				
			break;
			case "listItem_4":
				$output .= sdf_do_header_area('header-area-4', 'header-area-4 widget_area clearfix', $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row);
			break;
			case "listItem_5":
				$output .= sdf_get_logo($sidebar_id, $_smWidths, $box_data, $box_data_classes, $box_id);				
			break;
			case "listItem_6":
				$output .= '<div'.$sidebar_id.' class="' . $box_data_classes[$box_id] . ' ' . $_smWidths[$box_data[$box_id]] . '"><div class="row">';
				ob_start();
				sdf_nav($secondary_nav);
				$output .= ob_get_clean();
				$output .= '</div></div>';
			break;
			case "listItem_7":	
				$output .= sdf_do_header_area('slider-widget-area', 'slider_block_area', $_smWidths, $box_data, $box_data_classes, $box_id);
			break;
			case "listItem_8":
				$output .= '<div'.$sidebar_id.' class="' . $box_data_classes[$box_id] . ' ' . $_smWidths[$box_data[$box_id]] . '"><div class="row">';
				ob_start();
				sdf_nav_sec();
				$output .= ob_get_clean();
				$output .= '</div></div>';
			break;
			default:
			break;	
		}	
	endif;
	return ($output) ? $output: false;
} // end sdf_get_header_box

function sdf_do_header_area($area_id, $section_clss, $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row = '') {

	$output = '';
	if((function_exists('dynamic_sidebar') && is_active_sidebar($area_id)) || sdfGo()->sdf_get_global('misc','toggle_placeholders')) {

		$widget_sidebars_content = array();
		ob_start();
		dynamic_sidebar( $area_id );
		array_push($widget_sidebars_content, ob_get_clean());

		if((strlen($widget_sidebars_content[0]) > 0) || sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
		
			$output .= '<div id="'.$area_id.$no_nav_row.'" class="' . $box_data_classes[$box_id] . ' ' . $_smWidths[$box_data[$box_id]].'">';
			$output .= '<section class="'.$section_clss.'">';
			
			$output .= $widget_sidebars_content[0];
			
			if(sdfGo()->sdf_get_global('misc','toggle_placeholders') && (strlen($widget_sidebars_content[0]) == 0)) {
				$output .= '<div class="col-md-12">';
				$output .= sdf_placeholder($area_id);
				$output .= '</div>';
			}
			
			$output .= '</section>';
			$output .= '</div>';
		}
	}
	else {
		if(sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
			$output .= '<div id="'.$area_id.$no_nav_row.'" class="' . $box_data_classes[$box_id] . ' ' . $_smWidths[$box_data[$box_id]].'"><div class="row">';
			$output .= '<section class="'.$section_clss.'">';
			
			$output .= '<div class="col-md-12">';
			$output .= sdf_placeholder($area_id);
			$output .= '</div>';
			
			$output .= '</section>';
			$output .= '</div></div>';
		}
	}
	return $output;
}

function sdf_do_header() {
	// Header Sort Order
	$header_order = (sdfGo()->sdf_get_page('header','toggle_custom_header') == '0') ? sdfGo()->sdf_get_global('header','header_order') : sdfGo()->sdf_get_page('header','header_order');
	
	if(is_array($header_order) && !empty($header_order))
	{
		// active box sizes
		$box_data = (sdfGo()->sdf_get_page('header','toggle_custom_header') == '0') ? sdfGo()->sdf_get_global('header','box_data') : sdfGo()->sdf_get_page('header','box_data');
		$box_data_classes = (sdfGo()->sdf_get_page('header','toggle_custom_header') == '0') ? sdfGo()->sdf_get_global('header','box_data_classes') : sdfGo()->sdf_get_page('header','box_data_classes');
	
		$_smWidths = sdfGo()->_wpuGlobal->_smWidths;
		$boxFract = array (
			'1/12' => 1/12,
			'1/6' => 1/6,
			'1/4' => 1/4,
			'1/3' => 1/3,
			'5/12' => 5/12,
			'1/2' => 1/2,
			'7/12' => 7/12,
			'2/3' => 2/3,
			'3/4' => 3/4,
			'5/6' => 5/6,
			'11/12' => 11/12,
			'1/1' => 1/1
		);
		$boxToSidebar = array(
							'listItem_1'=> 'header-area-1',
							'listItem_2'=> 'header-area-2',
							'listItem_3'=> 'header-area-3',
							'listItem_4'=> 'header-area-4',
							'listItem_5'=> 'logo-area',
							'listItem_6'=> 'navigation-area',
							'listItem_7'=> 'slider-widget-area',
							'listItem_8'=> 'navigation-area-2'
							);
		$line_width = 0;
		$line_count = 1;
		$header_struct = array ();
		$secondary_nav = false;
		foreach ($header_order as $id => $box_name) {
			$line_width += $boxFract[$box_data[$box_name]];
			if($line_width > 1) {
				$line_count++;
				$line_width = $boxFract[$box_data[$box_name]];
			}
			$header_struct[$line_count][] = $box_name;
		}
		
		if( in_array('listItem_8', $header_order) ) {
			$secondary_nav = true;
		}
		
		if(is_array($box_data) && !empty($box_data))
		{
			foreach ($header_struct as $row_struct) {
				
					$header_row_top = '';
					$navbar_layout = '';
					$navbar_layout_classes = sdfGo()->_wpuGlobal->_ultMenuLayouts['classes'];
					if( in_array('listItem_6', $row_struct) || in_array('listItem_8', $row_struct) ) {
						$navbar_layout .= ' '.$navbar_layout_classes[sdfGo()->sdf_get_global('navigation','menu_layout')];
					}
					
					$data_sticky_opacity = (sdfGo()->sdf_get_global('navigation','sticky_menu_opacity')) ? ' data-sticky-opacity="'.sdfGo()->sdf_get_global('navigation','sticky_menu_opacity').'"' : '';

					$area_row_wide = '';
					$no_nav_row = '';
					// slider coming
					if (($row_struct[0] == 'listItem_7') && ($box_data['listItem_7'] == '1/1')) {
							//$area_row_wide = ' id="'.$boxToSidebar[$row_struct[0]].'"';
							$area_row_wide = ' id="h-slider-area"';
						if(SDF_PAGE_WIDTH == "wide") {
							$header_row_top .= '<div'.$area_row_wide.' class="container-wide">';
						} else {
							$header_row_top .= '<div'.$area_row_wide.' class="container-fluid"><div class="row">';
						}
					} elseif ((($row_struct[0] == 'listItem_6') && ($box_data['listItem_6'] == '1/1')) || (($row_struct[0] == 'listItem_8') && ($box_data['listItem_8'] == '1/1'))) {
						// row 1/1 nav
						if(SDF_PAGE_WIDTH == "wide") {
							$area_row_wide = ' id="'.$boxToSidebar[$row_struct[0]].'"';
							$header_row_top .= '<div'.$area_row_wide.' class="container-wide'.$navbar_layout.'"'.$data_sticky_opacity.'><div class="container"><div class="row">';
						} else {
							$header_row_top .= '<div'.$area_row_wide.' class="container'.$navbar_layout.'"'.$data_sticky_opacity.'><div class="row">';
						}
					} else {
						// row contains nav
						$area_row_wide = ( in_array('listItem_6', $row_struct) || in_array('listItem_8', $row_struct) ) ? ' id="'.$boxToSidebar['listItem_6'].'"' : '';
						$data_sticky_opacity = ( in_array('listItem_6', $row_struct) || in_array('listItem_8', $row_struct) ) ? $data_sticky_opacity : '';
						// row w/o nav
						if( $box_data[$row_struct[0]] == '1/1' ) {
							$no_nav_row = '-inner';
							$area_row_wide = ' id="'.$boxToSidebar[$row_struct[0]].'"';
						}
						
						if(SDF_PAGE_WIDTH == "wide") {
							$header_row_top .= '<div'.$area_row_wide.' class="container-wide'.$navbar_layout.'"'.$data_sticky_opacity.'><div class="container"><div class="row">';
						} else {
							$header_row_top .= '<div'.$area_row_wide.' class="container'.$navbar_layout.'"'.$data_sticky_opacity.'><div class="row">';
						}
					}
					
					// get row content
					$header_row = '';
					foreach ($row_struct as $box_key => $box_id) {
					
						$sidebar_id = (array_key_exists($box_id, $boxToSidebar)) ? $boxToSidebar[$box_id] : $box_id;
						$sidebar_id = ($area_row_wide == '') ? ' id="'.$sidebar_id.'"' :  '';
						
						$header_row .= sdf_get_header_box($sidebar_id, $_smWidths, $box_data, $box_data_classes, $box_id, $no_nav_row, $secondary_nav);
					
					}
					
					$header_row_bottom = '';
					
					// closing fullwidth slider row
					if (($box_id == 'listItem_7') && ($box_data['listItem_7'] == '1/1')) {
						if(SDF_PAGE_WIDTH == "wide") {
							$header_row_bottom .= '</div>';
						} else {
							$header_row_bottom .= '</div></div>';
						}
					// closing fullwidth navigation row
					} elseif ((($box_id == 'listItem_6') && ($box_data['listItem_6'] == '1/1')) || (($box_id == 'listItem_8') && ($box_data['listItem_8'] == '1/1'))) {
						if(SDF_PAGE_WIDTH == "wide") {
							$header_row_bottom .= '</div></div></div>';
						} else {
							$header_row_bottom .= '</div></div>';
						}
					// else closing non-slider-navigation-fullwidth row
					} else {
						if(SDF_PAGE_WIDTH == "wide") {
							$header_row_bottom .= '</div></div></div>';
						} else {
							$header_row_bottom .= '</div></div>';
						}
					}

					if($header_row != '') {
						echo $header_row_top;
						echo $header_row;
						echo $header_row_bottom;
					}
			}
		}			
		
	}
} // end sdf_do_header

