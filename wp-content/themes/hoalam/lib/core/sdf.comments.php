<?php

function sdf_comments_list($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;

      $add_below = 'div-comment';
?>
		<li <?php comment_class(empty( $args['has_children'] ) ? 'media' : 'media parent') ?> id="comment-<?php comment_ID() ?>">
			<div id="comment-<?php comment_ID(); ?>" class="pull-left">
			<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
			</div>

			<div class="media-body">
			<div class="comment-author media-heading">
			<?php
				$author = '<cite class="comment_author_name">'.get_comment_author().'</cite>';
				$link = get_comment_author_url();
				if(!empty($link))
					$author = '<a href="'.$link.'">'.$author.'</a>';
				printf(__('<cite class="author_name">%s</cite> <span class="says">says:</span>'), $author); 
			?>
			</div>
			<?php if ($comment->comment_approved == '0') : ?>
					<div class="alert alert-info">
							<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
					</div>
			<?php endif; ?>

			<div class="comment-meta commentmetadata">
				<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
					<time datetime="<?php get_the_time('c'); ?>">
						<?php printf( __( '%1$s at %2$s', SDF_TXT_DOMAIN ), get_comment_date(),  get_comment_time()) ?>
					</time>
				</a>
				<?php edit_comment_link(__('(Edit)'),'  ','' ); ?>
			</div>

			<div class="comment-content">
			  <?php comment_text() ?>
			</div>
			<div class="reply btn btn-link">
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			</div>

			</div>
<?php
}

