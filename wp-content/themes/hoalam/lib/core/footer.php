<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: class_footer.php
 * DESCRIPTIONS: Theme Footer Options.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: ULTFooter
 * Descripton: Implementation of Theme Footer Options    
 */
function sdf_do_before_footer() {

}
function sdf_do_footer() {

	if(sdfGo()->sdf_is_permissible('footer', 'toggle_footer')){

			if(function_exists('dynamic_sidebar') && is_active_sidebar('footer-widget-area')){

				$widget_sidebars_content = array();
				ob_start();
				dynamic_sidebar( 'footer-widget-area' );
				array_push($widget_sidebars_content, ob_get_clean());

				if(strlen($widget_sidebars_content[0]) > 0 ) {
				
					if(SDF_PAGE_WIDTH == "wide") {
						echo '<div id="sdf-footer" class="container-wide"><div class="container">';
					} else {
						echo '<div id="sdf-footer" class="container-fluid">';
					}
	
					//parallax
					$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
					$parallax_footer = sdfGo()->sdf_get_option('typography', 'footer_widget_area_par_bg_ratio');
					$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_footer != '') ? ' data-stellar-background-ratio="'.$parallax_footer.'"' : '';
					$parallax = (sdfGo()->sdf_get_option('typography', 'footer_widget_area_custom_bg') == 'parallax') ? $parallax : '';
					
					$styled_widgets = (sdfGo()->sdf_get_option('typography', 'footer_widget_area_wg_custom_style') == 'yes') ? ' styled_widgets' : '';
					
					echo '<div class="row"><div id="footer-widget-area" class="default-widget-area col-md-12'.$styled_widgets.'"'.$parallax.'>';
					echo '<section class="footer-widget-area widget_area clearfix">';
					
					echo $widget_sidebars_content[0];
					sdf_placeholder('footer');
					
					echo '</section>';
					echo '</div></div>';
			
					if(SDF_PAGE_WIDTH == "wide") {
						echo '</div></div>';
					} else {
						echo '</div>';
					}
				}
				
			}
		
	}
}
function sdf_do_after_footer() {

}
function sdf_footer_bottom(){
	
	if(sdfGo()->sdf_get_global('footer','footer_bottom_alignment') == 'left'):
		$xtr_class = 'text-left';
	elseif(sdfGo()->sdf_get_global('footer','footer_bottom_alignment') == 'right'):
		$xtr_class = 'text-right';
	elseif(sdfGo()->sdf_get_global('footer','footer_bottom_alignment') == 'center'):
		$xtr_class = 'text-center';
	else:
		$xtr_class = '';
	endif;
	
	if(sdfGo()->sdf_is_permissible('footer', 'toggle_footer')):
			
		if(SDF_PAGE_WIDTH == "wide") {
			$copyright_before = '<footer id="sdf-copyright" class="container-wide" itemtype="https://schema.org/WPFooter" itemscope="itemscope" role="contentinfo"><div class="container">';
		} else {
			$copyright_before = '<footer id="sdf-copyright" class="container-fluid" itemtype="https://schema.org/WPFooter" itemscope="itemscope" role="contentinfo">';
		}
		echo apply_filters('sdf_copyright_before', $copyright_before);

		if(function_exists('dynamic_sidebar') && is_active_sidebar('footer-bottom-widget-area')){

			$widget_sidebars_content = array();
			ob_start();
			dynamic_sidebar( 'footer-bottom-widget-area' );
			array_push($widget_sidebars_content, ob_get_clean());

			if(strlen($widget_sidebars_content[0]) > 0 ) {

				//parallax
				$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
				$parallax_footer = sdfGo()->sdf_get_option('typography', 'footer_bottom_widget_area_par_bg_ratio');
				$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_footer != '') ? ' data-stellar-background-ratio="'.$parallax_footer.'"' : '';
				$parallax = (sdfGo()->sdf_get_option('typography', 'footer_bottom_widget_area_custom_bg') == 'parallax') ? $parallax : '';
				
				$styled_widgets = (sdfGo()->sdf_get_option('typography', 'footer_bottom_widget_area_wg_custom_style') == 'yes') ? ' styled_widgets' : '';
				
				echo '<div class="row" role="contentinfo"><div id="footer-bottom-widget-area" class="default-widget-area col-md-12'.$styled_widgets.'"'.$parallax.'>';
				echo '<section class="footer-bottom-widget-area widget_area clearfix">';
				
				echo $widget_sidebars_content[0];
				sdf_placeholder('footer-bottom');
				
				echo '</section>';
				echo '</div></div>';
			}
			
		}
		echo '<div class="row"><div class="col-md-12 '.$xtr_class.'">';
		echo do_shortcode(stripslashes(sdfGo()->sdf_get_option('footer', 'copyright_footer')));
		echo '</div></div>';
		
	
		if(SDF_PAGE_WIDTH == "wide") {
			$copyright_after = '</div></footer>';
		} else {
			$copyright_after = '</footer>';
		}
		echo apply_filters('sdf_copyright_after', $copyright_after);
	endif;	
}

function sdf_scroll_to_top(){
	
	$toggle_to_top_button = sdfGo()->sdf_get_global('misc', 'toggle_scrollto');
	if ( sdf_is_not($toggle_to_top_button, '0') ) {
		$to_top_text = sdfGo()->sdf_get_global('typography','theme_scroll_to_top_button_custom_text');
		$to_top_icon = sdfGo()->sdf_get_global('typography','theme_scroll_to_top_button_icon');
		$to_top_icon = ( sdf_is_not($to_top_icon, 'no') ) ? '<i class="fa '.$to_top_icon.' '.sdfGo()->sdf_get_global('typography','theme_scroll_to_top_button_icon_size').'"></i>' : '';
		
		if( $to_top_icon != '' || $to_top_text != '' ) {
			echo '<a href="#top" class="to-top-button hiding" id="finished-scroll" data-original-title="'.__( 'Scroll To Top', SDF_TXT_DOMAIN ).'" data-placement="top" data-toggle="tooltip">'.$to_top_icon.' '.$to_top_text.'</a>';
		}
	}
	
}

function sdf_footer_output(){
	
	echo apply_filters('sdf_misc_footer_before','<div id="sdf-extras-footer">');
	// theme html before body end

	if(sdfGo()->sdf_get_global('lightbox','toggle_lightbox')){
	
		// timed lightbox
		$timed_lightbox_page = array();
		$timed_lightbox_size = array();
		$timed_lightbox_animation = array();
		$timed_lightbox_seconds = array();
		$timed_lightbox_position = array();
		// fallback for single timed lightbox
		if (!is_array(sdfGo()->sdf_get_page('lightbox','timed_lightbox_page'))) {
			$timed_lightbox_page[] = sdfGo()->sdf_get_page('lightbox','timed_lightbox_page');
			$timed_lightbox_size[] = sdfGo()->sdf_get_page('lightbox','timed_lightbox_size');
			$timed_lightbox_animation[] = sdfGo()->sdf_get_page('lightbox','timed_lightbox_animation');
			$timed_lightbox_seconds[] = sdfGo()->sdf_get_page('lightbox','timed_lightbox_seconds');
			$timed_lightbox_position[] = sdfGo()->sdf_get_page('lightbox','timed_lightbox_position');
		}
		else{
			$timed_lightbox_page = sdfGo()->sdf_get_page('lightbox','timed_lightbox_page');
			$timed_lightbox_size = sdfGo()->sdf_get_page('lightbox','timed_lightbox_size');
			$timed_lightbox_animation = sdfGo()->sdf_get_page('lightbox','timed_lightbox_animation');
			$timed_lightbox_seconds = sdfGo()->sdf_get_page('lightbox','timed_lightbox_seconds');
			$timed_lightbox_position = sdfGo()->sdf_get_page('lightbox','timed_lightbox_position');
		}
		
		$modals_count = count($timed_lightbox_page);
							
		for($i=0; $i<$modals_count; $i++){

			$timed_lightbox_size[$i] = ($timed_lightbox_size[$i] == 'glob') ? sdfGo()->sdf_get_global('lightbox','lightbox_size') : $timed_lightbox_size[$i];
			$timed_lightbox_animation[$i] = ($timed_lightbox_animation[$i] == 'glob') ? sdfGo()->sdf_get_global('lightbox','lightbox_animation_type') : $timed_lightbox_animation[$i];
			$timed_lightbox_seconds[$i] = $timed_lightbox_seconds[$i] * 1000;

			if($timed_lightbox_page[$i]){
				echo do_shortcode('[sdf_lightbox page_id="'.$timed_lightbox_page[$i].'" auto_trigger_time="'. $timed_lightbox_seconds[$i] .'" modal_animation="'.$timed_lightbox_animation[$i].'" modal_size="'.$timed_lightbox_size[$i].'" modal_position="'.$timed_lightbox_position[$i].'"]');
			}
		}
		
		// bottom lightbox
		$bottom_lightbox_page = '';
		$bottom_lightbox_size = '';
		$bottom_lightbox_animation = '';
		$bottom_lightbox_position = '';
		$bottom_lightbox_page = sdfGo()->sdf_get_page('lightbox', 'bottom_lightbox_page');
		$bottom_lightbox_size = sdfGo()->sdf_get_page('lightbox', 'bottom_lightbox_size');
		$bottom_lightbox_animation = sdfGo()->sdf_get_page('lightbox', 'bottom_lightbox_animation');
		$bottom_lightbox_position = sdfGo()->sdf_get_page('lightbox', 'bottom_lightbox_position');

		$bottom_lightbox_size = ($bottom_lightbox_size == 'glob') ? sdfGo()->sdf_get_global('lightbox', 'lightbox_size') : $bottom_lightbox_size;
		$bottom_lightbox_animation = ($bottom_lightbox_animation == 'glob') ? sdfGo()->sdf_get_global('lightbox','lightbox_animation_type') : $bottom_lightbox_animation;

		if($bottom_lightbox_page){
				echo do_shortcode('[sdf_lightbox page_id="'.$bottom_lightbox_page.'" modal_bottom="yes" modal_position="'.$bottom_lightbox_position.'" modal_animation="'.$bottom_lightbox_animation.'" modal_size="'.$bottom_lightbox_size.'"]');
		}
	}
	
	//display page modals, timed and bottom modals
	$post_id = get_the_ID();
	if ( false === ( $modal_query_results = get_transient( 'modals_for_page_'. $post_id ) ) ) {
			$modal_query_results = array();
	}
	//show modal js/content
	foreach ( $modal_query_results as $modal_query_result ) {
		$modal_query_result = unserialize( sdf_decode( $modal_query_result ));
		echo $modal_query_result;
	}
	delete_transient( 'modals_for_page_'. $post_id );
	
	echo apply_filters('sdf_misc_footer_after','</div>');
	sdf_footer_bottom();
	sdf_scroll_to_top();
	
	// boxed wrap
	if(SDF_PAGE_WIDTH != "wide") {
		echo '</div></div>';
	}
	
	
	if(sdfGo()->sdf_get_global('misc', 'custom_js_footer')):
			echo do_shortcode(stripslashes( sdfGo()->sdf_get_global('misc', 'custom_js_footer') ));
	endif;
	if(sdfGo()->sdf_get_page('misc', 'custom_js_footer')):
			echo do_shortcode(stripslashes( sdfGo()->sdf_get_page('misc', 'custom_js_footer') ));
	endif;
	// theme analytics output
	if(sdfGo()->sdf_get_global('misc', 'theme_analytics') != ''):
			echo do_shortcode(stripslashes( sdfGo()->sdf_get_global('misc', 'theme_analytics') ));
	endif;
	
}
add_action('wp_footer','sdf_footer_output');