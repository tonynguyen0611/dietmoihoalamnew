<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: shortcodes.php
 * DESCRIPTIONS: Theme Shortcodes.
 *
 * AUTHOR:  SEO Design Framework
 */ 
 
/**
* Use shortcodes in text widgets & excerpts
*/

function parse_shortcode_content( $content ) {
	$array_fix = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	);
	$content = strtr($content, $array_fix);
	$content = trim( do_shortcode( shortcode_unautop( $content )));
	
	return $content;
}
 
class SDFShortcodes {

	private $_moduleWidths = array (
		'1/12' => 'col-md-1',
		'1/6' => 'col-md-2',
		'1/4' => 'col-md-3',
		'1/3' => 'col-md-4',
		'5/12' => 'col-md-5',
		'1/2' => 'col-md-6',
		'7/12' => 'col-md-7',
		'2/3' => 'col-md-8',
		'3/4' => 'col-md-9',
		'5/6' => 'col-md-10',
		'11/12' => 'col-md-11',
		'1/1' => 'col-md-12'
		);
	
	function __construct() {
		add_action( 'init', array( $this, 'sdf_add_shortcodes' ) ); 
		add_action( 'sdf_add_shortcodes', array( $this, 'sdf_add_shortcodes' ) );
		add_filter( 'the_content', array( $this, 'sdf_run_shortcode' ), 7 );
		add_filter( 'widget_text', array( $this, 'sdf_run_shortcode' ), 7 );
		add_filter( 'the_excerpt', array( $this, 'sdf_run_shortcode' ), 7 );
		add_filter( 'get_the_excerpt', array( $this, 'sdf_run_shortcode' ), 7 );
	}

   /*--------------------------------------------------------------------------------------
    *
    * sdf_add_shortcodes
    * 
    *-------------------------------------------------------------------------------------*/
	function sdf_add_shortcodes() {
		
		add_shortcode( 'sdf_badge', array( $this, 'badge_shortcode' ));
		add_shortcode( 'sdf_blank_spacer', array( $this, 'blank_spacer_shortcode' ));
		add_shortcode( 'sdf_blockquote', array( $this, 'blockquote_shortcode' ));
		add_shortcode( 'sdf_button', array( $this, 'button_shortcode' ));    
		add_shortcode( 'sdf_cta_button', array( $this, 'cta_button_shortcode' )); 
		add_shortcode( 'sdf_contact', array( $this, 'contact_shortcode' ));
		add_shortcode( 'sdf_col', array( $this, 'column_shortcode' ));
		add_shortcode( 'sdf_collapsibles', array( $this, 'collapsibles_shortcode' ));
		add_shortcode( 'sdf_collapse', array( $this, 'collapse_shortcode' ));
		add_shortcode( 'sdf_code', array( $this, 'code_shortcode' ));
		add_shortcode( 'sdf_counter', array( $this, 'counter_shortcode' ));
		add_shortcode( 'sdf_cta_box', array( $this, 'cta_box_shortcode' ));
		add_shortcode( 'sdf_current_year', array( $this, 'current_year_shortcode' ));
		add_shortcode( 'sdf_email', array( $this, 'hide_email_shortcode' ));
		add_shortcode( 'sdf_headline', array( $this, 'headline_shortcode' )); 
		add_shortcode( 'sdf_hero', array( $this, 'hero_shortcode' ));
		add_shortcode( 'sdf_icon', array( $this, 'icon_shortcode' ));
		add_shortcode( 'sdf_image', array( $this, 'image_shortcode' )); 
		add_shortcode( 'sdf_lightbox', array( $this, 'lightbox_shortcode' ));
		add_shortcode( 'sdf_latest_posts', array( $this, 'latest_posts_shortcode' ));
		add_shortcode( 'sdf_list', array( $this, 'list_shortcode' ));
		add_shortcode( 'sdf_list_item', array( $this, 'list_item_shortcode' ));
		add_shortcode( 'sdf_label', array( $this, 'label_shortcode' ));
		add_shortcode( 'sdf_message_box', array( $this, 'message_box_shortcode' ));
		add_shortcode( 'sdf_portfolio', array( $this, 'portfolio_shortcode' ));
		add_shortcode( 'sdf_pre_code', array( $this, 'pre_code_shortcode' ));
		add_shortcode( 'sdf_row',  array( $this, 'row_shortcode' ));
		add_shortcode( 'sdf_raw_html', array( $this, 'raw_html_shortcode' ));
		add_shortcode( 'sdf_raw_js', array( $this, 'raw_js_shortcode' ));
		add_shortcode( 'sdf_short', array( $this, 'theme_callback_shortcode' ));
		add_shortcode( 'sdf_social_icons', array( $this, 'social_follow_buttons_shortcode' ));
		add_shortcode( 'sdf_soundcloud', 'sdf_soundcloud_shortcode' );
		add_shortcode( 'sdf_separator', array( $this, 'separator_shortcode' ));
		add_shortcode( 'sdf_separator_with_text', array( $this, 'separator_with_text_shortcode' ));
		add_shortcode( 'sdf_tabs', array( $this, 'tabs_shortcode' ));
		add_shortcode( 'sdf_tab', array( $this, 'tab_shortcode' ));
		add_shortcode( 'sdf_text_list', array( $this, 'text_list_shortcode' ));
		add_shortcode( 'sdf_teaser_box', array( $this, 'teaser_box_shortcode' ));
		add_shortcode( 'sdf_team_member_box', array( $this, 'team_member_box_shortcode' ));
		add_shortcode( 'sdf_text_block', array( $this, 'text_block_shortcode' ));
		add_shortcode( 'sdf_tooltip', array( $this, 'tooltip_shortcode' ));
		add_shortcode( 'sdf_twitter', array( $this, 'twitter_shortcode' ));
		add_shortcode( 'sdf_video', array( $this, 'video_shortcode' ));
		add_shortcode( 'sdf_video_oembed', array( $this, 'video_oembed_shortcode' ));
		add_shortcode( 'sdf_widget', array( $this, 'widget_shortcode' ));
		add_shortcode( 'sdf_well', array( $this, 'well_shortcode' )); 

	}

	
	// Actual processing of the shortcode happens here
	function sdf_run_shortcode( $content ) {
		global $shortcode_tags;
		
		// reset to avoid filtering issues
		if ( isset($GLOBALS['row_done']) ){
			$GLOBALS['row_done'] = '';
		}

		// Backup current registered shortcodes and clear them all out
		$orig_shortcode_tags = $shortcode_tags;
		remove_all_shortcodes();
	 
		do_action( 'sdf_add_shortcodes' );
		
		// Do the shortcode (only the one above is registered)
		$content = parse_shortcode_content( $content );
	 
		// Put the original shortcodes back
		$shortcode_tags = $orig_shortcode_tags;
	 
		return $content;
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_row
	* 
	*-------------------------------------------------------------------------------------*/

	function row_shortcode($atts, $content = null) { 
		extract(shortcode_atts(array(
		"box_shadow_color" => '',
		"box_shadow" => '',
		"border_color" => '',
		"border_style" => '',
		"border_width" => '',
		"last" => '',
		"class" => ''
		), $atts));
		
		$row_border = '';
		if ($border_color && $border_style && $border_width) {
			$row_border = 'border-width:'.$border_width.';border-color:'.$border_color.';border-style:'.$border_style.';';
		}
		$row_box_shadow = '';
		if ($box_shadow_color && $box_shadow) {
			$row_box_shadow = '-webkit-box-shadow:'.$box_shadow.' '.$box_shadow_color.';box-shadow:'.$box_shadow.' '.$box_shadow_color.';';
		}
		
		$styles = ($row_box_shadow != '' || $row_border != '') ? ' style="'.$row_box_shadow.$row_border.'"' : '';

		if (!isset($GLOBALS['row_done'])) {
			$GLOBALS['row_done']= '';
		}
	  
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		$classes .= ($last == 'yes') ? ' last_row' : '';
		
		$row_cont = '';
		
		if (SDF_PAGE_WIDTH == 'wide') {
			if (sdf_class('main') == 'col-md-12 ') { 
				$row_cont .= ( $GLOBALS['row_done'] == 'hero' || $GLOBALS['row_done'] == '' ) ? '<div class="container">' : '';
				$row_cont .= '<div class="row'.$classes.'"'.$styles.'>' . parse_shortcode_content( $content ) . '</div>';
			} else { 
				$row_cont .= '<div class="row'.$classes.'"'.$styles.'>' . parse_shortcode_content( $content ) . '</div>';
			};
		}
		else {
			$row_cont .= '<div class="row'.$classes.'"'.$styles.'>' . parse_shortcode_content( $content ) . '</div>';
		}
		$GLOBALS['row_done'] = 'row';
		
		return $row_cont;
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_hero
	* 
	*-------------------------------------------------------------------------------------*/
	function hero_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		"fullwidth_hero" => '',
		"full_height_hero" => '',
		"background_type" => '',
		"background_color" => '',
		"background_color_alpha" => '',
		"background_image" => '',
		"background_repeat" => '',
		"parallax_background_ratio" => '',
		"youtube_background_video" => '',
		"mute_background_video" => '',
		"hero_height" => '',
		"padding_top" => '',
		"padding_bottom" => '',
		"last" => '',
		"box_shadow_color" => '',
		"box_shadow" => '',
		"border_color" => '',
		"border_style" => '',
		"border_width" => '',
		"margins" => '',
		"class" => ''
		), $atts));
		
		$fullwidth_hero = ($fullwidth_hero == 'yes') ? '-fluid' : '';
		$full_height_hero = ($full_height_hero == 'yes') ? ' full-page-area' : '';
		
		$hero_border = '';
		if ($border_color && $border_style && $border_width) {
			$hero_border = 'border-width:'.$border_width.';border-color:'.$border_color.';border-style:'.$border_style.';';
		}
		$hero_box_shadow = '';
		if ($box_shadow_color && $box_shadow) {
			$hero_box_shadow = '-webkit-box-shadow:'.$box_shadow.' '.$box_shadow_color.';box-shadow:'.$box_shadow.' '.$box_shadow_color.';';
		}
		
		$hero_margin = '';
		if ($margins) {
			$hero_margin = 'margin:' . $margins . ';';
		}
		
		if (!isset($GLOBALS['row_done'])) {
			$GLOBALS['row_done']= '';
		}
		
		$mute_background_video = ($mute_background_video == 'yes') ? 'true' : 'false';
		$hero_height = ($hero_height != '') ? 'height:'.$hero_height.';' : '';
		$padding_top = ($padding_top != '') ? 'padding-top:'.$padding_top.';' : '';
		$padding_bottom = ($padding_bottom != '') ? 'padding-bottom:'.$padding_bottom.';' : '';
		
		$background = '';
		$parallax = '';
		$background_video = '';
		$background_player_class = '';
		
		$background_color_alpha = ($background_color_alpha != '') ? $background_color_alpha : '1';
		$background_color = ($background_color != '') ? 'rgba(' . sdf_hex2RGB ($background_color, true) . ',' . $background_color_alpha . ')' : '';
		
		// fallback
		if($background_type == '' && ($background_color != '' || $background_image != '')) $background_type = 'image';
		
		if ( $background_type == 'image') {
			$background .= get_background_color_image($background_color, $background_image, $background_repeat, false);
		}
		elseif ( $background_type == 'parallax') {
			//parallax
			$parallax .= ($parallax_background_ratio != '') ? ' data-stellar-background-ratio="'.$parallax_background_ratio.'"' : '';
			
			$background_color = ($background_color != '') ? 'background-color:'.$background_color.';' : '';
			$background_image = ($background_image != '') ? 'background-image:url('.$background_image.');' : '';
			$background .= $background_image.$background_color;
		}
		elseif ( $background_type == 'video' && $youtube_background_video != '') {
		$background_player_class = ' yt-bg-video player';
		$background_video = ' data-property="{videoURL:\''.$youtube_background_video.'\',containment:\'self\',autoPlay:true, quality:\'highres\', mute:'.$mute_background_video.', startAt:0, opacity:1}"';
		$background .= get_background_color_image($background_color, $background_image, $background_repeat, false);
		}
		
		$hero_style = '';
		if($background != '' || $hero_height != '' || $padding_top != '' || $padding_bottom != '') {
			$hero_style = $background . $hero_height . $padding_top . $padding_bottom;
		}
		
		$styles = ($hero_margin != '' || $hero_box_shadow != '' || $hero_border != '' || $hero_style != '') ? ' style="'.strip_tags($hero_margin.$hero_box_shadow.$hero_border.$hero_style).'"' : '';
		
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		$classes .= ($last == 'yes') ? ' last_row' : '';
		$classes .= ($background_type == 'parallax') ? ' sdf-parallax' : '';
		$classes .= $background_player_class;
		$classes .= $full_height_hero;
		
		$hero_cont = '';
		
		if (SDF_PAGE_WIDTH == 'wide') { 
			if (sdf_class('main') == 'col-md-12 ') { 
				$hero_cont .= ( $GLOBALS['row_done'] == 'row' ) ? '</div>' : '';
				$hero_cont .= '<div class="hero_block '.$classes.'"'.$styles.$background_video.$parallax.'><div class="container'.$fullwidth_hero.'"><div class="row">' . parse_shortcode_content( $content ) . '</div></div></div>';
				//$hero_cont .= ($last == 'yes') ? '' : '</div>';
			} else { 
				$hero_cont .= '<div class="hero_block row'.$classes.'"'.$styles.$background_video.$parallax.'>' . parse_shortcode_content( $content ) . '</div>';
			};
		}
		else {
			$hero_cont .= '<div class="hero_block row'.$classes.'"'.$styles.$background_video.$parallax.'>' . parse_shortcode_content( $content ) . '</div>';
		}
		$GLOBALS['row_done'] = 'hero';
		
	return $hero_cont;
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_col
	* 
	*-------------------------------------------------------------------------------------*/

	function column_shortcode( $atts, $content = null ) {
	  extract(shortcode_atts(array(
		"background_type" => '',
		"background_color" => '',
		"background_color_alpha" => '',
		"background_image" => '',
		"background_repeat" => '',
		"parallax_background_ratio" => '',
		"youtube_background_video" => '',
		"mute_background_video" => '',
		"col_height" => '',
		"padding_top" => '',
		"padding_bottom" => '',
		"box_shadow_color" => '',
		"box_shadow" => '',
		"border_color" => '',
		"border_style" => '',
		"border_width" => '',
		"border_radius" => '',
		"class" => ''
		), $atts));
		
		$col_border = '';
		if ($border_color && $border_style && $border_width) {
			$col_border .= 'border-width:'.$border_width.';border-color:'.$border_color.';border-style:'.$border_style.';';
		}
		if ($border_radius) {
			$col_border .= 'border-radius:'.$border_radius.';';
		}
		$col_box_shadow = '';
		if ($box_shadow_color && $box_shadow) {
			$col_box_shadow = '-webkit-box-shadow:'.$box_shadow.' '.$box_shadow_color.';box-shadow:'.$box_shadow.' '.$box_shadow_color.';';
		}
		
		$mute_background_video = ($mute_background_video == 'no') ? 'false' : 'true';
		$col_height = ($col_height != '') ? 'height:'.$col_height.';' : '';
		$padding_top = ($padding_top != '') ? 'padding-top:'.$padding_top.';' : '';
		$padding_bottom = ($padding_bottom != '') ? 'padding-bottom:'.$padding_bottom.';' : '';
		
		$background_cover = '-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;';
		$background = '';
		$parallax = '';
		$background_video = '';
		$background_player_class = '';
		
		$background_color_alpha = ($background_color_alpha != '') ? $background_color_alpha : '1';
		$background_color = ($background_color != '') ? 'rgba(' . sdf_hex2RGB ($background_color, true) . ',' . $background_color_alpha . ')' : '';
		
		if($background_type == '' && ($background_color != '' || $background_image != '')) $background_type = 'image';
		
		if ( $background_type == 'image') {
			$background .= get_background_color_image($background_color, $background_image, $background_repeat, false);
		}
		elseif ( $background_type == 'parallax') {
			//parallax
			$parallax .= ($parallax_background_ratio != '') ? ' data-stellar-background-ratio="'.$parallax_background_ratio.'"' : '';
			
			$background_color = ($background_color != '') ? 'background-color:'.$background_color.';' : '';
			$background_image = ($background_image != '') ? 'background-image:url('.$background_image.');' : '';
			$background .= $background_image.$background_color.
			'background-attachment: fixed;background-position: 50% 0;background-repeat: no-repeat;position: relative;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;';
		}
		elseif ( $background_type == 'video') {
		$background_player_class = ' yt-bg-video player';
		$background_video = ' data-property="{videoURL:\''.$youtube_background_video.'\',containment:\'self\',autoPlay:true, quality:\'highres\', mute:'.$mute_background_video.', startAt:0, opacity:1}"';
		}
		
		$col_style = '';
		if($background != '' || $col_height != '' || $padding_top != '' || $padding_bottom != '') {
			$col_style = $background . $col_height . $padding_top . $padding_bottom;
		}
		
		$styles = ($col_box_shadow != '' || $col_border != '' || $col_style != '') ? ' style="'.strip_tags($col_box_shadow.$col_border.$col_style).'"' : '';
		
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		$classes .= $background_player_class;
		
		if($atts['width'] == '1/2'):
		$col_width = 'col-md-6';
	  elseif($atts['width'] == '1/3'):
		$col_width = 'col-md-4';
	  elseif($atts['width'] == '5/12'):
		$col_width = 'col-md-5';
	  elseif($atts['width'] == '1/4'):
		$col_width = 'col-md-3';
	  elseif($atts['width'] == '1/6'):
		$col_width = 'col-md-2';
	  elseif($atts['width'] == '2/3' || $atts['width'] == '4/6'):
		$col_width = 'col-md-8';
	  elseif($atts['width'] == '7/12'):
		$col_width = 'col-md-7';
	  elseif($atts['width'] == '3/4'):
		$col_width = 'col-md-9';
	  elseif($atts['width'] == '5/6'):
		$col_width = 'col-md-10';
	  else:
		$col_width = 'col-md-12';
	  endif;
		
		return '<div class="' . $col_width . $classes . '"'.$styles.'><div class="row">' . parse_shortcode_content($content) . '</div></div>'; 
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_text_block
	* 
	*-------------------------------------------------------------------------------------*/
	function text_block_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"text_alignment" => '',
		"max_width" => '',
		"font_size" => '',
		"line_height" => '',
		"font_family" => '',
		"text_color" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"link_title" => '',
		"link" => '',
		"target" => '',
		"nofollow" => '',
		"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$classes = '';
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		if($text_alignment != '') $classes .= ' text-'.$text_alignment;
		$classes .= ($class != '') ? ' '.$class : '';
		 
	  $data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		$classes = ($classes != '') ? ' class="'.$classes.'"' : '';
		
		$font_size = ($font_size) ? (( strpos($font_size, 'px') === false ) ? 'font-size:' . $font_size . 'px;' : 'font-size:' . $font_size . ';' ) : '';
		$line_height = ($line_height) ? 'line-height:' . $line_height . ';' : '';
		$text_color = ($text_color) ? 'color:' . $text_color . ';'  : '';
		$max_width = ($max_width) ? 'max-width:' . $max_width . ';margin-left:auto; margin-right:auto;'  : '';
		
		// extract font weight
		$def_font_weight = '';
		$sdf_font_weight = '';
		$sdf_font_family = '';
		
		if($font_family){
			$font_args = explode(':', $font_family);
			$sdf_font_family = $font_args[0];
			$sdf_font_weight = (count($font_args) > 1) ? $font_args[1] : $def_font_weight;
		}

		$sdf_font_family = ($sdf_font_family) ? 'font-family:' . $sdf_font_family . ';'  : '';
		$sdf_font_weight = ($sdf_font_weight) ? 'font-weight:' . $sdf_font_weight . ';'  : '';
	
		$styles = ( ($top_margin != '') || ($bottom_margin != '') || ($font_size != '') || ($line_height != '') || ($sdf_font_family != '') || ($text_color != '') || ($max_width != '') ) ? ' style="' . $top_margin . $bottom_margin . $font_size . $line_height . $sdf_font_family . $sdf_font_weight . $text_color . $max_width . '"' : '';
	  
	  if($link_title != ''){$link_title = ' title="'.$link_title.'"';}
	  //fallback values
		$target = (!in_array($target, array('', 'no', 'None'))) ? ' target="'.$target.'"' : '';
	  $link_nofollow = ($nofollow == 'yes') ? ' rel="nofollow"' : '';
		
	  if($link == ''){	
			return '<div class="' . $module_class . '"><div' . $classes . $data_animation. $data_duration . $styles .'>' . parse_shortcode_content( $content ) . '</div></div>';
	  }
	  else {
	    $link = sdf_check_link_prefix($link);
			return '<a href="' . $link . '" class="' . $module_class . '"' . $link_title . $target . $link_nofollow .'><div' . $classes . $data_animation. $data_duration . $styles .'>' . parse_shortcode_content( $content ) . '</div></a>';
	  }
	}
	
	
  
  /*--------------------------------------------------------------------------------------
    *
    * sdf_headline
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
    function headline_shortcode( $atts, $content = null) {
		extract( shortcode_atts( array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"title" => '',
		"title_heading" => '',
		"font_family" => '',
		"font_size" => '',
		"line_height" => '',
		"letter_spacing" => '',
		"text_color" => '',
		"text_alignment" => '',
		"rotate_words" => '',
		"rotator_delay" => '',
		"rotator_separator" => '',
		"fit_text" => '',
		"fit_compressor" => '',
		"fit_min_font_size" => '',
		"fit_max_font_size" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"class" => ''
		  ), $atts ) );
			
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$headline_id = '';
		$headline_id_str = '';
		$headline_script = '';
		$rotate_script = '';
		$rotate_arr = array();
		$json_str = '';
		$data_animation = '';
		$data_duration = '';
		$classes = '';
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$h_class = '';
		if($text_alignment != '') $classes .= ' text-'.$text_alignment;
		$h_class = ($class != '') ? ' class="'.$class.'"' : '';
		
		// title fallback
		$title = ($title != '') ? $title : $content;
		
		$rotator_delay = ($rotator_delay != '') ? $rotator_delay : '4000';
		$fit_text = ($fit_text != '') ? $fit_text : 'no';
		$fit_compressor = ($fit_compressor != '') ? $fit_compressor : '1';
		$fit_min_font_size = ($fit_min_font_size != '') ? $fit_min_font_size : '';
		$fit_max_font_size = ($fit_max_font_size != '') ? $fit_max_font_size : '';
		
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		$font_size = ($font_size) ? (( strpos($font_size, 'px') === false ) ? 'font-size:' . $font_size . 'px;' : 'font-size:' . $font_size . ';' ) : '';
		$line_height = ($line_height) ? 'line-height:' . $line_height . ';' : '';
		$letter_spacing = ($letter_spacing) ? 'letter-spacing:' . $letter_spacing . ';'  : '';
		$text_color = ($text_color) ? 'color:' . $text_color . ';'  : '';
		
		// extract font weight
		$def_font_weight = '';
		$sdf_font_weight = '';
		$sdf_font_family = '';
		
		if($font_family){
			$font_args = explode(':', $font_family);
			$sdf_font_family = $font_args[0];
			$sdf_font_weight = (count($font_args) > 1) ? $font_args[1] : $def_font_weight;
		}

		$sdf_font_family = ($sdf_font_family) ? 'font-family:' . $sdf_font_family . ';'  : '';
		$sdf_font_weight = ($sdf_font_weight) ? 'font-weight:' . $sdf_font_weight . ';'  : '';
	
		$styles = ( ($top_margin != '') || ($bottom_margin != '') || ($font_size != '') || ($line_height != '') || ($sdf_font_family != '') || ($text_color != '') || ($letter_spacing != '') ) ? ' style="' . $font_size . $line_height . $sdf_font_family . $sdf_font_weight . $text_color . $letter_spacing . $top_margin . $bottom_margin . '"' : '';
		
if( $rotate_words == 'yes' || $fit_text == 'yes' ){
// get unique headline id
if( !isset($GLOBALS['headline_id']) )
$GLOBALS['headline_id'] = 0;
else 
$GLOBALS['headline_id']++;

$headline_id = 'hl-'.$GLOBALS['headline_id'];

$headline_script = '<script type="text/javascript">
/* <![CDATA[ */
(function($) { 
"use strict";
jQuery(document).ready(function($) {';
}

if($rotate_words == 'yes' && $rotator_separator != '' ){
$rotate_arr = explode($rotator_separator, $title);
$json_str = json_encode($rotate_arr);


$title = $rotate_arr[0];

$headline_script .= '
var rotate_headline = function() {
var rotating_words = '.$json_str.' ,
counter = 0;
setInterval(function() {
$("#' . $headline_id . ' ' . $title_heading .'").fadeOut(function(){
$(this).html(rotating_words[counter=(counter+1)%rotating_words.length]).fadeIn();
});
}, ' . $rotator_delay . ');
}
rotate_headline();';
}

if($fit_text == 'yes' ){
	if($fit_min_font_size && $fit_max_font_size ){
		$headline_script .= '$("#' . $headline_id . ' ' . $title_heading .'").fitText('.$fit_compressor.', { minFontSize: "'.$fit_min_font_size.'", maxFontSize: "'.$fit_max_font_size.'" });';
	}
	else{
		$headline_script .= '$("#' . $headline_id . ' ' . $title_heading .'").fitText('.$fit_compressor.');';
	}
}

if( $rotate_words == 'yes' || $fit_text == 'yes' ){
$headline_script .= '});
}(jQuery));
/* ]]> */
</script>';
$headline_id_str = ' id="'.$headline_id.'"';
}

		$headline = '<div class="' . $module_class . '"><div'.$headline_id_str.' class="headline clearfix' . $classes . '"' . $data_animation . $data_duration . '><' . $title_heading . $styles . $h_class . '>' . $title . '</' . $title_heading . '></div></div>';  
		
		return ( $headline_script.$headline );  
		
    }
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_raw_html
	* 
	*-------------------------------------------------------------------------------------*/
	function raw_html_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => ''
		  ), $atts ) );
			
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
	    return '<div class="' . $module_class . '"' . $styles . '>' . parse_shortcode_content( $content ) . '</div>'; 
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_raw_js
	* 
	*-------------------------------------------------------------------------------------*/
	function raw_js_shortcode( $atts, $content = null ) {
		
	    return '<script type="text/javascript">' . parse_shortcode_content( $content ) . '</script>';
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_button
	* 
	*-------------------------------------------------------------------------------------*/
	function button_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"style" => '',
		"size" => '',
		"link" => '',
		"button_layout" => '',
		"button_icon" => '',
		"button_icon_position" => '',
		"target" => '',
		"nofollow" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		//fallback
		$style = ($style) ? $style : 'btn-primary';
		$style = ( strpos($style,'btn-') === false ) ? 'btn-'.$style : $style;
		
		$classes = 'btn';
		if($style != '') $classes .= ' ' . $style;
		if($size != '') $classes .= ' ' . $size;
		$classes .= ($class != '') ? ' '.$class : '';
		//fallback values
		$target = (!in_array($target, array('', 'no', 'None'))) ? ' target="'.$target.'"' : '';
		$link_nofollow = ($nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		if($button_layout == 'block') $classes .= ' btn-block';
		if($button_layout == 'left') $classes .= ' btn-left';
		if($button_layout == 'right') $classes .= ' btn-right';
		if($button_layout == 'center') $module_class .= ' btn-center';
		
		$button_icon_size = ($size == 'btn-lg') ? ' fa-lg' : '';
		$button_icon_position = ($button_icon_position == '') ? ' icon-left-side' : ' '.$button_icon_position;
		$button_icon = (!in_array($button_icon, array('', 'no'))) ? '<i class="fa fa-fw ' . $button_icon . $button_icon_size . $button_icon_position . '"></i>' : '';
		
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		$classes = ($classes != '') ? ' class="'.$classes.'"' : '';
		
		$title = ( parse_shortcode_content( $content ) != '' ) ? parse_shortcode_content( $content ) : '';
		$button_text = ($button_icon_position == ' icon-left-side') ? $button_icon . $title : $title . $button_icon;
		
		if($link == ''){	
			$mod_btn = '<button' . $classes . $styles . $data_animation. $data_duration.' title="'.$title.'" role="button">' . $button_text . '</button> ';
	  }
	  else {
	    
	    $link = sdf_check_link_prefix($link);
			$mod_btn = '<a href="' . $link . '"' . $classes . $styles . $data_animation . $data_duration . ' title="'.$title.'" '.$target.' '.$link_nofollow .' role="button">' . $button_text . '</a> ';
	  }
		
			return '<div class="' . $module_class . '">' . $mod_btn . '</div>';
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_cta_button
	* 
	*-------------------------------------------------------------------------------------*/
	function cta_button_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"style" => '',
		"size" => '',
		"link" => '',
		"button_layout" => '',
		"button_icon" => '',
		"button_icon_position" => '',
		"subtitle" => '',
		"target" => '',
		"nofollow" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		//fallback
		$style = ($style) ? $style : 'btn-primary';
		$style = ( strpos($style,'btn-') === false ) ? 'btn-'.$style : $style;
		
		$classes = 'btn btn-cta';
		if($style != '') $classes .= ' ' . $style;
		if($size != '') $classes .= ' ' . $size;
		$classes .= ($class != '') ? ' '.$class : '';
		
		if($button_layout == 'block') $classes .= ' btn-block';
		if($button_layout == 'left') $classes .= ' btn-left';
		if($button_layout == 'right') $classes .= ' btn-right';
		if($button_layout == 'center') $module_class .= ' btn-center';
		
		$button_icon_size = ($size == 'btn-lg') ? ' fa-lg' : '';
		$button_icon_position = ($button_icon_position == '') ? ' icon-left-side' : ' '.$button_icon_position;
		$button_icon = (!in_array($button_icon, array('', 'no'))) ? '<i class="fa fa-fw ' . $button_icon . $button_icon_size . $button_icon_position . '"></i>' : '';

		$title = ( parse_shortcode_content( $content ) != '' ) ? parse_shortcode_content( $content ) : '';
		$button_text = ($button_icon_position == ' icon-left-side') ? $button_icon . $title : $title . $button_icon;
		
		if($subtitle != ''){$subtitle = '  <span class="subtitle">'.$subtitle.'</span>';}

		//fallback values
		$target = (!in_array($target, array('', 'no', 'None'))) ? ' target="'.$target.'"' : '';
		$link_nofollow = ($nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		$classes = ($classes != '') ? ' class="'.$classes.'"' : '';
		
		if($link == ''){	
			$mod_btn = '<button' . $classes.$styles.$data_animation. $data_duration.' title="'.$title.'" role="button">' . $button_text.$subtitle . '</button> ';
	  }
	  else {
	    $link = sdf_check_link_prefix($link);
			$mod_btn = '<a href="' . $link . '"' . $classes.$styles.$data_animation. $data_duration.' title="'.$title.'" '.$target.' '.$link_nofollow .' role="button">' . $button_text.$subtitle . '</a> ';
	  }
		
		return '<div class="' . $module_class . '">' . $mod_btn . '</div>';
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_message_box
	* 
	*-------------------------------------------------------------------------------------*/
	function message_box_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"style" => '',
		"class" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"dismissable" => true
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$close_button = ($dismissable) ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' : '';
		$classes = 'alert';
		if($style != '') $classes .= ' ' . $style;
		if($dismissable) $classes .= ' alert-dismissable';
		
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		$classes = ($classes != '') ? ' class="'.$classes.'"' : '';
		
	return '<div class="' . $module_class . '"><div' . $classes . $styles.$data_animation. $data_duration.'>' . $close_button . parse_shortcode_content( $content ) . '</div></div>';
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_cta_box
	* 
	*-------------------------------------------------------------------------------------*/
	function cta_box_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"button_title" => '',
		"button_subtitle" => '',
		"button_style" => '',
		"button_class" => '',
		"button_size" => '',
		"button_width" => '',
		"button_height" => '',
		"button_top_margin" => '',
		"button_bottom_margin" => '',
		"button_block" => '',
		"button_link" => '',
		"button_target" => '',
		"button_nofollow" => '',
		"button_position" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$button_classes = 'btn';
		//fallback
		$button_style = ($button_style) ? $button_style : 'btn-primary';
		$button_style = (strpos($button_style,'btn-') === false) ? 'btn-'.$button_style : $button_style;
		if($button_style != '') $button_classes .= ' ' . $button_style;
		if($button_class != '') $button_classes .= ' ' . $button_class;
		if($button_block == 'yes') $button_classes .= ' btn-block';
		if($button_size != '') $button_classes .= ' ' . $button_size;
		
		$button_classes = ($button_classes != '') ? ' class="'.$button_classes.'"' : '';
		
		//fallback values
		$button_target = (!in_array($button_target, array('', 'no', 'None'))) ? ' target="'.$button_target.'"' : '';
		$link_nofollow = ($button_nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		if($button_subtitle != ''){$button_subtitle = '  <span class="subtitle">'.$button_subtitle.'</span>';}
		$btn_height = '';
		if($button_height != ''){$btn_height = 'height:'.$button_height.'!important;max-height:'.$button_height.'!important;vertical-align:middle;display:table-cell;';}
		$btn_width = '';
		if($button_width != ''){$btn_width = 'width:'.$button_width.'!important;max-width:'.$button_width.'!important;';}
		$btn_top_margin = '';
		if($button_top_margin != ''){$btn_top_margin = 'margin-top:'.$button_top_margin.';';}
		$btn_bottom_margin = '';
		if($button_bottom_margin != ''){$btn_bottom_margin = 'margin-bottom:'.$button_bottom_margin.';';}
		$btn_style = '';
		if ($btn_height || $btn_width || $btn_top_margin || $btn_bottom_margin) {
			$btn_style = ' style="' . $btn_height . $btn_width . $btn_top_margin . $btn_bottom_margin . '"';
		}
		
		$button = '';
		
		if($button_link == ''){
			$button .= '<button' . $button_classes . $btn_style . ' title="' . $button_title . '">' . $button_title . $button_subtitle . '</button>';
		}
		else{
			$button_link = sdf_check_link_prefix($button_link);
			$button .= '<a href="' . $button_link . '"' . $button_classes . $btn_style . ' title="' . $button_title . '" ' . $button_target . ' ' . $link_nofollow . '>' . $button_title . $button_subtitle . '</a>';
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		if ($button_position == 'right') {
			return '<div class="' . $module_class . '"><div class="sdf-cta-box clearfix' . $classes . '"' . $styles . $data_animation . $data_duration . '><div class="cta-box-text pull-left"><div class="cta-box-button pull-right">' . $button . '</div>' . parse_shortcode_content( $content ) . '</div></div></div>';
		}
		elseif ($button_position == 'left') {
			return '<div class="' . $module_class . '"><div class="sdf-cta-box clearfix' . $classes . '"' . $styles . $data_animation . $data_duration . '><div class="cta-box-text pull-right"><div class="cta-box-button pull-left">' . $button . '</div>' . parse_shortcode_content( $content ) . '</div></div></div>';
		}
		elseif ($button_position == 'top') {
			return '<div class="' . $module_class . '"><div class="sdf-cta-box clearfix' . $classes . '"' . $styles . $data_animation . $data_duration . '><div class="cta-box-button clearfix">' . $button . '</div><div class="cta-box-text clearfix">' . parse_shortcode_content( $content ) . '</div></div></div>';
		}
		elseif ($button_position == 'bottom') {
			return '<div class="' . $module_class . '"><div class="sdf-cta-box clearfix' . $classes . '"' . $styles . $data_animation . $data_duration . '><div class="cta-box-text clearfix">' . parse_shortcode_content( $content ) . '</div><div class="cta-box-button clearfix">' . $button . '</div></div></div>';
		}
		else {
			return '<div class="' . $module_class . '"><div class="sdf-cta-box clearfix' . $classes . '"' . $styles . $data_animation . $data_duration . '><div class="cta-box-text clearfix">' . parse_shortcode_content( $content ) . '</div><div class="cta-box-button clearfix">' . $button . '</div></div>';
		}
	
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_teaser_box
	* 
	*-------------------------------------------------------------------------------------*/
	function teaser_box_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"text_alignment" => '',
		"title" => '',
		"title_heading" => '',
		"title_position" => '',
		"title_link" => '',
		"title_target" => '',
		"title_nofollow" => '',
		"subtitle" => '',
		"teaser_icon" => '',
		"icon_size" => '',
		"button_style" => '',
		"icon_position" => '',
		"button_position" => '',
		"button_border_width" => '',
		"button_border_radius" => '',
		"button_padding" => '',
		"button_height" => '',
		"button_width" => '',
		"teaser_image" => '',
		"has_retina_version" => '',
		"image_position" => '',
		"image_shape" => '',
		"image_size" => '',
		"image_size_shape" => '',
		"image_link" => '',
		"image_target" => '',
		"image_nofollow" => '',
		"animation" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => '',
		"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		//fallback for rename
		$button_position = (isset($icon_position) && $icon_position != '') ? $icon_position : $button_position;
		if ( in_array( $image_size_shape, array("circle", "leaf", "lemon", "square", "rectangle", "ellipse", "bullet", "mushroom", "old-screen") ) ) {	
			$image_shape = $image_size_shape;
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$classes = '';
		$img_classes = '';
		$data_no_retina = '';
		if($text_alignment != '') $classes .= ' text-'.$text_alignment;
		$classes .= ( $class != '' ) ? ' '.$class : '';
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		$data_no_retina .= ( $has_retina_version != 'yes' ) ? ' data-no-retina' : '';
		
		$teaser_pos_classes = array(
				'above_title' => 'clearfix text-center', 
				'below_title' => 'clearfix text-center', 
				'left_title' => 'media-left', 
				'right_title' => 'media-right',
				'above_text' => 'clearfix text-center', 
				'below_text' => 'clearfix text-center', 
				'left_text' => 'media-left', 
				'right_text' => 'media-right',
				'aside_left' => 'media-left', 
				'aside_right' => 'media-right'
		);
		
		if ( $title_link != '' ) {
			$title_link = sdf_check_link_prefix($title_link);
		}
		
		//fallback values
		$title_target = (!in_array($title_target, array('', 'no', 'None'))) ? ' target="'.$title_target.'"' : '';
		$link_nofollow = ($title_nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		//fallback
		$button_style = ($button_style) ? $button_style : 'btn-primary';
		$button_style = (strpos($button_style,'btn-') === false) ? 'btn-'.$button_style : $button_style;
		// inline style
		$btn_height = '';
		if($button_height != ''){$btn_height = 'height:'.$button_height.'!important;max-height:'.$button_height.'!important;vertical-align:middle;line-height:'.$button_height.'!important;';}
		$btn_width = '';
		if($button_width != ''){$btn_width = 'width:'.$button_width.'!important;max-width:'.$button_width.'!important;';}
		$btn_border_radius = '';
		if($button_border_radius != ''){$btn_border_radius = 'border-radius:'.$button_border_radius.';';}
		$btn_border_width = '';
		if($button_border_width != ''){$btn_border_width = 'border-width:'.$button_border_width.';';}
		$btn_padding = '';
		if($button_padding != ''){$btn_padding = 'padding:'.$button_padding.';';}
		
		$btn_inline_style = '';
		if ($btn_height || $btn_width || $btn_border_radius || $btn_border_width || $btn_padding) {
			$btn_inline_style = ' style="' . $btn_height . $btn_width . $btn_border_radius . $btn_border_width . $btn_padding . '"';
		}
		
		$icon_inline_style = '';
		if ($button_height) {
			$icon_inline_style = ' style="line-height:' . $button_height . '"';
		}
		
		// teaser icon
		if(!in_array($teaser_icon, array('', 'no'))){
			$icon_size = ($icon_size) ? ' '.$icon_size : '';
			$teaser_icon = '<i class="fa '.$teaser_icon . $icon_size.' fa-fw"'.$icon_inline_style.'></i>';
			
			if ( $title_link != '' && $title != '' ) {
				$teaser_icon = '<figure class="teaser-icon '.$teaser_pos_classes[$button_position].'"><a href="'.$title_link.'" title="'.$title.'"'.$link_nofollow.$title_target.' class="btn '.$button_style.'"'.$btn_inline_style.'>'.$teaser_icon.'</a></figure>';		
			}
			else {
				$teaser_icon = '<figure class="teaser-icon '.$teaser_pos_classes[$button_position].'"><button title="'.$title.'" class="btn '.$button_style.'"'.$btn_inline_style.'>'.$teaser_icon.'</button></figure>';		
			}
		}
		else{
		$teaser_icon = '';
		}

		// teaser image
		// prepare for the right thumbnail (square proportions)
		$thumb_size = 'sdf-image-md-12';
		$thumb_square_shapes = array("circle", "leaf", "lemon", "square");
		if ( isset ( $image_size ) && isset ( $image_shape )) {
			$thumb_size = $image_size;
			if ( in_array( $image_shape, $thumb_square_shapes) && !in_array($image_size, array('full', 'large', 'medium', 'thumbnail')) ) {	
				$thumb_size .= '-sq';
			}
		}
		$img_classes .= ( in_array( $image_size, array( 'full', 'large', 'medium', 'thumbnail' ) ) ) ? '' : ' '.$image_shape;
		
		$teaser_img ='';
		if($teaser_image != '') {
			$image_id = sdf_get_attachment_id_by_url($teaser_image);
			
			if($image_id) {
				$pin_view =  wp_get_attachment_image_src( $image_id, $thumb_size, false, '' );
				$pin_view = $pin_view[0];
				$teaser_img = '<div class="teaser-img"><img src="'.$pin_view.'" class="img-responsive '.$img_classes.'"'.$data_no_retina.' alt="'.$title.'"></div>';
			}
			else {
				$teaser_img = '<div class="teaser-img"><img src="'.$teaser_image.'" class="img-responsive '.$img_classes.'"'.$data_no_retina.' alt="'.$title.'"></div>';
			}
		}
		
		
		// teaser subtitle
		$teaser_subtitle = '';
		if ( $subtitle != '' ) {
			$teaser_subtitle = '<p class="teaser-subtitle">' . $subtitle . '</p>';
		}
		// teaser title
		$teaser_title = ( in_array( $button_position, array( 'above_title', 'below_title' )) && !in_array($teaser_icon, array('', 'no'))) ? '<div class="teaser-title text-center">' : '<div class="teaser-title clearfix">';
		
		if ( in_array($button_position, array( 'left_title', 'right_title' ))) {
			$teaser_title .= $teaser_icon;
		}

		if ( $title_link != '' && $title != '' ) {
			$teaser_title .= '<'.$title_heading.'><a href="'.$title_link.'" title="'.$title.'"'.$link_nofollow.$title_target.'>'.$title.'</a></'.$title_heading.'>' . $teaser_subtitle;
		}
		else {
			$teaser_title .= '<'.$title_heading.'>'.$title.'</'.$title_heading.'>' . $teaser_subtitle;
		}
		
		$teaser_title .= '</div>';
		
		if ( $button_position == 'above_title' ) {
			$teaser_title = $teaser_icon . $teaser_title;
		}
		elseif ( $button_position == 'below_title') {
			$teaser_title = $teaser_title . $teaser_icon;
		}
		
		// teaser text
		$teaser_content = parse_shortcode_content( $content );
		
		$teaser_text = (strlen($teaser_content) > 6) ? '<div class="teaser-text">' . $teaser_content . '</div>' : '';
		
		if ( in_array( $button_position, array( 'above_text', 'left_text' ))) {
			$teaser_text = $teaser_icon . $teaser_text;
		}
		elseif (in_array( $button_position, array( 'below_text', 'right_text' ))) {
			$teaser_text = $teaser_text . $teaser_icon;
		}
		
		$teaser_box = '<div class="' . $module_class . '"><div class="sdf-teaser-box clearfix' . $classes . '"'. $data_animation . $data_duration . $styles . '>';
		
		if ( in_array( $button_position, array( 'aside_left' ))) {
			$teaser_box .= $teaser_icon;
		}
		
		if ( in_array( $button_position, array( 'aside_right', 'aside_left' ))) {
			$teaser_box .= '<div class="media-body">';
		}
		
		if ($image_position == 'top') {
			$teaser_box .= $teaser_img;
		}
		if ($title_position == 'above_text') {
			$teaser_box .= $teaser_title;
		}
		
		$teaser_box .= $teaser_text;
		
		if ($title_position == 'below_text') {
			$teaser_box .= $teaser_title;
		}
		if ($image_position == 'bottom') {
			$teaser_box .= $teaser_img;
		}
		
		if ( in_array( $button_position, array( 'aside_right', 'aside_left' ))) {
			$teaser_box .= '</div>';
		}
		
		if ( in_array( $button_position, array( 'aside_right' ))) {
			$teaser_box .= $teaser_icon;
		}
		
		$teaser_box .= '</div></div>';
		
		return $teaser_box;
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_team_member_box
	* 
	*-------------------------------------------------------------------------------------*/
	function team_member_box_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		'text_alignment' => '',
		'member_name' => '',
		'name_heading' => '',
		'name_position' => '',
		'name_link' => '',
		'name_link_target' => '',
		'name_link_nofollow' => '',
		'member_title' => '',
		'title_tag' => '',
		'member_title_heading' => '',
		'member_image' => '',
		"has_retina_version" => '',
		'image_shape' => '',
		'image_size' => '',
		'image_size_shape' => '',
		'image_position' => '',
		"icon_size" => '',
		"button_style" => '',
		"button_border_radius" => '',
		"button_border_width" => '',
		"button_padding" => '',
		"button_margin" => '',
		"button_height" => '',
		"button_width" => '',
		'google_plus' => '',
		'skype' => '',
		'twitter' => '',
		'linkedin' => '',
		'facebook' => '',
		'pinterest' => '',
		'animation' => '',
		'entrance_animation' => '',
		'entrance_animation_duration' => '',
		'class' => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$classes = '';
		$img_classes = '';
		$data_no_retina = '';
		$classes .= ( $class != '' ) ? ' '.$class : '';
		
		$data_animation = '';
		$data_duration = '';
		//fallback
		$member_title_heading = ($title_tag) ? $title_tag : $member_title_heading;
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		if ( in_array( $image_size_shape, array("circle", "leaf", "lemon", "square", "rectangle", "ellipse", "bullet", "mushroom", "old-screen") ) ) {	
			$image_shape = $image_size_shape;
		}
		
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		$data_no_retina .= ( $has_retina_version != 'yes' ) ? ' data-no-retina' : '';
		
		$member_img_classes = array(
			'above_name' => 'clearfix', 
			'below_name' => 'clearfix', 
			'left_name' => 'pull-left', 
			'right_name' => 'pull-right',
			'above_text' => 'clearfix', 
			'below_text' => 'clearfix', 
			'left_text' => 'pull-left', 
			'right_text' => 'pull-right'
		);
		// prepare for the right thumbnail (square proportions)
		$thumb_size = 'sdf-image-md-12';
		$thumb_square_shapes = array("circle", "leaf", "lemon", "square");
		if ( isset ( $image_size ) && isset ( $image_shape )) {
			$thumb_size = $image_size;
			if ( in_array( $image_shape, $thumb_square_shapes) && !in_array($image_size, array('full', 'large', 'medium', 'thumbnail')) ) {	
				$thumb_size .= '-sq';
			}
		}
		$img_classes .= ( in_array( $image_size, array( 'full', 'large', 'medium', 'thumbnail' ) ) ) ? '' : ' '.$image_shape;
		
		$member_img ='';
		if($member_image != '') {
			$image_id = sdf_get_attachment_id_by_url($member_image);
			
			if($image_id) {
			$pin_view =  wp_get_attachment_image_src( $image_id, $thumb_size, false, '' );
			$pin_view = $pin_view[0];
			$member_img = '<img src="'.$pin_view.'" class="member-img img-responsive '.$member_img_classes[$image_position].' '.$img_classes.'"'.$data_no_retina.' alt="'.$member_name.'">';
			}
			else {
				$member_img = '<img src="'.$member_image.'" class="member-img img-responsive '.$member_img_classes[$image_position].' '.$img_classes.'"'.$data_no_retina.' alt="'.$member_name.'">';
			}
		}
		
		// member title
		$team_member_title = '';
		if ( $member_title != '' ) {
			$team_member_title = '<'.$member_title_heading.' class="member-title">' . $member_title . '</'.$member_title_heading.'>';
		}
		// member name
		$team_member_name = '<div class="member-name">';
		//fallback values
		$name_link_target = (!in_array($name_link_target, array('', 'no', 'None'))) ? ' target="'.$name_link_target.'"' : '';
		$link_nofollow = ($name_link_nofollow == 'yes') ? ' rel="nofollow"' : '';
		if ( $name_link != '' ) {
			$name_link = sdf_check_link_prefix($name_link);
			$team_member_name .= '<'.$name_heading.'><a href="'.$name_link.'" title="'.$member_name.'"'.$link_nofollow.$name_link_target.'>'.$member_name.'</a></'.$name_heading.'>' . $team_member_title;
		}
		else {
			$team_member_name .= '<'.$name_heading.'>'.$member_name.'</'.$name_heading.'>' . $team_member_title;
		}
		$team_member_name .= '</div>';
		
		if ( in_array( $image_position, array( 'above_name', 'left_name' ))) {
			$team_member_name = $member_img . $team_member_name;
		}
		elseif (in_array( $image_position, array( 'below_name', 'right_name' ))) {
			$team_member_name = $team_member_name . $member_img;
		}
		
		// member text
		$text_alignment = ($text_alignment != '') ? ' text-'.$text_alignment : '';
		$member_text = '<div class="member-text'.$text_alignment.'">' . parse_shortcode_content( $content ) . '</div>';
		
		if ( in_array( $image_position, array( 'above_text', 'left_text' ))) {
			$member_text = $member_img . $member_text;
		}
		elseif (in_array( $image_position, array( 'below_text', 'right_text' ))) {
			$member_text = $member_text . $member_img;
		}
		
		$team_member_box = '<div class="' . $module_class . '"><div class="sdf-team-member-box clearfix' . $classes . '"'. $styles . $data_animation . $data_duration . ' itemscope="itemscope" itemtype="https://schema.org/Person">';
		
		if ($name_position == 'above_text') {
			$team_member_box .= $team_member_name;
		}
		
		$team_member_box .= $member_text;
		
		if ($name_position == 'below_text') {
			$team_member_box .= $team_member_name;
		}
		
		$icon_size = ($icon_size) ? ' '.$icon_size : '';
		//fallback
		$button_style = ($button_style) ? $button_style : 'btn-primary';
		$button_style = (strpos($button_style,'btn-') === false) ? 'btn-'.$button_style : $button_style;
		// inline style
		$btn_height = '';
		if($button_height != ''){$btn_height = 'height:'.$button_height.'!important;max-height:'.$button_height.'!important;vertical-align:middle;line-height:'.$button_height.'!important;';}
		$btn_width = '';
		if($button_width != ''){$btn_width = 'width:'.$button_width.'!important;max-width:'.$button_width.'!important;';}
		$btn_border_radius = '';
		if($button_border_radius != ''){$btn_border_radius = 'border-radius:'.$button_border_radius.';';}
		$btn_border_width = '';
		if($button_border_width != ''){$btn_border_width = 'border-width:'.$button_border_width.';';}
		$btn_padding = '';
		if($button_padding != ''){$btn_padding = 'padding:'.$button_padding.';';}
		$btn_margin = '';
		if($button_margin != ''){$btn_margin = 'margin:'.$button_margin.';';}
		
		$btn_inline_style = '';
		if ($btn_height || $btn_width || $btn_border_radius || $btn_border_width || $btn_padding || $btn_margin) {
			$btn_inline_style = ' style="' . $btn_height . $btn_width . $btn_border_radius . $btn_border_width . $btn_padding . $btn_margin . '"';
		}

		if( ($google_plus != '') ||($skype != '') ||($twitter != '') ||($linkedin != '') ||($facebook != '') ||($pinterest != '') ) {
		$team_member_box .= '<div class="member-social">';
		$team_member_box .= '<ul class="social-menu">';
			if($google_plus != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="Google Plus" data-placement="top" data-toggle="tooltip" href="'.$google_plus.'"><i class="fa fa-google-plus'.$icon_size.'"></i></a>
						</li>';
			}
			if($skype != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="Skype" data-placement="top" data-toggle="tooltip" href="'.$skype.'"><i class="fa fa-skype'.$icon_size.'"></i></a>
						</li>';
			}
			if($twitter != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="Twitter" data-placement="top" data-toggle="tooltip" href="'.$twitter.'"><i class="fa fa-twitter'.$icon_size.'"></i></a>
						</li>';
			}
			if($linkedin != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="LinkedIn" data-placement="top" data-toggle="tooltip" href="'.$linkedin.'"><i class="fa fa-linkedin'.$icon_size.'"></i></a>
						</li>';
			}
			if($facebook != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="Facebook" data-placement="top" data-toggle="tooltip" href="'.$facebook.'"><i class="fa fa-facebook'.$icon_size.'"></i></a>
						</li>';
			}
			if($pinterest != '') {
				$team_member_box .= '<li>
						<a class="member-social-link btn '.$button_style.'"'.$btn_inline_style.' data-original-title="Pinterest" data-placement="top" data-toggle="tooltip" href="'.$pinterest.'"><i class="fa fa-pinterest'.$icon_size.'"></i></a>
						</li>';
			}
		$team_member_box .= '</ul>';
		$team_member_box .= '</div>';
		}
		$team_member_box .= '</div></div>';
		
		return $team_member_box;
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_code
	* 
	*-------------------------------------------------------------------------------------*/
	function code_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"type" => '',
		"size" => '',
		"link" => ''
		), $atts));
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		return '<div class="' . $module_class . '"><code>' . $content . '</code></div>';
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_pre_code
	* 
	*-------------------------------------------------------------------------------------*/
	function pre_code_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"type" => '',
		"size" => '',
		"link" => ''
		), $atts));
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		return '<div class="' . $module_class . '"><pre><code>' . $content . '</code></pre></div>';
		
	}
	

	/*--------------------------------------------------------------------------------------
	*
	* sdf_counter
	* 
	*-------------------------------------------------------------------------------------*/
	function counter_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"counter_type"              		=> '',
		"text_alignment" => '',
		"number"             		=> '',
		"number_font_size"         		=> '',
		"number_font_weight"       		=> '',
		"number_color"        		=> '',
		"text"              		=> '',
		"text_font_size"         		=> '',
		"text_font_weight"  		=> '',
		"text_transform"    		=> '',
		"text_color"        		=> '',
		"separator"         		=> '',
		"separator_style"         		=> '',
		"separator_line_width"         		=> '',
		"separator_line_color"   		=> '',
		"separator_top_margin" => '',
		"separator_bottom_margin" => '',
		"entrance_animation" => '',
		"entrance_animation_duration" => ''
		), $atts));

		//init
		$html                   = "";
		$counter_wrap_classes = "";
		$number_styles         = "";
		$text_styles            = "";
		$separator_styles       = "";
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$counter_type = ($counter_type != '') ? $counter_type : 'zero';
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		$separator_top_margin = ($separator_top_margin != '') ? 'margin-top:'.$separator_top_margin.';' : '';
		$separator_bottom_margin = ($separator_bottom_margin != '') ? 'margin-bottom:'.$separator_bottom_margin.';' : '';
		$custom_line_color = (!in_array( $separator_line_color, array( 'primary', 'font', '' ) )) ? 'border-bottom-color:'.$separator_line_color.';' : '';
		$custom_line_width = ($separator_line_width != '') ? 'border-bottom-width:'.$separator_line_width.';' : '';
		$separator_line_color = ($separator_line_color == 'primary') ? ' theme-primary' : '';
		$separator_styles .= $separator_line_color;
		
		$separator_inline_css = ( ($separator_top_margin != '') || ($separator_bottom_margin != '') || ($custom_line_color != '') || ($custom_line_width != '') ) ? ' style="' . $separator_top_margin . $separator_bottom_margin . $custom_line_color . $custom_line_width . '"' : '';
		
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$counter_wrap_classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';

		if($text_alignment != "") {
			$counter_wrap_classes .= " text-".$text_alignment;
		}
		if($number_color != "") {
			$number_styles .= "color: ".$number_color.";";
		}
		if($number_font_weight != "") {
			$number_styles .= "font-weight: ".$number_font_weight.";";
		}
		if($number_font_size != "") {
			$number_styles .= "font-size: ".$number_font_size.";";
		}
		if($text_font_weight != "") {
			$text_styles .= "font-weight: ".$text_font_weight.";";
		}
		if($text_font_size != "") {
			$text_styles .= "font-size: ".$text_font_size.";";
		}
		if($text_transform != "") {
			$text_styles .= "text-transform: ".$text_transform.";";
		}

		if($text_color != "") {
				$text_styles .= "color: ".$text_color.";";
		}

		$html .= '<div class="counter_wrap ' . $module_class . ' ' . $counter_wrap_classes . '" ' . $data_animation . $data_duration . '>';
		$html .= '<p class="counter '.$counter_type.'" style="'.$number_styles.'">'.$number.'</p>';

		if($separator == "yes") {
				$html .= '<div class="separator '. $separator_style .'"' . $separator_inline_css . '></div>';
		}

		$html .= $content;

		if($text != "") {
				$html .= '<p class="counter_text" style="' . $text_styles . '">' . $text . '</p>';
		}

		$html .= '</div>';

		return $html;
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_label
	* 
	*-------------------------------------------------------------------------------------*/
	function label_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		'style' => '',
		"top_margin" => '',
		"bottom_margin" => '',
		'title' => '',
		'align' => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$classes = '';
		if ($align) $classes .= ' pull-'.$align;
		$classes .= ($style) ? ' label-'.$style : ' label-default';
		
		return '<span class="label' . $classes . '"'.$styles.'>' . $title . '</span>';
		
	}

	/*--------------------------------------------------------------------------------------
	*
	* sdf_badge
	* 
	*-------------------------------------------------------------------------------------*/
	function badge_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		'title' => '',
		"top_margin" => '',
		"bottom_margin" => '',
		'align' => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$classes = '';
		if ($align) $classes .= ' pull-'.$align;

		return '<span class="badge ' . $classes . '"'.$styles.'>' . $title . '</span>';

	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_well
	* 
	*-------------------------------------------------------------------------------------*/
	function well_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		"module_width" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		'size' => '',
		'animation' => '',
		'entrance_animation' => '',
		'entrance_animation_duration' => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$classes = '';
		$classes .= ($size != '') ? ' '.$size : '';
		
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
	return '<div class="' . $module_class . '"><div class="well' . $classes . '"'.$styles . $data_animation . $data_duration . '>' . parse_shortcode_content( $content ) . '</div></div>';
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_blank_spacer
	* 
	*-------------------------------------------------------------------------------------*/
	function blank_spacer_shortcode( $atts, $content = null ) {
		extract(shortcode_atts(array(
		"module_width" => '',
		'height' => '',
		'class' => ''
		), $atts));
		$classes = ($class != '') ? ' '.$class : '';
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$height = ($height != '') ? ' '.$height : '';
	return '<div class="' . $module_class . $classes . '"><div class="blank_spacer clearfix" style=" height:' . $height . ';">' . parse_shortcode_content( $content ) . '</div></div>';
	}
	
  
  /*--------------------------------------------------------------------------------------
    *
    * sdf_separator
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
    function separator_shortcode( $atts, $content = null) {
		extract( shortcode_atts( array(
		"module_width" => '',
		"separator_style" => '',
		"line_color" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"custom_line_width" => ''
		  ), $atts ) );
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		$custom_line_color = (!in_array( $line_color, array( 'primary', 'font', '' ) )) ? 'border-bottom-color:'.$line_color.';' : '';
		$custom_line_width = ($custom_line_width != '') ? 'border-bottom-width:'.$custom_line_width.';' : '';
		$line_color = ($line_color == 'primary') ? ' theme-primary' : '';
		$separator_style .= $line_color;
		
		$inline_css = ( ($top_margin != '') || ($bottom_margin != '') || ($custom_line_color != '') || ($custom_line_width != '') ) ? ' style="' . $top_margin . $bottom_margin . $custom_line_color . $custom_line_width . '"' : '';

	return '<div class="' . $module_class . '"><div class="separator '. $separator_style .'"' . $inline_css . '></div></div>';  
    }
	
  
  /*--------------------------------------------------------------------------------------
    *
    * sdf_separator_with_text
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
    function separator_with_text_shortcode( $atts, $content = null) {
		extract( shortcode_atts( array(
		"module_width" => '',
		"separator_style" => '',
		"line_color" => '',
		"title" => '',
		"title_heading" => '',
		"top_margin" => '',
		"bottom_margin" => '',
		"custom_line_width" => ''
		  ), $atts ) );
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		$custom_line_color = (!in_array( $line_color, array( 'primary', 'font', '' ) )) ? 'border-bottom-color:'.$line_color.';' : '';
		$custom_line_width = ($custom_line_width != '') ? 'border-bottom-width:'.$custom_line_width.';' : '';
		$line_color = ($line_color == 'primary') ? ' theme-primary' : '';
		$separator_style .= $line_color;
		
		$inline_css = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		$inline_separator_style = ( ($custom_line_color != '') || ($custom_line_width != '') ) ? ' style="' . $custom_line_color . $custom_line_width . '"' : '';

	return '<div class="' . $module_class . '"><div class="separator-t"' . $inline_css . '><' . $title_heading . ' class="separator-text '. $separator_style .'"' . $inline_separator_style . '><span>' . $title . '</span></' . $title_heading . '></div></div>';  
    }

  /*--------------------------------------------------------------------------------------
    *
    * sdf_blockquote
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
  function blockquote_shortcode( $atts, $content = null ) {
    extract(shortcode_atts(array(
			"module_width" => '',
      "class" => '',
      "top_margin" => '',
      "bottom_margin" => '',
      "blockquote_alignment" => 'left',
      "quote_icon" => 'no',
      "quote_icon_size" => '',
      "blockquote_author" => '',
      "cite_title" => '',
      "cite_link" => '',
      "animation" => '',
      "entrance_animation" => '',
      "entrance_animation_duration" => ''
    ), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$inline_style = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$cite = '';
		
		$side = ($blockquote_alignment == 'right') ? ' blockquote-reverse' : '';
		if ($quote_icon == 'yes') {
		$quote_icon_size = ($quote_icon_size) ? ' '.$quote_icon_size : '';
		$quote_fa = ($blockquote_alignment == 'right') ? ' fa fa-quote-right'.$quote_icon_size : ' fa fa-quote-left'.$quote_icon_size;
		}
		
		$classes = ($class != '') ? $class : '';
		
		$classes .= ($blockquote_alignment != '') ? ' text-'.$blockquote_alignment : '';
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
	
		$output = '<div class="' . $module_class . '"><blockquote class="' . $classes.$side . '"' . $data_animation . $data_duration . $inline_style . '>';
		
		if ($quote_icon == 'yes') {
			$output .= '<div class="quote-icon pull-' . $blockquote_alignment . '"><i class="media-object' . $quote_fa . '"></i></div>'; 
		}
		
		$output .= '<div class="media-body"><p>' . parse_shortcode_content( $content ) . '</p>';

		if($cite_title != ''){
			$cite_link = sdf_check_link_prefix($cite_link);
			$cite = ($cite_link != '') ? '<cite title="' . $cite_title . '"><a href="' . $cite_link . '" title="' . $cite_title . '">' . $cite_title . '</a></cite>' : '<cite title="' . $cite_title . '">' . $cite_title . '</cite>';
		}
		if($cite != '' || $blockquote_author != '' ){
			$output .= '<footer>' . $blockquote_author . ' ' . $cite . '</footer>'; 
		}
		
		$output .= '</div>';
		$output .= '</blockquote></div>';
			
			return $output;
  }
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_short
	* 
	*-------------------------------------------------------------------------------------*/
	
	function theme_callback_shortcode($atts, $content = null){
		$atts = shortcode_atts( array( 'id' => '' ), $atts );
		$id = $atts['id'];
		$shortContent_key = null;
		$shortCode = sdfGo()->sdf_get_global('misc','theme_short_codes');
		for($i=0; $i< count($shortCode['code']); $i++) {
		   if($shortCode['code'][$i] == $id) $shortContent_key = "key_".$i;
		}
		if(!is_null($shortContent_key)):
			$shortContent_key = str_replace("key_","",$shortContent_key);
				return	stripslashes($shortCode['content'][$shortContent_key]);
		else:
			return false;
		endif;
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_widget
	* 
	*-------------------------------------------------------------------------------------*/
	
	function widget_shortcode($atts, $content = null){
		extract( shortcode_atts( array(
			"module_width" => '',
			"top_margin" => '',
			"bottom_margin" => '',
			"title" => '',
			"choose_sidebar" => '',
			"animation" => '',
			"entrance_animation" => '',
			"entrance_animation_duration" => '',
			"class" => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$module_class .= ($class != '') ? ' '.$class : '';
		$classes = '';
		$output = '';
		if(!isset($choose_sidebar)) return false;
		
		// title fallback
		$title = ($title != '') ? $title : $content;
		
		$title = ($title != '') ? '<h3 class="widgettitle">'.$title.'</h3>' : '';
		$data_animation = '';
		$data_duration = '';
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		if(function_exists('dynamic_sidebar') && is_active_sidebar($choose_sidebar)) {

			$widget_sidebars_content = array();
			ob_start();
			dynamic_sidebar( $choose_sidebar );
			array_push($widget_sidebars_content, ob_get_clean());

			if(strlen($widget_sidebars_content[0]) > 0) {
				$output = $widget_sidebars_content[0];

				if($output) $output = $title.'<section class="widget_area module row'.$classes.'"'. $data_animation . $data_duration .'>'.$output.'</section>';
			}
		}

			return '<div class="' . $module_class . '"'.$styles.'>'.$output.'</div>';
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_video	fallback*****************
	* 
	*-------------------------------------------------------------------------------------*/

	function video_shortcode( $atts, $content = null ) {
		
		extract( shortcode_atts( array(
			"module_width" => '',
			"title" => '',
			"video_url" => ''
		), $atts ) );
		
		$module_width = ($module_width) ? $module_width : '1/1';
		$video_url = sdf_check_link_prefix($video_url);
		// title fallback
		$title = ($title != '') ? $title : $content;
		
		return parse_shortcode_content('[sdf_video_oembed title="'.$title.'" module_width="'.$module_width.'" video_oembed_url="'.$video_url.'" video_oembed_aspect_ratio="16by9"][/sdf_video_oembed]');
		
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_video_oembed
	* 
	*-------------------------------------------------------------------------------------*/

	function video_oembed_shortcode( $atts, $content = null ) {

		global $wp_widget_factory;
		
		extract( shortcode_atts( array(
			"module_width" => '',
			"top_margin" => '',
			"bottom_margin" => '',
			"title" => '',
			"video_oembed_url" => '',
			"video_oembed_aspect_ratio" => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$widget_name = 'SDF_Video_oEmbed_Widget';
		if( $atts['video_oembed_url'] != '' ) {
			$atts['video_oembed_url'] = sdf_check_link_prefix($atts['video_oembed_url']);
		}
		
		if (!($wp_widget_factory->widgets[$widget_name] instanceof WP_Widget)) {
			$wp_class = 'WP_Widget_'.ucwords(strtolower($class));
			
			if (!($wp_widget_factory->widgets[$wp_class] instanceof WP_Widget)) {
				return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct", SDF_TXT_DOMAIN),'<strong>'.$class.'</strong>').'</p>';
			} else {
				$class = $wp_class;
			}
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		ob_start();
		the_widget($widget_name, $atts, array(
			'widget_id'=>'sdf_video_oembed_widget-'.rand()
		));
		$output = ob_get_contents();
		ob_end_clean();
		return '<div class="' . $module_class . '"'.$styles.'>'.$output.'</div>';
		
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_portfolio
	* 
	*-------------------------------------------------------------------------------------*/

	function portfolio_shortcode( $atts, $content = null ) {

		global $wp_widget_factory;
		
		extract( shortcode_atts( array(
			'module_width' => '',
			'top_margin' => '',
			'bottom_margin' => '',
			'title' => '',
			'layout_type' => 'grid',
			'project_type' => '',
			'rows' => '',
			'columns' => '',
			'grid_layout' => '',
			'layout' => '',
			'portfolio_filter' => '',
			'item_title_heading' => '',
			'list_item_image_position' => '',
			'info_display' => '',
			'show_overlay_title' => 'yes',
			'show_item_link_button' => 'yes',
			'show_prettyphoto_button' => 'yes',
			'thumbnails_shape' => 'rectangle',
			'thumbnails_size' => 'sdf-image-md-12',
			'entrance_animation' => '',
			'entrance_animation_duration' => '',
			'posts_count' => '',
			'posts_offset' => '',
			'word_count' => '',
			'show_images' => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$widget_name = 'Sdf_Portfolio_Widget';
		// layout fallback
		if ( isset($atts['layout']) ) $atts['grid_layout'] = $atts['layout'];
		// convert to array for grid_project_type or list_project_type
		$atts[$layout_type.'_project_type'] = explode(',', $project_type);
		
		$atts['pfw_layout_type'] = $layout_type;
		$atts[$layout_type.'_thumbnails_shape'] = $thumbnails_shape;
		$atts[$layout_type.'_thumbnails_size'] = $thumbnails_size;
		$atts[$layout_type.'_word_count'] = $word_count;
		$atts[$layout_type.'_posts_offset'] = $posts_offset;
		$atts[$layout_type.'_info_display'] = $info_display;
		$atts[$layout_type.'_entrance_animation'] = $entrance_animation;
		$atts[$layout_type.'_entrance_animation_duration'] = $entrance_animation_duration;
		$atts[$layout_type.'_item_title_heading'] = $item_title_heading;

		if (!($wp_widget_factory->widgets[$widget_name] instanceof WP_Widget)) {
			$wp_class = 'WP_Widget_'.ucwords(strtolower($class));
			
			if (!($wp_widget_factory->widgets[$wp_class] instanceof WP_Widget)) {
				return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct", SDF_TXT_DOMAIN),'<strong>'.$class.'</strong>').'</p>';
			} else {
				$class = $wp_class;
			}
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		ob_start();
		the_widget($widget_name, $atts, array(
			'widget_id'=>'sdf_portfolio_widget-'.rand()
		));
		$output = ob_get_contents();
		ob_end_clean();
		return '<div class="' . $module_class . '"'.$styles.'>'.$output.'</div>';
			
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_image
	* 
	*-------------------------------------------------------------------------------------*/

	function image_shortcode( $atts, $content = null ) {
	  extract( shortcode_atts( array(
			'module_width' => '',
			'top_margin' => '',
			'bottom_margin' => '',
			'title' => '',
			'alt_text' => '',
			'image' => '',
			'has_retina_version' => '',
			'image_shape' => '',
			'image_border_radius' => '',
			'image_size' => '',
			'image_size_shape' => '',
			'image_layout' => '',
			'image_alignment' => '',
			'image_hover_effect' => '',
			'image_hover_title' => '',
			'image_hover_subtitle' => '',
			'lightbox' => '',
			'lightbox_ad' => '',
			'link' => '',
			'link_title' => '',
			'target' => '',
			'nofollow' => '',
			'animation' => '',
			'entrance_animation' => '',
			'entrance_animation_duration' => '',
			"class" => ''
		), $atts));
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		$image_border_radius = ($image_border_radius != '') ? 'border-radius:'.$image_border_radius.';' : '';
		
		$image_hover_effect = ($image_hover_effect != '') ? ' effect-'.$image_hover_effect : '';
		$image_hover_title = ($image_hover_title != '') ? '<h3>'.$image_hover_title.'</h3>' : '';
		$image_hover_subtitle = ($image_hover_subtitle != '') ? '<p>'.$image_hover_subtitle.'</p>' : '';
		
		//$thumb_id = get_post_thumbnail_id(get_the_ID());
		//$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
		//if(count($alt)) echo $alt;

		$styles = ( ($top_margin != '') || ($bottom_margin != '') || ($image_border_radius != '') ) ? ' style="' . $top_margin . $bottom_margin . $image_border_radius . '"' : '';
		
		//fallback for rename
		$image_alignment = (isset($image_layout) && $image_layout != '') ? $image_layout : $image_alignment;
		if ( in_array( $image_size_shape, array("circle", "leaf", "lemon", "square", "rectangle", "ellipse", "bullet", "mushroom", "old-screen") ) ) {	
			$image_shape = $image_size_shape;
		}
		
	  $module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$data_no_retina = '';
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		$classes .= ( $image_shape != '' && !in_array( $image_size, array( 'full', 'large', 'medium', 'thumbnail' ) ) ) ? ' '.$image_shape : '';
		$classes .= ( $image_alignment != '' ) ? ' img-'.$image_alignment : ' img-left';
		$data_no_retina .= ( $has_retina_version != 'yes' ) ? ' data-no-retina' : '';
		$data_animation = '';
		$data_duration = '';
		
		//fallback
		$entrance_animation = ($animation) ? $animation : $entrance_animation;
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		// prepare for the right thumbnail (square proportions)
		$thumb_size = 'sdf-image-md-12';
		$thumb_square_shapes = array("circle", "leaf", "lemon", "square");
		if ( isset ( $image_size ) && isset ( $image_shape )) {
			$thumb_size = $image_size;
			if ( in_array( $image_shape, $thumb_square_shapes) && !in_array($image_size, array('full', 'large', 'medium', 'thumbnail')) ) {	
				$thumb_size .= '-sq';
			}
		}
			
		$mod_img ='';
		$view_more ='';
		if($image != '') {
			$image_id = sdf_get_attachment_id_by_url($image);
			if($image_id) {
				$pin_view =  wp_get_attachment_image_src( $image_id, $thumb_size, false, '' );
				$image = $pin_view[0];
			}
			
			$mod_img = '<div><img src="' . $image . '" class="image-module img-responsive'. $classes . '"' . $data_no_retina . $styles . $data_animation . $data_duration . ' title="' . $title . '" alt="' . $alt_text . '"></div>';
		}
		
		if($link_title != ''){$link_title = ' title="'.$link_title.'"';}
	  //fallback values
		$target = (!in_array($target, array('', 'no', 'None'))) ? ' target="'.$target.'"' : '';
	  $link_nofollow = ($nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		if($lightbox_ad == 'yes'){
			$view_more = '<div class="view-more">'.do_shortcode('[sdf_lightbox page_id="'.$lightbox_ad.'" modal_bottom="" modal_position="" modal_animation="" modal_size=""]View more[/sdf_lightbox]').'</div>';
		}
		elseif($lightbox == 'yes'){
			$view_more = '<div class="view-more"><a href="' . $image . '" class="pretty-view clearfix" data-rel="prettyPhoto"' . $link_title .'>View more</a></div>';
		}
		elseif($link != ''){	
	    $link = sdf_check_link_prefix($link);
			$view_more = '<div class="view-more"><a href="' . $link . '"' . $link_title . $target . $link_nofollow .' class="clearfix">View more</a></div>';
	  }
		
		if ( $view_more != '' || $image_hover_title != '' || $image_hover_subtitle != '' ) {
			$view_more = '<figcaption>'.$image_hover_title.$image_hover_subtitle.$view_more.'</figcaption>';
		}
		 
		$mod_img = '<figure class="image-module'.$image_hover_effect.'">'.$mod_img.$view_more.'</figure>';
		
	    return '<div class="' . $module_class . '">' . $mod_img . '</div>';
		
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_tooltip
	* 
	*-------------------------------------------------------------------------------------*/

	function tooltip_shortcode( $atts, $content = null ) {
	  $placement = 'top';
	  if(isset($atts[title])):
		$title = $atts[title];
	  endif;
	  if(isset($atts[placement])):
		$placement = $atts[placement];
	  endif;
	  return '<a data-toggle="tooltip" data-placement="' . $placement . '" title="' . $title . '" class="wpu-tooltip">' . $content . '</a>';
	}
	
	
	/*--------------------------------------------------------------------------------------
	*
	* sdf_lightbox
	* 
	*-------------------------------------------------------------------------------------*/

	function lightbox_shortcode( $atts, $content = null ) {
	  extract( shortcode_atts( array(
			'page_id' => '',
			'modal_size' => '',
			'modal_bottom_position' => '',
			'modal_position' => '',
			'modal_bottom' => '',
			'modal_animation' => '',
			'auto_trigger_time' => ''
		), $atts));
		
		if($page_id != ''){
		
		$modal_html = '';
		$modal_link = '';
		$data_animation = '';
		$data_duration = '';
		// check '', then default and get default, otherwise ''
		// fallback for "No" (old animate.css classes list)
		$entrance_animation = ( !in_array( $modal_animation, array( '', 'No' ))) ? ( ($modal_animation == 'default') ? sdfGo()->sdf_get_option('lightbox', 'lightbox_animation_type') : $modal_animation ) : '';
		$data_animation = ( sdf_is_not( $entrance_animation, '' )) ? ' data-animation="md-effect-'.$entrance_animation.'"' : '';
		$entrance_animation = ( sdf_is_not( $entrance_animation, '' )) ? ' md-effect-'.$entrance_animation : '';

		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		
		$modal_size = ($modal_size != '') ? ' modal-'.$modal_size : '';
		$modal_bottom = ($modal_bottom == 'yes' || in_array($modal_bottom_position, array('left', 'center', 'right'))) ? true : false;
		$modal_position = ($modal_bottom_position != '') ? 'modal-bottom-'.$modal_bottom_position : $modal_position;
		
		if(sdf_is_not($auto_trigger_time, '')):
		  $modal_html .= '<script type="text/javascript">'
					  . 'jQuery(document).ready(function() {
					  var modal_for_page = jQuery(\'#modal-for-page-' . $page_id . '\');
					  setTimeout( function() {
					  modal_for_page.modal(\'show\');
					  },'.$auto_trigger_time.');
					  modal_for_page.on(\'show.bs.modal\', function() {
					  var modal_dialog = jQuery(\'#modal-for-page-' . $page_id . ' .modal-dialog\');
					  modal_dialog.removeClass("animate_now");
					  var animation_type = (modal_dialog.data("animation") != \'No\') ? modal_dialog.data("animation") : \''.sdfGo()->sdf_get_option('lightbox', 'lightbox_animation_type').'\';
					  modal_dialog.addClass(animation_type);
					  });
					 });'
					  . '</script>';
		elseif($modal_bottom):
		  $modal_html .= '<script type="text/javascript">'
					  . 'jQuery(document).ready(function() {
							var modal_for_page = jQuery(\'#modal-for-page-' . $page_id . '\');
							jQuery(\'#content-wrap\').waypoint(function(direction) {
								if(direction === "down"){
									setTimeout( function() {
										modal_for_page.modal(\'show\');
									},500);
								}
							}, {
							offset: function() {
							return jQuery.waypoints(\'viewportHeight\') - jQuery(this).height() + 100;
							}
							});
							modal_for_page.on(\'show.bs.modal\', function() {
							var modal_dialog = jQuery(\'#modal-for-page-' . $page_id . ' .modal-dialog\');
							modal_dialog.removeClass("animate_now");
							var animation_type = (modal_dialog.data("animation") != \'No\') ? modal_dialog.data("animation") : \''.sdfGo()->sdf_get_option('lightbox', 'lightbox_animation_type').'\';
							modal_dialog.addClass(animation_type);
							});
					  });'
					  . '</script>';
		else:
			$modal_link = '<a data-target="#modal-for-page-' . $page_id . '" data-toggle="modal">' . $content . '</a>';
		endif;

		$modal_post = get_post($page_id);
		$modal_content = str_replace(']]>', ']]&gt;', apply_filters('the_content', $modal_post->post_content));
		$modal_html .= '<div id="modal-for-page-' . $page_id . '" class="modal fade'.$entrance_animation.'" tabindex="-1" role="dialog" aria-labelledby="modal-label-for-'.$page_id.'" aria-hidden="true">'
				. '<div class="modal-dialog '.$modal_position.$modal_size.'"'.$data_animation.'>'
				. '<div class="modal-content">'
				. '<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></div>'
				. '<div class="modal-body">' . $modal_content . '</div>'
				. '<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div></div></div></div>';
		
		$current_page_id = get_the_ID();
		if ( false === ( $modal_query_results = get_transient( 'modals_for_page_'. $current_page_id ) ) ) {
			$modal_query_results = array();
		}
		$modal_query_results[] = sdf_encode( serialize( $modal_html ));
		set_transient( 'modals_for_page_'. $current_page_id, $modal_query_results, 60 );
			
		return $modal_link;
		
	  }
	}
	


	/*--------------------------------------------------------------------------------------
    *
    * sdf_tabs
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function tabs_shortcode( $atts, $content = null ) {
    
    if( isset($GLOBALS['tabs_count']) )
      $GLOBALS['tabs_count']++;
    else
      $GLOBALS['tabs_count'] = 0;

	extract(shortcode_atts(array(
		"module_width" => '',
		"direction" => '',
		"class" => ''
     ), $atts));
    
    $tab_dir = ''; 
		$tab_dir = (sdf_is_not($direction, '')) ? ' tabs-'.$direction : ''; 
		$class = ($class != '') ? ' '.$class : '';
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
	
    preg_match_all( '/tab title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE );
    
    $tab_titles = array();
    if( isset($matches[1]) ){ $tab_titles = $matches[1]; }
    
    $output = '<div class="tabbable'.$tab_dir.''.$class.'">';
    
    if( count($tab_titles) ){
	
      if (sdf_is($direction, 'below')) {
	    $output .= '<div class="tab-content">';
        $output .= parse_shortcode_content( $content );
        $output .= '</div>';
	  }
	  
	  $output .= '<ul class="nav nav-tabs" role="tablist" id="custom-tabs-'. rand(1, 100) .'">';
      
      $i = 0;
      foreach( $tab_titles as $tab ){
        if($i == 0)
          $output .= '<li class="active" role="tab" data-toggle="tab">';
        else
          $output .= '<li role="tab" data-toggle="tab">';

        $output .= '<a href="#custom-tab-' . $GLOBALS['tabs_count'] . '-' . sanitize_title( $tab[0] ) . '"  data-toggle="tab">' . $tab[0] . '</a></li>';
        $i++;
      }
        
        $output .= '</ul>';
		
        if (sdf_is_not($direction, 'below')) {
	      $output .= '<div class="tab-content">';
          $output .= parse_shortcode_content( $content );
          $output .= '</div>';
	    }
    } else {
      $output .= parse_shortcode_content( $content );
    }
	$output .= '</div>';
    
    return '<div class="' . $module_class . '">'.$output.'</div>';
	}
  



	/*--------------------------------------------------------------------------------------
    *
    * sdf_tab
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function tab_shortcode( $atts, $content = null ) {

    if( !isset($GLOBALS['current_tabs']) ) {
      $GLOBALS['current_tabs'] = $GLOBALS['tabs_count'];
      $state = 'active';
    } else {

      if( $GLOBALS['current_tabs'] == $GLOBALS['tabs_count'] ) {
        $state = ''; 
      } else {
        $GLOBALS['current_tabs'] = $GLOBALS['tabs_count'];
        $state = 'active';
      }
    }

	extract( shortcode_atts( array( 
		'title' => 'Tab', 
		'background' => '',
		'background_hover' => ''
		), $atts ));
		
    if($background != '') {
	$background = ' '.$background.'-color-bg';
	} else {
	$background = ' body-color-bg';
	}
	if($background_hover != '') { 
	$background_hover = ' hover-'.$background_hover.'-color-bg';
	} else {
	$background_hover = ' hover-body-color-bg';
	}
	
    return '<div id="custom-tab-' . $GLOBALS['tabs_count'] . '-'. sanitize_title( $title ) .'" class="tab-pane ' . $state . $background . $background_hover . '">'. parse_shortcode_content( $content ) .'</div>';
	} 



	/*--------------------------------------------------------------------------------------
    *
    * sdf_collapsibles
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function collapsibles_shortcode( $atts, $content = null ) {
    
    if( isset($GLOBALS['collapsibles_count']) )
      $GLOBALS['collapsibles_count']++;
    else
      $GLOBALS['collapsibles_count'] = 0;

    extract(shortcode_atts(array(
			"module_width" => '',
			"class" => ''
     ), $atts));
		 
		$class = ($class != '') ? ' '.$class : '';
		//$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
    
    preg_match_all( '/collapse title="([^\"]+)"/i', $content, $matches, PREG_OFFSET_CAPTURE );
    
    $tab_titles = array();
    if( isset($matches[1]) ){ $tab_titles = $matches[1]; }
    
    $output = '';
    
    if( count($tab_titles) ){
      $output .= '<div class="panel-group'.$class.'" id="accordion-' . $GLOBALS['collapsibles_count'] . '" role="tablist" aria-multiselectable="true">'.parse_shortcode_content( $content ).'</div>';
    } else {
      $output .= parse_shortcode_content( $content );
    }
    
    return $output;
	}
	


	/*--------------------------------------------------------------------------------------
    *
    * sdf_collapse
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function collapse_shortcode( $atts, $content = null ) {

    if( !isset($GLOBALS['current_collapse']) )
      $GLOBALS['current_collapse'] = 0;
    else 
      $GLOBALS['current_collapse']++;

    extract( shortcode_atts( array( 
		'title' => 'Tab', 
		'state' => '',
		'background' => '',
		'background_hover' => ''
		), $atts ));
    
    if($state == 'active') {
    $state = 'in';
	}
	if($background != '') {
	$background = ' '.$background.'-color-bg';
	} else {
	$background = ' body-color-bg';
	}
	if($background_hover != '') { 
	$background_hover = ' hover-'.$background_hover.'-color-bg';
	} else {
	$background_hover = ' hover-body-color-bg';
	}

    return '<div class="panel panel-default">'
      .'<div class="panel-heading" role="tab">'
        .'<a data-toggle="collapse" data-parent="#accordion-' . $GLOBALS['collapsibles_count'] . '" href="#collapse_' . $GLOBALS['current_collapse'] . '_'. sanitize_title( $title ) .'" aria-controls="collapse_' . $GLOBALS['current_collapse'] . '_'. sanitize_title( $title ) .'">'
        . $title
        .'</a>'
      .'</div>'
      .'<div id="collapse_' . $GLOBALS['current_collapse'] . '_'. sanitize_title( $title ) .'" class="panel-collapse collapse ' . $state . '" role="tabpanel">'
        .'<div class="panel-body' . $background . $background_hover . '">' . $content . '</div>'
      .'</div>'
    .'</div>';
	}
	
	  /*--------------------------------------------------------------------------------------
    *
    * sdf_icon
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function icon_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
		'module_width' => '',
		'top_margin' => '',
		'bottom_margin' => '',
		'icon' => '',
		'icon_alignment' => '',
		'icon_size' => '',
		'icon_color' => '',
		'icon_link' => '',
		'target' => '',
		'nofollow' => '',
		'entrance_animation' => '',
		'entrance_animation_duration' => '',
		'margins' => '',
		'class' => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		if($icon == 'no') 
			return false;
			
		$icon_color = ($icon_color) ? 'color:' . $icon_color . ';'  : '';
		$margin = '';
		if ($margins) {
			$margin = 'margin:' . $margins . ';';
		}
		$classes = '';
		$classes .= ($class != '') ? ' '.$class : '';
		if($icon_alignment == 'left') $classes .= ' text-left';
		if($icon_alignment == 'right') $classes .= ' text-right';
		if($icon_alignment == 'center') $classes .= ' text-center';
		
		$fa_classes = 'fa '.$icon;
		if($icon_size != '') $fa_classes .= ' ' . $icon_size;
		
		//fallback values
		$target = (!in_array($target, array('', 'no', 'None'))) ? ' target="'.$target.'"' : '';
		$link_nofollow = ($nofollow == 'yes') ? ' rel="nofollow"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		$data_animation = '';
		$data_duration = '';
		$data_animation = ( sdf_is_not($entrance_animation, 'No') ) ? ' data-animation="'.$entrance_animation.'"' : '';
		$data_duration = ( sdf_is_not($entrance_animation_duration, '') && $data_animation != '') ? ' data-duration="'.$entrance_animation_duration.'"' : '';
		$classes .= ($data_animation != '') ? ' viewport_animate animate_now' : '';
		
		if($icon_link == ''){	
			$mod_icon = '<div class="' . $classes . '"'.$styles.$data_animation. $data_duration.'><i class="' . $fa_classes . '" style="'.$icon_color.$margin.'"'.$data_animation. $data_duration.'></i></div>';
	  }
	  else {
			$icon_link = sdf_check_link_prefix($icon_link);
			$mod_icon = '<a href="' . $icon_link . '" class="btn-block ' . $classes . '"'.$styles.$data_animation. $data_duration.'><i class="' . $fa_classes . '" style="'.$icon_color.$margin.'" '.$target.' '.$link_nofollow .'></i></a> ';
	  }
		
			return '<div class="' . $module_class . '">' . $mod_icon . '</div>';
	}
	
	  /*--------------------------------------------------------------------------------------
    *
    * sdf_text_list
    *
    * @since 1.0
    * 
    *-------------------------------------------------------------------------------------*/
	function text_list_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
		'module_width' => '',
		'top_margin' => '',
		'bottom_margin' => '',
		'list_content' => '',
		'list_content_alignment' => '',
		'list_type' => '',
		'list_style' => '',
		'list_icon' => '',
		'icon_size' => '',
		'entrance_animation' => '',
		'entrance_animation_duration' => '',
		'class' => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		$li_align = ($list_content_alignment != '') ? 'text-'.$list_content_alignment : '';
			
		if( $list_type == 'ul' || $list_type == '' ) {
			$ul_class = '';
			if( $list_style == 'none' ) {
				$ul_class = 'list-unstyled';
			}
			elseif( $list_style == 'custom' ) {
				$ul_class = 'list-custom';
			}
			elseif( $list_style == 'icon' ) {
				$ul_class = 'fa-ul';
			}
			else {
				$ul_class = 'list-'.$list_style;
			}
			
			$output = '<ul class="'.$ul_class.'"'.$styles.'>';
			
		}
		else {
			$output = '<ol'.$styles.'>';
		}
		
		$list_icon = ( $list_style == 'icon' && $list_icon != 'no') ? '<i class="fa-li fa ' . $list_icon . ' ' . $icon_size . '"></i>' : '';
		
		$li_start = '<li class="' . $li_align . '">'.$list_icon;
		
		$li_end = '</li>';
		
		$list_items = preg_split('#(\R+)#', $content);
		
		foreach ( $list_items as $list_item ) {
			$output .= $li_start . $list_item . $li_end;
		}
		
		if( $list_type == 'ul' ) {
			$output .= '</ul>';
		}
		else {
			$output .= '</ol>';
		}
		
		return '<div class="' . $module_class . '">'.$output.'</div>';
		
	}
	
	/**
 * Hide email from Spam Bots using a shortcode.
 *
 * @param array  $atts    Shortcode attributes. Not used.
 * @param string $content The shortcode content. Should be an email address.
 *
 * @return string The obfuscated email address. 
 *  
 * @since 1.9.2
 */
function hide_email_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}

	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
	
	
	/*--------------------------------------------------------------------------------------
    *
    * sdf_latest_posts
    *
    * @since 1.0.0
    * 
    *-------------------------------------------------------------------------------------*/
	
	function latest_posts_shortcode( $atts, $content = null ) {

		global $wp_widget_factory;
		
		extract( shortcode_atts( array(
			'module_width' => '',
			'top_margin' => '',
			'bottom_margin' => '',
			'title' => 'Latest Posts',
			'layout_type' => '',
			'post_categories' => '',
			'rows' => '',
			'columns' => '',
			'layout' => '',
			'grid_layout' => '',
			'posts_filter' => '',
			'item_title_heading' => '',
			'list_item_image_position' => '',
			'info_display' => '',
			'show_overlay_title' => 'yes',
			'show_item_link_button' => 'yes',
			'show_prettyphoto_button' => 'yes',
			'thumbnails_shape' => 'rectangle',
			'thumbnails_size' => 'sdf-image-md-12',
			'entrance_animation' => '',
			'entrance_animation_duration' => '',
			'posts_count' => '',
			'posts_offset' => '',
			'word_count' => '',
			'show_images' => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$widget_name = 'Sdf_Latest_Posts_Widget';
		// layout fallback
		if ( isset($atts['layout']) ) $atts['grid_layout'] = $atts['layout'];
		// convert to array for grid_post_cats or list_post_cats
		$atts[$layout_type.'_post_cats'] = explode(',', $post_categories);
		
		$atts['lpw_layout_type'] = $layout_type;
		$atts[$layout_type.'_thumbnails_shape'] = $thumbnails_shape;
		$atts[$layout_type.'_thumbnails_size'] = $thumbnails_size;
		$atts[$layout_type.'_word_count'] = $word_count;
		$atts[$layout_type.'_posts_offset'] = $posts_offset;
		$atts[$layout_type.'_info_display'] = $info_display;
		$atts[$layout_type.'_entrance_animation'] = $entrance_animation;
		$atts[$layout_type.'_entrance_animation_duration'] = $entrance_animation_duration;
		$atts[$layout_type.'_item_title_heading'] = $item_title_heading;

		if (!($wp_widget_factory->widgets[$widget_name] instanceof WP_Widget)) {
			$wp_class = 'WP_Widget_'.ucwords(strtolower($class));
			
			if (!($wp_widget_factory->widgets[$wp_class] instanceof WP_Widget)) {
				return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct", SDF_TXT_DOMAIN),'<strong>'.$class.'</strong>').'</p>';
			} else {
				$class = $wp_class;
			}
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		ob_start();
		the_widget($widget_name, $atts, array(
			'widget_id'=>'sdf_latest_posts_widget-'.rand()
		));
		$output = ob_get_contents();
		ob_end_clean();
		return '<div class="' . $module_class . '"'.$styles.'>'.$output.'</div>';
		
	}
	
	
	/*--------------------------------------------------------------------------------------
    *
    * sdf_contact
    *
    * @since 1.0.0
    * 
    *-------------------------------------------------------------------------------------*/
	
	function contact_shortcode( $atts, $content = null ) {

		global $wp_widget_factory;
		
		$widget_name = 'Sdf_Contact_Form_Widget';
		$widget_id = 'sdf_contact_form_widget-1';
		
		extract( shortcode_atts( array(
			'module_width' => '',
			'bottom_margin' => '',
			'top_margin' => '',
			'title' => '',
			'enable_captcha' => false,
			'choose_form_layout' => '',
			'recipient_email' => ''
		), $atts ) );
		
		$top_margin = ($top_margin != '') ? 'margin-top:'.$top_margin.';' : '';
		$bottom_margin = ($bottom_margin != '') ? 'margin-bottom:'.$bottom_margin.';' : '';
		
		$styles = ( ($top_margin != '') || ($bottom_margin != '') ) ? ' style="' . $top_margin . $bottom_margin . '"' : '';
		
		$enable_captcha = filter_var( $enable_captcha, FILTER_VALIDATE_BOOLEAN );

		$atts['layout'] = $choose_form_layout;
		$atts['toggle_captcha'] = $enable_captcha;
		$atts['sc_contact'] = 1;

		if (!($wp_widget_factory->widgets[$widget_name] instanceof WP_Widget)) {
			$wp_class = 'WP_Widget_'.ucwords(strtolower($class));
			
			if (!($wp_widget_factory->widgets[$wp_class] instanceof WP_Widget)) {
				return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct", SDF_TXT_DOMAIN),'<strong>'.$class.'</strong>').'</p>';
			} else {
				$class = $wp_class;
			}
		}
		
		$module_class = ($module_width) ? $this->_moduleWidths[$module_width] : 'col-md-12';
		
		ob_start();
		the_widget($widget_name, $atts, array(
			'widget_id'=> $widget_id
		));
		$output = ob_get_contents();
		ob_end_clean();
		return '<div class="' . $module_class . '"'.$styles.'>'.$output.'</div>';
		
	}
	
	
  /*--------------------------------------------------------------------------------------
	*
	* sdf_twitter
	*
	* @since 1.0
	* 
	*-------------------------------------------------------------------------------------*/

function twitter_build($atts) {
    require_once (SDF_CORE . "/twitteroauth.php" );
    $atts = shortcode_atts(array(
        'consumerkey' => sdfGo()->sdf_get_global('twitter', 'consumerkey'),
        'consumersecret' => sdfGo()->sdf_get_global('twitter', 'consumersecret'),
        'accesstoken' => sdfGo()->sdf_get_global('twitter', 'accesstoken'),
        'accesstokensecret' => sdfGo()->sdf_get_global('twitter', 'accesstokensecret'),
        'cachetime' => '1',
        'username' => SDF_TXT_DOMAIN,
        'tweetstoshow' => '10',
            ), $atts);
    //check settings and die if not set
    if (empty($atts['consumerkey']) || empty($atts['consumersecret']) || empty($atts['accesstoken']) || empty($atts['accesstokensecret']) || !isset($atts['cachetime']) || empty($atts['username'])) {
        return '<strong>' . __('Due to Twitter API changes you must insert Twitter APP. Check SDF theme Options for Twitter API and insert keys', SDF_TXT_DOMAIN) . '</strong>';
    }
    //check if cache needs update
    $sdf_twitter_last_cache_time = get_option('sdf_twitter_last_cache_time_' . $atts['username']);
    $diff = time() - $sdf_twitter_last_cache_time;
    $crt = $atts['cachetime'] * 3600;

    //yes, it needs update			
    if ($diff >= $crt || empty($sdf_twitter_last_cache_time)) {
        $connection = new TwitterOAuth($atts['consumerkey'], $atts['consumersecret'], $atts['accesstoken'], $atts['accesstokensecret']);
        $tweets = $connection->get("https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" . $atts['username'] . "&count=10") or die('Couldn\'t retrieve tweets! Wrong username?');
        if (!empty($tweets->errors)) {
            if ($tweets->errors[0]->message == 'Invalid or expired token') {
                return '<strong>' . $tweets->errors[0]->message . '!</strong><br />'.__('You\'ll need to regenerate it <a href="https://dev.twitter.com/apps" target="_blank">here</a>!',SDF_TXT_DOMAIN);
            } else {
                return '<strong>' . $tweets->errors[0]->message . '</strong>';
            }
            return;
        }
        $tweets_array = array();
        for ($i = 0; is_array($tweets) && $i <= count($tweets); $i++) {
            if (!empty($tweets[$i])) {
                $tweets_array[$i]['created_at'] = $tweets[$i]->created_at;
                $tweets_array[$i]['text'] = $tweets[$i]->text;
                $tweets_array[$i]['status_id'] = $tweets[$i]->id_str;
            }
        }
        //save tweets to wp option 		
        update_option('sdf_twitter_tweets_' . $atts['username'], serialize($tweets_array));
        update_option('sdf_twitter_last_cache_time_' . $atts['username'], time());
        echo '<!-- twitter cache has been updated! -->';
    }
    //convert links to clickable format
    if (!function_exists('convert_links')) {

        function convert_links($status, $targetBlank = true, $linkMaxLen = 250) {
            // the target
            $target = $targetBlank ? " target=\"_blank\" " : "";
            // convert link to url
            $status = preg_replace("/((http:\/\/|https:\/\/)[^ )]+)/e", "'<a href=\"$1\" title=\"$1\" $target >'. ((strlen('$1')>=$linkMaxLen ? substr('$1',0,$linkMaxLen).'...':'$1')).'</a>'", $status);
            // convert @ to follow
            $status = preg_replace("/(@([_a-z0-9\-]+))/i", "<a href=\"http://twitter.com/$2\" title=\"Follow $2\" $target >$1</a>", $status);
            // convert # to search
            $status = preg_replace("/(#([_a-z0-9\-]+))/i", "<a href=\"https://twitter.com/search?q=$2\" title=\"Search $1\" $target >$1</a>", $status);
            // return the status
            return $status;
        }

    }
    //convert dates to readable format
    if (!function_exists('relative_time')) {

        function relative_time($a) {
            //get current timestampt
            $b = strtotime("now");
            //get timestamp when tweet created
            $c = strtotime($a);
            //get difference
            $d = $b - $c;
            //calculate different time values
            $minute = 60;
            $hour = $minute * 60;
            $day = $hour * 24;
            $week = $day * 7;
            if (is_numeric($d) && $d > 0) {
                //if less then 3 seconds
                if ($d < 3)
                    return (sdfGo()->sdf_get_global('twitter', 'sdf_car_rn') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_rn') : __('right now', SDF_TXT_DOMAIN));
                //if less then minute
                if ($d < $minute)
                    return floor($d) . (sdfGo()->sdf_get_global('twitter', 'sdf_car_sa') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_sa') : __(' seconds ago', SDF_TXT_DOMAIN));
                //if less then 2 minutes
                if ($d < $minute * 2)
                    return (sdfGo()->sdf_get_global('twitter', 'sdf_car_aoma') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_aoma') : __('about 1 minute ago', SDF_TXT_DOMAIN));
                //if less then hour
                if ($d < $hour)
                    return floor($d / $minute) . (sdfGo()->sdf_get_global('twitter', 'sdf_car_ma') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_ma') : __(' minutes ago', SDF_TXT_DOMAIN));
                //if less then 2 hours
                if ($d < $hour * 2)
                    return (sdfGo()->sdf_get_global('twitter', 'sdf_car_aoha') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_aoha') : __('about 1 hour ago', SDF_TXT_DOMAIN));
                //if less then day
                if ($d < $day)
                    return floor($d / $hour) . (sdfGo()->sdf_get_global('twitter', 'sdf_car_ha') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_ha') : __(' hours ago', SDF_TXT_DOMAIN));
                //if more then day, but less then 2 days
                if ($d > $day && $d < $day * 2)
                    return (sdfGo()->sdf_get_global('twitter', 'sdf_car_yes') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_yes') : __('yesterday', SDF_TXT_DOMAIN));
                //if less then year
                if ($d < $day * 365)
                    return floor($d / $day) . (sdfGo()->sdf_get_global('twitter', 'sdf_car_da') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_da') : __(' days ago', SDF_TXT_DOMAIN));
                //else return more than a year
                return __("over a year ago","themewaves");
                return (sdfGo()->sdf_get_global('twitter', 'sdf_car_oaya') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_oaya') : __('over a year ago', SDF_TXT_DOMAIN));
            }
        }

    }
    $sdf_twitter_tweets = maybe_unserialize(get_option('sdf_twitter_tweets_' . $atts['username']));
    return $sdf_twitter_tweets;
}

    function twitter_shortcode($atts, $content) {
        $sdf_twitter_tweets = $this->twitter_build($atts);
        if (is_array($sdf_twitter_tweets)) {
            $output = '<div class="sdf-twitter">';
            $output.='<ul class="twts">';
            $fctr = '1';
            foreach ($sdf_twitter_tweets as $tweet) {
                $output.='<li><span>' . $this->convert_links($tweet['text']) . '</span><br /><a class="twitter_time" target="_blank" href="http://twitter.com/' . $atts['username'] . '/statuses/' . $tweet['status_id'] . '">' . $this->relative_time($tweet['created_at']) . '</a></li>';
                if ($fctr == $atts['tweetstoshow']) {
                    break;
                }
                $fctr++;
            }
            $output.='</ul>';
            $output.='<div class="twitter-follow">'  . (sdfGo()->sdf_get_global('twitter', 'sdf_car_follow') ? sdfGo()->sdf_get_global('twitter', 'sdf_car_follow') : __('Follow Us', SDF_TXT_DOMAIN)) . ' - <a target="_blank" href="http://twitter.com/' . $atts['username'] . '">@' . $atts['username'] . '</a></div>';
            $output.='</div>';
            return $output;
        } else {
            return $sdf_twitter_tweets;
        }
    }
		
	/*--------------------------------------------------------------------------------------
	*
	* sdf_current_year
	* 
	*-------------------------------------------------------------------------------------*/
	function current_year_shortcode( $atts, $content = null ) {
		
	    $date = getdate();
			return $date['year'];
	}
  
  	/**
	* Convert to HTML entities
	* @param str $content Text content of the shortcode
	* @return str Text content of the shortcode, converted to HTML entities
	*/
	private function clean_code_content ( $content ) {
		return htmlentities( trim( $content ), ENT_NOQUOTES, 'UTF-8', false );
	}

}

new SDFShortcodes();