<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: class_Title.php
 * Descripton: Implementation of Theme Navigation Options 
 *
 * AUTHOR:  SEO Design Framework
 *   
 */
 
function sdf_page_nav(){

	if( isset(sdfGo()->_sdfPageArgs['header']['toggle_custom_nav']) && isset(sdfGo()->_sdfPageArgs['header']['custom_nav_name']) ) {
		if( sdfGo()->_sdfPageArgs['header']['custom_nav_name'] != 'wpu-select-default' ) {	
			return sdfGo()->_sdfPageArgs['header']['custom_nav_name'];
		} 
		else {
			return false;
		}
	}	
	//fallback
	elseif( isset(sdfGo()->_sdfPageArgs['navigation']['toggle_custom_nav']) && isset(sdfGo()->_sdfPageArgs['navigation']['custom_nav_name']) ) {
		if( sdfGo()->_sdfPageArgs['navigation']['custom_nav_name'] != 'wpu-select-default' ) {	
			return sdfGo()->_sdfPageArgs['navigation']['custom_nav_name'];
		} 
		else {
			return false;
		}
	}
	else {
		return false;
	}
} // end sdf_page_nav

function sdf_do_nav($secondary_nav){

	if( isset(sdfGo()->_sdfPageArgs['header']['toggle_nav']) ) {
		$toggle_nav = sdfGo()->sdf_get_option('header', 'toggle_nav');
	}
	elseif( isset(sdfGo()->_sdfPageArgs['navigation']['toggle_nav']) ) {
		$toggle_nav = sdfGo()->sdf_get_option('navigation', 'toggle_nav');
	}
	else {
		$toggle_nav = "1";
	}

	if( $toggle_nav != "0" ) {
		sdf_before_nav();
		if(sdfGo()->sdf_get_global('navigation','menu_alignment') == 'left'):
			$xtr_class = 'navbar-left';
		elseif(sdfGo()->sdf_get_global('navigation','menu_alignment') == 'right'):
			$xtr_class = 'navbar-right';
		elseif(sdfGo()->sdf_get_global('navigation','menu_alignment') == 'center'):
			$xtr_class = 'navbar-center';
		else:
			$xtr_class = 'navbar-left';
		endif;
		
		$xtr_class = 'nav navbar-nav '.$xtr_class;

		if(sdfGo()->sdf_get_global('navigation','menu_layout') != ''):
			$navbar_class = 'navbar-default';
		else:
			$navbar_class = 'navbar-default';
		endif;
		
		$navbar_layout_classes = sdfGo()->_wpuGlobal->_ultMenuLayouts['classes'];
		
		$extra_navbar_container = ( in_array( $navbar_layout_classes[sdfGo()->sdf_get_global('navigation','menu_layout')], array( 'navbar-default', 'navbar-sticky' ) ) ) ? 'container-wide' : 'container-fluid';

		$theme_logo_heading = sdfGo()->sdf_get_global('typography','logo_heading');
		if (in_array($theme_logo_heading, array('', 'none'))) { 
			$theme_logo_heading = 'div';
		}
		
		?>
	
		<div id="sdf-nav" class="container-wide">

		<a class="sr-only" href="#content" title="Skip navigation to the content">Skip to content</a>
		<!-- .skip-link --> 
		
		<nav id="wpu-navigation" class="navbar <?php echo $navbar_class; ?>" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
		<div class="navbar-header">
		
		<?php if ($himg = sdf_get_logo_image()): ?>
			<div class="logo col-xs-8 logo-sm"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_html( get_bloginfo('name'), 1 ); ?>" rel="home"><img src="<?php echo $himg; ?>" class="img-responsive" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" /></a></div>
		<?php else : ?>
			<<?php echo $theme_logo_heading; ?> class="logo col-xs-8 logo-sm"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_html( get_bloginfo('name'), 1 ); ?>" rel="home"><?php echo get_bloginfo( 'name' ); ?></a></<?php echo $theme_logo_heading; ?>>
		<?php endif; ?>
		
		<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		</div>
		<div class="collapse navbar-collapse">
			<div class="<?php echo $extra_navbar_container; ?>">
        <?php 
				if (function_exists('wp_nav_menu')) {
					if( sdf_page_nav()){
						$navargs = array(  
						'container_class' => '', 
						'menu_class' => $xtr_class,
						'fallback_cb' => 'sdf_nav_default',
						'theme_location' => 'primary', 
						'menu' => sdf_page_nav(),
						'walker' => new SDF_Sublevel_Walker
						);
					}else{
						$navargs = array( 
						'container_class' => '', 
						'menu_class' => $xtr_class,
						'fallback_cb' => 'sdf_nav_default',
						'theme_location' => 'primary',
						'walker' => new SDF_Sublevel_Walker 
						);
					}
					wp_nav_menu($navargs );
					
					// hidden secondary nav
					if ( has_nav_menu( 'secondary' ) ) {
						if( sdf_page_nav()){
							$navargs = array(  
							'container_class' => '', 
							'menu_class' => $xtr_class.' hidden-lg',
							'fallback_cb' => 'sdf_nav_default',
							'theme_location' => 'secondary', 
							'menu' => sdf_page_nav(),
							'walker' => new SDF_Sublevel_Walker
							);
						}else{
							$navargs = array( 
							'container_class' => '', 
							'menu_class' => $xtr_class.' hidden-lg',
							'fallback_cb' => 'sdf_nav_default',
							'theme_location' => 'secondary',
							'walker' => new SDF_Sublevel_Walker 
							);
						}
						wp_nav_menu($navargs );
					}
				} else {
					sdf_nav_default();
				}
				?>
			</div>
		</div>
		</nav>
		</div>
		
	<?php
	sdf_after_nav();
	}
	
} // end sdf_do_nav

function sdf_do_nav_sec(){

	if( isset(sdfGo()->_sdfPageArgs['header']['toggle_nav']) ) {
		$toggle_nav = sdfGo()->sdf_get_option('header', 'toggle_nav');
	}
	elseif( isset(sdfGo()->_sdfPageArgs['navigation']['toggle_nav']) ) {
		$toggle_nav = sdfGo()->sdf_get_option('navigation', 'toggle_nav');
	}
	else {
		$toggle_nav = "1";
	}

	if( $toggle_nav != "0" ) {
		sdf_before_nav();
		if(sdfGo()->sdf_get_global('navigation','menu_alignment_2') == 'left'):
			$xtr_class = 'navbar-left';
		elseif(sdfGo()->sdf_get_global('navigation','menu_alignment_2') == 'right'):
			$xtr_class = 'navbar-right';
		elseif(sdfGo()->sdf_get_global('navigation','menu_alignment_2') == 'center'):
			$xtr_class = 'navbar-center';
		else:
			$xtr_class = 'navbar-left';
		endif;
		
		$xtr_class = 'nav navbar-nav '.$xtr_class;

		if(sdfGo()->sdf_get_global('navigation','menu_layout') != ''):
			$navbar_class = 'navbar-default';
		else:
			$navbar_class = 'navbar-default';
		endif;
		
		$navbar_layout_classes = sdfGo()->_wpuGlobal->_ultMenuLayouts['classes'];
		
		$extra_navbar_container = ( in_array( $navbar_layout_classes[sdfGo()->sdf_get_global('navigation','menu_layout')], array( 'navbar-default', 'navbar-sticky' ) ) ) ? 'container-wide' : 'container-fluid';

		$theme_logo_heading = sdfGo()->sdf_get_global('typography','logo_heading');
		if (in_array($theme_logo_heading, array('', 'none'))) { 
			$theme_logo_heading = 'div';
		}
		
		?>
	
		<div id="sdf-nav" class="container-wide">

		<a class="sr-only" href="#content" title="Skip navigation to the content">Skip to content</a>
		<!-- .skip-link --> 
		
		<nav id="wpu-navigation" class="navbar <?php echo $navbar_class; ?>" role="navigation" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
		<div class="collapse navbar-collapse">
			<div class="<?php echo $extra_navbar_container; ?>">
        <?php 
		 
		
		if ( has_nav_menu( 'secondary' ) ) {
			if (function_exists('wp_nav_menu')) {
				if( sdf_page_nav()){
					$navargs = array(  
					'container_class' => '', 
					'menu_class'=> $xtr_class,
					'fallback_cb' => 'sdf_nav_default',
					'theme_location' => 'secondary', 
					'menu' => sdf_page_nav(),
					'walker' => new SDF_Sublevel_Walker
					);
				}else{
					$navargs = array( 
					'container_class' => '', 
					'menu_class'=>  $xtr_class,
					'fallback_cb' => 'sdf_nav_default',
					'theme_location' => 'secondary',
					'walker' => new SDF_Sublevel_Walker 
					);
				}
				wp_nav_menu($navargs );
			}
		}
		?>
			</div>
		</div>
		</nav>
		</div>
		
	<?php
	sdf_after_nav();
	}
	
} // end sdf_do_nav_sec
function sdf_do_nav_before(){
	
}
function sdf_do_nav_after(){
	
}
function sdf_nav_default(){ 

	$wide_menu = (SDF_PAGE_WIDTH == "wide") ? ' container' : '' ;
	 
	$default_nav = array(
	'echo'=>false,
	'menu_class'=>'collapse navbar-collapse'.$wide_menu,
	'sort_column'=>'menu_order'
	);

	$sdf_menu = wp_page_menu($default_nav);

	echo $sdf_menu;
	
} // end sdf_nav_default

class SDF_Sublevel_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        if ($depth === 0) {
					$output .= '<span class="sdf-dropdown-top"><span class="sdf-dropdown-arrow-wrap"><span class="sdf-dropdown-arrow"></span></span></span><ul class="sub-menu">';
        }
				else {
					$output .= '<ul class="sub-menu">';
				}
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "</ul>\n";
    }
}

/* Search Icon/Form */
if(!function_exists('sdf_search_icon'))
{
	//first append search item to main menu
	add_filter( 'wp_nav_menu_items', 'sdf_search_icon', 10, 2 );
	add_filter( 'sdf_nav_default', 'sdf_search_icon', 10, 2 );

	function sdf_search_icon ( $items, $args )
	{	
		if(sdfGo()->_wpuGlobal->_currentSettings['navigation']['menu_settings']['theme_navbar_append_search'] == "yes"){
	    if ((is_object($args) && $args->theme_location == 'primary') || (is_string($args) && $args = "fallback_menu")){
	        $items .= '<li id="menu-item-search" class="hidden-xs menu-item menu-item-has-children menu-item-search"><a data-searchform=""><i class="fa fa-search"></i></a>
					<span class="sdf-dropdown-top">
					<span class="sdf-dropdown-arrow-wrap">
					<span class="sdf-dropdown-arrow"></span>
					</span>
					</span>
					<ul class="sub-menu searchform">
					<li id="menu-item-searchform" class="menu-item menu-item-searchform">'.get_search_form(false).'</li>
					</ul>
					</li>';
	    }
	    return $items;
		}
		else {
		  return $items;
		}
	}
}