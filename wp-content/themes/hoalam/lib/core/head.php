<?php
 function sdf_do_meta_title(){
	 if ( is_front_page() ){
			echo htmlentities(get_bloginfo('name').' | '.get_bloginfo('description'));
	} else {
		wp_title( '|', true, 'right' );
		echo esc_html( get_bloginfo('name'), 1 );
	}
 }


function sdf_do_js_head(){
	if(sdfGo()->sdf_get_global('misc', 'custom_js_header')):
			echo do_shortcode(stripslashes( sdfGo()->sdf_get_global('misc','custom_js_header') ));
	endif;
	if(sdfGo()->sdf_get_page('misc','custom_js_header')):
			echo do_shortcode(stripslashes( sdfGo()->sdf_get_page('misc','custom_js_header') ));
	endif;
}

function sdf_head_html(){ ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title><?php sdf_meta_title(); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
if( is_singular() && get_option( 'thread_comments' )) {
	wp_enqueue_script( 'comment-reply' );
}

wp_head();
sdf_head();
?>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" type="text/css" media="all" />
<script src="<?php echo SDF_BOOTSTRAP; ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>

<?php
$favtouch_favicon = (sdfGo()->sdf_get_global('favtouch_icons','favicon') == "") ? SDF_PARENT_URL . '/lib/img/logos/favicon.ico' : sdfGo()->sdf_get_global('favtouch_icons','favicon');
$favtouch_apple_touch_icon_57 = (sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_57') == "") ? SDF_PARENT_URL . '/lib/img/logos/apple-touch-icon-57-precomposed.png' : sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_57');
$favtouch_apple_touch_icon_72 = (sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_72') == "") ? SDF_PARENT_URL . '/lib/img/logos/apple-touch-icon-72-precomposed.png' : sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_72');
$favtouch_apple_touch_icon_114 = (sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_114') == "") ? SDF_PARENT_URL . '/lib/img/logos/apple-touch-icon-114-precomposed.png' : sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_114');
$favtouch_apple_touch_icon_144 = (sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_144') == "") ? SDF_PARENT_URL . '/lib/img/logos/apple-touch-icon-144-precomposed.png' : sdfGo()->sdf_get_global('favtouch_icons','apple_touch_icon_144');

echo '<link rel="shortcut icon" href="' . $favtouch_favicon . '"/>';
echo '<link rel="apple-touch-icon-precomposed" href="' . $favtouch_apple_touch_icon_57 . '"/>';
echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="' . $favtouch_apple_touch_icon_72 . '"/>';
echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="' . $favtouch_apple_touch_icon_114 . '"/>';
echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="' . $favtouch_apple_touch_icon_144 . '"/>';
	
?>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<?php 

}
function sdf_do_head(){
}
