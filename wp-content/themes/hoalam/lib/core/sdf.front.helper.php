<?php

if(!function_exists('sdf_get_attachment_id_by_url')) {
	/**
	* Get an attachment ID given a URL.
	* 
	* @param string $url
	*
	* @return int Attachment ID on success, 0 on failure
	*/
	
	function sdf_get_attachment_id_by_url($url) 
	{
		$attachment_id = 0;

	$dir = wp_upload_dir();

	if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
	
		if ( version_compare( $GLOBALS['wp_version'], '3.9.99', '>' ) ) {
			// NEW from WP 4.0
			$attachment_id = attachment_url_to_postid( $url );
		}
		else {
		
			$file = basename( $url );

			$query_args = array(
				'post_type'   => 'attachment',
				'post_status' => 'inherit',
				'fields'      => 'ids',
				'meta_query'  => array(
					array(
						'value'   => $file,
						'compare' => 'LIKE',
						'key'     => '_wp_attachment_metadata',
					),
				)
			);

			$query = new WP_Query( $query_args );

			if ( $query->have_posts() ) {

				foreach ( $query->posts as $post_id ) {

					$meta = wp_get_attachment_metadata( $post_id );

					$original_file       = basename( $meta['file'] );
					$cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );

					if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
						$attachment_id = $post_id;
						break;
					}

				}

			}
		}

	}

	return $attachment_id;
	}
}

/*
 * Get posts media URLs
 */
if(!function_exists('sdf_post_media_urls'))
{
	function sdf_post_media_urls( $target = '' ) {
		if ( empty($target) )  {
			$target = get_the_content();
		} elseif ( is_numeric($target) ) {
			$post = get_post($target);
			$target = empty($post) ? FALSE : $post->post_content;
		}
		if ( empty($target) ) return false;
		// apply filters to support shortcodes
		$target = apply_filters('the_content', $target);
		// extract urls
		$urls = wp_extract_urls( $content );
		$out = array();
		if ( ! empty($urls) ) {
			// parse only several media extensions, but allow filtering 
			$exts = apply_filters( 'post_media_urls_exts', array('jpg','gif','png','pdf') );
			foreach( $urls as $url ) {
				$ext = strtolower( pathinfo( basename($url) , PATHINFO_EXTENSION ) );
				if ( $ext === 'jpeg' ) $ext = 'jpg';
				if ( ! empty($ext) && in_array( $ext, $exts) ) {
					if ( ! isset($out[$ext]) ) $out[$ext] = array();
					$out[$ext][] = $url;
				}
			}
		}
		return $out;
	}
}

if(!function_exists('sdf_color_hex_light'))
{
	/**
	 *  Convert a hexa decimal color code to its darker or lighter value
	 *  @param string $color hex color code
	 *  @param string $shade darker or lighter
	 *  @param int $amount how much darker or lighter
	 *  @return string returns the converted string
	 */
 	function sdf_color_hex_light($color, $shade, $amount)
 	{

 		//remove # from the begiining if available and make sure that it gets appended again at the end if it was found
 		$newcolor = "";
 		$prepend = "";
 		if(strpos($color,'#') !== false)
 		{
 			$prepend = "#";
 			$color = substr($color, 1, strlen($color));
 		}

 		//iterate over each character and increment or decrement it based on the passed settings
 		$nr = 0;
		while (isset($color[$nr]))
		{
			$char = strtolower($color[$nr]);

			for($i = $amount; $i > 0; $i--)
			{
				if($shade == 'lighter')
				{
					switch($char)
					{
						case '9': $char = 'a'; break;
						case 'f': $char = 'f'; break;
						default: $char++;
					}
				}
				else if($shade == 'darker')
				{
					switch($char)
					{
						case 'a': $char = '9'; break;
						case '0': $char = '0'; break;
						default: $char = chr(ord($char) - 1 );
					}
				}
			}
			$nr ++;
			$newcolor.= $char;
		}

		$newcolor = $prepend.$newcolor;
		return $newcolor;
	}
}

if(!function_exists('sdf_page_holder_ID'))
{
	/**
	 *  Get page ID for e.g. blog page
	 */
 	function sdf_page_holder_ID()
 	{
 		global $post;

    $page_ID = null;
		//check if post is object otherwise you're not in singular post
    if( is_object($post) ){
			$page_ID = get_the_ID();
		} 
		if( is_home() && get_option( 'page_for_posts' ) ) { 
			$page_ID = get_option( 'page_for_posts' ); 
		}
		elseif(is_404()) {
			$page_ID = 'error404';
		}
		elseif(is_search()) {
			$page_ID = 'search';
		}
		
		return $page_ID;
	}
}

if(!function_exists('sdf_page_holder_title'))
{
	/**
	 *  checks which archive we are viewing and returns the title
	 */

	function sdf_page_holder_title()
	{
		$output = "";

		if ( is_category() )
		{
			$output = single_cat_title('',false);
		}
		elseif (is_home() && is_front_page()) {
			$output = get_option('blogname');
		}
		elseif (is_day())
		{
			$output = __('Archive for day:', SDF_TXT_DOMAIN)." ".get_the_time( __('F jS, Y', SDF_TXT_DOMAIN) );
		}
		elseif (is_month())
		{
			$output = __('Archive for month:', SDF_TXT_DOMAIN)." ".get_the_time( __('F, Y', SDF_TXT_DOMAIN) );
		}
		elseif (is_year())
		{
			$output = __('Archive for year:', SDF_TXT_DOMAIN)." ".get_the_time( __('Y', SDF_TXT_DOMAIN) );
		}
		elseif (is_search())
		{
			global $wp_query;
			if(!empty($wp_query->found_posts))
			{
				if($wp_query->found_posts > 1)
				{
					$output =  $wp_query->found_posts ." ". __('search results for:', SDF_TXT_DOMAIN)." ".esc_attr( get_search_query() );
				}
				else
				{
					$output =  $wp_query->found_posts ." ". __('search result for:', SDF_TXT_DOMAIN)." ".esc_attr( get_search_query() );
				}
			}
			else
			{
				if(!empty($_GET['s']))
				{
					$output = __('Search results for:', SDF_TXT_DOMAIN)." ".esc_attr( get_search_query() );
				}
				else
				{
					$output = __('To search the site please enter a valid term', SDF_TXT_DOMAIN);
				}
			}

		}
		elseif (is_author())
		{
			$curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));
			$output = __('Author:', SDF_TXT_DOMAIN)." ";
			
			if($curauth->first_name != "" || $curauth->last_name != "") {
						$output .= $curauth->first_name . " " . $curauth->last_name;
					} else {
						$output .= $curauth->display_name;
					}

		}
		elseif (is_tag())
		{
			$output = single_term_title('',false)." ".__('Tag', SDF_TXT_DOMAIN);
		}
		//is WooCommerce installed and is shop or single product page
		elseif( is_woocommerce_active() && (is_shop() || is_singular('product'))) {
			//get shop page id from options table
			$shop_id = get_option('woocommerce_shop_page_id');

			//get shop page and get it's title if set
			$shop = get_post($shop_id);
			if(isset($shop->post_title) && $shop->post_title !== '') {
				$output = $shop->post_title;
			}

		}
		//is WooCommerce installed and is current page product archive page
		elseif( is_woocommerce_active() && (is_product_category() || is_product_tag())) {
			global $wp_query;

			//get current taxonomy and it's name and assign to title
			$tax 			= $wp_query->get_queried_object();
			$category_title = $tax->name;
			$output 			= $category_title;
		}
		//is current page some archive page
		elseif (is_archive()) {
			$output = __('Archive', SDF_TXT_DOMAIN);
		}
		elseif(is_tax())
		{
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$output = __('Archive for:', SDF_TXT_DOMAIN)." ".$term->name;
		}

		//current page is regular page
		else {
			$id = sdf_page_holder_ID();
			$output = get_the_title($id);
		}

		if (isset($_GET['paged']) && !empty($_GET['paged']))
		{
			$output .= " (".__('Page', SDF_TXT_DOMAIN)." ".$_GET['paged'].")";
		}

		return $output;
	}
}