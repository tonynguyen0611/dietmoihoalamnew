<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: class_layout.php
 * DESCRIPTIONS: Theme Layout Options.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: ULTLayout
 * Descripton: Implementation of Theme Layout Options    
 */
  define('SDF_SECTION_WRAP', sdf_class("section"));
 // Layout
function sdf_do_quarterd_before() {
	if(sdfGo()->sdf_get_option('layout','theme_layout') == 'quarter_col'):
		echo apply_filters("sdf_quarter_before","<div".SDF_SECTION_WRAP.">");
	endif;
}

function sdf_do_quarterd_after() {
	if(sdfGo()->sdf_get_option('layout','theme_layout') == 'quarter_col'):
		echo apply_filters("sdf_quarter_after","</div>");
	endif;
}

function sdf_class($section){

	$page = sdfGo()->sdf_is_permissible('layout','toggle_page_layout');

	if($page) {
		$pageWidth = (sdfGo()->sdf_get_page('layout', 'page_width')) ? sdfGo()->sdf_get_page('layout', 'page_width') : 'wide';
		$pageLayout = (sdfGo()->sdf_get_page('layout', 'theme_layout')) ? sdfGo()->sdf_get_page('layout', 'theme_layout') : 'no_col';
		$pageSidebars = (sdfGo()->sdf_get_page('layout', 'page_sidebars')) ? sdfGo()->sdf_get_page('layout', 'page_sidebars') : '3';
	} else {
		$pageWidth = (sdfGo()->sdf_get_global('layout', 'page_width')) ? sdfGo()->sdf_get_global('layout', 'page_width') : 'wide';
		$pageLayout = (sdfGo()->sdf_get_global('layout', 'theme_layout')) ? sdfGo()->sdf_get_global('layout', 'theme_layout') : 'no_col';
		$pageSidebars = (sdfGo()->sdf_get_global('layout', 'page_sidebars')) ? sdfGo()->sdf_get_global('layout', 'page_sidebars') : '3';
	}
			
	try {	

		if($section):
			switch($section):
			case "left":
				switch($pageLayout){
					case "col_3":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;	
					case "col_right_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "col_left_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "ssc_left":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "css_right":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "no_col":
						return false;
					break;
					case "custom":
					if($pageWidth == 'fluid'){
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					}else{
						return "c-left ";
					}
					break;
					default:
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;	
				}
			break;
			case "right":
				switch($pageLayout){
					case "col_3":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;	
					case "col_right_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "col_left_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "ssc_left":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "css_right":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					break;
					case "no_col":
						return false;
					break;
					case "custom":
					if($pageWidth == 'fluid'){
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-5 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-3 ";
						elseif($pageSidebars == '2'):	
							return "col-md-2 ";
						elseif($pageSidebars == '1'):	
							return "col-md-1 ";
						else:
							return "col-md-3 ";
						endif;
					}else{
						return "c-right ";
					}
						
					break;
					default:
						return "col-md-3";
					break;	
				}
			break;
			case "main":
				switch($pageLayout){
					case "col_3":
						if($pageSidebars == '6'):
							return " ";
						elseif($pageSidebars == '5'):
							return "col-md-2 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-6 ";
						elseif($pageSidebars == '2'):	
							return "col-md-8 ";
						elseif($pageSidebars == '1'):	
							return "col-md-10 ";
						else:
							return "col-md-6 ";
						endif;
					break;	
					case "col_right_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-7 ";	
						elseif($pageSidebars == '4'):
							return "col-md-8 ";
						elseif($pageSidebars == '3'):	
							return "col-md-9 ";
						elseif($pageSidebars == '2'):	
							return "col-md-10 ";
						elseif($pageSidebars == '1'):	
							return "col-md-11 ";
						else:
							return "col-md-9 ";
						endif;
					break;
					case "col_left_2":
						if($pageSidebars == '6'):
							return "col-md-6 ";
						elseif($pageSidebars == '5'):
							return "col-md-7 ";	
						elseif($pageSidebars == '4'):
							return "col-md-8 ";
						elseif($pageSidebars == '3'):	
							return "col-md-9 ";
						elseif($pageSidebars == '2'):	
							return "col-md-10 ";
						elseif($pageSidebars == '1'):	
							return "col-md-11 ";
						else:
							return "col-md-9 ";
						endif;
					break;
					case "ssc_left":
						if($pageSidebars == '6'):
							return " ";
						elseif($pageSidebars == '5'):
							return "col-md-2 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-6 ";
						elseif($pageSidebars == '2'):	
							return "col-md-8 ";
						elseif($pageSidebars == '1'):	
							return "col-md-10 ";
						else:
							return "col-md-6 ";
						endif;
					break;
					case "css_right":
						if($pageSidebars == '6'):
							return " ";
						elseif($pageSidebars == '5'):
							return "col-md-2 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-6 ";
						elseif($pageSidebars == '2'):	
							return "col-md-8 ";
						elseif($pageSidebars == '1'):	
							return "col-md-10 ";
						else:
							return "col-md-6 ";
						endif;
					break;
					case "no_col":
						return "col-md-12 ";
					break;
					case "custom":
					if($pageWidth == 'fluid'){
						if($pageSidebars == '6'):
							return " ";
						elseif($pageSidebars == '5'):
							return "col-md-2 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-6 ";
						elseif($pageSidebars == '2'):	
							return "col-md-8 ";
						elseif($pageSidebars == '1'):	
							return "col-md-10 ";
						else:
							return "col-md-6 ";
						endif;
					}else{
						return " c-main";
					}
					break;
					default:
						if($pageSidebars == '6'):
							return " ";
						elseif($pageSidebars == '5'):
							return "col-md-2 ";	
						elseif($pageSidebars == '4'):
							return "col-md-4 ";
						elseif($pageSidebars == '3'):	
							return "col-md-6 ";
						elseif($pageSidebars == '2'):	
							return "col-md-8 ";
						elseif($pageSidebars == '1'):	
							return "col-md-10 ";
						else:
							return "col-md-6 ";
						endif;
					break;	
				}
			break;
			default:
			break;
		endswitch;				
	endif;
		
	} catch (Exception $e){}			
}

function getSectionClass($section) {
	if(sdf_is_sidebar($section)){
		return ' class="col-md-8"';
	}else{
		return ' class="col-md-12"';	
	}
}

function sdf_is_sidebar($location){
	try {

		$page = sdfGo()->sdf_is_permissible('layout','toggle_page_layout');

	$pageLayout =  sdfGo()->sdf_get_option('layout','theme_layout');
	
	if($location){
	switch($location){
	case "left":
		$tmpLeft = sdfGo()->sdf_get_option('layout','custom_left');

		if($page) {
			$pageLayout = (sdfGo()->sdf_get_page('layout', 'theme_layout')) ? sdfGo()->sdf_get_page('layout', 'theme_layout') : 'no_col';
		} else {
			$pageLayout = (sdfGo()->sdf_get_global('layout', 'theme_layout')) ? sdfGo()->sdf_get_global('layout', 'theme_layout') : 'no_col';
		}
		
		if((int)$tmpLeft === (int)0 && $pageLayout == 'custom'){
			return false;
		}
		
		$noSidebars = array("col_right_2","no_col");
		if(in_array($pageLayout,$noSidebars))
			return false;
		else
			return true;
	break;
	
	case "right":
	
		$tmpRight = sdfGo()->sdf_get_option('layout','custom_right');
		
		if($page) {
			$pageLayout = (sdfGo()->sdf_get_page('layout', 'theme_layout')) ? sdfGo()->sdf_get_page('layout', 'theme_layout') : 'no_col';
		} else {
			$pageLayout = (sdfGo()->sdf_get_global('layout', 'theme_layout')) ? sdfGo()->sdf_get_global('layout', 'theme_layout') : 'no_col';
		}

		if((int)$tmpRight === (int)0 && $pageLayout == 'custom'){
				return false;
		}

		$noSidebars = array("col_left_2","no_col");
		if(in_array($pageLayout,$noSidebars))
			return false;
		else
			return true;
			
	break;
	
	case "section":
		if($pageLayout == 'quarter_col'){
			return true;
			 } else {
			return false;
		}
	break;
	default:
	break;	
	}
	}else{ return false; }
	
	} catch (Exception $e){}
}

function sdf_placeholder($section){
	$toggle_placeholder =  sdfGo()->sdf_get_global('misc','toggle_placeholders');
	$output = '';
	if($toggle_placeholder):
		if(array_key_exists($section, sdfGo()->_wpuGlobal->_sdfSidebars)):
				
			$output .= '<div class="area_placeholder"><h3>'.sdfGo()->_wpuGlobal->_sdfSidebars[$section].'</h3>';
			$output .= '<p>This area is widgetized! Please add your widgets into "<strong>'.sdfGo()->_wpuGlobal->_sdfSidebars[$section].'</strong>" on <strong>Appearance > Widgets</strong> at your dashboard.</p></div>';
			
		endif;
	endif;
	
	return $output;
}