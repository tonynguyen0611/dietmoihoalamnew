<?php 

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
 * THEME NAME: SEO Design Framework 
 *
 * TYPE: sidebars.php
 * DESCRIPTIONS: Sidebars Rendering    
 *
 * AUTHOR:  SEO Design Framework
 *    
 */
 
function sdf_do_before_sidebar_right(){}
function sdf_do_sidebar_right(){ sdf_get_right_sidebar(); }
function sdf_do_after_sidebar_right(){}

function sdf_do_before_sidebar_left(){}
function sdf_do_sidebar_left(){	sdf_get_left_sidebar(); }
function sdf_do_after_sidebar_left(){}



function sdf_get_left_sidebar(){
	
if(sdf_is_sidebar("left")):
	
	//parallax
	$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
	$parallax_left = sdfGo()->sdf_get_option('typography', 'left_widget_area_par_bg_ratio');
	$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_left != '') ? ' data-stellar-background-ratio="'.$parallax_left.'"' : '';
	$parallax = (sdfGo()->sdf_get_option('typography', 'left_widget_area_custom_bg') == 'parallax') ? $parallax : '';
			
	$styled_widgets = (sdfGo()->sdf_get_option('typography', 'left_widget_area_wg_custom_style') == 'yes') ? ((sdfGo()->sdf_get_option('typography', 'left_widget_area_custom_bg') == 'yes') ? '' : ' styled_widgets') : '';
	echo '<aside id="left-widget-area" class="default-widget-area '.sdf_class('left').$styled_widgets.'"'.$parallax.' itemtype="https://schema.org/WPSideBar" itemscope="itemscope" role="complementary">';
	sdf_before_sidebar_left();
	get_sidebar('left'); 
	sdf_after_sidebar_left();	
	echo '</aside>';
	
endif;
}

function sdf_get_right_sidebar(){

if(sdf_is_sidebar("right")):
	
	//parallax
	$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
	$parallax_right = sdfGo()->sdf_get_option('typography', 'right_widget_area_par_bg_ratio');
	$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_right != '') ? ' data-stellar-background-ratio="'.$parallax_right.'"' : '';
	$parallax = (sdfGo()->sdf_get_option('typography', 'right_widget_area_custom_bg') == 'parallax') ? $parallax : '';
	
	$styled_widgets = (sdfGo()->sdf_get_option('typography', 'right_widget_area_wg_custom_style') == 'yes') ? ((sdfGo()->sdf_get_option('typography', 'right_widget_area_custom_bg') == 'yes') ? '' : ' styled_widgets') : '';
	echo '<aside id="right-widget-area" class="default-widget-area '.sdf_class('right').$styled_widgets.'"'.$parallax.' itemtype="https://schema.org/WPSideBar" itemscope="itemscope" role="complementary">';
	sdf_before_sidebar_right();
	get_sidebar(); 	
	sdf_after_sidebar_right();
	echo '</aside>';

	
endif;
} 


	$sdf_sidebars = array();
	$global_params = array(
		'before_widget'  =>   '<div id="%1$s" class="%2$s widget-container"><div class="widget clearfix">',
		'after_widget'   =>   '</div></div>',
		'before_title'   =>   '<h3 class="title widgettitle">',
		'after_title'    =>   '</h3>'
	);
	
	foreach (sdfGo()->_wpuGlobal->_sdfSidebars as $id => $name) {
		if ($id != 'default-widget-area') {
		$params = array_merge(compact('id', 'name'), $global_params);
		$sdf_sidebars[$id] = register_sidebar($params);
		}
	}
	
function sdf_truncate($string, $length, $end = '...') {
	if (strlen($string) > $length) return substr_replace($string, $end, $length);
		else return $string;
}

function sdf_add_meta_box() {

	add_meta_box('sdf_custom_sidebar', 'Custom Sidebar',  array(&$this,'sdf_sidebar_meta_box'), 'page', 'side', 'high');
}

function sdf_sidebar_meta_box() {
	
}

add_action('in_widget_form','sdf_wg_custom_menu_orientation_form',10,3);
add_action('in_widget_form','sdf_wg_layout_form',10,3);

/* Widget Layout/Width options */
add_filter( 'dynamic_sidebar_params', 'sdf_wg_layout_style' );

function sdf_wg_layout_style( $params ) {
	global $wp_registered_widgets;

	$wg_id = $params[0]['widget_id'];
	$wg_object = $wp_registered_widgets[$wg_id];
	$wg_count = $wg_object['params'][0]['number'];
	$wg_option = get_option($wg_object['callback'][0]->option_name);
	
	$sdf_wg_classes = array(
        'col-md-12' => 1,
        'col-md-6' => 1/2,
        'col-md-4' => 1/3,
        'col-md-3' => 1/4,
        'col-md-2' => 1/6,
        'col-md-8' => 2/3,
        'col-md-9' => 3/4,
        'col-md-10' => 5/6
    );
	
		if (!isset($wg_option[$wg_count]['sdf_wg_layout_class'])) $wg_option[$wg_count]['sdf_wg_layout_class'] = 'col-md-12';
	
		$wg_option[$wg_count]['sdf_wg_layout_class'] = (array_key_exists($wg_option[$wg_count]['sdf_wg_layout_class'], $sdf_wg_classes)) ? $wg_option[$wg_count]['sdf_wg_layout_class'] : 'col-md-12';
		
		$current_wg_class = ($wg_option[$wg_count]['sdf_wg_layout_class'] == '') ? 'col-md-12' : $wg_option[$wg_count]['sdf_wg_layout_class'];
		$current_wg_area = $params[0]['id'];
		
		if ( !isset($GLOBALS['sdf_wg_row_counter']) ) { // first time at all
			$GLOBALS['sdf_wg_row_counter'][$current_wg_area] = array();
			$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['rows'] = 1;
			$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'] = $sdf_wg_classes[$current_wg_class];
			$current_wg_class .= ' first';
		} 
		else {
			if (isset ($GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'])) { // not the first time in area
				$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'] += $sdf_wg_classes[$current_wg_class];
				if ($GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'] >= 1.05) { // a new row
					$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'] = $sdf_wg_classes[$current_wg_class];
					$current_wg_class .= ' first';
					$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['rows']++;
				}
			} 
			else { // first time in area
				$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['rows'] = 1;
				$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['row_width'] = $sdf_wg_classes[$current_wg_class];
				$current_wg_class .= ' first';
			}
		}
		
		//$current_wg_class .= ' area_row_'.$GLOBALS['sdf_wg_row_counter'][$current_wg_area]['rows'];

		preg_match( '/(\<[a-zA-Z]+)(.*?)(\>)/',  $params[0]['before_widget'],$mat );
		
		// insert custom menu orientation class
		if ( isset($wg_option[$wg_count]['sdf_wg_orient'])) {
			$sdf_wg_orient_class = ($wg_option[$wg_count]['sdf_wg_orient'] == 1) ? ' block_menu' : ' inline_menu';
			$current_wg_class .= $sdf_wg_orient_class;
		}
		
		if ( !in_array( $current_wg_area, array('slider-widget-area' ,'wp_inactive_widgets') ) ) {
			$id_var = str_replace("-", "_", urlencode(strtolower($current_wg_area)));
			
			if((sdfGo()->sdf_get_option('typography', $id_var.'_wg_align') != '') && (sdfGo()->sdf_get_option('typography', $id_var.'_wg_align') != null)) {
				$align_widgets = ' col-xs-12 col-sm-12 pull-'.sdfGo()->sdf_get_option('typography', $id_var.'_wg_align');
			}
			else {
				$align_widgets = ' col-xs-12 col-sm-12 pull-left';
			}
			$current_wg_class .= $align_widgets;
		}
		
		if(preg_match( '/class="/', $params[0]['before_widget'])){
			$params[0]['before_widget'] =  preg_replace( '/class="/', "class=\"{$current_wg_class} ", $params[0]['before_widget'], 1 );
		}else{
			$params[0]['before_widget'] = preg_replace( '/(\<[a-zA-Z]+)(.*?)(\>)/', "$1 $2  class=\"{$current_wg_class}\" $3", $params[0]['before_widget'], 1 );
		}

	return $params;
}

function sdf_wg_layout_form( $widget, $return, $instance ){
	
	if(!isset($instance['sdf_wg_layout_class'])){
		$instance['sdf_wg_layout_class'] = 'col-md-12';
	}
	$sdf_wg_layouts = array(
	
        '1_1' => array ('class' => 'col-md-12', 'desc' => __('1/1', 'display-widgets')),
        '1_2' => array ('class' => 'col-md-6', 'desc' => __('1/2', 'display-widgets')),
        '1_3' => array ('class' => 'col-md-4', 'desc' => __('1/3', 'display-widgets')),
        '1_4' => array ('class' => 'col-md-3', 'desc' => __('1/4', 'display-widgets')),
        '1_6' => array ('class' => 'col-md-2', 'desc' => __('1/6', 'display-widgets')),
        '2_3' => array ('class' => 'col-md-8', 'desc' => __('2/3', 'display-widgets')),
        '3_4' => array ('class' => 'col-md-9', 'desc' => __('3/4', 'display-widgets')),
        '5_6' => array ('class' => 'col-md-10', 'desc' => __('5/6', 'display-widgets'))
    );
	
	$select_opts = '<p><label for="'.$widget->get_field_id('sdf_wg_layout_class').'">'. _e('Choose Widget Layout', 'display-widgets').'</label>'; 
	
	$select_opts .= '<div class="wpu-widget-layouts" id="'.$widget->get_field_id('sdf_wg_layout_class').'">                      
						<ul>';
	if(isset($sdf_wg_layouts)){
		foreach($sdf_wg_layouts as $l_key => $l_val){
			$select_opts .= '<li><input type="radio" class="sdf_widget_layout" name="'.$widget->get_field_name('sdf_wg_layout_class').'" id="'.$widget->get_field_id('sdf_wg_layout_class').$l_key.'" value="'.$l_val["class"].'"'.(( $l_val['class'] == $instance['sdf_wg_layout_class'] ) ? ' checked="checked"' : '') . ' />
		<label for="'.$widget->get_field_id('sdf_wg_layout_class').$l_key.'"><span class="layout_'.$l_key.'"></span>' . (empty($l_val["desc"]) ? $l_val["class"] : $l_val["desc"]) . '</label>
	</li>';
			
		}
	}
	$select_opts .='</ul></div></p>';
	echo $select_opts;	
	
}
add_filter( 'widget_update_callback', 'sdf_wg_layout_update_callback', 10, 2 );

function sdf_wg_layout_update_callback( $instance, $new_instance ) {
	$instance['sdf_wg_layout_class'] = $new_instance['sdf_wg_layout_class'];

	return $instance;
}
function sdf_wg_custom_menu_orientation_form( $widget, $return, $instance ){

	if($widget->id_base == 'nav_menu'){
		
		$instance['sdf_wg_orient'] = (!isset($instance['sdf_wg_orient'])) ? 0 : $instance['sdf_wg_orient'];
	
	?>   
    <p>
    	<label for="<?php echo $widget->get_field_id('sdf_wg_orient'); ?>"><?php _e('Set Menu Orientation', 'display-widgets') ?></label>
    	<select name="<?php echo $widget->get_field_name('sdf_wg_orient'); ?>" id="<?php echo $widget->get_field_id('sdf_wg_orient'); ?>" class="widefat">
            <option value="0"><?php _e('Inline', 'display-widgets') ?></option> 
            <option value="1" <?php echo selected( $instance['sdf_wg_orient'], 1 ) ?>><?php _e('Stacked', 'display-widgets') ?></option>
        </select>
    </p>    
	<?php 
	}
}

add_filter( 'widget_update_callback', 'sdf_wg_custom_menu_orientation_update_callback', 10, 2 );

function sdf_wg_custom_menu_orientation_update_callback( $instance, $new_instance ) {
	$instance['sdf_wg_orient'] = $new_instance['sdf_wg_orient'];

	return $instance;
}