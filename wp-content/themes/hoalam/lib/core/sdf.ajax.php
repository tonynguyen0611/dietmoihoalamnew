<?php 
/*
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * DESCRIPTIONS: Framework Ajax Functions
 *
 * AUTHOR:  SEO Design Framework
 */ 
add_action('template_redirect', 'sdf_ajax_localize');
function sdf_ajax_localize() {
	wp_enqueue_script( 'function', get_template_directory_uri().'/js/sdf.ajax.js', '1.0');
	wp_localize_script( 'function', 'sdf_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}

/**
 * is_ajax - Check if page is loaded via ajax
 */
	function is_ajax() 
	{
		if ( defined( 'DOING_AJAX' ) )
			return true;

		return ( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' ) ? true : false;
	}