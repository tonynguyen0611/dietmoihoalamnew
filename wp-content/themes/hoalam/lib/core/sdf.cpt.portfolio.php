<?php
/*
 *
 * Portfolio functions
 * Firm Themes
 * www.firmthemes.com
 * 
 */
 
add_action('init', 'portfolio_register');
function portfolio_register() {
    
	// Create The Labels (Output) For The Post Type
	$labels = 
	array(
		// The plural form of the name of your post type.
		'name' => __( 'Portfolio', SDF_TXT_DOMAIN), 
		
		// The singular form of the name of your post type.
		'singular_name' => __('Portfolio', SDF_TXT_DOMAIN),
			
		// The menu item for adding a new post.
		'add_new' => _x( 'Add New', 'portfolio', SDF_TXT_DOMAIN),
		'add_new_item' => __( 'Add New portfolio ', SDF_TXT_DOMAIN),
		
		// The header shown when editing a post.
		'edit_item' => __('Edit Portfolio Item', SDF_TXT_DOMAIN),
		
		// Shown in the favourites menu in the admin header.
		'new_item' => __('New Portfolio Item', SDF_TXT_DOMAIN), 
		
		// Shown alongside the permalink on the edit post screen.
		'view_item' => __('View Portfolio', SDF_TXT_DOMAIN),
		
		// Button text for the search box on the edit posts screen.
		'search_items' => __('Search Portfolio', SDF_TXT_DOMAIN), 
		
		// Text to display when no posts are found through search in the admin.
		'not_found' =>  __('No Portfolio Items Found', SDF_TXT_DOMAIN),
		
		// Text to display when no posts are in the trash.
		'not_found_in_trash' => __('No Portfolio Items Found In Trash', SDF_TXT_DOMAIN),
		 
		// Used as a label for a parent post on the edit posts screen. Only useful for hierarchical post types.
		'parent_item_colon' => '' 
	);
	
	$portfolioSlug = (sdfGo()->sdf_get_option('portfolio', 'slug')) ? sdfGo()->sdf_get_option('portfolio', 'slug') : 'portfolio';
	$supports = array( 'title', 'thumbnail', 'excerpt', 'editor', 'comments' );
	$taxonomies = array( 'post_tag' );

	$post_type_args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_ui'           => true,
		'menu_icon'         => 'dashicons-portfolio',
		'capability_type'   => 'post',
		'hierarchical'      => false,
		'rewrite'           => array('slug' => $portfolioSlug, 'with_front' => true ),
		'query_var'         => true,
		'publicly_queryable' => true,
		'show_in_nav_menus'=> true,
		'supports'          => $supports,
		'menu_position'     => 5,
		'taxonomies'        => $taxonomies
	 );
    register_post_type( 'portfolio' , $post_type_args );
}

$args = array(
	"hierarchical" => true, 
	"label" => "Portfolio Categories",
	"singular_label" => "Portfolio Category",
	"rewrite" => true,
	"query_var" => true
);

register_taxonomy("portfolio_items", array("portfolio"), $args);

// add default term
//$parent_term = term_exists( 'portfolio_items', 'portfolio_items' );
//$parent_term_id = $parent_term['term_id'];
//wp_insert_term('Uncategorized', 'portfolio_items', array( 'slug' => 'uncategorized', 'parent'=> $parent_term_id ));

add_filter("manage_edit-portfolio_columns", "project_edit_columns");
function project_edit_columns($columns){
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Project",
            "description" => "Description",
            "type" => "Type of Project",
        );
        return $columns;
}
add_action("manage_portfolio_posts_custom_column",  "project_custom_columns");
function project_custom_columns($column){
        global $post, $wpdb, $wpalch_item_media;
        switch ($column)
        {
            case "description":
                the_excerpt();
                break;
            case "type":
                echo get_the_term_list($post->ID, 'portfolio_items', '', ', ','');
                break;
        }
}

if ( ! function_exists( 'total_pagination' ) ) {

	function total_pagination( $args = array(), $query = '' ) {
		global $wp_rewrite, $wp_query;

		do_action( 'total_pagination_start' );

		if ( $query ) {

			$wp_query = $query;

		} // End IF Statement

		if ( 1 >= $wp_query->max_num_pages )
			return;

		/* Current page. */
		$current = ( get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1 );

		/* Max number of pages. */
		$max_num_pages = intval( $wp_query->max_num_pages );

		/* Set up some default arguments for the paginate_links() function. */
		$defaults = array(
			'base' => '#%#%',
			'format' => '?page=%#%',
			'total' => $max_num_pages,
			'current' => $current,
			'prev_next' => true,
			'prev_text' => __( '&laquo; Previous', SDF_TXT_DOMAIN ), // Translate in .po
			'next_text' => __( 'Next &raquo;', SDF_TXT_DOMAIN ), // Translate in .po
			'show_all' => false,
			'end_size' => 1,
			'mid_size' => 1,
			'add_fragment' => '',
			'type' => 'list',
			'before' => '',
			'after' => '',
			'echo' => true, 
			'use_search_permastruct' => true
		);

		/* Merge the arguments input with the defaults. */
		$args = wp_parse_args( $args, $defaults );

		/* Don't allow the user to set this to an array. */
		if ( 'array' == $args['type'] )
			$args['type'] = 'plain';

		/* Make sure raw querystrings are displayed at the end of the URL, if using pretty permalinks. */
		$pattern = '/\?(.*?)\//i';

		preg_match( $pattern, $args['base'], $raw_querystring );

		if( $wp_rewrite->using_permalinks() && (!empty($raw_querystring)) ) {
			$raw_querystring[0] = str_replace( '', '', $raw_querystring[0] );
			@$args['base'] = str_replace( $raw_querystring[0], '', $args['base'] );
			@$args['base'] .= substr( $raw_querystring[0], 0, -1 );
			}
		
		/* Get the paginated links. */
		$page_links = paginate_links( $args );

		/* Remove 'page/1' from the entire output since it's not needed. */
		$page_links = str_replace( array( '&#038;page=1\'', '/page/1\'' ), '\'', $page_links );

		do_action( 'total_pagination_end' );

		/* Return the paginated links for use in themes. */
		if ( $args['echo'] )
			echo $page_links;
		else
			return $page_links;

	} // End total_pagination()

} // End IF Statement
if ( ! function_exists( 'get_the_portfolio_category_by_tax' ) ) {
	function get_the_portfolio_category_by_tax( $id = false, $tcat = 'portfolio_items' ) {
		$categories = get_the_terms( $id, $tcat );
		if ( ! $categories )
			$categories = array();

		$categories = array_values( $categories );

		foreach ( array_keys( $categories ) as $key ) {
			_make_cat_compat( $categories[$key] );
		}

		return apply_filters( 'get_the_categories', $categories );
	}
} // End IF Statement