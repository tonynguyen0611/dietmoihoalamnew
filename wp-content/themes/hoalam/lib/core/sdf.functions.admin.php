<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

/**
* Fully Disable Autosave
*/
if (!sdfGo()->sdf_get_global('misc', 'toggle_autosave')) {
	add_action( 'admin_init', 'sdf_disableAutosave');
	/**
	*
	* This function fully disables autosave
	*
	* @since 1.0
	*/
	function sdf_disableAutosave(){
		wp_deregister_script('autosave');
	}	
}

/**
* Check to see if Toggle Buttons are Global (when available) or when to set on/off.
*/
function checkButton($input,$check,$att=''){

	// Check if the button is the basic on/off set as 0/1.
	if(is_numeric($check)){
		if((string)$input == (string)$check){
			
			return ' '.$att.' active';
		} 
		else {
			
			return ' btn-default';
		}
	} 
	elseif((string)$input== (string)$check){

	 // Checks for all non On/Off Buttons.
		return ' '.$att.' active';
	} 
	else {
		
		return ' btn-default';
	}
}

