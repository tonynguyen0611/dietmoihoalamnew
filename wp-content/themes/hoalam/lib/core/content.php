<?php
function sdf_do_before_content(){

	if((function_exists('dynamic_sidebar') && is_active_sidebar('above-content-area')) || sdfGo()->sdf_get_global('misc','toggle_placeholders')) {

		$widget_sidebars_content = array();
		ob_start();
		dynamic_sidebar( 'above-content-area' );
		array_push($widget_sidebars_content, ob_get_clean());

		if((strlen($widget_sidebars_content[0]) > 0) || sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
		
			//parallax
			$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
			$parallax_above = sdfGo()->sdf_get_option('typography', 'above_content_area_par_bg_ratio');
			$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_above != '') ? ' data-stellar-background-ratio="'.$parallax_above.'"' : '';
			$parallax = (sdfGo()->sdf_get_option('typography', 'above_content_area_custom_bg') == 'parallax') ? $parallax : '';
			
			if(SDF_PAGE_WIDTH == "wide") {
				echo '<div id="above-content-area" class="default-widget-area container-wide"'.$parallax.'><div class="container">';
			} else {
				echo '<div id="above-content-area" class="default-widget-area container-fluid"'.$parallax.'>';
			}
			
			$styled_widgets = (sdfGo()->sdf_get_option('typography', 'above_content_area_wg_custom_style') == 'yes') ? ' styled_widgets' : '';
			
			echo '<div class="row"><div class="col-md-12'.$styled_widgets.'"><section class="above-content-area widget_area clearfix">';
			
			echo $widget_sidebars_content[0];
			if(sdfGo()->sdf_get_global('misc','toggle_placeholders') && (strlen($widget_sidebars_content[0]) == 0)) {
			echo '<div class="col-md-12">';
			sdf_placeholder('above-content-area');
			echo '</div>';
			}
			
			echo '</section></div></div>';
			
			if(SDF_PAGE_WIDTH == "wide") {
				echo '</div></div>';
			} else {
				echo '</div>';
			}
		}

	}
	else {
		if(sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
			echo '<div class="row"><div class="col-md-12"><section class="above-content-area widget_area clearfix">';
			echo '<div class="col-md-12">';
			sdf_placeholder('above-content-area');
			echo '</div>';
			echo '</section></div></div>';
		}
	}
	
}
	
function sdf_do_the_content(){
	
	$post_type = get_post_type();
	echo '<div id="sdf-content" class="'.sdf_class('main').'" role="main" itemscope="itemscope" itemtype="https://schema.org/Blog">';
	if ((is_page()) && (is_page_template ( 'tpl/template-something.php' ))) {
		get_template_part( 'tpl/template', 'something' );
	} 
	elseif (is_404()) {
		get_template_part( 'tpl/loop', '404' );
	}
	elseif (is_search()) {
		get_template_part( 'tpl/loop', 'search' );
	}
	elseif (is_attachment()) {
		get_template_part( 'tpl/loop', 'attachment' );
	}
	elseif ($post_type == 'post') {
		get_template_part( 'tpl/loop', 'post' );
	}
	elseif ($post_type == 'portfolio') {
		get_template_part( 'tpl/loop', 'portfolio' );
	}
	elseif ($post_type == 'testimonials') {
		get_template_part( 'tpl/loop', 'testimonials' );
	}
	elseif ($post_type == 'product') {
		get_template_part( 'tpl/loop', 'woocommerce' );
	}
	else {
		get_template_part( 'tpl/loop', 'page' );
	}
	echo '</div>';
}
function sdf_do_after_content(){

	if((function_exists('dynamic_sidebar') && is_active_sidebar('below-content-area')) || sdfGo()->sdf_get_global('misc','toggle_placeholders')){
	
		$widget_sidebars_content = array();
		ob_start();
		dynamic_sidebar( 'below-content-area' );
		array_push($widget_sidebars_content, ob_get_clean());

		if((strlen($widget_sidebars_content[0]) > 0) || sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
			
			//parallax
			$parallax_default = sdfGo()->sdf_get_option('typography', 'default_widget_area_par_bg_ratio');
			$parallax_below = sdfGo()->sdf_get_option('typography', 'below_content_area_par_bg_ratio');
			$parallax = ($parallax_default != '') ? ' data-stellar-background-ratio="'.$parallax_default.'"' : ($parallax_below != '') ? ' data-stellar-background-ratio="'.$parallax_below.'"' : '';
			$parallax = (sdfGo()->sdf_get_option('typography', 'below_content_area_custom_bg') == 'parallax') ? $parallax : '';
			
			if(SDF_PAGE_WIDTH == "wide") {
				echo '<div id="below-content-area" class="default-widget-area container-wide"'.$parallax.'><div class="container">';
			} else {
				echo '<div id="below-content-area" class="default-widget-area container-fluid"'.$parallax.'>';
			}
			
			$styled_widgets = (sdfGo()->sdf_get_option('typography', 'below_content_area_wg_custom_style') == 'yes') ? ' styled_widgets' : '';
			
			echo '<div class="row"><div class="col-md-12'.$styled_widgets.'"><section class="below-content-area widget_area clearfix">';
			
			echo $widget_sidebars_content[0];
			if(sdfGo()->sdf_get_global('misc','toggle_placeholders') && (strlen($widget_sidebars_content[0]) == 0)) {
			echo '<div class="col-md-12">';
			sdf_placeholder('below-content-area');
			echo '</div>';
			}
			
			echo '</section></div></div>';
			
			if(SDF_PAGE_WIDTH == "wide") {
				echo '</div></div>';
			} 
			else {
				echo '</div>';
			}
		}

	}
	else {
		if(sdfGo()->sdf_get_global('misc','toggle_placeholders')) {
			echo '<div class="row"><div class="col-md-12"><section class="below-content-area widget_area clearfix">';
			echo '<div class="col-md-12">';
			sdf_placeholder('below-content-area');
			echo '</div>';
			echo '</section></div></div>';
		}
	}
	
}

function sdf_page_title($page_ID){

	$toggle_page_title = sdfGo()->sdf_is_permissible('title', 'toggle_page_title');
	//$toggle_page_title = ($toggle_page_title == "1") ? true : false;
	$toggle_breadcrumbs = sdfGo()->sdf_is_permissible('breadcrumbs', 'toggle_breadcrumbs');
	//$toggle_breadcrumbs = ($toggle_breadcrumbs == "1") ? true : false;

	//parallax
	$parallax_page_header = sdfGo()->sdf_get_option('typography', 'page_header_par_bg_ratio');
	$parallax_page_header = ($parallax_page_header != '') ? ' data-stellar-background-ratio="'.$parallax_page_header.'"' : '';


	if( (($toggle_page_title || $toggle_breadcrumbs)) || is_archive() || is_search() || is_404() || is_attachment() || (is_woocommerce_active() && is_woocommerce()) ) {
		if (SDF_PAGE_WIDTH == 'wide') {
			echo '<header id="page-header" class="container-wide"'.$parallax_page_header.'><div class="container">';
		} 
		else {
			echo '<header id="page-header"><div class="container-fluid">';
		}
	}
	
	$page_title_header = sdfGo()->sdf_get_option('title', 'title_header');
	$page_subtitle_header = sdfGo()->sdf_get_option('title', 'subtitle_header');
	$page_subtitle_text = sdfGo()->sdf_get_option('title', 'subtitle_text');
	
	if ( is_archive() ) {
		echo '<h2 class="entry-title" itemprop="headline"><a href="'.get_permalink($page_ID).'" rel="bookmark" title="'.sdf_page_holder_title().'">'.sdf_page_holder_title().'</a></h2>';
	}
	elseif(is_search()) {
		echo '<h2 class="entry-title" itemprop="headline">'.sdf_page_holder_title().'</h2>';
	}
	elseif(is_404()) {
		echo '<h2 class="entry-title" itemprop="headline">'.__("Error 404 - page not found", SDF_TXT_DOMAIN).'</h2>';
	}
	elseif(is_attachment()) {
	?>
		<h2 class="entry-title" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></h2>
	<?php
	}
	elseif ( is_woocommerce_active() && is_woocommerce() ) {
		ob_start();
		woocommerce_page_title();
		$title    = ob_get_clean();
	?>	
	
		<<?php echo $page_title_header; ?> class="entry-title" itemprop="headline"><?php echo $title; ?></<?php echo $page_title_header; ?>>
		
	<?php
	}
	else {
		if($toggle_page_title){
			switch($page_title_header) {
				case "h1": ?>
				<h1 class="entry-title" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></h1>          
				<?php 
				break;
				case "h2": ?>
				<h2 class="entry-title" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></h2>            
				<?php 
				break;
				case "disable": ?>
				<div class="entry-title" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></div>
				<?php 
				break;
				default: ?>
				<h1 class="entry-title" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></h1>      
				<?php break;	
			}
			
			if(trim($page_subtitle_text)){
				switch($page_subtitle_header) {
					case "h2": ?>
					<h2 class="entry-subtitle"><?php echo $page_subtitle_text; ?></h2>          
					<?php 
					break;
					case "h3": ?>
					<h3 class="entry-subtitle"><?php echo $page_subtitle_text; ?></h3>            
					<?php 
					break;
					case "h4": ?>
					<h4 class="entry-subtitle"><?php echo $page_subtitle_text; ?></h4>            
					<?php 
					break;
					case "disable": ?>
					<div class='entry-subtitle'><?php echo $page_subtitle_text; ?></div>
					<?php 
					break;
					default: ?>
					<h2 class="entry-subtitle"><?php echo $page_subtitle_text; ?></h2>      
					<?php break;	
				}
			}
		}
		elseif(!$toggle_page_title){
			?>
				<p class="entry-title hidden" itemprop="headline"><?php sdf_on_page_anchor($page_ID); ?></p>          
			<?php 
		}
	}
		
	// breadcrumbs
	if( ($toggle_breadcrumbs && !is_front_page()) || is_archive() || is_search() ) {
		sdf_get_breadcrumbs();
	}
	
	if( (($toggle_page_title || $toggle_breadcrumbs)) || is_archive() || is_search() || is_404() || is_attachment() || (is_woocommerce_active() && is_woocommerce()) ) {
		if (SDF_PAGE_WIDTH == 'wide') {
			echo '</div></header>';
		} 
		else {
			echo '</div></header>';
		}
	}
	
}
function sdf_on_page_anchor($page_ID){
	$page_toggle_link = sdfGo()->sdf_is_permissible('title', 'toggle_custom_title');
	$page_custom_link = sdfGo()->sdf_get_option('title', 'title_custom_link');
	$page_custom_text = sdfGo()->sdf_get_option('title', 'title_custom_text');
	$page_title_header = sdfGo()->sdf_get_option('title', 'title_header');

		if($page_toggle_link){
		$anchor = "<a href=\"";
		if(trim($page_custom_link)){
			$anchor .= $page_custom_link;
		}
		else{
			$anchor .= get_permalink($page_ID);
		}
		if(trim($page_custom_text)){
			$text_title = $page_custom_text;
		}
		else{
			$text_title = get_the_title($page_ID);
		}
		$anchor .= "\" title=\"". sprintf( esc_attr__( 'Permalink to %s', SDF_TXT_DOMAIN ), the_title_attribute( 'echo=0' ) )."\" rel=\"bookmark\">".$text_title."</a>";
	}
	else{
		if(trim($page_custom_text)){
			$text_title = $page_custom_text;
		}
		else{
			$text_title = get_the_title($page_ID);
		}
		
		$anchor = $text_title;
			
	}
	echo $anchor;
}