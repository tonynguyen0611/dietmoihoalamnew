<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

if ( ! isset( $content_width ) ) $content_width = 1110;

// Accept gzip Encoding Suffix used for cloud hosted scripts
//$sdf_accept_enc = ( substr_count( $_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip' )) ? '.gz': '';

/*	
* Getting script tags
*/

//add_action( 'wp_print_scripts', 'sdf_detect_enqueued_scripts' );
//function sdf_detect_enqueued_scripts() {
//	global $wp_scripts;
//	foreach( $wp_scripts->queue as $handle ) :
//		echo $handle . ' | ';
//	endforeach;
//}


/**
 * Register menu location and activate native wordpress navigation
 */
if(!function_exists('sdf_register_main_menus'))
{
	function sdf_register_main_menus(){
		add_theme_support('nav_menus');
		register_nav_menus( array(
			'primary' => __( 'Primary Navigation',  SDF_TXT_DOMAIN),
			'secondary' => __( 'Secondary Navigation',  SDF_TXT_DOMAIN)
		) );
	}
	sdf_register_main_menus();
}


/**
 * Check links for missing http(s) prefix or
 * replace http:// with https:// on ssl
 */
if(!function_exists('sdf_check_link_prefix'))
{
	function sdf_check_link_prefix($link){
		if(in_array($link, array('/', '#'))) {
			return $link;
		}
		elseif ( ! preg_match( '/^(https?\:\/\/|\/\/)/', $link ) ) {
			$link = SDF_PROTOCOL_PREF . $link;	
		}
		else {
			if(SDF_SSL) $link = preg_replace( "/^http:/i", "https:", $link );			
		}
			return (filter_var($link, FILTER_VALIDATE_URL) !== false) ? $link : '';
	}
}

/**
 * Toggle Admin Bar
 */
function sdf_toggle_admin_bar(){ 
  if(sdfGo()->sdf_get_option('misc','toggle_admin_bar') == 0) {
		return false; 
	}
	else{
		return is_user_logged_in();
	}
}
add_filter( 'show_admin_bar' , 'sdf_toggle_admin_bar');

/**
 * Custom Excerpt More
 */
add_filter('excerpt_more', 'sdf_excerpt_more');
function sdf_excerpt_more($more) {
	global $post;
	$read_more_text = (sdfGo()->sdf_get_option('blog','read_more_text')) ? sdfGo()->sdf_get_option('blog','read_more_text'): __( 'Read More',  SDF_TXT_DOMAIN);
	return ' <a href="'. get_permalink($post->ID) . '" class="read-more-excerpt">'.$read_more_text.'</a>';
}

/**
* Custom Excerpt Length
*/
add_filter('excerpt_length', 'sdf_excerpt_length');
function sdf_excerpt_length() {
	return sdfGo()->sdf_get_global('blog','excerpt_length');
}
/**
* Word Count
*/
function words_count($content, $word_count) {
	$words = explode(' ', $content);
	return implode(' ', array_slice($words, 0, $word_count));
}

/**
 * @param string $code name of the shortcode
 * @param string $content
 * @return string content with shortcode striped
 */
function strip_shortcode($code, $content)
{
    global $shortcode_tags;

    $stack = $shortcode_tags;
    $shortcode_tags = array($code => 1);

    $content = strip_shortcodes($content);

    $shortcode_tags = $stack;
    return $content;
}

/**
* Trim Get Excerpts
*/
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wp_trim_all_excerpt');
function wp_trim_all_excerpt($raw_text, $excerpt_length = '') {
	// Creates an excerpt if needed; and shortens the manual excerpt as well
	if ( $raw_text == '' ) {
		$raw_text = get_the_content('');
	}
	$raw_excerpt = $raw_text;
	
	$raw_text = strip_shortcode('gallery', $raw_text);
	$raw_text = strip_shortcode('sdf_latest_posts', $raw_text);
	$raw_text = strip_shortcode('sdf_portfolio', $raw_text);
		
	$raw_text = apply_filters('the_content', $raw_text);
	$raw_text = str_replace(']]>', ']]&gt;', $raw_text);
	$raw_text = strip_tags($raw_text);
	if ($excerpt_length == ''){
		$excerpt_length = apply_filters('excerpt_length', sdfGo()->sdf_get_global('blog','excerpt_length'));
	}
	if ($excerpt_length){
		$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
		$raw_text = wp_trim_words( $raw_text, $excerpt_length, $excerpt_more );
	}
	return apply_filters('wp_trim_excerpt', $raw_text, $raw_excerpt); //since wp3.3
}

function get_excerpt_by_id($post_id, $length){

	$the_post_by_id = get_post($post_id);
	$raw_text = $the_post_by_id->post_content;
	$post_excerpt = trim($the_post_by_id->post_excerpt);
	
	$raw_text = ($post_excerpt != "") ? $post_excerpt : strip_tags($raw_text);
		if ( '' == $raw_text ) {
			return false;
		}
	
	$raw_text = strip_shortcode('gallery', $raw_text);
	$raw_text = strip_shortcode('sdf_latest_posts', $raw_text);
	$raw_text = strip_shortcode('sdf_portfolio', $raw_text);
	
	$sdfShortcodes = new SDFShortcodes();
	$raw_text = $sdfShortcodes->sdf_run_shortcode($raw_text); 
	$excerpt_length = apply_filters('excerpt_length', $length);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	$raw_text = wp_trim_words( $raw_text, $length, $excerpt_more ); //since wp3.3
	
	return apply_filters('wp_trim_excerpt', $raw_text); //since wp3.3
	
}
		
if ( function_exists( 'add_theme_support' ) ) {

/* Do theme setup on the 'after_setup_theme' hook. */
add_action( 'after_setup_theme', 'sdf_theme_setup' );

/**
 * Theme setup function.
 */
function sdf_theme_setup(){

	/* updater args */
	$updater_args = array(
			'repo_uri'  => 'http://www.seodesignframework.com/',
			'repo_slug' => 'sdf-core',
			'key'       => sdfGo()->sdf_get_option('misc','auto_update_email'),
			'username'  => sdfGo()->sdf_get_option('misc','auto_update_username'),
			'dashboard' => false,
	);

	/* add support for updater */
	add_theme_support( 'auto-hosted-theme-updater', $updater_args );
}
/* Load Theme Updater */
new SDF_Theme_Updater;

/**
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

/**
* Post Formats
*/
$sdf_post_formats = sdfGo()->_wpuGlobal->_sdfPostFormats;
unset($sdf_post_formats[0]);
add_theme_support( 'post-formats', $sdf_post_formats );
// add post-formats to post_type 'portfolio'
add_action( 'init', 'portfolio_taxonomy_for_object_type', 11 );
function portfolio_taxonomy_for_object_type() {
	add_post_type_support( 'portfolio', 'post-formats' );
	register_taxonomy_for_object_type( 'post_format', 'portfolio' );
};
/**
* Add RSS Links to head
*/
add_theme_support( 'automatic-feed-links' );

/* Comments */	
add_filter( 'comment_form_default_fields', 'tbs_comment_form_fields' );
function tbs_comment_form_fields( $fields ) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );
	$html5 = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
	$fields = array(
	'author' => '<div class="form-group comment-form-author">' . '<label for="author">' . __( 'Name', SDF_TXT_DOMAIN ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
	'<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
	'email' => '<div class="form-group comment-form-email"><label for="email">' . __( 'Email', SDF_TXT_DOMAIN ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
	'<input class="form-control" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
	'url' => '<div class="form-group comment-form-url"><label for="url">' . __( 'Website', SDF_TXT_DOMAIN ) . '</label> ' .
	'<input class="form-control" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>',
	);
	return $fields;
}

add_filter( 'comment_form_defaults', 'tbs_comment_form' );
function tbs_comment_form( $args ) {
	$args['comment_field'] = '<div class="form-group comment-form-comment">
	<label for="comment">' . _x( 'Comment', 'noun' ) . '</label>
	<textarea class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
	</div>';
	$args['title_reply'] = __( 'Leave a Reply', SDF_TXT_DOMAIN );
	$args['title_reply_to'] = __( 'Leave a Reply to %s', SDF_TXT_DOMAIN );
	$args['cancel_reply_link'] = __( 'Cancel reply', SDF_TXT_DOMAIN );
	return $args;
}

add_action('comment_form', 'tbs_comment_button' );
function tbs_comment_button() {
	echo '<button class="btn btn-primary" type="submit">' . __( 'Post Comment', SDF_TXT_DOMAIN ) . '</button>';
}
	
add_action('after_setup_theme', 'custom_image_sizes');
function custom_image_sizes() {	
	/**
	* Add featured image
	*/	
	add_theme_support( 'post-thumbnails' );
	/**
	* Add image sizes
	*/
	add_image_size( 'sdf-image-md-12', sdfGo()->sdf_get_option('misc','image_size_12_w'), sdfGo()->sdf_get_option('misc','image_size_12_h'), true);
	add_image_size( 'sdf-image-md-12-sq', sdfGo()->sdf_get_option('misc','image_size_12_w'), sdfGo()->sdf_get_option('misc','image_size_12_w'), true);
	add_image_size( 'sdf-image-md-6', sdfGo()->sdf_get_option('misc','image_size_6_w'), sdfGo()->sdf_get_option('misc','image_size_6_h'), true);
	add_image_size( 'sdf-image-md-6-sq', sdfGo()->sdf_get_option('misc','image_size_6_w'), sdfGo()->sdf_get_option('misc','image_size_6_w'), true);
	add_image_size( 'sdf-image-md-4', sdfGo()->sdf_get_option('misc','image_size_4_w'), sdfGo()->sdf_get_option('misc','image_size_4_h'), true);
	add_image_size( 'sdf-image-md-4-sq', sdfGo()->sdf_get_option('misc','image_size_4_w'), sdfGo()->sdf_get_option('misc','image_size_4_w'), true);
	add_image_size( 'sdf-image-md-3', sdfGo()->sdf_get_option('misc','image_size_3_w'), sdfGo()->sdf_get_option('misc','image_size_3_h'), true);
	add_image_size( 'sdf-image-md-3-sq', sdfGo()->sdf_get_option('misc','image_size_3_w'), sdfGo()->sdf_get_option('misc','image_size_3_w'), true);
	add_image_size( 'sdf-image-md-2', sdfGo()->sdf_get_option('misc','image_size_2_w'), sdfGo()->sdf_get_option('misc','image_size_2_h'), true);
	add_image_size( 'sdf-image-md-2-sq', sdfGo()->sdf_get_option('misc','image_size_2_w'), sdfGo()->sdf_get_option('misc','image_size_2_w'), true);
}

}

if(!function_exists('load_template_part')) {
	function load_template_part($template_name, $part_name=null) {
		ob_start();
		get_template_part($template_name, $part_name);
		$var = ob_get_contents();
		ob_end_clean();
		return $var;
	}
}
/**
 * Adds classes to the array of body classes.
 * @uses body_class() filter
 */
add_filter('body_class', 'sdf_body_classes');

function sdf_body_classes($classes) {
        $classes[] = SDF_PAGE_WIDTH.'-layout';
        return $classes;
}
if(!function_exists('sdf_is')) {
	/**
	 * @param  var - testing variable
	 * @param  value
	 * @return boolean
	 */
	function sdf_is(&$var, $value = null){
	  if(!is_null($value)){ return isset($var) && $var == $value; }
	  return isset($var) && !empty($var);
	}
}
if(!function_exists('sdf_is_not')) {
	/**
	 * @param  var - testing variable
	 * @param  value
	 * @return boolean
	 */
	function sdf_is_not(&$var, $value = null){
	  if(!is_null($value)){ return isset($var) && $var != $value; }
	  return isset($var) && !empty($var);
	}
}	

/*
 * Register theme text domain
 */
if(!function_exists('sdf_text_domain_setup'))
{
	add_action('after_setup_theme', 'sdf_text_domain_setup');
	function sdf_text_domain_setup()
	{
		load_theme_textdomain(SDF_TXT_DOMAIN, SDF_LANG);
	}
}

/*
 * Register custom sidebars
 */
add_theme_support('sdf_sidebar');
if(get_theme_support( 'sdf_sidebar' )) {  
	new sdf_sidebar();
}

// Set default editing mode
if(is_admin()) {
	add_filter('wp_default_editor', create_function('', 'return "html";'));
}

if(!function_exists('sdf_hex2RGB'))
{
	/**
	* Convert a hexa decimal color code to its RGB equivalent
	*
	* @param string $hexStr (hexadecimal color value)
	* @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	* @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	* @return array or string (depending on second parameter. Returns False if invalid hex color value)
	*/  
	
	function sdf_hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}
		return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
	}
}

if ( !function_exists('pre_check_custom_css_writable') ) {

	function pre_check_custom_css_writable() {
	// custom css directory
	$the_dir = get_template_directory() . '/lib/css';

	if(! get_filesystem_method(array(), $the_dir ) == "direct")
	add_action('admin_notices', 'pre_notice_custom_css_permission');
	}

	add_action( 'admin_init', 'pre_check_custom_css_writable' );
}

if ( !function_exists('pre_notice_custom_css_permission') ) {
	// display notice if user need to set permission for directory
	function pre_notice_custom_css_permission(){
	echo 'This theme needs permission to write custom styles in css file. Please change directory permission at ( wp-content/themes/seodesign/lib/css) to 644 or higher';
	}
}

if (!function_exists('sdf_pagination')) {
function sdf_pagination($pages = '', $range = 4, $paged = 1){  
	global $wp_query;

	if ( get_query_var('paged') ) { 
		$paged = get_query_var('paged'); 
	}
	elseif ( get_query_var('page') ) { 
		$paged = get_query_var('page'); 
	}
	else { 
		$paged = 1; 
	}
	
	$showitems = $range+1;  

	if($pages == ''){
			$pages = $wp_query->max_num_pages;
			if(!$pages){
					$pages = 1;
			}
	}   

	// Don't print empty markup if there's only one page.
	if ( $pages < 2 ) {
		return;
	} 
        
	echo "<section id=\"blog-nav\" class=\"clearfix\"><ul class=\"nav nav-pills\">";
	
	if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li class='first'><a href='".get_pagenum_link(1)."'><i class='fa fa-angle-double-left'></i></a></li>";
	
	echo "<li class='prev";
	
	if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
		echo " prev_first";
	}
	
	echo "'><a href='".get_pagenum_link($paged - 1)."'><i class='fa fa-angle-left'></i></a></li>";

	for ($i=1; $i <= $pages; $i++){
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
					echo ($paged == $i)? "<li class='active'><a>".$i."</a></li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";
			}
	}
	
	echo "<li class='next";
	
	if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
		echo " next_last";
	}
	
	echo "'><a href=\"";
	
	if($pages > $paged){
		echo get_pagenum_link($paged + 1);
	} else {
		echo get_pagenum_link($paged);
	}
	
	echo "\"><i class='fa fa-angle-right'></i></a></li>";  
	 
	if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li class='last'><a href='".get_pagenum_link($pages)."'><i class='fa fa-angle-double-right'></i></a></li>";
	
	echo "</ul></section>";
}
}

if(!function_exists('sdf_rewrite_rules_on_theme_switch')) {
	/**
	 * Setting rewrite rules when SDF theme is activated
	 */
	function sdf_rewrite_rules_on_theme_switch() {
		flush_rewrite_rules();
	}

	add_action( 'after_switch_theme', 'sdf_rewrite_rules_on_theme_switch' );
}

if(!function_exists('sdf_maintenance_mode')) {
    /**
     * Function that redirects to maintenance page
     */
    function sdf_maintenance_mode() {
			
			$protocol = is_ssl() ? "https://" : "http://";
			if( sdfGo()->sdf_get_option('misc','toggle_maintenance_mode') == 1 && sdfGo()->sdf_get_option('misc','theme_maintenance_page') != ""
			&& !in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))
			&& !is_admin()
			&& !is_user_logged_in()
			&& $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] != get_permalink(sdfGo()->sdf_get_option('misc','theme_maintenance_page'))) {
				wp_redirect(get_permalink(sdfGo()->sdf_get_option('misc','theme_maintenance_page')));
				exit;
			}
    }

    if( sdfGo()->sdf_get_option('misc','toggle_maintenance_mode') == 1 ) {
			add_action('init', 'sdf_maintenance_mode', 1);
    }
}

add_theme_support( 'woocommerce' );
/**
 * Check if WooCommerce plugin is active
 * @return boolean
 */
if ( ! function_exists( 'is_woocommerce_active' ) ) {
	function is_woocommerce_active() {
		return class_exists( 'Woocommerce' );
	}
}
	
if(!function_exists('sdf_is_product_category')) {
	function sdf_is_product_category() {
		return function_exists('is_product_category') && is_product_category();
	}
}

/**
 * Adds an SDF admin bar menu
 */
function sdf_admin_bar_menu() {

	// If the current user can write posts
	if ( ! current_user_can( 'edit_posts' ) ) {
		return;
	}
	// If "Design" Menu is disabled in global settings
	if(sdfGo()->sdf_get_option('misc','toggle_design_toolbar_menu') == 0) {
		return; 
	}

	global $wp_admin_bar;

	$wp_admin_bar->add_menu( array(
			'id'    => 'sdf-menu',
			'title' => __( 'Design', SDF_TXT_DOMAIN ),
			'href'  => get_admin_url( null, 'admin.php?page=sdf' ),
		) );

	$admin_menu = false;
	if ( is_multisite() ) {
		$options = get_site_option( 'sdf_ms' );
		if ( $options['access'] === 'superadmin' && is_super_admin() ) {
			$admin_menu = true;
		}
		elseif ( current_user_can( 'manage_options' ) ) {
			$admin_menu = true;
		}
	}
	elseif ( current_user_can( 'manage_options' ) ) {
		$admin_menu = true;
	}

	// @todo: add links to bulk title and bulk description edit pages
	if ( $admin_menu ) {
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-settings',
				'title'  => __( 'Global Settings', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-settings' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-silo',
				'title'  => __( 'Silo', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-silo' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-layout',
				'title'  => __( 'Layout', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-layout' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-styles',
				'title'  => __( 'Styles', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-styles' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-header',
				'title'  => __( 'Header', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-header' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-footer',
				'title'  => __( 'Footer', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-footer' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-revslider',
				'title'  => __( 'Slider', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=revslider' ),
			) );
		$wp_admin_bar->add_menu( array(
				'parent' => 'sdf-menu',
				'id'     => 'sdf-shortcode',
				'title'  => __( 'Shortcodes', SDF_TXT_DOMAIN ),
				'href'   => admin_url( 'admin.php?page=sdf-shortcode' ),
			) );
	}
}

add_action( 'admin_bar_menu', 'sdf_admin_bar_menu', 96 );

/**
 * Generate html tag
 *
 * @param string $tag Tag name
 * @param array $attr Tag attributes
 * @param bool|string $end Append closing tag. Also accepts body content
 * @return string The tag's html
 */
function sdf_html_tag($tag, $attr = array(), $end = false) {
	$html = '<'. $tag .' '. sdf_attr_to_html($attr);

	if ($end === true) {
		# <script></script>
		$html .= '></'. $tag .'>';
	} else if ($end === false) {
		# <br/>
		$html .= '/>';
	} else {
		# <div>content</div>
		$html .= '>'. $end .'</'. $tag .'>';
	}

	return $html;
}

/**
 * Generate attributes string for html tag
 * @param array $attr_array array('href' => '/', 'title' => 'Test')
 * @return string 'href="/" title="Test"'
 */
function sdf_attr_to_html(array $attr_array) {
	$html_attr = '';

	foreach ($attr_array as $attr_name => $attr_val) {
		if ($attr_val === false) {
			continue;
		}

		$html_attr .= $attr_name .'="'. sdf_htmlspecialchars($attr_val) .'" ';
	}

	return $html_attr;
}

/**
 * Use this id do not want to enter every time same last two parameters
 * Info: Cannot use default parameters because in php 5.2 encoding is not UTF-8 by default
 *
 * @param string $string
 * @return string
 */
function sdf_htmlspecialchars($string) {
	return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}

// Omit closing PHP tag to avoid "Headers already sent" issues.
