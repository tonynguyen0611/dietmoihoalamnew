<?php
/**
 * Functions for the basic WooCommerce integration
 *
 * @package SEODesign
 */

if ( is_woocommerce_active() ) {

	// remove all the woocommerce CSS as we have LESS
	add_filter( 'woocommerce_enqueue_styles', '__return_false' );

	/**
	 * Theme compatibility
	 *
	 * @link http://docs.woothemes.com/document/third-party-custom-theme-compatibility/
	 */
	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	// Display custom number of products per page
	add_filter( 'loop_shop_per_page', 'sdf_number_of_products_per_page' );
	if (!function_exists('sdf_number_of_products_per_page')) {
		function sdf_number_of_products_per_page() {
			return (int)sdfGo()->sdf_get_option('misc', 'woo_products_per_page');
		}
	} 
	
	// Change number or products per row
	add_filter( 'loop_shop_columns', 'sdf_loop_shop_columns' );
	if (!function_exists('sdf_loop_shop_columns')) {
		function sdf_loop_shop_columns() {
			return (int)sdfGo()->sdf_get_option('misc', 'woo_columns');
		}
	} 

	// remove the title, because we show it elsewhere
	add_filter( 'woocommerce_show_page_title', '__return_false' );
	add_filter( 'woocommerce_get_sidebar', '__return_false' );

	// remove breadcrumbs, we have our own
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

	// remove rating from the single page
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 3 );

	/**
	 * Change number of related columns/products for single product page.
	 *
	 * @param  array $args
	 * @return array
	 */
	add_filter( 'woocommerce_output_related_products_args', 'sdf_output_related_products_args' );
	if (!function_exists('sdf_output_related_products_args')) {
		function sdf_output_related_products_args( $args ) {
			$args[ 'posts_per_page' ] = (int)sdfGo()->sdf_get_option('misc', 'woo_related_products_per_page');
			$args[ 'columns' ]        = (int)sdfGo()->sdf_get_option('misc', 'woo_related_columns');

			return $args;
		}
	}

}