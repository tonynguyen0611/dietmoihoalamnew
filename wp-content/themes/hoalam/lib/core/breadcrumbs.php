<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: class_breadcrumbs.php
 * DESCRIPTIONS: Theme Breadcrumbs Options.
 *
 * AUTHOR:  SEO Design Framework
 */ 
/*
 * Class name: ULTBreadcrumbs
 * Descripton: Implementation of Theme Breadcrumbs Options    
 */
 
function sdf_get_breadcrumbs(){

	global $post;
	
		 if ( !is_front_page() || is_paged() ) { 
			$toggle_home_breadcrumb = sdfGo()->sdf_get_global('breadcrumbs','toggle_bread_home'); 
			$breadcrumb_home = sdfGo()->sdf_get_global('breadcrumbs','bread_home_text');
			$breadcrumb_current = sdfGo()->sdf_get_page('breadcrumbs','bread_current_text');
			
			$active_li = ' class="active"';
			$before_bread = '<span class="current">';
			$after_bread = '</span>'; 
			
			
			$sdf_breadcrumbs = ' <nav id="page-breadcrumbs">';
			
			 $sdf_breadcrumbs .= '<ol class="breadcrumb">';
			 if($toggle_home_breadcrumb == '1'){
				$sdf_breadcrumbs .= '<li><a href="' . home_url(). '">' . $breadcrumb_home . '</a></li>';
			 }
			 
			  if( is_home() && get_option( 'page_for_posts' ) ) {
					$page_ID = sdf_page_holder_ID();
					$breadcrumb_option = ($breadcrumb_current != '') ? $breadcrumb_current : get_the_title($page_ID);
					$sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . $breadcrumb_option;
				} 
				elseif ( is_category() || sdf_is_product_category() ) {
				  global $wp_query;
				  $cat_obj = $wp_query->get_queried_object();
				  $thisCat = $cat_obj->term_id;
				  $thisCat = get_category($thisCat);
					if (isset($thisCat->parent) && $thisCat->parent != 0) {
						$parentCat = get_category($thisCat->parent);
						if ($thisCat->parent != 0) {
							$cats = '<li>'.get_category_parents($parentCat, TRUE, '</li><li>');
							$cats = substr($cats, 0, -4);
							$sdf_breadcrumbs .= $cats;
						}
					}
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . single_cat_title('', false) . $after_bread.'</li>';
			 
				} 
				elseif ( is_day() ) {
				  $sdf_breadcrumbs .= '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li>';
				  $sdf_breadcrumbs .= '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></li>';
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . get_the_time('d') . $after_bread.'</li>';
			 
				} 
				elseif ( is_month() ) {
				  $sdf_breadcrumbs .= '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></li>';
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . get_the_time('F') . $after_bread.'</li>';
			 
				} 
				elseif ( is_year() ) {
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . get_the_time('Y') . $after_bread.'</li>';
			 
				} 
				elseif ( is_single() && !is_attachment() ) {
				  if ( get_post_type() != 'post' ) {
					$post_type = get_post_type_object(get_post_type());
					$slug = $post_type->rewrite;
					$sdf_breadcrumbs .= '<li>' . $post_type->labels->singular_name . '</li>';
					$sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread;
					$breadcrumb_option = ($breadcrumb_current != '') ? $breadcrumb_current : get_the_title();
					$sdf_breadcrumbs .= $breadcrumb_option;
				  $sdf_breadcrumbs .=  $after_bread.'</li>';
				  } else {
					$category = get_the_category();
					$category = $category[0];
					$cats = '<li>' . get_category_parents($category, TRUE, '</li><li>');
					$cats = substr($cats, 0, -4);
					$sdf_breadcrumbs .= $cats;
					
					$sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread;
					$breadcrumb_option = ($breadcrumb_current != '') ? $breadcrumb_current : get_the_title();
					$sdf_breadcrumbs .= $breadcrumb_option;
				  $sdf_breadcrumbs .=  $after_bread.'</li>';
				  }
			 
				} elseif ( is_search() ) {
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . 'Search results for "' . get_search_query() . '"' . $after_bread.'</li>';
			 
				} elseif ( is_tag() ) {
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . 'Posts tagged "' . single_tag_title('', false) . '"' . $after_bread.'</li>';
			 
				} elseif ( is_author() ) {
				   global $author;
				  $userdata = get_userdata($author);
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . 'Articles posted by ' . $userdata->display_name . $after_bread.'</li>';
			 
				} elseif ( is_404() ) {
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . 'Error 404' . $after_bread.'</li>';
					
				} elseif (is_woocommerce_active() && is_shop()){
					global $woocommerce;
					$shop_id = get_option('woocommerce_shop_page_id');
					$shop = get_page($shop_id);
					$sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread;
					$sdf_breadcrumbs .= $shop->post_title;
					$sdf_breadcrumbs .=  $after_bread.'</li>';
					
				} elseif ( !is_single() && !is_page() && get_post_type() != 'post' ) {
				  $post_type = get_post_type_object(get_post_type());
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . $post_type->labels->singular_name . $after_bread.'</li>';
			 
				} elseif ( is_attachment() ) {
				  $parent = get_post($post->post_parent);
				  $category = get_the_category($parent->ID);
						foreach($category as $cat)
						{
							if(!empty($cat->parent))
							{
									$parents = get_category_parents($cat->cat_ID, TRUE, '$$$', FALSE );
									$parents = explode("$$$", $parents);
									foreach ($parents as $parent_item)
									{
											if($parent_item) $sdf_breadcrumbs .= '<li>'.$parent_item.'</li>';
									}
									break;
							}
						}
				  //$sdf_breadcrumbs .= '<li>'.get_category_parents($cat, TRUE, '</li>');
				 // $sdf_breadcrumbs .= '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></li>';
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread . get_the_title() . $after_bread.'</li>';
			 
				} 
				elseif ( is_page() && !$post->post_parent ) {
				   $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread;
					$breadcrumb_option = ($breadcrumb_current != '') ? $breadcrumb_current : get_the_title();
					$sdf_breadcrumbs .= $breadcrumb_option;
				  $sdf_breadcrumbs .=  $after_bread.'</li>';
			 
				} 
				elseif ( is_page() && $post->post_parent ) {
				  $parent_id  = $post->post_parent;
				  $breadcrumbs = array();
				  while ($parent_id) {
					$page = get_page($parent_id);
					$breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
					$parent_id  = $page->post_parent;
				  }
				  $breadcrumbs = array_reverse($breadcrumbs);
				  foreach ($breadcrumbs as $crumb) $sdf_breadcrumbs .= $crumb;
				  $sdf_breadcrumbs .= '<li'.$active_li.'>'.$before_bread;
					$breadcrumb_option = ($breadcrumb_current != '') ? $breadcrumb_current : get_the_title();
					$sdf_breadcrumbs .= $breadcrumb_option;
				  $sdf_breadcrumbs .=  $after_bread.'</li>';
			 
				}
				
				if ( get_query_var('paged') ) {
					$sdf_breadcrumbs .= '<li'.$active_li.'>';
					if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $sdf_breadcrumbs .= ' (';
						$sdf_breadcrumbs .=  __('Page') . ' ' . get_query_var('paged');
					if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $sdf_breadcrumbs .= ')';
					  $sdf_breadcrumbs .= '</li>';
				}
				 
			
			$sdf_breadcrumbs .=  '</ol></nav>';
			
			
			 echo $sdf_breadcrumbs;
		}
}