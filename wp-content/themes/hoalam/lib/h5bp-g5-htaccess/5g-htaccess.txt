# BEGIN 5G Blacklist

# 5G:[QUERY STRINGS]
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteBase /
	RewriteCond %{QUERY_STRING} (\"|%22).*(<|>|%3) [NC,OR]
	RewriteCond %{QUERY_STRING} (javascript:).*(\;) [NC,OR]
	RewriteCond %{QUERY_STRING} (<|%3C).*script.*(>|%3) [NC,OR]
	RewriteCond %{QUERY_STRING} (\\|\.\./|`|=\'$|=%27$) [NC,OR]
	RewriteCond %{QUERY_STRING} (\;|\'|\"|%22).*(union|select|insert|drop|update|md5|benchmark|or|and|if) [NC,OR]
	RewriteCond %{QUERY_STRING} (base64_encode|localhost|mosconfig) [NC,OR]
	RewriteCond %{QUERY_STRING} (boot\.ini|echo.*kae|etc/passwd) [NC,OR]
	RewriteCond %{QUERY_STRING} (GLOBALS|REQUEST)(=|\[|%) [NC]
	RewriteRule .* - [F]
</IfModule>

# 5G:[USER AGENTS]
<IfModule mod_setenvif.c>
	# SetEnvIfNoCase User-Agent ^$ keep_out
	SetEnvIfNoCase User-Agent (binlar|casper|cmsworldmap|comodo|diavol|dotbot|feedfinder|flicky|ia_archiver|jakarta|kmccrew|nutch|planetwork|purebot|pycurl|skygrid|sucker|turnit|vikspider|zmeu) keep_out
	<limit GET POST PUT>
		Order Allow,Deny
		Allow from all
		Deny from env=keep_out
	</limit>
</IfModule>

# 5G:[REQUEST STRINGS]
<IfModule mod_alias.c>
	RedirectMatch 403 (https?|ftp|php)\://
	RedirectMatch 403 /(https?|ima|ucp)/
	RedirectMatch 403 /(Permanent|Better)$
	RedirectMatch 403 (\=\\\'|\=\\%27|/\\\'/?|\)\.css\()$
	RedirectMatch 403 (\,|\)\+|/\,/|\{0\}|\(/\(|\.\.\.|\+\+\+|\||\\\"\\\")
	RedirectMatch 403 \.(cgi|asp|aspx|cfg|dll|exe|jsp|mdb|sql|ini|rar)$
	RedirectMatch 403 /(contac|fpw|install|pingserver|register)\.php$
	RedirectMatch 403 (base64|crossdomain|localhost|wwwroot|e107\_)
	RedirectMatch 403 (eval\(|\_vti\_|\(null\)|echo.*kae|config\.xml)
	RedirectMatch 403 \.well\-known/host\-meta
	RedirectMatch 403 /function\.array\-rand
	RedirectMatch 403 \)\;\$\(this\)\.html\(
	RedirectMatch 403 proc/self/environ
	RedirectMatch 403 msnbot\.htm\)\.\_
	RedirectMatch 403 /ref\.outcontrol
	RedirectMatch 403 com\_cropimage
	RedirectMatch 403 indonesia\.htm
	RedirectMatch 403 \{\$itemURL\}
	RedirectMatch 403 function\(\)
	RedirectMatch 403 labels\.rdf
	RedirectMatch 403 /playing.php
	RedirectMatch 403 muieblackcat
</IfModule>

# 5G:[REQUEST METHOD]
<ifModule mod_rewrite.c>
	RewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)
	RewriteRule .* - [F]
</IfModule>

# 5G:[WordPress]
<ifModule mod_rewrite.c>
 RedirectMatch 403 /\$\&
 RedirectMatch 403 (?i)/\&(t|title)=
 RedirectMatch 403 (?i)/\.(bash|git|hg|log|svn|swp|tar)
 RedirectMatch 403 (?i)/(1|contact|i|index1|iprober|phpinfo|phpspy|product|signup|t|test|timthumb|tz|visit|webshell|wp-signup).php
 RedirectMatch 403 (?i)/(author-panel|class|database|manage|phpMyAdmin|register|submit-articles|system|usage|webmaster)/?$
 RedirectMatch 403 (?i)/(=|_mm|cgi|cvs|dbscripts|jsp|rnd|shadow|userfiles)
</ifModule>

# block humans.txt scans
<IfModule mod_rewrite.c>
	RewriteCond %{QUERY_STRING} http\:\/\/www\.google\.com\/humans\.txt\? [NC]
	RewriteRule .* - [F,L]
</IfModule>

# END 5G Blacklist