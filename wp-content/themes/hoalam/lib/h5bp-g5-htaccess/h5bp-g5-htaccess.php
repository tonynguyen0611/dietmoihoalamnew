<?php

/**
 * Show an admin notice if .htaccess isn't writable
 */
function sdf_htaccess_writable() {
  $toggle_h5bp_htaccess = (sdfGo()->sdf_get_option('misc', 'toggle_h5bp_htaccess') == 1) ? true : false;
  $toggle_5g_htaccess = (sdfGo()->sdf_get_option('misc', 'toggle_5g_htaccess') == 1) ? true : false;
	if (!is_writable(get_home_path() . '.htaccess') && ($toggle_h5bp_htaccess || $toggle_5g_htaccess)) {
    if (current_user_can('administrator')) {
      add_action('admin_notices', create_function('', "echo '<div class=\"error\"><p>" . sprintf(__('Please make sure your <a href="%s">.htaccess</a> file is writable ', SDF_TXT_DOMAIN), admin_url('options-permalink.php')) . "</p></div>';"));
    }
  }
}
add_action('admin_init', 'sdf_htaccess_writable');

/**
 * Add HTML5 Boilerplate's .htaccess via WordPress
 */
function sdf_add_h5bp_htaccess($content) {
  if ( is_multisite() ) {
		return;
	}
	
	global $wp_rewrite;
  $home_path = function_exists('get_home_path') ? get_home_path() : ABSPATH;
  $htaccess_file = $home_path . '.htaccess';
  $mod_rewrite_enabled = function_exists('got_mod_rewrite') ? got_mod_rewrite() : false;
	$toggle_h5bp_htaccess = (sdfGo()->sdf_get_option('misc', 'toggle_h5bp_htaccess') == 1) ? true : false;

  if ((!file_exists($htaccess_file) && is_writable($home_path) && $wp_rewrite->using_mod_rewrite_permalinks()) || is_writable($htaccess_file)) {
    if ($mod_rewrite_enabled) {
			$filename = dirname(__FILE__) . '/h5bp-htaccess.txt';
			$h5bp_file = ($toggle_h5bp_htaccess) ? extract_from_markers($filename, 'HTML5 Boilerplate') : array();
			return insert_with_markers($htaccess_file, 'HTML5 Boilerplate', $h5bp_file);
    }
  }

  return $content;
}

/**
 * Add 5G Blacklist's .htaccess via WordPress
 */
function sdf_add_5g_htaccess($content) {
	if ( is_multisite() ) {
		return;
	}

  global $wp_rewrite;
  $home_path = function_exists('get_home_path') ? get_home_path() : ABSPATH;
  $htaccess_file = $home_path . '.htaccess';
  $mod_rewrite_enabled = function_exists('got_mod_rewrite') ? got_mod_rewrite() : false;
	$toggle_5g_htaccess = (sdfGo()->sdf_get_option('misc', 'toggle_5g_htaccess') == 1) ? true : false;

  if ((!file_exists($htaccess_file) && is_writable($home_path) && $wp_rewrite->using_mod_rewrite_permalinks()) || is_writable($htaccess_file)) {
    if ($mod_rewrite_enabled) {
			$filename = dirname(__FILE__) . '/5g-htaccess.txt';
			$blacklist_file = ($toggle_5g_htaccess) ? extract_from_markers($filename, '5G Blacklist') : array();
			return insert_with_markers($htaccess_file, '5G Blacklist', $blacklist_file);
    }
  }

  return $content;
}
add_action('generate_rewrite_rules', 'sdf_add_h5bp_htaccess');
add_action('generate_rewrite_rules', 'sdf_add_5g_htaccess');
