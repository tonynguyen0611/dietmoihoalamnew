<?php  

if ( ! defined('SDF_PARENT_URL')) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit('No direct access allowed');
}

// Head & Header

function sdf_start_header(){
	sdf_before_header();
	sdf_the_header();
	sdf_after_header();
}
function sdf_start_page(){

	sdf_before_content();
	$page_ID = sdf_page_holder_ID();
	sdf_page_title($page_ID);
	
	$pageLayout =  sdfGo()->sdf_get_option('layout','theme_layout');
	$page = sdfGo()->sdf_is_permissible('layout','toggle_page_layout');

	if($page){
		$pageLayout = sdfGo()->_sdfPageArgs['layout']['theme_layout'];
	}else{
		$pageLayout = sdfGo()->_sdfGlobalArgs['layout']['theme_layout'];
	}
	
	if(SDF_PAGE_WIDTH == "wide") {
		if($pageLayout == 'no_col'){
			echo apply_filters('sdf_content_wrap','<div id="content-wrap" class="container-wide"><div class="container-fluid"><div class="row">');
		} else {
			echo apply_filters('sdf_content_wrap','<div id="content-wrap" class="container-wide"><div class="container"><div class="row">');
		}
	} else {
		echo apply_filters('sdf_content_wrap','<div id="content-wrap" class="container-fluid"><div class="row">');
	}
					
	if($pageLayout){
		switch($pageLayout):
			case "col_3":	
					sdf_start_sidebar_left();
					sdf_the_content();	
					sdf_start_sidebar_right(); 
			break;	
			case "col_right_2":
					sdf_the_content();	
					sdf_start_sidebar_right(); 
			break;
			case "col_left_2":	
					sdf_start_sidebar_left();
					sdf_the_content();	
				
			break;
			case "ssc_left": 
					sdf_start_sidebar_left();	
					sdf_start_sidebar_right();
					sdf_the_content();	
					
			break;
			case "css_right":
					sdf_the_content();	
					sdf_start_sidebar_left();	
					sdf_start_sidebar_right(); 
			break;
			case "no_col":
					sdf_the_content();	 
			break;
			case "custom":
					sdf_start_sidebar_left();
					sdf_the_content();		
					sdf_start_sidebar_right(); 
			break;
			default:
					sdf_start_sidebar_left();	
					sdf_the_content();	
					sdf_start_sidebar_right(); 
			break;	
		
		endswitch;	
	}
					
	if(SDF_PAGE_WIDTH == "wide") {
		if($pageLayout == 'no_col'){
			echo apply_filters('sdf_content_wrap_end','</div></div></div>');
		} else {
			echo apply_filters('sdf_content_wrap_end','</div></div></div>');
		}
	} else {
		echo apply_filters('sdf_content_wrap_end','</div></div>');
	}
	
	sdf_after_content();
}
function sdf_start_footer(){
	sdf_before_footer();
	sdf_footer();
	sdf_after_footer();

}
function sdf_start_sidebar_left(){
	sdf_before_sidebar_left();
	sdf_sidebar_left();
	sdf_after_sidebar_left();
}
function sdf_start_sidebar_right(){
	sdf_before_sidebar_right();
	sdf_sidebar_right();
	sdf_after_sidebar_right();
}
// Head & Header
function sdf_meta_title(){	do_action('sdf_meta_title'); }
function sdf_head(){ do_action('sdf_head'); }
function sdf_before_header() { do_action('sdf_before_header'); }
function sdf_the_header() { do_action('sdf_header'); }
function sdf_after_header() { do_action('sdf_after_header'); }
function sdf_before_nav(){do_action('sdf_nav_before');}
function sdf_nav($secondary_nav){ do_action('sdf_nav', $secondary_nav);}
function sdf_nav_sec(){ do_action('sdf_nav_sec');}
function sdf_after_nav(){do_action('sdf_nav_after');}

// SIDEBARS
function sdf_before_sidebar_right(){ do_action('sdf_before_sidebar_right');}
function sdf_sidebar_right() { do_action('sdf_sidebar_right'); }
function sdf_after_sidebar_right(){ do_action('sdf_after_sidebar_right');}

function sdf_before_sidebar_left(){ do_action('sdf_before_sidebar_left');}
function sdf_sidebar_left() { do_action('sdf_sidebar_left'); }
function sdf_after_sidebar_left(){ do_action('sdf_after_sidebar_left');}
// Content
function sdf_before_content() { do_action('sdf_before_content'); } 
function sdf_the_content() { do_action('sdf_the_content'); } 
function sdf_after_content() { do_action('sdf_after_content'); } 
// Footer
function sdf_before_footer() { do_action('sdf_before_footer'); }
function sdf_footer() { do_action('sdf_footer'); }
function sdf_after_footer() { do_action('sdf_after_footer'); }
