<?php



/*	
* Deferring and asyncing scripts
*/

// DEFER internal scripts
add_filter( 'script_loader_tag', 'sdf_defer_scripts', 10, 3 );
function sdf_defer_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 
		'jquery-migrate',
		'cookie',
		'sdf_contact_js',
		'sdf_bs_js',
		'sdf_prettyphoto_js',
		'sdf_smoothscroll',
		'sdf_scrollto',
		'sdf_yt_bg_video',
		'sdf_waypoints_js',
		'sdf_js'
	);

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script type="text/javascript" src="' . $src . '" defer="defer"></script>' . "\n";
    }

    return $tag;
}


// ASYNC external scripts
//add_filter( 'script_loader_tag', 'sdf_async_scripts', 10, 3 );
function sdf_async_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to async
	$async_scripts = array( 
		'sdf_bs_js'
	);

    if ( in_array( $handle, $async_scripts ) ) {
        return '<script type="text/javascript" src="' . $src . '" async="async"></script>' . "\n";
    }

    return $tag;
}
 
add_action('init', 'sdf_register_script');
function sdf_register_script(){
	
	global $sdf_accept_enc;
	if(!isset($sdf_accept_enc)) $sdf_accept_enc = '';
	// admin scripts
	wp_register_script('sdf_bs_js_admin', SDF_BOOTSTRAP.'/js/bootstrap.js', array('jquery'), null, true);
	wp_register_script('sdf_colorpicker_js', SDF_JS.'/jquery.minicolors.js', array('jquery'));
	wp_register_script('media_upload_js', SDF_JS.'/sdf.media.upload.js', array('jquery'), ''); 
	wp_register_script('sdf_media_js', SDF_JS.'/sdf.media.js', array('jquery'), ''); 
	wp_register_script('sdf_elements_js', SDF_JS.'/sdf.elements.js', array('jquery'), ''); 
	wp_register_script('sdf_admin_js', SDF_JS.'/sdf.admin.js', array('jquery'), ''); 
	wp_register_script('sdf_page_js', SDF_JS.'/sdf.page.js', array('jquery'), ''); 
	
	// front-end scripts
	wp_register_script('sdf_bs_js', SDF_BOOTSTRAP.'/js/bootstrap.min.js', array('jquery'), null, true);
	wp_register_script('sdf_prettyphoto_js', SDF_PRETTYPHOTO_PATH.'/jquery.prettyPhoto.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_nicescroll', SDF_JS.'/jquery.nicescroll.min.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_smoothscroll', SDF_JS.'/jquery.smoothscroll.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_scrollto', SDF_JS.'/jquery.scrollTo.min.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_yt_bg_video', SDF_YTPLAYER_PATH.'/jquery.mb.YTPlayer.min.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_waypoints_js', SDF_JS.'/jquery.waypoints.min.js'.$sdf_accept_enc, array('jquery'), null, true);
	wp_register_script('sdf_js', SDF_JS.'/sdf.min.js'.$sdf_accept_enc, array('jquery'), null, true);
	
	// admin styles 	 	
	wp_register_style('sdf-bootstrap-admin',  SDF_BOOTSTRAP.'/css/bootstrap.admin.css', array(), null, 'screen');
	wp_register_style('sdf-bootstrap-admin-theme',  SDF_BOOTSTRAP.'/css/bootstrap-theme.admin.css', array(), null, 'screen');		
	wp_register_style('sdf-colorpicker',  SDF_CSS.'/jquery.minicolors.css', array(), null, 'screen');	
	wp_register_style('sdf-css-admin', SDF_CSS.'/sdf.admin.css', array(), null, 'screen');
	wp_register_style('sdf-css-wp', SDF_CSS.'/sdf.wp.css', array(), null, 'screen');
	wp_register_style('sdf-css-wp-color', SDF_CSS.'/sdf.wp-color.css', array(), null, 'screen');
	
	// front-end styles
	wp_register_style('sdf-prettyphoto-css', SDF_PRETTYPHOTO_PATH.'/prettyPhoto.css'.$sdf_accept_enc, array(), null, 'screen');
	wp_register_style('sdf-animate-css', SDF_CSS.'/animate.min.css'.$sdf_accept_enc, array(), null, 'screen');
	wp_register_style('sdf-yt-bg-video', SDF_YTPLAYER_PATH.'/css/jquery.mb.YTPlayer.min.css'.$sdf_accept_enc, array(), null, 'screen');
	
	// all styles
	wp_register_style( 'sdf-font-awesome', 'https://netdna.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css', array(), null, 'screen' );
	  						
}


if(is_admin()):
	add_action('admin_enqueue_scripts', 'sdf_admin_scripts');
	add_action('admin_print_styles', 'sdf_admin_styles'); 
else:
	add_action('wp_enqueue_scripts', 'sdf_google_fonts' );
	add_action('wp_enqueue_scripts', 'sdf_styles' );
	add_action('wp_enqueue_scripts', 'sdf_scripts');
endif;


/*
 * Load Front Styles
 */
function sdf_styles(){

	global $sdf_accept_enc;
	if(!isset($sdf_accept_enc)) $sdf_accept_enc ='';
	
	$pageLayout = sdfGo()->sdf_get_option('layout','theme_layout');
	$page = sdfGo()->sdf_get_option('layout','toggle_page_layout');
	
	wp_enqueue_style('sdf-font-awesome');
	wp_enqueue_style('sdf-prettyphoto-css');
	if( sdfGo()->sdf_get_option('animations', 'toggle_animations') || sdfGo()->sdf_get_option('lightbox', 'lightbox_animation_type') != 'No' || sdfGo()->sdf_get_global('animations','feat_img_animate') ) {
		wp_enqueue_style('sdf-animate-css');
	}
	if(sdfGo()->sdf_get_global('misc','toggle_yt_bg_video')) {
		wp_enqueue_style('sdf-yt-bg-video');
	}		

	$current_preset_id = sdfGo()->sdf_get_option('preset', 'theme_preset');
	$filename = (SDF_SSL) ? 'ssl-preset-'.$current_preset_id.'.css' : 'preset-'.$current_preset_id.'.css';

	if(file_exists(SDF_PRESET_CSS_PATH.'/css/'.$filename) && !is_multisite()) {
		wp_enqueue_style('sdf-css-typography', SDF_PRESET_CSS_URL . $filename . $sdf_accept_enc, array(), filemtime(SDF_PRESET_CSS_PATH.'/css/'.$filename));
	}
	else {
		$nonce_css_tbs = wp_create_nonce("nonce_css_tbs");
		wp_enqueue_style( 'sdf-css-tbs', admin_url('admin-ajax.php').'?action=tbs_css&nonce='.$nonce_css_tbs );
		$nonce_css_typography = wp_create_nonce("nonce_css_typography");
		wp_enqueue_style( 'sdf-css-typography', admin_url('admin-ajax.php').'?action=typography_css&nonce='.$nonce_css_typography );		
	}
	
}

add_action('wp_ajax_tbs_css', 'sdf_tbs_styles');
add_action('wp_ajax_nopriv_tbs_css', 'sdf_tbs_styles');	
add_action('wp_ajax_typography_css', 'sdf_typography_styles');
add_action('wp_ajax_nopriv_typography_css', 'sdf_typography_styles');	

/*
 * Load Admin Styles
 */
 function sdf_admin_styles(){
	global $pagenow;
	$current = (isset($_GET['page'])) ? $_GET['page'] : '';
	$post_type = (isset($_GET['post_type'])) ? $_GET['post_type'] : '';
	$pages = array( 'edit.php' );
	$sdf_admin_pages = array('sdf','sdf-settings','sdf-silo','sdf-silo-manual-builder','sdf-header','sdf-layout','sdf-shortcode','sdf-styles','revslider','sdf-footer', 
	'seo', 'su-fofs', 'su-misc', 'su-user-code', 'su-autolinks', 'su-files', 'su-internal-link-aliases', 'su-meta-descriptions', 'su-meta-keywords', 'su-meta-robots', 'su-opengraph', 'seo-ultimate', 'su-wp-settings', 'su-titles', 'su-sds-blog');
	if( in_array( $current, $sdf_admin_pages) || in_array( $pagenow, $pages ) || in_array( $pagenow, array( 'post.php', 'post-new.php' )) ) {
		wp_enqueue_style('sdf-bootstrap-admin');
		wp_enqueue_style('sdf-bootstrap-admin-theme');
		wp_enqueue_style('sdf-font-awesome');
	}
	wp_enqueue_style('sdf-colorpicker');
	wp_enqueue_style('sdf-css-admin');
	if( in_array( $current, $sdf_admin_pages )) {
		wp_enqueue_style('sdf-css-wp');
		// overwrite color style
		global $_wp_admin_css_colors, $wp_version;
		$color_scheme = get_user_option( 'admin_color' );	
		if ( empty( $_wp_admin_css_colors[ $color_scheme ] ) ) {
			$color_scheme = 'light';
		}
		if ( ('light' == $color_scheme) && ($wp_version >= 3.8) ) {
			wp_enqueue_style('sdf-css-wp-color');
		}
	}
	
}
function sdf_google_fonts(){
	$fonts = sdfGo()->sdf_get_global('typography','google_web_font');
	if($fonts):
	foreach($fonts as $font):
	if(isset($font) && $font != ""):
			$font_name = str_replace( " ", "+", $font);
			$font_handle = strtolower(str_replace( " ", "-", $font));
			wp_register_style('sdf-google-'.$font_handle, '//fonts.googleapis.com/css?family='.$font_name, array(), '1.0.0', 'all');
			wp_enqueue_style('sdf-google-'.$font_handle);
		endif;
	endforeach;
		
	endif;
}

function sdf_admin_scripts(){ 
    
	global $pagenow;
	
	$current = (isset($_GET['page'])) ? $_GET['page'] : '';
	$post_type = (isset($_GET['post_type'])) ? $_GET['post_type'] : '';
	$pages = array( 'edit.php', 'widgets.php');
	$sdf_admin_pages = array('sdf','sdf-settings','seo','sdf-silo','sdf-silo-manual-builder','sdf-header','sdf-layout','sdf-shortcode','sdf-styles','revslider','sdf-footer');
		if ( in_array( $current, $sdf_admin_pages)){
			// global level
			wp_enqueue_script('jquery');
			//get new wp35 uploader on sdf pages
			if(function_exists('wp_enqueue_media'))
				wp_enqueue_media();
			
			if($current == 'sdf-layout'){
				wp_enqueue_script('jquery-ui-slider');
			}
			
			if($current == 'sdf-header'){
				wp_enqueue_script( 'jquery-ui-sortable' );
			}
			
		} elseif( in_array( $pagenow, $pages ) || in_array( $pagenow, array( 'post.php', 'post-new.php' )) ) {
			// admin edit page/post level
			wp_enqueue_script('jquery-ui-slider');
			wp_enqueue_script('sdf_page_js');
		}
		
		if ( in_array( $current, $sdf_admin_pages) || in_array( $pagenow, $pages ) || in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ){
		
		//wp_enqueue_script('sdf_media_js');
		//wp_enqueue_script('sdf_elements_js');
		wp_enqueue_script('sdf_colorpicker_js');
		wp_enqueue_script('sdf_bs_js_admin');
		
		// START sdf_admin_js
		wp_enqueue_script('sdf_admin_js');
			// pass data
			$sidebars_id_var = sdfGo()->_wpuGlobal->_sdfSidebars;
			foreach($sidebars_id_var as $key => &$value) {
				$value = str_replace("-", "_", urlencode(strtolower($key)));
			}
			$preset_colors = sdfGo()->sdf_get_global('typography', 'preset_colors');
			if(is_array($preset_colors)){
				if (sdfGo()->sdf_get_global('typography', 'theme_primary_color') != '') {
					$preset_colors['Primary'] = sdfGo()->sdf_get_global('typography', 'theme_primary_color');
				}
				foreach($preset_colors as $preset_key => $preset_value) {
					if (array_key_exists($preset_value, $preset_colors)) {
						$preset_colors[$preset_key] = $preset_colors[$preset_value];
					}
				}
			}
			$sdf_js_localized_data = array(
				'sdf_sidebars' => $sidebars_id_var,
				'sdf_preset_colors' => $preset_colors
			);
			sdf_pass_data_to_scripts( $sdf_js_localized_data, 'sdf_admin_js' );
		// END sdf_admin_js
		}
		
		// media
		wp_enqueue_script( 'thickbox' );
		wp_enqueue_script( 'media_upload_js' );
	
} // sdf_admin_scripts 
	
function sdf_scripts(){

	wp_enqueue_script('jquery');
	wp_enqueue_script('sdf_bs_js');
	wp_enqueue_script('sdf_prettyphoto_js');
	//wp_enqueue_script('sdf_video_js');
	if(sdfGo()->sdf_get_global('misc','toggle_nicescroll') == '1') {
		wp_enqueue_script('sdf_nicescroll');
	}
	elseif(sdfGo()->sdf_get_global('misc','toggle_nicescroll') == '2') {
		wp_enqueue_script('sdf_smoothscroll');
	}
	if(sdfGo()->sdf_get_global('misc','toggle_scrollto')) {
		wp_enqueue_script('sdf_scrollto');
	}
	if(sdfGo()->sdf_get_global('misc','toggle_yt_bg_video')) {
		wp_enqueue_script('sdf_yt_bg_video');
	}
	wp_enqueue_script('sdf_waypoints_js');
	wp_enqueue_script('sdf_js');
	// pass data to sdf_js
	// equal heights
	$eqheight_areas = array();
	$all_areas = sdfGo()->_wpuGlobal->_sdfSidebars;
	unset($all_areas['slider-widget-area']);
	foreach ($all_areas as $id => $name) {
		$id_var = str_replace("-", "_", urlencode(strtolower($id)));
		$eqheight_widgets = sdfGo()->sdf_get_option('typography', $id_var.'_equal_heights');
		if ($eqheight_widgets != 'off' && function_exists('dynamic_sidebar') && is_active_sidebar($id)) {
			$eqheight_areas[] = $id;
		}
	}
	
	$default_animation_duration = (sdfGo()->sdf_get_global('animations','default_animation_duration')) ? sdfGo()->sdf_get_global('animations','default_animation_duration') : 'animated08';
	$scrolltotop_duration = (sdfGo()->sdf_get_global('animations','scrolltotop_duration')) ? sdfGo()->sdf_get_global('animations','scrolltotop_duration') : '700';
	$scrolltotop_easing_type = (sdfGo()->sdf_get_global('animations','scrolltotop_easing_type')) ? sdfGo()->sdf_get_global('animations','scrolltotop_easing_type') : 'linear';
	$sdf_js_localized_data = array(
	  'eqheight_areas' => $eqheight_areas,
		'default_animation_duration' => $default_animation_duration,
	  'scrolltotop_duration' => $scrolltotop_duration,
	  'scrolltotop_easing_type' => $scrolltotop_easing_type
	);

	sdf_pass_data_to_scripts( $sdf_js_localized_data, 'sdf_js' );
	
}

function sdf_pass_data_to_scripts( $localized_data, $enqueued_script ){
	
	global $wp_scripts;
	$data = $wp_scripts->get_data($enqueued_script, 'data');
	if(empty($data)) {
	  wp_localize_script($enqueued_script, $enqueued_script.'_obj', $localized_data);
	}
	else {
	  if(!is_array($data)) {
		$data = json_decode(str_replace('var '.$enqueued_script.'_obj = ', '', substr($data, 0, -1)), true);
	  }
	  foreach($data as $key => $value) {
		$localized_data[$key] = $value;
	  }
	  $wp_scripts->add_data($enqueued_script, 'data', '');
	  wp_localize_script($enqueued_script, $enqueued_script.'_obj', $localized_data);
	}
}
// Omit closing PHP tag to avoid "Headers already sent" issues.
