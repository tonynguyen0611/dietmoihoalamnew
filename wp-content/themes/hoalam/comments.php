<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: comments.php
 * DESCRIPTIONS: Theme Comments
 * @package wpultimate
 *
 * AUTHOR:  SEO Design Framework
 */ 

?>
	<div id="comments" class="comments-area">
	<?php 
	if ( post_password_required() ) : ?>
		<div class="no-password alert alert-info"><?php _e( 'This post is password protected. Enter the password to view any comments.', SDF_TXT_DOMAIN ); ?></div>
	</div><!-- #comments -->
	<?php
		return;
	endif;

	if ( get_comments_number() != "0" || comments_open() ) : ?>
		<div class="comments-title">
			<?php 
			$ccount = (int) get_comments_number();
			printf( _n( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', $ccount, SDF_TXT_DOMAIN ), 
						number_format_i18n( $ccount ), '<span>' . get_the_title() . '</span>' );
			?>
					
		</div>
	<?php 
	endif;
		
	if ( have_comments() ) : ?>
		
		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through
			echo "<nav class='comment_nav_links comment_nav_links_above'>";
			echo "<span class='comment_prev_link'>";
				 previous_comments_link( __( '&larr; Older Comments', SDF_TXT_DOMAIN ) );
			echo "</span>";
			echo "<span class='comment_next_link'>";
				 next_comments_link( __( 'Newer Comments &rarr;', SDF_TXT_DOMAIN ) );
			echo "</span>";
			echo "</nav>";
		endif; // check for comment navigation ?>

		<ol class="comment-list media-list">
			<?php
				/* Loop through and list the comments. Tell wp_list_comments()
				 * to use sdf_comment_list() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define sdf_comment_list() and that will be used instead.
				 */
				wp_list_comments( array( 'type'=> 'comment', 'avatar_size' => '60', 'callback' => 'sdf_comments_list' ) );
			?>
		</ol>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through
			echo "<nav class='comment_nav_links comment_nav_links_below'>";
			echo "<span class='comment_prev_link'>";
				 previous_comments_link( __( '&larr; Older Comments', SDF_TXT_DOMAIN ) );
			echo "</span>";
			echo "<span class='comment_next_link'>";
				 next_comments_link( __( 'Newer Comments &rarr;', SDF_TXT_DOMAIN ) );
			echo "</span>";
			echo "</nav>";
		endif; // check for comment navigation ?>

	<?php endif; // have_comments() ?>

	<?php 
	

	if(comments_open()){
		 
		echo '<div class="comment_form">';
		echo '<p class="comments-contribute">' . __( 'Want to join the discussion? <br/>Feel free to contribute!' ,SDF_TXT_DOMAIN ) . '</p>';
		comment_form();
		echo "</div>";
	}
	else if(get_comments_number())
	{
		/*
		* If there are no comments and comments are closed, let's leave a little note, shall we?
		*/
	 	 ?>
	 	<div class="no-comments alert alert-info"><?php _e( 'Comments are closed.', SDF_TXT_DOMAIN ); ?></div>
		<?php 
	} 
	?>
</div><!-- #comments -->
