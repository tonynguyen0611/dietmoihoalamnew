<?php
$search_params = apply_filters('sdf_searchform_params', array(
	
	'searchform_id'	   	=> 's',
	'searchform_action'	=> home_url( '/' ),
	'placeholder'  	=> __('Search', SDF_TXT_DOMAIN)
));

?>
<form id="searchform" class="searchform" action="<?php echo $search_params['searchform_action']; ?>" method="get" role="search">
<label class="screen-reader-text sr-only" for="<?php echo $search_params['searchform_id']; ?>">Search for:</label>
<div class="input-group"><input id="<?php echo $search_params['searchform_id']; ?>" type="text" name="<?php echo $search_params['searchform_id']; ?>" class="form-control" placeholder="<?php echo $search_params['placeholder']; ?>" value="<?php the_search_query(); ?>">
<span class="input-group-btn"><button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button></span></div>
</form>