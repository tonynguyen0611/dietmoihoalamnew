<?php
/**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Header
 * DESCRIPTIONS: Header and navigation code for all theme files
 *
 * AUTHOR:  SEO Design Framework
 */ 
 sdf_head_html();
 $nicescroll = (sdfGo()->sdf_get_global('misc','toggle_nicescroll') == '1') ? '1' : '0';
 $scrollto = (sdfGo()->sdf_get_global('misc','toggle_scrollto')) ? '1' : '0';
 $ytbgvideo = (sdfGo()->sdf_get_global('misc','toggle_yt_bg_video')) ? '1' : '0';
 // video params
 $ytbgvideo_url = sdfGo()->sdf_get_page('misc','yt_bg_video_url');
 $ytbgvideo_volume = sdfGo()->sdf_get_page('misc','yt_bg_video_volume');
 $ytbgvideo_opacity = sdfGo()->sdf_get_page('misc','yt_bg_video_opacity');
 $ytbgvideo_mute = (sdfGo()->sdf_get_page('misc','yt_bg_video_mute') == '1') ? 'true' : 'false';
 $ytbgvideo_player = (sdfGo()->sdf_get_page('misc','yt_bg_video_player_controls') == '1') ? 'true' : 'false';
 $ytbgvideo_autoplay = (sdfGo()->sdf_get_page('misc','yt_bg_video_autoplay') == '1') ? 'true' : 'false';
 $ytbgvideo_ratio = sdfGo()->sdf_get_page('misc','yt_bg_video_ratio');
 $ytbgvideo_quality = sdfGo()->sdf_get_page('misc','yt_bg_video_quality');
 $ytbgvideo_logo = (sdfGo()->sdf_get_page('misc','yt_bg_video_logo') == '1') ? 'true' : 'false';
 
	// microdata
	$microdata = array();
	$microdata['itemscope'] = 'itemscope';
	$microdata['itemtype']  = 'http://schema.org/WebPage';

	if ( is_search() ) {
		$microdata['itemtype']  = 'http://schema.org/SearchResultsPage';
	}
?>
<body <?php body_class(); ?> data-nicescroll="<?php echo $nicescroll; ?>" data-scrollto="<?php echo $scrollto; ?>" data-ytbgvideo="<?php echo $ytbgvideo; ?>" itemtype="<?php echo $microdata['itemtype']; ?>" itemscope="<?php echo $microdata['itemscope']; ?>">
<a class="sdf-section-anchor" id="top"></a>

<?php if( ( ( is_front_page() || is_home() || is_single() || is_page() ) && sdfGo()->sdf_get_page('misc', 'yt_bg_video_url') != '' ) ) : ?>
    <a id="bgndVideo" class="yt-bg-video player" data-property="{ videoURL:'<?php echo $ytbgvideo_url; ?>',containment:'body',autoPlay:<?php echo $ytbgvideo_autoplay; ?>, mute:<?php echo $ytbgvideo_mute; ?>, startAt:0, ratio:'<?php echo $ytbgvideo_ratio; ?>', quality:'<?php echo $ytbgvideo_quality; ?>', showControls:<?php echo $ytbgvideo_player; ?>, showYTLogo:<?php echo $ytbgvideo_logo; ?>, opacity:<?php echo $ytbgvideo_opacity; ?>, vol:<?php echo $ytbgvideo_volume; ?> }" data-volume="<?php echo $ytbgvideo_volume; ?>">Background Video</a>            	
		<?php if( sdfGo()->sdf_get_page('misc','yt_bg_video_volume_button') == '1' ) : ?>
			<?php $mute_icon = ( sdfGo()->sdf_get_page('misc','yt_bg_video_mute') == '1' ) ? 'fa-volume-up' : 'fa-volume-off'; ?>
			<a href="#" class="sdf-video-control sdf-video-volume"><i class="fa <?php echo $mute_icon; ?> fa-2x"></i></a>
		<?php endif; ?>
		<?php if( sdfGo()->sdf_get_page('misc','yt_bg_video_play_button') == '1' ) : ?>
			<?php $play_icon = (sdfGo()->sdf_get_page('misc','yt_bg_video_autoplay') == '1') ? 'fa-pause' : 'fa-play'; ?>
			<a href="#" class="sdf-video-control sdf-video-play"><i class="fa <?php echo $play_icon; ?> fa-2x"></i></a>
		<?php endif; ?>
		
<?php endif; ?>
	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
	
	<?php 
	// boxed wrap
	if(SDF_PAGE_WIDTH != "wide") {
		echo '<div id="wrap" class="container"><div class="row">';
	}	
	
	sdf_start_header(); ?>
<!-- end Header Blocks --> 