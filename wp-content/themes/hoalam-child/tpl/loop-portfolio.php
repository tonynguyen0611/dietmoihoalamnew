<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: WordPress Post Loop.
 * DESCRIPTIONS: WordPress Loop for displaying posts and post content.
 *
 * AUTHOR:  SEO Design Framework
 */ 
	
	$sdf_wide_template = ((SDF_PAGE_WIDTH == 'wide') && (sdf_class('main') == 'col-md-12 ')) ? true : false;
	
	while (have_posts()) : the_post(); ?>
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
<?php 
	
	$page_id = get_the_ID();
	$post_content = get_the_content();
	
	if ( (function_exists('has_post_thumbnail')) && (has_post_thumbnail()) ) { ?>
			
		<div class="entry-thumbnail">
		<?php if ($sdf_wide_template) : ?>
			<div class="container">
		<?php endif;
	
		// featured image animation on/off
		$data_animation = '';
		$data_duration = '';
		$animation_class = '';
		if(sdfGo()->sdf_get_option('animations', 'feat_img_animate')) {
		$data_animation = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != 'No' && sdfGo()->sdf_get_option('animations', 'feat_img_animation_type') != '') ? ' data-animation="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_type').'"' : '';
		$data_duration = (sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration') != '' && $data_animation != '') ? ' data-duration="'.sdfGo()->sdf_get_option('animations', 'feat_img_animation_duration').'"' : '';
		$animation_class = ($data_animation != '') ? ' viewport_animate animate_now' : '';
		}
		
		$featured_view ='';
		$pin_view =  wp_get_attachment_image_src( get_post_thumbnail_id($page_id), 'sdf-image-md-12', false, '' );
		$pin_view = $pin_view[0];
		$featured_view = '<img class="img-responsive'.$animation_class.'"'.$data_animation.$data_duration.' alt="'.get_the_title().'" src="'.$pin_view.'">';
		echo '<div class="wp-featured-image"><a href="'.$pin_view.'" title="'.get_the_title().'" data-rel="prettyPhoto" class="pretty-view">'.$featured_view.'</a></div>';
	
	
		if ($sdf_wide_template) : ?>
			</div>
		<?php endif; ?>
		</div><!-- .entry-thumbnail -->
		
	<?php } ?>
	 
	<?php if ($sdf_wide_template) : ?>
		<div class="container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="container-fluid">
	<?php endif;
	//share links on single post
	if (is_singular() && sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'above'){
		sdf_social_sharing_buttons();
	}
	
	if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>
	
	<div class="entry-content">
	<?php if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
		<div class="container">
	<?php endif; ?>
	
		<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', SDF_TXT_DOMAIN ) ); ?>
		
	<?php if ($sdf_wide_template && !(strpos($post_content,'[/sdf_hero]') || strpos($post_content,'[/sdf_row]'))) : ?>
		</div>
	<?php endif; ?>
	</div><!-- .entry-content -->
	
	<?php if ($sdf_wide_template) : ?>
		<div class="container">
	<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
		<div class="container-fluid">
	<?php endif;
	
	//share links on single post
	if (sdfGo()->sdf_get_option('social_media', 'toggle_social_share') && sdfGo()->sdf_get_option('social_media', 'social_share_position') == 'below'){
		sdf_social_sharing_buttons();
	}
	
	edit_post_link(__('Edit page', SDF_TXT_DOMAIN), '<div class="container-fluid"><p class="edit">','</p></div>'); ?>
		
	<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
		</div>
	<?php endif; ?>	
		
	</article>
	
	<?php if ( is_singular() && sdfGo()->sdf_get_option('blog', 'toggle_portfolio_comments') ) : ?>
		<?php if ( comments_open() || get_comments_number() != "0" ) : ?>
			<?php if ($sdf_wide_template) : ?>
				<div class="container">
			<?php elseif (sdf_class('main') == 'col-md-12 ') : ?>
				<div class="container-fluid">
			<?php endif; ?>
			
				<div class="row">	
					<div class="col-md-11 col-md-offset-1">
							<?php comments_template(); ?>
					</div>
				</div>
				
			<?php if ($sdf_wide_template || sdf_class('main') == 'col-md-12 ') : ?>
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endif; ?>
	
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
