<?php
 /**
 * THEME NAME: SEO Design Framework 
 * VERSION: 1.0
 *
 * TYPE: Global Footer
 * DESCRIPTIONS: Template to display the footer of the theme.
 *
 * AUTHOR:  SEO Design Framework
 */ 
?>
<?php sdf_start_footer(); ?>
<!-- end Footer Blocks -->      
<?php
	wp_footer();
?>
	<script>
	jQuery("#woocommerce-product-search-field-0").attr('placeholder','Tìm kiếm sản phẩm...');
jQuery(".a.button.wc-forward").html("Tiếp tục mua");
	</script>
</body>
</html>
